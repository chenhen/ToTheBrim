﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif



#include "codegen/il2cpp-codegen-metadata.h"





IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END




// 0x00000001 System.Void AudioManager::Awake()
extern void AudioManager_Awake_mD7873A38A3ED577A313A05D24BF6511E59EDEC01 (void);
// 0x00000002 System.Void AudioManager::PlaySuccess()
extern void AudioManager_PlaySuccess_m889550BD8021F9E8D34CF8EF258D939304F41592 (void);
// 0x00000003 System.Void AudioManager::PlayFail()
extern void AudioManager_PlayFail_m2EBA3AB666F25E529F92838FA8AD8DFC237E283E (void);
// 0x00000004 System.Void AudioManager::PlayPouring()
extern void AudioManager_PlayPouring_m89F9FF74C7EEAAA4DC3F3571E3A17DDCF12095F6 (void);
// 0x00000005 System.Void AudioManager::PlayPutDown()
extern void AudioManager_PlayPutDown_mF66C8A85DB3F79B4C8CAB1C15B07A363906C7AB5 (void);
// 0x00000006 System.Void AudioManager::PlayReset()
extern void AudioManager_PlayReset_mB174F3D616241B53848F3152C5C1A61801A42A3D (void);
// 0x00000007 System.Void AudioManager::StopEffects()
extern void AudioManager_StopEffects_m0FB721DBE099F916C2AA75B9DE1A47EAD9D6598F (void);
// 0x00000008 System.Void AudioManager::.ctor()
extern void AudioManager__ctor_m6C686441D1A1A223E4CF940A8EB0128535D603BD (void);
// 0x00000009 System.Void GameManager::Awake()
extern void GameManager_Awake_m22F42B2A82708B10F652CAD8F2E0A4767110FF30 (void);
// 0x0000000A System.Void GameManager::Start()
extern void GameManager_Start_m26461AEF27E44DB8FECCBC19D6C9E228B658BF8E (void);
// 0x0000000B System.Void GameManager::Update()
extern void GameManager_Update_mC9303BA7C3117BD861F49F8E36151CC52117E6C1 (void);
// 0x0000000C System.Void GameManager::OnFinish()
extern void GameManager_OnFinish_m795F03CAEB00F8B8486BA4ADA99A705C1C672D20 (void);
// 0x0000000D System.Collections.IEnumerator GameManager::Cork()
extern void GameManager_Cork_mD9B04D863523B536A5B3660DFB5374D64387AB61 (void);
// 0x0000000E System.Void GameManager::Onwin()
extern void GameManager_Onwin_m51D63DA951D328D51482F5786657863B6A45A8B0 (void);
// 0x0000000F System.Void GameManager::OnLoose()
extern void GameManager_OnLoose_m6EB04A2B6EEE20375AB4764571C72E10CF3BB605 (void);
// 0x00000010 System.Collections.IEnumerator GameManager::WaitAndReset()
extern void GameManager_WaitAndReset_m27D83AD57897C467040734244B7C73CD7DAC48EC (void);
// 0x00000011 System.Void GameManager::Reset()
extern void GameManager_Reset_mA71D1D96D0E38DEC1C7E61CE76675E314FE252C2 (void);
// 0x00000012 System.Void GameManager::.ctor()
extern void GameManager__ctor_mE8666F6D0CA9C31E16B719F79780DC4B0245B64D (void);
// 0x00000013 System.Void GameManager_<Cork>d__20::.ctor(System.Int32)
extern void U3CCorkU3Ed__20__ctor_m1B3AEA4ECAAF4A5DD71410ED9F45A805C876DD6C (void);
// 0x00000014 System.Void GameManager_<Cork>d__20::System.IDisposable.Dispose()
extern void U3CCorkU3Ed__20_System_IDisposable_Dispose_mA1C04912A692F37B43576DAAC5238DCE6D38A972 (void);
// 0x00000015 System.Boolean GameManager_<Cork>d__20::MoveNext()
extern void U3CCorkU3Ed__20_MoveNext_m7D50079DFAE8DD7C70F8868752A6B72048F564C2 (void);
// 0x00000016 System.Object GameManager_<Cork>d__20::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CCorkU3Ed__20_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m0E98DCD2422A1C444C8783B78DAEF8F26FDD7F37 (void);
// 0x00000017 System.Void GameManager_<Cork>d__20::System.Collections.IEnumerator.Reset()
extern void U3CCorkU3Ed__20_System_Collections_IEnumerator_Reset_m4B46102788FE393F65B8A0ED00E0617AE2DC38C5 (void);
// 0x00000018 System.Object GameManager_<Cork>d__20::System.Collections.IEnumerator.get_Current()
extern void U3CCorkU3Ed__20_System_Collections_IEnumerator_get_Current_mF144219FA16314C6311E6D16F64CCF2E40C74869 (void);
// 0x00000019 System.Void GameManager_<WaitAndReset>d__23::.ctor(System.Int32)
extern void U3CWaitAndResetU3Ed__23__ctor_m29886341D87FA57883BA5D66A9441D560B58F68C (void);
// 0x0000001A System.Void GameManager_<WaitAndReset>d__23::System.IDisposable.Dispose()
extern void U3CWaitAndResetU3Ed__23_System_IDisposable_Dispose_m9DB94CFF080D4848C5A141756B5BB2BBE1842CD9 (void);
// 0x0000001B System.Boolean GameManager_<WaitAndReset>d__23::MoveNext()
extern void U3CWaitAndResetU3Ed__23_MoveNext_mB8EC03324C16D7C36C39A98CA75B3C4A9FB778ED (void);
// 0x0000001C System.Object GameManager_<WaitAndReset>d__23::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CWaitAndResetU3Ed__23_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m1DDA78D95C32E45F45A58B8B1868AAAF556209BE (void);
// 0x0000001D System.Void GameManager_<WaitAndReset>d__23::System.Collections.IEnumerator.Reset()
extern void U3CWaitAndResetU3Ed__23_System_Collections_IEnumerator_Reset_m7B1AE96CF48EDAF04FDAF37AA422A0BEC3025D44 (void);
// 0x0000001E System.Object GameManager_<WaitAndReset>d__23::System.Collections.IEnumerator.get_Current()
extern void U3CWaitAndResetU3Ed__23_System_Collections_IEnumerator_get_Current_mC4FC314B8341CCBC51EA81CD9F6FB1B383DFDC83 (void);
// 0x0000001F System.Void InputHandler::Awake()
extern void InputHandler_Awake_m312AFC247EF04CB92C06AD4F35AD02F621C62388 (void);
// 0x00000020 System.Void InputHandler::Update()
extern void InputHandler_Update_m22B46EB5A6FA4F2C8B5B8790BE389EF5BC8BCACE (void);
// 0x00000021 System.Void InputHandler::StartPouring()
extern void InputHandler_StartPouring_mC4CDFA2AFE573EC3D87D37DC2A8A72A6BC34D811 (void);
// 0x00000022 System.Boolean InputHandler::IsPointerOverGameObject()
extern void InputHandler_IsPointerOverGameObject_m39ECD4281D05F816512779423A6F8D97E215D874 (void);
// 0x00000023 System.Void InputHandler::.ctor()
extern void InputHandler__ctor_mA408269F104C8A8C1931F10877DD4206AB201C90 (void);
// 0x00000024 System.Void TestingsManager::Update()
extern void TestingsManager_Update_mF85D98D4DC5DF45D491B077136E3D450098E4CCF (void);
// 0x00000025 System.Void TestingsManager::.ctor()
extern void TestingsManager__ctor_m5239FF3C69C48D92C66A2611CA990C697A237239 (void);
// 0x00000026 System.Void ActorCOMTransform::Update()
extern void ActorCOMTransform_Update_m42CC68376DF28C96387BACFC64C48515AE9CE469 (void);
// 0x00000027 System.Void ActorCOMTransform::.ctor()
extern void ActorCOMTransform__ctor_mB7B969737BB7416B0E66C1CD4B8FDF1E8A7186CD (void);
// 0x00000028 System.Void ActorSpawner::Update()
extern void ActorSpawner_Update_mBB452D66B51B50FE636D74E7FF957A10A1FE2A07 (void);
// 0x00000029 System.Void ActorSpawner::.ctor()
extern void ActorSpawner__ctor_m48C1E536BA52A4F4EDE74F5B90749B4619F13C86 (void);
// 0x0000002A System.Void AddRandomVelocity::Update()
extern void AddRandomVelocity_Update_m6584A3F85D2E61F6D420B0B911B3C61637008449 (void);
// 0x0000002B System.Void AddRandomVelocity::.ctor()
extern void AddRandomVelocity__ctor_mC1CFE0D59EF94BA30BA6AAB81CA6FF0899F84DDF (void);
// 0x0000002C System.Void Blinker::Awake()
extern void Blinker_Awake_mC92D3CBC36B46F324C8197377BC40A434109C876 (void);
// 0x0000002D System.Void Blinker::Blink()
extern void Blinker_Blink_m4082F3A19006F464F4C14105BED460D5A1A0E273 (void);
// 0x0000002E System.Void Blinker::LateUpdate()
extern void Blinker_LateUpdate_mEE40A5EDA3EAFBB9CAD39BC413CB921E15CF6CCB (void);
// 0x0000002F System.Void Blinker::.ctor()
extern void Blinker__ctor_mAE1E307D31F5C3BB83001FBC5B2D61F827A69F3C (void);
// 0x00000030 System.Void ColliderHighlighter::Awake()
extern void ColliderHighlighter_Awake_m5920B8F626704DB40821E9BB9A85FDA1E0BFE4B3 (void);
// 0x00000031 System.Void ColliderHighlighter::OnEnable()
extern void ColliderHighlighter_OnEnable_m6F1425D169D88CC4D90E0A8F144A05C55F7E03B6 (void);
// 0x00000032 System.Void ColliderHighlighter::OnDisable()
extern void ColliderHighlighter_OnDisable_m2780DDE2CFE790FF4B04AD45654BDB74DD752893 (void);
// 0x00000033 System.Void ColliderHighlighter::Solver_OnCollision(System.Object,Obi.ObiSolver_ObiCollisionEventArgs)
extern void ColliderHighlighter_Solver_OnCollision_m8A90B739BAB8461A0179C2B4CD4EE87AB3DE8D30 (void);
// 0x00000034 System.Void ColliderHighlighter::.ctor()
extern void ColliderHighlighter__ctor_mBE8B58BC252B389A907738D87DAD06F094A62587 (void);
// 0x00000035 System.Void CollisionEventHandler::Awake()
extern void CollisionEventHandler_Awake_m744FDE1AB7D3F6777399B5108896E5E2CC61FE93 (void);
// 0x00000036 System.Void CollisionEventHandler::OnEnable()
extern void CollisionEventHandler_OnEnable_m2D3F33422D785CEDFCDCF1012A0B37B5F3ACB9F6 (void);
// 0x00000037 System.Void CollisionEventHandler::OnDisable()
extern void CollisionEventHandler_OnDisable_m75B03CB65B3D0226C04FF77A99D488596144BC32 (void);
// 0x00000038 System.Void CollisionEventHandler::Solver_OnCollision(System.Object,Obi.ObiSolver_ObiCollisionEventArgs)
extern void CollisionEventHandler_Solver_OnCollision_m484E685CBD1E502504C567FD2D5CCB6F0860D616 (void);
// 0x00000039 System.Void CollisionEventHandler::OnDrawGizmos()
extern void CollisionEventHandler_OnDrawGizmos_m7939D47B098838344A41DD08F547354E268E07A8 (void);
// 0x0000003A System.Void CollisionEventHandler::.ctor()
extern void CollisionEventHandler__ctor_m92805BEBFF6E7114772EF8DFF4D5C244467A479B (void);
// 0x0000003B System.Void DebugParticleFrames::Awake()
extern void DebugParticleFrames_Awake_mA5C0197560CB8F73D7472974A693020EA29B28B3 (void);
// 0x0000003C System.Void DebugParticleFrames::OnDrawGizmos()
extern void DebugParticleFrames_OnDrawGizmos_m51378254E7930D9050D3355F810A3BC15A4CBBED (void);
// 0x0000003D System.Void DebugParticleFrames::.ctor()
extern void DebugParticleFrames__ctor_mBE2F687FB03E680B1957BB4D2C9F19E43F3CF260 (void);
// 0x0000003E System.Void ExtrapolationCamera::Start()
extern void ExtrapolationCamera_Start_mCB1FD551CBFB047EEA0FA534CE64CA969640D51E (void);
// 0x0000003F System.Void ExtrapolationCamera::FixedUpdate()
extern void ExtrapolationCamera_FixedUpdate_m622D9ECE2C7F91E1C5A70068B94B7C3E03AF4523 (void);
// 0x00000040 System.Void ExtrapolationCamera::LateUpdate()
extern void ExtrapolationCamera_LateUpdate_mB90259A761A12F0D8301E75BC381A6C0B32B443D (void);
// 0x00000041 System.Void ExtrapolationCamera::Teleport(UnityEngine.Vector3,UnityEngine.Quaternion)
extern void ExtrapolationCamera_Teleport_m8A6D37587FD1DF2518851886F5BE70B897DE0B67 (void);
// 0x00000042 System.Void ExtrapolationCamera::.ctor()
extern void ExtrapolationCamera__ctor_mA4E8D57C9E25BC7F9DA5676D79A7FC5505073032 (void);
// 0x00000043 System.Single FPSDisplay::get_CurrentFPS()
extern void FPSDisplay_get_CurrentFPS_mA9019EF8EAA0384D37F2DE714D84E5413E783C72 (void);
// 0x00000044 System.Single FPSDisplay::get_FPSMedian()
extern void FPSDisplay_get_FPSMedian_m6ACF1BBC790E915C8CE3D94C797CE4A4E8F116DD (void);
// 0x00000045 System.Single FPSDisplay::get_FPSAverage()
extern void FPSDisplay_get_FPSAverage_m52F2116EA4DD6DA75E483FE24D21321CF9F2D4C1 (void);
// 0x00000046 System.Void FPSDisplay::Start()
extern void FPSDisplay_Start_mF1CE43CCE2AAB57ECD8D8B623B7E7358AB163C55 (void);
// 0x00000047 System.Void FPSDisplay::Update()
extern void FPSDisplay_Update_m8740416265A05D0C56CA05EC0DE3A12E5132B028 (void);
// 0x00000048 System.Void FPSDisplay::ResetMedianAndAverage()
extern void FPSDisplay_ResetMedianAndAverage_m82840B2C8D0189EA211AE4B14A8C94B22D932F70 (void);
// 0x00000049 System.Void FPSDisplay::.ctor()
extern void FPSDisplay__ctor_mC4AD184EF09FAF5C43D89D0F7C211B2754D95B18 (void);
// 0x0000004A System.Void ObiActorTeleport::Teleport()
extern void ObiActorTeleport_Teleport_m83F48614A9D7AE20261E92546E28B7C65C26DA1D (void);
// 0x0000004B System.Void ObiActorTeleport::.ctor()
extern void ObiActorTeleport__ctor_mCC83730596EA6276332F31F582F5C8D1F08EE019 (void);
// 0x0000004C System.Void ObiParticleCounter::Awake()
extern void ObiParticleCounter_Awake_m42D6C0254A2B27439769A96FE50705D33EC62B98 (void);
// 0x0000004D System.Void ObiParticleCounter::OnEnable()
extern void ObiParticleCounter_OnEnable_mA9798F6382D0620125A95B047C974EBA660926CB (void);
// 0x0000004E System.Void ObiParticleCounter::OnDisable()
extern void ObiParticleCounter_OnDisable_m3224433810D75B192BD03D399ADE656864BBD594 (void);
// 0x0000004F System.Void ObiParticleCounter::Solver_OnCollision(System.Object,Obi.ObiSolver_ObiCollisionEventArgs)
extern void ObiParticleCounter_Solver_OnCollision_mC585B439D90C23BA5FE74E65D3195651DCAEF392 (void);
// 0x00000050 System.Void ObiParticleCounter::.ctor()
extern void ObiParticleCounter__ctor_m3107B0DABE8BEA1C8EB84C5A8EEEDB3C3E4B1B52 (void);
// 0x00000051 System.Void ObjectDragger::OnMouseDown()
extern void ObjectDragger_OnMouseDown_mC96AA07AABD47631E097AEA08E9A81AE1506EEEE (void);
// 0x00000052 System.Void ObjectDragger::OnMouseDrag()
extern void ObjectDragger_OnMouseDrag_m7E0B861E5546C709DED1ACF6F33ED31D6004FA6E (void);
// 0x00000053 System.Void ObjectDragger::.ctor()
extern void ObjectDragger__ctor_m97083AF9B77C7E2554A3D0701C6FAEE5D2282A6C (void);
// 0x00000054 System.Void ObjectLimit::Update()
extern void ObjectLimit_Update_m6689D4679CA59887B038EE4BD3F2294753C57E38 (void);
// 0x00000055 System.Void ObjectLimit::.ctor()
extern void ObjectLimit__ctor_m8F9F789DA4B949FECADA677D21A26E893A0A5F41 (void);
// 0x00000056 System.Void SlowmoToggler::Slowmo(System.Boolean)
extern void SlowmoToggler_Slowmo_mF683360609D33F0B08F26A1019AAAD2BA21C8404 (void);
// 0x00000057 System.Void SlowmoToggler::.ctor()
extern void SlowmoToggler__ctor_m17E4BB8E004DE90384ADF6074E74BC7C383E7001 (void);
// 0x00000058 System.Void WorldSpaceGravity::Awake()
extern void WorldSpaceGravity_Awake_m20A0105B37EEF41CE69A528E59C13099276E4F2B (void);
// 0x00000059 System.Void WorldSpaceGravity::Update()
extern void WorldSpaceGravity_Update_m9BFD6836B3B4FD9F671F9CC77622279143C1F72C (void);
// 0x0000005A System.Void WorldSpaceGravity::.ctor()
extern void WorldSpaceGravity__ctor_m8A54BF21A4069DBE4B27EA67FDABDC1C26766539 (void);
// 0x0000005B System.Void BucketController::Update()
extern void BucketController_Update_m008F7F716352BFFA46E104AB534AFCA974BF6789 (void);
// 0x0000005C System.Void BucketController::.ctor()
extern void BucketController__ctor_mFED593F8CAEB864494D8730A259B40C16F46CA37 (void);
// 0x0000005D System.Void EmitOnKeyPress::Start()
extern void EmitOnKeyPress_Start_m984AC303BA37CD0140668F919F0D5EBA56B9B176 (void);
// 0x0000005E System.Void EmitOnKeyPress::Update()
extern void EmitOnKeyPress_Update_mEE6EFC2796CFE7CDD30FC46137513C172BA055CF (void);
// 0x0000005F System.Void EmitOnKeyPress::.ctor()
extern void EmitOnKeyPress__ctor_m168554363F3AB623ADB803B992E5BB2211C0A8FD (void);
// 0x00000060 System.Void FluidColorizer::Awake()
extern void FluidColorizer_Awake_m9F11E1D1AEFB40F7B35B112B17F0342F23E779A6 (void);
// 0x00000061 System.Void FluidColorizer::.ctor()
extern void FluidColorizer__ctor_mABDE4E9B160DDE0CA4593C3FF87C669306098903 (void);
// 0x00000062 System.Void FluidJetController::Start()
extern void FluidJetController_Start_mFCC15C1D73ED40A6531C9AADEBF1F3488F589B8A (void);
// 0x00000063 System.Void FluidJetController::Update()
extern void FluidJetController_Update_m5B7F25050B2CEFFCEC416C7CCCAB687960829BF5 (void);
// 0x00000064 System.Void FluidJetController::.ctor()
extern void FluidJetController__ctor_mAAF3034F58E75E0163933D2AF31B79C98D67D69A (void);
// 0x00000065 System.Void MazeManager::Start()
extern void MazeManager_Start_m276D6F781914CDF48C22AB61C8B8EB48B77EE3FE (void);
// 0x00000066 System.Void MazeManager::OnDestroy()
extern void MazeManager_OnDestroy_mFB114E6F773097ECC5FAC553F342E3E9B7792130 (void);
// 0x00000067 System.Void MazeManager::Update()
extern void MazeManager_Update_m09E9E1FB5592856B0C52C70E99D6F5692BE070F7 (void);
// 0x00000068 System.Void MazeManager::Emitter_OnEmitParticle(Obi.ObiEmitter,System.Int32)
extern void MazeManager_Emitter_OnEmitParticle_m06F11C54DD888E8A601ECBDC44ED5CEA6860019C (void);
// 0x00000069 System.Void MazeManager::Solver_OnCollision(Obi.ObiSolver,Obi.ObiSolver_ObiCollisionEventArgs)
extern void MazeManager_Solver_OnCollision_m0ADC8D8AEB3F206A1ECF603EF4FAA786729298FA (void);
// 0x0000006A System.Void MazeManager::LateUpdate()
extern void MazeManager_LateUpdate_mCEABA2653181FA101A6771F3B049977C97B3DEF6 (void);
// 0x0000006B System.Void MazeManager::UpdateScore(System.Int32,System.Int32)
extern void MazeManager_UpdateScore_mFE1E20D6BCE9B1D1075E4C7BF46549C8149DAAC1 (void);
// 0x0000006C System.Void MazeManager::.ctor()
extern void MazeManager__ctor_m1A6CB9C2134EC6A163876D4EEF3A47B63EB45D3D (void);
// 0x0000006D System.Void MazeManager_ScoreChangedEvent::.ctor()
extern void ScoreChangedEvent__ctor_m84BC5912F40C30B13177AE4E3C39C0086C45EBE3 (void);
// 0x0000006E System.Void MazeScoreFormatter::.ctor()
extern void MazeScoreFormatter__ctor_m7F79F8A84E67DA33ECEB611C05B304CD2E85912F (void);
// 0x0000006F System.Void Melt::Awake()
extern void Melt_Awake_mFDD6A55B57DE4B672FDF4B5499837ACB69FB5398 (void);
// 0x00000070 System.Void Melt::OnEnable()
extern void Melt_OnEnable_mCB1F4B0A6DC0D86472F3E5D2E8260423EFEBD01F (void);
// 0x00000071 System.Void Melt::OnDisable()
extern void Melt_OnDisable_mF75168AFEB7C04AC56284D600C18384E5AE0ABA3 (void);
// 0x00000072 System.Void Melt::Solver_OnCollision(System.Object,Obi.ObiSolver_ObiCollisionEventArgs)
extern void Melt_Solver_OnCollision_m724C80341332D561A396BEB5144AA9901B2AA087 (void);
// 0x00000073 System.Void Melt::.ctor()
extern void Melt__ctor_mEDC99D3BDD9AE9107F31433FDD7D476A69601B71 (void);
// 0x00000074 System.Void OverlapTest::Start()
extern void OverlapTest_Start_m15E93BE72D8AF07CE5A0E032952077CFB15D58EA (void);
// 0x00000075 System.Void OverlapTest::OnDestroy()
extern void OverlapTest_OnDestroy_m9C12565A9A469694C074108060DEF2E3CFF5EB15 (void);
// 0x00000076 System.Void OverlapTest::Update()
extern void OverlapTest_Update_mD6A39B9F4B3F0C2ECABC5E8FF7C2B7C08CDA104A (void);
// 0x00000077 System.Void OverlapTest::.ctor()
extern void OverlapTest__ctor_m72D449A3BCB9A7477DD4AE509299712D4D1C7332 (void);
// 0x00000078 System.Void SolidifyOnContact::Awake()
extern void SolidifyOnContact_Awake_m04E647E38D75D40434778B46FCCF1943157BC051 (void);
// 0x00000079 System.Void SolidifyOnContact::OnEnable()
extern void SolidifyOnContact_OnEnable_m6722DBB38E090A2CF64AFFBE085F6BE6DBA9596A (void);
// 0x0000007A System.Void SolidifyOnContact::OnDisable()
extern void SolidifyOnContact_OnDisable_mC566024AF07BA1E885926C86652062C720CB549A (void);
// 0x0000007B System.Void SolidifyOnContact::Solver_OnCollision(System.Object,Obi.ObiSolver_ObiCollisionEventArgs)
extern void SolidifyOnContact_Solver_OnCollision_mC76B5284427900A4AE193B6E2550AE29BDF59C8C (void);
// 0x0000007C System.Void SolidifyOnContact::Solver_OnParticleCollision(System.Object,Obi.ObiSolver_ObiCollisionEventArgs)
extern void SolidifyOnContact_Solver_OnParticleCollision_m526847FE99DE60C560649680BE7A186F167519BE (void);
// 0x0000007D System.Void SolidifyOnContact::Solver_OnBeginStep(Obi.ObiSolver,System.Single)
extern void SolidifyOnContact_Solver_OnBeginStep_m911806948B7A39B06E11B612E686E1A3748E4BD7 (void);
// 0x0000007E System.Void SolidifyOnContact::Solidify(System.Int32,SolidifyOnContact_SolidData)
extern void SolidifyOnContact_Solidify_m3B39017C56D3AB8088D8C442218E1DAC70D44C71 (void);
// 0x0000007F System.Void SolidifyOnContact::.ctor()
extern void SolidifyOnContact__ctor_mE55DB8596DD9D736E039F5796E89021D3C7BAB55 (void);
// 0x00000080 System.Void SolidifyOnContact_SolidData::.ctor(UnityEngine.Transform)
extern void SolidData__ctor_m30AF1DF84C23E743F7616A684C16F3EEECF568FC_AdjustorThunk (void);
// 0x00000081 System.Void WaveGenerator::Start()
extern void WaveGenerator_Start_mD28FC8776B74581D0304A114D8DA84982226B950 (void);
// 0x00000082 System.Void WaveGenerator::Update()
extern void WaveGenerator_Update_m5E104EDCE18E29AA31613031EBC99C5367BB42A1 (void);
// 0x00000083 System.Void WaveGenerator::.ctor()
extern void WaveGenerator__ctor_mCCBADDD48AEFF991A51C0A20312668033AC13F29 (void);
// 0x00000084 System.Void Obi.ObiCharacter::Start()
extern void ObiCharacter_Start_m9AF9959BA6BB669EC44CC6A19F2C4921A8D3EC12 (void);
// 0x00000085 System.Void Obi.ObiCharacter::Move(UnityEngine.Vector3,System.Boolean,System.Boolean)
extern void ObiCharacter_Move_mE36529E342431BC2E3F82ED9221503AF7F957934 (void);
// 0x00000086 System.Void Obi.ObiCharacter::ScaleCapsuleForCrouching(System.Boolean)
extern void ObiCharacter_ScaleCapsuleForCrouching_mBEE0D314BF3D424DEC1B2D4249AC6F1F1B577A51 (void);
// 0x00000087 System.Void Obi.ObiCharacter::PreventStandingInLowHeadroom()
extern void ObiCharacter_PreventStandingInLowHeadroom_mE8D5660997F99D5B260E7CCC9BF758FFF4FDFEDD (void);
// 0x00000088 System.Void Obi.ObiCharacter::UpdateAnimator(UnityEngine.Vector3)
extern void ObiCharacter_UpdateAnimator_m0133BC89517D199811A84D3308D99A39508A6163 (void);
// 0x00000089 System.Void Obi.ObiCharacter::HandleAirborneMovement()
extern void ObiCharacter_HandleAirborneMovement_m2237FCB54D63F3C66E8DC444AE9485868A07E604 (void);
// 0x0000008A System.Void Obi.ObiCharacter::HandleGroundedMovement(System.Boolean,System.Boolean)
extern void ObiCharacter_HandleGroundedMovement_m665A36CA6A0EAD5D0C62AA11737F2A6308EEEDC6 (void);
// 0x0000008B System.Void Obi.ObiCharacter::ApplyExtraTurnRotation()
extern void ObiCharacter_ApplyExtraTurnRotation_m0D9CC7D6882FDB7888330B062F69EE0FA0667BF5 (void);
// 0x0000008C System.Void Obi.ObiCharacter::OnAnimatorMove()
extern void ObiCharacter_OnAnimatorMove_mE0C7A78FB55C063DF34E2BC3FF6F84125B63AB6A (void);
// 0x0000008D System.Void Obi.ObiCharacter::CheckGroundStatus()
extern void ObiCharacter_CheckGroundStatus_m21B58AEB9AB71B4D7885687B1C5979A9CB9CF45E (void);
// 0x0000008E System.Void Obi.ObiCharacter::.ctor()
extern void ObiCharacter__ctor_m16255C98358F19058CDB9991C96F04FE19EA7A93 (void);
// 0x0000008F System.Void Obi.SampleCharacterController::Start()
extern void SampleCharacterController_Start_mE5EAF1421DBAD9FFAC2D550E04332581CDFFE4BC (void);
// 0x00000090 System.Void Obi.SampleCharacterController::FixedUpdate()
extern void SampleCharacterController_FixedUpdate_m1AC87DDCCC039856B18C977E02E0CD2D80F1A524 (void);
// 0x00000091 System.Void Obi.SampleCharacterController::.ctor()
extern void SampleCharacterController__ctor_m8605568614106AD773DE856FE8AC1968C23A4262 (void);
// 0x00000092 System.Void Obi.ColorFromPhase::Awake()
extern void ColorFromPhase_Awake_m8EE051CBD4CAF6406768C2D6941DCABCD6CFD2B9 (void);
// 0x00000093 System.Void Obi.ColorFromPhase::LateUpdate()
extern void ColorFromPhase_LateUpdate_m56F07E6A796032EB6DD06CE10C57090983DAA1AB (void);
// 0x00000094 System.Void Obi.ColorFromPhase::.ctor()
extern void ColorFromPhase__ctor_mECC2D22AC69AC7C21BC64BF585AB14C65E7A2415 (void);
// 0x00000095 System.Void Obi.ColorFromVelocity::Awake()
extern void ColorFromVelocity_Awake_mBA50845A66FAE55EB4F4637C1BF190EE329FAEB5 (void);
// 0x00000096 System.Void Obi.ColorFromVelocity::OnEnable()
extern void ColorFromVelocity_OnEnable_m60CE9B091B8725925F60E867BE08F1F137E2AB9D (void);
// 0x00000097 System.Void Obi.ColorFromVelocity::LateUpdate()
extern void ColorFromVelocity_LateUpdate_m4A026CE0BCACBCA70F219D29A3EEFA7EDE33C2A3 (void);
// 0x00000098 System.Void Obi.ColorFromVelocity::.ctor()
extern void ColorFromVelocity__ctor_mD26A1B2F148FC8BF6F57EDEA8179E0B2D8CD8FFC (void);
// 0x00000099 System.Void Obi.ColorFromViscosity::Awake()
extern void ColorFromViscosity_Awake_m2E3E3C1A9B61E1938F4962882728E94CF0E0648F (void);
// 0x0000009A System.Void Obi.ColorFromViscosity::LateUpdate()
extern void ColorFromViscosity_LateUpdate_m45B832BA7E0E701E0DD987925E0AE9E9A5DBEB21 (void);
// 0x0000009B System.Void Obi.ColorFromViscosity::.ctor()
extern void ColorFromViscosity__ctor_m60EAD23EC722A04DDA00EDCFCDCD4454828D52C0 (void);
// 0x0000009C System.Void Obi.ColorRandomizer::Start()
extern void ColorRandomizer_Start_mF174C12B77903483554BBCE5D4A4D82118DD4651 (void);
// 0x0000009D System.Void Obi.ColorRandomizer::.ctor()
extern void ColorRandomizer__ctor_mF1141F2D29657D595C863D186B46C6B3CD672A85 (void);
// 0x0000009E System.Void Obi.LookAroundCamera::Awake()
extern void LookAroundCamera_Awake_mE90B4191F1288756CA34EEC0C21C1998251B0E8E (void);
// 0x0000009F System.Void Obi.LookAroundCamera::LookAt(UnityEngine.Vector3,UnityEngine.Vector3)
extern void LookAroundCamera_LookAt_mEF29B91315C1AD0156E505C73EF9931CDD6E7718 (void);
// 0x000000A0 System.Void Obi.LookAroundCamera::UpdateShot()
extern void LookAroundCamera_UpdateShot_m8C764716D01D701607A13430043C9E5B87A50597 (void);
// 0x000000A1 System.Void Obi.LookAroundCamera::LateUpdate()
extern void LookAroundCamera_LateUpdate_m18FD061FDD268DE063DEC79BAD72F08432F1A7A8 (void);
// 0x000000A2 System.Void Obi.LookAroundCamera::.ctor()
extern void LookAroundCamera__ctor_m52D5B9733D32E65A768D29868D4B07EB63988D12 (void);
// 0x000000A3 System.Void Obi.LookAroundCamera_CameraShot::.ctor(UnityEngine.Vector3,UnityEngine.Quaternion,UnityEngine.Vector3,System.Single)
extern void CameraShot__ctor_mD9736DEA6A3F14DF47920666D00E1ABA5899E179_AdjustorThunk (void);
// 0x000000A4 System.Void Obi.MoveAndRotate::Start()
extern void MoveAndRotate_Start_m81492AAA391819DF463B1781F3F647B8F50E8E1D (void);
// 0x000000A5 System.Void Obi.MoveAndRotate::FixedUpdate()
extern void MoveAndRotate_FixedUpdate_mC9020498CC29B200215037EB0EA7508367CCD8BD (void);
// 0x000000A6 System.Void Obi.MoveAndRotate::.ctor()
extern void MoveAndRotate__ctor_mC46EA5D888532267ADC051A68F468CD7FD5D2130 (void);
// 0x000000A7 System.Void Obi.MoveAndRotate_Vector3andSpace::.ctor()
extern void Vector3andSpace__ctor_m5653715EE0EAB2271B055D2729CCF8046D2CE78B (void);
// 0x000000A8 System.Void Obi.ObiFluidPropertyColorizer::Awake()
extern void ObiFluidPropertyColorizer_Awake_m84BF1C1151295238AAE21C63E854C547BC6A3F07 (void);
// 0x000000A9 System.Void Obi.ObiFluidPropertyColorizer::OnEnable()
extern void ObiFluidPropertyColorizer_OnEnable_m097799C7DB76BB239571172F65AA5940954C1783 (void);
// 0x000000AA System.Void Obi.ObiFluidPropertyColorizer::LateUpdate()
extern void ObiFluidPropertyColorizer_LateUpdate_mF8D65010BBD4C17BF1D45644090839C1CB12AC4A (void);
// 0x000000AB System.Void Obi.ObiFluidPropertyColorizer::.ctor()
extern void ObiFluidPropertyColorizer__ctor_mA9D6BD22FB733D0AF6FB5650F3219A89E4DC4E7B (void);
// 0x000000AC System.Void Obi.ViscositySurfTensionToUserData::Awake()
extern void ViscositySurfTensionToUserData_Awake_m1FE92886BD05CD7178D08BB3FED6A6203F878DE6 (void);
// 0x000000AD System.Void Obi.ViscositySurfTensionToUserData::Emitter_OnEmitParticle(Obi.ObiEmitter,System.Int32)
extern void ViscositySurfTensionToUserData_Emitter_OnEmitParticle_m6BAA37F749FC12DD292005FCA13C35B630BE6F9A (void);
// 0x000000AE System.Void Obi.ViscositySurfTensionToUserData::.ctor()
extern void ViscositySurfTensionToUserData__ctor_m34A0B9DE9EBE374AA00CECC7D368B05E61EEC932 (void);
static Il2CppMethodPointer s_methodPointers[174] = 
{
	AudioManager_Awake_mD7873A38A3ED577A313A05D24BF6511E59EDEC01,
	AudioManager_PlaySuccess_m889550BD8021F9E8D34CF8EF258D939304F41592,
	AudioManager_PlayFail_m2EBA3AB666F25E529F92838FA8AD8DFC237E283E,
	AudioManager_PlayPouring_m89F9FF74C7EEAAA4DC3F3571E3A17DDCF12095F6,
	AudioManager_PlayPutDown_mF66C8A85DB3F79B4C8CAB1C15B07A363906C7AB5,
	AudioManager_PlayReset_mB174F3D616241B53848F3152C5C1A61801A42A3D,
	AudioManager_StopEffects_m0FB721DBE099F916C2AA75B9DE1A47EAD9D6598F,
	AudioManager__ctor_m6C686441D1A1A223E4CF940A8EB0128535D603BD,
	GameManager_Awake_m22F42B2A82708B10F652CAD8F2E0A4767110FF30,
	GameManager_Start_m26461AEF27E44DB8FECCBC19D6C9E228B658BF8E,
	GameManager_Update_mC9303BA7C3117BD861F49F8E36151CC52117E6C1,
	GameManager_OnFinish_m795F03CAEB00F8B8486BA4ADA99A705C1C672D20,
	GameManager_Cork_mD9B04D863523B536A5B3660DFB5374D64387AB61,
	GameManager_Onwin_m51D63DA951D328D51482F5786657863B6A45A8B0,
	GameManager_OnLoose_m6EB04A2B6EEE20375AB4764571C72E10CF3BB605,
	GameManager_WaitAndReset_m27D83AD57897C467040734244B7C73CD7DAC48EC,
	GameManager_Reset_mA71D1D96D0E38DEC1C7E61CE76675E314FE252C2,
	GameManager__ctor_mE8666F6D0CA9C31E16B719F79780DC4B0245B64D,
	U3CCorkU3Ed__20__ctor_m1B3AEA4ECAAF4A5DD71410ED9F45A805C876DD6C,
	U3CCorkU3Ed__20_System_IDisposable_Dispose_mA1C04912A692F37B43576DAAC5238DCE6D38A972,
	U3CCorkU3Ed__20_MoveNext_m7D50079DFAE8DD7C70F8868752A6B72048F564C2,
	U3CCorkU3Ed__20_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m0E98DCD2422A1C444C8783B78DAEF8F26FDD7F37,
	U3CCorkU3Ed__20_System_Collections_IEnumerator_Reset_m4B46102788FE393F65B8A0ED00E0617AE2DC38C5,
	U3CCorkU3Ed__20_System_Collections_IEnumerator_get_Current_mF144219FA16314C6311E6D16F64CCF2E40C74869,
	U3CWaitAndResetU3Ed__23__ctor_m29886341D87FA57883BA5D66A9441D560B58F68C,
	U3CWaitAndResetU3Ed__23_System_IDisposable_Dispose_m9DB94CFF080D4848C5A141756B5BB2BBE1842CD9,
	U3CWaitAndResetU3Ed__23_MoveNext_mB8EC03324C16D7C36C39A98CA75B3C4A9FB778ED,
	U3CWaitAndResetU3Ed__23_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m1DDA78D95C32E45F45A58B8B1868AAAF556209BE,
	U3CWaitAndResetU3Ed__23_System_Collections_IEnumerator_Reset_m7B1AE96CF48EDAF04FDAF37AA422A0BEC3025D44,
	U3CWaitAndResetU3Ed__23_System_Collections_IEnumerator_get_Current_mC4FC314B8341CCBC51EA81CD9F6FB1B383DFDC83,
	InputHandler_Awake_m312AFC247EF04CB92C06AD4F35AD02F621C62388,
	InputHandler_Update_m22B46EB5A6FA4F2C8B5B8790BE389EF5BC8BCACE,
	InputHandler_StartPouring_mC4CDFA2AFE573EC3D87D37DC2A8A72A6BC34D811,
	InputHandler_IsPointerOverGameObject_m39ECD4281D05F816512779423A6F8D97E215D874,
	InputHandler__ctor_mA408269F104C8A8C1931F10877DD4206AB201C90,
	TestingsManager_Update_mF85D98D4DC5DF45D491B077136E3D450098E4CCF,
	TestingsManager__ctor_m5239FF3C69C48D92C66A2611CA990C697A237239,
	ActorCOMTransform_Update_m42CC68376DF28C96387BACFC64C48515AE9CE469,
	ActorCOMTransform__ctor_mB7B969737BB7416B0E66C1CD4B8FDF1E8A7186CD,
	ActorSpawner_Update_mBB452D66B51B50FE636D74E7FF957A10A1FE2A07,
	ActorSpawner__ctor_m48C1E536BA52A4F4EDE74F5B90749B4619F13C86,
	AddRandomVelocity_Update_m6584A3F85D2E61F6D420B0B911B3C61637008449,
	AddRandomVelocity__ctor_mC1CFE0D59EF94BA30BA6AAB81CA6FF0899F84DDF,
	Blinker_Awake_mC92D3CBC36B46F324C8197377BC40A434109C876,
	Blinker_Blink_m4082F3A19006F464F4C14105BED460D5A1A0E273,
	Blinker_LateUpdate_mEE40A5EDA3EAFBB9CAD39BC413CB921E15CF6CCB,
	Blinker__ctor_mAE1E307D31F5C3BB83001FBC5B2D61F827A69F3C,
	ColliderHighlighter_Awake_m5920B8F626704DB40821E9BB9A85FDA1E0BFE4B3,
	ColliderHighlighter_OnEnable_m6F1425D169D88CC4D90E0A8F144A05C55F7E03B6,
	ColliderHighlighter_OnDisable_m2780DDE2CFE790FF4B04AD45654BDB74DD752893,
	ColliderHighlighter_Solver_OnCollision_m8A90B739BAB8461A0179C2B4CD4EE87AB3DE8D30,
	ColliderHighlighter__ctor_mBE8B58BC252B389A907738D87DAD06F094A62587,
	CollisionEventHandler_Awake_m744FDE1AB7D3F6777399B5108896E5E2CC61FE93,
	CollisionEventHandler_OnEnable_m2D3F33422D785CEDFCDCF1012A0B37B5F3ACB9F6,
	CollisionEventHandler_OnDisable_m75B03CB65B3D0226C04FF77A99D488596144BC32,
	CollisionEventHandler_Solver_OnCollision_m484E685CBD1E502504C567FD2D5CCB6F0860D616,
	CollisionEventHandler_OnDrawGizmos_m7939D47B098838344A41DD08F547354E268E07A8,
	CollisionEventHandler__ctor_m92805BEBFF6E7114772EF8DFF4D5C244467A479B,
	DebugParticleFrames_Awake_mA5C0197560CB8F73D7472974A693020EA29B28B3,
	DebugParticleFrames_OnDrawGizmos_m51378254E7930D9050D3355F810A3BC15A4CBBED,
	DebugParticleFrames__ctor_mBE2F687FB03E680B1957BB4D2C9F19E43F3CF260,
	ExtrapolationCamera_Start_mCB1FD551CBFB047EEA0FA534CE64CA969640D51E,
	ExtrapolationCamera_FixedUpdate_m622D9ECE2C7F91E1C5A70068B94B7C3E03AF4523,
	ExtrapolationCamera_LateUpdate_mB90259A761A12F0D8301E75BC381A6C0B32B443D,
	ExtrapolationCamera_Teleport_m8A6D37587FD1DF2518851886F5BE70B897DE0B67,
	ExtrapolationCamera__ctor_mA4E8D57C9E25BC7F9DA5676D79A7FC5505073032,
	FPSDisplay_get_CurrentFPS_mA9019EF8EAA0384D37F2DE714D84E5413E783C72,
	FPSDisplay_get_FPSMedian_m6ACF1BBC790E915C8CE3D94C797CE4A4E8F116DD,
	FPSDisplay_get_FPSAverage_m52F2116EA4DD6DA75E483FE24D21321CF9F2D4C1,
	FPSDisplay_Start_mF1CE43CCE2AAB57ECD8D8B623B7E7358AB163C55,
	FPSDisplay_Update_m8740416265A05D0C56CA05EC0DE3A12E5132B028,
	FPSDisplay_ResetMedianAndAverage_m82840B2C8D0189EA211AE4B14A8C94B22D932F70,
	FPSDisplay__ctor_mC4AD184EF09FAF5C43D89D0F7C211B2754D95B18,
	ObiActorTeleport_Teleport_m83F48614A9D7AE20261E92546E28B7C65C26DA1D,
	ObiActorTeleport__ctor_mCC83730596EA6276332F31F582F5C8D1F08EE019,
	ObiParticleCounter_Awake_m42D6C0254A2B27439769A96FE50705D33EC62B98,
	ObiParticleCounter_OnEnable_mA9798F6382D0620125A95B047C974EBA660926CB,
	ObiParticleCounter_OnDisable_m3224433810D75B192BD03D399ADE656864BBD594,
	ObiParticleCounter_Solver_OnCollision_mC585B439D90C23BA5FE74E65D3195651DCAEF392,
	ObiParticleCounter__ctor_m3107B0DABE8BEA1C8EB84C5A8EEEDB3C3E4B1B52,
	ObjectDragger_OnMouseDown_mC96AA07AABD47631E097AEA08E9A81AE1506EEEE,
	ObjectDragger_OnMouseDrag_m7E0B861E5546C709DED1ACF6F33ED31D6004FA6E,
	ObjectDragger__ctor_m97083AF9B77C7E2554A3D0701C6FAEE5D2282A6C,
	ObjectLimit_Update_m6689D4679CA59887B038EE4BD3F2294753C57E38,
	ObjectLimit__ctor_m8F9F789DA4B949FECADA677D21A26E893A0A5F41,
	SlowmoToggler_Slowmo_mF683360609D33F0B08F26A1019AAAD2BA21C8404,
	SlowmoToggler__ctor_m17E4BB8E004DE90384ADF6074E74BC7C383E7001,
	WorldSpaceGravity_Awake_m20A0105B37EEF41CE69A528E59C13099276E4F2B,
	WorldSpaceGravity_Update_m9BFD6836B3B4FD9F671F9CC77622279143C1F72C,
	WorldSpaceGravity__ctor_m8A54BF21A4069DBE4B27EA67FDABDC1C26766539,
	BucketController_Update_m008F7F716352BFFA46E104AB534AFCA974BF6789,
	BucketController__ctor_mFED593F8CAEB864494D8730A259B40C16F46CA37,
	EmitOnKeyPress_Start_m984AC303BA37CD0140668F919F0D5EBA56B9B176,
	EmitOnKeyPress_Update_mEE6EFC2796CFE7CDD30FC46137513C172BA055CF,
	EmitOnKeyPress__ctor_m168554363F3AB623ADB803B992E5BB2211C0A8FD,
	FluidColorizer_Awake_m9F11E1D1AEFB40F7B35B112B17F0342F23E779A6,
	FluidColorizer__ctor_mABDE4E9B160DDE0CA4593C3FF87C669306098903,
	FluidJetController_Start_mFCC15C1D73ED40A6531C9AADEBF1F3488F589B8A,
	FluidJetController_Update_m5B7F25050B2CEFFCEC416C7CCCAB687960829BF5,
	FluidJetController__ctor_mAAF3034F58E75E0163933D2AF31B79C98D67D69A,
	MazeManager_Start_m276D6F781914CDF48C22AB61C8B8EB48B77EE3FE,
	MazeManager_OnDestroy_mFB114E6F773097ECC5FAC553F342E3E9B7792130,
	MazeManager_Update_m09E9E1FB5592856B0C52C70E99D6F5692BE070F7,
	MazeManager_Emitter_OnEmitParticle_m06F11C54DD888E8A601ECBDC44ED5CEA6860019C,
	MazeManager_Solver_OnCollision_m0ADC8D8AEB3F206A1ECF603EF4FAA786729298FA,
	MazeManager_LateUpdate_mCEABA2653181FA101A6771F3B049977C97B3DEF6,
	MazeManager_UpdateScore_mFE1E20D6BCE9B1D1075E4C7BF46549C8149DAAC1,
	MazeManager__ctor_m1A6CB9C2134EC6A163876D4EEF3A47B63EB45D3D,
	ScoreChangedEvent__ctor_m84BC5912F40C30B13177AE4E3C39C0086C45EBE3,
	MazeScoreFormatter__ctor_m7F79F8A84E67DA33ECEB611C05B304CD2E85912F,
	Melt_Awake_mFDD6A55B57DE4B672FDF4B5499837ACB69FB5398,
	Melt_OnEnable_mCB1F4B0A6DC0D86472F3E5D2E8260423EFEBD01F,
	Melt_OnDisable_mF75168AFEB7C04AC56284D600C18384E5AE0ABA3,
	Melt_Solver_OnCollision_m724C80341332D561A396BEB5144AA9901B2AA087,
	Melt__ctor_mEDC99D3BDD9AE9107F31433FDD7D476A69601B71,
	OverlapTest_Start_m15E93BE72D8AF07CE5A0E032952077CFB15D58EA,
	OverlapTest_OnDestroy_m9C12565A9A469694C074108060DEF2E3CFF5EB15,
	OverlapTest_Update_mD6A39B9F4B3F0C2ECABC5E8FF7C2B7C08CDA104A,
	OverlapTest__ctor_m72D449A3BCB9A7477DD4AE509299712D4D1C7332,
	SolidifyOnContact_Awake_m04E647E38D75D40434778B46FCCF1943157BC051,
	SolidifyOnContact_OnEnable_m6722DBB38E090A2CF64AFFBE085F6BE6DBA9596A,
	SolidifyOnContact_OnDisable_mC566024AF07BA1E885926C86652062C720CB549A,
	SolidifyOnContact_Solver_OnCollision_mC76B5284427900A4AE193B6E2550AE29BDF59C8C,
	SolidifyOnContact_Solver_OnParticleCollision_m526847FE99DE60C560649680BE7A186F167519BE,
	SolidifyOnContact_Solver_OnBeginStep_m911806948B7A39B06E11B612E686E1A3748E4BD7,
	SolidifyOnContact_Solidify_m3B39017C56D3AB8088D8C442218E1DAC70D44C71,
	SolidifyOnContact__ctor_mE55DB8596DD9D736E039F5796E89021D3C7BAB55,
	SolidData__ctor_m30AF1DF84C23E743F7616A684C16F3EEECF568FC_AdjustorThunk,
	WaveGenerator_Start_mD28FC8776B74581D0304A114D8DA84982226B950,
	WaveGenerator_Update_m5E104EDCE18E29AA31613031EBC99C5367BB42A1,
	WaveGenerator__ctor_mCCBADDD48AEFF991A51C0A20312668033AC13F29,
	ObiCharacter_Start_m9AF9959BA6BB669EC44CC6A19F2C4921A8D3EC12,
	ObiCharacter_Move_mE36529E342431BC2E3F82ED9221503AF7F957934,
	ObiCharacter_ScaleCapsuleForCrouching_mBEE0D314BF3D424DEC1B2D4249AC6F1F1B577A51,
	ObiCharacter_PreventStandingInLowHeadroom_mE8D5660997F99D5B260E7CCC9BF758FFF4FDFEDD,
	ObiCharacter_UpdateAnimator_m0133BC89517D199811A84D3308D99A39508A6163,
	ObiCharacter_HandleAirborneMovement_m2237FCB54D63F3C66E8DC444AE9485868A07E604,
	ObiCharacter_HandleGroundedMovement_m665A36CA6A0EAD5D0C62AA11737F2A6308EEEDC6,
	ObiCharacter_ApplyExtraTurnRotation_m0D9CC7D6882FDB7888330B062F69EE0FA0667BF5,
	ObiCharacter_OnAnimatorMove_mE0C7A78FB55C063DF34E2BC3FF6F84125B63AB6A,
	ObiCharacter_CheckGroundStatus_m21B58AEB9AB71B4D7885687B1C5979A9CB9CF45E,
	ObiCharacter__ctor_m16255C98358F19058CDB9991C96F04FE19EA7A93,
	SampleCharacterController_Start_mE5EAF1421DBAD9FFAC2D550E04332581CDFFE4BC,
	SampleCharacterController_FixedUpdate_m1AC87DDCCC039856B18C977E02E0CD2D80F1A524,
	SampleCharacterController__ctor_m8605568614106AD773DE856FE8AC1968C23A4262,
	ColorFromPhase_Awake_m8EE051CBD4CAF6406768C2D6941DCABCD6CFD2B9,
	ColorFromPhase_LateUpdate_m56F07E6A796032EB6DD06CE10C57090983DAA1AB,
	ColorFromPhase__ctor_mECC2D22AC69AC7C21BC64BF585AB14C65E7A2415,
	ColorFromVelocity_Awake_mBA50845A66FAE55EB4F4637C1BF190EE329FAEB5,
	ColorFromVelocity_OnEnable_m60CE9B091B8725925F60E867BE08F1F137E2AB9D,
	ColorFromVelocity_LateUpdate_m4A026CE0BCACBCA70F219D29A3EEFA7EDE33C2A3,
	ColorFromVelocity__ctor_mD26A1B2F148FC8BF6F57EDEA8179E0B2D8CD8FFC,
	ColorFromViscosity_Awake_m2E3E3C1A9B61E1938F4962882728E94CF0E0648F,
	ColorFromViscosity_LateUpdate_m45B832BA7E0E701E0DD987925E0AE9E9A5DBEB21,
	ColorFromViscosity__ctor_m60EAD23EC722A04DDA00EDCFCDCD4454828D52C0,
	ColorRandomizer_Start_mF174C12B77903483554BBCE5D4A4D82118DD4651,
	ColorRandomizer__ctor_mF1141F2D29657D595C863D186B46C6B3CD672A85,
	LookAroundCamera_Awake_mE90B4191F1288756CA34EEC0C21C1998251B0E8E,
	LookAroundCamera_LookAt_mEF29B91315C1AD0156E505C73EF9931CDD6E7718,
	LookAroundCamera_UpdateShot_m8C764716D01D701607A13430043C9E5B87A50597,
	LookAroundCamera_LateUpdate_m18FD061FDD268DE063DEC79BAD72F08432F1A7A8,
	LookAroundCamera__ctor_m52D5B9733D32E65A768D29868D4B07EB63988D12,
	CameraShot__ctor_mD9736DEA6A3F14DF47920666D00E1ABA5899E179_AdjustorThunk,
	MoveAndRotate_Start_m81492AAA391819DF463B1781F3F647B8F50E8E1D,
	MoveAndRotate_FixedUpdate_mC9020498CC29B200215037EB0EA7508367CCD8BD,
	MoveAndRotate__ctor_mC46EA5D888532267ADC051A68F468CD7FD5D2130,
	Vector3andSpace__ctor_m5653715EE0EAB2271B055D2729CCF8046D2CE78B,
	ObiFluidPropertyColorizer_Awake_m84BF1C1151295238AAE21C63E854C547BC6A3F07,
	ObiFluidPropertyColorizer_OnEnable_m097799C7DB76BB239571172F65AA5940954C1783,
	ObiFluidPropertyColorizer_LateUpdate_mF8D65010BBD4C17BF1D45644090839C1CB12AC4A,
	ObiFluidPropertyColorizer__ctor_mA9D6BD22FB733D0AF6FB5650F3219A89E4DC4E7B,
	ViscositySurfTensionToUserData_Awake_m1FE92886BD05CD7178D08BB3FED6A6203F878DE6,
	ViscositySurfTensionToUserData_Emitter_OnEmitParticle_m6BAA37F749FC12DD292005FCA13C35B630BE6F9A,
	ViscositySurfTensionToUserData__ctor_m34A0B9DE9EBE374AA00CECC7D368B05E61EEC932,
};
static const int32_t s_InvokerIndices[174] = 
{
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	14,
	23,
	23,
	14,
	23,
	23,
	32,
	23,
	102,
	14,
	23,
	14,
	32,
	23,
	102,
	14,
	23,
	14,
	23,
	23,
	23,
	49,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	27,
	23,
	23,
	23,
	23,
	27,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	1452,
	23,
	650,
	650,
	650,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	27,
	23,
	23,
	23,
	23,
	23,
	23,
	31,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	124,
	27,
	23,
	157,
	23,
	23,
	23,
	23,
	23,
	23,
	27,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	27,
	27,
	824,
	1660,
	23,
	26,
	23,
	23,
	23,
	23,
	1661,
	31,
	23,
	980,
	23,
	42,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	977,
	23,
	23,
	23,
	1662,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	124,
	23,
};
extern const Il2CppCodeGenModule g_AssemblyU2DCSharpCodeGenModule;
const Il2CppCodeGenModule g_AssemblyU2DCSharpCodeGenModule = 
{
	"Assembly-CSharp.dll",
	174,
	s_methodPointers,
	s_InvokerIndices,
	0,
	NULL,
	0,
	NULL,
	0,
	NULL,
	NULL,
};

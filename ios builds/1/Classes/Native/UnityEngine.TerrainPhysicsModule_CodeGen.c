﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif



#include "codegen/il2cpp-codegen-metadata.h"





IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END




// 0x00000001 UnityEngine.TerrainData UnityEngine.TerrainCollider::get_terrainData()
extern void TerrainCollider_get_terrainData_m2A58531B47F4E61286A3E4B787869BFFADA56D37 (void);
static Il2CppMethodPointer s_methodPointers[1] = 
{
	TerrainCollider_get_terrainData_m2A58531B47F4E61286A3E4B787869BFFADA56D37,
};
static const int32_t s_InvokerIndices[1] = 
{
	14,
};
extern const Il2CppCodeGenModule g_UnityEngine_TerrainPhysicsModuleCodeGenModule;
const Il2CppCodeGenModule g_UnityEngine_TerrainPhysicsModuleCodeGenModule = 
{
	"UnityEngine.TerrainPhysicsModule.dll",
	1,
	s_methodPointers,
	s_InvokerIndices,
	0,
	NULL,
	0,
	NULL,
	0,
	NULL,
	NULL,
};

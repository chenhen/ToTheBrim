﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif



#include "codegen/il2cpp-codegen-metadata.h"





IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END




// 0x00000001 System.Exception System.Linq.Error::ArgumentNull(System.String)
extern void Error_ArgumentNull_m0EDA0D46D72CA692518E3E2EB75B48044D8FD41E (void);
// 0x00000002 System.Exception System.Linq.Error::MoreThanOneMatch()
extern void Error_MoreThanOneMatch_m4C4756AF34A76EF12F3B2B6D8C78DE547F0FBCF8 (void);
// 0x00000003 System.Exception System.Linq.Error::NoElements()
extern void Error_NoElements_mB89E91246572F009281D79730950808F17C3F353 (void);
// 0x00000004 System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable::Where(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x00000005 System.Func`2<TSource,System.Boolean> System.Linq.Enumerable::CombinePredicates(System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,System.Boolean>)
// 0x00000006 TSource[] System.Linq.Enumerable::ToArray(System.Collections.Generic.IEnumerable`1<TSource>)
// 0x00000007 TSource System.Linq.Enumerable::First(System.Collections.Generic.IEnumerable`1<TSource>)
// 0x00000008 TSource System.Linq.Enumerable::SingleOrDefault(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x00000009 System.Boolean System.Linq.Enumerable::Any(System.Collections.Generic.IEnumerable`1<TSource>)
// 0x0000000A System.Boolean System.Linq.Enumerable::Any(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x0000000B System.Int32 System.Linq.Enumerable::Count(System.Collections.Generic.IEnumerable`1<TSource>)
// 0x0000000C System.Int32 System.Linq.Enumerable::Count(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x0000000D System.Void System.Linq.Enumerable_Iterator`1::.ctor()
// 0x0000000E TSource System.Linq.Enumerable_Iterator`1::get_Current()
// 0x0000000F System.Linq.Enumerable_Iterator`1<TSource> System.Linq.Enumerable_Iterator`1::Clone()
// 0x00000010 System.Void System.Linq.Enumerable_Iterator`1::Dispose()
// 0x00000011 System.Collections.Generic.IEnumerator`1<TSource> System.Linq.Enumerable_Iterator`1::GetEnumerator()
// 0x00000012 System.Boolean System.Linq.Enumerable_Iterator`1::MoveNext()
// 0x00000013 System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable_Iterator`1::Where(System.Func`2<TSource,System.Boolean>)
// 0x00000014 System.Object System.Linq.Enumerable_Iterator`1::System.Collections.IEnumerator.get_Current()
// 0x00000015 System.Collections.IEnumerator System.Linq.Enumerable_Iterator`1::System.Collections.IEnumerable.GetEnumerator()
// 0x00000016 System.Void System.Linq.Enumerable_Iterator`1::System.Collections.IEnumerator.Reset()
// 0x00000017 System.Void System.Linq.Enumerable_WhereEnumerableIterator`1::.ctor(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x00000018 System.Linq.Enumerable_Iterator`1<TSource> System.Linq.Enumerable_WhereEnumerableIterator`1::Clone()
// 0x00000019 System.Void System.Linq.Enumerable_WhereEnumerableIterator`1::Dispose()
// 0x0000001A System.Boolean System.Linq.Enumerable_WhereEnumerableIterator`1::MoveNext()
// 0x0000001B System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable_WhereEnumerableIterator`1::Where(System.Func`2<TSource,System.Boolean>)
// 0x0000001C System.Void System.Linq.Enumerable_WhereArrayIterator`1::.ctor(TSource[],System.Func`2<TSource,System.Boolean>)
// 0x0000001D System.Linq.Enumerable_Iterator`1<TSource> System.Linq.Enumerable_WhereArrayIterator`1::Clone()
// 0x0000001E System.Boolean System.Linq.Enumerable_WhereArrayIterator`1::MoveNext()
// 0x0000001F System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable_WhereArrayIterator`1::Where(System.Func`2<TSource,System.Boolean>)
// 0x00000020 System.Void System.Linq.Enumerable_WhereListIterator`1::.ctor(System.Collections.Generic.List`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x00000021 System.Linq.Enumerable_Iterator`1<TSource> System.Linq.Enumerable_WhereListIterator`1::Clone()
// 0x00000022 System.Boolean System.Linq.Enumerable_WhereListIterator`1::MoveNext()
// 0x00000023 System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable_WhereListIterator`1::Where(System.Func`2<TSource,System.Boolean>)
// 0x00000024 System.Void System.Linq.Enumerable_<>c__DisplayClass6_0`1::.ctor()
// 0x00000025 System.Boolean System.Linq.Enumerable_<>c__DisplayClass6_0`1::<CombinePredicates>b__0(TSource)
// 0x00000026 System.Void System.Linq.Buffer`1::.ctor(System.Collections.Generic.IEnumerable`1<TElement>)
// 0x00000027 TElement[] System.Linq.Buffer`1::ToArray()
// 0x00000028 System.Void System.Collections.Generic.HashSet`1::.ctor()
// 0x00000029 System.Void System.Collections.Generic.HashSet`1::.ctor(System.Collections.Generic.IEqualityComparer`1<T>)
// 0x0000002A System.Void System.Collections.Generic.HashSet`1::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
// 0x0000002B System.Void System.Collections.Generic.HashSet`1::System.Collections.Generic.ICollection<T>.Add(T)
// 0x0000002C System.Void System.Collections.Generic.HashSet`1::Clear()
// 0x0000002D System.Boolean System.Collections.Generic.HashSet`1::Contains(T)
// 0x0000002E System.Void System.Collections.Generic.HashSet`1::CopyTo(T[],System.Int32)
// 0x0000002F System.Boolean System.Collections.Generic.HashSet`1::Remove(T)
// 0x00000030 System.Int32 System.Collections.Generic.HashSet`1::get_Count()
// 0x00000031 System.Boolean System.Collections.Generic.HashSet`1::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
// 0x00000032 System.Collections.Generic.HashSet`1_Enumerator<T> System.Collections.Generic.HashSet`1::GetEnumerator()
// 0x00000033 System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.HashSet`1::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
// 0x00000034 System.Collections.IEnumerator System.Collections.Generic.HashSet`1::System.Collections.IEnumerable.GetEnumerator()
// 0x00000035 System.Void System.Collections.Generic.HashSet`1::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
// 0x00000036 System.Void System.Collections.Generic.HashSet`1::OnDeserialization(System.Object)
// 0x00000037 System.Boolean System.Collections.Generic.HashSet`1::Add(T)
// 0x00000038 System.Void System.Collections.Generic.HashSet`1::ExceptWith(System.Collections.Generic.IEnumerable`1<T>)
// 0x00000039 System.Void System.Collections.Generic.HashSet`1::CopyTo(T[])
// 0x0000003A System.Void System.Collections.Generic.HashSet`1::CopyTo(T[],System.Int32,System.Int32)
// 0x0000003B System.Void System.Collections.Generic.HashSet`1::Initialize(System.Int32)
// 0x0000003C System.Void System.Collections.Generic.HashSet`1::IncreaseCapacity()
// 0x0000003D System.Void System.Collections.Generic.HashSet`1::SetCapacity(System.Int32)
// 0x0000003E System.Boolean System.Collections.Generic.HashSet`1::AddIfNotPresent(T)
// 0x0000003F System.Int32 System.Collections.Generic.HashSet`1::InternalGetHashCode(T)
// 0x00000040 System.Void System.Collections.Generic.HashSet`1_Enumerator::.ctor(System.Collections.Generic.HashSet`1<T>)
// 0x00000041 System.Void System.Collections.Generic.HashSet`1_Enumerator::Dispose()
// 0x00000042 System.Boolean System.Collections.Generic.HashSet`1_Enumerator::MoveNext()
// 0x00000043 T System.Collections.Generic.HashSet`1_Enumerator::get_Current()
// 0x00000044 System.Object System.Collections.Generic.HashSet`1_Enumerator::System.Collections.IEnumerator.get_Current()
// 0x00000045 System.Void System.Collections.Generic.HashSet`1_Enumerator::System.Collections.IEnumerator.Reset()
static Il2CppMethodPointer s_methodPointers[69] = 
{
	Error_ArgumentNull_m0EDA0D46D72CA692518E3E2EB75B48044D8FD41E,
	Error_MoreThanOneMatch_m4C4756AF34A76EF12F3B2B6D8C78DE547F0FBCF8,
	Error_NoElements_mB89E91246572F009281D79730950808F17C3F353,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
};
static const int32_t s_InvokerIndices[69] = 
{
	0,
	4,
	4,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
};
static const Il2CppTokenRangePair s_rgctxIndices[17] = 
{
	{ 0x02000004, { 34, 4 } },
	{ 0x02000005, { 38, 9 } },
	{ 0x02000006, { 47, 7 } },
	{ 0x02000007, { 54, 10 } },
	{ 0x02000008, { 64, 1 } },
	{ 0x02000009, { 65, 4 } },
	{ 0x0200000A, { 69, 25 } },
	{ 0x0200000C, { 94, 2 } },
	{ 0x06000004, { 0, 10 } },
	{ 0x06000005, { 10, 5 } },
	{ 0x06000006, { 15, 3 } },
	{ 0x06000007, { 18, 4 } },
	{ 0x06000008, { 22, 3 } },
	{ 0x06000009, { 25, 1 } },
	{ 0x0600000A, { 26, 3 } },
	{ 0x0600000B, { 29, 2 } },
	{ 0x0600000C, { 31, 3 } },
};
static const Il2CppRGCTXDefinition s_rgctxValues[96] = 
{
	{ (Il2CppRGCTXDataType)2, 9023 },
	{ (Il2CppRGCTXDataType)3, 10280 },
	{ (Il2CppRGCTXDataType)2, 9024 },
	{ (Il2CppRGCTXDataType)2, 9025 },
	{ (Il2CppRGCTXDataType)3, 10281 },
	{ (Il2CppRGCTXDataType)2, 9026 },
	{ (Il2CppRGCTXDataType)2, 9027 },
	{ (Il2CppRGCTXDataType)3, 10282 },
	{ (Il2CppRGCTXDataType)2, 9028 },
	{ (Il2CppRGCTXDataType)3, 10283 },
	{ (Il2CppRGCTXDataType)2, 9029 },
	{ (Il2CppRGCTXDataType)3, 10284 },
	{ (Il2CppRGCTXDataType)3, 10285 },
	{ (Il2CppRGCTXDataType)2, 5027 },
	{ (Il2CppRGCTXDataType)3, 10286 },
	{ (Il2CppRGCTXDataType)2, 9030 },
	{ (Il2CppRGCTXDataType)3, 10287 },
	{ (Il2CppRGCTXDataType)3, 10288 },
	{ (Il2CppRGCTXDataType)2, 9031 },
	{ (Il2CppRGCTXDataType)2, 9032 },
	{ (Il2CppRGCTXDataType)2, 5032 },
	{ (Il2CppRGCTXDataType)2, 9033 },
	{ (Il2CppRGCTXDataType)2, 5034 },
	{ (Il2CppRGCTXDataType)2, 9034 },
	{ (Il2CppRGCTXDataType)3, 10289 },
	{ (Il2CppRGCTXDataType)2, 5037 },
	{ (Il2CppRGCTXDataType)2, 5039 },
	{ (Il2CppRGCTXDataType)2, 9035 },
	{ (Il2CppRGCTXDataType)3, 10290 },
	{ (Il2CppRGCTXDataType)2, 9036 },
	{ (Il2CppRGCTXDataType)2, 5042 },
	{ (Il2CppRGCTXDataType)2, 5044 },
	{ (Il2CppRGCTXDataType)2, 9037 },
	{ (Il2CppRGCTXDataType)3, 10291 },
	{ (Il2CppRGCTXDataType)3, 10292 },
	{ (Il2CppRGCTXDataType)3, 10293 },
	{ (Il2CppRGCTXDataType)2, 5049 },
	{ (Il2CppRGCTXDataType)3, 10294 },
	{ (Il2CppRGCTXDataType)3, 10295 },
	{ (Il2CppRGCTXDataType)2, 5058 },
	{ (Il2CppRGCTXDataType)2, 9038 },
	{ (Il2CppRGCTXDataType)3, 10296 },
	{ (Il2CppRGCTXDataType)3, 10297 },
	{ (Il2CppRGCTXDataType)2, 5060 },
	{ (Il2CppRGCTXDataType)2, 8918 },
	{ (Il2CppRGCTXDataType)3, 10298 },
	{ (Il2CppRGCTXDataType)3, 10299 },
	{ (Il2CppRGCTXDataType)3, 10300 },
	{ (Il2CppRGCTXDataType)2, 5067 },
	{ (Il2CppRGCTXDataType)2, 9039 },
	{ (Il2CppRGCTXDataType)3, 10301 },
	{ (Il2CppRGCTXDataType)3, 10302 },
	{ (Il2CppRGCTXDataType)3, 9845 },
	{ (Il2CppRGCTXDataType)3, 10303 },
	{ (Il2CppRGCTXDataType)3, 10304 },
	{ (Il2CppRGCTXDataType)2, 5076 },
	{ (Il2CppRGCTXDataType)2, 9040 },
	{ (Il2CppRGCTXDataType)3, 10305 },
	{ (Il2CppRGCTXDataType)3, 10306 },
	{ (Il2CppRGCTXDataType)3, 10307 },
	{ (Il2CppRGCTXDataType)3, 10308 },
	{ (Il2CppRGCTXDataType)3, 10309 },
	{ (Il2CppRGCTXDataType)3, 9851 },
	{ (Il2CppRGCTXDataType)3, 10310 },
	{ (Il2CppRGCTXDataType)3, 10311 },
	{ (Il2CppRGCTXDataType)2, 9041 },
	{ (Il2CppRGCTXDataType)2, 5092 },
	{ (Il2CppRGCTXDataType)2, 5090 },
	{ (Il2CppRGCTXDataType)2, 9042 },
	{ (Il2CppRGCTXDataType)3, 10312 },
	{ (Il2CppRGCTXDataType)2, 9043 },
	{ (Il2CppRGCTXDataType)3, 10313 },
	{ (Il2CppRGCTXDataType)3, 10314 },
	{ (Il2CppRGCTXDataType)3, 10315 },
	{ (Il2CppRGCTXDataType)2, 5096 },
	{ (Il2CppRGCTXDataType)3, 10316 },
	{ (Il2CppRGCTXDataType)3, 10317 },
	{ (Il2CppRGCTXDataType)2, 5099 },
	{ (Il2CppRGCTXDataType)3, 10318 },
	{ (Il2CppRGCTXDataType)1, 9044 },
	{ (Il2CppRGCTXDataType)2, 5098 },
	{ (Il2CppRGCTXDataType)3, 10319 },
	{ (Il2CppRGCTXDataType)1, 5098 },
	{ (Il2CppRGCTXDataType)1, 5096 },
	{ (Il2CppRGCTXDataType)2, 9045 },
	{ (Il2CppRGCTXDataType)2, 5098 },
	{ (Il2CppRGCTXDataType)3, 10320 },
	{ (Il2CppRGCTXDataType)2, 5101 },
	{ (Il2CppRGCTXDataType)2, 5100 },
	{ (Il2CppRGCTXDataType)3, 10321 },
	{ (Il2CppRGCTXDataType)3, 10322 },
	{ (Il2CppRGCTXDataType)3, 10323 },
	{ (Il2CppRGCTXDataType)3, 10324 },
	{ (Il2CppRGCTXDataType)2, 5097 },
	{ (Il2CppRGCTXDataType)3, 10325 },
	{ (Il2CppRGCTXDataType)2, 5111 },
};
extern const Il2CppCodeGenModule g_System_CoreCodeGenModule;
const Il2CppCodeGenModule g_System_CoreCodeGenModule = 
{
	"System.Core.dll",
	69,
	s_methodPointers,
	s_InvokerIndices,
	0,
	NULL,
	17,
	s_rgctxIndices,
	96,
	s_rgctxValues,
	NULL,
};

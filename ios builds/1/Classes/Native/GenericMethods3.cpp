﻿#include "pch-cpp.hpp"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <stdint.h>
#include <limits>


template <typename T1>
struct InterfaceActionInvoker1
{
	typedef void (*Action)(void*, T1, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeClass* declaringInterface, RuntimeObject* obj, T1 p1)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_interface_invoke_data(slot, obj, declaringInterface);
		((Action)invokeData.methodPtr)(obj, p1, invokeData.method);
	}
};
struct InterfaceActionInvoker0
{
	typedef void (*Action)(void*, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeClass* declaringInterface, RuntimeObject* obj)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_interface_invoke_data(slot, obj, declaringInterface);
		((Action)invokeData.methodPtr)(obj, invokeData.method);
	}
};

// System.Action
struct Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6;
// System.ArgumentNullException
struct ArgumentNullException_tFB5C4621957BC53A7D1B4FDD5C38B4D6E15DB8FB;
// System.AsyncCallback
struct AsyncCallback_tA7921BEF974919C46FF8F9D9867C567B200BB0EA;
// System.Threading.CancellationTokenSource
struct CancellationTokenSource_t78B989179DE23EDD36F870FFEE20A15D6D3C65B3;
// System.Threading.ContextCallback
struct ContextCallback_t93707E0430F4FF3E15E1FB5A4844BE89C657AE8B;
// System.DelegateData
struct DelegateData_t17DD30660E330C49381DAA99F934BE75CB11F288;
// System.Exception
struct Exception_t;
// System.Threading.ExecutionContext
struct ExecutionContext_t16AC73BB21FEEEAD34A017877AC18DD8BB836414;
// System.IAsyncResult
struct IAsyncResult_tC9F97BF36FCF122D29D3101D80642278297BF370;
// System.Runtime.CompilerServices.IAsyncStateMachine
struct IAsyncStateMachine_tAE063F84A60E1058FCA4E3EA9F555D3462641F7D;
// System.Collections.IDictionary
struct IDictionary_t99871C56B8EC2452AC5C4CF3831695E617B89D3A;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.Runtime.Serialization.SafeSerializationManager
struct SafeSerializationManager_tDE44F029589A028F8A3053C5C06153FAB4AAE29F;
// System.Threading.SemaphoreSlim
struct SemaphoreSlim_t3EF85FC980AE57957BEBB6B78E81DE2E3233D385;
// System.Threading.Tasks.StackGuard
struct StackGuard_t88E1EE4741AD02CA5FEA04A4EB2CC70F230E0E6D;
// System.String
struct String_t;
// System.Threading.SynchronizationContext
struct SynchronizationContext_t17D9365B5E0D30A0910A16FA4351C525232EF069;
// System.Threading.Tasks.Task
struct Task_t804B25CFE3FC13AAEE16C8FA3BF52513F2A8DB60;
// System.Threading.Tasks.TaskFactory
struct TaskFactory_t22D999A05A967C31A4B5FFBD08864809BF35EA3B;
// System.Threading.Tasks.TaskScheduler
struct TaskScheduler_t74FBEEEDBDD5E0088FF0EEC18F45CD866B098D5D;
// System.Threading.Thread
struct Thread_tB9EB71664220EE16451AF3276D78DE6614D2A414;
// System.Runtime.CompilerServices.AsyncMethodBuilderCore/MoveNextRunner
struct MoveNextRunner_tFAEA0BEDD353E2E34E8E287C67B1F5572FD30C2D;
// System.Threading.SemaphoreSlim/TaskNode
struct TaskNode_tD3014A57510D018F890E6524AC62F9417E2E6C4E;
// System.Threading.Tasks.Task/ContingentProperties
struct ContingentProperties_t1E249C737B8B8644ED1D60EEFA101D326B199EA0;
// System.Action`1<System.Object>
struct Action_1_tD9663D9715FAA4E62035CFCF1AD4D094EE7872DC;
// System.Collections.Generic.Dictionary`2<System.Int32,System.Threading.Tasks.Task>
struct Dictionary_2_tB758E2A2593CD827EFC041BE1F1BB4B68DE1C3E8;
// System.Func`1<System.Threading.Tasks.Task/ContingentProperties>
struct Func_1_tBCF42601FA307876E83080BE4204110820F8BF3B;
// System.Func`2<System.Threading.Tasks.Task`1<System.Threading.Tasks.Task>,System.Threading.Tasks.Task`1<System.Boolean>>
struct Func_2_t24DC43D57AB022882FE433E3B16B6D7E4BD14BB4;
// System.Func`2<System.Threading.Tasks.Task`1<System.Threading.Tasks.Task>,System.Threading.Tasks.Task`1<System.Object>>
struct Func_2_t44F36790F9746FCE5ABFDE6205B6020B2578F6DD;
// Obi.ObiNativeList`1<Obi.Aabb>
struct ObiNativeList_1_t70F6D07F08AA8D90F04FAF88B56FBA49BF80957D;
// Obi.ObiNativeList`1<Obi.AffineTransform>
struct ObiNativeList_1_t03BFDE41D0D9A18FFB5DAA42B75CB84B78E2C769;
// Obi.ObiNativeList`1<Obi.BIHNode>
struct ObiNativeList_1_tAB1F96F0D20C4C225040A67D9C56B627503C46A7;
// Obi.ObiNativeList`1<Obi.CellSpan>
struct ObiNativeList_1_t32C81553D1B41E21CC334835948700A53FAFF834;
// Obi.ObiNativeList`1<Obi.ColliderRigidbody>
struct ObiNativeList_1_t05A6C6A8DA09130C90EB95364D1840CBCDA790F9;
// Obi.ObiNativeList`1<Obi.ColliderShape>
struct ObiNativeList_1_t30AA57B53B1359362269E706BCAAE5664D2065C0;
// Obi.ObiNativeList`1<Obi.CollisionMaterial>
struct ObiNativeList_1_t0B4D5B3FAA024A9AC166F149B983A78AF3080145;
// Obi.ObiNativeList`1<Obi.DFNode>
struct ObiNativeList_1_t6ADADB0DAAC6C2F9D071750D398057641903EC70;
// Obi.ObiNativeList`1<Obi.DistanceFieldHeader>
struct ObiNativeList_1_t018D553481251B27F7FE696F676040DCB6C3F0EA;
// Obi.ObiNativeList`1<Obi.Edge>
struct ObiNativeList_1_t96FABDF83D294D0ABBFEFAE74DEFCD1CF2243AF7;
// Obi.ObiNativeList`1<Obi.EdgeMeshHeader>
struct ObiNativeList_1_t82AAD6F0A45EA387F6031B345E681AE0DADF910C;
// Obi.ObiNativeList`1<Obi.HeightFieldHeader>
struct ObiNativeList_1_t2A4871B78EE973A2C9E2473CBF7A597169D426CB;
// Obi.ObiNativeList`1<System.Int32>
struct ObiNativeList_1_t838DB140073974101A3C0BD56D899DA67D9B2EC4;
// Obi.ObiNativeList`1<System.IntPtr>
struct ObiNativeList_1_t2691FD222DD25B2FB43888C0159E83392B57A150;
// Obi.ObiNativeList`1<UnityEngine.Matrix4x4>
struct ObiNativeList_1_t93A1DE0BF264327DCAC68372C96B4969F13BC062;
// Obi.ObiNativeList`1<UnityEngine.Quaternion>
struct ObiNativeList_1_tED35083346C98040EABF6B4FAC5245310D8B9939;
// Obi.ObiNativeList`1<Obi.QueryResult>
struct ObiNativeList_1_tE4E9A225B4175E5D04A1C69103C9CB0962541089;
// Obi.ObiNativeList`1<Obi.QueryShape>
struct ObiNativeList_1_tD298AB56D582AAA797727DF401F4FAC39E7DCAEF;
// Obi.ObiNativeList`1<System.Single>
struct ObiNativeList_1_tE371D1BA733B218B4F6296CB724D923CB25CC53B;
// Obi.ObiNativeList`1<Obi.Triangle>
struct ObiNativeList_1_tDE675C68FBF18547F862FC9F08A57ABD59D44A88;
// Obi.ObiNativeList`1<Obi.TriangleMeshHeader>
struct ObiNativeList_1_t8C4BAE9CBB1F8A70288B65B1791A14FB832E1B1F;
// Obi.ObiNativeList`1<Obi.VInt4>
struct ObiNativeList_1_tBFECFF8B42E1120D079278D2C8F079D3E3BEA07A;
// Obi.ObiNativeList`1<UnityEngine.Vector2>
struct ObiNativeList_1_t5C6ED20BB4BD829B45C019D5E3348CC6BAF2F37D;
// Obi.ObiNativeList`1<UnityEngine.Vector3>
struct ObiNativeList_1_t0F29E875C6BF5DD7F89BA979309B5604FDC8DB26;
// Obi.ObiNativeList`1<UnityEngine.Vector4>
struct ObiNativeList_1_t667688879E4BD2DFE8EDD92169E59F19619DF4F4;
// Obi.ObiNativeList`1<Oni/Contact>
struct ObiNativeList_1_tCEAE7C719A3DBFC45782E33AFCE91E13E9CA9AF4;
// System.Predicate`1<System.Object>
struct Predicate_1_t5C96B81B31A697B11C4C3767E3298773AF25DFEB;
// System.Predicate`1<System.Threading.Tasks.Task>
struct Predicate_1_tC0DBBC8498BD1EE6ABFFAA5628024105FA7D11BD;
// System.Threading.Tasks.TaskFactory`1<System.Boolean>
struct TaskFactory_1_t069438A73348A2B1B34A2C68E0478EE107ECCFC7;
// System.Threading.Tasks.TaskFactory`1<System.Object>
struct TaskFactory_1_t16A95DD17BBA3D00F0A85C5077BB248421EF3A55;
// System.Threading.Tasks.Task`1<System.Boolean>
struct Task_1_t9C1FE9F18F52F3409B9E970FA38801A443AE7849;
// System.Threading.Tasks.Task`1<System.Object>
struct Task_1_tC1805497876E88B78A2B0CB81C6409E0B381AC17;
// System.Threading.Tasks.Task`1<System.Threading.Tasks.Task>
struct Task_1_t24E932728D4BE67BFA41487F43AE4FAEBBAC7284;
// Obi.Aabb[]
struct AabbU5BU5D_t808E7E46E2A4C909D8A884895C6CA043EF193923;
// Obi.AffineTransform[]
struct AffineTransformU5BU5D_t610EDC53C4D6044DC0373666A79BFE3B0F149B60;
// Obi.BIHNode[]
struct BIHNodeU5BU5D_tFFEC19AD5DEFC4CDF6B909096C4A7FE258470889;
// Obi.CellSpan[]
struct CellSpanU5BU5D_t88DFA1D47CFDAF1D579130B3C6868ACE657C031A;
// System.Char[]
struct CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34;
// Obi.ColliderRigidbody[]
struct ColliderRigidbodyU5BU5D_tC368D75C1E91456D9E934F1B3B604AA02083D1A1;
// Obi.ColliderShape[]
struct ColliderShapeU5BU5D_t64D38EA60BE56F70B656C9C727A5F2530D2C2E24;
// Obi.CollisionMaterial[]
struct CollisionMaterialU5BU5D_t133A699CD2AAD3027174B99647BC3F4E6FF64B93;
// UnityEngine.ComputeBuffer
struct ComputeBuffer_t795666617BDC98AB4033FDD4CE0304A22B7E6427;
// Oni/Contact[]
struct ContactU5BU5D_t0EA4D0F4F28544AEC9172BDBCC872BD763772C2C;
// Obi.DFNode[]
struct DFNodeU5BU5D_tD7926F1E8C4599C77D5BD2BB151B499CCE86E5D0;
// System.Delegate[]
struct DelegateU5BU5D_t677D8FE08A5F99E8EE49150B73966CD6E9BF7DB8;
// Obi.DistanceFieldHeader[]
struct DistanceFieldHeaderU5BU5D_t0593F81444848E01D41886C6C8DDCDB5E63DD2EA;
// Obi.EdgeMeshHeader[]
struct EdgeMeshHeaderU5BU5D_t46887E858B4028955E4ADCCF2838C854CFE81B81;
// Obi.Edge[]
struct EdgeU5BU5D_tB4E38D11113C524DF358EB19FA6D4CB4C3E914B8;
// Obi.HeightFieldHeader[]
struct HeightFieldHeaderU5BU5D_t45DA5AA034FA4400953925EDC54523CCDAC9A6AE;
// System.Int32[]
struct Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32;
// System.IntPtr[]
struct IntPtrU5BU5D_t27FC72B0409D75AAF33EC42498E8094E95FEE9A6;
// UnityEngine.Matrix4x4[]
struct Matrix4x4U5BU5D_tE53F71E9C9110DD439281A6AB8B699F9F85D8F82;
// UnityEngine.Quaternion[]
struct QuaternionU5BU5D_t584B1CC68E95071898E32F34DB2CC1E4A726FAA6;
// Obi.QueryResult[]
struct QueryResultU5BU5D_t7296831A0BF97D2990464E82C176858D4287CAAA;
// Obi.QueryShape[]
struct QueryShapeU5BU5D_t423AECB189811BF7BFCA5C6D7786795BDBC4BFC5;
// System.Single[]
struct SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA;
// System.Diagnostics.StackTrace[]
struct StackTraceU5BU5D_t4AD999C288CB6D1F38A299D12B1598D606588971;
// Obi.TriangleMeshHeader[]
struct TriangleMeshHeaderU5BU5D_tD0A41E3D9563534F8DE40F7964BBFDBBA1E0EEC2;
// Obi.Triangle[]
struct TriangleU5BU5D_tBE6B25F3BD28AB7727D9AFAD215574627C8ADD53;
// Obi.VInt4[]
struct VInt4U5BU5D_tAFB782B2F5085208B324359A3380F874BB4F708A;
// UnityEngine.Vector2[]
struct Vector2U5BU5D_tE0F58A2D6D8592B5EC37D9CDEF09103A02E5D7FA;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4;
// UnityEngine.Vector4[]
struct Vector4U5BU5D_tCE72D928AA6FF1852BAC5E4396F6F0131ED11871;
// System.Void
struct Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5;

IL2CPP_EXTERN_C RuntimeClass* ArgumentNullException_tFB5C4621957BC53A7D1B4FDD5C38B4D6E15DB8FB_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Exception_t_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* ExecutionContext_t16AC73BB21FEEEAD34A017877AC18DD8BB836414_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* IAsyncStateMachine_tAE063F84A60E1058FCA4E3EA9F555D3462641F7D_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* ICriticalNotifyCompletion_t3F4FCEA29CDCACD07747F3A1D6D1406521556838_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C String_t* _stringLiteralC70965A7D491520CA8D04D4EA01613EFED3309E0;
IL2CPP_EXTERN_C const RuntimeMethod* AsyncTaskMethodBuilder_1_Start_TisRuntimeObject_m25C57EA59EF179635A81530036C12082EE50878B_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* AsyncTaskMethodBuilder_1_Start_TisU3CWaitUntilCountOrTimeoutAsyncU3Ed__31_t227D1F5F19C948CA8C23C80B5F19147D4AAED14F_m67A42FEF4C09E843DDA99A81F84E675F9AB87CF9_RuntimeMethod_var;
IL2CPP_EXTERN_C const uint32_t AsyncTaskMethodBuilder_1_AwaitUnsafeOnCompleted_TisConfiguredTaskAwaiter_t286C97C0AF102C4C0BE55CE2025CC7BD1FB5C20C_TisU3CWaitUntilCountOrTimeoutAsyncU3Ed__31_t227D1F5F19C948CA8C23C80B5F19147D4AAED14F_m06010DFBCE2CDD2AE1BB5ADD25230521E9B527DC_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t AsyncTaskMethodBuilder_1_AwaitUnsafeOnCompleted_TisConfiguredTaskAwaiter_t2CE498F9A6CE5405242AE2D77F03E58985B7C3ED_TisU3CWaitUntilCountOrTimeoutAsyncU3Ed__31_t227D1F5F19C948CA8C23C80B5F19147D4AAED14F_m4FDC6642836D25291A0E343845527873A88013C9_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t AsyncTaskMethodBuilder_1_AwaitUnsafeOnCompleted_TisRuntimeObject_TisRuntimeObject_mD7F0BC5F052B851265FF930DD5AB23C8E96269DE_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t AsyncTaskMethodBuilder_1_Start_TisRuntimeObject_m25C57EA59EF179635A81530036C12082EE50878B_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t AsyncTaskMethodBuilder_1_Start_TisU3CWaitUntilCountOrTimeoutAsyncU3Ed__31_t227D1F5F19C948CA8C23C80B5F19147D4AAED14F_m67A42FEF4C09E843DDA99A81F84E675F9AB87CF9_MetadataUsageId;
struct Delegate_t_marshaled_com;
struct Delegate_t_marshaled_pinvoke;
struct Exception_t_marshaled_com;
struct Exception_t_marshaled_pinvoke;


IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object


// Obi.ObiNativeList`1<Obi.Aabb>
struct  ObiNativeList_1_t70F6D07F08AA8D90F04FAF88B56FBA49BF80957D  : public RuntimeObject
{
public:
	// T[] Obi.ObiNativeList`1::serializedContents
	AabbU5BU5D_t808E7E46E2A4C909D8A884895C6CA043EF193923* ___serializedContents_0;
	// System.Void* Obi.ObiNativeList`1::m_AlignedPtr
	void* ___m_AlignedPtr_1;
	// System.Int32 Obi.ObiNativeList`1::m_Stride
	int32_t ___m_Stride_2;
	// System.Int32 Obi.ObiNativeList`1::m_Capacity
	int32_t ___m_Capacity_3;
	// System.Int32 Obi.ObiNativeList`1::m_Count
	int32_t ___m_Count_4;
	// System.Boolean Obi.ObiNativeList`1::m_AsNativeArray
	bool ___m_AsNativeArray_5;
	// UnityEngine.ComputeBuffer Obi.ObiNativeList`1::m_ComputeBuffer
	ComputeBuffer_t795666617BDC98AB4033FDD4CE0304A22B7E6427 * ___m_ComputeBuffer_6;

public:
	inline static int32_t get_offset_of_serializedContents_0() { return static_cast<int32_t>(offsetof(ObiNativeList_1_t70F6D07F08AA8D90F04FAF88B56FBA49BF80957D, ___serializedContents_0)); }
	inline AabbU5BU5D_t808E7E46E2A4C909D8A884895C6CA043EF193923* get_serializedContents_0() const { return ___serializedContents_0; }
	inline AabbU5BU5D_t808E7E46E2A4C909D8A884895C6CA043EF193923** get_address_of_serializedContents_0() { return &___serializedContents_0; }
	inline void set_serializedContents_0(AabbU5BU5D_t808E7E46E2A4C909D8A884895C6CA043EF193923* value)
	{
		___serializedContents_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___serializedContents_0), (void*)value);
	}

	inline static int32_t get_offset_of_m_AlignedPtr_1() { return static_cast<int32_t>(offsetof(ObiNativeList_1_t70F6D07F08AA8D90F04FAF88B56FBA49BF80957D, ___m_AlignedPtr_1)); }
	inline void* get_m_AlignedPtr_1() const { return ___m_AlignedPtr_1; }
	inline void** get_address_of_m_AlignedPtr_1() { return &___m_AlignedPtr_1; }
	inline void set_m_AlignedPtr_1(void* value)
	{
		___m_AlignedPtr_1 = value;
	}

	inline static int32_t get_offset_of_m_Stride_2() { return static_cast<int32_t>(offsetof(ObiNativeList_1_t70F6D07F08AA8D90F04FAF88B56FBA49BF80957D, ___m_Stride_2)); }
	inline int32_t get_m_Stride_2() const { return ___m_Stride_2; }
	inline int32_t* get_address_of_m_Stride_2() { return &___m_Stride_2; }
	inline void set_m_Stride_2(int32_t value)
	{
		___m_Stride_2 = value;
	}

	inline static int32_t get_offset_of_m_Capacity_3() { return static_cast<int32_t>(offsetof(ObiNativeList_1_t70F6D07F08AA8D90F04FAF88B56FBA49BF80957D, ___m_Capacity_3)); }
	inline int32_t get_m_Capacity_3() const { return ___m_Capacity_3; }
	inline int32_t* get_address_of_m_Capacity_3() { return &___m_Capacity_3; }
	inline void set_m_Capacity_3(int32_t value)
	{
		___m_Capacity_3 = value;
	}

	inline static int32_t get_offset_of_m_Count_4() { return static_cast<int32_t>(offsetof(ObiNativeList_1_t70F6D07F08AA8D90F04FAF88B56FBA49BF80957D, ___m_Count_4)); }
	inline int32_t get_m_Count_4() const { return ___m_Count_4; }
	inline int32_t* get_address_of_m_Count_4() { return &___m_Count_4; }
	inline void set_m_Count_4(int32_t value)
	{
		___m_Count_4 = value;
	}

	inline static int32_t get_offset_of_m_AsNativeArray_5() { return static_cast<int32_t>(offsetof(ObiNativeList_1_t70F6D07F08AA8D90F04FAF88B56FBA49BF80957D, ___m_AsNativeArray_5)); }
	inline bool get_m_AsNativeArray_5() const { return ___m_AsNativeArray_5; }
	inline bool* get_address_of_m_AsNativeArray_5() { return &___m_AsNativeArray_5; }
	inline void set_m_AsNativeArray_5(bool value)
	{
		___m_AsNativeArray_5 = value;
	}

	inline static int32_t get_offset_of_m_ComputeBuffer_6() { return static_cast<int32_t>(offsetof(ObiNativeList_1_t70F6D07F08AA8D90F04FAF88B56FBA49BF80957D, ___m_ComputeBuffer_6)); }
	inline ComputeBuffer_t795666617BDC98AB4033FDD4CE0304A22B7E6427 * get_m_ComputeBuffer_6() const { return ___m_ComputeBuffer_6; }
	inline ComputeBuffer_t795666617BDC98AB4033FDD4CE0304A22B7E6427 ** get_address_of_m_ComputeBuffer_6() { return &___m_ComputeBuffer_6; }
	inline void set_m_ComputeBuffer_6(ComputeBuffer_t795666617BDC98AB4033FDD4CE0304A22B7E6427 * value)
	{
		___m_ComputeBuffer_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_ComputeBuffer_6), (void*)value);
	}
};


// Obi.ObiNativeList`1<Obi.AffineTransform>
struct  ObiNativeList_1_t03BFDE41D0D9A18FFB5DAA42B75CB84B78E2C769  : public RuntimeObject
{
public:
	// T[] Obi.ObiNativeList`1::serializedContents
	AffineTransformU5BU5D_t610EDC53C4D6044DC0373666A79BFE3B0F149B60* ___serializedContents_0;
	// System.Void* Obi.ObiNativeList`1::m_AlignedPtr
	void* ___m_AlignedPtr_1;
	// System.Int32 Obi.ObiNativeList`1::m_Stride
	int32_t ___m_Stride_2;
	// System.Int32 Obi.ObiNativeList`1::m_Capacity
	int32_t ___m_Capacity_3;
	// System.Int32 Obi.ObiNativeList`1::m_Count
	int32_t ___m_Count_4;
	// System.Boolean Obi.ObiNativeList`1::m_AsNativeArray
	bool ___m_AsNativeArray_5;
	// UnityEngine.ComputeBuffer Obi.ObiNativeList`1::m_ComputeBuffer
	ComputeBuffer_t795666617BDC98AB4033FDD4CE0304A22B7E6427 * ___m_ComputeBuffer_6;

public:
	inline static int32_t get_offset_of_serializedContents_0() { return static_cast<int32_t>(offsetof(ObiNativeList_1_t03BFDE41D0D9A18FFB5DAA42B75CB84B78E2C769, ___serializedContents_0)); }
	inline AffineTransformU5BU5D_t610EDC53C4D6044DC0373666A79BFE3B0F149B60* get_serializedContents_0() const { return ___serializedContents_0; }
	inline AffineTransformU5BU5D_t610EDC53C4D6044DC0373666A79BFE3B0F149B60** get_address_of_serializedContents_0() { return &___serializedContents_0; }
	inline void set_serializedContents_0(AffineTransformU5BU5D_t610EDC53C4D6044DC0373666A79BFE3B0F149B60* value)
	{
		___serializedContents_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___serializedContents_0), (void*)value);
	}

	inline static int32_t get_offset_of_m_AlignedPtr_1() { return static_cast<int32_t>(offsetof(ObiNativeList_1_t03BFDE41D0D9A18FFB5DAA42B75CB84B78E2C769, ___m_AlignedPtr_1)); }
	inline void* get_m_AlignedPtr_1() const { return ___m_AlignedPtr_1; }
	inline void** get_address_of_m_AlignedPtr_1() { return &___m_AlignedPtr_1; }
	inline void set_m_AlignedPtr_1(void* value)
	{
		___m_AlignedPtr_1 = value;
	}

	inline static int32_t get_offset_of_m_Stride_2() { return static_cast<int32_t>(offsetof(ObiNativeList_1_t03BFDE41D0D9A18FFB5DAA42B75CB84B78E2C769, ___m_Stride_2)); }
	inline int32_t get_m_Stride_2() const { return ___m_Stride_2; }
	inline int32_t* get_address_of_m_Stride_2() { return &___m_Stride_2; }
	inline void set_m_Stride_2(int32_t value)
	{
		___m_Stride_2 = value;
	}

	inline static int32_t get_offset_of_m_Capacity_3() { return static_cast<int32_t>(offsetof(ObiNativeList_1_t03BFDE41D0D9A18FFB5DAA42B75CB84B78E2C769, ___m_Capacity_3)); }
	inline int32_t get_m_Capacity_3() const { return ___m_Capacity_3; }
	inline int32_t* get_address_of_m_Capacity_3() { return &___m_Capacity_3; }
	inline void set_m_Capacity_3(int32_t value)
	{
		___m_Capacity_3 = value;
	}

	inline static int32_t get_offset_of_m_Count_4() { return static_cast<int32_t>(offsetof(ObiNativeList_1_t03BFDE41D0D9A18FFB5DAA42B75CB84B78E2C769, ___m_Count_4)); }
	inline int32_t get_m_Count_4() const { return ___m_Count_4; }
	inline int32_t* get_address_of_m_Count_4() { return &___m_Count_4; }
	inline void set_m_Count_4(int32_t value)
	{
		___m_Count_4 = value;
	}

	inline static int32_t get_offset_of_m_AsNativeArray_5() { return static_cast<int32_t>(offsetof(ObiNativeList_1_t03BFDE41D0D9A18FFB5DAA42B75CB84B78E2C769, ___m_AsNativeArray_5)); }
	inline bool get_m_AsNativeArray_5() const { return ___m_AsNativeArray_5; }
	inline bool* get_address_of_m_AsNativeArray_5() { return &___m_AsNativeArray_5; }
	inline void set_m_AsNativeArray_5(bool value)
	{
		___m_AsNativeArray_5 = value;
	}

	inline static int32_t get_offset_of_m_ComputeBuffer_6() { return static_cast<int32_t>(offsetof(ObiNativeList_1_t03BFDE41D0D9A18FFB5DAA42B75CB84B78E2C769, ___m_ComputeBuffer_6)); }
	inline ComputeBuffer_t795666617BDC98AB4033FDD4CE0304A22B7E6427 * get_m_ComputeBuffer_6() const { return ___m_ComputeBuffer_6; }
	inline ComputeBuffer_t795666617BDC98AB4033FDD4CE0304A22B7E6427 ** get_address_of_m_ComputeBuffer_6() { return &___m_ComputeBuffer_6; }
	inline void set_m_ComputeBuffer_6(ComputeBuffer_t795666617BDC98AB4033FDD4CE0304A22B7E6427 * value)
	{
		___m_ComputeBuffer_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_ComputeBuffer_6), (void*)value);
	}
};


// Obi.ObiNativeList`1<Obi.BIHNode>
struct  ObiNativeList_1_tAB1F96F0D20C4C225040A67D9C56B627503C46A7  : public RuntimeObject
{
public:
	// T[] Obi.ObiNativeList`1::serializedContents
	BIHNodeU5BU5D_tFFEC19AD5DEFC4CDF6B909096C4A7FE258470889* ___serializedContents_0;
	// System.Void* Obi.ObiNativeList`1::m_AlignedPtr
	void* ___m_AlignedPtr_1;
	// System.Int32 Obi.ObiNativeList`1::m_Stride
	int32_t ___m_Stride_2;
	// System.Int32 Obi.ObiNativeList`1::m_Capacity
	int32_t ___m_Capacity_3;
	// System.Int32 Obi.ObiNativeList`1::m_Count
	int32_t ___m_Count_4;
	// System.Boolean Obi.ObiNativeList`1::m_AsNativeArray
	bool ___m_AsNativeArray_5;
	// UnityEngine.ComputeBuffer Obi.ObiNativeList`1::m_ComputeBuffer
	ComputeBuffer_t795666617BDC98AB4033FDD4CE0304A22B7E6427 * ___m_ComputeBuffer_6;

public:
	inline static int32_t get_offset_of_serializedContents_0() { return static_cast<int32_t>(offsetof(ObiNativeList_1_tAB1F96F0D20C4C225040A67D9C56B627503C46A7, ___serializedContents_0)); }
	inline BIHNodeU5BU5D_tFFEC19AD5DEFC4CDF6B909096C4A7FE258470889* get_serializedContents_0() const { return ___serializedContents_0; }
	inline BIHNodeU5BU5D_tFFEC19AD5DEFC4CDF6B909096C4A7FE258470889** get_address_of_serializedContents_0() { return &___serializedContents_0; }
	inline void set_serializedContents_0(BIHNodeU5BU5D_tFFEC19AD5DEFC4CDF6B909096C4A7FE258470889* value)
	{
		___serializedContents_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___serializedContents_0), (void*)value);
	}

	inline static int32_t get_offset_of_m_AlignedPtr_1() { return static_cast<int32_t>(offsetof(ObiNativeList_1_tAB1F96F0D20C4C225040A67D9C56B627503C46A7, ___m_AlignedPtr_1)); }
	inline void* get_m_AlignedPtr_1() const { return ___m_AlignedPtr_1; }
	inline void** get_address_of_m_AlignedPtr_1() { return &___m_AlignedPtr_1; }
	inline void set_m_AlignedPtr_1(void* value)
	{
		___m_AlignedPtr_1 = value;
	}

	inline static int32_t get_offset_of_m_Stride_2() { return static_cast<int32_t>(offsetof(ObiNativeList_1_tAB1F96F0D20C4C225040A67D9C56B627503C46A7, ___m_Stride_2)); }
	inline int32_t get_m_Stride_2() const { return ___m_Stride_2; }
	inline int32_t* get_address_of_m_Stride_2() { return &___m_Stride_2; }
	inline void set_m_Stride_2(int32_t value)
	{
		___m_Stride_2 = value;
	}

	inline static int32_t get_offset_of_m_Capacity_3() { return static_cast<int32_t>(offsetof(ObiNativeList_1_tAB1F96F0D20C4C225040A67D9C56B627503C46A7, ___m_Capacity_3)); }
	inline int32_t get_m_Capacity_3() const { return ___m_Capacity_3; }
	inline int32_t* get_address_of_m_Capacity_3() { return &___m_Capacity_3; }
	inline void set_m_Capacity_3(int32_t value)
	{
		___m_Capacity_3 = value;
	}

	inline static int32_t get_offset_of_m_Count_4() { return static_cast<int32_t>(offsetof(ObiNativeList_1_tAB1F96F0D20C4C225040A67D9C56B627503C46A7, ___m_Count_4)); }
	inline int32_t get_m_Count_4() const { return ___m_Count_4; }
	inline int32_t* get_address_of_m_Count_4() { return &___m_Count_4; }
	inline void set_m_Count_4(int32_t value)
	{
		___m_Count_4 = value;
	}

	inline static int32_t get_offset_of_m_AsNativeArray_5() { return static_cast<int32_t>(offsetof(ObiNativeList_1_tAB1F96F0D20C4C225040A67D9C56B627503C46A7, ___m_AsNativeArray_5)); }
	inline bool get_m_AsNativeArray_5() const { return ___m_AsNativeArray_5; }
	inline bool* get_address_of_m_AsNativeArray_5() { return &___m_AsNativeArray_5; }
	inline void set_m_AsNativeArray_5(bool value)
	{
		___m_AsNativeArray_5 = value;
	}

	inline static int32_t get_offset_of_m_ComputeBuffer_6() { return static_cast<int32_t>(offsetof(ObiNativeList_1_tAB1F96F0D20C4C225040A67D9C56B627503C46A7, ___m_ComputeBuffer_6)); }
	inline ComputeBuffer_t795666617BDC98AB4033FDD4CE0304A22B7E6427 * get_m_ComputeBuffer_6() const { return ___m_ComputeBuffer_6; }
	inline ComputeBuffer_t795666617BDC98AB4033FDD4CE0304A22B7E6427 ** get_address_of_m_ComputeBuffer_6() { return &___m_ComputeBuffer_6; }
	inline void set_m_ComputeBuffer_6(ComputeBuffer_t795666617BDC98AB4033FDD4CE0304A22B7E6427 * value)
	{
		___m_ComputeBuffer_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_ComputeBuffer_6), (void*)value);
	}
};


// Obi.ObiNativeList`1<Obi.CellSpan>
struct  ObiNativeList_1_t32C81553D1B41E21CC334835948700A53FAFF834  : public RuntimeObject
{
public:
	// T[] Obi.ObiNativeList`1::serializedContents
	CellSpanU5BU5D_t88DFA1D47CFDAF1D579130B3C6868ACE657C031A* ___serializedContents_0;
	// System.Void* Obi.ObiNativeList`1::m_AlignedPtr
	void* ___m_AlignedPtr_1;
	// System.Int32 Obi.ObiNativeList`1::m_Stride
	int32_t ___m_Stride_2;
	// System.Int32 Obi.ObiNativeList`1::m_Capacity
	int32_t ___m_Capacity_3;
	// System.Int32 Obi.ObiNativeList`1::m_Count
	int32_t ___m_Count_4;
	// System.Boolean Obi.ObiNativeList`1::m_AsNativeArray
	bool ___m_AsNativeArray_5;
	// UnityEngine.ComputeBuffer Obi.ObiNativeList`1::m_ComputeBuffer
	ComputeBuffer_t795666617BDC98AB4033FDD4CE0304A22B7E6427 * ___m_ComputeBuffer_6;

public:
	inline static int32_t get_offset_of_serializedContents_0() { return static_cast<int32_t>(offsetof(ObiNativeList_1_t32C81553D1B41E21CC334835948700A53FAFF834, ___serializedContents_0)); }
	inline CellSpanU5BU5D_t88DFA1D47CFDAF1D579130B3C6868ACE657C031A* get_serializedContents_0() const { return ___serializedContents_0; }
	inline CellSpanU5BU5D_t88DFA1D47CFDAF1D579130B3C6868ACE657C031A** get_address_of_serializedContents_0() { return &___serializedContents_0; }
	inline void set_serializedContents_0(CellSpanU5BU5D_t88DFA1D47CFDAF1D579130B3C6868ACE657C031A* value)
	{
		___serializedContents_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___serializedContents_0), (void*)value);
	}

	inline static int32_t get_offset_of_m_AlignedPtr_1() { return static_cast<int32_t>(offsetof(ObiNativeList_1_t32C81553D1B41E21CC334835948700A53FAFF834, ___m_AlignedPtr_1)); }
	inline void* get_m_AlignedPtr_1() const { return ___m_AlignedPtr_1; }
	inline void** get_address_of_m_AlignedPtr_1() { return &___m_AlignedPtr_1; }
	inline void set_m_AlignedPtr_1(void* value)
	{
		___m_AlignedPtr_1 = value;
	}

	inline static int32_t get_offset_of_m_Stride_2() { return static_cast<int32_t>(offsetof(ObiNativeList_1_t32C81553D1B41E21CC334835948700A53FAFF834, ___m_Stride_2)); }
	inline int32_t get_m_Stride_2() const { return ___m_Stride_2; }
	inline int32_t* get_address_of_m_Stride_2() { return &___m_Stride_2; }
	inline void set_m_Stride_2(int32_t value)
	{
		___m_Stride_2 = value;
	}

	inline static int32_t get_offset_of_m_Capacity_3() { return static_cast<int32_t>(offsetof(ObiNativeList_1_t32C81553D1B41E21CC334835948700A53FAFF834, ___m_Capacity_3)); }
	inline int32_t get_m_Capacity_3() const { return ___m_Capacity_3; }
	inline int32_t* get_address_of_m_Capacity_3() { return &___m_Capacity_3; }
	inline void set_m_Capacity_3(int32_t value)
	{
		___m_Capacity_3 = value;
	}

	inline static int32_t get_offset_of_m_Count_4() { return static_cast<int32_t>(offsetof(ObiNativeList_1_t32C81553D1B41E21CC334835948700A53FAFF834, ___m_Count_4)); }
	inline int32_t get_m_Count_4() const { return ___m_Count_4; }
	inline int32_t* get_address_of_m_Count_4() { return &___m_Count_4; }
	inline void set_m_Count_4(int32_t value)
	{
		___m_Count_4 = value;
	}

	inline static int32_t get_offset_of_m_AsNativeArray_5() { return static_cast<int32_t>(offsetof(ObiNativeList_1_t32C81553D1B41E21CC334835948700A53FAFF834, ___m_AsNativeArray_5)); }
	inline bool get_m_AsNativeArray_5() const { return ___m_AsNativeArray_5; }
	inline bool* get_address_of_m_AsNativeArray_5() { return &___m_AsNativeArray_5; }
	inline void set_m_AsNativeArray_5(bool value)
	{
		___m_AsNativeArray_5 = value;
	}

	inline static int32_t get_offset_of_m_ComputeBuffer_6() { return static_cast<int32_t>(offsetof(ObiNativeList_1_t32C81553D1B41E21CC334835948700A53FAFF834, ___m_ComputeBuffer_6)); }
	inline ComputeBuffer_t795666617BDC98AB4033FDD4CE0304A22B7E6427 * get_m_ComputeBuffer_6() const { return ___m_ComputeBuffer_6; }
	inline ComputeBuffer_t795666617BDC98AB4033FDD4CE0304A22B7E6427 ** get_address_of_m_ComputeBuffer_6() { return &___m_ComputeBuffer_6; }
	inline void set_m_ComputeBuffer_6(ComputeBuffer_t795666617BDC98AB4033FDD4CE0304A22B7E6427 * value)
	{
		___m_ComputeBuffer_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_ComputeBuffer_6), (void*)value);
	}
};


// Obi.ObiNativeList`1<Obi.ColliderRigidbody>
struct  ObiNativeList_1_t05A6C6A8DA09130C90EB95364D1840CBCDA790F9  : public RuntimeObject
{
public:
	// T[] Obi.ObiNativeList`1::serializedContents
	ColliderRigidbodyU5BU5D_tC368D75C1E91456D9E934F1B3B604AA02083D1A1* ___serializedContents_0;
	// System.Void* Obi.ObiNativeList`1::m_AlignedPtr
	void* ___m_AlignedPtr_1;
	// System.Int32 Obi.ObiNativeList`1::m_Stride
	int32_t ___m_Stride_2;
	// System.Int32 Obi.ObiNativeList`1::m_Capacity
	int32_t ___m_Capacity_3;
	// System.Int32 Obi.ObiNativeList`1::m_Count
	int32_t ___m_Count_4;
	// System.Boolean Obi.ObiNativeList`1::m_AsNativeArray
	bool ___m_AsNativeArray_5;
	// UnityEngine.ComputeBuffer Obi.ObiNativeList`1::m_ComputeBuffer
	ComputeBuffer_t795666617BDC98AB4033FDD4CE0304A22B7E6427 * ___m_ComputeBuffer_6;

public:
	inline static int32_t get_offset_of_serializedContents_0() { return static_cast<int32_t>(offsetof(ObiNativeList_1_t05A6C6A8DA09130C90EB95364D1840CBCDA790F9, ___serializedContents_0)); }
	inline ColliderRigidbodyU5BU5D_tC368D75C1E91456D9E934F1B3B604AA02083D1A1* get_serializedContents_0() const { return ___serializedContents_0; }
	inline ColliderRigidbodyU5BU5D_tC368D75C1E91456D9E934F1B3B604AA02083D1A1** get_address_of_serializedContents_0() { return &___serializedContents_0; }
	inline void set_serializedContents_0(ColliderRigidbodyU5BU5D_tC368D75C1E91456D9E934F1B3B604AA02083D1A1* value)
	{
		___serializedContents_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___serializedContents_0), (void*)value);
	}

	inline static int32_t get_offset_of_m_AlignedPtr_1() { return static_cast<int32_t>(offsetof(ObiNativeList_1_t05A6C6A8DA09130C90EB95364D1840CBCDA790F9, ___m_AlignedPtr_1)); }
	inline void* get_m_AlignedPtr_1() const { return ___m_AlignedPtr_1; }
	inline void** get_address_of_m_AlignedPtr_1() { return &___m_AlignedPtr_1; }
	inline void set_m_AlignedPtr_1(void* value)
	{
		___m_AlignedPtr_1 = value;
	}

	inline static int32_t get_offset_of_m_Stride_2() { return static_cast<int32_t>(offsetof(ObiNativeList_1_t05A6C6A8DA09130C90EB95364D1840CBCDA790F9, ___m_Stride_2)); }
	inline int32_t get_m_Stride_2() const { return ___m_Stride_2; }
	inline int32_t* get_address_of_m_Stride_2() { return &___m_Stride_2; }
	inline void set_m_Stride_2(int32_t value)
	{
		___m_Stride_2 = value;
	}

	inline static int32_t get_offset_of_m_Capacity_3() { return static_cast<int32_t>(offsetof(ObiNativeList_1_t05A6C6A8DA09130C90EB95364D1840CBCDA790F9, ___m_Capacity_3)); }
	inline int32_t get_m_Capacity_3() const { return ___m_Capacity_3; }
	inline int32_t* get_address_of_m_Capacity_3() { return &___m_Capacity_3; }
	inline void set_m_Capacity_3(int32_t value)
	{
		___m_Capacity_3 = value;
	}

	inline static int32_t get_offset_of_m_Count_4() { return static_cast<int32_t>(offsetof(ObiNativeList_1_t05A6C6A8DA09130C90EB95364D1840CBCDA790F9, ___m_Count_4)); }
	inline int32_t get_m_Count_4() const { return ___m_Count_4; }
	inline int32_t* get_address_of_m_Count_4() { return &___m_Count_4; }
	inline void set_m_Count_4(int32_t value)
	{
		___m_Count_4 = value;
	}

	inline static int32_t get_offset_of_m_AsNativeArray_5() { return static_cast<int32_t>(offsetof(ObiNativeList_1_t05A6C6A8DA09130C90EB95364D1840CBCDA790F9, ___m_AsNativeArray_5)); }
	inline bool get_m_AsNativeArray_5() const { return ___m_AsNativeArray_5; }
	inline bool* get_address_of_m_AsNativeArray_5() { return &___m_AsNativeArray_5; }
	inline void set_m_AsNativeArray_5(bool value)
	{
		___m_AsNativeArray_5 = value;
	}

	inline static int32_t get_offset_of_m_ComputeBuffer_6() { return static_cast<int32_t>(offsetof(ObiNativeList_1_t05A6C6A8DA09130C90EB95364D1840CBCDA790F9, ___m_ComputeBuffer_6)); }
	inline ComputeBuffer_t795666617BDC98AB4033FDD4CE0304A22B7E6427 * get_m_ComputeBuffer_6() const { return ___m_ComputeBuffer_6; }
	inline ComputeBuffer_t795666617BDC98AB4033FDD4CE0304A22B7E6427 ** get_address_of_m_ComputeBuffer_6() { return &___m_ComputeBuffer_6; }
	inline void set_m_ComputeBuffer_6(ComputeBuffer_t795666617BDC98AB4033FDD4CE0304A22B7E6427 * value)
	{
		___m_ComputeBuffer_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_ComputeBuffer_6), (void*)value);
	}
};


// Obi.ObiNativeList`1<Obi.ColliderShape>
struct  ObiNativeList_1_t30AA57B53B1359362269E706BCAAE5664D2065C0  : public RuntimeObject
{
public:
	// T[] Obi.ObiNativeList`1::serializedContents
	ColliderShapeU5BU5D_t64D38EA60BE56F70B656C9C727A5F2530D2C2E24* ___serializedContents_0;
	// System.Void* Obi.ObiNativeList`1::m_AlignedPtr
	void* ___m_AlignedPtr_1;
	// System.Int32 Obi.ObiNativeList`1::m_Stride
	int32_t ___m_Stride_2;
	// System.Int32 Obi.ObiNativeList`1::m_Capacity
	int32_t ___m_Capacity_3;
	// System.Int32 Obi.ObiNativeList`1::m_Count
	int32_t ___m_Count_4;
	// System.Boolean Obi.ObiNativeList`1::m_AsNativeArray
	bool ___m_AsNativeArray_5;
	// UnityEngine.ComputeBuffer Obi.ObiNativeList`1::m_ComputeBuffer
	ComputeBuffer_t795666617BDC98AB4033FDD4CE0304A22B7E6427 * ___m_ComputeBuffer_6;

public:
	inline static int32_t get_offset_of_serializedContents_0() { return static_cast<int32_t>(offsetof(ObiNativeList_1_t30AA57B53B1359362269E706BCAAE5664D2065C0, ___serializedContents_0)); }
	inline ColliderShapeU5BU5D_t64D38EA60BE56F70B656C9C727A5F2530D2C2E24* get_serializedContents_0() const { return ___serializedContents_0; }
	inline ColliderShapeU5BU5D_t64D38EA60BE56F70B656C9C727A5F2530D2C2E24** get_address_of_serializedContents_0() { return &___serializedContents_0; }
	inline void set_serializedContents_0(ColliderShapeU5BU5D_t64D38EA60BE56F70B656C9C727A5F2530D2C2E24* value)
	{
		___serializedContents_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___serializedContents_0), (void*)value);
	}

	inline static int32_t get_offset_of_m_AlignedPtr_1() { return static_cast<int32_t>(offsetof(ObiNativeList_1_t30AA57B53B1359362269E706BCAAE5664D2065C0, ___m_AlignedPtr_1)); }
	inline void* get_m_AlignedPtr_1() const { return ___m_AlignedPtr_1; }
	inline void** get_address_of_m_AlignedPtr_1() { return &___m_AlignedPtr_1; }
	inline void set_m_AlignedPtr_1(void* value)
	{
		___m_AlignedPtr_1 = value;
	}

	inline static int32_t get_offset_of_m_Stride_2() { return static_cast<int32_t>(offsetof(ObiNativeList_1_t30AA57B53B1359362269E706BCAAE5664D2065C0, ___m_Stride_2)); }
	inline int32_t get_m_Stride_2() const { return ___m_Stride_2; }
	inline int32_t* get_address_of_m_Stride_2() { return &___m_Stride_2; }
	inline void set_m_Stride_2(int32_t value)
	{
		___m_Stride_2 = value;
	}

	inline static int32_t get_offset_of_m_Capacity_3() { return static_cast<int32_t>(offsetof(ObiNativeList_1_t30AA57B53B1359362269E706BCAAE5664D2065C0, ___m_Capacity_3)); }
	inline int32_t get_m_Capacity_3() const { return ___m_Capacity_3; }
	inline int32_t* get_address_of_m_Capacity_3() { return &___m_Capacity_3; }
	inline void set_m_Capacity_3(int32_t value)
	{
		___m_Capacity_3 = value;
	}

	inline static int32_t get_offset_of_m_Count_4() { return static_cast<int32_t>(offsetof(ObiNativeList_1_t30AA57B53B1359362269E706BCAAE5664D2065C0, ___m_Count_4)); }
	inline int32_t get_m_Count_4() const { return ___m_Count_4; }
	inline int32_t* get_address_of_m_Count_4() { return &___m_Count_4; }
	inline void set_m_Count_4(int32_t value)
	{
		___m_Count_4 = value;
	}

	inline static int32_t get_offset_of_m_AsNativeArray_5() { return static_cast<int32_t>(offsetof(ObiNativeList_1_t30AA57B53B1359362269E706BCAAE5664D2065C0, ___m_AsNativeArray_5)); }
	inline bool get_m_AsNativeArray_5() const { return ___m_AsNativeArray_5; }
	inline bool* get_address_of_m_AsNativeArray_5() { return &___m_AsNativeArray_5; }
	inline void set_m_AsNativeArray_5(bool value)
	{
		___m_AsNativeArray_5 = value;
	}

	inline static int32_t get_offset_of_m_ComputeBuffer_6() { return static_cast<int32_t>(offsetof(ObiNativeList_1_t30AA57B53B1359362269E706BCAAE5664D2065C0, ___m_ComputeBuffer_6)); }
	inline ComputeBuffer_t795666617BDC98AB4033FDD4CE0304A22B7E6427 * get_m_ComputeBuffer_6() const { return ___m_ComputeBuffer_6; }
	inline ComputeBuffer_t795666617BDC98AB4033FDD4CE0304A22B7E6427 ** get_address_of_m_ComputeBuffer_6() { return &___m_ComputeBuffer_6; }
	inline void set_m_ComputeBuffer_6(ComputeBuffer_t795666617BDC98AB4033FDD4CE0304A22B7E6427 * value)
	{
		___m_ComputeBuffer_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_ComputeBuffer_6), (void*)value);
	}
};


// Obi.ObiNativeList`1<Obi.CollisionMaterial>
struct  ObiNativeList_1_t0B4D5B3FAA024A9AC166F149B983A78AF3080145  : public RuntimeObject
{
public:
	// T[] Obi.ObiNativeList`1::serializedContents
	CollisionMaterialU5BU5D_t133A699CD2AAD3027174B99647BC3F4E6FF64B93* ___serializedContents_0;
	// System.Void* Obi.ObiNativeList`1::m_AlignedPtr
	void* ___m_AlignedPtr_1;
	// System.Int32 Obi.ObiNativeList`1::m_Stride
	int32_t ___m_Stride_2;
	// System.Int32 Obi.ObiNativeList`1::m_Capacity
	int32_t ___m_Capacity_3;
	// System.Int32 Obi.ObiNativeList`1::m_Count
	int32_t ___m_Count_4;
	// System.Boolean Obi.ObiNativeList`1::m_AsNativeArray
	bool ___m_AsNativeArray_5;
	// UnityEngine.ComputeBuffer Obi.ObiNativeList`1::m_ComputeBuffer
	ComputeBuffer_t795666617BDC98AB4033FDD4CE0304A22B7E6427 * ___m_ComputeBuffer_6;

public:
	inline static int32_t get_offset_of_serializedContents_0() { return static_cast<int32_t>(offsetof(ObiNativeList_1_t0B4D5B3FAA024A9AC166F149B983A78AF3080145, ___serializedContents_0)); }
	inline CollisionMaterialU5BU5D_t133A699CD2AAD3027174B99647BC3F4E6FF64B93* get_serializedContents_0() const { return ___serializedContents_0; }
	inline CollisionMaterialU5BU5D_t133A699CD2AAD3027174B99647BC3F4E6FF64B93** get_address_of_serializedContents_0() { return &___serializedContents_0; }
	inline void set_serializedContents_0(CollisionMaterialU5BU5D_t133A699CD2AAD3027174B99647BC3F4E6FF64B93* value)
	{
		___serializedContents_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___serializedContents_0), (void*)value);
	}

	inline static int32_t get_offset_of_m_AlignedPtr_1() { return static_cast<int32_t>(offsetof(ObiNativeList_1_t0B4D5B3FAA024A9AC166F149B983A78AF3080145, ___m_AlignedPtr_1)); }
	inline void* get_m_AlignedPtr_1() const { return ___m_AlignedPtr_1; }
	inline void** get_address_of_m_AlignedPtr_1() { return &___m_AlignedPtr_1; }
	inline void set_m_AlignedPtr_1(void* value)
	{
		___m_AlignedPtr_1 = value;
	}

	inline static int32_t get_offset_of_m_Stride_2() { return static_cast<int32_t>(offsetof(ObiNativeList_1_t0B4D5B3FAA024A9AC166F149B983A78AF3080145, ___m_Stride_2)); }
	inline int32_t get_m_Stride_2() const { return ___m_Stride_2; }
	inline int32_t* get_address_of_m_Stride_2() { return &___m_Stride_2; }
	inline void set_m_Stride_2(int32_t value)
	{
		___m_Stride_2 = value;
	}

	inline static int32_t get_offset_of_m_Capacity_3() { return static_cast<int32_t>(offsetof(ObiNativeList_1_t0B4D5B3FAA024A9AC166F149B983A78AF3080145, ___m_Capacity_3)); }
	inline int32_t get_m_Capacity_3() const { return ___m_Capacity_3; }
	inline int32_t* get_address_of_m_Capacity_3() { return &___m_Capacity_3; }
	inline void set_m_Capacity_3(int32_t value)
	{
		___m_Capacity_3 = value;
	}

	inline static int32_t get_offset_of_m_Count_4() { return static_cast<int32_t>(offsetof(ObiNativeList_1_t0B4D5B3FAA024A9AC166F149B983A78AF3080145, ___m_Count_4)); }
	inline int32_t get_m_Count_4() const { return ___m_Count_4; }
	inline int32_t* get_address_of_m_Count_4() { return &___m_Count_4; }
	inline void set_m_Count_4(int32_t value)
	{
		___m_Count_4 = value;
	}

	inline static int32_t get_offset_of_m_AsNativeArray_5() { return static_cast<int32_t>(offsetof(ObiNativeList_1_t0B4D5B3FAA024A9AC166F149B983A78AF3080145, ___m_AsNativeArray_5)); }
	inline bool get_m_AsNativeArray_5() const { return ___m_AsNativeArray_5; }
	inline bool* get_address_of_m_AsNativeArray_5() { return &___m_AsNativeArray_5; }
	inline void set_m_AsNativeArray_5(bool value)
	{
		___m_AsNativeArray_5 = value;
	}

	inline static int32_t get_offset_of_m_ComputeBuffer_6() { return static_cast<int32_t>(offsetof(ObiNativeList_1_t0B4D5B3FAA024A9AC166F149B983A78AF3080145, ___m_ComputeBuffer_6)); }
	inline ComputeBuffer_t795666617BDC98AB4033FDD4CE0304A22B7E6427 * get_m_ComputeBuffer_6() const { return ___m_ComputeBuffer_6; }
	inline ComputeBuffer_t795666617BDC98AB4033FDD4CE0304A22B7E6427 ** get_address_of_m_ComputeBuffer_6() { return &___m_ComputeBuffer_6; }
	inline void set_m_ComputeBuffer_6(ComputeBuffer_t795666617BDC98AB4033FDD4CE0304A22B7E6427 * value)
	{
		___m_ComputeBuffer_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_ComputeBuffer_6), (void*)value);
	}
};


// Obi.ObiNativeList`1<Obi.DFNode>
struct  ObiNativeList_1_t6ADADB0DAAC6C2F9D071750D398057641903EC70  : public RuntimeObject
{
public:
	// T[] Obi.ObiNativeList`1::serializedContents
	DFNodeU5BU5D_tD7926F1E8C4599C77D5BD2BB151B499CCE86E5D0* ___serializedContents_0;
	// System.Void* Obi.ObiNativeList`1::m_AlignedPtr
	void* ___m_AlignedPtr_1;
	// System.Int32 Obi.ObiNativeList`1::m_Stride
	int32_t ___m_Stride_2;
	// System.Int32 Obi.ObiNativeList`1::m_Capacity
	int32_t ___m_Capacity_3;
	// System.Int32 Obi.ObiNativeList`1::m_Count
	int32_t ___m_Count_4;
	// System.Boolean Obi.ObiNativeList`1::m_AsNativeArray
	bool ___m_AsNativeArray_5;
	// UnityEngine.ComputeBuffer Obi.ObiNativeList`1::m_ComputeBuffer
	ComputeBuffer_t795666617BDC98AB4033FDD4CE0304A22B7E6427 * ___m_ComputeBuffer_6;

public:
	inline static int32_t get_offset_of_serializedContents_0() { return static_cast<int32_t>(offsetof(ObiNativeList_1_t6ADADB0DAAC6C2F9D071750D398057641903EC70, ___serializedContents_0)); }
	inline DFNodeU5BU5D_tD7926F1E8C4599C77D5BD2BB151B499CCE86E5D0* get_serializedContents_0() const { return ___serializedContents_0; }
	inline DFNodeU5BU5D_tD7926F1E8C4599C77D5BD2BB151B499CCE86E5D0** get_address_of_serializedContents_0() { return &___serializedContents_0; }
	inline void set_serializedContents_0(DFNodeU5BU5D_tD7926F1E8C4599C77D5BD2BB151B499CCE86E5D0* value)
	{
		___serializedContents_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___serializedContents_0), (void*)value);
	}

	inline static int32_t get_offset_of_m_AlignedPtr_1() { return static_cast<int32_t>(offsetof(ObiNativeList_1_t6ADADB0DAAC6C2F9D071750D398057641903EC70, ___m_AlignedPtr_1)); }
	inline void* get_m_AlignedPtr_1() const { return ___m_AlignedPtr_1; }
	inline void** get_address_of_m_AlignedPtr_1() { return &___m_AlignedPtr_1; }
	inline void set_m_AlignedPtr_1(void* value)
	{
		___m_AlignedPtr_1 = value;
	}

	inline static int32_t get_offset_of_m_Stride_2() { return static_cast<int32_t>(offsetof(ObiNativeList_1_t6ADADB0DAAC6C2F9D071750D398057641903EC70, ___m_Stride_2)); }
	inline int32_t get_m_Stride_2() const { return ___m_Stride_2; }
	inline int32_t* get_address_of_m_Stride_2() { return &___m_Stride_2; }
	inline void set_m_Stride_2(int32_t value)
	{
		___m_Stride_2 = value;
	}

	inline static int32_t get_offset_of_m_Capacity_3() { return static_cast<int32_t>(offsetof(ObiNativeList_1_t6ADADB0DAAC6C2F9D071750D398057641903EC70, ___m_Capacity_3)); }
	inline int32_t get_m_Capacity_3() const { return ___m_Capacity_3; }
	inline int32_t* get_address_of_m_Capacity_3() { return &___m_Capacity_3; }
	inline void set_m_Capacity_3(int32_t value)
	{
		___m_Capacity_3 = value;
	}

	inline static int32_t get_offset_of_m_Count_4() { return static_cast<int32_t>(offsetof(ObiNativeList_1_t6ADADB0DAAC6C2F9D071750D398057641903EC70, ___m_Count_4)); }
	inline int32_t get_m_Count_4() const { return ___m_Count_4; }
	inline int32_t* get_address_of_m_Count_4() { return &___m_Count_4; }
	inline void set_m_Count_4(int32_t value)
	{
		___m_Count_4 = value;
	}

	inline static int32_t get_offset_of_m_AsNativeArray_5() { return static_cast<int32_t>(offsetof(ObiNativeList_1_t6ADADB0DAAC6C2F9D071750D398057641903EC70, ___m_AsNativeArray_5)); }
	inline bool get_m_AsNativeArray_5() const { return ___m_AsNativeArray_5; }
	inline bool* get_address_of_m_AsNativeArray_5() { return &___m_AsNativeArray_5; }
	inline void set_m_AsNativeArray_5(bool value)
	{
		___m_AsNativeArray_5 = value;
	}

	inline static int32_t get_offset_of_m_ComputeBuffer_6() { return static_cast<int32_t>(offsetof(ObiNativeList_1_t6ADADB0DAAC6C2F9D071750D398057641903EC70, ___m_ComputeBuffer_6)); }
	inline ComputeBuffer_t795666617BDC98AB4033FDD4CE0304A22B7E6427 * get_m_ComputeBuffer_6() const { return ___m_ComputeBuffer_6; }
	inline ComputeBuffer_t795666617BDC98AB4033FDD4CE0304A22B7E6427 ** get_address_of_m_ComputeBuffer_6() { return &___m_ComputeBuffer_6; }
	inline void set_m_ComputeBuffer_6(ComputeBuffer_t795666617BDC98AB4033FDD4CE0304A22B7E6427 * value)
	{
		___m_ComputeBuffer_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_ComputeBuffer_6), (void*)value);
	}
};


// Obi.ObiNativeList`1<Obi.DistanceFieldHeader>
struct  ObiNativeList_1_t018D553481251B27F7FE696F676040DCB6C3F0EA  : public RuntimeObject
{
public:
	// T[] Obi.ObiNativeList`1::serializedContents
	DistanceFieldHeaderU5BU5D_t0593F81444848E01D41886C6C8DDCDB5E63DD2EA* ___serializedContents_0;
	// System.Void* Obi.ObiNativeList`1::m_AlignedPtr
	void* ___m_AlignedPtr_1;
	// System.Int32 Obi.ObiNativeList`1::m_Stride
	int32_t ___m_Stride_2;
	// System.Int32 Obi.ObiNativeList`1::m_Capacity
	int32_t ___m_Capacity_3;
	// System.Int32 Obi.ObiNativeList`1::m_Count
	int32_t ___m_Count_4;
	// System.Boolean Obi.ObiNativeList`1::m_AsNativeArray
	bool ___m_AsNativeArray_5;
	// UnityEngine.ComputeBuffer Obi.ObiNativeList`1::m_ComputeBuffer
	ComputeBuffer_t795666617BDC98AB4033FDD4CE0304A22B7E6427 * ___m_ComputeBuffer_6;

public:
	inline static int32_t get_offset_of_serializedContents_0() { return static_cast<int32_t>(offsetof(ObiNativeList_1_t018D553481251B27F7FE696F676040DCB6C3F0EA, ___serializedContents_0)); }
	inline DistanceFieldHeaderU5BU5D_t0593F81444848E01D41886C6C8DDCDB5E63DD2EA* get_serializedContents_0() const { return ___serializedContents_0; }
	inline DistanceFieldHeaderU5BU5D_t0593F81444848E01D41886C6C8DDCDB5E63DD2EA** get_address_of_serializedContents_0() { return &___serializedContents_0; }
	inline void set_serializedContents_0(DistanceFieldHeaderU5BU5D_t0593F81444848E01D41886C6C8DDCDB5E63DD2EA* value)
	{
		___serializedContents_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___serializedContents_0), (void*)value);
	}

	inline static int32_t get_offset_of_m_AlignedPtr_1() { return static_cast<int32_t>(offsetof(ObiNativeList_1_t018D553481251B27F7FE696F676040DCB6C3F0EA, ___m_AlignedPtr_1)); }
	inline void* get_m_AlignedPtr_1() const { return ___m_AlignedPtr_1; }
	inline void** get_address_of_m_AlignedPtr_1() { return &___m_AlignedPtr_1; }
	inline void set_m_AlignedPtr_1(void* value)
	{
		___m_AlignedPtr_1 = value;
	}

	inline static int32_t get_offset_of_m_Stride_2() { return static_cast<int32_t>(offsetof(ObiNativeList_1_t018D553481251B27F7FE696F676040DCB6C3F0EA, ___m_Stride_2)); }
	inline int32_t get_m_Stride_2() const { return ___m_Stride_2; }
	inline int32_t* get_address_of_m_Stride_2() { return &___m_Stride_2; }
	inline void set_m_Stride_2(int32_t value)
	{
		___m_Stride_2 = value;
	}

	inline static int32_t get_offset_of_m_Capacity_3() { return static_cast<int32_t>(offsetof(ObiNativeList_1_t018D553481251B27F7FE696F676040DCB6C3F0EA, ___m_Capacity_3)); }
	inline int32_t get_m_Capacity_3() const { return ___m_Capacity_3; }
	inline int32_t* get_address_of_m_Capacity_3() { return &___m_Capacity_3; }
	inline void set_m_Capacity_3(int32_t value)
	{
		___m_Capacity_3 = value;
	}

	inline static int32_t get_offset_of_m_Count_4() { return static_cast<int32_t>(offsetof(ObiNativeList_1_t018D553481251B27F7FE696F676040DCB6C3F0EA, ___m_Count_4)); }
	inline int32_t get_m_Count_4() const { return ___m_Count_4; }
	inline int32_t* get_address_of_m_Count_4() { return &___m_Count_4; }
	inline void set_m_Count_4(int32_t value)
	{
		___m_Count_4 = value;
	}

	inline static int32_t get_offset_of_m_AsNativeArray_5() { return static_cast<int32_t>(offsetof(ObiNativeList_1_t018D553481251B27F7FE696F676040DCB6C3F0EA, ___m_AsNativeArray_5)); }
	inline bool get_m_AsNativeArray_5() const { return ___m_AsNativeArray_5; }
	inline bool* get_address_of_m_AsNativeArray_5() { return &___m_AsNativeArray_5; }
	inline void set_m_AsNativeArray_5(bool value)
	{
		___m_AsNativeArray_5 = value;
	}

	inline static int32_t get_offset_of_m_ComputeBuffer_6() { return static_cast<int32_t>(offsetof(ObiNativeList_1_t018D553481251B27F7FE696F676040DCB6C3F0EA, ___m_ComputeBuffer_6)); }
	inline ComputeBuffer_t795666617BDC98AB4033FDD4CE0304A22B7E6427 * get_m_ComputeBuffer_6() const { return ___m_ComputeBuffer_6; }
	inline ComputeBuffer_t795666617BDC98AB4033FDD4CE0304A22B7E6427 ** get_address_of_m_ComputeBuffer_6() { return &___m_ComputeBuffer_6; }
	inline void set_m_ComputeBuffer_6(ComputeBuffer_t795666617BDC98AB4033FDD4CE0304A22B7E6427 * value)
	{
		___m_ComputeBuffer_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_ComputeBuffer_6), (void*)value);
	}
};


// Obi.ObiNativeList`1<Obi.Edge>
struct  ObiNativeList_1_t96FABDF83D294D0ABBFEFAE74DEFCD1CF2243AF7  : public RuntimeObject
{
public:
	// T[] Obi.ObiNativeList`1::serializedContents
	EdgeU5BU5D_tB4E38D11113C524DF358EB19FA6D4CB4C3E914B8* ___serializedContents_0;
	// System.Void* Obi.ObiNativeList`1::m_AlignedPtr
	void* ___m_AlignedPtr_1;
	// System.Int32 Obi.ObiNativeList`1::m_Stride
	int32_t ___m_Stride_2;
	// System.Int32 Obi.ObiNativeList`1::m_Capacity
	int32_t ___m_Capacity_3;
	// System.Int32 Obi.ObiNativeList`1::m_Count
	int32_t ___m_Count_4;
	// System.Boolean Obi.ObiNativeList`1::m_AsNativeArray
	bool ___m_AsNativeArray_5;
	// UnityEngine.ComputeBuffer Obi.ObiNativeList`1::m_ComputeBuffer
	ComputeBuffer_t795666617BDC98AB4033FDD4CE0304A22B7E6427 * ___m_ComputeBuffer_6;

public:
	inline static int32_t get_offset_of_serializedContents_0() { return static_cast<int32_t>(offsetof(ObiNativeList_1_t96FABDF83D294D0ABBFEFAE74DEFCD1CF2243AF7, ___serializedContents_0)); }
	inline EdgeU5BU5D_tB4E38D11113C524DF358EB19FA6D4CB4C3E914B8* get_serializedContents_0() const { return ___serializedContents_0; }
	inline EdgeU5BU5D_tB4E38D11113C524DF358EB19FA6D4CB4C3E914B8** get_address_of_serializedContents_0() { return &___serializedContents_0; }
	inline void set_serializedContents_0(EdgeU5BU5D_tB4E38D11113C524DF358EB19FA6D4CB4C3E914B8* value)
	{
		___serializedContents_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___serializedContents_0), (void*)value);
	}

	inline static int32_t get_offset_of_m_AlignedPtr_1() { return static_cast<int32_t>(offsetof(ObiNativeList_1_t96FABDF83D294D0ABBFEFAE74DEFCD1CF2243AF7, ___m_AlignedPtr_1)); }
	inline void* get_m_AlignedPtr_1() const { return ___m_AlignedPtr_1; }
	inline void** get_address_of_m_AlignedPtr_1() { return &___m_AlignedPtr_1; }
	inline void set_m_AlignedPtr_1(void* value)
	{
		___m_AlignedPtr_1 = value;
	}

	inline static int32_t get_offset_of_m_Stride_2() { return static_cast<int32_t>(offsetof(ObiNativeList_1_t96FABDF83D294D0ABBFEFAE74DEFCD1CF2243AF7, ___m_Stride_2)); }
	inline int32_t get_m_Stride_2() const { return ___m_Stride_2; }
	inline int32_t* get_address_of_m_Stride_2() { return &___m_Stride_2; }
	inline void set_m_Stride_2(int32_t value)
	{
		___m_Stride_2 = value;
	}

	inline static int32_t get_offset_of_m_Capacity_3() { return static_cast<int32_t>(offsetof(ObiNativeList_1_t96FABDF83D294D0ABBFEFAE74DEFCD1CF2243AF7, ___m_Capacity_3)); }
	inline int32_t get_m_Capacity_3() const { return ___m_Capacity_3; }
	inline int32_t* get_address_of_m_Capacity_3() { return &___m_Capacity_3; }
	inline void set_m_Capacity_3(int32_t value)
	{
		___m_Capacity_3 = value;
	}

	inline static int32_t get_offset_of_m_Count_4() { return static_cast<int32_t>(offsetof(ObiNativeList_1_t96FABDF83D294D0ABBFEFAE74DEFCD1CF2243AF7, ___m_Count_4)); }
	inline int32_t get_m_Count_4() const { return ___m_Count_4; }
	inline int32_t* get_address_of_m_Count_4() { return &___m_Count_4; }
	inline void set_m_Count_4(int32_t value)
	{
		___m_Count_4 = value;
	}

	inline static int32_t get_offset_of_m_AsNativeArray_5() { return static_cast<int32_t>(offsetof(ObiNativeList_1_t96FABDF83D294D0ABBFEFAE74DEFCD1CF2243AF7, ___m_AsNativeArray_5)); }
	inline bool get_m_AsNativeArray_5() const { return ___m_AsNativeArray_5; }
	inline bool* get_address_of_m_AsNativeArray_5() { return &___m_AsNativeArray_5; }
	inline void set_m_AsNativeArray_5(bool value)
	{
		___m_AsNativeArray_5 = value;
	}

	inline static int32_t get_offset_of_m_ComputeBuffer_6() { return static_cast<int32_t>(offsetof(ObiNativeList_1_t96FABDF83D294D0ABBFEFAE74DEFCD1CF2243AF7, ___m_ComputeBuffer_6)); }
	inline ComputeBuffer_t795666617BDC98AB4033FDD4CE0304A22B7E6427 * get_m_ComputeBuffer_6() const { return ___m_ComputeBuffer_6; }
	inline ComputeBuffer_t795666617BDC98AB4033FDD4CE0304A22B7E6427 ** get_address_of_m_ComputeBuffer_6() { return &___m_ComputeBuffer_6; }
	inline void set_m_ComputeBuffer_6(ComputeBuffer_t795666617BDC98AB4033FDD4CE0304A22B7E6427 * value)
	{
		___m_ComputeBuffer_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_ComputeBuffer_6), (void*)value);
	}
};


// Obi.ObiNativeList`1<Obi.EdgeMeshHeader>
struct  ObiNativeList_1_t82AAD6F0A45EA387F6031B345E681AE0DADF910C  : public RuntimeObject
{
public:
	// T[] Obi.ObiNativeList`1::serializedContents
	EdgeMeshHeaderU5BU5D_t46887E858B4028955E4ADCCF2838C854CFE81B81* ___serializedContents_0;
	// System.Void* Obi.ObiNativeList`1::m_AlignedPtr
	void* ___m_AlignedPtr_1;
	// System.Int32 Obi.ObiNativeList`1::m_Stride
	int32_t ___m_Stride_2;
	// System.Int32 Obi.ObiNativeList`1::m_Capacity
	int32_t ___m_Capacity_3;
	// System.Int32 Obi.ObiNativeList`1::m_Count
	int32_t ___m_Count_4;
	// System.Boolean Obi.ObiNativeList`1::m_AsNativeArray
	bool ___m_AsNativeArray_5;
	// UnityEngine.ComputeBuffer Obi.ObiNativeList`1::m_ComputeBuffer
	ComputeBuffer_t795666617BDC98AB4033FDD4CE0304A22B7E6427 * ___m_ComputeBuffer_6;

public:
	inline static int32_t get_offset_of_serializedContents_0() { return static_cast<int32_t>(offsetof(ObiNativeList_1_t82AAD6F0A45EA387F6031B345E681AE0DADF910C, ___serializedContents_0)); }
	inline EdgeMeshHeaderU5BU5D_t46887E858B4028955E4ADCCF2838C854CFE81B81* get_serializedContents_0() const { return ___serializedContents_0; }
	inline EdgeMeshHeaderU5BU5D_t46887E858B4028955E4ADCCF2838C854CFE81B81** get_address_of_serializedContents_0() { return &___serializedContents_0; }
	inline void set_serializedContents_0(EdgeMeshHeaderU5BU5D_t46887E858B4028955E4ADCCF2838C854CFE81B81* value)
	{
		___serializedContents_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___serializedContents_0), (void*)value);
	}

	inline static int32_t get_offset_of_m_AlignedPtr_1() { return static_cast<int32_t>(offsetof(ObiNativeList_1_t82AAD6F0A45EA387F6031B345E681AE0DADF910C, ___m_AlignedPtr_1)); }
	inline void* get_m_AlignedPtr_1() const { return ___m_AlignedPtr_1; }
	inline void** get_address_of_m_AlignedPtr_1() { return &___m_AlignedPtr_1; }
	inline void set_m_AlignedPtr_1(void* value)
	{
		___m_AlignedPtr_1 = value;
	}

	inline static int32_t get_offset_of_m_Stride_2() { return static_cast<int32_t>(offsetof(ObiNativeList_1_t82AAD6F0A45EA387F6031B345E681AE0DADF910C, ___m_Stride_2)); }
	inline int32_t get_m_Stride_2() const { return ___m_Stride_2; }
	inline int32_t* get_address_of_m_Stride_2() { return &___m_Stride_2; }
	inline void set_m_Stride_2(int32_t value)
	{
		___m_Stride_2 = value;
	}

	inline static int32_t get_offset_of_m_Capacity_3() { return static_cast<int32_t>(offsetof(ObiNativeList_1_t82AAD6F0A45EA387F6031B345E681AE0DADF910C, ___m_Capacity_3)); }
	inline int32_t get_m_Capacity_3() const { return ___m_Capacity_3; }
	inline int32_t* get_address_of_m_Capacity_3() { return &___m_Capacity_3; }
	inline void set_m_Capacity_3(int32_t value)
	{
		___m_Capacity_3 = value;
	}

	inline static int32_t get_offset_of_m_Count_4() { return static_cast<int32_t>(offsetof(ObiNativeList_1_t82AAD6F0A45EA387F6031B345E681AE0DADF910C, ___m_Count_4)); }
	inline int32_t get_m_Count_4() const { return ___m_Count_4; }
	inline int32_t* get_address_of_m_Count_4() { return &___m_Count_4; }
	inline void set_m_Count_4(int32_t value)
	{
		___m_Count_4 = value;
	}

	inline static int32_t get_offset_of_m_AsNativeArray_5() { return static_cast<int32_t>(offsetof(ObiNativeList_1_t82AAD6F0A45EA387F6031B345E681AE0DADF910C, ___m_AsNativeArray_5)); }
	inline bool get_m_AsNativeArray_5() const { return ___m_AsNativeArray_5; }
	inline bool* get_address_of_m_AsNativeArray_5() { return &___m_AsNativeArray_5; }
	inline void set_m_AsNativeArray_5(bool value)
	{
		___m_AsNativeArray_5 = value;
	}

	inline static int32_t get_offset_of_m_ComputeBuffer_6() { return static_cast<int32_t>(offsetof(ObiNativeList_1_t82AAD6F0A45EA387F6031B345E681AE0DADF910C, ___m_ComputeBuffer_6)); }
	inline ComputeBuffer_t795666617BDC98AB4033FDD4CE0304A22B7E6427 * get_m_ComputeBuffer_6() const { return ___m_ComputeBuffer_6; }
	inline ComputeBuffer_t795666617BDC98AB4033FDD4CE0304A22B7E6427 ** get_address_of_m_ComputeBuffer_6() { return &___m_ComputeBuffer_6; }
	inline void set_m_ComputeBuffer_6(ComputeBuffer_t795666617BDC98AB4033FDD4CE0304A22B7E6427 * value)
	{
		___m_ComputeBuffer_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_ComputeBuffer_6), (void*)value);
	}
};


// Obi.ObiNativeList`1<Obi.HeightFieldHeader>
struct  ObiNativeList_1_t2A4871B78EE973A2C9E2473CBF7A597169D426CB  : public RuntimeObject
{
public:
	// T[] Obi.ObiNativeList`1::serializedContents
	HeightFieldHeaderU5BU5D_t45DA5AA034FA4400953925EDC54523CCDAC9A6AE* ___serializedContents_0;
	// System.Void* Obi.ObiNativeList`1::m_AlignedPtr
	void* ___m_AlignedPtr_1;
	// System.Int32 Obi.ObiNativeList`1::m_Stride
	int32_t ___m_Stride_2;
	// System.Int32 Obi.ObiNativeList`1::m_Capacity
	int32_t ___m_Capacity_3;
	// System.Int32 Obi.ObiNativeList`1::m_Count
	int32_t ___m_Count_4;
	// System.Boolean Obi.ObiNativeList`1::m_AsNativeArray
	bool ___m_AsNativeArray_5;
	// UnityEngine.ComputeBuffer Obi.ObiNativeList`1::m_ComputeBuffer
	ComputeBuffer_t795666617BDC98AB4033FDD4CE0304A22B7E6427 * ___m_ComputeBuffer_6;

public:
	inline static int32_t get_offset_of_serializedContents_0() { return static_cast<int32_t>(offsetof(ObiNativeList_1_t2A4871B78EE973A2C9E2473CBF7A597169D426CB, ___serializedContents_0)); }
	inline HeightFieldHeaderU5BU5D_t45DA5AA034FA4400953925EDC54523CCDAC9A6AE* get_serializedContents_0() const { return ___serializedContents_0; }
	inline HeightFieldHeaderU5BU5D_t45DA5AA034FA4400953925EDC54523CCDAC9A6AE** get_address_of_serializedContents_0() { return &___serializedContents_0; }
	inline void set_serializedContents_0(HeightFieldHeaderU5BU5D_t45DA5AA034FA4400953925EDC54523CCDAC9A6AE* value)
	{
		___serializedContents_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___serializedContents_0), (void*)value);
	}

	inline static int32_t get_offset_of_m_AlignedPtr_1() { return static_cast<int32_t>(offsetof(ObiNativeList_1_t2A4871B78EE973A2C9E2473CBF7A597169D426CB, ___m_AlignedPtr_1)); }
	inline void* get_m_AlignedPtr_1() const { return ___m_AlignedPtr_1; }
	inline void** get_address_of_m_AlignedPtr_1() { return &___m_AlignedPtr_1; }
	inline void set_m_AlignedPtr_1(void* value)
	{
		___m_AlignedPtr_1 = value;
	}

	inline static int32_t get_offset_of_m_Stride_2() { return static_cast<int32_t>(offsetof(ObiNativeList_1_t2A4871B78EE973A2C9E2473CBF7A597169D426CB, ___m_Stride_2)); }
	inline int32_t get_m_Stride_2() const { return ___m_Stride_2; }
	inline int32_t* get_address_of_m_Stride_2() { return &___m_Stride_2; }
	inline void set_m_Stride_2(int32_t value)
	{
		___m_Stride_2 = value;
	}

	inline static int32_t get_offset_of_m_Capacity_3() { return static_cast<int32_t>(offsetof(ObiNativeList_1_t2A4871B78EE973A2C9E2473CBF7A597169D426CB, ___m_Capacity_3)); }
	inline int32_t get_m_Capacity_3() const { return ___m_Capacity_3; }
	inline int32_t* get_address_of_m_Capacity_3() { return &___m_Capacity_3; }
	inline void set_m_Capacity_3(int32_t value)
	{
		___m_Capacity_3 = value;
	}

	inline static int32_t get_offset_of_m_Count_4() { return static_cast<int32_t>(offsetof(ObiNativeList_1_t2A4871B78EE973A2C9E2473CBF7A597169D426CB, ___m_Count_4)); }
	inline int32_t get_m_Count_4() const { return ___m_Count_4; }
	inline int32_t* get_address_of_m_Count_4() { return &___m_Count_4; }
	inline void set_m_Count_4(int32_t value)
	{
		___m_Count_4 = value;
	}

	inline static int32_t get_offset_of_m_AsNativeArray_5() { return static_cast<int32_t>(offsetof(ObiNativeList_1_t2A4871B78EE973A2C9E2473CBF7A597169D426CB, ___m_AsNativeArray_5)); }
	inline bool get_m_AsNativeArray_5() const { return ___m_AsNativeArray_5; }
	inline bool* get_address_of_m_AsNativeArray_5() { return &___m_AsNativeArray_5; }
	inline void set_m_AsNativeArray_5(bool value)
	{
		___m_AsNativeArray_5 = value;
	}

	inline static int32_t get_offset_of_m_ComputeBuffer_6() { return static_cast<int32_t>(offsetof(ObiNativeList_1_t2A4871B78EE973A2C9E2473CBF7A597169D426CB, ___m_ComputeBuffer_6)); }
	inline ComputeBuffer_t795666617BDC98AB4033FDD4CE0304A22B7E6427 * get_m_ComputeBuffer_6() const { return ___m_ComputeBuffer_6; }
	inline ComputeBuffer_t795666617BDC98AB4033FDD4CE0304A22B7E6427 ** get_address_of_m_ComputeBuffer_6() { return &___m_ComputeBuffer_6; }
	inline void set_m_ComputeBuffer_6(ComputeBuffer_t795666617BDC98AB4033FDD4CE0304A22B7E6427 * value)
	{
		___m_ComputeBuffer_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_ComputeBuffer_6), (void*)value);
	}
};


// Obi.ObiNativeList`1<Obi.QueryResult>
struct  ObiNativeList_1_tE4E9A225B4175E5D04A1C69103C9CB0962541089  : public RuntimeObject
{
public:
	// T[] Obi.ObiNativeList`1::serializedContents
	QueryResultU5BU5D_t7296831A0BF97D2990464E82C176858D4287CAAA* ___serializedContents_0;
	// System.Void* Obi.ObiNativeList`1::m_AlignedPtr
	void* ___m_AlignedPtr_1;
	// System.Int32 Obi.ObiNativeList`1::m_Stride
	int32_t ___m_Stride_2;
	// System.Int32 Obi.ObiNativeList`1::m_Capacity
	int32_t ___m_Capacity_3;
	// System.Int32 Obi.ObiNativeList`1::m_Count
	int32_t ___m_Count_4;
	// System.Boolean Obi.ObiNativeList`1::m_AsNativeArray
	bool ___m_AsNativeArray_5;
	// UnityEngine.ComputeBuffer Obi.ObiNativeList`1::m_ComputeBuffer
	ComputeBuffer_t795666617BDC98AB4033FDD4CE0304A22B7E6427 * ___m_ComputeBuffer_6;

public:
	inline static int32_t get_offset_of_serializedContents_0() { return static_cast<int32_t>(offsetof(ObiNativeList_1_tE4E9A225B4175E5D04A1C69103C9CB0962541089, ___serializedContents_0)); }
	inline QueryResultU5BU5D_t7296831A0BF97D2990464E82C176858D4287CAAA* get_serializedContents_0() const { return ___serializedContents_0; }
	inline QueryResultU5BU5D_t7296831A0BF97D2990464E82C176858D4287CAAA** get_address_of_serializedContents_0() { return &___serializedContents_0; }
	inline void set_serializedContents_0(QueryResultU5BU5D_t7296831A0BF97D2990464E82C176858D4287CAAA* value)
	{
		___serializedContents_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___serializedContents_0), (void*)value);
	}

	inline static int32_t get_offset_of_m_AlignedPtr_1() { return static_cast<int32_t>(offsetof(ObiNativeList_1_tE4E9A225B4175E5D04A1C69103C9CB0962541089, ___m_AlignedPtr_1)); }
	inline void* get_m_AlignedPtr_1() const { return ___m_AlignedPtr_1; }
	inline void** get_address_of_m_AlignedPtr_1() { return &___m_AlignedPtr_1; }
	inline void set_m_AlignedPtr_1(void* value)
	{
		___m_AlignedPtr_1 = value;
	}

	inline static int32_t get_offset_of_m_Stride_2() { return static_cast<int32_t>(offsetof(ObiNativeList_1_tE4E9A225B4175E5D04A1C69103C9CB0962541089, ___m_Stride_2)); }
	inline int32_t get_m_Stride_2() const { return ___m_Stride_2; }
	inline int32_t* get_address_of_m_Stride_2() { return &___m_Stride_2; }
	inline void set_m_Stride_2(int32_t value)
	{
		___m_Stride_2 = value;
	}

	inline static int32_t get_offset_of_m_Capacity_3() { return static_cast<int32_t>(offsetof(ObiNativeList_1_tE4E9A225B4175E5D04A1C69103C9CB0962541089, ___m_Capacity_3)); }
	inline int32_t get_m_Capacity_3() const { return ___m_Capacity_3; }
	inline int32_t* get_address_of_m_Capacity_3() { return &___m_Capacity_3; }
	inline void set_m_Capacity_3(int32_t value)
	{
		___m_Capacity_3 = value;
	}

	inline static int32_t get_offset_of_m_Count_4() { return static_cast<int32_t>(offsetof(ObiNativeList_1_tE4E9A225B4175E5D04A1C69103C9CB0962541089, ___m_Count_4)); }
	inline int32_t get_m_Count_4() const { return ___m_Count_4; }
	inline int32_t* get_address_of_m_Count_4() { return &___m_Count_4; }
	inline void set_m_Count_4(int32_t value)
	{
		___m_Count_4 = value;
	}

	inline static int32_t get_offset_of_m_AsNativeArray_5() { return static_cast<int32_t>(offsetof(ObiNativeList_1_tE4E9A225B4175E5D04A1C69103C9CB0962541089, ___m_AsNativeArray_5)); }
	inline bool get_m_AsNativeArray_5() const { return ___m_AsNativeArray_5; }
	inline bool* get_address_of_m_AsNativeArray_5() { return &___m_AsNativeArray_5; }
	inline void set_m_AsNativeArray_5(bool value)
	{
		___m_AsNativeArray_5 = value;
	}

	inline static int32_t get_offset_of_m_ComputeBuffer_6() { return static_cast<int32_t>(offsetof(ObiNativeList_1_tE4E9A225B4175E5D04A1C69103C9CB0962541089, ___m_ComputeBuffer_6)); }
	inline ComputeBuffer_t795666617BDC98AB4033FDD4CE0304A22B7E6427 * get_m_ComputeBuffer_6() const { return ___m_ComputeBuffer_6; }
	inline ComputeBuffer_t795666617BDC98AB4033FDD4CE0304A22B7E6427 ** get_address_of_m_ComputeBuffer_6() { return &___m_ComputeBuffer_6; }
	inline void set_m_ComputeBuffer_6(ComputeBuffer_t795666617BDC98AB4033FDD4CE0304A22B7E6427 * value)
	{
		___m_ComputeBuffer_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_ComputeBuffer_6), (void*)value);
	}
};


// Obi.ObiNativeList`1<Obi.QueryShape>
struct  ObiNativeList_1_tD298AB56D582AAA797727DF401F4FAC39E7DCAEF  : public RuntimeObject
{
public:
	// T[] Obi.ObiNativeList`1::serializedContents
	QueryShapeU5BU5D_t423AECB189811BF7BFCA5C6D7786795BDBC4BFC5* ___serializedContents_0;
	// System.Void* Obi.ObiNativeList`1::m_AlignedPtr
	void* ___m_AlignedPtr_1;
	// System.Int32 Obi.ObiNativeList`1::m_Stride
	int32_t ___m_Stride_2;
	// System.Int32 Obi.ObiNativeList`1::m_Capacity
	int32_t ___m_Capacity_3;
	// System.Int32 Obi.ObiNativeList`1::m_Count
	int32_t ___m_Count_4;
	// System.Boolean Obi.ObiNativeList`1::m_AsNativeArray
	bool ___m_AsNativeArray_5;
	// UnityEngine.ComputeBuffer Obi.ObiNativeList`1::m_ComputeBuffer
	ComputeBuffer_t795666617BDC98AB4033FDD4CE0304A22B7E6427 * ___m_ComputeBuffer_6;

public:
	inline static int32_t get_offset_of_serializedContents_0() { return static_cast<int32_t>(offsetof(ObiNativeList_1_tD298AB56D582AAA797727DF401F4FAC39E7DCAEF, ___serializedContents_0)); }
	inline QueryShapeU5BU5D_t423AECB189811BF7BFCA5C6D7786795BDBC4BFC5* get_serializedContents_0() const { return ___serializedContents_0; }
	inline QueryShapeU5BU5D_t423AECB189811BF7BFCA5C6D7786795BDBC4BFC5** get_address_of_serializedContents_0() { return &___serializedContents_0; }
	inline void set_serializedContents_0(QueryShapeU5BU5D_t423AECB189811BF7BFCA5C6D7786795BDBC4BFC5* value)
	{
		___serializedContents_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___serializedContents_0), (void*)value);
	}

	inline static int32_t get_offset_of_m_AlignedPtr_1() { return static_cast<int32_t>(offsetof(ObiNativeList_1_tD298AB56D582AAA797727DF401F4FAC39E7DCAEF, ___m_AlignedPtr_1)); }
	inline void* get_m_AlignedPtr_1() const { return ___m_AlignedPtr_1; }
	inline void** get_address_of_m_AlignedPtr_1() { return &___m_AlignedPtr_1; }
	inline void set_m_AlignedPtr_1(void* value)
	{
		___m_AlignedPtr_1 = value;
	}

	inline static int32_t get_offset_of_m_Stride_2() { return static_cast<int32_t>(offsetof(ObiNativeList_1_tD298AB56D582AAA797727DF401F4FAC39E7DCAEF, ___m_Stride_2)); }
	inline int32_t get_m_Stride_2() const { return ___m_Stride_2; }
	inline int32_t* get_address_of_m_Stride_2() { return &___m_Stride_2; }
	inline void set_m_Stride_2(int32_t value)
	{
		___m_Stride_2 = value;
	}

	inline static int32_t get_offset_of_m_Capacity_3() { return static_cast<int32_t>(offsetof(ObiNativeList_1_tD298AB56D582AAA797727DF401F4FAC39E7DCAEF, ___m_Capacity_3)); }
	inline int32_t get_m_Capacity_3() const { return ___m_Capacity_3; }
	inline int32_t* get_address_of_m_Capacity_3() { return &___m_Capacity_3; }
	inline void set_m_Capacity_3(int32_t value)
	{
		___m_Capacity_3 = value;
	}

	inline static int32_t get_offset_of_m_Count_4() { return static_cast<int32_t>(offsetof(ObiNativeList_1_tD298AB56D582AAA797727DF401F4FAC39E7DCAEF, ___m_Count_4)); }
	inline int32_t get_m_Count_4() const { return ___m_Count_4; }
	inline int32_t* get_address_of_m_Count_4() { return &___m_Count_4; }
	inline void set_m_Count_4(int32_t value)
	{
		___m_Count_4 = value;
	}

	inline static int32_t get_offset_of_m_AsNativeArray_5() { return static_cast<int32_t>(offsetof(ObiNativeList_1_tD298AB56D582AAA797727DF401F4FAC39E7DCAEF, ___m_AsNativeArray_5)); }
	inline bool get_m_AsNativeArray_5() const { return ___m_AsNativeArray_5; }
	inline bool* get_address_of_m_AsNativeArray_5() { return &___m_AsNativeArray_5; }
	inline void set_m_AsNativeArray_5(bool value)
	{
		___m_AsNativeArray_5 = value;
	}

	inline static int32_t get_offset_of_m_ComputeBuffer_6() { return static_cast<int32_t>(offsetof(ObiNativeList_1_tD298AB56D582AAA797727DF401F4FAC39E7DCAEF, ___m_ComputeBuffer_6)); }
	inline ComputeBuffer_t795666617BDC98AB4033FDD4CE0304A22B7E6427 * get_m_ComputeBuffer_6() const { return ___m_ComputeBuffer_6; }
	inline ComputeBuffer_t795666617BDC98AB4033FDD4CE0304A22B7E6427 ** get_address_of_m_ComputeBuffer_6() { return &___m_ComputeBuffer_6; }
	inline void set_m_ComputeBuffer_6(ComputeBuffer_t795666617BDC98AB4033FDD4CE0304A22B7E6427 * value)
	{
		___m_ComputeBuffer_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_ComputeBuffer_6), (void*)value);
	}
};


// Obi.ObiNativeList`1<Obi.Triangle>
struct  ObiNativeList_1_tDE675C68FBF18547F862FC9F08A57ABD59D44A88  : public RuntimeObject
{
public:
	// T[] Obi.ObiNativeList`1::serializedContents
	TriangleU5BU5D_tBE6B25F3BD28AB7727D9AFAD215574627C8ADD53* ___serializedContents_0;
	// System.Void* Obi.ObiNativeList`1::m_AlignedPtr
	void* ___m_AlignedPtr_1;
	// System.Int32 Obi.ObiNativeList`1::m_Stride
	int32_t ___m_Stride_2;
	// System.Int32 Obi.ObiNativeList`1::m_Capacity
	int32_t ___m_Capacity_3;
	// System.Int32 Obi.ObiNativeList`1::m_Count
	int32_t ___m_Count_4;
	// System.Boolean Obi.ObiNativeList`1::m_AsNativeArray
	bool ___m_AsNativeArray_5;
	// UnityEngine.ComputeBuffer Obi.ObiNativeList`1::m_ComputeBuffer
	ComputeBuffer_t795666617BDC98AB4033FDD4CE0304A22B7E6427 * ___m_ComputeBuffer_6;

public:
	inline static int32_t get_offset_of_serializedContents_0() { return static_cast<int32_t>(offsetof(ObiNativeList_1_tDE675C68FBF18547F862FC9F08A57ABD59D44A88, ___serializedContents_0)); }
	inline TriangleU5BU5D_tBE6B25F3BD28AB7727D9AFAD215574627C8ADD53* get_serializedContents_0() const { return ___serializedContents_0; }
	inline TriangleU5BU5D_tBE6B25F3BD28AB7727D9AFAD215574627C8ADD53** get_address_of_serializedContents_0() { return &___serializedContents_0; }
	inline void set_serializedContents_0(TriangleU5BU5D_tBE6B25F3BD28AB7727D9AFAD215574627C8ADD53* value)
	{
		___serializedContents_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___serializedContents_0), (void*)value);
	}

	inline static int32_t get_offset_of_m_AlignedPtr_1() { return static_cast<int32_t>(offsetof(ObiNativeList_1_tDE675C68FBF18547F862FC9F08A57ABD59D44A88, ___m_AlignedPtr_1)); }
	inline void* get_m_AlignedPtr_1() const { return ___m_AlignedPtr_1; }
	inline void** get_address_of_m_AlignedPtr_1() { return &___m_AlignedPtr_1; }
	inline void set_m_AlignedPtr_1(void* value)
	{
		___m_AlignedPtr_1 = value;
	}

	inline static int32_t get_offset_of_m_Stride_2() { return static_cast<int32_t>(offsetof(ObiNativeList_1_tDE675C68FBF18547F862FC9F08A57ABD59D44A88, ___m_Stride_2)); }
	inline int32_t get_m_Stride_2() const { return ___m_Stride_2; }
	inline int32_t* get_address_of_m_Stride_2() { return &___m_Stride_2; }
	inline void set_m_Stride_2(int32_t value)
	{
		___m_Stride_2 = value;
	}

	inline static int32_t get_offset_of_m_Capacity_3() { return static_cast<int32_t>(offsetof(ObiNativeList_1_tDE675C68FBF18547F862FC9F08A57ABD59D44A88, ___m_Capacity_3)); }
	inline int32_t get_m_Capacity_3() const { return ___m_Capacity_3; }
	inline int32_t* get_address_of_m_Capacity_3() { return &___m_Capacity_3; }
	inline void set_m_Capacity_3(int32_t value)
	{
		___m_Capacity_3 = value;
	}

	inline static int32_t get_offset_of_m_Count_4() { return static_cast<int32_t>(offsetof(ObiNativeList_1_tDE675C68FBF18547F862FC9F08A57ABD59D44A88, ___m_Count_4)); }
	inline int32_t get_m_Count_4() const { return ___m_Count_4; }
	inline int32_t* get_address_of_m_Count_4() { return &___m_Count_4; }
	inline void set_m_Count_4(int32_t value)
	{
		___m_Count_4 = value;
	}

	inline static int32_t get_offset_of_m_AsNativeArray_5() { return static_cast<int32_t>(offsetof(ObiNativeList_1_tDE675C68FBF18547F862FC9F08A57ABD59D44A88, ___m_AsNativeArray_5)); }
	inline bool get_m_AsNativeArray_5() const { return ___m_AsNativeArray_5; }
	inline bool* get_address_of_m_AsNativeArray_5() { return &___m_AsNativeArray_5; }
	inline void set_m_AsNativeArray_5(bool value)
	{
		___m_AsNativeArray_5 = value;
	}

	inline static int32_t get_offset_of_m_ComputeBuffer_6() { return static_cast<int32_t>(offsetof(ObiNativeList_1_tDE675C68FBF18547F862FC9F08A57ABD59D44A88, ___m_ComputeBuffer_6)); }
	inline ComputeBuffer_t795666617BDC98AB4033FDD4CE0304A22B7E6427 * get_m_ComputeBuffer_6() const { return ___m_ComputeBuffer_6; }
	inline ComputeBuffer_t795666617BDC98AB4033FDD4CE0304A22B7E6427 ** get_address_of_m_ComputeBuffer_6() { return &___m_ComputeBuffer_6; }
	inline void set_m_ComputeBuffer_6(ComputeBuffer_t795666617BDC98AB4033FDD4CE0304A22B7E6427 * value)
	{
		___m_ComputeBuffer_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_ComputeBuffer_6), (void*)value);
	}
};


// Obi.ObiNativeList`1<Obi.TriangleMeshHeader>
struct  ObiNativeList_1_t8C4BAE9CBB1F8A70288B65B1791A14FB832E1B1F  : public RuntimeObject
{
public:
	// T[] Obi.ObiNativeList`1::serializedContents
	TriangleMeshHeaderU5BU5D_tD0A41E3D9563534F8DE40F7964BBFDBBA1E0EEC2* ___serializedContents_0;
	// System.Void* Obi.ObiNativeList`1::m_AlignedPtr
	void* ___m_AlignedPtr_1;
	// System.Int32 Obi.ObiNativeList`1::m_Stride
	int32_t ___m_Stride_2;
	// System.Int32 Obi.ObiNativeList`1::m_Capacity
	int32_t ___m_Capacity_3;
	// System.Int32 Obi.ObiNativeList`1::m_Count
	int32_t ___m_Count_4;
	// System.Boolean Obi.ObiNativeList`1::m_AsNativeArray
	bool ___m_AsNativeArray_5;
	// UnityEngine.ComputeBuffer Obi.ObiNativeList`1::m_ComputeBuffer
	ComputeBuffer_t795666617BDC98AB4033FDD4CE0304A22B7E6427 * ___m_ComputeBuffer_6;

public:
	inline static int32_t get_offset_of_serializedContents_0() { return static_cast<int32_t>(offsetof(ObiNativeList_1_t8C4BAE9CBB1F8A70288B65B1791A14FB832E1B1F, ___serializedContents_0)); }
	inline TriangleMeshHeaderU5BU5D_tD0A41E3D9563534F8DE40F7964BBFDBBA1E0EEC2* get_serializedContents_0() const { return ___serializedContents_0; }
	inline TriangleMeshHeaderU5BU5D_tD0A41E3D9563534F8DE40F7964BBFDBBA1E0EEC2** get_address_of_serializedContents_0() { return &___serializedContents_0; }
	inline void set_serializedContents_0(TriangleMeshHeaderU5BU5D_tD0A41E3D9563534F8DE40F7964BBFDBBA1E0EEC2* value)
	{
		___serializedContents_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___serializedContents_0), (void*)value);
	}

	inline static int32_t get_offset_of_m_AlignedPtr_1() { return static_cast<int32_t>(offsetof(ObiNativeList_1_t8C4BAE9CBB1F8A70288B65B1791A14FB832E1B1F, ___m_AlignedPtr_1)); }
	inline void* get_m_AlignedPtr_1() const { return ___m_AlignedPtr_1; }
	inline void** get_address_of_m_AlignedPtr_1() { return &___m_AlignedPtr_1; }
	inline void set_m_AlignedPtr_1(void* value)
	{
		___m_AlignedPtr_1 = value;
	}

	inline static int32_t get_offset_of_m_Stride_2() { return static_cast<int32_t>(offsetof(ObiNativeList_1_t8C4BAE9CBB1F8A70288B65B1791A14FB832E1B1F, ___m_Stride_2)); }
	inline int32_t get_m_Stride_2() const { return ___m_Stride_2; }
	inline int32_t* get_address_of_m_Stride_2() { return &___m_Stride_2; }
	inline void set_m_Stride_2(int32_t value)
	{
		___m_Stride_2 = value;
	}

	inline static int32_t get_offset_of_m_Capacity_3() { return static_cast<int32_t>(offsetof(ObiNativeList_1_t8C4BAE9CBB1F8A70288B65B1791A14FB832E1B1F, ___m_Capacity_3)); }
	inline int32_t get_m_Capacity_3() const { return ___m_Capacity_3; }
	inline int32_t* get_address_of_m_Capacity_3() { return &___m_Capacity_3; }
	inline void set_m_Capacity_3(int32_t value)
	{
		___m_Capacity_3 = value;
	}

	inline static int32_t get_offset_of_m_Count_4() { return static_cast<int32_t>(offsetof(ObiNativeList_1_t8C4BAE9CBB1F8A70288B65B1791A14FB832E1B1F, ___m_Count_4)); }
	inline int32_t get_m_Count_4() const { return ___m_Count_4; }
	inline int32_t* get_address_of_m_Count_4() { return &___m_Count_4; }
	inline void set_m_Count_4(int32_t value)
	{
		___m_Count_4 = value;
	}

	inline static int32_t get_offset_of_m_AsNativeArray_5() { return static_cast<int32_t>(offsetof(ObiNativeList_1_t8C4BAE9CBB1F8A70288B65B1791A14FB832E1B1F, ___m_AsNativeArray_5)); }
	inline bool get_m_AsNativeArray_5() const { return ___m_AsNativeArray_5; }
	inline bool* get_address_of_m_AsNativeArray_5() { return &___m_AsNativeArray_5; }
	inline void set_m_AsNativeArray_5(bool value)
	{
		___m_AsNativeArray_5 = value;
	}

	inline static int32_t get_offset_of_m_ComputeBuffer_6() { return static_cast<int32_t>(offsetof(ObiNativeList_1_t8C4BAE9CBB1F8A70288B65B1791A14FB832E1B1F, ___m_ComputeBuffer_6)); }
	inline ComputeBuffer_t795666617BDC98AB4033FDD4CE0304A22B7E6427 * get_m_ComputeBuffer_6() const { return ___m_ComputeBuffer_6; }
	inline ComputeBuffer_t795666617BDC98AB4033FDD4CE0304A22B7E6427 ** get_address_of_m_ComputeBuffer_6() { return &___m_ComputeBuffer_6; }
	inline void set_m_ComputeBuffer_6(ComputeBuffer_t795666617BDC98AB4033FDD4CE0304A22B7E6427 * value)
	{
		___m_ComputeBuffer_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_ComputeBuffer_6), (void*)value);
	}
};


// Obi.ObiNativeList`1<Obi.VInt4>
struct  ObiNativeList_1_tBFECFF8B42E1120D079278D2C8F079D3E3BEA07A  : public RuntimeObject
{
public:
	// T[] Obi.ObiNativeList`1::serializedContents
	VInt4U5BU5D_tAFB782B2F5085208B324359A3380F874BB4F708A* ___serializedContents_0;
	// System.Void* Obi.ObiNativeList`1::m_AlignedPtr
	void* ___m_AlignedPtr_1;
	// System.Int32 Obi.ObiNativeList`1::m_Stride
	int32_t ___m_Stride_2;
	// System.Int32 Obi.ObiNativeList`1::m_Capacity
	int32_t ___m_Capacity_3;
	// System.Int32 Obi.ObiNativeList`1::m_Count
	int32_t ___m_Count_4;
	// System.Boolean Obi.ObiNativeList`1::m_AsNativeArray
	bool ___m_AsNativeArray_5;
	// UnityEngine.ComputeBuffer Obi.ObiNativeList`1::m_ComputeBuffer
	ComputeBuffer_t795666617BDC98AB4033FDD4CE0304A22B7E6427 * ___m_ComputeBuffer_6;

public:
	inline static int32_t get_offset_of_serializedContents_0() { return static_cast<int32_t>(offsetof(ObiNativeList_1_tBFECFF8B42E1120D079278D2C8F079D3E3BEA07A, ___serializedContents_0)); }
	inline VInt4U5BU5D_tAFB782B2F5085208B324359A3380F874BB4F708A* get_serializedContents_0() const { return ___serializedContents_0; }
	inline VInt4U5BU5D_tAFB782B2F5085208B324359A3380F874BB4F708A** get_address_of_serializedContents_0() { return &___serializedContents_0; }
	inline void set_serializedContents_0(VInt4U5BU5D_tAFB782B2F5085208B324359A3380F874BB4F708A* value)
	{
		___serializedContents_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___serializedContents_0), (void*)value);
	}

	inline static int32_t get_offset_of_m_AlignedPtr_1() { return static_cast<int32_t>(offsetof(ObiNativeList_1_tBFECFF8B42E1120D079278D2C8F079D3E3BEA07A, ___m_AlignedPtr_1)); }
	inline void* get_m_AlignedPtr_1() const { return ___m_AlignedPtr_1; }
	inline void** get_address_of_m_AlignedPtr_1() { return &___m_AlignedPtr_1; }
	inline void set_m_AlignedPtr_1(void* value)
	{
		___m_AlignedPtr_1 = value;
	}

	inline static int32_t get_offset_of_m_Stride_2() { return static_cast<int32_t>(offsetof(ObiNativeList_1_tBFECFF8B42E1120D079278D2C8F079D3E3BEA07A, ___m_Stride_2)); }
	inline int32_t get_m_Stride_2() const { return ___m_Stride_2; }
	inline int32_t* get_address_of_m_Stride_2() { return &___m_Stride_2; }
	inline void set_m_Stride_2(int32_t value)
	{
		___m_Stride_2 = value;
	}

	inline static int32_t get_offset_of_m_Capacity_3() { return static_cast<int32_t>(offsetof(ObiNativeList_1_tBFECFF8B42E1120D079278D2C8F079D3E3BEA07A, ___m_Capacity_3)); }
	inline int32_t get_m_Capacity_3() const { return ___m_Capacity_3; }
	inline int32_t* get_address_of_m_Capacity_3() { return &___m_Capacity_3; }
	inline void set_m_Capacity_3(int32_t value)
	{
		___m_Capacity_3 = value;
	}

	inline static int32_t get_offset_of_m_Count_4() { return static_cast<int32_t>(offsetof(ObiNativeList_1_tBFECFF8B42E1120D079278D2C8F079D3E3BEA07A, ___m_Count_4)); }
	inline int32_t get_m_Count_4() const { return ___m_Count_4; }
	inline int32_t* get_address_of_m_Count_4() { return &___m_Count_4; }
	inline void set_m_Count_4(int32_t value)
	{
		___m_Count_4 = value;
	}

	inline static int32_t get_offset_of_m_AsNativeArray_5() { return static_cast<int32_t>(offsetof(ObiNativeList_1_tBFECFF8B42E1120D079278D2C8F079D3E3BEA07A, ___m_AsNativeArray_5)); }
	inline bool get_m_AsNativeArray_5() const { return ___m_AsNativeArray_5; }
	inline bool* get_address_of_m_AsNativeArray_5() { return &___m_AsNativeArray_5; }
	inline void set_m_AsNativeArray_5(bool value)
	{
		___m_AsNativeArray_5 = value;
	}

	inline static int32_t get_offset_of_m_ComputeBuffer_6() { return static_cast<int32_t>(offsetof(ObiNativeList_1_tBFECFF8B42E1120D079278D2C8F079D3E3BEA07A, ___m_ComputeBuffer_6)); }
	inline ComputeBuffer_t795666617BDC98AB4033FDD4CE0304A22B7E6427 * get_m_ComputeBuffer_6() const { return ___m_ComputeBuffer_6; }
	inline ComputeBuffer_t795666617BDC98AB4033FDD4CE0304A22B7E6427 ** get_address_of_m_ComputeBuffer_6() { return &___m_ComputeBuffer_6; }
	inline void set_m_ComputeBuffer_6(ComputeBuffer_t795666617BDC98AB4033FDD4CE0304A22B7E6427 * value)
	{
		___m_ComputeBuffer_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_ComputeBuffer_6), (void*)value);
	}
};


// Obi.ObiNativeList`1<Oni_Contact>
struct  ObiNativeList_1_tCEAE7C719A3DBFC45782E33AFCE91E13E9CA9AF4  : public RuntimeObject
{
public:
	// T[] Obi.ObiNativeList`1::serializedContents
	ContactU5BU5D_t0EA4D0F4F28544AEC9172BDBCC872BD763772C2C* ___serializedContents_0;
	// System.Void* Obi.ObiNativeList`1::m_AlignedPtr
	void* ___m_AlignedPtr_1;
	// System.Int32 Obi.ObiNativeList`1::m_Stride
	int32_t ___m_Stride_2;
	// System.Int32 Obi.ObiNativeList`1::m_Capacity
	int32_t ___m_Capacity_3;
	// System.Int32 Obi.ObiNativeList`1::m_Count
	int32_t ___m_Count_4;
	// System.Boolean Obi.ObiNativeList`1::m_AsNativeArray
	bool ___m_AsNativeArray_5;
	// UnityEngine.ComputeBuffer Obi.ObiNativeList`1::m_ComputeBuffer
	ComputeBuffer_t795666617BDC98AB4033FDD4CE0304A22B7E6427 * ___m_ComputeBuffer_6;

public:
	inline static int32_t get_offset_of_serializedContents_0() { return static_cast<int32_t>(offsetof(ObiNativeList_1_tCEAE7C719A3DBFC45782E33AFCE91E13E9CA9AF4, ___serializedContents_0)); }
	inline ContactU5BU5D_t0EA4D0F4F28544AEC9172BDBCC872BD763772C2C* get_serializedContents_0() const { return ___serializedContents_0; }
	inline ContactU5BU5D_t0EA4D0F4F28544AEC9172BDBCC872BD763772C2C** get_address_of_serializedContents_0() { return &___serializedContents_0; }
	inline void set_serializedContents_0(ContactU5BU5D_t0EA4D0F4F28544AEC9172BDBCC872BD763772C2C* value)
	{
		___serializedContents_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___serializedContents_0), (void*)value);
	}

	inline static int32_t get_offset_of_m_AlignedPtr_1() { return static_cast<int32_t>(offsetof(ObiNativeList_1_tCEAE7C719A3DBFC45782E33AFCE91E13E9CA9AF4, ___m_AlignedPtr_1)); }
	inline void* get_m_AlignedPtr_1() const { return ___m_AlignedPtr_1; }
	inline void** get_address_of_m_AlignedPtr_1() { return &___m_AlignedPtr_1; }
	inline void set_m_AlignedPtr_1(void* value)
	{
		___m_AlignedPtr_1 = value;
	}

	inline static int32_t get_offset_of_m_Stride_2() { return static_cast<int32_t>(offsetof(ObiNativeList_1_tCEAE7C719A3DBFC45782E33AFCE91E13E9CA9AF4, ___m_Stride_2)); }
	inline int32_t get_m_Stride_2() const { return ___m_Stride_2; }
	inline int32_t* get_address_of_m_Stride_2() { return &___m_Stride_2; }
	inline void set_m_Stride_2(int32_t value)
	{
		___m_Stride_2 = value;
	}

	inline static int32_t get_offset_of_m_Capacity_3() { return static_cast<int32_t>(offsetof(ObiNativeList_1_tCEAE7C719A3DBFC45782E33AFCE91E13E9CA9AF4, ___m_Capacity_3)); }
	inline int32_t get_m_Capacity_3() const { return ___m_Capacity_3; }
	inline int32_t* get_address_of_m_Capacity_3() { return &___m_Capacity_3; }
	inline void set_m_Capacity_3(int32_t value)
	{
		___m_Capacity_3 = value;
	}

	inline static int32_t get_offset_of_m_Count_4() { return static_cast<int32_t>(offsetof(ObiNativeList_1_tCEAE7C719A3DBFC45782E33AFCE91E13E9CA9AF4, ___m_Count_4)); }
	inline int32_t get_m_Count_4() const { return ___m_Count_4; }
	inline int32_t* get_address_of_m_Count_4() { return &___m_Count_4; }
	inline void set_m_Count_4(int32_t value)
	{
		___m_Count_4 = value;
	}

	inline static int32_t get_offset_of_m_AsNativeArray_5() { return static_cast<int32_t>(offsetof(ObiNativeList_1_tCEAE7C719A3DBFC45782E33AFCE91E13E9CA9AF4, ___m_AsNativeArray_5)); }
	inline bool get_m_AsNativeArray_5() const { return ___m_AsNativeArray_5; }
	inline bool* get_address_of_m_AsNativeArray_5() { return &___m_AsNativeArray_5; }
	inline void set_m_AsNativeArray_5(bool value)
	{
		___m_AsNativeArray_5 = value;
	}

	inline static int32_t get_offset_of_m_ComputeBuffer_6() { return static_cast<int32_t>(offsetof(ObiNativeList_1_tCEAE7C719A3DBFC45782E33AFCE91E13E9CA9AF4, ___m_ComputeBuffer_6)); }
	inline ComputeBuffer_t795666617BDC98AB4033FDD4CE0304A22B7E6427 * get_m_ComputeBuffer_6() const { return ___m_ComputeBuffer_6; }
	inline ComputeBuffer_t795666617BDC98AB4033FDD4CE0304A22B7E6427 ** get_address_of_m_ComputeBuffer_6() { return &___m_ComputeBuffer_6; }
	inline void set_m_ComputeBuffer_6(ComputeBuffer_t795666617BDC98AB4033FDD4CE0304A22B7E6427 * value)
	{
		___m_ComputeBuffer_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_ComputeBuffer_6), (void*)value);
	}
};


// Obi.ObiNativeList`1<System.Int32>
struct  ObiNativeList_1_t838DB140073974101A3C0BD56D899DA67D9B2EC4  : public RuntimeObject
{
public:
	// T[] Obi.ObiNativeList`1::serializedContents
	Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* ___serializedContents_0;
	// System.Void* Obi.ObiNativeList`1::m_AlignedPtr
	void* ___m_AlignedPtr_1;
	// System.Int32 Obi.ObiNativeList`1::m_Stride
	int32_t ___m_Stride_2;
	// System.Int32 Obi.ObiNativeList`1::m_Capacity
	int32_t ___m_Capacity_3;
	// System.Int32 Obi.ObiNativeList`1::m_Count
	int32_t ___m_Count_4;
	// System.Boolean Obi.ObiNativeList`1::m_AsNativeArray
	bool ___m_AsNativeArray_5;
	// UnityEngine.ComputeBuffer Obi.ObiNativeList`1::m_ComputeBuffer
	ComputeBuffer_t795666617BDC98AB4033FDD4CE0304A22B7E6427 * ___m_ComputeBuffer_6;

public:
	inline static int32_t get_offset_of_serializedContents_0() { return static_cast<int32_t>(offsetof(ObiNativeList_1_t838DB140073974101A3C0BD56D899DA67D9B2EC4, ___serializedContents_0)); }
	inline Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* get_serializedContents_0() const { return ___serializedContents_0; }
	inline Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32** get_address_of_serializedContents_0() { return &___serializedContents_0; }
	inline void set_serializedContents_0(Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* value)
	{
		___serializedContents_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___serializedContents_0), (void*)value);
	}

	inline static int32_t get_offset_of_m_AlignedPtr_1() { return static_cast<int32_t>(offsetof(ObiNativeList_1_t838DB140073974101A3C0BD56D899DA67D9B2EC4, ___m_AlignedPtr_1)); }
	inline void* get_m_AlignedPtr_1() const { return ___m_AlignedPtr_1; }
	inline void** get_address_of_m_AlignedPtr_1() { return &___m_AlignedPtr_1; }
	inline void set_m_AlignedPtr_1(void* value)
	{
		___m_AlignedPtr_1 = value;
	}

	inline static int32_t get_offset_of_m_Stride_2() { return static_cast<int32_t>(offsetof(ObiNativeList_1_t838DB140073974101A3C0BD56D899DA67D9B2EC4, ___m_Stride_2)); }
	inline int32_t get_m_Stride_2() const { return ___m_Stride_2; }
	inline int32_t* get_address_of_m_Stride_2() { return &___m_Stride_2; }
	inline void set_m_Stride_2(int32_t value)
	{
		___m_Stride_2 = value;
	}

	inline static int32_t get_offset_of_m_Capacity_3() { return static_cast<int32_t>(offsetof(ObiNativeList_1_t838DB140073974101A3C0BD56D899DA67D9B2EC4, ___m_Capacity_3)); }
	inline int32_t get_m_Capacity_3() const { return ___m_Capacity_3; }
	inline int32_t* get_address_of_m_Capacity_3() { return &___m_Capacity_3; }
	inline void set_m_Capacity_3(int32_t value)
	{
		___m_Capacity_3 = value;
	}

	inline static int32_t get_offset_of_m_Count_4() { return static_cast<int32_t>(offsetof(ObiNativeList_1_t838DB140073974101A3C0BD56D899DA67D9B2EC4, ___m_Count_4)); }
	inline int32_t get_m_Count_4() const { return ___m_Count_4; }
	inline int32_t* get_address_of_m_Count_4() { return &___m_Count_4; }
	inline void set_m_Count_4(int32_t value)
	{
		___m_Count_4 = value;
	}

	inline static int32_t get_offset_of_m_AsNativeArray_5() { return static_cast<int32_t>(offsetof(ObiNativeList_1_t838DB140073974101A3C0BD56D899DA67D9B2EC4, ___m_AsNativeArray_5)); }
	inline bool get_m_AsNativeArray_5() const { return ___m_AsNativeArray_5; }
	inline bool* get_address_of_m_AsNativeArray_5() { return &___m_AsNativeArray_5; }
	inline void set_m_AsNativeArray_5(bool value)
	{
		___m_AsNativeArray_5 = value;
	}

	inline static int32_t get_offset_of_m_ComputeBuffer_6() { return static_cast<int32_t>(offsetof(ObiNativeList_1_t838DB140073974101A3C0BD56D899DA67D9B2EC4, ___m_ComputeBuffer_6)); }
	inline ComputeBuffer_t795666617BDC98AB4033FDD4CE0304A22B7E6427 * get_m_ComputeBuffer_6() const { return ___m_ComputeBuffer_6; }
	inline ComputeBuffer_t795666617BDC98AB4033FDD4CE0304A22B7E6427 ** get_address_of_m_ComputeBuffer_6() { return &___m_ComputeBuffer_6; }
	inline void set_m_ComputeBuffer_6(ComputeBuffer_t795666617BDC98AB4033FDD4CE0304A22B7E6427 * value)
	{
		___m_ComputeBuffer_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_ComputeBuffer_6), (void*)value);
	}
};


// Obi.ObiNativeList`1<System.IntPtr>
struct  ObiNativeList_1_t2691FD222DD25B2FB43888C0159E83392B57A150  : public RuntimeObject
{
public:
	// T[] Obi.ObiNativeList`1::serializedContents
	IntPtrU5BU5D_t27FC72B0409D75AAF33EC42498E8094E95FEE9A6* ___serializedContents_0;
	// System.Void* Obi.ObiNativeList`1::m_AlignedPtr
	void* ___m_AlignedPtr_1;
	// System.Int32 Obi.ObiNativeList`1::m_Stride
	int32_t ___m_Stride_2;
	// System.Int32 Obi.ObiNativeList`1::m_Capacity
	int32_t ___m_Capacity_3;
	// System.Int32 Obi.ObiNativeList`1::m_Count
	int32_t ___m_Count_4;
	// System.Boolean Obi.ObiNativeList`1::m_AsNativeArray
	bool ___m_AsNativeArray_5;
	// UnityEngine.ComputeBuffer Obi.ObiNativeList`1::m_ComputeBuffer
	ComputeBuffer_t795666617BDC98AB4033FDD4CE0304A22B7E6427 * ___m_ComputeBuffer_6;

public:
	inline static int32_t get_offset_of_serializedContents_0() { return static_cast<int32_t>(offsetof(ObiNativeList_1_t2691FD222DD25B2FB43888C0159E83392B57A150, ___serializedContents_0)); }
	inline IntPtrU5BU5D_t27FC72B0409D75AAF33EC42498E8094E95FEE9A6* get_serializedContents_0() const { return ___serializedContents_0; }
	inline IntPtrU5BU5D_t27FC72B0409D75AAF33EC42498E8094E95FEE9A6** get_address_of_serializedContents_0() { return &___serializedContents_0; }
	inline void set_serializedContents_0(IntPtrU5BU5D_t27FC72B0409D75AAF33EC42498E8094E95FEE9A6* value)
	{
		___serializedContents_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___serializedContents_0), (void*)value);
	}

	inline static int32_t get_offset_of_m_AlignedPtr_1() { return static_cast<int32_t>(offsetof(ObiNativeList_1_t2691FD222DD25B2FB43888C0159E83392B57A150, ___m_AlignedPtr_1)); }
	inline void* get_m_AlignedPtr_1() const { return ___m_AlignedPtr_1; }
	inline void** get_address_of_m_AlignedPtr_1() { return &___m_AlignedPtr_1; }
	inline void set_m_AlignedPtr_1(void* value)
	{
		___m_AlignedPtr_1 = value;
	}

	inline static int32_t get_offset_of_m_Stride_2() { return static_cast<int32_t>(offsetof(ObiNativeList_1_t2691FD222DD25B2FB43888C0159E83392B57A150, ___m_Stride_2)); }
	inline int32_t get_m_Stride_2() const { return ___m_Stride_2; }
	inline int32_t* get_address_of_m_Stride_2() { return &___m_Stride_2; }
	inline void set_m_Stride_2(int32_t value)
	{
		___m_Stride_2 = value;
	}

	inline static int32_t get_offset_of_m_Capacity_3() { return static_cast<int32_t>(offsetof(ObiNativeList_1_t2691FD222DD25B2FB43888C0159E83392B57A150, ___m_Capacity_3)); }
	inline int32_t get_m_Capacity_3() const { return ___m_Capacity_3; }
	inline int32_t* get_address_of_m_Capacity_3() { return &___m_Capacity_3; }
	inline void set_m_Capacity_3(int32_t value)
	{
		___m_Capacity_3 = value;
	}

	inline static int32_t get_offset_of_m_Count_4() { return static_cast<int32_t>(offsetof(ObiNativeList_1_t2691FD222DD25B2FB43888C0159E83392B57A150, ___m_Count_4)); }
	inline int32_t get_m_Count_4() const { return ___m_Count_4; }
	inline int32_t* get_address_of_m_Count_4() { return &___m_Count_4; }
	inline void set_m_Count_4(int32_t value)
	{
		___m_Count_4 = value;
	}

	inline static int32_t get_offset_of_m_AsNativeArray_5() { return static_cast<int32_t>(offsetof(ObiNativeList_1_t2691FD222DD25B2FB43888C0159E83392B57A150, ___m_AsNativeArray_5)); }
	inline bool get_m_AsNativeArray_5() const { return ___m_AsNativeArray_5; }
	inline bool* get_address_of_m_AsNativeArray_5() { return &___m_AsNativeArray_5; }
	inline void set_m_AsNativeArray_5(bool value)
	{
		___m_AsNativeArray_5 = value;
	}

	inline static int32_t get_offset_of_m_ComputeBuffer_6() { return static_cast<int32_t>(offsetof(ObiNativeList_1_t2691FD222DD25B2FB43888C0159E83392B57A150, ___m_ComputeBuffer_6)); }
	inline ComputeBuffer_t795666617BDC98AB4033FDD4CE0304A22B7E6427 * get_m_ComputeBuffer_6() const { return ___m_ComputeBuffer_6; }
	inline ComputeBuffer_t795666617BDC98AB4033FDD4CE0304A22B7E6427 ** get_address_of_m_ComputeBuffer_6() { return &___m_ComputeBuffer_6; }
	inline void set_m_ComputeBuffer_6(ComputeBuffer_t795666617BDC98AB4033FDD4CE0304A22B7E6427 * value)
	{
		___m_ComputeBuffer_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_ComputeBuffer_6), (void*)value);
	}
};


// Obi.ObiNativeList`1<System.Single>
struct  ObiNativeList_1_tE371D1BA733B218B4F6296CB724D923CB25CC53B  : public RuntimeObject
{
public:
	// T[] Obi.ObiNativeList`1::serializedContents
	SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA* ___serializedContents_0;
	// System.Void* Obi.ObiNativeList`1::m_AlignedPtr
	void* ___m_AlignedPtr_1;
	// System.Int32 Obi.ObiNativeList`1::m_Stride
	int32_t ___m_Stride_2;
	// System.Int32 Obi.ObiNativeList`1::m_Capacity
	int32_t ___m_Capacity_3;
	// System.Int32 Obi.ObiNativeList`1::m_Count
	int32_t ___m_Count_4;
	// System.Boolean Obi.ObiNativeList`1::m_AsNativeArray
	bool ___m_AsNativeArray_5;
	// UnityEngine.ComputeBuffer Obi.ObiNativeList`1::m_ComputeBuffer
	ComputeBuffer_t795666617BDC98AB4033FDD4CE0304A22B7E6427 * ___m_ComputeBuffer_6;

public:
	inline static int32_t get_offset_of_serializedContents_0() { return static_cast<int32_t>(offsetof(ObiNativeList_1_tE371D1BA733B218B4F6296CB724D923CB25CC53B, ___serializedContents_0)); }
	inline SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA* get_serializedContents_0() const { return ___serializedContents_0; }
	inline SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA** get_address_of_serializedContents_0() { return &___serializedContents_0; }
	inline void set_serializedContents_0(SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA* value)
	{
		___serializedContents_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___serializedContents_0), (void*)value);
	}

	inline static int32_t get_offset_of_m_AlignedPtr_1() { return static_cast<int32_t>(offsetof(ObiNativeList_1_tE371D1BA733B218B4F6296CB724D923CB25CC53B, ___m_AlignedPtr_1)); }
	inline void* get_m_AlignedPtr_1() const { return ___m_AlignedPtr_1; }
	inline void** get_address_of_m_AlignedPtr_1() { return &___m_AlignedPtr_1; }
	inline void set_m_AlignedPtr_1(void* value)
	{
		___m_AlignedPtr_1 = value;
	}

	inline static int32_t get_offset_of_m_Stride_2() { return static_cast<int32_t>(offsetof(ObiNativeList_1_tE371D1BA733B218B4F6296CB724D923CB25CC53B, ___m_Stride_2)); }
	inline int32_t get_m_Stride_2() const { return ___m_Stride_2; }
	inline int32_t* get_address_of_m_Stride_2() { return &___m_Stride_2; }
	inline void set_m_Stride_2(int32_t value)
	{
		___m_Stride_2 = value;
	}

	inline static int32_t get_offset_of_m_Capacity_3() { return static_cast<int32_t>(offsetof(ObiNativeList_1_tE371D1BA733B218B4F6296CB724D923CB25CC53B, ___m_Capacity_3)); }
	inline int32_t get_m_Capacity_3() const { return ___m_Capacity_3; }
	inline int32_t* get_address_of_m_Capacity_3() { return &___m_Capacity_3; }
	inline void set_m_Capacity_3(int32_t value)
	{
		___m_Capacity_3 = value;
	}

	inline static int32_t get_offset_of_m_Count_4() { return static_cast<int32_t>(offsetof(ObiNativeList_1_tE371D1BA733B218B4F6296CB724D923CB25CC53B, ___m_Count_4)); }
	inline int32_t get_m_Count_4() const { return ___m_Count_4; }
	inline int32_t* get_address_of_m_Count_4() { return &___m_Count_4; }
	inline void set_m_Count_4(int32_t value)
	{
		___m_Count_4 = value;
	}

	inline static int32_t get_offset_of_m_AsNativeArray_5() { return static_cast<int32_t>(offsetof(ObiNativeList_1_tE371D1BA733B218B4F6296CB724D923CB25CC53B, ___m_AsNativeArray_5)); }
	inline bool get_m_AsNativeArray_5() const { return ___m_AsNativeArray_5; }
	inline bool* get_address_of_m_AsNativeArray_5() { return &___m_AsNativeArray_5; }
	inline void set_m_AsNativeArray_5(bool value)
	{
		___m_AsNativeArray_5 = value;
	}

	inline static int32_t get_offset_of_m_ComputeBuffer_6() { return static_cast<int32_t>(offsetof(ObiNativeList_1_tE371D1BA733B218B4F6296CB724D923CB25CC53B, ___m_ComputeBuffer_6)); }
	inline ComputeBuffer_t795666617BDC98AB4033FDD4CE0304A22B7E6427 * get_m_ComputeBuffer_6() const { return ___m_ComputeBuffer_6; }
	inline ComputeBuffer_t795666617BDC98AB4033FDD4CE0304A22B7E6427 ** get_address_of_m_ComputeBuffer_6() { return &___m_ComputeBuffer_6; }
	inline void set_m_ComputeBuffer_6(ComputeBuffer_t795666617BDC98AB4033FDD4CE0304A22B7E6427 * value)
	{
		___m_ComputeBuffer_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_ComputeBuffer_6), (void*)value);
	}
};


// Obi.ObiNativeList`1<UnityEngine.Matrix4x4>
struct  ObiNativeList_1_t93A1DE0BF264327DCAC68372C96B4969F13BC062  : public RuntimeObject
{
public:
	// T[] Obi.ObiNativeList`1::serializedContents
	Matrix4x4U5BU5D_tE53F71E9C9110DD439281A6AB8B699F9F85D8F82* ___serializedContents_0;
	// System.Void* Obi.ObiNativeList`1::m_AlignedPtr
	void* ___m_AlignedPtr_1;
	// System.Int32 Obi.ObiNativeList`1::m_Stride
	int32_t ___m_Stride_2;
	// System.Int32 Obi.ObiNativeList`1::m_Capacity
	int32_t ___m_Capacity_3;
	// System.Int32 Obi.ObiNativeList`1::m_Count
	int32_t ___m_Count_4;
	// System.Boolean Obi.ObiNativeList`1::m_AsNativeArray
	bool ___m_AsNativeArray_5;
	// UnityEngine.ComputeBuffer Obi.ObiNativeList`1::m_ComputeBuffer
	ComputeBuffer_t795666617BDC98AB4033FDD4CE0304A22B7E6427 * ___m_ComputeBuffer_6;

public:
	inline static int32_t get_offset_of_serializedContents_0() { return static_cast<int32_t>(offsetof(ObiNativeList_1_t93A1DE0BF264327DCAC68372C96B4969F13BC062, ___serializedContents_0)); }
	inline Matrix4x4U5BU5D_tE53F71E9C9110DD439281A6AB8B699F9F85D8F82* get_serializedContents_0() const { return ___serializedContents_0; }
	inline Matrix4x4U5BU5D_tE53F71E9C9110DD439281A6AB8B699F9F85D8F82** get_address_of_serializedContents_0() { return &___serializedContents_0; }
	inline void set_serializedContents_0(Matrix4x4U5BU5D_tE53F71E9C9110DD439281A6AB8B699F9F85D8F82* value)
	{
		___serializedContents_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___serializedContents_0), (void*)value);
	}

	inline static int32_t get_offset_of_m_AlignedPtr_1() { return static_cast<int32_t>(offsetof(ObiNativeList_1_t93A1DE0BF264327DCAC68372C96B4969F13BC062, ___m_AlignedPtr_1)); }
	inline void* get_m_AlignedPtr_1() const { return ___m_AlignedPtr_1; }
	inline void** get_address_of_m_AlignedPtr_1() { return &___m_AlignedPtr_1; }
	inline void set_m_AlignedPtr_1(void* value)
	{
		___m_AlignedPtr_1 = value;
	}

	inline static int32_t get_offset_of_m_Stride_2() { return static_cast<int32_t>(offsetof(ObiNativeList_1_t93A1DE0BF264327DCAC68372C96B4969F13BC062, ___m_Stride_2)); }
	inline int32_t get_m_Stride_2() const { return ___m_Stride_2; }
	inline int32_t* get_address_of_m_Stride_2() { return &___m_Stride_2; }
	inline void set_m_Stride_2(int32_t value)
	{
		___m_Stride_2 = value;
	}

	inline static int32_t get_offset_of_m_Capacity_3() { return static_cast<int32_t>(offsetof(ObiNativeList_1_t93A1DE0BF264327DCAC68372C96B4969F13BC062, ___m_Capacity_3)); }
	inline int32_t get_m_Capacity_3() const { return ___m_Capacity_3; }
	inline int32_t* get_address_of_m_Capacity_3() { return &___m_Capacity_3; }
	inline void set_m_Capacity_3(int32_t value)
	{
		___m_Capacity_3 = value;
	}

	inline static int32_t get_offset_of_m_Count_4() { return static_cast<int32_t>(offsetof(ObiNativeList_1_t93A1DE0BF264327DCAC68372C96B4969F13BC062, ___m_Count_4)); }
	inline int32_t get_m_Count_4() const { return ___m_Count_4; }
	inline int32_t* get_address_of_m_Count_4() { return &___m_Count_4; }
	inline void set_m_Count_4(int32_t value)
	{
		___m_Count_4 = value;
	}

	inline static int32_t get_offset_of_m_AsNativeArray_5() { return static_cast<int32_t>(offsetof(ObiNativeList_1_t93A1DE0BF264327DCAC68372C96B4969F13BC062, ___m_AsNativeArray_5)); }
	inline bool get_m_AsNativeArray_5() const { return ___m_AsNativeArray_5; }
	inline bool* get_address_of_m_AsNativeArray_5() { return &___m_AsNativeArray_5; }
	inline void set_m_AsNativeArray_5(bool value)
	{
		___m_AsNativeArray_5 = value;
	}

	inline static int32_t get_offset_of_m_ComputeBuffer_6() { return static_cast<int32_t>(offsetof(ObiNativeList_1_t93A1DE0BF264327DCAC68372C96B4969F13BC062, ___m_ComputeBuffer_6)); }
	inline ComputeBuffer_t795666617BDC98AB4033FDD4CE0304A22B7E6427 * get_m_ComputeBuffer_6() const { return ___m_ComputeBuffer_6; }
	inline ComputeBuffer_t795666617BDC98AB4033FDD4CE0304A22B7E6427 ** get_address_of_m_ComputeBuffer_6() { return &___m_ComputeBuffer_6; }
	inline void set_m_ComputeBuffer_6(ComputeBuffer_t795666617BDC98AB4033FDD4CE0304A22B7E6427 * value)
	{
		___m_ComputeBuffer_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_ComputeBuffer_6), (void*)value);
	}
};


// Obi.ObiNativeList`1<UnityEngine.Quaternion>
struct  ObiNativeList_1_tED35083346C98040EABF6B4FAC5245310D8B9939  : public RuntimeObject
{
public:
	// T[] Obi.ObiNativeList`1::serializedContents
	QuaternionU5BU5D_t584B1CC68E95071898E32F34DB2CC1E4A726FAA6* ___serializedContents_0;
	// System.Void* Obi.ObiNativeList`1::m_AlignedPtr
	void* ___m_AlignedPtr_1;
	// System.Int32 Obi.ObiNativeList`1::m_Stride
	int32_t ___m_Stride_2;
	// System.Int32 Obi.ObiNativeList`1::m_Capacity
	int32_t ___m_Capacity_3;
	// System.Int32 Obi.ObiNativeList`1::m_Count
	int32_t ___m_Count_4;
	// System.Boolean Obi.ObiNativeList`1::m_AsNativeArray
	bool ___m_AsNativeArray_5;
	// UnityEngine.ComputeBuffer Obi.ObiNativeList`1::m_ComputeBuffer
	ComputeBuffer_t795666617BDC98AB4033FDD4CE0304A22B7E6427 * ___m_ComputeBuffer_6;

public:
	inline static int32_t get_offset_of_serializedContents_0() { return static_cast<int32_t>(offsetof(ObiNativeList_1_tED35083346C98040EABF6B4FAC5245310D8B9939, ___serializedContents_0)); }
	inline QuaternionU5BU5D_t584B1CC68E95071898E32F34DB2CC1E4A726FAA6* get_serializedContents_0() const { return ___serializedContents_0; }
	inline QuaternionU5BU5D_t584B1CC68E95071898E32F34DB2CC1E4A726FAA6** get_address_of_serializedContents_0() { return &___serializedContents_0; }
	inline void set_serializedContents_0(QuaternionU5BU5D_t584B1CC68E95071898E32F34DB2CC1E4A726FAA6* value)
	{
		___serializedContents_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___serializedContents_0), (void*)value);
	}

	inline static int32_t get_offset_of_m_AlignedPtr_1() { return static_cast<int32_t>(offsetof(ObiNativeList_1_tED35083346C98040EABF6B4FAC5245310D8B9939, ___m_AlignedPtr_1)); }
	inline void* get_m_AlignedPtr_1() const { return ___m_AlignedPtr_1; }
	inline void** get_address_of_m_AlignedPtr_1() { return &___m_AlignedPtr_1; }
	inline void set_m_AlignedPtr_1(void* value)
	{
		___m_AlignedPtr_1 = value;
	}

	inline static int32_t get_offset_of_m_Stride_2() { return static_cast<int32_t>(offsetof(ObiNativeList_1_tED35083346C98040EABF6B4FAC5245310D8B9939, ___m_Stride_2)); }
	inline int32_t get_m_Stride_2() const { return ___m_Stride_2; }
	inline int32_t* get_address_of_m_Stride_2() { return &___m_Stride_2; }
	inline void set_m_Stride_2(int32_t value)
	{
		___m_Stride_2 = value;
	}

	inline static int32_t get_offset_of_m_Capacity_3() { return static_cast<int32_t>(offsetof(ObiNativeList_1_tED35083346C98040EABF6B4FAC5245310D8B9939, ___m_Capacity_3)); }
	inline int32_t get_m_Capacity_3() const { return ___m_Capacity_3; }
	inline int32_t* get_address_of_m_Capacity_3() { return &___m_Capacity_3; }
	inline void set_m_Capacity_3(int32_t value)
	{
		___m_Capacity_3 = value;
	}

	inline static int32_t get_offset_of_m_Count_4() { return static_cast<int32_t>(offsetof(ObiNativeList_1_tED35083346C98040EABF6B4FAC5245310D8B9939, ___m_Count_4)); }
	inline int32_t get_m_Count_4() const { return ___m_Count_4; }
	inline int32_t* get_address_of_m_Count_4() { return &___m_Count_4; }
	inline void set_m_Count_4(int32_t value)
	{
		___m_Count_4 = value;
	}

	inline static int32_t get_offset_of_m_AsNativeArray_5() { return static_cast<int32_t>(offsetof(ObiNativeList_1_tED35083346C98040EABF6B4FAC5245310D8B9939, ___m_AsNativeArray_5)); }
	inline bool get_m_AsNativeArray_5() const { return ___m_AsNativeArray_5; }
	inline bool* get_address_of_m_AsNativeArray_5() { return &___m_AsNativeArray_5; }
	inline void set_m_AsNativeArray_5(bool value)
	{
		___m_AsNativeArray_5 = value;
	}

	inline static int32_t get_offset_of_m_ComputeBuffer_6() { return static_cast<int32_t>(offsetof(ObiNativeList_1_tED35083346C98040EABF6B4FAC5245310D8B9939, ___m_ComputeBuffer_6)); }
	inline ComputeBuffer_t795666617BDC98AB4033FDD4CE0304A22B7E6427 * get_m_ComputeBuffer_6() const { return ___m_ComputeBuffer_6; }
	inline ComputeBuffer_t795666617BDC98AB4033FDD4CE0304A22B7E6427 ** get_address_of_m_ComputeBuffer_6() { return &___m_ComputeBuffer_6; }
	inline void set_m_ComputeBuffer_6(ComputeBuffer_t795666617BDC98AB4033FDD4CE0304A22B7E6427 * value)
	{
		___m_ComputeBuffer_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_ComputeBuffer_6), (void*)value);
	}
};


// Obi.ObiNativeList`1<UnityEngine.Vector2>
struct  ObiNativeList_1_t5C6ED20BB4BD829B45C019D5E3348CC6BAF2F37D  : public RuntimeObject
{
public:
	// T[] Obi.ObiNativeList`1::serializedContents
	Vector2U5BU5D_tE0F58A2D6D8592B5EC37D9CDEF09103A02E5D7FA* ___serializedContents_0;
	// System.Void* Obi.ObiNativeList`1::m_AlignedPtr
	void* ___m_AlignedPtr_1;
	// System.Int32 Obi.ObiNativeList`1::m_Stride
	int32_t ___m_Stride_2;
	// System.Int32 Obi.ObiNativeList`1::m_Capacity
	int32_t ___m_Capacity_3;
	// System.Int32 Obi.ObiNativeList`1::m_Count
	int32_t ___m_Count_4;
	// System.Boolean Obi.ObiNativeList`1::m_AsNativeArray
	bool ___m_AsNativeArray_5;
	// UnityEngine.ComputeBuffer Obi.ObiNativeList`1::m_ComputeBuffer
	ComputeBuffer_t795666617BDC98AB4033FDD4CE0304A22B7E6427 * ___m_ComputeBuffer_6;

public:
	inline static int32_t get_offset_of_serializedContents_0() { return static_cast<int32_t>(offsetof(ObiNativeList_1_t5C6ED20BB4BD829B45C019D5E3348CC6BAF2F37D, ___serializedContents_0)); }
	inline Vector2U5BU5D_tE0F58A2D6D8592B5EC37D9CDEF09103A02E5D7FA* get_serializedContents_0() const { return ___serializedContents_0; }
	inline Vector2U5BU5D_tE0F58A2D6D8592B5EC37D9CDEF09103A02E5D7FA** get_address_of_serializedContents_0() { return &___serializedContents_0; }
	inline void set_serializedContents_0(Vector2U5BU5D_tE0F58A2D6D8592B5EC37D9CDEF09103A02E5D7FA* value)
	{
		___serializedContents_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___serializedContents_0), (void*)value);
	}

	inline static int32_t get_offset_of_m_AlignedPtr_1() { return static_cast<int32_t>(offsetof(ObiNativeList_1_t5C6ED20BB4BD829B45C019D5E3348CC6BAF2F37D, ___m_AlignedPtr_1)); }
	inline void* get_m_AlignedPtr_1() const { return ___m_AlignedPtr_1; }
	inline void** get_address_of_m_AlignedPtr_1() { return &___m_AlignedPtr_1; }
	inline void set_m_AlignedPtr_1(void* value)
	{
		___m_AlignedPtr_1 = value;
	}

	inline static int32_t get_offset_of_m_Stride_2() { return static_cast<int32_t>(offsetof(ObiNativeList_1_t5C6ED20BB4BD829B45C019D5E3348CC6BAF2F37D, ___m_Stride_2)); }
	inline int32_t get_m_Stride_2() const { return ___m_Stride_2; }
	inline int32_t* get_address_of_m_Stride_2() { return &___m_Stride_2; }
	inline void set_m_Stride_2(int32_t value)
	{
		___m_Stride_2 = value;
	}

	inline static int32_t get_offset_of_m_Capacity_3() { return static_cast<int32_t>(offsetof(ObiNativeList_1_t5C6ED20BB4BD829B45C019D5E3348CC6BAF2F37D, ___m_Capacity_3)); }
	inline int32_t get_m_Capacity_3() const { return ___m_Capacity_3; }
	inline int32_t* get_address_of_m_Capacity_3() { return &___m_Capacity_3; }
	inline void set_m_Capacity_3(int32_t value)
	{
		___m_Capacity_3 = value;
	}

	inline static int32_t get_offset_of_m_Count_4() { return static_cast<int32_t>(offsetof(ObiNativeList_1_t5C6ED20BB4BD829B45C019D5E3348CC6BAF2F37D, ___m_Count_4)); }
	inline int32_t get_m_Count_4() const { return ___m_Count_4; }
	inline int32_t* get_address_of_m_Count_4() { return &___m_Count_4; }
	inline void set_m_Count_4(int32_t value)
	{
		___m_Count_4 = value;
	}

	inline static int32_t get_offset_of_m_AsNativeArray_5() { return static_cast<int32_t>(offsetof(ObiNativeList_1_t5C6ED20BB4BD829B45C019D5E3348CC6BAF2F37D, ___m_AsNativeArray_5)); }
	inline bool get_m_AsNativeArray_5() const { return ___m_AsNativeArray_5; }
	inline bool* get_address_of_m_AsNativeArray_5() { return &___m_AsNativeArray_5; }
	inline void set_m_AsNativeArray_5(bool value)
	{
		___m_AsNativeArray_5 = value;
	}

	inline static int32_t get_offset_of_m_ComputeBuffer_6() { return static_cast<int32_t>(offsetof(ObiNativeList_1_t5C6ED20BB4BD829B45C019D5E3348CC6BAF2F37D, ___m_ComputeBuffer_6)); }
	inline ComputeBuffer_t795666617BDC98AB4033FDD4CE0304A22B7E6427 * get_m_ComputeBuffer_6() const { return ___m_ComputeBuffer_6; }
	inline ComputeBuffer_t795666617BDC98AB4033FDD4CE0304A22B7E6427 ** get_address_of_m_ComputeBuffer_6() { return &___m_ComputeBuffer_6; }
	inline void set_m_ComputeBuffer_6(ComputeBuffer_t795666617BDC98AB4033FDD4CE0304A22B7E6427 * value)
	{
		___m_ComputeBuffer_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_ComputeBuffer_6), (void*)value);
	}
};


// Obi.ObiNativeList`1<UnityEngine.Vector3>
struct  ObiNativeList_1_t0F29E875C6BF5DD7F89BA979309B5604FDC8DB26  : public RuntimeObject
{
public:
	// T[] Obi.ObiNativeList`1::serializedContents
	Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* ___serializedContents_0;
	// System.Void* Obi.ObiNativeList`1::m_AlignedPtr
	void* ___m_AlignedPtr_1;
	// System.Int32 Obi.ObiNativeList`1::m_Stride
	int32_t ___m_Stride_2;
	// System.Int32 Obi.ObiNativeList`1::m_Capacity
	int32_t ___m_Capacity_3;
	// System.Int32 Obi.ObiNativeList`1::m_Count
	int32_t ___m_Count_4;
	// System.Boolean Obi.ObiNativeList`1::m_AsNativeArray
	bool ___m_AsNativeArray_5;
	// UnityEngine.ComputeBuffer Obi.ObiNativeList`1::m_ComputeBuffer
	ComputeBuffer_t795666617BDC98AB4033FDD4CE0304A22B7E6427 * ___m_ComputeBuffer_6;

public:
	inline static int32_t get_offset_of_serializedContents_0() { return static_cast<int32_t>(offsetof(ObiNativeList_1_t0F29E875C6BF5DD7F89BA979309B5604FDC8DB26, ___serializedContents_0)); }
	inline Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* get_serializedContents_0() const { return ___serializedContents_0; }
	inline Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4** get_address_of_serializedContents_0() { return &___serializedContents_0; }
	inline void set_serializedContents_0(Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* value)
	{
		___serializedContents_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___serializedContents_0), (void*)value);
	}

	inline static int32_t get_offset_of_m_AlignedPtr_1() { return static_cast<int32_t>(offsetof(ObiNativeList_1_t0F29E875C6BF5DD7F89BA979309B5604FDC8DB26, ___m_AlignedPtr_1)); }
	inline void* get_m_AlignedPtr_1() const { return ___m_AlignedPtr_1; }
	inline void** get_address_of_m_AlignedPtr_1() { return &___m_AlignedPtr_1; }
	inline void set_m_AlignedPtr_1(void* value)
	{
		___m_AlignedPtr_1 = value;
	}

	inline static int32_t get_offset_of_m_Stride_2() { return static_cast<int32_t>(offsetof(ObiNativeList_1_t0F29E875C6BF5DD7F89BA979309B5604FDC8DB26, ___m_Stride_2)); }
	inline int32_t get_m_Stride_2() const { return ___m_Stride_2; }
	inline int32_t* get_address_of_m_Stride_2() { return &___m_Stride_2; }
	inline void set_m_Stride_2(int32_t value)
	{
		___m_Stride_2 = value;
	}

	inline static int32_t get_offset_of_m_Capacity_3() { return static_cast<int32_t>(offsetof(ObiNativeList_1_t0F29E875C6BF5DD7F89BA979309B5604FDC8DB26, ___m_Capacity_3)); }
	inline int32_t get_m_Capacity_3() const { return ___m_Capacity_3; }
	inline int32_t* get_address_of_m_Capacity_3() { return &___m_Capacity_3; }
	inline void set_m_Capacity_3(int32_t value)
	{
		___m_Capacity_3 = value;
	}

	inline static int32_t get_offset_of_m_Count_4() { return static_cast<int32_t>(offsetof(ObiNativeList_1_t0F29E875C6BF5DD7F89BA979309B5604FDC8DB26, ___m_Count_4)); }
	inline int32_t get_m_Count_4() const { return ___m_Count_4; }
	inline int32_t* get_address_of_m_Count_4() { return &___m_Count_4; }
	inline void set_m_Count_4(int32_t value)
	{
		___m_Count_4 = value;
	}

	inline static int32_t get_offset_of_m_AsNativeArray_5() { return static_cast<int32_t>(offsetof(ObiNativeList_1_t0F29E875C6BF5DD7F89BA979309B5604FDC8DB26, ___m_AsNativeArray_5)); }
	inline bool get_m_AsNativeArray_5() const { return ___m_AsNativeArray_5; }
	inline bool* get_address_of_m_AsNativeArray_5() { return &___m_AsNativeArray_5; }
	inline void set_m_AsNativeArray_5(bool value)
	{
		___m_AsNativeArray_5 = value;
	}

	inline static int32_t get_offset_of_m_ComputeBuffer_6() { return static_cast<int32_t>(offsetof(ObiNativeList_1_t0F29E875C6BF5DD7F89BA979309B5604FDC8DB26, ___m_ComputeBuffer_6)); }
	inline ComputeBuffer_t795666617BDC98AB4033FDD4CE0304A22B7E6427 * get_m_ComputeBuffer_6() const { return ___m_ComputeBuffer_6; }
	inline ComputeBuffer_t795666617BDC98AB4033FDD4CE0304A22B7E6427 ** get_address_of_m_ComputeBuffer_6() { return &___m_ComputeBuffer_6; }
	inline void set_m_ComputeBuffer_6(ComputeBuffer_t795666617BDC98AB4033FDD4CE0304A22B7E6427 * value)
	{
		___m_ComputeBuffer_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_ComputeBuffer_6), (void*)value);
	}
};


// Obi.ObiNativeList`1<UnityEngine.Vector4>
struct  ObiNativeList_1_t667688879E4BD2DFE8EDD92169E59F19619DF4F4  : public RuntimeObject
{
public:
	// T[] Obi.ObiNativeList`1::serializedContents
	Vector4U5BU5D_tCE72D928AA6FF1852BAC5E4396F6F0131ED11871* ___serializedContents_0;
	// System.Void* Obi.ObiNativeList`1::m_AlignedPtr
	void* ___m_AlignedPtr_1;
	// System.Int32 Obi.ObiNativeList`1::m_Stride
	int32_t ___m_Stride_2;
	// System.Int32 Obi.ObiNativeList`1::m_Capacity
	int32_t ___m_Capacity_3;
	// System.Int32 Obi.ObiNativeList`1::m_Count
	int32_t ___m_Count_4;
	// System.Boolean Obi.ObiNativeList`1::m_AsNativeArray
	bool ___m_AsNativeArray_5;
	// UnityEngine.ComputeBuffer Obi.ObiNativeList`1::m_ComputeBuffer
	ComputeBuffer_t795666617BDC98AB4033FDD4CE0304A22B7E6427 * ___m_ComputeBuffer_6;

public:
	inline static int32_t get_offset_of_serializedContents_0() { return static_cast<int32_t>(offsetof(ObiNativeList_1_t667688879E4BD2DFE8EDD92169E59F19619DF4F4, ___serializedContents_0)); }
	inline Vector4U5BU5D_tCE72D928AA6FF1852BAC5E4396F6F0131ED11871* get_serializedContents_0() const { return ___serializedContents_0; }
	inline Vector4U5BU5D_tCE72D928AA6FF1852BAC5E4396F6F0131ED11871** get_address_of_serializedContents_0() { return &___serializedContents_0; }
	inline void set_serializedContents_0(Vector4U5BU5D_tCE72D928AA6FF1852BAC5E4396F6F0131ED11871* value)
	{
		___serializedContents_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___serializedContents_0), (void*)value);
	}

	inline static int32_t get_offset_of_m_AlignedPtr_1() { return static_cast<int32_t>(offsetof(ObiNativeList_1_t667688879E4BD2DFE8EDD92169E59F19619DF4F4, ___m_AlignedPtr_1)); }
	inline void* get_m_AlignedPtr_1() const { return ___m_AlignedPtr_1; }
	inline void** get_address_of_m_AlignedPtr_1() { return &___m_AlignedPtr_1; }
	inline void set_m_AlignedPtr_1(void* value)
	{
		___m_AlignedPtr_1 = value;
	}

	inline static int32_t get_offset_of_m_Stride_2() { return static_cast<int32_t>(offsetof(ObiNativeList_1_t667688879E4BD2DFE8EDD92169E59F19619DF4F4, ___m_Stride_2)); }
	inline int32_t get_m_Stride_2() const { return ___m_Stride_2; }
	inline int32_t* get_address_of_m_Stride_2() { return &___m_Stride_2; }
	inline void set_m_Stride_2(int32_t value)
	{
		___m_Stride_2 = value;
	}

	inline static int32_t get_offset_of_m_Capacity_3() { return static_cast<int32_t>(offsetof(ObiNativeList_1_t667688879E4BD2DFE8EDD92169E59F19619DF4F4, ___m_Capacity_3)); }
	inline int32_t get_m_Capacity_3() const { return ___m_Capacity_3; }
	inline int32_t* get_address_of_m_Capacity_3() { return &___m_Capacity_3; }
	inline void set_m_Capacity_3(int32_t value)
	{
		___m_Capacity_3 = value;
	}

	inline static int32_t get_offset_of_m_Count_4() { return static_cast<int32_t>(offsetof(ObiNativeList_1_t667688879E4BD2DFE8EDD92169E59F19619DF4F4, ___m_Count_4)); }
	inline int32_t get_m_Count_4() const { return ___m_Count_4; }
	inline int32_t* get_address_of_m_Count_4() { return &___m_Count_4; }
	inline void set_m_Count_4(int32_t value)
	{
		___m_Count_4 = value;
	}

	inline static int32_t get_offset_of_m_AsNativeArray_5() { return static_cast<int32_t>(offsetof(ObiNativeList_1_t667688879E4BD2DFE8EDD92169E59F19619DF4F4, ___m_AsNativeArray_5)); }
	inline bool get_m_AsNativeArray_5() const { return ___m_AsNativeArray_5; }
	inline bool* get_address_of_m_AsNativeArray_5() { return &___m_AsNativeArray_5; }
	inline void set_m_AsNativeArray_5(bool value)
	{
		___m_AsNativeArray_5 = value;
	}

	inline static int32_t get_offset_of_m_ComputeBuffer_6() { return static_cast<int32_t>(offsetof(ObiNativeList_1_t667688879E4BD2DFE8EDD92169E59F19619DF4F4, ___m_ComputeBuffer_6)); }
	inline ComputeBuffer_t795666617BDC98AB4033FDD4CE0304A22B7E6427 * get_m_ComputeBuffer_6() const { return ___m_ComputeBuffer_6; }
	inline ComputeBuffer_t795666617BDC98AB4033FDD4CE0304A22B7E6427 ** get_address_of_m_ComputeBuffer_6() { return &___m_ComputeBuffer_6; }
	inline void set_m_ComputeBuffer_6(ComputeBuffer_t795666617BDC98AB4033FDD4CE0304A22B7E6427 * value)
	{
		___m_ComputeBuffer_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_ComputeBuffer_6), (void*)value);
	}
};


// System.Runtime.CompilerServices.AsyncMethodBuilderCore_MoveNextRunner
struct  MoveNextRunner_tFAEA0BEDD353E2E34E8E287C67B1F5572FD30C2D  : public RuntimeObject
{
public:
	// System.Threading.ExecutionContext System.Runtime.CompilerServices.AsyncMethodBuilderCore_MoveNextRunner::m_context
	ExecutionContext_t16AC73BB21FEEEAD34A017877AC18DD8BB836414 * ___m_context_0;
	// System.Runtime.CompilerServices.IAsyncStateMachine System.Runtime.CompilerServices.AsyncMethodBuilderCore_MoveNextRunner::m_stateMachine
	RuntimeObject* ___m_stateMachine_1;

public:
	inline static int32_t get_offset_of_m_context_0() { return static_cast<int32_t>(offsetof(MoveNextRunner_tFAEA0BEDD353E2E34E8E287C67B1F5572FD30C2D, ___m_context_0)); }
	inline ExecutionContext_t16AC73BB21FEEEAD34A017877AC18DD8BB836414 * get_m_context_0() const { return ___m_context_0; }
	inline ExecutionContext_t16AC73BB21FEEEAD34A017877AC18DD8BB836414 ** get_address_of_m_context_0() { return &___m_context_0; }
	inline void set_m_context_0(ExecutionContext_t16AC73BB21FEEEAD34A017877AC18DD8BB836414 * value)
	{
		___m_context_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_context_0), (void*)value);
	}

	inline static int32_t get_offset_of_m_stateMachine_1() { return static_cast<int32_t>(offsetof(MoveNextRunner_tFAEA0BEDD353E2E34E8E287C67B1F5572FD30C2D, ___m_stateMachine_1)); }
	inline RuntimeObject* get_m_stateMachine_1() const { return ___m_stateMachine_1; }
	inline RuntimeObject** get_address_of_m_stateMachine_1() { return &___m_stateMachine_1; }
	inline void set_m_stateMachine_1(RuntimeObject* value)
	{
		___m_stateMachine_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_stateMachine_1), (void*)value);
	}
};

struct MoveNextRunner_tFAEA0BEDD353E2E34E8E287C67B1F5572FD30C2D_StaticFields
{
public:
	// System.Threading.ContextCallback System.Runtime.CompilerServices.AsyncMethodBuilderCore_MoveNextRunner::s_invokeMoveNext
	ContextCallback_t93707E0430F4FF3E15E1FB5A4844BE89C657AE8B * ___s_invokeMoveNext_2;

public:
	inline static int32_t get_offset_of_s_invokeMoveNext_2() { return static_cast<int32_t>(offsetof(MoveNextRunner_tFAEA0BEDD353E2E34E8E287C67B1F5572FD30C2D_StaticFields, ___s_invokeMoveNext_2)); }
	inline ContextCallback_t93707E0430F4FF3E15E1FB5A4844BE89C657AE8B * get_s_invokeMoveNext_2() const { return ___s_invokeMoveNext_2; }
	inline ContextCallback_t93707E0430F4FF3E15E1FB5A4844BE89C657AE8B ** get_address_of_s_invokeMoveNext_2() { return &___s_invokeMoveNext_2; }
	inline void set_s_invokeMoveNext_2(ContextCallback_t93707E0430F4FF3E15E1FB5A4844BE89C657AE8B * value)
	{
		___s_invokeMoveNext_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_invokeMoveNext_2), (void*)value);
	}
};


// System.String
struct  String_t  : public RuntimeObject
{
public:
	// System.Int32 System.String::m_stringLength
	int32_t ___m_stringLength_0;
	// System.Char System.String::m_firstChar
	Il2CppChar ___m_firstChar_1;

public:
	inline static int32_t get_offset_of_m_stringLength_0() { return static_cast<int32_t>(offsetof(String_t, ___m_stringLength_0)); }
	inline int32_t get_m_stringLength_0() const { return ___m_stringLength_0; }
	inline int32_t* get_address_of_m_stringLength_0() { return &___m_stringLength_0; }
	inline void set_m_stringLength_0(int32_t value)
	{
		___m_stringLength_0 = value;
	}

	inline static int32_t get_offset_of_m_firstChar_1() { return static_cast<int32_t>(offsetof(String_t, ___m_firstChar_1)); }
	inline Il2CppChar get_m_firstChar_1() const { return ___m_firstChar_1; }
	inline Il2CppChar* get_address_of_m_firstChar_1() { return &___m_firstChar_1; }
	inline void set_m_firstChar_1(Il2CppChar value)
	{
		___m_firstChar_1 = value;
	}
};

struct String_t_StaticFields
{
public:
	// System.String System.String::Empty
	String_t* ___Empty_5;

public:
	inline static int32_t get_offset_of_Empty_5() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___Empty_5)); }
	inline String_t* get_Empty_5() const { return ___Empty_5; }
	inline String_t** get_address_of_Empty_5() { return &___Empty_5; }
	inline void set_Empty_5(String_t* value)
	{
		___Empty_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Empty_5), (void*)value);
	}
};


// System.Threading.SynchronizationContext
struct  SynchronizationContext_t17D9365B5E0D30A0910A16FA4351C525232EF069  : public RuntimeObject
{
public:

public:
};


// System.Threading.Volatile
struct  Volatile_t7A8B2983396C4500A8FC226CDB66FE9067DA4AE6  : public RuntimeObject
{
public:

public:
};


// System.ValueType
struct  ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52  : public RuntimeObject
{
public:

public:
};

// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52_marshaled_com
{
};

// Unity.Collections.LowLevel.Unsafe.UnsafeUtility
struct  UnsafeUtility_tAA965823E05BE8ADD69F58C82BF0DF723476E551  : public RuntimeObject
{
public:

public:
};


// Obi.BIHNode
struct  BIHNode_tA8B64B5175479DEC9FBF8312EF5F2163C1FE0D05 
{
public:
	// System.Int32 Obi.BIHNode::firstChild
	int32_t ___firstChild_0;
	// System.Int32 Obi.BIHNode::start
	int32_t ___start_1;
	// System.Int32 Obi.BIHNode::count
	int32_t ___count_2;
	// System.Int32 Obi.BIHNode::axis
	int32_t ___axis_3;
	// System.Single Obi.BIHNode::min
	float ___min_4;
	// System.Single Obi.BIHNode::max
	float ___max_5;

public:
	inline static int32_t get_offset_of_firstChild_0() { return static_cast<int32_t>(offsetof(BIHNode_tA8B64B5175479DEC9FBF8312EF5F2163C1FE0D05, ___firstChild_0)); }
	inline int32_t get_firstChild_0() const { return ___firstChild_0; }
	inline int32_t* get_address_of_firstChild_0() { return &___firstChild_0; }
	inline void set_firstChild_0(int32_t value)
	{
		___firstChild_0 = value;
	}

	inline static int32_t get_offset_of_start_1() { return static_cast<int32_t>(offsetof(BIHNode_tA8B64B5175479DEC9FBF8312EF5F2163C1FE0D05, ___start_1)); }
	inline int32_t get_start_1() const { return ___start_1; }
	inline int32_t* get_address_of_start_1() { return &___start_1; }
	inline void set_start_1(int32_t value)
	{
		___start_1 = value;
	}

	inline static int32_t get_offset_of_count_2() { return static_cast<int32_t>(offsetof(BIHNode_tA8B64B5175479DEC9FBF8312EF5F2163C1FE0D05, ___count_2)); }
	inline int32_t get_count_2() const { return ___count_2; }
	inline int32_t* get_address_of_count_2() { return &___count_2; }
	inline void set_count_2(int32_t value)
	{
		___count_2 = value;
	}

	inline static int32_t get_offset_of_axis_3() { return static_cast<int32_t>(offsetof(BIHNode_tA8B64B5175479DEC9FBF8312EF5F2163C1FE0D05, ___axis_3)); }
	inline int32_t get_axis_3() const { return ___axis_3; }
	inline int32_t* get_address_of_axis_3() { return &___axis_3; }
	inline void set_axis_3(int32_t value)
	{
		___axis_3 = value;
	}

	inline static int32_t get_offset_of_min_4() { return static_cast<int32_t>(offsetof(BIHNode_tA8B64B5175479DEC9FBF8312EF5F2163C1FE0D05, ___min_4)); }
	inline float get_min_4() const { return ___min_4; }
	inline float* get_address_of_min_4() { return &___min_4; }
	inline void set_min_4(float value)
	{
		___min_4 = value;
	}

	inline static int32_t get_offset_of_max_5() { return static_cast<int32_t>(offsetof(BIHNode_tA8B64B5175479DEC9FBF8312EF5F2163C1FE0D05, ___max_5)); }
	inline float get_max_5() const { return ___max_5; }
	inline float* get_address_of_max_5() { return &___max_5; }
	inline void set_max_5(float value)
	{
		___max_5 = value;
	}
};


// Obi.DistanceFieldHeader
struct  DistanceFieldHeader_tE66F34D3B1EDB411B0D6E1346D68383E03E33AD4 
{
public:
	// System.Int32 Obi.DistanceFieldHeader::firstNode
	int32_t ___firstNode_0;
	// System.Int32 Obi.DistanceFieldHeader::nodeCount
	int32_t ___nodeCount_1;

public:
	inline static int32_t get_offset_of_firstNode_0() { return static_cast<int32_t>(offsetof(DistanceFieldHeader_tE66F34D3B1EDB411B0D6E1346D68383E03E33AD4, ___firstNode_0)); }
	inline int32_t get_firstNode_0() const { return ___firstNode_0; }
	inline int32_t* get_address_of_firstNode_0() { return &___firstNode_0; }
	inline void set_firstNode_0(int32_t value)
	{
		___firstNode_0 = value;
	}

	inline static int32_t get_offset_of_nodeCount_1() { return static_cast<int32_t>(offsetof(DistanceFieldHeader_tE66F34D3B1EDB411B0D6E1346D68383E03E33AD4, ___nodeCount_1)); }
	inline int32_t get_nodeCount_1() const { return ___nodeCount_1; }
	inline int32_t* get_address_of_nodeCount_1() { return &___nodeCount_1; }
	inline void set_nodeCount_1(int32_t value)
	{
		___nodeCount_1 = value;
	}
};


// Obi.EdgeMeshHeader
struct  EdgeMeshHeader_t235B26AE87E4F8BC38EC627430A9E0E344A03878 
{
public:
	// System.Int32 Obi.EdgeMeshHeader::firstNode
	int32_t ___firstNode_0;
	// System.Int32 Obi.EdgeMeshHeader::nodeCount
	int32_t ___nodeCount_1;
	// System.Int32 Obi.EdgeMeshHeader::firstEdge
	int32_t ___firstEdge_2;
	// System.Int32 Obi.EdgeMeshHeader::edgeCount
	int32_t ___edgeCount_3;
	// System.Int32 Obi.EdgeMeshHeader::firstVertex
	int32_t ___firstVertex_4;
	// System.Int32 Obi.EdgeMeshHeader::vertexCount
	int32_t ___vertexCount_5;

public:
	inline static int32_t get_offset_of_firstNode_0() { return static_cast<int32_t>(offsetof(EdgeMeshHeader_t235B26AE87E4F8BC38EC627430A9E0E344A03878, ___firstNode_0)); }
	inline int32_t get_firstNode_0() const { return ___firstNode_0; }
	inline int32_t* get_address_of_firstNode_0() { return &___firstNode_0; }
	inline void set_firstNode_0(int32_t value)
	{
		___firstNode_0 = value;
	}

	inline static int32_t get_offset_of_nodeCount_1() { return static_cast<int32_t>(offsetof(EdgeMeshHeader_t235B26AE87E4F8BC38EC627430A9E0E344A03878, ___nodeCount_1)); }
	inline int32_t get_nodeCount_1() const { return ___nodeCount_1; }
	inline int32_t* get_address_of_nodeCount_1() { return &___nodeCount_1; }
	inline void set_nodeCount_1(int32_t value)
	{
		___nodeCount_1 = value;
	}

	inline static int32_t get_offset_of_firstEdge_2() { return static_cast<int32_t>(offsetof(EdgeMeshHeader_t235B26AE87E4F8BC38EC627430A9E0E344A03878, ___firstEdge_2)); }
	inline int32_t get_firstEdge_2() const { return ___firstEdge_2; }
	inline int32_t* get_address_of_firstEdge_2() { return &___firstEdge_2; }
	inline void set_firstEdge_2(int32_t value)
	{
		___firstEdge_2 = value;
	}

	inline static int32_t get_offset_of_edgeCount_3() { return static_cast<int32_t>(offsetof(EdgeMeshHeader_t235B26AE87E4F8BC38EC627430A9E0E344A03878, ___edgeCount_3)); }
	inline int32_t get_edgeCount_3() const { return ___edgeCount_3; }
	inline int32_t* get_address_of_edgeCount_3() { return &___edgeCount_3; }
	inline void set_edgeCount_3(int32_t value)
	{
		___edgeCount_3 = value;
	}

	inline static int32_t get_offset_of_firstVertex_4() { return static_cast<int32_t>(offsetof(EdgeMeshHeader_t235B26AE87E4F8BC38EC627430A9E0E344A03878, ___firstVertex_4)); }
	inline int32_t get_firstVertex_4() const { return ___firstVertex_4; }
	inline int32_t* get_address_of_firstVertex_4() { return &___firstVertex_4; }
	inline void set_firstVertex_4(int32_t value)
	{
		___firstVertex_4 = value;
	}

	inline static int32_t get_offset_of_vertexCount_5() { return static_cast<int32_t>(offsetof(EdgeMeshHeader_t235B26AE87E4F8BC38EC627430A9E0E344A03878, ___vertexCount_5)); }
	inline int32_t get_vertexCount_5() const { return ___vertexCount_5; }
	inline int32_t* get_address_of_vertexCount_5() { return &___vertexCount_5; }
	inline void set_vertexCount_5(int32_t value)
	{
		___vertexCount_5 = value;
	}
};


// Obi.HeightFieldHeader
struct  HeightFieldHeader_t10BB39F6CE13CD0FBDC200A25AFD4000062D7E4E 
{
public:
	// System.Int32 Obi.HeightFieldHeader::firstSample
	int32_t ___firstSample_0;
	// System.Int32 Obi.HeightFieldHeader::sampleCount
	int32_t ___sampleCount_1;

public:
	inline static int32_t get_offset_of_firstSample_0() { return static_cast<int32_t>(offsetof(HeightFieldHeader_t10BB39F6CE13CD0FBDC200A25AFD4000062D7E4E, ___firstSample_0)); }
	inline int32_t get_firstSample_0() const { return ___firstSample_0; }
	inline int32_t* get_address_of_firstSample_0() { return &___firstSample_0; }
	inline void set_firstSample_0(int32_t value)
	{
		___firstSample_0 = value;
	}

	inline static int32_t get_offset_of_sampleCount_1() { return static_cast<int32_t>(offsetof(HeightFieldHeader_t10BB39F6CE13CD0FBDC200A25AFD4000062D7E4E, ___sampleCount_1)); }
	inline int32_t get_sampleCount_1() const { return ___sampleCount_1; }
	inline int32_t* get_address_of_sampleCount_1() { return &___sampleCount_1; }
	inline void set_sampleCount_1(int32_t value)
	{
		___sampleCount_1 = value;
	}
};


// Obi.TriangleMeshHeader
struct  TriangleMeshHeader_tD4F431BF8B26AB40AEE8CC5AF9C045A565607267 
{
public:
	// System.Int32 Obi.TriangleMeshHeader::firstNode
	int32_t ___firstNode_0;
	// System.Int32 Obi.TriangleMeshHeader::nodeCount
	int32_t ___nodeCount_1;
	// System.Int32 Obi.TriangleMeshHeader::firstTriangle
	int32_t ___firstTriangle_2;
	// System.Int32 Obi.TriangleMeshHeader::triangleCount
	int32_t ___triangleCount_3;
	// System.Int32 Obi.TriangleMeshHeader::firstVertex
	int32_t ___firstVertex_4;
	// System.Int32 Obi.TriangleMeshHeader::vertexCount
	int32_t ___vertexCount_5;

public:
	inline static int32_t get_offset_of_firstNode_0() { return static_cast<int32_t>(offsetof(TriangleMeshHeader_tD4F431BF8B26AB40AEE8CC5AF9C045A565607267, ___firstNode_0)); }
	inline int32_t get_firstNode_0() const { return ___firstNode_0; }
	inline int32_t* get_address_of_firstNode_0() { return &___firstNode_0; }
	inline void set_firstNode_0(int32_t value)
	{
		___firstNode_0 = value;
	}

	inline static int32_t get_offset_of_nodeCount_1() { return static_cast<int32_t>(offsetof(TriangleMeshHeader_tD4F431BF8B26AB40AEE8CC5AF9C045A565607267, ___nodeCount_1)); }
	inline int32_t get_nodeCount_1() const { return ___nodeCount_1; }
	inline int32_t* get_address_of_nodeCount_1() { return &___nodeCount_1; }
	inline void set_nodeCount_1(int32_t value)
	{
		___nodeCount_1 = value;
	}

	inline static int32_t get_offset_of_firstTriangle_2() { return static_cast<int32_t>(offsetof(TriangleMeshHeader_tD4F431BF8B26AB40AEE8CC5AF9C045A565607267, ___firstTriangle_2)); }
	inline int32_t get_firstTriangle_2() const { return ___firstTriangle_2; }
	inline int32_t* get_address_of_firstTriangle_2() { return &___firstTriangle_2; }
	inline void set_firstTriangle_2(int32_t value)
	{
		___firstTriangle_2 = value;
	}

	inline static int32_t get_offset_of_triangleCount_3() { return static_cast<int32_t>(offsetof(TriangleMeshHeader_tD4F431BF8B26AB40AEE8CC5AF9C045A565607267, ___triangleCount_3)); }
	inline int32_t get_triangleCount_3() const { return ___triangleCount_3; }
	inline int32_t* get_address_of_triangleCount_3() { return &___triangleCount_3; }
	inline void set_triangleCount_3(int32_t value)
	{
		___triangleCount_3 = value;
	}

	inline static int32_t get_offset_of_firstVertex_4() { return static_cast<int32_t>(offsetof(TriangleMeshHeader_tD4F431BF8B26AB40AEE8CC5AF9C045A565607267, ___firstVertex_4)); }
	inline int32_t get_firstVertex_4() const { return ___firstVertex_4; }
	inline int32_t* get_address_of_firstVertex_4() { return &___firstVertex_4; }
	inline void set_firstVertex_4(int32_t value)
	{
		___firstVertex_4 = value;
	}

	inline static int32_t get_offset_of_vertexCount_5() { return static_cast<int32_t>(offsetof(TriangleMeshHeader_tD4F431BF8B26AB40AEE8CC5AF9C045A565607267, ___vertexCount_5)); }
	inline int32_t get_vertexCount_5() const { return ___vertexCount_5; }
	inline int32_t* get_address_of_vertexCount_5() { return &___vertexCount_5; }
	inline void set_vertexCount_5(int32_t value)
	{
		___vertexCount_5 = value;
	}
};


// Obi.VInt4
struct  VInt4_t7102225B83232A71C9D784846D42B69BF440AD5D 
{
public:
	// System.Int32 Obi.VInt4::x
	int32_t ___x_0;
	// System.Int32 Obi.VInt4::y
	int32_t ___y_1;
	// System.Int32 Obi.VInt4::z
	int32_t ___z_2;
	// System.Int32 Obi.VInt4::w
	int32_t ___w_3;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(VInt4_t7102225B83232A71C9D784846D42B69BF440AD5D, ___x_0)); }
	inline int32_t get_x_0() const { return ___x_0; }
	inline int32_t* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(int32_t value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(VInt4_t7102225B83232A71C9D784846D42B69BF440AD5D, ___y_1)); }
	inline int32_t get_y_1() const { return ___y_1; }
	inline int32_t* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(int32_t value)
	{
		___y_1 = value;
	}

	inline static int32_t get_offset_of_z_2() { return static_cast<int32_t>(offsetof(VInt4_t7102225B83232A71C9D784846D42B69BF440AD5D, ___z_2)); }
	inline int32_t get_z_2() const { return ___z_2; }
	inline int32_t* get_address_of_z_2() { return &___z_2; }
	inline void set_z_2(int32_t value)
	{
		___z_2 = value;
	}

	inline static int32_t get_offset_of_w_3() { return static_cast<int32_t>(offsetof(VInt4_t7102225B83232A71C9D784846D42B69BF440AD5D, ___w_3)); }
	inline int32_t get_w_3() const { return ___w_3; }
	inline int32_t* get_address_of_w_3() { return &___w_3; }
	inline void set_w_3(int32_t value)
	{
		___w_3 = value;
	}
};


// System.Boolean
struct  Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37, ___m_value_0)); }
	inline bool get_m_value_0() const { return ___m_value_0; }
	inline bool* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(bool value)
	{
		___m_value_0 = value;
	}
};

struct Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields
{
public:
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_5;
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_6;

public:
	inline static int32_t get_offset_of_TrueString_5() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields, ___TrueString_5)); }
	inline String_t* get_TrueString_5() const { return ___TrueString_5; }
	inline String_t** get_address_of_TrueString_5() { return &___TrueString_5; }
	inline void set_TrueString_5(String_t* value)
	{
		___TrueString_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___TrueString_5), (void*)value);
	}

	inline static int32_t get_offset_of_FalseString_6() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields, ___FalseString_6)); }
	inline String_t* get_FalseString_6() const { return ___FalseString_6; }
	inline String_t** get_address_of_FalseString_6() { return &___FalseString_6; }
	inline void set_FalseString_6(String_t* value)
	{
		___FalseString_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FalseString_6), (void*)value);
	}
};


// System.Byte
struct  Byte_t0111FAB8B8685667EDDAF77683F0D8F86B659056 
{
public:
	// System.Byte System.Byte::m_value
	uint8_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Byte_t0111FAB8B8685667EDDAF77683F0D8F86B659056, ___m_value_0)); }
	inline uint8_t get_m_value_0() const { return ___m_value_0; }
	inline uint8_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(uint8_t value)
	{
		___m_value_0 = value;
	}
};


// System.Enum
struct  Enum_t23B90B40F60E677A8025267341651C94AE079CDA  : public ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52
{
public:

public:
};

struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t23B90B40F60E677A8025267341651C94AE079CDA_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___enumSeperatorCharArray_0), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_marshaled_com
{
};

// System.Int32
struct  Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046 
{
public:
	// System.Int32 System.Int32::m_value
	int32_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046, ___m_value_0)); }
	inline int32_t get_m_value_0() const { return ___m_value_0; }
	inline int32_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(int32_t value)
	{
		___m_value_0 = value;
	}
};


// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};


// System.Runtime.CompilerServices.AsyncMethodBuilderCore
struct  AsyncMethodBuilderCore_t2C85055E04767C52B9F66144476FCBF500DBFA34 
{
public:
	// System.Runtime.CompilerServices.IAsyncStateMachine System.Runtime.CompilerServices.AsyncMethodBuilderCore::m_stateMachine
	RuntimeObject* ___m_stateMachine_0;
	// System.Action System.Runtime.CompilerServices.AsyncMethodBuilderCore::m_defaultContextAction
	Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 * ___m_defaultContextAction_1;

public:
	inline static int32_t get_offset_of_m_stateMachine_0() { return static_cast<int32_t>(offsetof(AsyncMethodBuilderCore_t2C85055E04767C52B9F66144476FCBF500DBFA34, ___m_stateMachine_0)); }
	inline RuntimeObject* get_m_stateMachine_0() const { return ___m_stateMachine_0; }
	inline RuntimeObject** get_address_of_m_stateMachine_0() { return &___m_stateMachine_0; }
	inline void set_m_stateMachine_0(RuntimeObject* value)
	{
		___m_stateMachine_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_stateMachine_0), (void*)value);
	}

	inline static int32_t get_offset_of_m_defaultContextAction_1() { return static_cast<int32_t>(offsetof(AsyncMethodBuilderCore_t2C85055E04767C52B9F66144476FCBF500DBFA34, ___m_defaultContextAction_1)); }
	inline Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 * get_m_defaultContextAction_1() const { return ___m_defaultContextAction_1; }
	inline Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 ** get_address_of_m_defaultContextAction_1() { return &___m_defaultContextAction_1; }
	inline void set_m_defaultContextAction_1(Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 * value)
	{
		___m_defaultContextAction_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_defaultContextAction_1), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.Runtime.CompilerServices.AsyncMethodBuilderCore
struct AsyncMethodBuilderCore_t2C85055E04767C52B9F66144476FCBF500DBFA34_marshaled_pinvoke
{
	RuntimeObject* ___m_stateMachine_0;
	Il2CppMethodPointer ___m_defaultContextAction_1;
};
// Native definition for COM marshalling of System.Runtime.CompilerServices.AsyncMethodBuilderCore
struct AsyncMethodBuilderCore_t2C85055E04767C52B9F66144476FCBF500DBFA34_marshaled_com
{
	RuntimeObject* ___m_stateMachine_0;
	Il2CppMethodPointer ___m_defaultContextAction_1;
};

// System.Runtime.CompilerServices.ConfiguredTaskAwaitable`1_ConfiguredTaskAwaiter<System.Boolean>
struct  ConfiguredTaskAwaiter_t286C97C0AF102C4C0BE55CE2025CC7BD1FB5C20C 
{
public:
	// System.Threading.Tasks.Task`1<TResult> System.Runtime.CompilerServices.ConfiguredTaskAwaitable`1_ConfiguredTaskAwaiter::m_task
	Task_1_t9C1FE9F18F52F3409B9E970FA38801A443AE7849 * ___m_task_0;
	// System.Boolean System.Runtime.CompilerServices.ConfiguredTaskAwaitable`1_ConfiguredTaskAwaiter::m_continueOnCapturedContext
	bool ___m_continueOnCapturedContext_1;

public:
	inline static int32_t get_offset_of_m_task_0() { return static_cast<int32_t>(offsetof(ConfiguredTaskAwaiter_t286C97C0AF102C4C0BE55CE2025CC7BD1FB5C20C, ___m_task_0)); }
	inline Task_1_t9C1FE9F18F52F3409B9E970FA38801A443AE7849 * get_m_task_0() const { return ___m_task_0; }
	inline Task_1_t9C1FE9F18F52F3409B9E970FA38801A443AE7849 ** get_address_of_m_task_0() { return &___m_task_0; }
	inline void set_m_task_0(Task_1_t9C1FE9F18F52F3409B9E970FA38801A443AE7849 * value)
	{
		___m_task_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_task_0), (void*)value);
	}

	inline static int32_t get_offset_of_m_continueOnCapturedContext_1() { return static_cast<int32_t>(offsetof(ConfiguredTaskAwaiter_t286C97C0AF102C4C0BE55CE2025CC7BD1FB5C20C, ___m_continueOnCapturedContext_1)); }
	inline bool get_m_continueOnCapturedContext_1() const { return ___m_continueOnCapturedContext_1; }
	inline bool* get_address_of_m_continueOnCapturedContext_1() { return &___m_continueOnCapturedContext_1; }
	inline void set_m_continueOnCapturedContext_1(bool value)
	{
		___m_continueOnCapturedContext_1 = value;
	}
};


// System.Runtime.CompilerServices.ConfiguredTaskAwaitable`1_ConfiguredTaskAwaiter<System.Object>
struct  ConfiguredTaskAwaiter_t2CE498F9A6CE5405242AE2D77F03E58985B7C3ED 
{
public:
	// System.Threading.Tasks.Task`1<TResult> System.Runtime.CompilerServices.ConfiguredTaskAwaitable`1_ConfiguredTaskAwaiter::m_task
	Task_1_tC1805497876E88B78A2B0CB81C6409E0B381AC17 * ___m_task_0;
	// System.Boolean System.Runtime.CompilerServices.ConfiguredTaskAwaitable`1_ConfiguredTaskAwaiter::m_continueOnCapturedContext
	bool ___m_continueOnCapturedContext_1;

public:
	inline static int32_t get_offset_of_m_task_0() { return static_cast<int32_t>(offsetof(ConfiguredTaskAwaiter_t2CE498F9A6CE5405242AE2D77F03E58985B7C3ED, ___m_task_0)); }
	inline Task_1_tC1805497876E88B78A2B0CB81C6409E0B381AC17 * get_m_task_0() const { return ___m_task_0; }
	inline Task_1_tC1805497876E88B78A2B0CB81C6409E0B381AC17 ** get_address_of_m_task_0() { return &___m_task_0; }
	inline void set_m_task_0(Task_1_tC1805497876E88B78A2B0CB81C6409E0B381AC17 * value)
	{
		___m_task_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_task_0), (void*)value);
	}

	inline static int32_t get_offset_of_m_continueOnCapturedContext_1() { return static_cast<int32_t>(offsetof(ConfiguredTaskAwaiter_t2CE498F9A6CE5405242AE2D77F03E58985B7C3ED, ___m_continueOnCapturedContext_1)); }
	inline bool get_m_continueOnCapturedContext_1() const { return ___m_continueOnCapturedContext_1; }
	inline bool* get_address_of_m_continueOnCapturedContext_1() { return &___m_continueOnCapturedContext_1; }
	inline void set_m_continueOnCapturedContext_1(bool value)
	{
		___m_continueOnCapturedContext_1 = value;
	}
};


// System.Runtime.CompilerServices.ConfiguredTaskAwaitable`1_ConfiguredTaskAwaiter<System.Threading.Tasks.Task>
struct  ConfiguredTaskAwaiter_tF64824CB5C3CFE2E1C4CAFE410B4CDE6831E4C78 
{
public:
	// System.Threading.Tasks.Task`1<TResult> System.Runtime.CompilerServices.ConfiguredTaskAwaitable`1_ConfiguredTaskAwaiter::m_task
	Task_1_t24E932728D4BE67BFA41487F43AE4FAEBBAC7284 * ___m_task_0;
	// System.Boolean System.Runtime.CompilerServices.ConfiguredTaskAwaitable`1_ConfiguredTaskAwaiter::m_continueOnCapturedContext
	bool ___m_continueOnCapturedContext_1;

public:
	inline static int32_t get_offset_of_m_task_0() { return static_cast<int32_t>(offsetof(ConfiguredTaskAwaiter_tF64824CB5C3CFE2E1C4CAFE410B4CDE6831E4C78, ___m_task_0)); }
	inline Task_1_t24E932728D4BE67BFA41487F43AE4FAEBBAC7284 * get_m_task_0() const { return ___m_task_0; }
	inline Task_1_t24E932728D4BE67BFA41487F43AE4FAEBBAC7284 ** get_address_of_m_task_0() { return &___m_task_0; }
	inline void set_m_task_0(Task_1_t24E932728D4BE67BFA41487F43AE4FAEBBAC7284 * value)
	{
		___m_task_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_task_0), (void*)value);
	}

	inline static int32_t get_offset_of_m_continueOnCapturedContext_1() { return static_cast<int32_t>(offsetof(ConfiguredTaskAwaiter_tF64824CB5C3CFE2E1C4CAFE410B4CDE6831E4C78, ___m_continueOnCapturedContext_1)); }
	inline bool get_m_continueOnCapturedContext_1() const { return ___m_continueOnCapturedContext_1; }
	inline bool* get_address_of_m_continueOnCapturedContext_1() { return &___m_continueOnCapturedContext_1; }
	inline void set_m_continueOnCapturedContext_1(bool value)
	{
		___m_continueOnCapturedContext_1 = value;
	}
};


// System.Single
struct  Single_tE07797BA3C98D4CA9B5A19413C19A76688AB899E 
{
public:
	// System.Single System.Single::m_value
	float ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Single_tE07797BA3C98D4CA9B5A19413C19A76688AB899E, ___m_value_0)); }
	inline float get_m_value_0() const { return ___m_value_0; }
	inline float* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(float value)
	{
		___m_value_0 = value;
	}
};


// System.Threading.CancellationToken
struct  CancellationToken_tC9D68381C9164A4BA10397257E87ADC832AF5FFD 
{
public:
	// System.Threading.CancellationTokenSource System.Threading.CancellationToken::m_source
	CancellationTokenSource_t78B989179DE23EDD36F870FFEE20A15D6D3C65B3 * ___m_source_0;

public:
	inline static int32_t get_offset_of_m_source_0() { return static_cast<int32_t>(offsetof(CancellationToken_tC9D68381C9164A4BA10397257E87ADC832AF5FFD, ___m_source_0)); }
	inline CancellationTokenSource_t78B989179DE23EDD36F870FFEE20A15D6D3C65B3 * get_m_source_0() const { return ___m_source_0; }
	inline CancellationTokenSource_t78B989179DE23EDD36F870FFEE20A15D6D3C65B3 ** get_address_of_m_source_0() { return &___m_source_0; }
	inline void set_m_source_0(CancellationTokenSource_t78B989179DE23EDD36F870FFEE20A15D6D3C65B3 * value)
	{
		___m_source_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_source_0), (void*)value);
	}
};

struct CancellationToken_tC9D68381C9164A4BA10397257E87ADC832AF5FFD_StaticFields
{
public:
	// System.Action`1<System.Object> System.Threading.CancellationToken::s_ActionToActionObjShunt
	Action_1_tD9663D9715FAA4E62035CFCF1AD4D094EE7872DC * ___s_ActionToActionObjShunt_1;

public:
	inline static int32_t get_offset_of_s_ActionToActionObjShunt_1() { return static_cast<int32_t>(offsetof(CancellationToken_tC9D68381C9164A4BA10397257E87ADC832AF5FFD_StaticFields, ___s_ActionToActionObjShunt_1)); }
	inline Action_1_tD9663D9715FAA4E62035CFCF1AD4D094EE7872DC * get_s_ActionToActionObjShunt_1() const { return ___s_ActionToActionObjShunt_1; }
	inline Action_1_tD9663D9715FAA4E62035CFCF1AD4D094EE7872DC ** get_address_of_s_ActionToActionObjShunt_1() { return &___s_ActionToActionObjShunt_1; }
	inline void set_s_ActionToActionObjShunt_1(Action_1_tD9663D9715FAA4E62035CFCF1AD4D094EE7872DC * value)
	{
		___s_ActionToActionObjShunt_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_ActionToActionObjShunt_1), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.Threading.CancellationToken
struct CancellationToken_tC9D68381C9164A4BA10397257E87ADC832AF5FFD_marshaled_pinvoke
{
	CancellationTokenSource_t78B989179DE23EDD36F870FFEE20A15D6D3C65B3 * ___m_source_0;
};
// Native definition for COM marshalling of System.Threading.CancellationToken
struct CancellationToken_tC9D68381C9164A4BA10397257E87ADC832AF5FFD_marshaled_com
{
	CancellationTokenSource_t78B989179DE23EDD36F870FFEE20A15D6D3C65B3 * ___m_source_0;
};

// System.Threading.ExecutionContext_Reader
struct  Reader_t6C70587C0F5A8CE8367A0407E3109E196764848C 
{
public:
	// System.Threading.ExecutionContext System.Threading.ExecutionContext_Reader::m_ec
	ExecutionContext_t16AC73BB21FEEEAD34A017877AC18DD8BB836414 * ___m_ec_0;

public:
	inline static int32_t get_offset_of_m_ec_0() { return static_cast<int32_t>(offsetof(Reader_t6C70587C0F5A8CE8367A0407E3109E196764848C, ___m_ec_0)); }
	inline ExecutionContext_t16AC73BB21FEEEAD34A017877AC18DD8BB836414 * get_m_ec_0() const { return ___m_ec_0; }
	inline ExecutionContext_t16AC73BB21FEEEAD34A017877AC18DD8BB836414 ** get_address_of_m_ec_0() { return &___m_ec_0; }
	inline void set_m_ec_0(ExecutionContext_t16AC73BB21FEEEAD34A017877AC18DD8BB836414 * value)
	{
		___m_ec_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_ec_0), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.Threading.ExecutionContext/Reader
struct Reader_t6C70587C0F5A8CE8367A0407E3109E196764848C_marshaled_pinvoke
{
	ExecutionContext_t16AC73BB21FEEEAD34A017877AC18DD8BB836414 * ___m_ec_0;
};
// Native definition for COM marshalling of System.Threading.ExecutionContext/Reader
struct Reader_t6C70587C0F5A8CE8367A0407E3109E196764848C_marshaled_com
{
	ExecutionContext_t16AC73BB21FEEEAD34A017877AC18DD8BB836414 * ___m_ec_0;
};

// System.Void
struct  Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5__padding[1];
	};

public:
};


// UnityEngine.Experimental.GlobalIllumination.LinearColor
struct  LinearColor_tB134EA090C61E6624DE36F52980CA7E2C893D5F2 
{
public:
	// System.Single UnityEngine.Experimental.GlobalIllumination.LinearColor::m_red
	float ___m_red_0;
	// System.Single UnityEngine.Experimental.GlobalIllumination.LinearColor::m_green
	float ___m_green_1;
	// System.Single UnityEngine.Experimental.GlobalIllumination.LinearColor::m_blue
	float ___m_blue_2;
	// System.Single UnityEngine.Experimental.GlobalIllumination.LinearColor::m_intensity
	float ___m_intensity_3;

public:
	inline static int32_t get_offset_of_m_red_0() { return static_cast<int32_t>(offsetof(LinearColor_tB134EA090C61E6624DE36F52980CA7E2C893D5F2, ___m_red_0)); }
	inline float get_m_red_0() const { return ___m_red_0; }
	inline float* get_address_of_m_red_0() { return &___m_red_0; }
	inline void set_m_red_0(float value)
	{
		___m_red_0 = value;
	}

	inline static int32_t get_offset_of_m_green_1() { return static_cast<int32_t>(offsetof(LinearColor_tB134EA090C61E6624DE36F52980CA7E2C893D5F2, ___m_green_1)); }
	inline float get_m_green_1() const { return ___m_green_1; }
	inline float* get_address_of_m_green_1() { return &___m_green_1; }
	inline void set_m_green_1(float value)
	{
		___m_green_1 = value;
	}

	inline static int32_t get_offset_of_m_blue_2() { return static_cast<int32_t>(offsetof(LinearColor_tB134EA090C61E6624DE36F52980CA7E2C893D5F2, ___m_blue_2)); }
	inline float get_m_blue_2() const { return ___m_blue_2; }
	inline float* get_address_of_m_blue_2() { return &___m_blue_2; }
	inline void set_m_blue_2(float value)
	{
		___m_blue_2 = value;
	}

	inline static int32_t get_offset_of_m_intensity_3() { return static_cast<int32_t>(offsetof(LinearColor_tB134EA090C61E6624DE36F52980CA7E2C893D5F2, ___m_intensity_3)); }
	inline float get_m_intensity_3() const { return ___m_intensity_3; }
	inline float* get_address_of_m_intensity_3() { return &___m_intensity_3; }
	inline void set_m_intensity_3(float value)
	{
		___m_intensity_3 = value;
	}
};


// UnityEngine.Matrix4x4
struct  Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461 
{
public:
	// System.Single UnityEngine.Matrix4x4::m00
	float ___m00_0;
	// System.Single UnityEngine.Matrix4x4::m10
	float ___m10_1;
	// System.Single UnityEngine.Matrix4x4::m20
	float ___m20_2;
	// System.Single UnityEngine.Matrix4x4::m30
	float ___m30_3;
	// System.Single UnityEngine.Matrix4x4::m01
	float ___m01_4;
	// System.Single UnityEngine.Matrix4x4::m11
	float ___m11_5;
	// System.Single UnityEngine.Matrix4x4::m21
	float ___m21_6;
	// System.Single UnityEngine.Matrix4x4::m31
	float ___m31_7;
	// System.Single UnityEngine.Matrix4x4::m02
	float ___m02_8;
	// System.Single UnityEngine.Matrix4x4::m12
	float ___m12_9;
	// System.Single UnityEngine.Matrix4x4::m22
	float ___m22_10;
	// System.Single UnityEngine.Matrix4x4::m32
	float ___m32_11;
	// System.Single UnityEngine.Matrix4x4::m03
	float ___m03_12;
	// System.Single UnityEngine.Matrix4x4::m13
	float ___m13_13;
	// System.Single UnityEngine.Matrix4x4::m23
	float ___m23_14;
	// System.Single UnityEngine.Matrix4x4::m33
	float ___m33_15;

public:
	inline static int32_t get_offset_of_m00_0() { return static_cast<int32_t>(offsetof(Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461, ___m00_0)); }
	inline float get_m00_0() const { return ___m00_0; }
	inline float* get_address_of_m00_0() { return &___m00_0; }
	inline void set_m00_0(float value)
	{
		___m00_0 = value;
	}

	inline static int32_t get_offset_of_m10_1() { return static_cast<int32_t>(offsetof(Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461, ___m10_1)); }
	inline float get_m10_1() const { return ___m10_1; }
	inline float* get_address_of_m10_1() { return &___m10_1; }
	inline void set_m10_1(float value)
	{
		___m10_1 = value;
	}

	inline static int32_t get_offset_of_m20_2() { return static_cast<int32_t>(offsetof(Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461, ___m20_2)); }
	inline float get_m20_2() const { return ___m20_2; }
	inline float* get_address_of_m20_2() { return &___m20_2; }
	inline void set_m20_2(float value)
	{
		___m20_2 = value;
	}

	inline static int32_t get_offset_of_m30_3() { return static_cast<int32_t>(offsetof(Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461, ___m30_3)); }
	inline float get_m30_3() const { return ___m30_3; }
	inline float* get_address_of_m30_3() { return &___m30_3; }
	inline void set_m30_3(float value)
	{
		___m30_3 = value;
	}

	inline static int32_t get_offset_of_m01_4() { return static_cast<int32_t>(offsetof(Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461, ___m01_4)); }
	inline float get_m01_4() const { return ___m01_4; }
	inline float* get_address_of_m01_4() { return &___m01_4; }
	inline void set_m01_4(float value)
	{
		___m01_4 = value;
	}

	inline static int32_t get_offset_of_m11_5() { return static_cast<int32_t>(offsetof(Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461, ___m11_5)); }
	inline float get_m11_5() const { return ___m11_5; }
	inline float* get_address_of_m11_5() { return &___m11_5; }
	inline void set_m11_5(float value)
	{
		___m11_5 = value;
	}

	inline static int32_t get_offset_of_m21_6() { return static_cast<int32_t>(offsetof(Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461, ___m21_6)); }
	inline float get_m21_6() const { return ___m21_6; }
	inline float* get_address_of_m21_6() { return &___m21_6; }
	inline void set_m21_6(float value)
	{
		___m21_6 = value;
	}

	inline static int32_t get_offset_of_m31_7() { return static_cast<int32_t>(offsetof(Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461, ___m31_7)); }
	inline float get_m31_7() const { return ___m31_7; }
	inline float* get_address_of_m31_7() { return &___m31_7; }
	inline void set_m31_7(float value)
	{
		___m31_7 = value;
	}

	inline static int32_t get_offset_of_m02_8() { return static_cast<int32_t>(offsetof(Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461, ___m02_8)); }
	inline float get_m02_8() const { return ___m02_8; }
	inline float* get_address_of_m02_8() { return &___m02_8; }
	inline void set_m02_8(float value)
	{
		___m02_8 = value;
	}

	inline static int32_t get_offset_of_m12_9() { return static_cast<int32_t>(offsetof(Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461, ___m12_9)); }
	inline float get_m12_9() const { return ___m12_9; }
	inline float* get_address_of_m12_9() { return &___m12_9; }
	inline void set_m12_9(float value)
	{
		___m12_9 = value;
	}

	inline static int32_t get_offset_of_m22_10() { return static_cast<int32_t>(offsetof(Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461, ___m22_10)); }
	inline float get_m22_10() const { return ___m22_10; }
	inline float* get_address_of_m22_10() { return &___m22_10; }
	inline void set_m22_10(float value)
	{
		___m22_10 = value;
	}

	inline static int32_t get_offset_of_m32_11() { return static_cast<int32_t>(offsetof(Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461, ___m32_11)); }
	inline float get_m32_11() const { return ___m32_11; }
	inline float* get_address_of_m32_11() { return &___m32_11; }
	inline void set_m32_11(float value)
	{
		___m32_11 = value;
	}

	inline static int32_t get_offset_of_m03_12() { return static_cast<int32_t>(offsetof(Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461, ___m03_12)); }
	inline float get_m03_12() const { return ___m03_12; }
	inline float* get_address_of_m03_12() { return &___m03_12; }
	inline void set_m03_12(float value)
	{
		___m03_12 = value;
	}

	inline static int32_t get_offset_of_m13_13() { return static_cast<int32_t>(offsetof(Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461, ___m13_13)); }
	inline float get_m13_13() const { return ___m13_13; }
	inline float* get_address_of_m13_13() { return &___m13_13; }
	inline void set_m13_13(float value)
	{
		___m13_13 = value;
	}

	inline static int32_t get_offset_of_m23_14() { return static_cast<int32_t>(offsetof(Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461, ___m23_14)); }
	inline float get_m23_14() const { return ___m23_14; }
	inline float* get_address_of_m23_14() { return &___m23_14; }
	inline void set_m23_14(float value)
	{
		___m23_14 = value;
	}

	inline static int32_t get_offset_of_m33_15() { return static_cast<int32_t>(offsetof(Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461, ___m33_15)); }
	inline float get_m33_15() const { return ___m33_15; }
	inline float* get_address_of_m33_15() { return &___m33_15; }
	inline void set_m33_15(float value)
	{
		___m33_15 = value;
	}
};

struct Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461_StaticFields
{
public:
	// UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::zeroMatrix
	Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461  ___zeroMatrix_16;
	// UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::identityMatrix
	Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461  ___identityMatrix_17;

public:
	inline static int32_t get_offset_of_zeroMatrix_16() { return static_cast<int32_t>(offsetof(Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461_StaticFields, ___zeroMatrix_16)); }
	inline Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461  get_zeroMatrix_16() const { return ___zeroMatrix_16; }
	inline Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461 * get_address_of_zeroMatrix_16() { return &___zeroMatrix_16; }
	inline void set_zeroMatrix_16(Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461  value)
	{
		___zeroMatrix_16 = value;
	}

	inline static int32_t get_offset_of_identityMatrix_17() { return static_cast<int32_t>(offsetof(Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461_StaticFields, ___identityMatrix_17)); }
	inline Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461  get_identityMatrix_17() const { return ___identityMatrix_17; }
	inline Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461 * get_address_of_identityMatrix_17() { return &___identityMatrix_17; }
	inline void set_identityMatrix_17(Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461  value)
	{
		___identityMatrix_17 = value;
	}
};


// UnityEngine.Quaternion
struct  Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4 
{
public:
	// System.Single UnityEngine.Quaternion::x
	float ___x_0;
	// System.Single UnityEngine.Quaternion::y
	float ___y_1;
	// System.Single UnityEngine.Quaternion::z
	float ___z_2;
	// System.Single UnityEngine.Quaternion::w
	float ___w_3;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}

	inline static int32_t get_offset_of_z_2() { return static_cast<int32_t>(offsetof(Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4, ___z_2)); }
	inline float get_z_2() const { return ___z_2; }
	inline float* get_address_of_z_2() { return &___z_2; }
	inline void set_z_2(float value)
	{
		___z_2 = value;
	}

	inline static int32_t get_offset_of_w_3() { return static_cast<int32_t>(offsetof(Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4, ___w_3)); }
	inline float get_w_3() const { return ___w_3; }
	inline float* get_address_of_w_3() { return &___w_3; }
	inline void set_w_3(float value)
	{
		___w_3 = value;
	}
};

struct Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4_StaticFields
{
public:
	// UnityEngine.Quaternion UnityEngine.Quaternion::identityQuaternion
	Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  ___identityQuaternion_4;

public:
	inline static int32_t get_offset_of_identityQuaternion_4() { return static_cast<int32_t>(offsetof(Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4_StaticFields, ___identityQuaternion_4)); }
	inline Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  get_identityQuaternion_4() const { return ___identityQuaternion_4; }
	inline Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4 * get_address_of_identityQuaternion_4() { return &___identityQuaternion_4; }
	inline void set_identityQuaternion_4(Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  value)
	{
		___identityQuaternion_4 = value;
	}
};


// UnityEngine.Rendering.BatchVisibility
struct  BatchVisibility_tFA63D052426424FBD58F78E973AAAC52A67B5AFE 
{
public:
	// System.Int32 UnityEngine.Rendering.BatchVisibility::offset
	int32_t ___offset_0;
	// System.Int32 UnityEngine.Rendering.BatchVisibility::instancesCount
	int32_t ___instancesCount_1;
	// System.Int32 UnityEngine.Rendering.BatchVisibility::visibleCount
	int32_t ___visibleCount_2;

public:
	inline static int32_t get_offset_of_offset_0() { return static_cast<int32_t>(offsetof(BatchVisibility_tFA63D052426424FBD58F78E973AAAC52A67B5AFE, ___offset_0)); }
	inline int32_t get_offset_0() const { return ___offset_0; }
	inline int32_t* get_address_of_offset_0() { return &___offset_0; }
	inline void set_offset_0(int32_t value)
	{
		___offset_0 = value;
	}

	inline static int32_t get_offset_of_instancesCount_1() { return static_cast<int32_t>(offsetof(BatchVisibility_tFA63D052426424FBD58F78E973AAAC52A67B5AFE, ___instancesCount_1)); }
	inline int32_t get_instancesCount_1() const { return ___instancesCount_1; }
	inline int32_t* get_address_of_instancesCount_1() { return &___instancesCount_1; }
	inline void set_instancesCount_1(int32_t value)
	{
		___instancesCount_1 = value;
	}

	inline static int32_t get_offset_of_visibleCount_2() { return static_cast<int32_t>(offsetof(BatchVisibility_tFA63D052426424FBD58F78E973AAAC52A67B5AFE, ___visibleCount_2)); }
	inline int32_t get_visibleCount_2() const { return ___visibleCount_2; }
	inline int32_t* get_address_of_visibleCount_2() { return &___visibleCount_2; }
	inline void set_visibleCount_2(int32_t value)
	{
		___visibleCount_2 = value;
	}
};


// UnityEngine.Vector2
struct  Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 
{
public:
	// System.Single UnityEngine.Vector2::x
	float ___x_0;
	// System.Single UnityEngine.Vector2::y
	float ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}
};

struct Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields
{
public:
	// UnityEngine.Vector2 UnityEngine.Vector2::zeroVector
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___zeroVector_2;
	// UnityEngine.Vector2 UnityEngine.Vector2::oneVector
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___oneVector_3;
	// UnityEngine.Vector2 UnityEngine.Vector2::upVector
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___upVector_4;
	// UnityEngine.Vector2 UnityEngine.Vector2::downVector
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___downVector_5;
	// UnityEngine.Vector2 UnityEngine.Vector2::leftVector
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___leftVector_6;
	// UnityEngine.Vector2 UnityEngine.Vector2::rightVector
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___rightVector_7;
	// UnityEngine.Vector2 UnityEngine.Vector2::positiveInfinityVector
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___positiveInfinityVector_8;
	// UnityEngine.Vector2 UnityEngine.Vector2::negativeInfinityVector
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___negativeInfinityVector_9;

public:
	inline static int32_t get_offset_of_zeroVector_2() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields, ___zeroVector_2)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_zeroVector_2() const { return ___zeroVector_2; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_zeroVector_2() { return &___zeroVector_2; }
	inline void set_zeroVector_2(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___zeroVector_2 = value;
	}

	inline static int32_t get_offset_of_oneVector_3() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields, ___oneVector_3)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_oneVector_3() const { return ___oneVector_3; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_oneVector_3() { return &___oneVector_3; }
	inline void set_oneVector_3(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___oneVector_3 = value;
	}

	inline static int32_t get_offset_of_upVector_4() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields, ___upVector_4)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_upVector_4() const { return ___upVector_4; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_upVector_4() { return &___upVector_4; }
	inline void set_upVector_4(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___upVector_4 = value;
	}

	inline static int32_t get_offset_of_downVector_5() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields, ___downVector_5)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_downVector_5() const { return ___downVector_5; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_downVector_5() { return &___downVector_5; }
	inline void set_downVector_5(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___downVector_5 = value;
	}

	inline static int32_t get_offset_of_leftVector_6() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields, ___leftVector_6)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_leftVector_6() const { return ___leftVector_6; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_leftVector_6() { return &___leftVector_6; }
	inline void set_leftVector_6(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___leftVector_6 = value;
	}

	inline static int32_t get_offset_of_rightVector_7() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields, ___rightVector_7)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_rightVector_7() const { return ___rightVector_7; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_rightVector_7() { return &___rightVector_7; }
	inline void set_rightVector_7(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___rightVector_7 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields, ___positiveInfinityVector_8)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_positiveInfinityVector_8() const { return ___positiveInfinityVector_8; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_positiveInfinityVector_8() { return &___positiveInfinityVector_8; }
	inline void set_positiveInfinityVector_8(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___positiveInfinityVector_8 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_9() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields, ___negativeInfinityVector_9)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_negativeInfinityVector_9() const { return ___negativeInfinityVector_9; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_negativeInfinityVector_9() { return &___negativeInfinityVector_9; }
	inline void set_negativeInfinityVector_9(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___negativeInfinityVector_9 = value;
	}
};


// UnityEngine.Vector3
struct  Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_2;
	// System.Single UnityEngine.Vector3::y
	float ___y_3;
	// System.Single UnityEngine.Vector3::z
	float ___z_4;

public:
	inline static int32_t get_offset_of_x_2() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E, ___x_2)); }
	inline float get_x_2() const { return ___x_2; }
	inline float* get_address_of_x_2() { return &___x_2; }
	inline void set_x_2(float value)
	{
		___x_2 = value;
	}

	inline static int32_t get_offset_of_y_3() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E, ___y_3)); }
	inline float get_y_3() const { return ___y_3; }
	inline float* get_address_of_y_3() { return &___y_3; }
	inline void set_y_3(float value)
	{
		___y_3 = value;
	}

	inline static int32_t get_offset_of_z_4() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E, ___z_4)); }
	inline float get_z_4() const { return ___z_4; }
	inline float* get_address_of_z_4() { return &___z_4; }
	inline void set_z_4(float value)
	{
		___z_4 = value;
	}
};

struct Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___zeroVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___oneVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___upVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___downVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___leftVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___rightVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___forwardVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___backVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___positiveInfinityVector_13;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___negativeInfinityVector_14;

public:
	inline static int32_t get_offset_of_zeroVector_5() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___zeroVector_5)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_zeroVector_5() const { return ___zeroVector_5; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_zeroVector_5() { return &___zeroVector_5; }
	inline void set_zeroVector_5(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___zeroVector_5 = value;
	}

	inline static int32_t get_offset_of_oneVector_6() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___oneVector_6)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_oneVector_6() const { return ___oneVector_6; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_oneVector_6() { return &___oneVector_6; }
	inline void set_oneVector_6(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___oneVector_6 = value;
	}

	inline static int32_t get_offset_of_upVector_7() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___upVector_7)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_upVector_7() const { return ___upVector_7; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_upVector_7() { return &___upVector_7; }
	inline void set_upVector_7(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___upVector_7 = value;
	}

	inline static int32_t get_offset_of_downVector_8() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___downVector_8)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_downVector_8() const { return ___downVector_8; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_downVector_8() { return &___downVector_8; }
	inline void set_downVector_8(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___downVector_8 = value;
	}

	inline static int32_t get_offset_of_leftVector_9() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___leftVector_9)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_leftVector_9() const { return ___leftVector_9; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_leftVector_9() { return &___leftVector_9; }
	inline void set_leftVector_9(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___leftVector_9 = value;
	}

	inline static int32_t get_offset_of_rightVector_10() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___rightVector_10)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_rightVector_10() const { return ___rightVector_10; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_rightVector_10() { return &___rightVector_10; }
	inline void set_rightVector_10(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___rightVector_10 = value;
	}

	inline static int32_t get_offset_of_forwardVector_11() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___forwardVector_11)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_forwardVector_11() const { return ___forwardVector_11; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_forwardVector_11() { return &___forwardVector_11; }
	inline void set_forwardVector_11(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___forwardVector_11 = value;
	}

	inline static int32_t get_offset_of_backVector_12() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___backVector_12)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_backVector_12() const { return ___backVector_12; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_backVector_12() { return &___backVector_12; }
	inline void set_backVector_12(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___backVector_12 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___positiveInfinityVector_13)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_positiveInfinityVector_13() const { return ___positiveInfinityVector_13; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_positiveInfinityVector_13() { return &___positiveInfinityVector_13; }
	inline void set_positiveInfinityVector_13(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___positiveInfinityVector_13 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_14() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___negativeInfinityVector_14)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_negativeInfinityVector_14() const { return ___negativeInfinityVector_14; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_negativeInfinityVector_14() { return &___negativeInfinityVector_14; }
	inline void set_negativeInfinityVector_14(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___negativeInfinityVector_14 = value;
	}
};


// UnityEngine.Vector4
struct  Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7 
{
public:
	// System.Single UnityEngine.Vector4::x
	float ___x_1;
	// System.Single UnityEngine.Vector4::y
	float ___y_2;
	// System.Single UnityEngine.Vector4::z
	float ___z_3;
	// System.Single UnityEngine.Vector4::w
	float ___w_4;

public:
	inline static int32_t get_offset_of_x_1() { return static_cast<int32_t>(offsetof(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7, ___x_1)); }
	inline float get_x_1() const { return ___x_1; }
	inline float* get_address_of_x_1() { return &___x_1; }
	inline void set_x_1(float value)
	{
		___x_1 = value;
	}

	inline static int32_t get_offset_of_y_2() { return static_cast<int32_t>(offsetof(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7, ___y_2)); }
	inline float get_y_2() const { return ___y_2; }
	inline float* get_address_of_y_2() { return &___y_2; }
	inline void set_y_2(float value)
	{
		___y_2 = value;
	}

	inline static int32_t get_offset_of_z_3() { return static_cast<int32_t>(offsetof(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7, ___z_3)); }
	inline float get_z_3() const { return ___z_3; }
	inline float* get_address_of_z_3() { return &___z_3; }
	inline void set_z_3(float value)
	{
		___z_3 = value;
	}

	inline static int32_t get_offset_of_w_4() { return static_cast<int32_t>(offsetof(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7, ___w_4)); }
	inline float get_w_4() const { return ___w_4; }
	inline float* get_address_of_w_4() { return &___w_4; }
	inline void set_w_4(float value)
	{
		___w_4 = value;
	}
};

struct Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7_StaticFields
{
public:
	// UnityEngine.Vector4 UnityEngine.Vector4::zeroVector
	Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  ___zeroVector_5;
	// UnityEngine.Vector4 UnityEngine.Vector4::oneVector
	Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  ___oneVector_6;
	// UnityEngine.Vector4 UnityEngine.Vector4::positiveInfinityVector
	Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  ___positiveInfinityVector_7;
	// UnityEngine.Vector4 UnityEngine.Vector4::negativeInfinityVector
	Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  ___negativeInfinityVector_8;

public:
	inline static int32_t get_offset_of_zeroVector_5() { return static_cast<int32_t>(offsetof(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7_StaticFields, ___zeroVector_5)); }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  get_zeroVector_5() const { return ___zeroVector_5; }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7 * get_address_of_zeroVector_5() { return &___zeroVector_5; }
	inline void set_zeroVector_5(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  value)
	{
		___zeroVector_5 = value;
	}

	inline static int32_t get_offset_of_oneVector_6() { return static_cast<int32_t>(offsetof(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7_StaticFields, ___oneVector_6)); }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  get_oneVector_6() const { return ___oneVector_6; }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7 * get_address_of_oneVector_6() { return &___oneVector_6; }
	inline void set_oneVector_6(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  value)
	{
		___oneVector_6 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_7() { return static_cast<int32_t>(offsetof(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7_StaticFields, ___positiveInfinityVector_7)); }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  get_positiveInfinityVector_7() const { return ___positiveInfinityVector_7; }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7 * get_address_of_positiveInfinityVector_7() { return &___positiveInfinityVector_7; }
	inline void set_positiveInfinityVector_7(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  value)
	{
		___positiveInfinityVector_7 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7_StaticFields, ___negativeInfinityVector_8)); }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  get_negativeInfinityVector_8() const { return ___negativeInfinityVector_8; }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7 * get_address_of_negativeInfinityVector_8() { return &___negativeInfinityVector_8; }
	inline void set_negativeInfinityVector_8(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  value)
	{
		___negativeInfinityVector_8 = value;
	}
};


// Obi.Aabb
struct  Aabb_t4DF77A399A9E4CB5D52707E4B8B53D3483CD84CF 
{
public:
	// UnityEngine.Vector4 Obi.Aabb::min
	Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  ___min_0;
	// UnityEngine.Vector4 Obi.Aabb::max
	Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  ___max_1;

public:
	inline static int32_t get_offset_of_min_0() { return static_cast<int32_t>(offsetof(Aabb_t4DF77A399A9E4CB5D52707E4B8B53D3483CD84CF, ___min_0)); }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  get_min_0() const { return ___min_0; }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7 * get_address_of_min_0() { return &___min_0; }
	inline void set_min_0(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  value)
	{
		___min_0 = value;
	}

	inline static int32_t get_offset_of_max_1() { return static_cast<int32_t>(offsetof(Aabb_t4DF77A399A9E4CB5D52707E4B8B53D3483CD84CF, ___max_1)); }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  get_max_1() const { return ___max_1; }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7 * get_address_of_max_1() { return &___max_1; }
	inline void set_max_1(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  value)
	{
		___max_1 = value;
	}
};


// Obi.AffineTransform
struct  AffineTransform_tC1AED177DBAAED2A3AD2CCA40394AAA8C3DF0193 
{
public:
	// UnityEngine.Vector4 Obi.AffineTransform::translation
	Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  ___translation_0;
	// UnityEngine.Vector4 Obi.AffineTransform::scale
	Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  ___scale_1;
	// UnityEngine.Quaternion Obi.AffineTransform::rotation
	Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  ___rotation_2;

public:
	inline static int32_t get_offset_of_translation_0() { return static_cast<int32_t>(offsetof(AffineTransform_tC1AED177DBAAED2A3AD2CCA40394AAA8C3DF0193, ___translation_0)); }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  get_translation_0() const { return ___translation_0; }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7 * get_address_of_translation_0() { return &___translation_0; }
	inline void set_translation_0(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  value)
	{
		___translation_0 = value;
	}

	inline static int32_t get_offset_of_scale_1() { return static_cast<int32_t>(offsetof(AffineTransform_tC1AED177DBAAED2A3AD2CCA40394AAA8C3DF0193, ___scale_1)); }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  get_scale_1() const { return ___scale_1; }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7 * get_address_of_scale_1() { return &___scale_1; }
	inline void set_scale_1(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  value)
	{
		___scale_1 = value;
	}

	inline static int32_t get_offset_of_rotation_2() { return static_cast<int32_t>(offsetof(AffineTransform_tC1AED177DBAAED2A3AD2CCA40394AAA8C3DF0193, ___rotation_2)); }
	inline Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  get_rotation_2() const { return ___rotation_2; }
	inline Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4 * get_address_of_rotation_2() { return &___rotation_2; }
	inline void set_rotation_2(Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  value)
	{
		___rotation_2 = value;
	}
};


// Obi.CellSpan
struct  CellSpan_tE5A1247201F556E0880AD919A58F92DEA8402291 
{
public:
	// Obi.VInt4 Obi.CellSpan::min
	VInt4_t7102225B83232A71C9D784846D42B69BF440AD5D  ___min_0;
	// Obi.VInt4 Obi.CellSpan::max
	VInt4_t7102225B83232A71C9D784846D42B69BF440AD5D  ___max_1;

public:
	inline static int32_t get_offset_of_min_0() { return static_cast<int32_t>(offsetof(CellSpan_tE5A1247201F556E0880AD919A58F92DEA8402291, ___min_0)); }
	inline VInt4_t7102225B83232A71C9D784846D42B69BF440AD5D  get_min_0() const { return ___min_0; }
	inline VInt4_t7102225B83232A71C9D784846D42B69BF440AD5D * get_address_of_min_0() { return &___min_0; }
	inline void set_min_0(VInt4_t7102225B83232A71C9D784846D42B69BF440AD5D  value)
	{
		___min_0 = value;
	}

	inline static int32_t get_offset_of_max_1() { return static_cast<int32_t>(offsetof(CellSpan_tE5A1247201F556E0880AD919A58F92DEA8402291, ___max_1)); }
	inline VInt4_t7102225B83232A71C9D784846D42B69BF440AD5D  get_max_1() const { return ___max_1; }
	inline VInt4_t7102225B83232A71C9D784846D42B69BF440AD5D * get_address_of_max_1() { return &___max_1; }
	inline void set_max_1(VInt4_t7102225B83232A71C9D784846D42B69BF440AD5D  value)
	{
		___max_1 = value;
	}
};


// Obi.ColliderRigidbody
struct  ColliderRigidbody_tC1FF76AA900641BCDF2B31E90931360DA75EAA0D 
{
public:
	// UnityEngine.Matrix4x4 Obi.ColliderRigidbody::inverseInertiaTensor
	Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461  ___inverseInertiaTensor_0;
	// UnityEngine.Vector4 Obi.ColliderRigidbody::velocity
	Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  ___velocity_1;
	// UnityEngine.Vector4 Obi.ColliderRigidbody::angularVelocity
	Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  ___angularVelocity_2;
	// UnityEngine.Vector4 Obi.ColliderRigidbody::com
	Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  ___com_3;
	// System.Single Obi.ColliderRigidbody::inverseMass
	float ___inverseMass_4;
	// System.Int32 Obi.ColliderRigidbody::pad0
	int32_t ___pad0_5;
	// System.Int32 Obi.ColliderRigidbody::pad1
	int32_t ___pad1_6;
	// System.Int32 Obi.ColliderRigidbody::pad2
	int32_t ___pad2_7;

public:
	inline static int32_t get_offset_of_inverseInertiaTensor_0() { return static_cast<int32_t>(offsetof(ColliderRigidbody_tC1FF76AA900641BCDF2B31E90931360DA75EAA0D, ___inverseInertiaTensor_0)); }
	inline Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461  get_inverseInertiaTensor_0() const { return ___inverseInertiaTensor_0; }
	inline Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461 * get_address_of_inverseInertiaTensor_0() { return &___inverseInertiaTensor_0; }
	inline void set_inverseInertiaTensor_0(Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461  value)
	{
		___inverseInertiaTensor_0 = value;
	}

	inline static int32_t get_offset_of_velocity_1() { return static_cast<int32_t>(offsetof(ColliderRigidbody_tC1FF76AA900641BCDF2B31E90931360DA75EAA0D, ___velocity_1)); }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  get_velocity_1() const { return ___velocity_1; }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7 * get_address_of_velocity_1() { return &___velocity_1; }
	inline void set_velocity_1(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  value)
	{
		___velocity_1 = value;
	}

	inline static int32_t get_offset_of_angularVelocity_2() { return static_cast<int32_t>(offsetof(ColliderRigidbody_tC1FF76AA900641BCDF2B31E90931360DA75EAA0D, ___angularVelocity_2)); }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  get_angularVelocity_2() const { return ___angularVelocity_2; }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7 * get_address_of_angularVelocity_2() { return &___angularVelocity_2; }
	inline void set_angularVelocity_2(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  value)
	{
		___angularVelocity_2 = value;
	}

	inline static int32_t get_offset_of_com_3() { return static_cast<int32_t>(offsetof(ColliderRigidbody_tC1FF76AA900641BCDF2B31E90931360DA75EAA0D, ___com_3)); }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  get_com_3() const { return ___com_3; }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7 * get_address_of_com_3() { return &___com_3; }
	inline void set_com_3(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  value)
	{
		___com_3 = value;
	}

	inline static int32_t get_offset_of_inverseMass_4() { return static_cast<int32_t>(offsetof(ColliderRigidbody_tC1FF76AA900641BCDF2B31E90931360DA75EAA0D, ___inverseMass_4)); }
	inline float get_inverseMass_4() const { return ___inverseMass_4; }
	inline float* get_address_of_inverseMass_4() { return &___inverseMass_4; }
	inline void set_inverseMass_4(float value)
	{
		___inverseMass_4 = value;
	}

	inline static int32_t get_offset_of_pad0_5() { return static_cast<int32_t>(offsetof(ColliderRigidbody_tC1FF76AA900641BCDF2B31E90931360DA75EAA0D, ___pad0_5)); }
	inline int32_t get_pad0_5() const { return ___pad0_5; }
	inline int32_t* get_address_of_pad0_5() { return &___pad0_5; }
	inline void set_pad0_5(int32_t value)
	{
		___pad0_5 = value;
	}

	inline static int32_t get_offset_of_pad1_6() { return static_cast<int32_t>(offsetof(ColliderRigidbody_tC1FF76AA900641BCDF2B31E90931360DA75EAA0D, ___pad1_6)); }
	inline int32_t get_pad1_6() const { return ___pad1_6; }
	inline int32_t* get_address_of_pad1_6() { return &___pad1_6; }
	inline void set_pad1_6(int32_t value)
	{
		___pad1_6 = value;
	}

	inline static int32_t get_offset_of_pad2_7() { return static_cast<int32_t>(offsetof(ColliderRigidbody_tC1FF76AA900641BCDF2B31E90931360DA75EAA0D, ___pad2_7)); }
	inline int32_t get_pad2_7() const { return ___pad2_7; }
	inline int32_t* get_address_of_pad2_7() { return &___pad2_7; }
	inline void set_pad2_7(int32_t value)
	{
		___pad2_7 = value;
	}
};


// Obi.ColliderShape_ShapeType
struct  ShapeType_t4DBDE212C5D7665EA765DFD423D74985AABA3457 
{
public:
	// System.Int32 Obi.ColliderShape_ShapeType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ShapeType_t4DBDE212C5D7665EA765DFD423D74985AABA3457, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// Obi.DFNode
struct  DFNode_t8FC489FB11B708202922AF5F52E4AFEB7B205668 
{
public:
	// UnityEngine.Vector4 Obi.DFNode::distancesA
	Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  ___distancesA_0;
	// UnityEngine.Vector4 Obi.DFNode::distancesB
	Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  ___distancesB_1;
	// UnityEngine.Vector4 Obi.DFNode::center
	Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  ___center_2;
	// System.Int32 Obi.DFNode::firstChild
	int32_t ___firstChild_3;
	// System.Int32 Obi.DFNode::pad0
	int32_t ___pad0_4;
	// System.Int32 Obi.DFNode::pad1
	int32_t ___pad1_5;
	// System.Int32 Obi.DFNode::pad2
	int32_t ___pad2_6;

public:
	inline static int32_t get_offset_of_distancesA_0() { return static_cast<int32_t>(offsetof(DFNode_t8FC489FB11B708202922AF5F52E4AFEB7B205668, ___distancesA_0)); }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  get_distancesA_0() const { return ___distancesA_0; }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7 * get_address_of_distancesA_0() { return &___distancesA_0; }
	inline void set_distancesA_0(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  value)
	{
		___distancesA_0 = value;
	}

	inline static int32_t get_offset_of_distancesB_1() { return static_cast<int32_t>(offsetof(DFNode_t8FC489FB11B708202922AF5F52E4AFEB7B205668, ___distancesB_1)); }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  get_distancesB_1() const { return ___distancesB_1; }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7 * get_address_of_distancesB_1() { return &___distancesB_1; }
	inline void set_distancesB_1(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  value)
	{
		___distancesB_1 = value;
	}

	inline static int32_t get_offset_of_center_2() { return static_cast<int32_t>(offsetof(DFNode_t8FC489FB11B708202922AF5F52E4AFEB7B205668, ___center_2)); }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  get_center_2() const { return ___center_2; }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7 * get_address_of_center_2() { return &___center_2; }
	inline void set_center_2(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  value)
	{
		___center_2 = value;
	}

	inline static int32_t get_offset_of_firstChild_3() { return static_cast<int32_t>(offsetof(DFNode_t8FC489FB11B708202922AF5F52E4AFEB7B205668, ___firstChild_3)); }
	inline int32_t get_firstChild_3() const { return ___firstChild_3; }
	inline int32_t* get_address_of_firstChild_3() { return &___firstChild_3; }
	inline void set_firstChild_3(int32_t value)
	{
		___firstChild_3 = value;
	}

	inline static int32_t get_offset_of_pad0_4() { return static_cast<int32_t>(offsetof(DFNode_t8FC489FB11B708202922AF5F52E4AFEB7B205668, ___pad0_4)); }
	inline int32_t get_pad0_4() const { return ___pad0_4; }
	inline int32_t* get_address_of_pad0_4() { return &___pad0_4; }
	inline void set_pad0_4(int32_t value)
	{
		___pad0_4 = value;
	}

	inline static int32_t get_offset_of_pad1_5() { return static_cast<int32_t>(offsetof(DFNode_t8FC489FB11B708202922AF5F52E4AFEB7B205668, ___pad1_5)); }
	inline int32_t get_pad1_5() const { return ___pad1_5; }
	inline int32_t* get_address_of_pad1_5() { return &___pad1_5; }
	inline void set_pad1_5(int32_t value)
	{
		___pad1_5 = value;
	}

	inline static int32_t get_offset_of_pad2_6() { return static_cast<int32_t>(offsetof(DFNode_t8FC489FB11B708202922AF5F52E4AFEB7B205668, ___pad2_6)); }
	inline int32_t get_pad2_6() const { return ___pad2_6; }
	inline int32_t* get_address_of_pad2_6() { return &___pad2_6; }
	inline void set_pad2_6(int32_t value)
	{
		___pad2_6 = value;
	}
};


// Obi.QueryResult
struct  QueryResult_t4DCEE89279D61183A30969C97F725AA603AD118C 
{
public:
	union
	{
		struct
		{
			// UnityEngine.Vector4 Obi.QueryResult::simplexBary
			Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  ___simplexBary_0;
			// UnityEngine.Vector4 Obi.QueryResult::queryPoint
			Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  ___queryPoint_1;
			// UnityEngine.Vector4 Obi.QueryResult::normal
			Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  ___normal_2;
			// System.Single Obi.QueryResult::distance
			float ___distance_3;
			// System.Int32 Obi.QueryResult::simplexIndex
			int32_t ___simplexIndex_4;
			// System.Int32 Obi.QueryResult::queryIndex
			int32_t ___queryIndex_5;
		};
		uint8_t QueryResult_t4DCEE89279D61183A30969C97F725AA603AD118C__padding[64];
	};

public:
	inline static int32_t get_offset_of_simplexBary_0() { return static_cast<int32_t>(offsetof(QueryResult_t4DCEE89279D61183A30969C97F725AA603AD118C, ___simplexBary_0)); }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  get_simplexBary_0() const { return ___simplexBary_0; }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7 * get_address_of_simplexBary_0() { return &___simplexBary_0; }
	inline void set_simplexBary_0(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  value)
	{
		___simplexBary_0 = value;
	}

	inline static int32_t get_offset_of_queryPoint_1() { return static_cast<int32_t>(offsetof(QueryResult_t4DCEE89279D61183A30969C97F725AA603AD118C, ___queryPoint_1)); }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  get_queryPoint_1() const { return ___queryPoint_1; }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7 * get_address_of_queryPoint_1() { return &___queryPoint_1; }
	inline void set_queryPoint_1(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  value)
	{
		___queryPoint_1 = value;
	}

	inline static int32_t get_offset_of_normal_2() { return static_cast<int32_t>(offsetof(QueryResult_t4DCEE89279D61183A30969C97F725AA603AD118C, ___normal_2)); }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  get_normal_2() const { return ___normal_2; }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7 * get_address_of_normal_2() { return &___normal_2; }
	inline void set_normal_2(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  value)
	{
		___normal_2 = value;
	}

	inline static int32_t get_offset_of_distance_3() { return static_cast<int32_t>(offsetof(QueryResult_t4DCEE89279D61183A30969C97F725AA603AD118C, ___distance_3)); }
	inline float get_distance_3() const { return ___distance_3; }
	inline float* get_address_of_distance_3() { return &___distance_3; }
	inline void set_distance_3(float value)
	{
		___distance_3 = value;
	}

	inline static int32_t get_offset_of_simplexIndex_4() { return static_cast<int32_t>(offsetof(QueryResult_t4DCEE89279D61183A30969C97F725AA603AD118C, ___simplexIndex_4)); }
	inline int32_t get_simplexIndex_4() const { return ___simplexIndex_4; }
	inline int32_t* get_address_of_simplexIndex_4() { return &___simplexIndex_4; }
	inline void set_simplexIndex_4(int32_t value)
	{
		___simplexIndex_4 = value;
	}

	inline static int32_t get_offset_of_queryIndex_5() { return static_cast<int32_t>(offsetof(QueryResult_t4DCEE89279D61183A30969C97F725AA603AD118C, ___queryIndex_5)); }
	inline int32_t get_queryIndex_5() const { return ___queryIndex_5; }
	inline int32_t* get_address_of_queryIndex_5() { return &___queryIndex_5; }
	inline void set_queryIndex_5(int32_t value)
	{
		___queryIndex_5 = value;
	}
};


// Obi.QueryShape_QueryType
struct  QueryType_tB7ABA496561EDA432C6A12A5DDB8E54184413CD2 
{
public:
	// System.Int32 Obi.QueryShape_QueryType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(QueryType_tB7ABA496561EDA432C6A12A5DDB8E54184413CD2, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// Oni_Contact
struct  Contact_tA38E76CD5E9A53DE837B07BBB25CABE68B2AF756 
{
public:
	union
	{
		struct
		{
			// UnityEngine.Vector4 Oni_Contact::pointA
			Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  ___pointA_0;
			// UnityEngine.Vector4 Oni_Contact::pointB
			Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  ___pointB_1;
			// UnityEngine.Vector4 Oni_Contact::normal
			Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  ___normal_2;
			// UnityEngine.Vector4 Oni_Contact::tangent
			Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  ___tangent_3;
			// UnityEngine.Vector4 Oni_Contact::bitangent
			Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  ___bitangent_4;
			// System.Single Oni_Contact::distance
			float ___distance_5;
			// System.Single Oni_Contact::normalImpulse
			float ___normalImpulse_6;
			// System.Single Oni_Contact::tangentImpulse
			float ___tangentImpulse_7;
			// System.Single Oni_Contact::bitangentImpulse
			float ___bitangentImpulse_8;
			// System.Single Oni_Contact::stickImpulse
			float ___stickImpulse_9;
			// System.Single Oni_Contact::rollingFrictionImpulse
			float ___rollingFrictionImpulse_10;
			// System.Int32 Oni_Contact::bodyA
			int32_t ___bodyA_11;
			// System.Int32 Oni_Contact::bodyB
			int32_t ___bodyB_12;
		};
		uint8_t Contact_tA38E76CD5E9A53DE837B07BBB25CABE68B2AF756__padding[144];
	};

public:
	inline static int32_t get_offset_of_pointA_0() { return static_cast<int32_t>(offsetof(Contact_tA38E76CD5E9A53DE837B07BBB25CABE68B2AF756, ___pointA_0)); }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  get_pointA_0() const { return ___pointA_0; }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7 * get_address_of_pointA_0() { return &___pointA_0; }
	inline void set_pointA_0(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  value)
	{
		___pointA_0 = value;
	}

	inline static int32_t get_offset_of_pointB_1() { return static_cast<int32_t>(offsetof(Contact_tA38E76CD5E9A53DE837B07BBB25CABE68B2AF756, ___pointB_1)); }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  get_pointB_1() const { return ___pointB_1; }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7 * get_address_of_pointB_1() { return &___pointB_1; }
	inline void set_pointB_1(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  value)
	{
		___pointB_1 = value;
	}

	inline static int32_t get_offset_of_normal_2() { return static_cast<int32_t>(offsetof(Contact_tA38E76CD5E9A53DE837B07BBB25CABE68B2AF756, ___normal_2)); }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  get_normal_2() const { return ___normal_2; }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7 * get_address_of_normal_2() { return &___normal_2; }
	inline void set_normal_2(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  value)
	{
		___normal_2 = value;
	}

	inline static int32_t get_offset_of_tangent_3() { return static_cast<int32_t>(offsetof(Contact_tA38E76CD5E9A53DE837B07BBB25CABE68B2AF756, ___tangent_3)); }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  get_tangent_3() const { return ___tangent_3; }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7 * get_address_of_tangent_3() { return &___tangent_3; }
	inline void set_tangent_3(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  value)
	{
		___tangent_3 = value;
	}

	inline static int32_t get_offset_of_bitangent_4() { return static_cast<int32_t>(offsetof(Contact_tA38E76CD5E9A53DE837B07BBB25CABE68B2AF756, ___bitangent_4)); }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  get_bitangent_4() const { return ___bitangent_4; }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7 * get_address_of_bitangent_4() { return &___bitangent_4; }
	inline void set_bitangent_4(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  value)
	{
		___bitangent_4 = value;
	}

	inline static int32_t get_offset_of_distance_5() { return static_cast<int32_t>(offsetof(Contact_tA38E76CD5E9A53DE837B07BBB25CABE68B2AF756, ___distance_5)); }
	inline float get_distance_5() const { return ___distance_5; }
	inline float* get_address_of_distance_5() { return &___distance_5; }
	inline void set_distance_5(float value)
	{
		___distance_5 = value;
	}

	inline static int32_t get_offset_of_normalImpulse_6() { return static_cast<int32_t>(offsetof(Contact_tA38E76CD5E9A53DE837B07BBB25CABE68B2AF756, ___normalImpulse_6)); }
	inline float get_normalImpulse_6() const { return ___normalImpulse_6; }
	inline float* get_address_of_normalImpulse_6() { return &___normalImpulse_6; }
	inline void set_normalImpulse_6(float value)
	{
		___normalImpulse_6 = value;
	}

	inline static int32_t get_offset_of_tangentImpulse_7() { return static_cast<int32_t>(offsetof(Contact_tA38E76CD5E9A53DE837B07BBB25CABE68B2AF756, ___tangentImpulse_7)); }
	inline float get_tangentImpulse_7() const { return ___tangentImpulse_7; }
	inline float* get_address_of_tangentImpulse_7() { return &___tangentImpulse_7; }
	inline void set_tangentImpulse_7(float value)
	{
		___tangentImpulse_7 = value;
	}

	inline static int32_t get_offset_of_bitangentImpulse_8() { return static_cast<int32_t>(offsetof(Contact_tA38E76CD5E9A53DE837B07BBB25CABE68B2AF756, ___bitangentImpulse_8)); }
	inline float get_bitangentImpulse_8() const { return ___bitangentImpulse_8; }
	inline float* get_address_of_bitangentImpulse_8() { return &___bitangentImpulse_8; }
	inline void set_bitangentImpulse_8(float value)
	{
		___bitangentImpulse_8 = value;
	}

	inline static int32_t get_offset_of_stickImpulse_9() { return static_cast<int32_t>(offsetof(Contact_tA38E76CD5E9A53DE837B07BBB25CABE68B2AF756, ___stickImpulse_9)); }
	inline float get_stickImpulse_9() const { return ___stickImpulse_9; }
	inline float* get_address_of_stickImpulse_9() { return &___stickImpulse_9; }
	inline void set_stickImpulse_9(float value)
	{
		___stickImpulse_9 = value;
	}

	inline static int32_t get_offset_of_rollingFrictionImpulse_10() { return static_cast<int32_t>(offsetof(Contact_tA38E76CD5E9A53DE837B07BBB25CABE68B2AF756, ___rollingFrictionImpulse_10)); }
	inline float get_rollingFrictionImpulse_10() const { return ___rollingFrictionImpulse_10; }
	inline float* get_address_of_rollingFrictionImpulse_10() { return &___rollingFrictionImpulse_10; }
	inline void set_rollingFrictionImpulse_10(float value)
	{
		___rollingFrictionImpulse_10 = value;
	}

	inline static int32_t get_offset_of_bodyA_11() { return static_cast<int32_t>(offsetof(Contact_tA38E76CD5E9A53DE837B07BBB25CABE68B2AF756, ___bodyA_11)); }
	inline int32_t get_bodyA_11() const { return ___bodyA_11; }
	inline int32_t* get_address_of_bodyA_11() { return &___bodyA_11; }
	inline void set_bodyA_11(int32_t value)
	{
		___bodyA_11 = value;
	}

	inline static int32_t get_offset_of_bodyB_12() { return static_cast<int32_t>(offsetof(Contact_tA38E76CD5E9A53DE837B07BBB25CABE68B2AF756, ___bodyB_12)); }
	inline int32_t get_bodyB_12() const { return ___bodyB_12; }
	inline int32_t* get_address_of_bodyB_12() { return &___bodyB_12; }
	inline void set_bodyB_12(int32_t value)
	{
		___bodyB_12 = value;
	}
};


// Oni_MaterialCombineMode
struct  MaterialCombineMode_t6D9889E6299865FDA792A7E3A202B4FCC7E347CD 
{
public:
	// System.Int32 Oni_MaterialCombineMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(MaterialCombineMode_t6D9889E6299865FDA792A7E3A202B4FCC7E347CD, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// System.Delegate
struct  Delegate_t  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::extra_arg
	intptr_t ___extra_arg_5;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_6;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_7;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_8;
	// System.DelegateData System.Delegate::data
	DelegateData_t17DD30660E330C49381DAA99F934BE75CB11F288 * ___data_9;
	// System.Boolean System.Delegate::method_is_virtual
	bool ___method_is_virtual_10;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t, ___invoke_impl_1)); }
	inline intptr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline intptr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(intptr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_target_2), (void*)value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_3)); }
	inline intptr_t get_method_3() const { return ___method_3; }
	inline intptr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(intptr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t, ___delegate_trampoline_4)); }
	inline intptr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline intptr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(intptr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_extra_arg_5() { return static_cast<int32_t>(offsetof(Delegate_t, ___extra_arg_5)); }
	inline intptr_t get_extra_arg_5() const { return ___extra_arg_5; }
	inline intptr_t* get_address_of_extra_arg_5() { return &___extra_arg_5; }
	inline void set_extra_arg_5(intptr_t value)
	{
		___extra_arg_5 = value;
	}

	inline static int32_t get_offset_of_method_code_6() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_code_6)); }
	inline intptr_t get_method_code_6() const { return ___method_code_6; }
	inline intptr_t* get_address_of_method_code_6() { return &___method_code_6; }
	inline void set_method_code_6(intptr_t value)
	{
		___method_code_6 = value;
	}

	inline static int32_t get_offset_of_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_info_7)); }
	inline MethodInfo_t * get_method_info_7() const { return ___method_info_7; }
	inline MethodInfo_t ** get_address_of_method_info_7() { return &___method_info_7; }
	inline void set_method_info_7(MethodInfo_t * value)
	{
		___method_info_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___method_info_7), (void*)value);
	}

	inline static int32_t get_offset_of_original_method_info_8() { return static_cast<int32_t>(offsetof(Delegate_t, ___original_method_info_8)); }
	inline MethodInfo_t * get_original_method_info_8() const { return ___original_method_info_8; }
	inline MethodInfo_t ** get_address_of_original_method_info_8() { return &___original_method_info_8; }
	inline void set_original_method_info_8(MethodInfo_t * value)
	{
		___original_method_info_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___original_method_info_8), (void*)value);
	}

	inline static int32_t get_offset_of_data_9() { return static_cast<int32_t>(offsetof(Delegate_t, ___data_9)); }
	inline DelegateData_t17DD30660E330C49381DAA99F934BE75CB11F288 * get_data_9() const { return ___data_9; }
	inline DelegateData_t17DD30660E330C49381DAA99F934BE75CB11F288 ** get_address_of_data_9() { return &___data_9; }
	inline void set_data_9(DelegateData_t17DD30660E330C49381DAA99F934BE75CB11F288 * value)
	{
		___data_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___data_9), (void*)value);
	}

	inline static int32_t get_offset_of_method_is_virtual_10() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_is_virtual_10)); }
	inline bool get_method_is_virtual_10() const { return ___method_is_virtual_10; }
	inline bool* get_address_of_method_is_virtual_10() { return &___method_is_virtual_10; }
	inline void set_method_is_virtual_10(bool value)
	{
		___method_is_virtual_10 = value;
	}
};

// Native definition for P/Invoke marshalling of System.Delegate
struct Delegate_t_marshaled_pinvoke
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t17DD30660E330C49381DAA99F934BE75CB11F288 * ___data_9;
	int32_t ___method_is_virtual_10;
};
// Native definition for COM marshalling of System.Delegate
struct Delegate_t_marshaled_com
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t17DD30660E330C49381DAA99F934BE75CB11F288 * ___data_9;
	int32_t ___method_is_virtual_10;
};

// System.Exception
struct  Exception_t  : public RuntimeObject
{
public:
	// System.String System.Exception::_className
	String_t* ____className_1;
	// System.String System.Exception::_message
	String_t* ____message_2;
	// System.Collections.IDictionary System.Exception::_data
	RuntimeObject* ____data_3;
	// System.Exception System.Exception::_innerException
	Exception_t * ____innerException_4;
	// System.String System.Exception::_helpURL
	String_t* ____helpURL_5;
	// System.Object System.Exception::_stackTrace
	RuntimeObject * ____stackTrace_6;
	// System.String System.Exception::_stackTraceString
	String_t* ____stackTraceString_7;
	// System.String System.Exception::_remoteStackTraceString
	String_t* ____remoteStackTraceString_8;
	// System.Int32 System.Exception::_remoteStackIndex
	int32_t ____remoteStackIndex_9;
	// System.Object System.Exception::_dynamicMethods
	RuntimeObject * ____dynamicMethods_10;
	// System.Int32 System.Exception::_HResult
	int32_t ____HResult_11;
	// System.String System.Exception::_source
	String_t* ____source_12;
	// System.Runtime.Serialization.SafeSerializationManager System.Exception::_safeSerializationManager
	SafeSerializationManager_tDE44F029589A028F8A3053C5C06153FAB4AAE29F * ____safeSerializationManager_13;
	// System.Diagnostics.StackTrace[] System.Exception::captured_traces
	StackTraceU5BU5D_t4AD999C288CB6D1F38A299D12B1598D606588971* ___captured_traces_14;
	// System.IntPtr[] System.Exception::native_trace_ips
	IntPtrU5BU5D_t27FC72B0409D75AAF33EC42498E8094E95FEE9A6* ___native_trace_ips_15;

public:
	inline static int32_t get_offset_of__className_1() { return static_cast<int32_t>(offsetof(Exception_t, ____className_1)); }
	inline String_t* get__className_1() const { return ____className_1; }
	inline String_t** get_address_of__className_1() { return &____className_1; }
	inline void set__className_1(String_t* value)
	{
		____className_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____className_1), (void*)value);
	}

	inline static int32_t get_offset_of__message_2() { return static_cast<int32_t>(offsetof(Exception_t, ____message_2)); }
	inline String_t* get__message_2() const { return ____message_2; }
	inline String_t** get_address_of__message_2() { return &____message_2; }
	inline void set__message_2(String_t* value)
	{
		____message_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____message_2), (void*)value);
	}

	inline static int32_t get_offset_of__data_3() { return static_cast<int32_t>(offsetof(Exception_t, ____data_3)); }
	inline RuntimeObject* get__data_3() const { return ____data_3; }
	inline RuntimeObject** get_address_of__data_3() { return &____data_3; }
	inline void set__data_3(RuntimeObject* value)
	{
		____data_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____data_3), (void*)value);
	}

	inline static int32_t get_offset_of__innerException_4() { return static_cast<int32_t>(offsetof(Exception_t, ____innerException_4)); }
	inline Exception_t * get__innerException_4() const { return ____innerException_4; }
	inline Exception_t ** get_address_of__innerException_4() { return &____innerException_4; }
	inline void set__innerException_4(Exception_t * value)
	{
		____innerException_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____innerException_4), (void*)value);
	}

	inline static int32_t get_offset_of__helpURL_5() { return static_cast<int32_t>(offsetof(Exception_t, ____helpURL_5)); }
	inline String_t* get__helpURL_5() const { return ____helpURL_5; }
	inline String_t** get_address_of__helpURL_5() { return &____helpURL_5; }
	inline void set__helpURL_5(String_t* value)
	{
		____helpURL_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____helpURL_5), (void*)value);
	}

	inline static int32_t get_offset_of__stackTrace_6() { return static_cast<int32_t>(offsetof(Exception_t, ____stackTrace_6)); }
	inline RuntimeObject * get__stackTrace_6() const { return ____stackTrace_6; }
	inline RuntimeObject ** get_address_of__stackTrace_6() { return &____stackTrace_6; }
	inline void set__stackTrace_6(RuntimeObject * value)
	{
		____stackTrace_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____stackTrace_6), (void*)value);
	}

	inline static int32_t get_offset_of__stackTraceString_7() { return static_cast<int32_t>(offsetof(Exception_t, ____stackTraceString_7)); }
	inline String_t* get__stackTraceString_7() const { return ____stackTraceString_7; }
	inline String_t** get_address_of__stackTraceString_7() { return &____stackTraceString_7; }
	inline void set__stackTraceString_7(String_t* value)
	{
		____stackTraceString_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____stackTraceString_7), (void*)value);
	}

	inline static int32_t get_offset_of__remoteStackTraceString_8() { return static_cast<int32_t>(offsetof(Exception_t, ____remoteStackTraceString_8)); }
	inline String_t* get__remoteStackTraceString_8() const { return ____remoteStackTraceString_8; }
	inline String_t** get_address_of__remoteStackTraceString_8() { return &____remoteStackTraceString_8; }
	inline void set__remoteStackTraceString_8(String_t* value)
	{
		____remoteStackTraceString_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____remoteStackTraceString_8), (void*)value);
	}

	inline static int32_t get_offset_of__remoteStackIndex_9() { return static_cast<int32_t>(offsetof(Exception_t, ____remoteStackIndex_9)); }
	inline int32_t get__remoteStackIndex_9() const { return ____remoteStackIndex_9; }
	inline int32_t* get_address_of__remoteStackIndex_9() { return &____remoteStackIndex_9; }
	inline void set__remoteStackIndex_9(int32_t value)
	{
		____remoteStackIndex_9 = value;
	}

	inline static int32_t get_offset_of__dynamicMethods_10() { return static_cast<int32_t>(offsetof(Exception_t, ____dynamicMethods_10)); }
	inline RuntimeObject * get__dynamicMethods_10() const { return ____dynamicMethods_10; }
	inline RuntimeObject ** get_address_of__dynamicMethods_10() { return &____dynamicMethods_10; }
	inline void set__dynamicMethods_10(RuntimeObject * value)
	{
		____dynamicMethods_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____dynamicMethods_10), (void*)value);
	}

	inline static int32_t get_offset_of__HResult_11() { return static_cast<int32_t>(offsetof(Exception_t, ____HResult_11)); }
	inline int32_t get__HResult_11() const { return ____HResult_11; }
	inline int32_t* get_address_of__HResult_11() { return &____HResult_11; }
	inline void set__HResult_11(int32_t value)
	{
		____HResult_11 = value;
	}

	inline static int32_t get_offset_of__source_12() { return static_cast<int32_t>(offsetof(Exception_t, ____source_12)); }
	inline String_t* get__source_12() const { return ____source_12; }
	inline String_t** get_address_of__source_12() { return &____source_12; }
	inline void set__source_12(String_t* value)
	{
		____source_12 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____source_12), (void*)value);
	}

	inline static int32_t get_offset_of__safeSerializationManager_13() { return static_cast<int32_t>(offsetof(Exception_t, ____safeSerializationManager_13)); }
	inline SafeSerializationManager_tDE44F029589A028F8A3053C5C06153FAB4AAE29F * get__safeSerializationManager_13() const { return ____safeSerializationManager_13; }
	inline SafeSerializationManager_tDE44F029589A028F8A3053C5C06153FAB4AAE29F ** get_address_of__safeSerializationManager_13() { return &____safeSerializationManager_13; }
	inline void set__safeSerializationManager_13(SafeSerializationManager_tDE44F029589A028F8A3053C5C06153FAB4AAE29F * value)
	{
		____safeSerializationManager_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____safeSerializationManager_13), (void*)value);
	}

	inline static int32_t get_offset_of_captured_traces_14() { return static_cast<int32_t>(offsetof(Exception_t, ___captured_traces_14)); }
	inline StackTraceU5BU5D_t4AD999C288CB6D1F38A299D12B1598D606588971* get_captured_traces_14() const { return ___captured_traces_14; }
	inline StackTraceU5BU5D_t4AD999C288CB6D1F38A299D12B1598D606588971** get_address_of_captured_traces_14() { return &___captured_traces_14; }
	inline void set_captured_traces_14(StackTraceU5BU5D_t4AD999C288CB6D1F38A299D12B1598D606588971* value)
	{
		___captured_traces_14 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___captured_traces_14), (void*)value);
	}

	inline static int32_t get_offset_of_native_trace_ips_15() { return static_cast<int32_t>(offsetof(Exception_t, ___native_trace_ips_15)); }
	inline IntPtrU5BU5D_t27FC72B0409D75AAF33EC42498E8094E95FEE9A6* get_native_trace_ips_15() const { return ___native_trace_ips_15; }
	inline IntPtrU5BU5D_t27FC72B0409D75AAF33EC42498E8094E95FEE9A6** get_address_of_native_trace_ips_15() { return &___native_trace_ips_15; }
	inline void set_native_trace_ips_15(IntPtrU5BU5D_t27FC72B0409D75AAF33EC42498E8094E95FEE9A6* value)
	{
		___native_trace_ips_15 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___native_trace_ips_15), (void*)value);
	}
};

struct Exception_t_StaticFields
{
public:
	// System.Object System.Exception::s_EDILock
	RuntimeObject * ___s_EDILock_0;

public:
	inline static int32_t get_offset_of_s_EDILock_0() { return static_cast<int32_t>(offsetof(Exception_t_StaticFields, ___s_EDILock_0)); }
	inline RuntimeObject * get_s_EDILock_0() const { return ___s_EDILock_0; }
	inline RuntimeObject ** get_address_of_s_EDILock_0() { return &___s_EDILock_0; }
	inline void set_s_EDILock_0(RuntimeObject * value)
	{
		___s_EDILock_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_EDILock_0), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.Exception
struct Exception_t_marshaled_pinvoke
{
	char* ____className_1;
	char* ____message_2;
	RuntimeObject* ____data_3;
	Exception_t_marshaled_pinvoke* ____innerException_4;
	char* ____helpURL_5;
	Il2CppIUnknown* ____stackTrace_6;
	char* ____stackTraceString_7;
	char* ____remoteStackTraceString_8;
	int32_t ____remoteStackIndex_9;
	Il2CppIUnknown* ____dynamicMethods_10;
	int32_t ____HResult_11;
	char* ____source_12;
	SafeSerializationManager_tDE44F029589A028F8A3053C5C06153FAB4AAE29F * ____safeSerializationManager_13;
	StackTraceU5BU5D_t4AD999C288CB6D1F38A299D12B1598D606588971* ___captured_traces_14;
	Il2CppSafeArray/*NONE*/* ___native_trace_ips_15;
};
// Native definition for COM marshalling of System.Exception
struct Exception_t_marshaled_com
{
	Il2CppChar* ____className_1;
	Il2CppChar* ____message_2;
	RuntimeObject* ____data_3;
	Exception_t_marshaled_com* ____innerException_4;
	Il2CppChar* ____helpURL_5;
	Il2CppIUnknown* ____stackTrace_6;
	Il2CppChar* ____stackTraceString_7;
	Il2CppChar* ____remoteStackTraceString_8;
	int32_t ____remoteStackIndex_9;
	Il2CppIUnknown* ____dynamicMethods_10;
	int32_t ____HResult_11;
	Il2CppChar* ____source_12;
	SafeSerializationManager_tDE44F029589A028F8A3053C5C06153FAB4AAE29F * ____safeSerializationManager_13;
	StackTraceU5BU5D_t4AD999C288CB6D1F38A299D12B1598D606588971* ___captured_traces_14;
	Il2CppSafeArray/*NONE*/* ___native_trace_ips_15;
};

// System.Runtime.CompilerServices.AsyncTaskMethodBuilder`1<System.Boolean>
struct  AsyncTaskMethodBuilder_1_tABE1DEF12F121D6FC8ABF04869ED964FF83EA9B5 
{
public:
	// System.Runtime.CompilerServices.AsyncMethodBuilderCore System.Runtime.CompilerServices.AsyncTaskMethodBuilder`1::m_coreState
	AsyncMethodBuilderCore_t2C85055E04767C52B9F66144476FCBF500DBFA34  ___m_coreState_1;
	// System.Threading.Tasks.Task`1<TResult> System.Runtime.CompilerServices.AsyncTaskMethodBuilder`1::m_task
	Task_1_t9C1FE9F18F52F3409B9E970FA38801A443AE7849 * ___m_task_2;

public:
	inline static int32_t get_offset_of_m_coreState_1() { return static_cast<int32_t>(offsetof(AsyncTaskMethodBuilder_1_tABE1DEF12F121D6FC8ABF04869ED964FF83EA9B5, ___m_coreState_1)); }
	inline AsyncMethodBuilderCore_t2C85055E04767C52B9F66144476FCBF500DBFA34  get_m_coreState_1() const { return ___m_coreState_1; }
	inline AsyncMethodBuilderCore_t2C85055E04767C52B9F66144476FCBF500DBFA34 * get_address_of_m_coreState_1() { return &___m_coreState_1; }
	inline void set_m_coreState_1(AsyncMethodBuilderCore_t2C85055E04767C52B9F66144476FCBF500DBFA34  value)
	{
		___m_coreState_1 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___m_coreState_1))->___m_stateMachine_0), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___m_coreState_1))->___m_defaultContextAction_1), (void*)NULL);
		#endif
	}

	inline static int32_t get_offset_of_m_task_2() { return static_cast<int32_t>(offsetof(AsyncTaskMethodBuilder_1_tABE1DEF12F121D6FC8ABF04869ED964FF83EA9B5, ___m_task_2)); }
	inline Task_1_t9C1FE9F18F52F3409B9E970FA38801A443AE7849 * get_m_task_2() const { return ___m_task_2; }
	inline Task_1_t9C1FE9F18F52F3409B9E970FA38801A443AE7849 ** get_address_of_m_task_2() { return &___m_task_2; }
	inline void set_m_task_2(Task_1_t9C1FE9F18F52F3409B9E970FA38801A443AE7849 * value)
	{
		___m_task_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_task_2), (void*)value);
	}
};

struct AsyncTaskMethodBuilder_1_tABE1DEF12F121D6FC8ABF04869ED964FF83EA9B5_StaticFields
{
public:
	// System.Threading.Tasks.Task`1<TResult> System.Runtime.CompilerServices.AsyncTaskMethodBuilder`1::s_defaultResultTask
	Task_1_t9C1FE9F18F52F3409B9E970FA38801A443AE7849 * ___s_defaultResultTask_0;

public:
	inline static int32_t get_offset_of_s_defaultResultTask_0() { return static_cast<int32_t>(offsetof(AsyncTaskMethodBuilder_1_tABE1DEF12F121D6FC8ABF04869ED964FF83EA9B5_StaticFields, ___s_defaultResultTask_0)); }
	inline Task_1_t9C1FE9F18F52F3409B9E970FA38801A443AE7849 * get_s_defaultResultTask_0() const { return ___s_defaultResultTask_0; }
	inline Task_1_t9C1FE9F18F52F3409B9E970FA38801A443AE7849 ** get_address_of_s_defaultResultTask_0() { return &___s_defaultResultTask_0; }
	inline void set_s_defaultResultTask_0(Task_1_t9C1FE9F18F52F3409B9E970FA38801A443AE7849 * value)
	{
		___s_defaultResultTask_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_defaultResultTask_0), (void*)value);
	}
};


// System.Runtime.CompilerServices.AsyncTaskMethodBuilder`1<System.Object>
struct  AsyncTaskMethodBuilder_1_tDD2A3BA099C327938EA03C35FDB1A7502BA73020 
{
public:
	// System.Runtime.CompilerServices.AsyncMethodBuilderCore System.Runtime.CompilerServices.AsyncTaskMethodBuilder`1::m_coreState
	AsyncMethodBuilderCore_t2C85055E04767C52B9F66144476FCBF500DBFA34  ___m_coreState_1;
	// System.Threading.Tasks.Task`1<TResult> System.Runtime.CompilerServices.AsyncTaskMethodBuilder`1::m_task
	Task_1_tC1805497876E88B78A2B0CB81C6409E0B381AC17 * ___m_task_2;

public:
	inline static int32_t get_offset_of_m_coreState_1() { return static_cast<int32_t>(offsetof(AsyncTaskMethodBuilder_1_tDD2A3BA099C327938EA03C35FDB1A7502BA73020, ___m_coreState_1)); }
	inline AsyncMethodBuilderCore_t2C85055E04767C52B9F66144476FCBF500DBFA34  get_m_coreState_1() const { return ___m_coreState_1; }
	inline AsyncMethodBuilderCore_t2C85055E04767C52B9F66144476FCBF500DBFA34 * get_address_of_m_coreState_1() { return &___m_coreState_1; }
	inline void set_m_coreState_1(AsyncMethodBuilderCore_t2C85055E04767C52B9F66144476FCBF500DBFA34  value)
	{
		___m_coreState_1 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___m_coreState_1))->___m_stateMachine_0), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___m_coreState_1))->___m_defaultContextAction_1), (void*)NULL);
		#endif
	}

	inline static int32_t get_offset_of_m_task_2() { return static_cast<int32_t>(offsetof(AsyncTaskMethodBuilder_1_tDD2A3BA099C327938EA03C35FDB1A7502BA73020, ___m_task_2)); }
	inline Task_1_tC1805497876E88B78A2B0CB81C6409E0B381AC17 * get_m_task_2() const { return ___m_task_2; }
	inline Task_1_tC1805497876E88B78A2B0CB81C6409E0B381AC17 ** get_address_of_m_task_2() { return &___m_task_2; }
	inline void set_m_task_2(Task_1_tC1805497876E88B78A2B0CB81C6409E0B381AC17 * value)
	{
		___m_task_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_task_2), (void*)value);
	}
};

struct AsyncTaskMethodBuilder_1_tDD2A3BA099C327938EA03C35FDB1A7502BA73020_StaticFields
{
public:
	// System.Threading.Tasks.Task`1<TResult> System.Runtime.CompilerServices.AsyncTaskMethodBuilder`1::s_defaultResultTask
	Task_1_tC1805497876E88B78A2B0CB81C6409E0B381AC17 * ___s_defaultResultTask_0;

public:
	inline static int32_t get_offset_of_s_defaultResultTask_0() { return static_cast<int32_t>(offsetof(AsyncTaskMethodBuilder_1_tDD2A3BA099C327938EA03C35FDB1A7502BA73020_StaticFields, ___s_defaultResultTask_0)); }
	inline Task_1_tC1805497876E88B78A2B0CB81C6409E0B381AC17 * get_s_defaultResultTask_0() const { return ___s_defaultResultTask_0; }
	inline Task_1_tC1805497876E88B78A2B0CB81C6409E0B381AC17 ** get_address_of_s_defaultResultTask_0() { return &___s_defaultResultTask_0; }
	inline void set_s_defaultResultTask_0(Task_1_tC1805497876E88B78A2B0CB81C6409E0B381AC17 * value)
	{
		___s_defaultResultTask_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_defaultResultTask_0), (void*)value);
	}
};


// System.Threading.ExecutionContextSwitcher
struct  ExecutionContextSwitcher_t11B7DEE83408478EE3D5E29C988E5385AA9D7277 
{
public:
	// System.Threading.ExecutionContext_Reader System.Threading.ExecutionContextSwitcher::outerEC
	Reader_t6C70587C0F5A8CE8367A0407E3109E196764848C  ___outerEC_0;
	// System.Boolean System.Threading.ExecutionContextSwitcher::outerECBelongsToScope
	bool ___outerECBelongsToScope_1;
	// System.Object System.Threading.ExecutionContextSwitcher::hecsw
	RuntimeObject * ___hecsw_2;
	// System.Threading.Thread System.Threading.ExecutionContextSwitcher::thread
	Thread_tB9EB71664220EE16451AF3276D78DE6614D2A414 * ___thread_3;

public:
	inline static int32_t get_offset_of_outerEC_0() { return static_cast<int32_t>(offsetof(ExecutionContextSwitcher_t11B7DEE83408478EE3D5E29C988E5385AA9D7277, ___outerEC_0)); }
	inline Reader_t6C70587C0F5A8CE8367A0407E3109E196764848C  get_outerEC_0() const { return ___outerEC_0; }
	inline Reader_t6C70587C0F5A8CE8367A0407E3109E196764848C * get_address_of_outerEC_0() { return &___outerEC_0; }
	inline void set_outerEC_0(Reader_t6C70587C0F5A8CE8367A0407E3109E196764848C  value)
	{
		___outerEC_0 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___outerEC_0))->___m_ec_0), (void*)NULL);
	}

	inline static int32_t get_offset_of_outerECBelongsToScope_1() { return static_cast<int32_t>(offsetof(ExecutionContextSwitcher_t11B7DEE83408478EE3D5E29C988E5385AA9D7277, ___outerECBelongsToScope_1)); }
	inline bool get_outerECBelongsToScope_1() const { return ___outerECBelongsToScope_1; }
	inline bool* get_address_of_outerECBelongsToScope_1() { return &___outerECBelongsToScope_1; }
	inline void set_outerECBelongsToScope_1(bool value)
	{
		___outerECBelongsToScope_1 = value;
	}

	inline static int32_t get_offset_of_hecsw_2() { return static_cast<int32_t>(offsetof(ExecutionContextSwitcher_t11B7DEE83408478EE3D5E29C988E5385AA9D7277, ___hecsw_2)); }
	inline RuntimeObject * get_hecsw_2() const { return ___hecsw_2; }
	inline RuntimeObject ** get_address_of_hecsw_2() { return &___hecsw_2; }
	inline void set_hecsw_2(RuntimeObject * value)
	{
		___hecsw_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___hecsw_2), (void*)value);
	}

	inline static int32_t get_offset_of_thread_3() { return static_cast<int32_t>(offsetof(ExecutionContextSwitcher_t11B7DEE83408478EE3D5E29C988E5385AA9D7277, ___thread_3)); }
	inline Thread_tB9EB71664220EE16451AF3276D78DE6614D2A414 * get_thread_3() const { return ___thread_3; }
	inline Thread_tB9EB71664220EE16451AF3276D78DE6614D2A414 ** get_address_of_thread_3() { return &___thread_3; }
	inline void set_thread_3(Thread_tB9EB71664220EE16451AF3276D78DE6614D2A414 * value)
	{
		___thread_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___thread_3), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.Threading.ExecutionContextSwitcher
struct ExecutionContextSwitcher_t11B7DEE83408478EE3D5E29C988E5385AA9D7277_marshaled_pinvoke
{
	Reader_t6C70587C0F5A8CE8367A0407E3109E196764848C_marshaled_pinvoke ___outerEC_0;
	int32_t ___outerECBelongsToScope_1;
	Il2CppIUnknown* ___hecsw_2;
	Thread_tB9EB71664220EE16451AF3276D78DE6614D2A414 * ___thread_3;
};
// Native definition for COM marshalling of System.Threading.ExecutionContextSwitcher
struct ExecutionContextSwitcher_t11B7DEE83408478EE3D5E29C988E5385AA9D7277_marshaled_com
{
	Reader_t6C70587C0F5A8CE8367A0407E3109E196764848C_marshaled_com ___outerEC_0;
	int32_t ___outerECBelongsToScope_1;
	Il2CppIUnknown* ___hecsw_2;
	Thread_tB9EB71664220EE16451AF3276D78DE6614D2A414 * ___thread_3;
};

// System.Threading.Tasks.Task
struct  Task_t804B25CFE3FC13AAEE16C8FA3BF52513F2A8DB60  : public RuntimeObject
{
public:
	// System.Int32 modreq(System.Runtime.CompilerServices.IsVolatile) System.Threading.Tasks.Task::m_taskId
	int32_t ___m_taskId_4;
	// System.Object System.Threading.Tasks.Task::m_action
	RuntimeObject * ___m_action_5;
	// System.Object System.Threading.Tasks.Task::m_stateObject
	RuntimeObject * ___m_stateObject_6;
	// System.Threading.Tasks.TaskScheduler System.Threading.Tasks.Task::m_taskScheduler
	TaskScheduler_t74FBEEEDBDD5E0088FF0EEC18F45CD866B098D5D * ___m_taskScheduler_7;
	// System.Threading.Tasks.Task System.Threading.Tasks.Task::m_parent
	Task_t804B25CFE3FC13AAEE16C8FA3BF52513F2A8DB60 * ___m_parent_8;
	// System.Int32 modreq(System.Runtime.CompilerServices.IsVolatile) System.Threading.Tasks.Task::m_stateFlags
	int32_t ___m_stateFlags_9;
	// System.Object modreq(System.Runtime.CompilerServices.IsVolatile) System.Threading.Tasks.Task::m_continuationObject
	RuntimeObject * ___m_continuationObject_10;
	// System.Threading.Tasks.Task_ContingentProperties modreq(System.Runtime.CompilerServices.IsVolatile) System.Threading.Tasks.Task::m_contingentProperties
	ContingentProperties_t1E249C737B8B8644ED1D60EEFA101D326B199EA0 * ___m_contingentProperties_15;

public:
	inline static int32_t get_offset_of_m_taskId_4() { return static_cast<int32_t>(offsetof(Task_t804B25CFE3FC13AAEE16C8FA3BF52513F2A8DB60, ___m_taskId_4)); }
	inline int32_t get_m_taskId_4() const { return ___m_taskId_4; }
	inline int32_t* get_address_of_m_taskId_4() { return &___m_taskId_4; }
	inline void set_m_taskId_4(int32_t value)
	{
		___m_taskId_4 = value;
	}

	inline static int32_t get_offset_of_m_action_5() { return static_cast<int32_t>(offsetof(Task_t804B25CFE3FC13AAEE16C8FA3BF52513F2A8DB60, ___m_action_5)); }
	inline RuntimeObject * get_m_action_5() const { return ___m_action_5; }
	inline RuntimeObject ** get_address_of_m_action_5() { return &___m_action_5; }
	inline void set_m_action_5(RuntimeObject * value)
	{
		___m_action_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_action_5), (void*)value);
	}

	inline static int32_t get_offset_of_m_stateObject_6() { return static_cast<int32_t>(offsetof(Task_t804B25CFE3FC13AAEE16C8FA3BF52513F2A8DB60, ___m_stateObject_6)); }
	inline RuntimeObject * get_m_stateObject_6() const { return ___m_stateObject_6; }
	inline RuntimeObject ** get_address_of_m_stateObject_6() { return &___m_stateObject_6; }
	inline void set_m_stateObject_6(RuntimeObject * value)
	{
		___m_stateObject_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_stateObject_6), (void*)value);
	}

	inline static int32_t get_offset_of_m_taskScheduler_7() { return static_cast<int32_t>(offsetof(Task_t804B25CFE3FC13AAEE16C8FA3BF52513F2A8DB60, ___m_taskScheduler_7)); }
	inline TaskScheduler_t74FBEEEDBDD5E0088FF0EEC18F45CD866B098D5D * get_m_taskScheduler_7() const { return ___m_taskScheduler_7; }
	inline TaskScheduler_t74FBEEEDBDD5E0088FF0EEC18F45CD866B098D5D ** get_address_of_m_taskScheduler_7() { return &___m_taskScheduler_7; }
	inline void set_m_taskScheduler_7(TaskScheduler_t74FBEEEDBDD5E0088FF0EEC18F45CD866B098D5D * value)
	{
		___m_taskScheduler_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_taskScheduler_7), (void*)value);
	}

	inline static int32_t get_offset_of_m_parent_8() { return static_cast<int32_t>(offsetof(Task_t804B25CFE3FC13AAEE16C8FA3BF52513F2A8DB60, ___m_parent_8)); }
	inline Task_t804B25CFE3FC13AAEE16C8FA3BF52513F2A8DB60 * get_m_parent_8() const { return ___m_parent_8; }
	inline Task_t804B25CFE3FC13AAEE16C8FA3BF52513F2A8DB60 ** get_address_of_m_parent_8() { return &___m_parent_8; }
	inline void set_m_parent_8(Task_t804B25CFE3FC13AAEE16C8FA3BF52513F2A8DB60 * value)
	{
		___m_parent_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_parent_8), (void*)value);
	}

	inline static int32_t get_offset_of_m_stateFlags_9() { return static_cast<int32_t>(offsetof(Task_t804B25CFE3FC13AAEE16C8FA3BF52513F2A8DB60, ___m_stateFlags_9)); }
	inline int32_t get_m_stateFlags_9() const { return ___m_stateFlags_9; }
	inline int32_t* get_address_of_m_stateFlags_9() { return &___m_stateFlags_9; }
	inline void set_m_stateFlags_9(int32_t value)
	{
		___m_stateFlags_9 = value;
	}

	inline static int32_t get_offset_of_m_continuationObject_10() { return static_cast<int32_t>(offsetof(Task_t804B25CFE3FC13AAEE16C8FA3BF52513F2A8DB60, ___m_continuationObject_10)); }
	inline RuntimeObject * get_m_continuationObject_10() const { return ___m_continuationObject_10; }
	inline RuntimeObject ** get_address_of_m_continuationObject_10() { return &___m_continuationObject_10; }
	inline void set_m_continuationObject_10(RuntimeObject * value)
	{
		___m_continuationObject_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_continuationObject_10), (void*)value);
	}

	inline static int32_t get_offset_of_m_contingentProperties_15() { return static_cast<int32_t>(offsetof(Task_t804B25CFE3FC13AAEE16C8FA3BF52513F2A8DB60, ___m_contingentProperties_15)); }
	inline ContingentProperties_t1E249C737B8B8644ED1D60EEFA101D326B199EA0 * get_m_contingentProperties_15() const { return ___m_contingentProperties_15; }
	inline ContingentProperties_t1E249C737B8B8644ED1D60EEFA101D326B199EA0 ** get_address_of_m_contingentProperties_15() { return &___m_contingentProperties_15; }
	inline void set_m_contingentProperties_15(ContingentProperties_t1E249C737B8B8644ED1D60EEFA101D326B199EA0 * value)
	{
		___m_contingentProperties_15 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_contingentProperties_15), (void*)value);
	}
};

struct Task_t804B25CFE3FC13AAEE16C8FA3BF52513F2A8DB60_StaticFields
{
public:
	// System.Int32 System.Threading.Tasks.Task::s_taskIdCounter
	int32_t ___s_taskIdCounter_2;
	// System.Threading.Tasks.TaskFactory System.Threading.Tasks.Task::s_factory
	TaskFactory_t22D999A05A967C31A4B5FFBD08864809BF35EA3B * ___s_factory_3;
	// System.Object System.Threading.Tasks.Task::s_taskCompletionSentinel
	RuntimeObject * ___s_taskCompletionSentinel_11;
	// System.Boolean System.Threading.Tasks.Task::s_asyncDebuggingEnabled
	bool ___s_asyncDebuggingEnabled_12;
	// System.Collections.Generic.Dictionary`2<System.Int32,System.Threading.Tasks.Task> System.Threading.Tasks.Task::s_currentActiveTasks
	Dictionary_2_tB758E2A2593CD827EFC041BE1F1BB4B68DE1C3E8 * ___s_currentActiveTasks_13;
	// System.Object System.Threading.Tasks.Task::s_activeTasksLock
	RuntimeObject * ___s_activeTasksLock_14;
	// System.Action`1<System.Object> System.Threading.Tasks.Task::s_taskCancelCallback
	Action_1_tD9663D9715FAA4E62035CFCF1AD4D094EE7872DC * ___s_taskCancelCallback_16;
	// System.Func`1<System.Threading.Tasks.Task_ContingentProperties> System.Threading.Tasks.Task::s_createContingentProperties
	Func_1_tBCF42601FA307876E83080BE4204110820F8BF3B * ___s_createContingentProperties_17;
	// System.Threading.Tasks.Task System.Threading.Tasks.Task::s_completedTask
	Task_t804B25CFE3FC13AAEE16C8FA3BF52513F2A8DB60 * ___s_completedTask_18;
	// System.Predicate`1<System.Threading.Tasks.Task> System.Threading.Tasks.Task::s_IsExceptionObservedByParentPredicate
	Predicate_1_tC0DBBC8498BD1EE6ABFFAA5628024105FA7D11BD * ___s_IsExceptionObservedByParentPredicate_19;
	// System.Threading.ContextCallback System.Threading.Tasks.Task::s_ecCallback
	ContextCallback_t93707E0430F4FF3E15E1FB5A4844BE89C657AE8B * ___s_ecCallback_20;
	// System.Predicate`1<System.Object> System.Threading.Tasks.Task::s_IsTaskContinuationNullPredicate
	Predicate_1_t5C96B81B31A697B11C4C3767E3298773AF25DFEB * ___s_IsTaskContinuationNullPredicate_21;

public:
	inline static int32_t get_offset_of_s_taskIdCounter_2() { return static_cast<int32_t>(offsetof(Task_t804B25CFE3FC13AAEE16C8FA3BF52513F2A8DB60_StaticFields, ___s_taskIdCounter_2)); }
	inline int32_t get_s_taskIdCounter_2() const { return ___s_taskIdCounter_2; }
	inline int32_t* get_address_of_s_taskIdCounter_2() { return &___s_taskIdCounter_2; }
	inline void set_s_taskIdCounter_2(int32_t value)
	{
		___s_taskIdCounter_2 = value;
	}

	inline static int32_t get_offset_of_s_factory_3() { return static_cast<int32_t>(offsetof(Task_t804B25CFE3FC13AAEE16C8FA3BF52513F2A8DB60_StaticFields, ___s_factory_3)); }
	inline TaskFactory_t22D999A05A967C31A4B5FFBD08864809BF35EA3B * get_s_factory_3() const { return ___s_factory_3; }
	inline TaskFactory_t22D999A05A967C31A4B5FFBD08864809BF35EA3B ** get_address_of_s_factory_3() { return &___s_factory_3; }
	inline void set_s_factory_3(TaskFactory_t22D999A05A967C31A4B5FFBD08864809BF35EA3B * value)
	{
		___s_factory_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_factory_3), (void*)value);
	}

	inline static int32_t get_offset_of_s_taskCompletionSentinel_11() { return static_cast<int32_t>(offsetof(Task_t804B25CFE3FC13AAEE16C8FA3BF52513F2A8DB60_StaticFields, ___s_taskCompletionSentinel_11)); }
	inline RuntimeObject * get_s_taskCompletionSentinel_11() const { return ___s_taskCompletionSentinel_11; }
	inline RuntimeObject ** get_address_of_s_taskCompletionSentinel_11() { return &___s_taskCompletionSentinel_11; }
	inline void set_s_taskCompletionSentinel_11(RuntimeObject * value)
	{
		___s_taskCompletionSentinel_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_taskCompletionSentinel_11), (void*)value);
	}

	inline static int32_t get_offset_of_s_asyncDebuggingEnabled_12() { return static_cast<int32_t>(offsetof(Task_t804B25CFE3FC13AAEE16C8FA3BF52513F2A8DB60_StaticFields, ___s_asyncDebuggingEnabled_12)); }
	inline bool get_s_asyncDebuggingEnabled_12() const { return ___s_asyncDebuggingEnabled_12; }
	inline bool* get_address_of_s_asyncDebuggingEnabled_12() { return &___s_asyncDebuggingEnabled_12; }
	inline void set_s_asyncDebuggingEnabled_12(bool value)
	{
		___s_asyncDebuggingEnabled_12 = value;
	}

	inline static int32_t get_offset_of_s_currentActiveTasks_13() { return static_cast<int32_t>(offsetof(Task_t804B25CFE3FC13AAEE16C8FA3BF52513F2A8DB60_StaticFields, ___s_currentActiveTasks_13)); }
	inline Dictionary_2_tB758E2A2593CD827EFC041BE1F1BB4B68DE1C3E8 * get_s_currentActiveTasks_13() const { return ___s_currentActiveTasks_13; }
	inline Dictionary_2_tB758E2A2593CD827EFC041BE1F1BB4B68DE1C3E8 ** get_address_of_s_currentActiveTasks_13() { return &___s_currentActiveTasks_13; }
	inline void set_s_currentActiveTasks_13(Dictionary_2_tB758E2A2593CD827EFC041BE1F1BB4B68DE1C3E8 * value)
	{
		___s_currentActiveTasks_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_currentActiveTasks_13), (void*)value);
	}

	inline static int32_t get_offset_of_s_activeTasksLock_14() { return static_cast<int32_t>(offsetof(Task_t804B25CFE3FC13AAEE16C8FA3BF52513F2A8DB60_StaticFields, ___s_activeTasksLock_14)); }
	inline RuntimeObject * get_s_activeTasksLock_14() const { return ___s_activeTasksLock_14; }
	inline RuntimeObject ** get_address_of_s_activeTasksLock_14() { return &___s_activeTasksLock_14; }
	inline void set_s_activeTasksLock_14(RuntimeObject * value)
	{
		___s_activeTasksLock_14 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_activeTasksLock_14), (void*)value);
	}

	inline static int32_t get_offset_of_s_taskCancelCallback_16() { return static_cast<int32_t>(offsetof(Task_t804B25CFE3FC13AAEE16C8FA3BF52513F2A8DB60_StaticFields, ___s_taskCancelCallback_16)); }
	inline Action_1_tD9663D9715FAA4E62035CFCF1AD4D094EE7872DC * get_s_taskCancelCallback_16() const { return ___s_taskCancelCallback_16; }
	inline Action_1_tD9663D9715FAA4E62035CFCF1AD4D094EE7872DC ** get_address_of_s_taskCancelCallback_16() { return &___s_taskCancelCallback_16; }
	inline void set_s_taskCancelCallback_16(Action_1_tD9663D9715FAA4E62035CFCF1AD4D094EE7872DC * value)
	{
		___s_taskCancelCallback_16 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_taskCancelCallback_16), (void*)value);
	}

	inline static int32_t get_offset_of_s_createContingentProperties_17() { return static_cast<int32_t>(offsetof(Task_t804B25CFE3FC13AAEE16C8FA3BF52513F2A8DB60_StaticFields, ___s_createContingentProperties_17)); }
	inline Func_1_tBCF42601FA307876E83080BE4204110820F8BF3B * get_s_createContingentProperties_17() const { return ___s_createContingentProperties_17; }
	inline Func_1_tBCF42601FA307876E83080BE4204110820F8BF3B ** get_address_of_s_createContingentProperties_17() { return &___s_createContingentProperties_17; }
	inline void set_s_createContingentProperties_17(Func_1_tBCF42601FA307876E83080BE4204110820F8BF3B * value)
	{
		___s_createContingentProperties_17 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_createContingentProperties_17), (void*)value);
	}

	inline static int32_t get_offset_of_s_completedTask_18() { return static_cast<int32_t>(offsetof(Task_t804B25CFE3FC13AAEE16C8FA3BF52513F2A8DB60_StaticFields, ___s_completedTask_18)); }
	inline Task_t804B25CFE3FC13AAEE16C8FA3BF52513F2A8DB60 * get_s_completedTask_18() const { return ___s_completedTask_18; }
	inline Task_t804B25CFE3FC13AAEE16C8FA3BF52513F2A8DB60 ** get_address_of_s_completedTask_18() { return &___s_completedTask_18; }
	inline void set_s_completedTask_18(Task_t804B25CFE3FC13AAEE16C8FA3BF52513F2A8DB60 * value)
	{
		___s_completedTask_18 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_completedTask_18), (void*)value);
	}

	inline static int32_t get_offset_of_s_IsExceptionObservedByParentPredicate_19() { return static_cast<int32_t>(offsetof(Task_t804B25CFE3FC13AAEE16C8FA3BF52513F2A8DB60_StaticFields, ___s_IsExceptionObservedByParentPredicate_19)); }
	inline Predicate_1_tC0DBBC8498BD1EE6ABFFAA5628024105FA7D11BD * get_s_IsExceptionObservedByParentPredicate_19() const { return ___s_IsExceptionObservedByParentPredicate_19; }
	inline Predicate_1_tC0DBBC8498BD1EE6ABFFAA5628024105FA7D11BD ** get_address_of_s_IsExceptionObservedByParentPredicate_19() { return &___s_IsExceptionObservedByParentPredicate_19; }
	inline void set_s_IsExceptionObservedByParentPredicate_19(Predicate_1_tC0DBBC8498BD1EE6ABFFAA5628024105FA7D11BD * value)
	{
		___s_IsExceptionObservedByParentPredicate_19 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_IsExceptionObservedByParentPredicate_19), (void*)value);
	}

	inline static int32_t get_offset_of_s_ecCallback_20() { return static_cast<int32_t>(offsetof(Task_t804B25CFE3FC13AAEE16C8FA3BF52513F2A8DB60_StaticFields, ___s_ecCallback_20)); }
	inline ContextCallback_t93707E0430F4FF3E15E1FB5A4844BE89C657AE8B * get_s_ecCallback_20() const { return ___s_ecCallback_20; }
	inline ContextCallback_t93707E0430F4FF3E15E1FB5A4844BE89C657AE8B ** get_address_of_s_ecCallback_20() { return &___s_ecCallback_20; }
	inline void set_s_ecCallback_20(ContextCallback_t93707E0430F4FF3E15E1FB5A4844BE89C657AE8B * value)
	{
		___s_ecCallback_20 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_ecCallback_20), (void*)value);
	}

	inline static int32_t get_offset_of_s_IsTaskContinuationNullPredicate_21() { return static_cast<int32_t>(offsetof(Task_t804B25CFE3FC13AAEE16C8FA3BF52513F2A8DB60_StaticFields, ___s_IsTaskContinuationNullPredicate_21)); }
	inline Predicate_1_t5C96B81B31A697B11C4C3767E3298773AF25DFEB * get_s_IsTaskContinuationNullPredicate_21() const { return ___s_IsTaskContinuationNullPredicate_21; }
	inline Predicate_1_t5C96B81B31A697B11C4C3767E3298773AF25DFEB ** get_address_of_s_IsTaskContinuationNullPredicate_21() { return &___s_IsTaskContinuationNullPredicate_21; }
	inline void set_s_IsTaskContinuationNullPredicate_21(Predicate_1_t5C96B81B31A697B11C4C3767E3298773AF25DFEB * value)
	{
		___s_IsTaskContinuationNullPredicate_21 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_IsTaskContinuationNullPredicate_21), (void*)value);
	}
};

struct Task_t804B25CFE3FC13AAEE16C8FA3BF52513F2A8DB60_ThreadStaticFields
{
public:
	// System.Threading.Tasks.Task System.Threading.Tasks.Task::t_currentTask
	Task_t804B25CFE3FC13AAEE16C8FA3BF52513F2A8DB60 * ___t_currentTask_0;
	// System.Threading.Tasks.StackGuard System.Threading.Tasks.Task::t_stackGuard
	StackGuard_t88E1EE4741AD02CA5FEA04A4EB2CC70F230E0E6D * ___t_stackGuard_1;

public:
	inline static int32_t get_offset_of_t_currentTask_0() { return static_cast<int32_t>(offsetof(Task_t804B25CFE3FC13AAEE16C8FA3BF52513F2A8DB60_ThreadStaticFields, ___t_currentTask_0)); }
	inline Task_t804B25CFE3FC13AAEE16C8FA3BF52513F2A8DB60 * get_t_currentTask_0() const { return ___t_currentTask_0; }
	inline Task_t804B25CFE3FC13AAEE16C8FA3BF52513F2A8DB60 ** get_address_of_t_currentTask_0() { return &___t_currentTask_0; }
	inline void set_t_currentTask_0(Task_t804B25CFE3FC13AAEE16C8FA3BF52513F2A8DB60 * value)
	{
		___t_currentTask_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___t_currentTask_0), (void*)value);
	}

	inline static int32_t get_offset_of_t_stackGuard_1() { return static_cast<int32_t>(offsetof(Task_t804B25CFE3FC13AAEE16C8FA3BF52513F2A8DB60_ThreadStaticFields, ___t_stackGuard_1)); }
	inline StackGuard_t88E1EE4741AD02CA5FEA04A4EB2CC70F230E0E6D * get_t_stackGuard_1() const { return ___t_stackGuard_1; }
	inline StackGuard_t88E1EE4741AD02CA5FEA04A4EB2CC70F230E0E6D ** get_address_of_t_stackGuard_1() { return &___t_stackGuard_1; }
	inline void set_t_stackGuard_1(StackGuard_t88E1EE4741AD02CA5FEA04A4EB2CC70F230E0E6D * value)
	{
		___t_stackGuard_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___t_stackGuard_1), (void*)value);
	}
};


// Unity.Collections.Allocator
struct  Allocator_t9888223DEF4F46F3419ECFCCD0753599BEE52A05 
{
public:
	// System.Int32 Unity.Collections.Allocator::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Allocator_t9888223DEF4F46F3419ECFCCD0753599BEE52A05, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.Experimental.GlobalIllumination.FalloffType
struct  FalloffType_t983DA2C11C909629E51BD1D4CF088C689C9863CB 
{
public:
	// System.Byte UnityEngine.Experimental.GlobalIllumination.FalloffType::value__
	uint8_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(FalloffType_t983DA2C11C909629E51BD1D4CF088C689C9863CB, ___value___2)); }
	inline uint8_t get_value___2() const { return ___value___2; }
	inline uint8_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(uint8_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.Experimental.GlobalIllumination.LightMode
struct  LightMode_t9D89979F39C1DBB9CD1E275BDD77C7EA1B506491 
{
public:
	// System.Byte UnityEngine.Experimental.GlobalIllumination.LightMode::value__
	uint8_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(LightMode_t9D89979F39C1DBB9CD1E275BDD77C7EA1B506491, ___value___2)); }
	inline uint8_t get_value___2() const { return ___value___2; }
	inline uint8_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(uint8_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.Experimental.GlobalIllumination.LightType
struct  LightType_t4205DE4BEF130CE507C87172DAB60E5B1EB05552 
{
public:
	// System.Byte UnityEngine.Experimental.GlobalIllumination.LightType::value__
	uint8_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(LightType_t4205DE4BEF130CE507C87172DAB60E5B1EB05552, ___value___2)); }
	inline uint8_t get_value___2() const { return ___value___2; }
	inline uint8_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(uint8_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.Plane
struct  Plane_t80844BF2332EAFC1DDEDD616A950242031A115C7 
{
public:
	// UnityEngine.Vector3 UnityEngine.Plane::m_Normal
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___m_Normal_1;
	// System.Single UnityEngine.Plane::m_Distance
	float ___m_Distance_2;

public:
	inline static int32_t get_offset_of_m_Normal_1() { return static_cast<int32_t>(offsetof(Plane_t80844BF2332EAFC1DDEDD616A950242031A115C7, ___m_Normal_1)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_m_Normal_1() const { return ___m_Normal_1; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_m_Normal_1() { return &___m_Normal_1; }
	inline void set_m_Normal_1(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___m_Normal_1 = value;
	}

	inline static int32_t get_offset_of_m_Distance_2() { return static_cast<int32_t>(offsetof(Plane_t80844BF2332EAFC1DDEDD616A950242031A115C7, ___m_Distance_2)); }
	inline float get_m_Distance_2() const { return ___m_Distance_2; }
	inline float* get_address_of_m_Distance_2() { return &___m_Distance_2; }
	inline void set_m_Distance_2(float value)
	{
		___m_Distance_2 = value;
	}
};


// Obi.ColliderShape
struct  ColliderShape_t7AF5542901AEB2CFA54B6BFD8C852EC91554F586 
{
public:
	// UnityEngine.Vector4 Obi.ColliderShape::center
	Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  ___center_0;
	// UnityEngine.Vector4 Obi.ColliderShape::size
	Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  ___size_1;
	// Obi.ColliderShape_ShapeType Obi.ColliderShape::type
	int32_t ___type_2;
	// System.Single Obi.ColliderShape::contactOffset
	float ___contactOffset_3;
	// System.Int32 Obi.ColliderShape::dataIndex
	int32_t ___dataIndex_4;
	// System.Int32 Obi.ColliderShape::rigidbodyIndex
	int32_t ___rigidbodyIndex_5;
	// System.Int32 Obi.ColliderShape::materialIndex
	int32_t ___materialIndex_6;
	// System.Int32 Obi.ColliderShape::filter
	int32_t ___filter_7;
	// System.Int32 Obi.ColliderShape::flags
	int32_t ___flags_8;
	// System.Int32 Obi.ColliderShape::is2D
	int32_t ___is2D_9;

public:
	inline static int32_t get_offset_of_center_0() { return static_cast<int32_t>(offsetof(ColliderShape_t7AF5542901AEB2CFA54B6BFD8C852EC91554F586, ___center_0)); }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  get_center_0() const { return ___center_0; }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7 * get_address_of_center_0() { return &___center_0; }
	inline void set_center_0(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  value)
	{
		___center_0 = value;
	}

	inline static int32_t get_offset_of_size_1() { return static_cast<int32_t>(offsetof(ColliderShape_t7AF5542901AEB2CFA54B6BFD8C852EC91554F586, ___size_1)); }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  get_size_1() const { return ___size_1; }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7 * get_address_of_size_1() { return &___size_1; }
	inline void set_size_1(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  value)
	{
		___size_1 = value;
	}

	inline static int32_t get_offset_of_type_2() { return static_cast<int32_t>(offsetof(ColliderShape_t7AF5542901AEB2CFA54B6BFD8C852EC91554F586, ___type_2)); }
	inline int32_t get_type_2() const { return ___type_2; }
	inline int32_t* get_address_of_type_2() { return &___type_2; }
	inline void set_type_2(int32_t value)
	{
		___type_2 = value;
	}

	inline static int32_t get_offset_of_contactOffset_3() { return static_cast<int32_t>(offsetof(ColliderShape_t7AF5542901AEB2CFA54B6BFD8C852EC91554F586, ___contactOffset_3)); }
	inline float get_contactOffset_3() const { return ___contactOffset_3; }
	inline float* get_address_of_contactOffset_3() { return &___contactOffset_3; }
	inline void set_contactOffset_3(float value)
	{
		___contactOffset_3 = value;
	}

	inline static int32_t get_offset_of_dataIndex_4() { return static_cast<int32_t>(offsetof(ColliderShape_t7AF5542901AEB2CFA54B6BFD8C852EC91554F586, ___dataIndex_4)); }
	inline int32_t get_dataIndex_4() const { return ___dataIndex_4; }
	inline int32_t* get_address_of_dataIndex_4() { return &___dataIndex_4; }
	inline void set_dataIndex_4(int32_t value)
	{
		___dataIndex_4 = value;
	}

	inline static int32_t get_offset_of_rigidbodyIndex_5() { return static_cast<int32_t>(offsetof(ColliderShape_t7AF5542901AEB2CFA54B6BFD8C852EC91554F586, ___rigidbodyIndex_5)); }
	inline int32_t get_rigidbodyIndex_5() const { return ___rigidbodyIndex_5; }
	inline int32_t* get_address_of_rigidbodyIndex_5() { return &___rigidbodyIndex_5; }
	inline void set_rigidbodyIndex_5(int32_t value)
	{
		___rigidbodyIndex_5 = value;
	}

	inline static int32_t get_offset_of_materialIndex_6() { return static_cast<int32_t>(offsetof(ColliderShape_t7AF5542901AEB2CFA54B6BFD8C852EC91554F586, ___materialIndex_6)); }
	inline int32_t get_materialIndex_6() const { return ___materialIndex_6; }
	inline int32_t* get_address_of_materialIndex_6() { return &___materialIndex_6; }
	inline void set_materialIndex_6(int32_t value)
	{
		___materialIndex_6 = value;
	}

	inline static int32_t get_offset_of_filter_7() { return static_cast<int32_t>(offsetof(ColliderShape_t7AF5542901AEB2CFA54B6BFD8C852EC91554F586, ___filter_7)); }
	inline int32_t get_filter_7() const { return ___filter_7; }
	inline int32_t* get_address_of_filter_7() { return &___filter_7; }
	inline void set_filter_7(int32_t value)
	{
		___filter_7 = value;
	}

	inline static int32_t get_offset_of_flags_8() { return static_cast<int32_t>(offsetof(ColliderShape_t7AF5542901AEB2CFA54B6BFD8C852EC91554F586, ___flags_8)); }
	inline int32_t get_flags_8() const { return ___flags_8; }
	inline int32_t* get_address_of_flags_8() { return &___flags_8; }
	inline void set_flags_8(int32_t value)
	{
		___flags_8 = value;
	}

	inline static int32_t get_offset_of_is2D_9() { return static_cast<int32_t>(offsetof(ColliderShape_t7AF5542901AEB2CFA54B6BFD8C852EC91554F586, ___is2D_9)); }
	inline int32_t get_is2D_9() const { return ___is2D_9; }
	inline int32_t* get_address_of_is2D_9() { return &___is2D_9; }
	inline void set_is2D_9(int32_t value)
	{
		___is2D_9 = value;
	}
};


// Obi.CollisionMaterial
struct  CollisionMaterial_t7582B504E28F3D7E580DBB66ECD74F967C92E758 
{
public:
	// System.Single Obi.CollisionMaterial::dynamicFriction
	float ___dynamicFriction_0;
	// System.Single Obi.CollisionMaterial::staticFriction
	float ___staticFriction_1;
	// System.Single Obi.CollisionMaterial::rollingFriction
	float ___rollingFriction_2;
	// System.Single Obi.CollisionMaterial::stickiness
	float ___stickiness_3;
	// System.Single Obi.CollisionMaterial::stickDistance
	float ___stickDistance_4;
	// Oni_MaterialCombineMode Obi.CollisionMaterial::frictionCombine
	int32_t ___frictionCombine_5;
	// Oni_MaterialCombineMode Obi.CollisionMaterial::stickinessCombine
	int32_t ___stickinessCombine_6;
	// System.Int32 Obi.CollisionMaterial::rollingContacts
	int32_t ___rollingContacts_7;

public:
	inline static int32_t get_offset_of_dynamicFriction_0() { return static_cast<int32_t>(offsetof(CollisionMaterial_t7582B504E28F3D7E580DBB66ECD74F967C92E758, ___dynamicFriction_0)); }
	inline float get_dynamicFriction_0() const { return ___dynamicFriction_0; }
	inline float* get_address_of_dynamicFriction_0() { return &___dynamicFriction_0; }
	inline void set_dynamicFriction_0(float value)
	{
		___dynamicFriction_0 = value;
	}

	inline static int32_t get_offset_of_staticFriction_1() { return static_cast<int32_t>(offsetof(CollisionMaterial_t7582B504E28F3D7E580DBB66ECD74F967C92E758, ___staticFriction_1)); }
	inline float get_staticFriction_1() const { return ___staticFriction_1; }
	inline float* get_address_of_staticFriction_1() { return &___staticFriction_1; }
	inline void set_staticFriction_1(float value)
	{
		___staticFriction_1 = value;
	}

	inline static int32_t get_offset_of_rollingFriction_2() { return static_cast<int32_t>(offsetof(CollisionMaterial_t7582B504E28F3D7E580DBB66ECD74F967C92E758, ___rollingFriction_2)); }
	inline float get_rollingFriction_2() const { return ___rollingFriction_2; }
	inline float* get_address_of_rollingFriction_2() { return &___rollingFriction_2; }
	inline void set_rollingFriction_2(float value)
	{
		___rollingFriction_2 = value;
	}

	inline static int32_t get_offset_of_stickiness_3() { return static_cast<int32_t>(offsetof(CollisionMaterial_t7582B504E28F3D7E580DBB66ECD74F967C92E758, ___stickiness_3)); }
	inline float get_stickiness_3() const { return ___stickiness_3; }
	inline float* get_address_of_stickiness_3() { return &___stickiness_3; }
	inline void set_stickiness_3(float value)
	{
		___stickiness_3 = value;
	}

	inline static int32_t get_offset_of_stickDistance_4() { return static_cast<int32_t>(offsetof(CollisionMaterial_t7582B504E28F3D7E580DBB66ECD74F967C92E758, ___stickDistance_4)); }
	inline float get_stickDistance_4() const { return ___stickDistance_4; }
	inline float* get_address_of_stickDistance_4() { return &___stickDistance_4; }
	inline void set_stickDistance_4(float value)
	{
		___stickDistance_4 = value;
	}

	inline static int32_t get_offset_of_frictionCombine_5() { return static_cast<int32_t>(offsetof(CollisionMaterial_t7582B504E28F3D7E580DBB66ECD74F967C92E758, ___frictionCombine_5)); }
	inline int32_t get_frictionCombine_5() const { return ___frictionCombine_5; }
	inline int32_t* get_address_of_frictionCombine_5() { return &___frictionCombine_5; }
	inline void set_frictionCombine_5(int32_t value)
	{
		___frictionCombine_5 = value;
	}

	inline static int32_t get_offset_of_stickinessCombine_6() { return static_cast<int32_t>(offsetof(CollisionMaterial_t7582B504E28F3D7E580DBB66ECD74F967C92E758, ___stickinessCombine_6)); }
	inline int32_t get_stickinessCombine_6() const { return ___stickinessCombine_6; }
	inline int32_t* get_address_of_stickinessCombine_6() { return &___stickinessCombine_6; }
	inline void set_stickinessCombine_6(int32_t value)
	{
		___stickinessCombine_6 = value;
	}

	inline static int32_t get_offset_of_rollingContacts_7() { return static_cast<int32_t>(offsetof(CollisionMaterial_t7582B504E28F3D7E580DBB66ECD74F967C92E758, ___rollingContacts_7)); }
	inline int32_t get_rollingContacts_7() const { return ___rollingContacts_7; }
	inline int32_t* get_address_of_rollingContacts_7() { return &___rollingContacts_7; }
	inline void set_rollingContacts_7(int32_t value)
	{
		___rollingContacts_7 = value;
	}
};


// Obi.Edge
struct  Edge_tB72B5CE4AD9C11C74696DF37DCED8FB9C2FFDD3C 
{
public:
	// System.Int32 Obi.Edge::i1
	int32_t ___i1_0;
	// System.Int32 Obi.Edge::i2
	int32_t ___i2_1;
	// Obi.Aabb Obi.Edge::b
	Aabb_t4DF77A399A9E4CB5D52707E4B8B53D3483CD84CF  ___b_2;

public:
	inline static int32_t get_offset_of_i1_0() { return static_cast<int32_t>(offsetof(Edge_tB72B5CE4AD9C11C74696DF37DCED8FB9C2FFDD3C, ___i1_0)); }
	inline int32_t get_i1_0() const { return ___i1_0; }
	inline int32_t* get_address_of_i1_0() { return &___i1_0; }
	inline void set_i1_0(int32_t value)
	{
		___i1_0 = value;
	}

	inline static int32_t get_offset_of_i2_1() { return static_cast<int32_t>(offsetof(Edge_tB72B5CE4AD9C11C74696DF37DCED8FB9C2FFDD3C, ___i2_1)); }
	inline int32_t get_i2_1() const { return ___i2_1; }
	inline int32_t* get_address_of_i2_1() { return &___i2_1; }
	inline void set_i2_1(int32_t value)
	{
		___i2_1 = value;
	}

	inline static int32_t get_offset_of_b_2() { return static_cast<int32_t>(offsetof(Edge_tB72B5CE4AD9C11C74696DF37DCED8FB9C2FFDD3C, ___b_2)); }
	inline Aabb_t4DF77A399A9E4CB5D52707E4B8B53D3483CD84CF  get_b_2() const { return ___b_2; }
	inline Aabb_t4DF77A399A9E4CB5D52707E4B8B53D3483CD84CF * get_address_of_b_2() { return &___b_2; }
	inline void set_b_2(Aabb_t4DF77A399A9E4CB5D52707E4B8B53D3483CD84CF  value)
	{
		___b_2 = value;
	}
};


// Obi.QueryShape
struct  QueryShape_t0A83A6359C90B5213CDEC851B226F8DAAD6D07AF 
{
public:
	// UnityEngine.Vector4 Obi.QueryShape::center
	Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  ___center_0;
	// UnityEngine.Vector4 Obi.QueryShape::size
	Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  ___size_1;
	// Obi.QueryShape_QueryType Obi.QueryShape::type
	int32_t ___type_2;
	// System.Single Obi.QueryShape::contactOffset
	float ___contactOffset_3;
	// System.Single Obi.QueryShape::maxDistance
	float ___maxDistance_4;
	// System.Int32 Obi.QueryShape::filter
	int32_t ___filter_5;

public:
	inline static int32_t get_offset_of_center_0() { return static_cast<int32_t>(offsetof(QueryShape_t0A83A6359C90B5213CDEC851B226F8DAAD6D07AF, ___center_0)); }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  get_center_0() const { return ___center_0; }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7 * get_address_of_center_0() { return &___center_0; }
	inline void set_center_0(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  value)
	{
		___center_0 = value;
	}

	inline static int32_t get_offset_of_size_1() { return static_cast<int32_t>(offsetof(QueryShape_t0A83A6359C90B5213CDEC851B226F8DAAD6D07AF, ___size_1)); }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  get_size_1() const { return ___size_1; }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7 * get_address_of_size_1() { return &___size_1; }
	inline void set_size_1(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  value)
	{
		___size_1 = value;
	}

	inline static int32_t get_offset_of_type_2() { return static_cast<int32_t>(offsetof(QueryShape_t0A83A6359C90B5213CDEC851B226F8DAAD6D07AF, ___type_2)); }
	inline int32_t get_type_2() const { return ___type_2; }
	inline int32_t* get_address_of_type_2() { return &___type_2; }
	inline void set_type_2(int32_t value)
	{
		___type_2 = value;
	}

	inline static int32_t get_offset_of_contactOffset_3() { return static_cast<int32_t>(offsetof(QueryShape_t0A83A6359C90B5213CDEC851B226F8DAAD6D07AF, ___contactOffset_3)); }
	inline float get_contactOffset_3() const { return ___contactOffset_3; }
	inline float* get_address_of_contactOffset_3() { return &___contactOffset_3; }
	inline void set_contactOffset_3(float value)
	{
		___contactOffset_3 = value;
	}

	inline static int32_t get_offset_of_maxDistance_4() { return static_cast<int32_t>(offsetof(QueryShape_t0A83A6359C90B5213CDEC851B226F8DAAD6D07AF, ___maxDistance_4)); }
	inline float get_maxDistance_4() const { return ___maxDistance_4; }
	inline float* get_address_of_maxDistance_4() { return &___maxDistance_4; }
	inline void set_maxDistance_4(float value)
	{
		___maxDistance_4 = value;
	}

	inline static int32_t get_offset_of_filter_5() { return static_cast<int32_t>(offsetof(QueryShape_t0A83A6359C90B5213CDEC851B226F8DAAD6D07AF, ___filter_5)); }
	inline int32_t get_filter_5() const { return ___filter_5; }
	inline int32_t* get_address_of_filter_5() { return &___filter_5; }
	inline void set_filter_5(int32_t value)
	{
		___filter_5 = value;
	}
};


// Obi.Triangle
struct  Triangle_t6EBD8067A5E92E374FBCEE8434826BC760D9FDC8 
{
public:
	// System.Int32 Obi.Triangle::i1
	int32_t ___i1_0;
	// System.Int32 Obi.Triangle::i2
	int32_t ___i2_1;
	// System.Int32 Obi.Triangle::i3
	int32_t ___i3_2;
	// Obi.Aabb Obi.Triangle::b
	Aabb_t4DF77A399A9E4CB5D52707E4B8B53D3483CD84CF  ___b_3;

public:
	inline static int32_t get_offset_of_i1_0() { return static_cast<int32_t>(offsetof(Triangle_t6EBD8067A5E92E374FBCEE8434826BC760D9FDC8, ___i1_0)); }
	inline int32_t get_i1_0() const { return ___i1_0; }
	inline int32_t* get_address_of_i1_0() { return &___i1_0; }
	inline void set_i1_0(int32_t value)
	{
		___i1_0 = value;
	}

	inline static int32_t get_offset_of_i2_1() { return static_cast<int32_t>(offsetof(Triangle_t6EBD8067A5E92E374FBCEE8434826BC760D9FDC8, ___i2_1)); }
	inline int32_t get_i2_1() const { return ___i2_1; }
	inline int32_t* get_address_of_i2_1() { return &___i2_1; }
	inline void set_i2_1(int32_t value)
	{
		___i2_1 = value;
	}

	inline static int32_t get_offset_of_i3_2() { return static_cast<int32_t>(offsetof(Triangle_t6EBD8067A5E92E374FBCEE8434826BC760D9FDC8, ___i3_2)); }
	inline int32_t get_i3_2() const { return ___i3_2; }
	inline int32_t* get_address_of_i3_2() { return &___i3_2; }
	inline void set_i3_2(int32_t value)
	{
		___i3_2 = value;
	}

	inline static int32_t get_offset_of_b_3() { return static_cast<int32_t>(offsetof(Triangle_t6EBD8067A5E92E374FBCEE8434826BC760D9FDC8, ___b_3)); }
	inline Aabb_t4DF77A399A9E4CB5D52707E4B8B53D3483CD84CF  get_b_3() const { return ___b_3; }
	inline Aabb_t4DF77A399A9E4CB5D52707E4B8B53D3483CD84CF * get_address_of_b_3() { return &___b_3; }
	inline void set_b_3(Aabb_t4DF77A399A9E4CB5D52707E4B8B53D3483CD84CF  value)
	{
		___b_3 = value;
	}
};


// System.MulticastDelegate
struct  MulticastDelegate_t  : public Delegate_t
{
public:
	// System.Delegate[] System.MulticastDelegate::delegates
	DelegateU5BU5D_t677D8FE08A5F99E8EE49150B73966CD6E9BF7DB8* ___delegates_11;

public:
	inline static int32_t get_offset_of_delegates_11() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___delegates_11)); }
	inline DelegateU5BU5D_t677D8FE08A5F99E8EE49150B73966CD6E9BF7DB8* get_delegates_11() const { return ___delegates_11; }
	inline DelegateU5BU5D_t677D8FE08A5F99E8EE49150B73966CD6E9BF7DB8** get_address_of_delegates_11() { return &___delegates_11; }
	inline void set_delegates_11(DelegateU5BU5D_t677D8FE08A5F99E8EE49150B73966CD6E9BF7DB8* value)
	{
		___delegates_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___delegates_11), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_pinvoke : public Delegate_t_marshaled_pinvoke
{
	Delegate_t_marshaled_pinvoke** ___delegates_11;
};
// Native definition for COM marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_com : public Delegate_t_marshaled_com
{
	Delegate_t_marshaled_com** ___delegates_11;
};

// System.SystemException
struct  SystemException_tC551B4D6EE3772B5F32C71EE8C719F4B43ECCC62  : public Exception_t
{
public:

public:
};


// System.Threading.SemaphoreSlim_<WaitUntilCountOrTimeoutAsync>d__31
struct  U3CWaitUntilCountOrTimeoutAsyncU3Ed__31_t227D1F5F19C948CA8C23C80B5F19147D4AAED14F 
{
public:
	// System.Int32 System.Threading.SemaphoreSlim_<WaitUntilCountOrTimeoutAsync>d__31::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Runtime.CompilerServices.AsyncTaskMethodBuilder`1<System.Boolean> System.Threading.SemaphoreSlim_<WaitUntilCountOrTimeoutAsync>d__31::<>t__builder
	AsyncTaskMethodBuilder_1_tABE1DEF12F121D6FC8ABF04869ED964FF83EA9B5  ___U3CU3Et__builder_1;
	// System.Threading.CancellationToken System.Threading.SemaphoreSlim_<WaitUntilCountOrTimeoutAsync>d__31::cancellationToken
	CancellationToken_tC9D68381C9164A4BA10397257E87ADC832AF5FFD  ___cancellationToken_2;
	// System.Threading.SemaphoreSlim_TaskNode System.Threading.SemaphoreSlim_<WaitUntilCountOrTimeoutAsync>d__31::asyncWaiter
	TaskNode_tD3014A57510D018F890E6524AC62F9417E2E6C4E * ___asyncWaiter_3;
	// System.Int32 System.Threading.SemaphoreSlim_<WaitUntilCountOrTimeoutAsync>d__31::millisecondsTimeout
	int32_t ___millisecondsTimeout_4;
	// System.Threading.CancellationTokenSource System.Threading.SemaphoreSlim_<WaitUntilCountOrTimeoutAsync>d__31::<cts>5__1
	CancellationTokenSource_t78B989179DE23EDD36F870FFEE20A15D6D3C65B3 * ___U3CctsU3E5__1_5;
	// System.Threading.SemaphoreSlim System.Threading.SemaphoreSlim_<WaitUntilCountOrTimeoutAsync>d__31::<>4__this
	SemaphoreSlim_t3EF85FC980AE57957BEBB6B78E81DE2E3233D385 * ___U3CU3E4__this_6;
	// System.Object System.Threading.SemaphoreSlim_<WaitUntilCountOrTimeoutAsync>d__31::<>7__wrap1
	RuntimeObject * ___U3CU3E7__wrap1_7;
	// System.Runtime.CompilerServices.ConfiguredTaskAwaitable`1_ConfiguredTaskAwaiter<System.Threading.Tasks.Task> System.Threading.SemaphoreSlim_<WaitUntilCountOrTimeoutAsync>d__31::<>u__1
	ConfiguredTaskAwaiter_tF64824CB5C3CFE2E1C4CAFE410B4CDE6831E4C78  ___U3CU3Eu__1_8;
	// System.Runtime.CompilerServices.ConfiguredTaskAwaitable`1_ConfiguredTaskAwaiter<System.Boolean> System.Threading.SemaphoreSlim_<WaitUntilCountOrTimeoutAsync>d__31::<>u__2
	ConfiguredTaskAwaiter_t286C97C0AF102C4C0BE55CE2025CC7BD1FB5C20C  ___U3CU3Eu__2_9;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CWaitUntilCountOrTimeoutAsyncU3Ed__31_t227D1F5F19C948CA8C23C80B5F19147D4AAED14F, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3Et__builder_1() { return static_cast<int32_t>(offsetof(U3CWaitUntilCountOrTimeoutAsyncU3Ed__31_t227D1F5F19C948CA8C23C80B5F19147D4AAED14F, ___U3CU3Et__builder_1)); }
	inline AsyncTaskMethodBuilder_1_tABE1DEF12F121D6FC8ABF04869ED964FF83EA9B5  get_U3CU3Et__builder_1() const { return ___U3CU3Et__builder_1; }
	inline AsyncTaskMethodBuilder_1_tABE1DEF12F121D6FC8ABF04869ED964FF83EA9B5 * get_address_of_U3CU3Et__builder_1() { return &___U3CU3Et__builder_1; }
	inline void set_U3CU3Et__builder_1(AsyncTaskMethodBuilder_1_tABE1DEF12F121D6FC8ABF04869ED964FF83EA9B5  value)
	{
		___U3CU3Et__builder_1 = value;
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___U3CU3Et__builder_1))->___m_coreState_1))->___m_stateMachine_0), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___U3CU3Et__builder_1))->___m_coreState_1))->___m_defaultContextAction_1), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___U3CU3Et__builder_1))->___m_task_2), (void*)NULL);
		#endif
	}

	inline static int32_t get_offset_of_cancellationToken_2() { return static_cast<int32_t>(offsetof(U3CWaitUntilCountOrTimeoutAsyncU3Ed__31_t227D1F5F19C948CA8C23C80B5F19147D4AAED14F, ___cancellationToken_2)); }
	inline CancellationToken_tC9D68381C9164A4BA10397257E87ADC832AF5FFD  get_cancellationToken_2() const { return ___cancellationToken_2; }
	inline CancellationToken_tC9D68381C9164A4BA10397257E87ADC832AF5FFD * get_address_of_cancellationToken_2() { return &___cancellationToken_2; }
	inline void set_cancellationToken_2(CancellationToken_tC9D68381C9164A4BA10397257E87ADC832AF5FFD  value)
	{
		___cancellationToken_2 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___cancellationToken_2))->___m_source_0), (void*)NULL);
	}

	inline static int32_t get_offset_of_asyncWaiter_3() { return static_cast<int32_t>(offsetof(U3CWaitUntilCountOrTimeoutAsyncU3Ed__31_t227D1F5F19C948CA8C23C80B5F19147D4AAED14F, ___asyncWaiter_3)); }
	inline TaskNode_tD3014A57510D018F890E6524AC62F9417E2E6C4E * get_asyncWaiter_3() const { return ___asyncWaiter_3; }
	inline TaskNode_tD3014A57510D018F890E6524AC62F9417E2E6C4E ** get_address_of_asyncWaiter_3() { return &___asyncWaiter_3; }
	inline void set_asyncWaiter_3(TaskNode_tD3014A57510D018F890E6524AC62F9417E2E6C4E * value)
	{
		___asyncWaiter_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___asyncWaiter_3), (void*)value);
	}

	inline static int32_t get_offset_of_millisecondsTimeout_4() { return static_cast<int32_t>(offsetof(U3CWaitUntilCountOrTimeoutAsyncU3Ed__31_t227D1F5F19C948CA8C23C80B5F19147D4AAED14F, ___millisecondsTimeout_4)); }
	inline int32_t get_millisecondsTimeout_4() const { return ___millisecondsTimeout_4; }
	inline int32_t* get_address_of_millisecondsTimeout_4() { return &___millisecondsTimeout_4; }
	inline void set_millisecondsTimeout_4(int32_t value)
	{
		___millisecondsTimeout_4 = value;
	}

	inline static int32_t get_offset_of_U3CctsU3E5__1_5() { return static_cast<int32_t>(offsetof(U3CWaitUntilCountOrTimeoutAsyncU3Ed__31_t227D1F5F19C948CA8C23C80B5F19147D4AAED14F, ___U3CctsU3E5__1_5)); }
	inline CancellationTokenSource_t78B989179DE23EDD36F870FFEE20A15D6D3C65B3 * get_U3CctsU3E5__1_5() const { return ___U3CctsU3E5__1_5; }
	inline CancellationTokenSource_t78B989179DE23EDD36F870FFEE20A15D6D3C65B3 ** get_address_of_U3CctsU3E5__1_5() { return &___U3CctsU3E5__1_5; }
	inline void set_U3CctsU3E5__1_5(CancellationTokenSource_t78B989179DE23EDD36F870FFEE20A15D6D3C65B3 * value)
	{
		___U3CctsU3E5__1_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CctsU3E5__1_5), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_6() { return static_cast<int32_t>(offsetof(U3CWaitUntilCountOrTimeoutAsyncU3Ed__31_t227D1F5F19C948CA8C23C80B5F19147D4AAED14F, ___U3CU3E4__this_6)); }
	inline SemaphoreSlim_t3EF85FC980AE57957BEBB6B78E81DE2E3233D385 * get_U3CU3E4__this_6() const { return ___U3CU3E4__this_6; }
	inline SemaphoreSlim_t3EF85FC980AE57957BEBB6B78E81DE2E3233D385 ** get_address_of_U3CU3E4__this_6() { return &___U3CU3E4__this_6; }
	inline void set_U3CU3E4__this_6(SemaphoreSlim_t3EF85FC980AE57957BEBB6B78E81DE2E3233D385 * value)
	{
		___U3CU3E4__this_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E4__this_6), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E7__wrap1_7() { return static_cast<int32_t>(offsetof(U3CWaitUntilCountOrTimeoutAsyncU3Ed__31_t227D1F5F19C948CA8C23C80B5F19147D4AAED14F, ___U3CU3E7__wrap1_7)); }
	inline RuntimeObject * get_U3CU3E7__wrap1_7() const { return ___U3CU3E7__wrap1_7; }
	inline RuntimeObject ** get_address_of_U3CU3E7__wrap1_7() { return &___U3CU3E7__wrap1_7; }
	inline void set_U3CU3E7__wrap1_7(RuntimeObject * value)
	{
		___U3CU3E7__wrap1_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E7__wrap1_7), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3Eu__1_8() { return static_cast<int32_t>(offsetof(U3CWaitUntilCountOrTimeoutAsyncU3Ed__31_t227D1F5F19C948CA8C23C80B5F19147D4AAED14F, ___U3CU3Eu__1_8)); }
	inline ConfiguredTaskAwaiter_tF64824CB5C3CFE2E1C4CAFE410B4CDE6831E4C78  get_U3CU3Eu__1_8() const { return ___U3CU3Eu__1_8; }
	inline ConfiguredTaskAwaiter_tF64824CB5C3CFE2E1C4CAFE410B4CDE6831E4C78 * get_address_of_U3CU3Eu__1_8() { return &___U3CU3Eu__1_8; }
	inline void set_U3CU3Eu__1_8(ConfiguredTaskAwaiter_tF64824CB5C3CFE2E1C4CAFE410B4CDE6831E4C78  value)
	{
		___U3CU3Eu__1_8 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___U3CU3Eu__1_8))->___m_task_0), (void*)NULL);
	}

	inline static int32_t get_offset_of_U3CU3Eu__2_9() { return static_cast<int32_t>(offsetof(U3CWaitUntilCountOrTimeoutAsyncU3Ed__31_t227D1F5F19C948CA8C23C80B5F19147D4AAED14F, ___U3CU3Eu__2_9)); }
	inline ConfiguredTaskAwaiter_t286C97C0AF102C4C0BE55CE2025CC7BD1FB5C20C  get_U3CU3Eu__2_9() const { return ___U3CU3Eu__2_9; }
	inline ConfiguredTaskAwaiter_t286C97C0AF102C4C0BE55CE2025CC7BD1FB5C20C * get_address_of_U3CU3Eu__2_9() { return &___U3CU3Eu__2_9; }
	inline void set_U3CU3Eu__2_9(ConfiguredTaskAwaiter_t286C97C0AF102C4C0BE55CE2025CC7BD1FB5C20C  value)
	{
		___U3CU3Eu__2_9 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___U3CU3Eu__2_9))->___m_task_0), (void*)NULL);
	}
};


// System.Threading.Tasks.Task`1<System.Boolean>
struct  Task_1_t9C1FE9F18F52F3409B9E970FA38801A443AE7849  : public Task_t804B25CFE3FC13AAEE16C8FA3BF52513F2A8DB60
{
public:
	// TResult System.Threading.Tasks.Task`1::m_result
	bool ___m_result_22;

public:
	inline static int32_t get_offset_of_m_result_22() { return static_cast<int32_t>(offsetof(Task_1_t9C1FE9F18F52F3409B9E970FA38801A443AE7849, ___m_result_22)); }
	inline bool get_m_result_22() const { return ___m_result_22; }
	inline bool* get_address_of_m_result_22() { return &___m_result_22; }
	inline void set_m_result_22(bool value)
	{
		___m_result_22 = value;
	}
};

struct Task_1_t9C1FE9F18F52F3409B9E970FA38801A443AE7849_StaticFields
{
public:
	// System.Threading.Tasks.TaskFactory`1<TResult> System.Threading.Tasks.Task`1::s_Factory
	TaskFactory_1_t069438A73348A2B1B34A2C68E0478EE107ECCFC7 * ___s_Factory_23;
	// System.Func`2<System.Threading.Tasks.Task`1<System.Threading.Tasks.Task>,System.Threading.Tasks.Task`1<TResult>> System.Threading.Tasks.Task`1::TaskWhenAnyCast
	Func_2_t24DC43D57AB022882FE433E3B16B6D7E4BD14BB4 * ___TaskWhenAnyCast_24;

public:
	inline static int32_t get_offset_of_s_Factory_23() { return static_cast<int32_t>(offsetof(Task_1_t9C1FE9F18F52F3409B9E970FA38801A443AE7849_StaticFields, ___s_Factory_23)); }
	inline TaskFactory_1_t069438A73348A2B1B34A2C68E0478EE107ECCFC7 * get_s_Factory_23() const { return ___s_Factory_23; }
	inline TaskFactory_1_t069438A73348A2B1B34A2C68E0478EE107ECCFC7 ** get_address_of_s_Factory_23() { return &___s_Factory_23; }
	inline void set_s_Factory_23(TaskFactory_1_t069438A73348A2B1B34A2C68E0478EE107ECCFC7 * value)
	{
		___s_Factory_23 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_Factory_23), (void*)value);
	}

	inline static int32_t get_offset_of_TaskWhenAnyCast_24() { return static_cast<int32_t>(offsetof(Task_1_t9C1FE9F18F52F3409B9E970FA38801A443AE7849_StaticFields, ___TaskWhenAnyCast_24)); }
	inline Func_2_t24DC43D57AB022882FE433E3B16B6D7E4BD14BB4 * get_TaskWhenAnyCast_24() const { return ___TaskWhenAnyCast_24; }
	inline Func_2_t24DC43D57AB022882FE433E3B16B6D7E4BD14BB4 ** get_address_of_TaskWhenAnyCast_24() { return &___TaskWhenAnyCast_24; }
	inline void set_TaskWhenAnyCast_24(Func_2_t24DC43D57AB022882FE433E3B16B6D7E4BD14BB4 * value)
	{
		___TaskWhenAnyCast_24 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___TaskWhenAnyCast_24), (void*)value);
	}
};


// System.Threading.Tasks.Task`1<System.Object>
struct  Task_1_tC1805497876E88B78A2B0CB81C6409E0B381AC17  : public Task_t804B25CFE3FC13AAEE16C8FA3BF52513F2A8DB60
{
public:
	// TResult System.Threading.Tasks.Task`1::m_result
	RuntimeObject * ___m_result_22;

public:
	inline static int32_t get_offset_of_m_result_22() { return static_cast<int32_t>(offsetof(Task_1_tC1805497876E88B78A2B0CB81C6409E0B381AC17, ___m_result_22)); }
	inline RuntimeObject * get_m_result_22() const { return ___m_result_22; }
	inline RuntimeObject ** get_address_of_m_result_22() { return &___m_result_22; }
	inline void set_m_result_22(RuntimeObject * value)
	{
		___m_result_22 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_result_22), (void*)value);
	}
};

struct Task_1_tC1805497876E88B78A2B0CB81C6409E0B381AC17_StaticFields
{
public:
	// System.Threading.Tasks.TaskFactory`1<TResult> System.Threading.Tasks.Task`1::s_Factory
	TaskFactory_1_t16A95DD17BBA3D00F0A85C5077BB248421EF3A55 * ___s_Factory_23;
	// System.Func`2<System.Threading.Tasks.Task`1<System.Threading.Tasks.Task>,System.Threading.Tasks.Task`1<TResult>> System.Threading.Tasks.Task`1::TaskWhenAnyCast
	Func_2_t44F36790F9746FCE5ABFDE6205B6020B2578F6DD * ___TaskWhenAnyCast_24;

public:
	inline static int32_t get_offset_of_s_Factory_23() { return static_cast<int32_t>(offsetof(Task_1_tC1805497876E88B78A2B0CB81C6409E0B381AC17_StaticFields, ___s_Factory_23)); }
	inline TaskFactory_1_t16A95DD17BBA3D00F0A85C5077BB248421EF3A55 * get_s_Factory_23() const { return ___s_Factory_23; }
	inline TaskFactory_1_t16A95DD17BBA3D00F0A85C5077BB248421EF3A55 ** get_address_of_s_Factory_23() { return &___s_Factory_23; }
	inline void set_s_Factory_23(TaskFactory_1_t16A95DD17BBA3D00F0A85C5077BB248421EF3A55 * value)
	{
		___s_Factory_23 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_Factory_23), (void*)value);
	}

	inline static int32_t get_offset_of_TaskWhenAnyCast_24() { return static_cast<int32_t>(offsetof(Task_1_tC1805497876E88B78A2B0CB81C6409E0B381AC17_StaticFields, ___TaskWhenAnyCast_24)); }
	inline Func_2_t44F36790F9746FCE5ABFDE6205B6020B2578F6DD * get_TaskWhenAnyCast_24() const { return ___TaskWhenAnyCast_24; }
	inline Func_2_t44F36790F9746FCE5ABFDE6205B6020B2578F6DD ** get_address_of_TaskWhenAnyCast_24() { return &___TaskWhenAnyCast_24; }
	inline void set_TaskWhenAnyCast_24(Func_2_t44F36790F9746FCE5ABFDE6205B6020B2578F6DD * value)
	{
		___TaskWhenAnyCast_24 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___TaskWhenAnyCast_24), (void*)value);
	}
};


// Unity.Collections.NativeArray`1<Obi.Aabb>
struct  NativeArray_1_t43E7EA4C279410A42CF9A0F46F22F5A084752842 
{
public:
	// System.Void* Unity.Collections.NativeArray`1::m_Buffer
	void* ___m_Buffer_0;
	// System.Int32 Unity.Collections.NativeArray`1::m_Length
	int32_t ___m_Length_1;
	// Unity.Collections.Allocator Unity.Collections.NativeArray`1::m_AllocatorLabel
	int32_t ___m_AllocatorLabel_2;

public:
	inline static int32_t get_offset_of_m_Buffer_0() { return static_cast<int32_t>(offsetof(NativeArray_1_t43E7EA4C279410A42CF9A0F46F22F5A084752842, ___m_Buffer_0)); }
	inline void* get_m_Buffer_0() const { return ___m_Buffer_0; }
	inline void** get_address_of_m_Buffer_0() { return &___m_Buffer_0; }
	inline void set_m_Buffer_0(void* value)
	{
		___m_Buffer_0 = value;
	}

	inline static int32_t get_offset_of_m_Length_1() { return static_cast<int32_t>(offsetof(NativeArray_1_t43E7EA4C279410A42CF9A0F46F22F5A084752842, ___m_Length_1)); }
	inline int32_t get_m_Length_1() const { return ___m_Length_1; }
	inline int32_t* get_address_of_m_Length_1() { return &___m_Length_1; }
	inline void set_m_Length_1(int32_t value)
	{
		___m_Length_1 = value;
	}

	inline static int32_t get_offset_of_m_AllocatorLabel_2() { return static_cast<int32_t>(offsetof(NativeArray_1_t43E7EA4C279410A42CF9A0F46F22F5A084752842, ___m_AllocatorLabel_2)); }
	inline int32_t get_m_AllocatorLabel_2() const { return ___m_AllocatorLabel_2; }
	inline int32_t* get_address_of_m_AllocatorLabel_2() { return &___m_AllocatorLabel_2; }
	inline void set_m_AllocatorLabel_2(int32_t value)
	{
		___m_AllocatorLabel_2 = value;
	}
};


// Unity.Collections.NativeArray`1<Obi.AffineTransform>
struct  NativeArray_1_tA435936F3C77B97EDF0C46997DD48EF589966961 
{
public:
	// System.Void* Unity.Collections.NativeArray`1::m_Buffer
	void* ___m_Buffer_0;
	// System.Int32 Unity.Collections.NativeArray`1::m_Length
	int32_t ___m_Length_1;
	// Unity.Collections.Allocator Unity.Collections.NativeArray`1::m_AllocatorLabel
	int32_t ___m_AllocatorLabel_2;

public:
	inline static int32_t get_offset_of_m_Buffer_0() { return static_cast<int32_t>(offsetof(NativeArray_1_tA435936F3C77B97EDF0C46997DD48EF589966961, ___m_Buffer_0)); }
	inline void* get_m_Buffer_0() const { return ___m_Buffer_0; }
	inline void** get_address_of_m_Buffer_0() { return &___m_Buffer_0; }
	inline void set_m_Buffer_0(void* value)
	{
		___m_Buffer_0 = value;
	}

	inline static int32_t get_offset_of_m_Length_1() { return static_cast<int32_t>(offsetof(NativeArray_1_tA435936F3C77B97EDF0C46997DD48EF589966961, ___m_Length_1)); }
	inline int32_t get_m_Length_1() const { return ___m_Length_1; }
	inline int32_t* get_address_of_m_Length_1() { return &___m_Length_1; }
	inline void set_m_Length_1(int32_t value)
	{
		___m_Length_1 = value;
	}

	inline static int32_t get_offset_of_m_AllocatorLabel_2() { return static_cast<int32_t>(offsetof(NativeArray_1_tA435936F3C77B97EDF0C46997DD48EF589966961, ___m_AllocatorLabel_2)); }
	inline int32_t get_m_AllocatorLabel_2() const { return ___m_AllocatorLabel_2; }
	inline int32_t* get_address_of_m_AllocatorLabel_2() { return &___m_AllocatorLabel_2; }
	inline void set_m_AllocatorLabel_2(int32_t value)
	{
		___m_AllocatorLabel_2 = value;
	}
};


// Unity.Collections.NativeArray`1<Obi.BIHNode>
struct  NativeArray_1_t76B0EA4D6D2405DF9C6DE308D69483AF146C1D3C 
{
public:
	// System.Void* Unity.Collections.NativeArray`1::m_Buffer
	void* ___m_Buffer_0;
	// System.Int32 Unity.Collections.NativeArray`1::m_Length
	int32_t ___m_Length_1;
	// Unity.Collections.Allocator Unity.Collections.NativeArray`1::m_AllocatorLabel
	int32_t ___m_AllocatorLabel_2;

public:
	inline static int32_t get_offset_of_m_Buffer_0() { return static_cast<int32_t>(offsetof(NativeArray_1_t76B0EA4D6D2405DF9C6DE308D69483AF146C1D3C, ___m_Buffer_0)); }
	inline void* get_m_Buffer_0() const { return ___m_Buffer_0; }
	inline void** get_address_of_m_Buffer_0() { return &___m_Buffer_0; }
	inline void set_m_Buffer_0(void* value)
	{
		___m_Buffer_0 = value;
	}

	inline static int32_t get_offset_of_m_Length_1() { return static_cast<int32_t>(offsetof(NativeArray_1_t76B0EA4D6D2405DF9C6DE308D69483AF146C1D3C, ___m_Length_1)); }
	inline int32_t get_m_Length_1() const { return ___m_Length_1; }
	inline int32_t* get_address_of_m_Length_1() { return &___m_Length_1; }
	inline void set_m_Length_1(int32_t value)
	{
		___m_Length_1 = value;
	}

	inline static int32_t get_offset_of_m_AllocatorLabel_2() { return static_cast<int32_t>(offsetof(NativeArray_1_t76B0EA4D6D2405DF9C6DE308D69483AF146C1D3C, ___m_AllocatorLabel_2)); }
	inline int32_t get_m_AllocatorLabel_2() const { return ___m_AllocatorLabel_2; }
	inline int32_t* get_address_of_m_AllocatorLabel_2() { return &___m_AllocatorLabel_2; }
	inline void set_m_AllocatorLabel_2(int32_t value)
	{
		___m_AllocatorLabel_2 = value;
	}
};


// Unity.Collections.NativeArray`1<Obi.CellSpan>
struct  NativeArray_1_t1414BC68B8A64DA9A80F71AE9C749A11FEBEDEC4 
{
public:
	// System.Void* Unity.Collections.NativeArray`1::m_Buffer
	void* ___m_Buffer_0;
	// System.Int32 Unity.Collections.NativeArray`1::m_Length
	int32_t ___m_Length_1;
	// Unity.Collections.Allocator Unity.Collections.NativeArray`1::m_AllocatorLabel
	int32_t ___m_AllocatorLabel_2;

public:
	inline static int32_t get_offset_of_m_Buffer_0() { return static_cast<int32_t>(offsetof(NativeArray_1_t1414BC68B8A64DA9A80F71AE9C749A11FEBEDEC4, ___m_Buffer_0)); }
	inline void* get_m_Buffer_0() const { return ___m_Buffer_0; }
	inline void** get_address_of_m_Buffer_0() { return &___m_Buffer_0; }
	inline void set_m_Buffer_0(void* value)
	{
		___m_Buffer_0 = value;
	}

	inline static int32_t get_offset_of_m_Length_1() { return static_cast<int32_t>(offsetof(NativeArray_1_t1414BC68B8A64DA9A80F71AE9C749A11FEBEDEC4, ___m_Length_1)); }
	inline int32_t get_m_Length_1() const { return ___m_Length_1; }
	inline int32_t* get_address_of_m_Length_1() { return &___m_Length_1; }
	inline void set_m_Length_1(int32_t value)
	{
		___m_Length_1 = value;
	}

	inline static int32_t get_offset_of_m_AllocatorLabel_2() { return static_cast<int32_t>(offsetof(NativeArray_1_t1414BC68B8A64DA9A80F71AE9C749A11FEBEDEC4, ___m_AllocatorLabel_2)); }
	inline int32_t get_m_AllocatorLabel_2() const { return ___m_AllocatorLabel_2; }
	inline int32_t* get_address_of_m_AllocatorLabel_2() { return &___m_AllocatorLabel_2; }
	inline void set_m_AllocatorLabel_2(int32_t value)
	{
		___m_AllocatorLabel_2 = value;
	}
};


// Unity.Collections.NativeArray`1<Obi.ColliderRigidbody>
struct  NativeArray_1_t572B8FA0970987F9DED50CD9493AA60298DD0644 
{
public:
	// System.Void* Unity.Collections.NativeArray`1::m_Buffer
	void* ___m_Buffer_0;
	// System.Int32 Unity.Collections.NativeArray`1::m_Length
	int32_t ___m_Length_1;
	// Unity.Collections.Allocator Unity.Collections.NativeArray`1::m_AllocatorLabel
	int32_t ___m_AllocatorLabel_2;

public:
	inline static int32_t get_offset_of_m_Buffer_0() { return static_cast<int32_t>(offsetof(NativeArray_1_t572B8FA0970987F9DED50CD9493AA60298DD0644, ___m_Buffer_0)); }
	inline void* get_m_Buffer_0() const { return ___m_Buffer_0; }
	inline void** get_address_of_m_Buffer_0() { return &___m_Buffer_0; }
	inline void set_m_Buffer_0(void* value)
	{
		___m_Buffer_0 = value;
	}

	inline static int32_t get_offset_of_m_Length_1() { return static_cast<int32_t>(offsetof(NativeArray_1_t572B8FA0970987F9DED50CD9493AA60298DD0644, ___m_Length_1)); }
	inline int32_t get_m_Length_1() const { return ___m_Length_1; }
	inline int32_t* get_address_of_m_Length_1() { return &___m_Length_1; }
	inline void set_m_Length_1(int32_t value)
	{
		___m_Length_1 = value;
	}

	inline static int32_t get_offset_of_m_AllocatorLabel_2() { return static_cast<int32_t>(offsetof(NativeArray_1_t572B8FA0970987F9DED50CD9493AA60298DD0644, ___m_AllocatorLabel_2)); }
	inline int32_t get_m_AllocatorLabel_2() const { return ___m_AllocatorLabel_2; }
	inline int32_t* get_address_of_m_AllocatorLabel_2() { return &___m_AllocatorLabel_2; }
	inline void set_m_AllocatorLabel_2(int32_t value)
	{
		___m_AllocatorLabel_2 = value;
	}
};


// Unity.Collections.NativeArray`1<Obi.ColliderShape>
struct  NativeArray_1_t10D0F10FEFFE209A84987F5B88F042A28DAEFA03 
{
public:
	// System.Void* Unity.Collections.NativeArray`1::m_Buffer
	void* ___m_Buffer_0;
	// System.Int32 Unity.Collections.NativeArray`1::m_Length
	int32_t ___m_Length_1;
	// Unity.Collections.Allocator Unity.Collections.NativeArray`1::m_AllocatorLabel
	int32_t ___m_AllocatorLabel_2;

public:
	inline static int32_t get_offset_of_m_Buffer_0() { return static_cast<int32_t>(offsetof(NativeArray_1_t10D0F10FEFFE209A84987F5B88F042A28DAEFA03, ___m_Buffer_0)); }
	inline void* get_m_Buffer_0() const { return ___m_Buffer_0; }
	inline void** get_address_of_m_Buffer_0() { return &___m_Buffer_0; }
	inline void set_m_Buffer_0(void* value)
	{
		___m_Buffer_0 = value;
	}

	inline static int32_t get_offset_of_m_Length_1() { return static_cast<int32_t>(offsetof(NativeArray_1_t10D0F10FEFFE209A84987F5B88F042A28DAEFA03, ___m_Length_1)); }
	inline int32_t get_m_Length_1() const { return ___m_Length_1; }
	inline int32_t* get_address_of_m_Length_1() { return &___m_Length_1; }
	inline void set_m_Length_1(int32_t value)
	{
		___m_Length_1 = value;
	}

	inline static int32_t get_offset_of_m_AllocatorLabel_2() { return static_cast<int32_t>(offsetof(NativeArray_1_t10D0F10FEFFE209A84987F5B88F042A28DAEFA03, ___m_AllocatorLabel_2)); }
	inline int32_t get_m_AllocatorLabel_2() const { return ___m_AllocatorLabel_2; }
	inline int32_t* get_address_of_m_AllocatorLabel_2() { return &___m_AllocatorLabel_2; }
	inline void set_m_AllocatorLabel_2(int32_t value)
	{
		___m_AllocatorLabel_2 = value;
	}
};


// Unity.Collections.NativeArray`1<Obi.CollisionMaterial>
struct  NativeArray_1_t9C2DA1A93847BC1D3620655C78E87ACDA7CEC0BB 
{
public:
	// System.Void* Unity.Collections.NativeArray`1::m_Buffer
	void* ___m_Buffer_0;
	// System.Int32 Unity.Collections.NativeArray`1::m_Length
	int32_t ___m_Length_1;
	// Unity.Collections.Allocator Unity.Collections.NativeArray`1::m_AllocatorLabel
	int32_t ___m_AllocatorLabel_2;

public:
	inline static int32_t get_offset_of_m_Buffer_0() { return static_cast<int32_t>(offsetof(NativeArray_1_t9C2DA1A93847BC1D3620655C78E87ACDA7CEC0BB, ___m_Buffer_0)); }
	inline void* get_m_Buffer_0() const { return ___m_Buffer_0; }
	inline void** get_address_of_m_Buffer_0() { return &___m_Buffer_0; }
	inline void set_m_Buffer_0(void* value)
	{
		___m_Buffer_0 = value;
	}

	inline static int32_t get_offset_of_m_Length_1() { return static_cast<int32_t>(offsetof(NativeArray_1_t9C2DA1A93847BC1D3620655C78E87ACDA7CEC0BB, ___m_Length_1)); }
	inline int32_t get_m_Length_1() const { return ___m_Length_1; }
	inline int32_t* get_address_of_m_Length_1() { return &___m_Length_1; }
	inline void set_m_Length_1(int32_t value)
	{
		___m_Length_1 = value;
	}

	inline static int32_t get_offset_of_m_AllocatorLabel_2() { return static_cast<int32_t>(offsetof(NativeArray_1_t9C2DA1A93847BC1D3620655C78E87ACDA7CEC0BB, ___m_AllocatorLabel_2)); }
	inline int32_t get_m_AllocatorLabel_2() const { return ___m_AllocatorLabel_2; }
	inline int32_t* get_address_of_m_AllocatorLabel_2() { return &___m_AllocatorLabel_2; }
	inline void set_m_AllocatorLabel_2(int32_t value)
	{
		___m_AllocatorLabel_2 = value;
	}
};


// Unity.Collections.NativeArray`1<Obi.DFNode>
struct  NativeArray_1_tFA6A4A2D858B86A96EEA4C98F7A00D1C3C1B7045 
{
public:
	// System.Void* Unity.Collections.NativeArray`1::m_Buffer
	void* ___m_Buffer_0;
	// System.Int32 Unity.Collections.NativeArray`1::m_Length
	int32_t ___m_Length_1;
	// Unity.Collections.Allocator Unity.Collections.NativeArray`1::m_AllocatorLabel
	int32_t ___m_AllocatorLabel_2;

public:
	inline static int32_t get_offset_of_m_Buffer_0() { return static_cast<int32_t>(offsetof(NativeArray_1_tFA6A4A2D858B86A96EEA4C98F7A00D1C3C1B7045, ___m_Buffer_0)); }
	inline void* get_m_Buffer_0() const { return ___m_Buffer_0; }
	inline void** get_address_of_m_Buffer_0() { return &___m_Buffer_0; }
	inline void set_m_Buffer_0(void* value)
	{
		___m_Buffer_0 = value;
	}

	inline static int32_t get_offset_of_m_Length_1() { return static_cast<int32_t>(offsetof(NativeArray_1_tFA6A4A2D858B86A96EEA4C98F7A00D1C3C1B7045, ___m_Length_1)); }
	inline int32_t get_m_Length_1() const { return ___m_Length_1; }
	inline int32_t* get_address_of_m_Length_1() { return &___m_Length_1; }
	inline void set_m_Length_1(int32_t value)
	{
		___m_Length_1 = value;
	}

	inline static int32_t get_offset_of_m_AllocatorLabel_2() { return static_cast<int32_t>(offsetof(NativeArray_1_tFA6A4A2D858B86A96EEA4C98F7A00D1C3C1B7045, ___m_AllocatorLabel_2)); }
	inline int32_t get_m_AllocatorLabel_2() const { return ___m_AllocatorLabel_2; }
	inline int32_t* get_address_of_m_AllocatorLabel_2() { return &___m_AllocatorLabel_2; }
	inline void set_m_AllocatorLabel_2(int32_t value)
	{
		___m_AllocatorLabel_2 = value;
	}
};


// Unity.Collections.NativeArray`1<Obi.DistanceFieldHeader>
struct  NativeArray_1_t1F82401C884356A2CEC88A9DC6CA01BF4295C31A 
{
public:
	// System.Void* Unity.Collections.NativeArray`1::m_Buffer
	void* ___m_Buffer_0;
	// System.Int32 Unity.Collections.NativeArray`1::m_Length
	int32_t ___m_Length_1;
	// Unity.Collections.Allocator Unity.Collections.NativeArray`1::m_AllocatorLabel
	int32_t ___m_AllocatorLabel_2;

public:
	inline static int32_t get_offset_of_m_Buffer_0() { return static_cast<int32_t>(offsetof(NativeArray_1_t1F82401C884356A2CEC88A9DC6CA01BF4295C31A, ___m_Buffer_0)); }
	inline void* get_m_Buffer_0() const { return ___m_Buffer_0; }
	inline void** get_address_of_m_Buffer_0() { return &___m_Buffer_0; }
	inline void set_m_Buffer_0(void* value)
	{
		___m_Buffer_0 = value;
	}

	inline static int32_t get_offset_of_m_Length_1() { return static_cast<int32_t>(offsetof(NativeArray_1_t1F82401C884356A2CEC88A9DC6CA01BF4295C31A, ___m_Length_1)); }
	inline int32_t get_m_Length_1() const { return ___m_Length_1; }
	inline int32_t* get_address_of_m_Length_1() { return &___m_Length_1; }
	inline void set_m_Length_1(int32_t value)
	{
		___m_Length_1 = value;
	}

	inline static int32_t get_offset_of_m_AllocatorLabel_2() { return static_cast<int32_t>(offsetof(NativeArray_1_t1F82401C884356A2CEC88A9DC6CA01BF4295C31A, ___m_AllocatorLabel_2)); }
	inline int32_t get_m_AllocatorLabel_2() const { return ___m_AllocatorLabel_2; }
	inline int32_t* get_address_of_m_AllocatorLabel_2() { return &___m_AllocatorLabel_2; }
	inline void set_m_AllocatorLabel_2(int32_t value)
	{
		___m_AllocatorLabel_2 = value;
	}
};


// Unity.Collections.NativeArray`1<Obi.Edge>
struct  NativeArray_1_t7C51D2F91F4BF0F8A2B61735BA98C900AE793065 
{
public:
	// System.Void* Unity.Collections.NativeArray`1::m_Buffer
	void* ___m_Buffer_0;
	// System.Int32 Unity.Collections.NativeArray`1::m_Length
	int32_t ___m_Length_1;
	// Unity.Collections.Allocator Unity.Collections.NativeArray`1::m_AllocatorLabel
	int32_t ___m_AllocatorLabel_2;

public:
	inline static int32_t get_offset_of_m_Buffer_0() { return static_cast<int32_t>(offsetof(NativeArray_1_t7C51D2F91F4BF0F8A2B61735BA98C900AE793065, ___m_Buffer_0)); }
	inline void* get_m_Buffer_0() const { return ___m_Buffer_0; }
	inline void** get_address_of_m_Buffer_0() { return &___m_Buffer_0; }
	inline void set_m_Buffer_0(void* value)
	{
		___m_Buffer_0 = value;
	}

	inline static int32_t get_offset_of_m_Length_1() { return static_cast<int32_t>(offsetof(NativeArray_1_t7C51D2F91F4BF0F8A2B61735BA98C900AE793065, ___m_Length_1)); }
	inline int32_t get_m_Length_1() const { return ___m_Length_1; }
	inline int32_t* get_address_of_m_Length_1() { return &___m_Length_1; }
	inline void set_m_Length_1(int32_t value)
	{
		___m_Length_1 = value;
	}

	inline static int32_t get_offset_of_m_AllocatorLabel_2() { return static_cast<int32_t>(offsetof(NativeArray_1_t7C51D2F91F4BF0F8A2B61735BA98C900AE793065, ___m_AllocatorLabel_2)); }
	inline int32_t get_m_AllocatorLabel_2() const { return ___m_AllocatorLabel_2; }
	inline int32_t* get_address_of_m_AllocatorLabel_2() { return &___m_AllocatorLabel_2; }
	inline void set_m_AllocatorLabel_2(int32_t value)
	{
		___m_AllocatorLabel_2 = value;
	}
};


// Unity.Collections.NativeArray`1<Obi.EdgeMeshHeader>
struct  NativeArray_1_tAFBEB18DC14E5B6EF8F550019E04E75AC3289B6A 
{
public:
	// System.Void* Unity.Collections.NativeArray`1::m_Buffer
	void* ___m_Buffer_0;
	// System.Int32 Unity.Collections.NativeArray`1::m_Length
	int32_t ___m_Length_1;
	// Unity.Collections.Allocator Unity.Collections.NativeArray`1::m_AllocatorLabel
	int32_t ___m_AllocatorLabel_2;

public:
	inline static int32_t get_offset_of_m_Buffer_0() { return static_cast<int32_t>(offsetof(NativeArray_1_tAFBEB18DC14E5B6EF8F550019E04E75AC3289B6A, ___m_Buffer_0)); }
	inline void* get_m_Buffer_0() const { return ___m_Buffer_0; }
	inline void** get_address_of_m_Buffer_0() { return &___m_Buffer_0; }
	inline void set_m_Buffer_0(void* value)
	{
		___m_Buffer_0 = value;
	}

	inline static int32_t get_offset_of_m_Length_1() { return static_cast<int32_t>(offsetof(NativeArray_1_tAFBEB18DC14E5B6EF8F550019E04E75AC3289B6A, ___m_Length_1)); }
	inline int32_t get_m_Length_1() const { return ___m_Length_1; }
	inline int32_t* get_address_of_m_Length_1() { return &___m_Length_1; }
	inline void set_m_Length_1(int32_t value)
	{
		___m_Length_1 = value;
	}

	inline static int32_t get_offset_of_m_AllocatorLabel_2() { return static_cast<int32_t>(offsetof(NativeArray_1_tAFBEB18DC14E5B6EF8F550019E04E75AC3289B6A, ___m_AllocatorLabel_2)); }
	inline int32_t get_m_AllocatorLabel_2() const { return ___m_AllocatorLabel_2; }
	inline int32_t* get_address_of_m_AllocatorLabel_2() { return &___m_AllocatorLabel_2; }
	inline void set_m_AllocatorLabel_2(int32_t value)
	{
		___m_AllocatorLabel_2 = value;
	}
};


// Unity.Collections.NativeArray`1<Obi.HeightFieldHeader>
struct  NativeArray_1_tB28C2BA1925952F16FE6E1D099581CE6CE963139 
{
public:
	// System.Void* Unity.Collections.NativeArray`1::m_Buffer
	void* ___m_Buffer_0;
	// System.Int32 Unity.Collections.NativeArray`1::m_Length
	int32_t ___m_Length_1;
	// Unity.Collections.Allocator Unity.Collections.NativeArray`1::m_AllocatorLabel
	int32_t ___m_AllocatorLabel_2;

public:
	inline static int32_t get_offset_of_m_Buffer_0() { return static_cast<int32_t>(offsetof(NativeArray_1_tB28C2BA1925952F16FE6E1D099581CE6CE963139, ___m_Buffer_0)); }
	inline void* get_m_Buffer_0() const { return ___m_Buffer_0; }
	inline void** get_address_of_m_Buffer_0() { return &___m_Buffer_0; }
	inline void set_m_Buffer_0(void* value)
	{
		___m_Buffer_0 = value;
	}

	inline static int32_t get_offset_of_m_Length_1() { return static_cast<int32_t>(offsetof(NativeArray_1_tB28C2BA1925952F16FE6E1D099581CE6CE963139, ___m_Length_1)); }
	inline int32_t get_m_Length_1() const { return ___m_Length_1; }
	inline int32_t* get_address_of_m_Length_1() { return &___m_Length_1; }
	inline void set_m_Length_1(int32_t value)
	{
		___m_Length_1 = value;
	}

	inline static int32_t get_offset_of_m_AllocatorLabel_2() { return static_cast<int32_t>(offsetof(NativeArray_1_tB28C2BA1925952F16FE6E1D099581CE6CE963139, ___m_AllocatorLabel_2)); }
	inline int32_t get_m_AllocatorLabel_2() const { return ___m_AllocatorLabel_2; }
	inline int32_t* get_address_of_m_AllocatorLabel_2() { return &___m_AllocatorLabel_2; }
	inline void set_m_AllocatorLabel_2(int32_t value)
	{
		___m_AllocatorLabel_2 = value;
	}
};


// Unity.Collections.NativeArray`1<Obi.QueryResult>
struct  NativeArray_1_t95FE546DF579DC0687965D420C82508F2B433411 
{
public:
	// System.Void* Unity.Collections.NativeArray`1::m_Buffer
	void* ___m_Buffer_0;
	// System.Int32 Unity.Collections.NativeArray`1::m_Length
	int32_t ___m_Length_1;
	// Unity.Collections.Allocator Unity.Collections.NativeArray`1::m_AllocatorLabel
	int32_t ___m_AllocatorLabel_2;

public:
	inline static int32_t get_offset_of_m_Buffer_0() { return static_cast<int32_t>(offsetof(NativeArray_1_t95FE546DF579DC0687965D420C82508F2B433411, ___m_Buffer_0)); }
	inline void* get_m_Buffer_0() const { return ___m_Buffer_0; }
	inline void** get_address_of_m_Buffer_0() { return &___m_Buffer_0; }
	inline void set_m_Buffer_0(void* value)
	{
		___m_Buffer_0 = value;
	}

	inline static int32_t get_offset_of_m_Length_1() { return static_cast<int32_t>(offsetof(NativeArray_1_t95FE546DF579DC0687965D420C82508F2B433411, ___m_Length_1)); }
	inline int32_t get_m_Length_1() const { return ___m_Length_1; }
	inline int32_t* get_address_of_m_Length_1() { return &___m_Length_1; }
	inline void set_m_Length_1(int32_t value)
	{
		___m_Length_1 = value;
	}

	inline static int32_t get_offset_of_m_AllocatorLabel_2() { return static_cast<int32_t>(offsetof(NativeArray_1_t95FE546DF579DC0687965D420C82508F2B433411, ___m_AllocatorLabel_2)); }
	inline int32_t get_m_AllocatorLabel_2() const { return ___m_AllocatorLabel_2; }
	inline int32_t* get_address_of_m_AllocatorLabel_2() { return &___m_AllocatorLabel_2; }
	inline void set_m_AllocatorLabel_2(int32_t value)
	{
		___m_AllocatorLabel_2 = value;
	}
};


// Unity.Collections.NativeArray`1<Obi.QueryShape>
struct  NativeArray_1_t17470C60D1D175C02D1B8DE9E928D177760EB8F3 
{
public:
	// System.Void* Unity.Collections.NativeArray`1::m_Buffer
	void* ___m_Buffer_0;
	// System.Int32 Unity.Collections.NativeArray`1::m_Length
	int32_t ___m_Length_1;
	// Unity.Collections.Allocator Unity.Collections.NativeArray`1::m_AllocatorLabel
	int32_t ___m_AllocatorLabel_2;

public:
	inline static int32_t get_offset_of_m_Buffer_0() { return static_cast<int32_t>(offsetof(NativeArray_1_t17470C60D1D175C02D1B8DE9E928D177760EB8F3, ___m_Buffer_0)); }
	inline void* get_m_Buffer_0() const { return ___m_Buffer_0; }
	inline void** get_address_of_m_Buffer_0() { return &___m_Buffer_0; }
	inline void set_m_Buffer_0(void* value)
	{
		___m_Buffer_0 = value;
	}

	inline static int32_t get_offset_of_m_Length_1() { return static_cast<int32_t>(offsetof(NativeArray_1_t17470C60D1D175C02D1B8DE9E928D177760EB8F3, ___m_Length_1)); }
	inline int32_t get_m_Length_1() const { return ___m_Length_1; }
	inline int32_t* get_address_of_m_Length_1() { return &___m_Length_1; }
	inline void set_m_Length_1(int32_t value)
	{
		___m_Length_1 = value;
	}

	inline static int32_t get_offset_of_m_AllocatorLabel_2() { return static_cast<int32_t>(offsetof(NativeArray_1_t17470C60D1D175C02D1B8DE9E928D177760EB8F3, ___m_AllocatorLabel_2)); }
	inline int32_t get_m_AllocatorLabel_2() const { return ___m_AllocatorLabel_2; }
	inline int32_t* get_address_of_m_AllocatorLabel_2() { return &___m_AllocatorLabel_2; }
	inline void set_m_AllocatorLabel_2(int32_t value)
	{
		___m_AllocatorLabel_2 = value;
	}
};


// Unity.Collections.NativeArray`1<Obi.Triangle>
struct  NativeArray_1_t6611EE86470DC9133E759336BB08C0018D2AACA1 
{
public:
	// System.Void* Unity.Collections.NativeArray`1::m_Buffer
	void* ___m_Buffer_0;
	// System.Int32 Unity.Collections.NativeArray`1::m_Length
	int32_t ___m_Length_1;
	// Unity.Collections.Allocator Unity.Collections.NativeArray`1::m_AllocatorLabel
	int32_t ___m_AllocatorLabel_2;

public:
	inline static int32_t get_offset_of_m_Buffer_0() { return static_cast<int32_t>(offsetof(NativeArray_1_t6611EE86470DC9133E759336BB08C0018D2AACA1, ___m_Buffer_0)); }
	inline void* get_m_Buffer_0() const { return ___m_Buffer_0; }
	inline void** get_address_of_m_Buffer_0() { return &___m_Buffer_0; }
	inline void set_m_Buffer_0(void* value)
	{
		___m_Buffer_0 = value;
	}

	inline static int32_t get_offset_of_m_Length_1() { return static_cast<int32_t>(offsetof(NativeArray_1_t6611EE86470DC9133E759336BB08C0018D2AACA1, ___m_Length_1)); }
	inline int32_t get_m_Length_1() const { return ___m_Length_1; }
	inline int32_t* get_address_of_m_Length_1() { return &___m_Length_1; }
	inline void set_m_Length_1(int32_t value)
	{
		___m_Length_1 = value;
	}

	inline static int32_t get_offset_of_m_AllocatorLabel_2() { return static_cast<int32_t>(offsetof(NativeArray_1_t6611EE86470DC9133E759336BB08C0018D2AACA1, ___m_AllocatorLabel_2)); }
	inline int32_t get_m_AllocatorLabel_2() const { return ___m_AllocatorLabel_2; }
	inline int32_t* get_address_of_m_AllocatorLabel_2() { return &___m_AllocatorLabel_2; }
	inline void set_m_AllocatorLabel_2(int32_t value)
	{
		___m_AllocatorLabel_2 = value;
	}
};


// Unity.Collections.NativeArray`1<Obi.TriangleMeshHeader>
struct  NativeArray_1_tFC2C5BFEF570BAC41857EA3575BEB7E1F9C1F564 
{
public:
	// System.Void* Unity.Collections.NativeArray`1::m_Buffer
	void* ___m_Buffer_0;
	// System.Int32 Unity.Collections.NativeArray`1::m_Length
	int32_t ___m_Length_1;
	// Unity.Collections.Allocator Unity.Collections.NativeArray`1::m_AllocatorLabel
	int32_t ___m_AllocatorLabel_2;

public:
	inline static int32_t get_offset_of_m_Buffer_0() { return static_cast<int32_t>(offsetof(NativeArray_1_tFC2C5BFEF570BAC41857EA3575BEB7E1F9C1F564, ___m_Buffer_0)); }
	inline void* get_m_Buffer_0() const { return ___m_Buffer_0; }
	inline void** get_address_of_m_Buffer_0() { return &___m_Buffer_0; }
	inline void set_m_Buffer_0(void* value)
	{
		___m_Buffer_0 = value;
	}

	inline static int32_t get_offset_of_m_Length_1() { return static_cast<int32_t>(offsetof(NativeArray_1_tFC2C5BFEF570BAC41857EA3575BEB7E1F9C1F564, ___m_Length_1)); }
	inline int32_t get_m_Length_1() const { return ___m_Length_1; }
	inline int32_t* get_address_of_m_Length_1() { return &___m_Length_1; }
	inline void set_m_Length_1(int32_t value)
	{
		___m_Length_1 = value;
	}

	inline static int32_t get_offset_of_m_AllocatorLabel_2() { return static_cast<int32_t>(offsetof(NativeArray_1_tFC2C5BFEF570BAC41857EA3575BEB7E1F9C1F564, ___m_AllocatorLabel_2)); }
	inline int32_t get_m_AllocatorLabel_2() const { return ___m_AllocatorLabel_2; }
	inline int32_t* get_address_of_m_AllocatorLabel_2() { return &___m_AllocatorLabel_2; }
	inline void set_m_AllocatorLabel_2(int32_t value)
	{
		___m_AllocatorLabel_2 = value;
	}
};


// Unity.Collections.NativeArray`1<Obi.VInt4>
struct  NativeArray_1_t62E64122D8894FB32635400C7790A369FB6512F4 
{
public:
	// System.Void* Unity.Collections.NativeArray`1::m_Buffer
	void* ___m_Buffer_0;
	// System.Int32 Unity.Collections.NativeArray`1::m_Length
	int32_t ___m_Length_1;
	// Unity.Collections.Allocator Unity.Collections.NativeArray`1::m_AllocatorLabel
	int32_t ___m_AllocatorLabel_2;

public:
	inline static int32_t get_offset_of_m_Buffer_0() { return static_cast<int32_t>(offsetof(NativeArray_1_t62E64122D8894FB32635400C7790A369FB6512F4, ___m_Buffer_0)); }
	inline void* get_m_Buffer_0() const { return ___m_Buffer_0; }
	inline void** get_address_of_m_Buffer_0() { return &___m_Buffer_0; }
	inline void set_m_Buffer_0(void* value)
	{
		___m_Buffer_0 = value;
	}

	inline static int32_t get_offset_of_m_Length_1() { return static_cast<int32_t>(offsetof(NativeArray_1_t62E64122D8894FB32635400C7790A369FB6512F4, ___m_Length_1)); }
	inline int32_t get_m_Length_1() const { return ___m_Length_1; }
	inline int32_t* get_address_of_m_Length_1() { return &___m_Length_1; }
	inline void set_m_Length_1(int32_t value)
	{
		___m_Length_1 = value;
	}

	inline static int32_t get_offset_of_m_AllocatorLabel_2() { return static_cast<int32_t>(offsetof(NativeArray_1_t62E64122D8894FB32635400C7790A369FB6512F4, ___m_AllocatorLabel_2)); }
	inline int32_t get_m_AllocatorLabel_2() const { return ___m_AllocatorLabel_2; }
	inline int32_t* get_address_of_m_AllocatorLabel_2() { return &___m_AllocatorLabel_2; }
	inline void set_m_AllocatorLabel_2(int32_t value)
	{
		___m_AllocatorLabel_2 = value;
	}
};


// Unity.Collections.NativeArray`1<Oni_Contact>
struct  NativeArray_1_tD60C94148A4EF939C0FEC50D112D82FBFA965023 
{
public:
	// System.Void* Unity.Collections.NativeArray`1::m_Buffer
	void* ___m_Buffer_0;
	// System.Int32 Unity.Collections.NativeArray`1::m_Length
	int32_t ___m_Length_1;
	// Unity.Collections.Allocator Unity.Collections.NativeArray`1::m_AllocatorLabel
	int32_t ___m_AllocatorLabel_2;

public:
	inline static int32_t get_offset_of_m_Buffer_0() { return static_cast<int32_t>(offsetof(NativeArray_1_tD60C94148A4EF939C0FEC50D112D82FBFA965023, ___m_Buffer_0)); }
	inline void* get_m_Buffer_0() const { return ___m_Buffer_0; }
	inline void** get_address_of_m_Buffer_0() { return &___m_Buffer_0; }
	inline void set_m_Buffer_0(void* value)
	{
		___m_Buffer_0 = value;
	}

	inline static int32_t get_offset_of_m_Length_1() { return static_cast<int32_t>(offsetof(NativeArray_1_tD60C94148A4EF939C0FEC50D112D82FBFA965023, ___m_Length_1)); }
	inline int32_t get_m_Length_1() const { return ___m_Length_1; }
	inline int32_t* get_address_of_m_Length_1() { return &___m_Length_1; }
	inline void set_m_Length_1(int32_t value)
	{
		___m_Length_1 = value;
	}

	inline static int32_t get_offset_of_m_AllocatorLabel_2() { return static_cast<int32_t>(offsetof(NativeArray_1_tD60C94148A4EF939C0FEC50D112D82FBFA965023, ___m_AllocatorLabel_2)); }
	inline int32_t get_m_AllocatorLabel_2() const { return ___m_AllocatorLabel_2; }
	inline int32_t* get_address_of_m_AllocatorLabel_2() { return &___m_AllocatorLabel_2; }
	inline void set_m_AllocatorLabel_2(int32_t value)
	{
		___m_AllocatorLabel_2 = value;
	}
};


// Unity.Collections.NativeArray`1<System.Int32>
struct  NativeArray_1_tD60079F06B473C85CF6C2BB4F2D203F38191AE99 
{
public:
	// System.Void* Unity.Collections.NativeArray`1::m_Buffer
	void* ___m_Buffer_0;
	// System.Int32 Unity.Collections.NativeArray`1::m_Length
	int32_t ___m_Length_1;
	// Unity.Collections.Allocator Unity.Collections.NativeArray`1::m_AllocatorLabel
	int32_t ___m_AllocatorLabel_2;

public:
	inline static int32_t get_offset_of_m_Buffer_0() { return static_cast<int32_t>(offsetof(NativeArray_1_tD60079F06B473C85CF6C2BB4F2D203F38191AE99, ___m_Buffer_0)); }
	inline void* get_m_Buffer_0() const { return ___m_Buffer_0; }
	inline void** get_address_of_m_Buffer_0() { return &___m_Buffer_0; }
	inline void set_m_Buffer_0(void* value)
	{
		___m_Buffer_0 = value;
	}

	inline static int32_t get_offset_of_m_Length_1() { return static_cast<int32_t>(offsetof(NativeArray_1_tD60079F06B473C85CF6C2BB4F2D203F38191AE99, ___m_Length_1)); }
	inline int32_t get_m_Length_1() const { return ___m_Length_1; }
	inline int32_t* get_address_of_m_Length_1() { return &___m_Length_1; }
	inline void set_m_Length_1(int32_t value)
	{
		___m_Length_1 = value;
	}

	inline static int32_t get_offset_of_m_AllocatorLabel_2() { return static_cast<int32_t>(offsetof(NativeArray_1_tD60079F06B473C85CF6C2BB4F2D203F38191AE99, ___m_AllocatorLabel_2)); }
	inline int32_t get_m_AllocatorLabel_2() const { return ___m_AllocatorLabel_2; }
	inline int32_t* get_address_of_m_AllocatorLabel_2() { return &___m_AllocatorLabel_2; }
	inline void set_m_AllocatorLabel_2(int32_t value)
	{
		___m_AllocatorLabel_2 = value;
	}
};


// Unity.Collections.NativeArray`1<System.IntPtr>
struct  NativeArray_1_t5DC9D761D5D8A1E88FF45B12664595E0483C146F 
{
public:
	// System.Void* Unity.Collections.NativeArray`1::m_Buffer
	void* ___m_Buffer_0;
	// System.Int32 Unity.Collections.NativeArray`1::m_Length
	int32_t ___m_Length_1;
	// Unity.Collections.Allocator Unity.Collections.NativeArray`1::m_AllocatorLabel
	int32_t ___m_AllocatorLabel_2;

public:
	inline static int32_t get_offset_of_m_Buffer_0() { return static_cast<int32_t>(offsetof(NativeArray_1_t5DC9D761D5D8A1E88FF45B12664595E0483C146F, ___m_Buffer_0)); }
	inline void* get_m_Buffer_0() const { return ___m_Buffer_0; }
	inline void** get_address_of_m_Buffer_0() { return &___m_Buffer_0; }
	inline void set_m_Buffer_0(void* value)
	{
		___m_Buffer_0 = value;
	}

	inline static int32_t get_offset_of_m_Length_1() { return static_cast<int32_t>(offsetof(NativeArray_1_t5DC9D761D5D8A1E88FF45B12664595E0483C146F, ___m_Length_1)); }
	inline int32_t get_m_Length_1() const { return ___m_Length_1; }
	inline int32_t* get_address_of_m_Length_1() { return &___m_Length_1; }
	inline void set_m_Length_1(int32_t value)
	{
		___m_Length_1 = value;
	}

	inline static int32_t get_offset_of_m_AllocatorLabel_2() { return static_cast<int32_t>(offsetof(NativeArray_1_t5DC9D761D5D8A1E88FF45B12664595E0483C146F, ___m_AllocatorLabel_2)); }
	inline int32_t get_m_AllocatorLabel_2() const { return ___m_AllocatorLabel_2; }
	inline int32_t* get_address_of_m_AllocatorLabel_2() { return &___m_AllocatorLabel_2; }
	inline void set_m_AllocatorLabel_2(int32_t value)
	{
		___m_AllocatorLabel_2 = value;
	}
};


// Unity.Collections.NativeArray`1<System.Single>
struct  NativeArray_1_t5F920DC5A531D604D161A0FAD3479B5BFE0D9232 
{
public:
	// System.Void* Unity.Collections.NativeArray`1::m_Buffer
	void* ___m_Buffer_0;
	// System.Int32 Unity.Collections.NativeArray`1::m_Length
	int32_t ___m_Length_1;
	// Unity.Collections.Allocator Unity.Collections.NativeArray`1::m_AllocatorLabel
	int32_t ___m_AllocatorLabel_2;

public:
	inline static int32_t get_offset_of_m_Buffer_0() { return static_cast<int32_t>(offsetof(NativeArray_1_t5F920DC5A531D604D161A0FAD3479B5BFE0D9232, ___m_Buffer_0)); }
	inline void* get_m_Buffer_0() const { return ___m_Buffer_0; }
	inline void** get_address_of_m_Buffer_0() { return &___m_Buffer_0; }
	inline void set_m_Buffer_0(void* value)
	{
		___m_Buffer_0 = value;
	}

	inline static int32_t get_offset_of_m_Length_1() { return static_cast<int32_t>(offsetof(NativeArray_1_t5F920DC5A531D604D161A0FAD3479B5BFE0D9232, ___m_Length_1)); }
	inline int32_t get_m_Length_1() const { return ___m_Length_1; }
	inline int32_t* get_address_of_m_Length_1() { return &___m_Length_1; }
	inline void set_m_Length_1(int32_t value)
	{
		___m_Length_1 = value;
	}

	inline static int32_t get_offset_of_m_AllocatorLabel_2() { return static_cast<int32_t>(offsetof(NativeArray_1_t5F920DC5A531D604D161A0FAD3479B5BFE0D9232, ___m_AllocatorLabel_2)); }
	inline int32_t get_m_AllocatorLabel_2() const { return ___m_AllocatorLabel_2; }
	inline int32_t* get_address_of_m_AllocatorLabel_2() { return &___m_AllocatorLabel_2; }
	inline void set_m_AllocatorLabel_2(int32_t value)
	{
		___m_AllocatorLabel_2 = value;
	}
};


// Unity.Collections.NativeArray`1<UnityEngine.Matrix4x4>
struct  NativeArray_1_t61B1FEBC3536B66CB721DC2B6CEDBB648D8F7AC8 
{
public:
	// System.Void* Unity.Collections.NativeArray`1::m_Buffer
	void* ___m_Buffer_0;
	// System.Int32 Unity.Collections.NativeArray`1::m_Length
	int32_t ___m_Length_1;
	// Unity.Collections.Allocator Unity.Collections.NativeArray`1::m_AllocatorLabel
	int32_t ___m_AllocatorLabel_2;

public:
	inline static int32_t get_offset_of_m_Buffer_0() { return static_cast<int32_t>(offsetof(NativeArray_1_t61B1FEBC3536B66CB721DC2B6CEDBB648D8F7AC8, ___m_Buffer_0)); }
	inline void* get_m_Buffer_0() const { return ___m_Buffer_0; }
	inline void** get_address_of_m_Buffer_0() { return &___m_Buffer_0; }
	inline void set_m_Buffer_0(void* value)
	{
		___m_Buffer_0 = value;
	}

	inline static int32_t get_offset_of_m_Length_1() { return static_cast<int32_t>(offsetof(NativeArray_1_t61B1FEBC3536B66CB721DC2B6CEDBB648D8F7AC8, ___m_Length_1)); }
	inline int32_t get_m_Length_1() const { return ___m_Length_1; }
	inline int32_t* get_address_of_m_Length_1() { return &___m_Length_1; }
	inline void set_m_Length_1(int32_t value)
	{
		___m_Length_1 = value;
	}

	inline static int32_t get_offset_of_m_AllocatorLabel_2() { return static_cast<int32_t>(offsetof(NativeArray_1_t61B1FEBC3536B66CB721DC2B6CEDBB648D8F7AC8, ___m_AllocatorLabel_2)); }
	inline int32_t get_m_AllocatorLabel_2() const { return ___m_AllocatorLabel_2; }
	inline int32_t* get_address_of_m_AllocatorLabel_2() { return &___m_AllocatorLabel_2; }
	inline void set_m_AllocatorLabel_2(int32_t value)
	{
		___m_AllocatorLabel_2 = value;
	}
};


// Unity.Collections.NativeArray`1<UnityEngine.Quaternion>
struct  NativeArray_1_t2672EA3A09E3FE76419439DC581905D460584A60 
{
public:
	// System.Void* Unity.Collections.NativeArray`1::m_Buffer
	void* ___m_Buffer_0;
	// System.Int32 Unity.Collections.NativeArray`1::m_Length
	int32_t ___m_Length_1;
	// Unity.Collections.Allocator Unity.Collections.NativeArray`1::m_AllocatorLabel
	int32_t ___m_AllocatorLabel_2;

public:
	inline static int32_t get_offset_of_m_Buffer_0() { return static_cast<int32_t>(offsetof(NativeArray_1_t2672EA3A09E3FE76419439DC581905D460584A60, ___m_Buffer_0)); }
	inline void* get_m_Buffer_0() const { return ___m_Buffer_0; }
	inline void** get_address_of_m_Buffer_0() { return &___m_Buffer_0; }
	inline void set_m_Buffer_0(void* value)
	{
		___m_Buffer_0 = value;
	}

	inline static int32_t get_offset_of_m_Length_1() { return static_cast<int32_t>(offsetof(NativeArray_1_t2672EA3A09E3FE76419439DC581905D460584A60, ___m_Length_1)); }
	inline int32_t get_m_Length_1() const { return ___m_Length_1; }
	inline int32_t* get_address_of_m_Length_1() { return &___m_Length_1; }
	inline void set_m_Length_1(int32_t value)
	{
		___m_Length_1 = value;
	}

	inline static int32_t get_offset_of_m_AllocatorLabel_2() { return static_cast<int32_t>(offsetof(NativeArray_1_t2672EA3A09E3FE76419439DC581905D460584A60, ___m_AllocatorLabel_2)); }
	inline int32_t get_m_AllocatorLabel_2() const { return ___m_AllocatorLabel_2; }
	inline int32_t* get_address_of_m_AllocatorLabel_2() { return &___m_AllocatorLabel_2; }
	inline void set_m_AllocatorLabel_2(int32_t value)
	{
		___m_AllocatorLabel_2 = value;
	}
};


// Unity.Collections.NativeArray`1<UnityEngine.Vector2>
struct  NativeArray_1_t431C85F30275831D1F5D458AB73DFCE050693BC0 
{
public:
	// System.Void* Unity.Collections.NativeArray`1::m_Buffer
	void* ___m_Buffer_0;
	// System.Int32 Unity.Collections.NativeArray`1::m_Length
	int32_t ___m_Length_1;
	// Unity.Collections.Allocator Unity.Collections.NativeArray`1::m_AllocatorLabel
	int32_t ___m_AllocatorLabel_2;

public:
	inline static int32_t get_offset_of_m_Buffer_0() { return static_cast<int32_t>(offsetof(NativeArray_1_t431C85F30275831D1F5D458AB73DFCE050693BC0, ___m_Buffer_0)); }
	inline void* get_m_Buffer_0() const { return ___m_Buffer_0; }
	inline void** get_address_of_m_Buffer_0() { return &___m_Buffer_0; }
	inline void set_m_Buffer_0(void* value)
	{
		___m_Buffer_0 = value;
	}

	inline static int32_t get_offset_of_m_Length_1() { return static_cast<int32_t>(offsetof(NativeArray_1_t431C85F30275831D1F5D458AB73DFCE050693BC0, ___m_Length_1)); }
	inline int32_t get_m_Length_1() const { return ___m_Length_1; }
	inline int32_t* get_address_of_m_Length_1() { return &___m_Length_1; }
	inline void set_m_Length_1(int32_t value)
	{
		___m_Length_1 = value;
	}

	inline static int32_t get_offset_of_m_AllocatorLabel_2() { return static_cast<int32_t>(offsetof(NativeArray_1_t431C85F30275831D1F5D458AB73DFCE050693BC0, ___m_AllocatorLabel_2)); }
	inline int32_t get_m_AllocatorLabel_2() const { return ___m_AllocatorLabel_2; }
	inline int32_t* get_address_of_m_AllocatorLabel_2() { return &___m_AllocatorLabel_2; }
	inline void set_m_AllocatorLabel_2(int32_t value)
	{
		___m_AllocatorLabel_2 = value;
	}
};


// Unity.Collections.NativeArray`1<UnityEngine.Vector3>
struct  NativeArray_1_t2577BCA09CA248EFB78BD7FDA7F09D5C395DFF52 
{
public:
	// System.Void* Unity.Collections.NativeArray`1::m_Buffer
	void* ___m_Buffer_0;
	// System.Int32 Unity.Collections.NativeArray`1::m_Length
	int32_t ___m_Length_1;
	// Unity.Collections.Allocator Unity.Collections.NativeArray`1::m_AllocatorLabel
	int32_t ___m_AllocatorLabel_2;

public:
	inline static int32_t get_offset_of_m_Buffer_0() { return static_cast<int32_t>(offsetof(NativeArray_1_t2577BCA09CA248EFB78BD7FDA7F09D5C395DFF52, ___m_Buffer_0)); }
	inline void* get_m_Buffer_0() const { return ___m_Buffer_0; }
	inline void** get_address_of_m_Buffer_0() { return &___m_Buffer_0; }
	inline void set_m_Buffer_0(void* value)
	{
		___m_Buffer_0 = value;
	}

	inline static int32_t get_offset_of_m_Length_1() { return static_cast<int32_t>(offsetof(NativeArray_1_t2577BCA09CA248EFB78BD7FDA7F09D5C395DFF52, ___m_Length_1)); }
	inline int32_t get_m_Length_1() const { return ___m_Length_1; }
	inline int32_t* get_address_of_m_Length_1() { return &___m_Length_1; }
	inline void set_m_Length_1(int32_t value)
	{
		___m_Length_1 = value;
	}

	inline static int32_t get_offset_of_m_AllocatorLabel_2() { return static_cast<int32_t>(offsetof(NativeArray_1_t2577BCA09CA248EFB78BD7FDA7F09D5C395DFF52, ___m_AllocatorLabel_2)); }
	inline int32_t get_m_AllocatorLabel_2() const { return ___m_AllocatorLabel_2; }
	inline int32_t* get_address_of_m_AllocatorLabel_2() { return &___m_AllocatorLabel_2; }
	inline void set_m_AllocatorLabel_2(int32_t value)
	{
		___m_AllocatorLabel_2 = value;
	}
};


// Unity.Collections.NativeArray`1<UnityEngine.Vector4>
struct  NativeArray_1_t381CA7D92591F9C96DB84965C31AF8713A546156 
{
public:
	// System.Void* Unity.Collections.NativeArray`1::m_Buffer
	void* ___m_Buffer_0;
	// System.Int32 Unity.Collections.NativeArray`1::m_Length
	int32_t ___m_Length_1;
	// Unity.Collections.Allocator Unity.Collections.NativeArray`1::m_AllocatorLabel
	int32_t ___m_AllocatorLabel_2;

public:
	inline static int32_t get_offset_of_m_Buffer_0() { return static_cast<int32_t>(offsetof(NativeArray_1_t381CA7D92591F9C96DB84965C31AF8713A546156, ___m_Buffer_0)); }
	inline void* get_m_Buffer_0() const { return ___m_Buffer_0; }
	inline void** get_address_of_m_Buffer_0() { return &___m_Buffer_0; }
	inline void set_m_Buffer_0(void* value)
	{
		___m_Buffer_0 = value;
	}

	inline static int32_t get_offset_of_m_Length_1() { return static_cast<int32_t>(offsetof(NativeArray_1_t381CA7D92591F9C96DB84965C31AF8713A546156, ___m_Length_1)); }
	inline int32_t get_m_Length_1() const { return ___m_Length_1; }
	inline int32_t* get_address_of_m_Length_1() { return &___m_Length_1; }
	inline void set_m_Length_1(int32_t value)
	{
		___m_Length_1 = value;
	}

	inline static int32_t get_offset_of_m_AllocatorLabel_2() { return static_cast<int32_t>(offsetof(NativeArray_1_t381CA7D92591F9C96DB84965C31AF8713A546156, ___m_AllocatorLabel_2)); }
	inline int32_t get_m_AllocatorLabel_2() const { return ___m_AllocatorLabel_2; }
	inline int32_t* get_address_of_m_AllocatorLabel_2() { return &___m_AllocatorLabel_2; }
	inline void set_m_AllocatorLabel_2(int32_t value)
	{
		___m_AllocatorLabel_2 = value;
	}
};


// UnityEngine.Experimental.GlobalIllumination.LightDataGI
struct  LightDataGI_t0C34AB69E4E96717FD276B35116C798A641D44F2 
{
public:
	// System.Int32 UnityEngine.Experimental.GlobalIllumination.LightDataGI::instanceID
	int32_t ___instanceID_0;
	// System.Int32 UnityEngine.Experimental.GlobalIllumination.LightDataGI::cookieID
	int32_t ___cookieID_1;
	// System.Single UnityEngine.Experimental.GlobalIllumination.LightDataGI::cookieScale
	float ___cookieScale_2;
	// UnityEngine.Experimental.GlobalIllumination.LinearColor UnityEngine.Experimental.GlobalIllumination.LightDataGI::color
	LinearColor_tB134EA090C61E6624DE36F52980CA7E2C893D5F2  ___color_3;
	// UnityEngine.Experimental.GlobalIllumination.LinearColor UnityEngine.Experimental.GlobalIllumination.LightDataGI::indirectColor
	LinearColor_tB134EA090C61E6624DE36F52980CA7E2C893D5F2  ___indirectColor_4;
	// UnityEngine.Quaternion UnityEngine.Experimental.GlobalIllumination.LightDataGI::orientation
	Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  ___orientation_5;
	// UnityEngine.Vector3 UnityEngine.Experimental.GlobalIllumination.LightDataGI::position
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___position_6;
	// System.Single UnityEngine.Experimental.GlobalIllumination.LightDataGI::range
	float ___range_7;
	// System.Single UnityEngine.Experimental.GlobalIllumination.LightDataGI::coneAngle
	float ___coneAngle_8;
	// System.Single UnityEngine.Experimental.GlobalIllumination.LightDataGI::innerConeAngle
	float ___innerConeAngle_9;
	// System.Single UnityEngine.Experimental.GlobalIllumination.LightDataGI::shape0
	float ___shape0_10;
	// System.Single UnityEngine.Experimental.GlobalIllumination.LightDataGI::shape1
	float ___shape1_11;
	// UnityEngine.Experimental.GlobalIllumination.LightType UnityEngine.Experimental.GlobalIllumination.LightDataGI::type
	uint8_t ___type_12;
	// UnityEngine.Experimental.GlobalIllumination.LightMode UnityEngine.Experimental.GlobalIllumination.LightDataGI::mode
	uint8_t ___mode_13;
	// System.Byte UnityEngine.Experimental.GlobalIllumination.LightDataGI::shadow
	uint8_t ___shadow_14;
	// UnityEngine.Experimental.GlobalIllumination.FalloffType UnityEngine.Experimental.GlobalIllumination.LightDataGI::falloff
	uint8_t ___falloff_15;

public:
	inline static int32_t get_offset_of_instanceID_0() { return static_cast<int32_t>(offsetof(LightDataGI_t0C34AB69E4E96717FD276B35116C798A641D44F2, ___instanceID_0)); }
	inline int32_t get_instanceID_0() const { return ___instanceID_0; }
	inline int32_t* get_address_of_instanceID_0() { return &___instanceID_0; }
	inline void set_instanceID_0(int32_t value)
	{
		___instanceID_0 = value;
	}

	inline static int32_t get_offset_of_cookieID_1() { return static_cast<int32_t>(offsetof(LightDataGI_t0C34AB69E4E96717FD276B35116C798A641D44F2, ___cookieID_1)); }
	inline int32_t get_cookieID_1() const { return ___cookieID_1; }
	inline int32_t* get_address_of_cookieID_1() { return &___cookieID_1; }
	inline void set_cookieID_1(int32_t value)
	{
		___cookieID_1 = value;
	}

	inline static int32_t get_offset_of_cookieScale_2() { return static_cast<int32_t>(offsetof(LightDataGI_t0C34AB69E4E96717FD276B35116C798A641D44F2, ___cookieScale_2)); }
	inline float get_cookieScale_2() const { return ___cookieScale_2; }
	inline float* get_address_of_cookieScale_2() { return &___cookieScale_2; }
	inline void set_cookieScale_2(float value)
	{
		___cookieScale_2 = value;
	}

	inline static int32_t get_offset_of_color_3() { return static_cast<int32_t>(offsetof(LightDataGI_t0C34AB69E4E96717FD276B35116C798A641D44F2, ___color_3)); }
	inline LinearColor_tB134EA090C61E6624DE36F52980CA7E2C893D5F2  get_color_3() const { return ___color_3; }
	inline LinearColor_tB134EA090C61E6624DE36F52980CA7E2C893D5F2 * get_address_of_color_3() { return &___color_3; }
	inline void set_color_3(LinearColor_tB134EA090C61E6624DE36F52980CA7E2C893D5F2  value)
	{
		___color_3 = value;
	}

	inline static int32_t get_offset_of_indirectColor_4() { return static_cast<int32_t>(offsetof(LightDataGI_t0C34AB69E4E96717FD276B35116C798A641D44F2, ___indirectColor_4)); }
	inline LinearColor_tB134EA090C61E6624DE36F52980CA7E2C893D5F2  get_indirectColor_4() const { return ___indirectColor_4; }
	inline LinearColor_tB134EA090C61E6624DE36F52980CA7E2C893D5F2 * get_address_of_indirectColor_4() { return &___indirectColor_4; }
	inline void set_indirectColor_4(LinearColor_tB134EA090C61E6624DE36F52980CA7E2C893D5F2  value)
	{
		___indirectColor_4 = value;
	}

	inline static int32_t get_offset_of_orientation_5() { return static_cast<int32_t>(offsetof(LightDataGI_t0C34AB69E4E96717FD276B35116C798A641D44F2, ___orientation_5)); }
	inline Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  get_orientation_5() const { return ___orientation_5; }
	inline Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4 * get_address_of_orientation_5() { return &___orientation_5; }
	inline void set_orientation_5(Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  value)
	{
		___orientation_5 = value;
	}

	inline static int32_t get_offset_of_position_6() { return static_cast<int32_t>(offsetof(LightDataGI_t0C34AB69E4E96717FD276B35116C798A641D44F2, ___position_6)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_position_6() const { return ___position_6; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_position_6() { return &___position_6; }
	inline void set_position_6(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___position_6 = value;
	}

	inline static int32_t get_offset_of_range_7() { return static_cast<int32_t>(offsetof(LightDataGI_t0C34AB69E4E96717FD276B35116C798A641D44F2, ___range_7)); }
	inline float get_range_7() const { return ___range_7; }
	inline float* get_address_of_range_7() { return &___range_7; }
	inline void set_range_7(float value)
	{
		___range_7 = value;
	}

	inline static int32_t get_offset_of_coneAngle_8() { return static_cast<int32_t>(offsetof(LightDataGI_t0C34AB69E4E96717FD276B35116C798A641D44F2, ___coneAngle_8)); }
	inline float get_coneAngle_8() const { return ___coneAngle_8; }
	inline float* get_address_of_coneAngle_8() { return &___coneAngle_8; }
	inline void set_coneAngle_8(float value)
	{
		___coneAngle_8 = value;
	}

	inline static int32_t get_offset_of_innerConeAngle_9() { return static_cast<int32_t>(offsetof(LightDataGI_t0C34AB69E4E96717FD276B35116C798A641D44F2, ___innerConeAngle_9)); }
	inline float get_innerConeAngle_9() const { return ___innerConeAngle_9; }
	inline float* get_address_of_innerConeAngle_9() { return &___innerConeAngle_9; }
	inline void set_innerConeAngle_9(float value)
	{
		___innerConeAngle_9 = value;
	}

	inline static int32_t get_offset_of_shape0_10() { return static_cast<int32_t>(offsetof(LightDataGI_t0C34AB69E4E96717FD276B35116C798A641D44F2, ___shape0_10)); }
	inline float get_shape0_10() const { return ___shape0_10; }
	inline float* get_address_of_shape0_10() { return &___shape0_10; }
	inline void set_shape0_10(float value)
	{
		___shape0_10 = value;
	}

	inline static int32_t get_offset_of_shape1_11() { return static_cast<int32_t>(offsetof(LightDataGI_t0C34AB69E4E96717FD276B35116C798A641D44F2, ___shape1_11)); }
	inline float get_shape1_11() const { return ___shape1_11; }
	inline float* get_address_of_shape1_11() { return &___shape1_11; }
	inline void set_shape1_11(float value)
	{
		___shape1_11 = value;
	}

	inline static int32_t get_offset_of_type_12() { return static_cast<int32_t>(offsetof(LightDataGI_t0C34AB69E4E96717FD276B35116C798A641D44F2, ___type_12)); }
	inline uint8_t get_type_12() const { return ___type_12; }
	inline uint8_t* get_address_of_type_12() { return &___type_12; }
	inline void set_type_12(uint8_t value)
	{
		___type_12 = value;
	}

	inline static int32_t get_offset_of_mode_13() { return static_cast<int32_t>(offsetof(LightDataGI_t0C34AB69E4E96717FD276B35116C798A641D44F2, ___mode_13)); }
	inline uint8_t get_mode_13() const { return ___mode_13; }
	inline uint8_t* get_address_of_mode_13() { return &___mode_13; }
	inline void set_mode_13(uint8_t value)
	{
		___mode_13 = value;
	}

	inline static int32_t get_offset_of_shadow_14() { return static_cast<int32_t>(offsetof(LightDataGI_t0C34AB69E4E96717FD276B35116C798A641D44F2, ___shadow_14)); }
	inline uint8_t get_shadow_14() const { return ___shadow_14; }
	inline uint8_t* get_address_of_shadow_14() { return &___shadow_14; }
	inline void set_shadow_14(uint8_t value)
	{
		___shadow_14 = value;
	}

	inline static int32_t get_offset_of_falloff_15() { return static_cast<int32_t>(offsetof(LightDataGI_t0C34AB69E4E96717FD276B35116C798A641D44F2, ___falloff_15)); }
	inline uint8_t get_falloff_15() const { return ___falloff_15; }
	inline uint8_t* get_address_of_falloff_15() { return &___falloff_15; }
	inline void set_falloff_15(uint8_t value)
	{
		___falloff_15 = value;
	}
};


// System.Action
struct  Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6  : public MulticastDelegate_t
{
public:

public:
};


// System.ArgumentException
struct  ArgumentException_t505FA8C11E883F2D96C797AD9D396490794DEE00  : public SystemException_tC551B4D6EE3772B5F32C71EE8C719F4B43ECCC62
{
public:
	// System.String System.ArgumentException::m_paramName
	String_t* ___m_paramName_17;

public:
	inline static int32_t get_offset_of_m_paramName_17() { return static_cast<int32_t>(offsetof(ArgumentException_t505FA8C11E883F2D96C797AD9D396490794DEE00, ___m_paramName_17)); }
	inline String_t* get_m_paramName_17() const { return ___m_paramName_17; }
	inline String_t** get_address_of_m_paramName_17() { return &___m_paramName_17; }
	inline void set_m_paramName_17(String_t* value)
	{
		___m_paramName_17 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_paramName_17), (void*)value);
	}
};


// System.ArgumentNullException
struct  ArgumentNullException_tFB5C4621957BC53A7D1B4FDD5C38B4D6E15DB8FB  : public ArgumentException_t505FA8C11E883F2D96C797AD9D396490794DEE00
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif


// System.Threading.Tasks.Task`1<TResult> System.Runtime.CompilerServices.AsyncTaskMethodBuilder`1<System.Boolean>::get_Task()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Task_1_t9C1FE9F18F52F3409B9E970FA38801A443AE7849 * AsyncTaskMethodBuilder_1_get_Task_m6F8035FA0DEBE32D20BAD45867B41491F5D55F78_gshared (AsyncTaskMethodBuilder_1_tABE1DEF12F121D6FC8ABF04869ED964FF83EA9B5 * __this, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.AsyncTaskMethodBuilder`1<System.Boolean>::AwaitUnsafeOnCompleted<System.Runtime.CompilerServices.ConfiguredTaskAwaitable`1/ConfiguredTaskAwaiter<System.Boolean>,System.Threading.SemaphoreSlim/<WaitUntilCountOrTimeoutAsync>d__31>(TAwaiter&,TStateMachine&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AsyncTaskMethodBuilder_1_AwaitUnsafeOnCompleted_TisConfiguredTaskAwaiter_t286C97C0AF102C4C0BE55CE2025CC7BD1FB5C20C_TisU3CWaitUntilCountOrTimeoutAsyncU3Ed__31_t227D1F5F19C948CA8C23C80B5F19147D4AAED14F_m06010DFBCE2CDD2AE1BB5ADD25230521E9B527DC_gshared (AsyncTaskMethodBuilder_1_tABE1DEF12F121D6FC8ABF04869ED964FF83EA9B5 * __this, ConfiguredTaskAwaiter_t286C97C0AF102C4C0BE55CE2025CC7BD1FB5C20C * ___awaiter0, U3CWaitUntilCountOrTimeoutAsyncU3Ed__31_t227D1F5F19C948CA8C23C80B5F19147D4AAED14F * ___stateMachine1, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.AsyncTaskMethodBuilder`1<System.Boolean>::AwaitUnsafeOnCompleted<System.Runtime.CompilerServices.ConfiguredTaskAwaitable`1/ConfiguredTaskAwaiter<System.Object>,System.Threading.SemaphoreSlim/<WaitUntilCountOrTimeoutAsync>d__31>(TAwaiter&,TStateMachine&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AsyncTaskMethodBuilder_1_AwaitUnsafeOnCompleted_TisConfiguredTaskAwaiter_t2CE498F9A6CE5405242AE2D77F03E58985B7C3ED_TisU3CWaitUntilCountOrTimeoutAsyncU3Ed__31_t227D1F5F19C948CA8C23C80B5F19147D4AAED14F_m4FDC6642836D25291A0E343845527873A88013C9_gshared (AsyncTaskMethodBuilder_1_tABE1DEF12F121D6FC8ABF04869ED964FF83EA9B5 * __this, ConfiguredTaskAwaiter_t2CE498F9A6CE5405242AE2D77F03E58985B7C3ED * ___awaiter0, U3CWaitUntilCountOrTimeoutAsyncU3Ed__31_t227D1F5F19C948CA8C23C80B5F19147D4AAED14F * ___stateMachine1, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.AsyncTaskMethodBuilder`1<System.Boolean>::Start<System.Threading.SemaphoreSlim/<WaitUntilCountOrTimeoutAsync>d__31>(TStateMachine&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AsyncTaskMethodBuilder_1_Start_TisU3CWaitUntilCountOrTimeoutAsyncU3Ed__31_t227D1F5F19C948CA8C23C80B5F19147D4AAED14F_m67A42FEF4C09E843DDA99A81F84E675F9AB87CF9_gshared (AsyncTaskMethodBuilder_1_tABE1DEF12F121D6FC8ABF04869ED964FF83EA9B5 * __this, U3CWaitUntilCountOrTimeoutAsyncU3Ed__31_t227D1F5F19C948CA8C23C80B5F19147D4AAED14F * ___stateMachine0, const RuntimeMethod* method);
// System.Threading.Tasks.Task`1<TResult> System.Runtime.CompilerServices.AsyncTaskMethodBuilder`1<System.Object>::get_Task()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Task_1_tC1805497876E88B78A2B0CB81C6409E0B381AC17 * AsyncTaskMethodBuilder_1_get_Task_m61DEC300353320E428E17DA0D59D61974F4415BB_gshared (AsyncTaskMethodBuilder_1_tDD2A3BA099C327938EA03C35FDB1A7502BA73020 * __this, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.AsyncTaskMethodBuilder`1<System.Object>::AwaitUnsafeOnCompleted<System.Object,System.Object>(TAwaiter&,TStateMachine&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AsyncTaskMethodBuilder_1_AwaitUnsafeOnCompleted_TisRuntimeObject_TisRuntimeObject_mD7F0BC5F052B851265FF930DD5AB23C8E96269DE_gshared (AsyncTaskMethodBuilder_1_tDD2A3BA099C327938EA03C35FDB1A7502BA73020 * __this, RuntimeObject ** ___awaiter0, RuntimeObject ** ___stateMachine1, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.AsyncTaskMethodBuilder`1<System.Object>::Start<System.Object>(TStateMachine&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AsyncTaskMethodBuilder_1_Start_TisRuntimeObject_m25C57EA59EF179635A81530036C12082EE50878B_gshared (AsyncTaskMethodBuilder_1_tDD2A3BA099C327938EA03C35FDB1A7502BA73020 * __this, RuntimeObject ** ___stateMachine0, const RuntimeMethod* method);

// System.Boolean System.Threading.Tasks.AsyncCausalityTracer::get_LoggingOn()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool AsyncCausalityTracer_get_LoggingOn_mE0A03E121425371B1D1B65640172137C3B8EEA15 (const RuntimeMethod* method);
// System.Threading.Tasks.Task`1<TResult> System.Runtime.CompilerServices.AsyncTaskMethodBuilder`1<System.Boolean>::get_Task()
inline Task_1_t9C1FE9F18F52F3409B9E970FA38801A443AE7849 * AsyncTaskMethodBuilder_1_get_Task_m6F8035FA0DEBE32D20BAD45867B41491F5D55F78 (AsyncTaskMethodBuilder_1_tABE1DEF12F121D6FC8ABF04869ED964FF83EA9B5 * __this, const RuntimeMethod* method)
{
	return ((  Task_1_t9C1FE9F18F52F3409B9E970FA38801A443AE7849 * (*) (AsyncTaskMethodBuilder_1_tABE1DEF12F121D6FC8ABF04869ED964FF83EA9B5 *, const RuntimeMethod*))AsyncTaskMethodBuilder_1_get_Task_m6F8035FA0DEBE32D20BAD45867B41491F5D55F78_gshared)(__this, method);
}
// System.Action System.Runtime.CompilerServices.AsyncMethodBuilderCore::GetCompletionAction(System.Threading.Tasks.Task,System.Runtime.CompilerServices.AsyncMethodBuilderCore/MoveNextRunner&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 * AsyncMethodBuilderCore_GetCompletionAction_m7FE7F57CC452F0EDE870AB08EEB648E2027D4F5C (AsyncMethodBuilderCore_t2C85055E04767C52B9F66144476FCBF500DBFA34 * __this, Task_t804B25CFE3FC13AAEE16C8FA3BF52513F2A8DB60 * ___taskForTracing0, MoveNextRunner_tFAEA0BEDD353E2E34E8E287C67B1F5572FD30C2D ** ___runnerToInitialize1, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.AsyncMethodBuilderCore::PostBoxInitialization(System.Runtime.CompilerServices.IAsyncStateMachine,System.Runtime.CompilerServices.AsyncMethodBuilderCore/MoveNextRunner,System.Threading.Tasks.Task)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AsyncMethodBuilderCore_PostBoxInitialization_m22C1D9A2745255C6FC1426D4CB0C4355FBDA07E3 (AsyncMethodBuilderCore_t2C85055E04767C52B9F66144476FCBF500DBFA34 * __this, RuntimeObject* ___stateMachine0, MoveNextRunner_tFAEA0BEDD353E2E34E8E287C67B1F5572FD30C2D * ___runner1, Task_t804B25CFE3FC13AAEE16C8FA3BF52513F2A8DB60 * ___builtTask2, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.AsyncMethodBuilderCore::ThrowAsync(System.Exception,System.Threading.SynchronizationContext)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AsyncMethodBuilderCore_ThrowAsync_m69800E9752B30F7A1D824C2F1EBDEBA44BA75610 (Exception_t * ___exception0, SynchronizationContext_t17D9365B5E0D30A0910A16FA4351C525232EF069 * ___targetContext1, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.AsyncTaskMethodBuilder`1<System.Boolean>::AwaitUnsafeOnCompleted<System.Runtime.CompilerServices.ConfiguredTaskAwaitable`1/ConfiguredTaskAwaiter<System.Boolean>,System.Threading.SemaphoreSlim/<WaitUntilCountOrTimeoutAsync>d__31>(TAwaiter&,TStateMachine&)
inline void AsyncTaskMethodBuilder_1_AwaitUnsafeOnCompleted_TisConfiguredTaskAwaiter_t286C97C0AF102C4C0BE55CE2025CC7BD1FB5C20C_TisU3CWaitUntilCountOrTimeoutAsyncU3Ed__31_t227D1F5F19C948CA8C23C80B5F19147D4AAED14F_m06010DFBCE2CDD2AE1BB5ADD25230521E9B527DC (AsyncTaskMethodBuilder_1_tABE1DEF12F121D6FC8ABF04869ED964FF83EA9B5 * __this, ConfiguredTaskAwaiter_t286C97C0AF102C4C0BE55CE2025CC7BD1FB5C20C * ___awaiter0, U3CWaitUntilCountOrTimeoutAsyncU3Ed__31_t227D1F5F19C948CA8C23C80B5F19147D4AAED14F * ___stateMachine1, const RuntimeMethod* method)
{
	((  void (*) (AsyncTaskMethodBuilder_1_tABE1DEF12F121D6FC8ABF04869ED964FF83EA9B5 *, ConfiguredTaskAwaiter_t286C97C0AF102C4C0BE55CE2025CC7BD1FB5C20C *, U3CWaitUntilCountOrTimeoutAsyncU3Ed__31_t227D1F5F19C948CA8C23C80B5F19147D4AAED14F *, const RuntimeMethod*))AsyncTaskMethodBuilder_1_AwaitUnsafeOnCompleted_TisConfiguredTaskAwaiter_t286C97C0AF102C4C0BE55CE2025CC7BD1FB5C20C_TisU3CWaitUntilCountOrTimeoutAsyncU3Ed__31_t227D1F5F19C948CA8C23C80B5F19147D4AAED14F_m06010DFBCE2CDD2AE1BB5ADD25230521E9B527DC_gshared)(__this, ___awaiter0, ___stateMachine1, method);
}
// System.Void System.Runtime.CompilerServices.AsyncTaskMethodBuilder`1<System.Boolean>::AwaitUnsafeOnCompleted<System.Runtime.CompilerServices.ConfiguredTaskAwaitable`1/ConfiguredTaskAwaiter<System.Object>,System.Threading.SemaphoreSlim/<WaitUntilCountOrTimeoutAsync>d__31>(TAwaiter&,TStateMachine&)
inline void AsyncTaskMethodBuilder_1_AwaitUnsafeOnCompleted_TisConfiguredTaskAwaiter_t2CE498F9A6CE5405242AE2D77F03E58985B7C3ED_TisU3CWaitUntilCountOrTimeoutAsyncU3Ed__31_t227D1F5F19C948CA8C23C80B5F19147D4AAED14F_m4FDC6642836D25291A0E343845527873A88013C9 (AsyncTaskMethodBuilder_1_tABE1DEF12F121D6FC8ABF04869ED964FF83EA9B5 * __this, ConfiguredTaskAwaiter_t2CE498F9A6CE5405242AE2D77F03E58985B7C3ED * ___awaiter0, U3CWaitUntilCountOrTimeoutAsyncU3Ed__31_t227D1F5F19C948CA8C23C80B5F19147D4AAED14F * ___stateMachine1, const RuntimeMethod* method)
{
	((  void (*) (AsyncTaskMethodBuilder_1_tABE1DEF12F121D6FC8ABF04869ED964FF83EA9B5 *, ConfiguredTaskAwaiter_t2CE498F9A6CE5405242AE2D77F03E58985B7C3ED *, U3CWaitUntilCountOrTimeoutAsyncU3Ed__31_t227D1F5F19C948CA8C23C80B5F19147D4AAED14F *, const RuntimeMethod*))AsyncTaskMethodBuilder_1_AwaitUnsafeOnCompleted_TisConfiguredTaskAwaiter_t2CE498F9A6CE5405242AE2D77F03E58985B7C3ED_TisU3CWaitUntilCountOrTimeoutAsyncU3Ed__31_t227D1F5F19C948CA8C23C80B5F19147D4AAED14F_m4FDC6642836D25291A0E343845527873A88013C9_gshared)(__this, ___awaiter0, ___stateMachine1, method);
}
// System.Void System.ArgumentNullException::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ArgumentNullException__ctor_m81AB157B93BFE2FBFDB08B88F84B444293042F97 (ArgumentNullException_tFB5C4621957BC53A7D1B4FDD5C38B4D6E15DB8FB * __this, String_t* ___paramName0, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.RuntimeHelpers::PrepareConstrainedRegions()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RuntimeHelpers_PrepareConstrainedRegions_m4A4D3987FEE068EE30D1ABC4005CDD29D5C52560 (const RuntimeMethod* method);
// System.Void System.Threading.ExecutionContext::EstablishCopyOnWriteScope(System.Threading.ExecutionContextSwitcher&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ExecutionContext_EstablishCopyOnWriteScope_m526741703B27626D3AD07B14ADA3FECBF5C4E8DD (ExecutionContextSwitcher_t11B7DEE83408478EE3D5E29C988E5385AA9D7277 * ___ecsw0, const RuntimeMethod* method);
// System.Void System.Threading.SemaphoreSlim/<WaitUntilCountOrTimeoutAsync>d__31::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CWaitUntilCountOrTimeoutAsyncU3Ed__31_MoveNext_mB5E635B57C016D9A7039342703EB200F7BE8218E (U3CWaitUntilCountOrTimeoutAsyncU3Ed__31_t227D1F5F19C948CA8C23C80B5F19147D4AAED14F * __this, const RuntimeMethod* method);
// System.Void System.Threading.ExecutionContextSwitcher::Undo()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ExecutionContextSwitcher_Undo_mEC7752EB8502405D0F45F0E337C1B1FF34B74BF8 (ExecutionContextSwitcher_t11B7DEE83408478EE3D5E29C988E5385AA9D7277 * __this, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.AsyncTaskMethodBuilder`1<System.Boolean>::Start<System.Threading.SemaphoreSlim/<WaitUntilCountOrTimeoutAsync>d__31>(TStateMachine&)
inline void AsyncTaskMethodBuilder_1_Start_TisU3CWaitUntilCountOrTimeoutAsyncU3Ed__31_t227D1F5F19C948CA8C23C80B5F19147D4AAED14F_m67A42FEF4C09E843DDA99A81F84E675F9AB87CF9 (AsyncTaskMethodBuilder_1_tABE1DEF12F121D6FC8ABF04869ED964FF83EA9B5 * __this, U3CWaitUntilCountOrTimeoutAsyncU3Ed__31_t227D1F5F19C948CA8C23C80B5F19147D4AAED14F * ___stateMachine0, const RuntimeMethod* method)
{
	((  void (*) (AsyncTaskMethodBuilder_1_tABE1DEF12F121D6FC8ABF04869ED964FF83EA9B5 *, U3CWaitUntilCountOrTimeoutAsyncU3Ed__31_t227D1F5F19C948CA8C23C80B5F19147D4AAED14F *, const RuntimeMethod*))AsyncTaskMethodBuilder_1_Start_TisU3CWaitUntilCountOrTimeoutAsyncU3Ed__31_t227D1F5F19C948CA8C23C80B5F19147D4AAED14F_m67A42FEF4C09E843DDA99A81F84E675F9AB87CF9_gshared)(__this, ___stateMachine0, method);
}
// System.Threading.Tasks.Task`1<TResult> System.Runtime.CompilerServices.AsyncTaskMethodBuilder`1<System.Object>::get_Task()
inline Task_1_tC1805497876E88B78A2B0CB81C6409E0B381AC17 * AsyncTaskMethodBuilder_1_get_Task_m61DEC300353320E428E17DA0D59D61974F4415BB (AsyncTaskMethodBuilder_1_tDD2A3BA099C327938EA03C35FDB1A7502BA73020 * __this, const RuntimeMethod* method)
{
	return ((  Task_1_tC1805497876E88B78A2B0CB81C6409E0B381AC17 * (*) (AsyncTaskMethodBuilder_1_tDD2A3BA099C327938EA03C35FDB1A7502BA73020 *, const RuntimeMethod*))AsyncTaskMethodBuilder_1_get_Task_m61DEC300353320E428E17DA0D59D61974F4415BB_gshared)(__this, method);
}
// System.Void System.Runtime.CompilerServices.AsyncTaskMethodBuilder`1<System.Object>::AwaitUnsafeOnCompleted<System.Object,System.Object>(TAwaiter&,TStateMachine&)
inline void AsyncTaskMethodBuilder_1_AwaitUnsafeOnCompleted_TisRuntimeObject_TisRuntimeObject_mD7F0BC5F052B851265FF930DD5AB23C8E96269DE (AsyncTaskMethodBuilder_1_tDD2A3BA099C327938EA03C35FDB1A7502BA73020 * __this, RuntimeObject ** ___awaiter0, RuntimeObject ** ___stateMachine1, const RuntimeMethod* method)
{
	((  void (*) (AsyncTaskMethodBuilder_1_tDD2A3BA099C327938EA03C35FDB1A7502BA73020 *, RuntimeObject **, RuntimeObject **, const RuntimeMethod*))AsyncTaskMethodBuilder_1_AwaitUnsafeOnCompleted_TisRuntimeObject_TisRuntimeObject_mD7F0BC5F052B851265FF930DD5AB23C8E96269DE_gshared)(__this, ___awaiter0, ___stateMachine1, method);
}
// System.Void System.Runtime.CompilerServices.AsyncTaskMethodBuilder`1<System.Object>::Start<System.Object>(TStateMachine&)
inline void AsyncTaskMethodBuilder_1_Start_TisRuntimeObject_m25C57EA59EF179635A81530036C12082EE50878B (AsyncTaskMethodBuilder_1_tDD2A3BA099C327938EA03C35FDB1A7502BA73020 * __this, RuntimeObject ** ___stateMachine0, const RuntimeMethod* method)
{
	((  void (*) (AsyncTaskMethodBuilder_1_tDD2A3BA099C327938EA03C35FDB1A7502BA73020 *, RuntimeObject **, const RuntimeMethod*))AsyncTaskMethodBuilder_1_Start_TisRuntimeObject_m25C57EA59EF179635A81530036C12082EE50878B_gshared)(__this, ___stateMachine0, method);
}
// System.Int32 Unity.Collections.LowLevel.Unsafe.UnsafeUtility::SizeOf<Obi.Triangle>()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t UnsafeUtility_SizeOf_TisTriangle_t6EBD8067A5E92E374FBCEE8434826BC760D9FDC8_m267F7E203AEA1F96AA9A239A17ED622B27350167_gshared (const RuntimeMethod* method)
{
	{
		uint32_t L_0 = sizeof(Triangle_t6EBD8067A5E92E374FBCEE8434826BC760D9FDC8 );
		return (int32_t)L_0;
	}
}
// System.Int32 Unity.Collections.LowLevel.Unsafe.UnsafeUtility::SizeOf<Obi.TriangleMeshHeader>()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t UnsafeUtility_SizeOf_TisTriangleMeshHeader_tD4F431BF8B26AB40AEE8CC5AF9C045A565607267_m38A15DB106996335A821554322FA681B9CE9312F_gshared (const RuntimeMethod* method)
{
	{
		uint32_t L_0 = sizeof(TriangleMeshHeader_tD4F431BF8B26AB40AEE8CC5AF9C045A565607267 );
		return (int32_t)L_0;
	}
}
// System.Int32 Unity.Collections.LowLevel.Unsafe.UnsafeUtility::SizeOf<Obi.VInt4>()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t UnsafeUtility_SizeOf_TisVInt4_t7102225B83232A71C9D784846D42B69BF440AD5D_m88E1869C2FB310CFAA5DB2F3036E1971886A2199_gshared (const RuntimeMethod* method)
{
	{
		uint32_t L_0 = sizeof(VInt4_t7102225B83232A71C9D784846D42B69BF440AD5D );
		return (int32_t)L_0;
	}
}
// System.Int32 Unity.Collections.LowLevel.Unsafe.UnsafeUtility::SizeOf<UnityEngine.Vector2>()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t UnsafeUtility_SizeOf_TisVector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_mB0BA7AC549DE2A13AFD6C8FAF906DFABF03C148D_gshared (const RuntimeMethod* method)
{
	{
		uint32_t L_0 = sizeof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 );
		return (int32_t)L_0;
	}
}
// System.Int32 Unity.Collections.LowLevel.Unsafe.UnsafeUtility::SizeOf<UnityEngine.Vector3>()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t UnsafeUtility_SizeOf_TisVector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_m8508D752DDCACDDA57921C7B14C2ABAB9E07C134_gshared (const RuntimeMethod* method)
{
	{
		uint32_t L_0 = sizeof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E );
		return (int32_t)L_0;
	}
}
// System.Int32 Unity.Collections.LowLevel.Unsafe.UnsafeUtility::SizeOf<UnityEngine.Vector4>()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t UnsafeUtility_SizeOf_TisVector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7_mFA1DB8F130AAD9A5A0EAA97B9F2CCA33EFDF55FA_gshared (const RuntimeMethod* method)
{
	{
		uint32_t L_0 = sizeof(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7 );
		return (int32_t)L_0;
	}
}
// System.Int32 Unity.Collections.LowLevel.Unsafe.UnsafeUtility::SizeOf<Oni_Contact>()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t UnsafeUtility_SizeOf_TisContact_tA38E76CD5E9A53DE837B07BBB25CABE68B2AF756_mD7D4B63B5BEB074B60A5DE72988B65FC8EB58F84_gshared (const RuntimeMethod* method)
{
	{
		uint32_t L_0 = sizeof(Contact_tA38E76CD5E9A53DE837B07BBB25CABE68B2AF756 );
		return (int32_t)L_0;
	}
}
// System.Void Unity.Collections.LowLevel.Unsafe.UnsafeUtility::WriteArrayElement<Obi.Aabb>(System.Void*,System.Int32,T)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UnsafeUtility_WriteArrayElement_TisAabb_t4DF77A399A9E4CB5D52707E4B8B53D3483CD84CF_mE10D1D3C0BB44566894F23D176D3FF444215E787_gshared (void* ___destination0, int32_t ___index1, Aabb_t4DF77A399A9E4CB5D52707E4B8B53D3483CD84CF  ___value2, const RuntimeMethod* method)
{
	{
		void* L_0 = ___destination0;
		int32_t L_1 = ___index1;
		uint32_t L_2 = sizeof(Aabb_t4DF77A399A9E4CB5D52707E4B8B53D3483CD84CF );
		Aabb_t4DF77A399A9E4CB5D52707E4B8B53D3483CD84CF  L_3 = ___value2;
		*(Aabb_t4DF77A399A9E4CB5D52707E4B8B53D3483CD84CF *)((void*)il2cpp_codegen_add((intptr_t)L_0, (intptr_t)(((intptr_t)((int64_t)il2cpp_codegen_multiply((int64_t)(((int64_t)((int64_t)L_1))), (int64_t)(((int64_t)((int64_t)(((int32_t)((int32_t)L_2)))))))))))) = L_3;
		return;
	}
}
// System.Void Unity.Collections.LowLevel.Unsafe.UnsafeUtility::WriteArrayElement<Obi.AffineTransform>(System.Void*,System.Int32,T)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UnsafeUtility_WriteArrayElement_TisAffineTransform_tC1AED177DBAAED2A3AD2CCA40394AAA8C3DF0193_mFCC78116077978941684A97A87B2F3240D8B0657_gshared (void* ___destination0, int32_t ___index1, AffineTransform_tC1AED177DBAAED2A3AD2CCA40394AAA8C3DF0193  ___value2, const RuntimeMethod* method)
{
	{
		void* L_0 = ___destination0;
		int32_t L_1 = ___index1;
		uint32_t L_2 = sizeof(AffineTransform_tC1AED177DBAAED2A3AD2CCA40394AAA8C3DF0193 );
		AffineTransform_tC1AED177DBAAED2A3AD2CCA40394AAA8C3DF0193  L_3 = ___value2;
		*(AffineTransform_tC1AED177DBAAED2A3AD2CCA40394AAA8C3DF0193 *)((void*)il2cpp_codegen_add((intptr_t)L_0, (intptr_t)(((intptr_t)((int64_t)il2cpp_codegen_multiply((int64_t)(((int64_t)((int64_t)L_1))), (int64_t)(((int64_t)((int64_t)(((int32_t)((int32_t)L_2)))))))))))) = L_3;
		return;
	}
}
// System.Void Unity.Collections.LowLevel.Unsafe.UnsafeUtility::WriteArrayElement<Obi.BIHNode>(System.Void*,System.Int32,T)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UnsafeUtility_WriteArrayElement_TisBIHNode_tA8B64B5175479DEC9FBF8312EF5F2163C1FE0D05_m620E29B0710C0AF98F3D4B8722F8005F8F25C10A_gshared (void* ___destination0, int32_t ___index1, BIHNode_tA8B64B5175479DEC9FBF8312EF5F2163C1FE0D05  ___value2, const RuntimeMethod* method)
{
	{
		void* L_0 = ___destination0;
		int32_t L_1 = ___index1;
		uint32_t L_2 = sizeof(BIHNode_tA8B64B5175479DEC9FBF8312EF5F2163C1FE0D05 );
		BIHNode_tA8B64B5175479DEC9FBF8312EF5F2163C1FE0D05  L_3 = ___value2;
		*(BIHNode_tA8B64B5175479DEC9FBF8312EF5F2163C1FE0D05 *)((void*)il2cpp_codegen_add((intptr_t)L_0, (intptr_t)(((intptr_t)((int64_t)il2cpp_codegen_multiply((int64_t)(((int64_t)((int64_t)L_1))), (int64_t)(((int64_t)((int64_t)(((int32_t)((int32_t)L_2)))))))))))) = L_3;
		return;
	}
}
// System.Void Unity.Collections.LowLevel.Unsafe.UnsafeUtility::WriteArrayElement<UnityEngine.Rendering.BatchVisibility>(System.Void*,System.Int32,T)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UnsafeUtility_WriteArrayElement_TisBatchVisibility_tFA63D052426424FBD58F78E973AAAC52A67B5AFE_mC2658E4BCDD5CB7B35E546FAE518CF3C4102626E_gshared (void* ___destination0, int32_t ___index1, BatchVisibility_tFA63D052426424FBD58F78E973AAAC52A67B5AFE  ___value2, const RuntimeMethod* method)
{
	{
		void* L_0 = ___destination0;
		int32_t L_1 = ___index1;
		uint32_t L_2 = sizeof(BatchVisibility_tFA63D052426424FBD58F78E973AAAC52A67B5AFE );
		BatchVisibility_tFA63D052426424FBD58F78E973AAAC52A67B5AFE  L_3 = ___value2;
		*(BatchVisibility_tFA63D052426424FBD58F78E973AAAC52A67B5AFE *)((void*)il2cpp_codegen_add((intptr_t)L_0, (intptr_t)(((intptr_t)((int64_t)il2cpp_codegen_multiply((int64_t)(((int64_t)((int64_t)L_1))), (int64_t)(((int64_t)((int64_t)(((int32_t)((int32_t)L_2)))))))))))) = L_3;
		return;
	}
}
// System.Void Unity.Collections.LowLevel.Unsafe.UnsafeUtility::WriteArrayElement<System.Byte>(System.Void*,System.Int32,T)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UnsafeUtility_WriteArrayElement_TisByte_t0111FAB8B8685667EDDAF77683F0D8F86B659056_mB82F9E11B8FEC9E090AEA6C9C2C8A611682E7825_gshared (void* ___destination0, int32_t ___index1, uint8_t ___value2, const RuntimeMethod* method)
{
	{
		void* L_0 = ___destination0;
		int32_t L_1 = ___index1;
		uint32_t L_2 = sizeof(uint8_t);
		uint8_t L_3 = ___value2;
		*(uint8_t*)((void*)il2cpp_codegen_add((intptr_t)L_0, (intptr_t)(((intptr_t)((int64_t)il2cpp_codegen_multiply((int64_t)(((int64_t)((int64_t)L_1))), (int64_t)(((int64_t)((int64_t)(((int32_t)((int32_t)L_2)))))))))))) = L_3;
		return;
	}
}
// System.Void Unity.Collections.LowLevel.Unsafe.UnsafeUtility::WriteArrayElement<Obi.CellSpan>(System.Void*,System.Int32,T)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UnsafeUtility_WriteArrayElement_TisCellSpan_tE5A1247201F556E0880AD919A58F92DEA8402291_mC8FB687DB97B1C1D2EAB362CF7C9EFD29D1C397A_gshared (void* ___destination0, int32_t ___index1, CellSpan_tE5A1247201F556E0880AD919A58F92DEA8402291  ___value2, const RuntimeMethod* method)
{
	{
		void* L_0 = ___destination0;
		int32_t L_1 = ___index1;
		uint32_t L_2 = sizeof(CellSpan_tE5A1247201F556E0880AD919A58F92DEA8402291 );
		CellSpan_tE5A1247201F556E0880AD919A58F92DEA8402291  L_3 = ___value2;
		*(CellSpan_tE5A1247201F556E0880AD919A58F92DEA8402291 *)((void*)il2cpp_codegen_add((intptr_t)L_0, (intptr_t)(((intptr_t)((int64_t)il2cpp_codegen_multiply((int64_t)(((int64_t)((int64_t)L_1))), (int64_t)(((int64_t)((int64_t)(((int32_t)((int32_t)L_2)))))))))))) = L_3;
		return;
	}
}
// System.Void Unity.Collections.LowLevel.Unsafe.UnsafeUtility::WriteArrayElement<Obi.ColliderRigidbody>(System.Void*,System.Int32,T)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UnsafeUtility_WriteArrayElement_TisColliderRigidbody_tC1FF76AA900641BCDF2B31E90931360DA75EAA0D_m11D85177DAC6B349B997D5743AC230553EF6BF9D_gshared (void* ___destination0, int32_t ___index1, ColliderRigidbody_tC1FF76AA900641BCDF2B31E90931360DA75EAA0D  ___value2, const RuntimeMethod* method)
{
	{
		void* L_0 = ___destination0;
		int32_t L_1 = ___index1;
		uint32_t L_2 = sizeof(ColliderRigidbody_tC1FF76AA900641BCDF2B31E90931360DA75EAA0D );
		ColliderRigidbody_tC1FF76AA900641BCDF2B31E90931360DA75EAA0D  L_3 = ___value2;
		*(ColliderRigidbody_tC1FF76AA900641BCDF2B31E90931360DA75EAA0D *)((void*)il2cpp_codegen_add((intptr_t)L_0, (intptr_t)(((intptr_t)((int64_t)il2cpp_codegen_multiply((int64_t)(((int64_t)((int64_t)L_1))), (int64_t)(((int64_t)((int64_t)(((int32_t)((int32_t)L_2)))))))))))) = L_3;
		return;
	}
}
// System.Void Unity.Collections.LowLevel.Unsafe.UnsafeUtility::WriteArrayElement<Obi.ColliderShape>(System.Void*,System.Int32,T)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UnsafeUtility_WriteArrayElement_TisColliderShape_t7AF5542901AEB2CFA54B6BFD8C852EC91554F586_mD0027F75E7961236DBAD407E805030D60284304C_gshared (void* ___destination0, int32_t ___index1, ColliderShape_t7AF5542901AEB2CFA54B6BFD8C852EC91554F586  ___value2, const RuntimeMethod* method)
{
	{
		void* L_0 = ___destination0;
		int32_t L_1 = ___index1;
		uint32_t L_2 = sizeof(ColliderShape_t7AF5542901AEB2CFA54B6BFD8C852EC91554F586 );
		ColliderShape_t7AF5542901AEB2CFA54B6BFD8C852EC91554F586  L_3 = ___value2;
		*(ColliderShape_t7AF5542901AEB2CFA54B6BFD8C852EC91554F586 *)((void*)il2cpp_codegen_add((intptr_t)L_0, (intptr_t)(((intptr_t)((int64_t)il2cpp_codegen_multiply((int64_t)(((int64_t)((int64_t)L_1))), (int64_t)(((int64_t)((int64_t)(((int32_t)((int32_t)L_2)))))))))))) = L_3;
		return;
	}
}
// System.Void Unity.Collections.LowLevel.Unsafe.UnsafeUtility::WriteArrayElement<Obi.CollisionMaterial>(System.Void*,System.Int32,T)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UnsafeUtility_WriteArrayElement_TisCollisionMaterial_t7582B504E28F3D7E580DBB66ECD74F967C92E758_m4459048CFD4708BD559A873A3BB9DF3394CEC656_gshared (void* ___destination0, int32_t ___index1, CollisionMaterial_t7582B504E28F3D7E580DBB66ECD74F967C92E758  ___value2, const RuntimeMethod* method)
{
	{
		void* L_0 = ___destination0;
		int32_t L_1 = ___index1;
		uint32_t L_2 = sizeof(CollisionMaterial_t7582B504E28F3D7E580DBB66ECD74F967C92E758 );
		CollisionMaterial_t7582B504E28F3D7E580DBB66ECD74F967C92E758  L_3 = ___value2;
		*(CollisionMaterial_t7582B504E28F3D7E580DBB66ECD74F967C92E758 *)((void*)il2cpp_codegen_add((intptr_t)L_0, (intptr_t)(((intptr_t)((int64_t)il2cpp_codegen_multiply((int64_t)(((int64_t)((int64_t)L_1))), (int64_t)(((int64_t)((int64_t)(((int32_t)((int32_t)L_2)))))))))))) = L_3;
		return;
	}
}
// System.Void Unity.Collections.LowLevel.Unsafe.UnsafeUtility::WriteArrayElement<Obi.DFNode>(System.Void*,System.Int32,T)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UnsafeUtility_WriteArrayElement_TisDFNode_t8FC489FB11B708202922AF5F52E4AFEB7B205668_m336BC036E38143A2AAA05BFE5AACE73491EA1D46_gshared (void* ___destination0, int32_t ___index1, DFNode_t8FC489FB11B708202922AF5F52E4AFEB7B205668  ___value2, const RuntimeMethod* method)
{
	{
		void* L_0 = ___destination0;
		int32_t L_1 = ___index1;
		uint32_t L_2 = sizeof(DFNode_t8FC489FB11B708202922AF5F52E4AFEB7B205668 );
		DFNode_t8FC489FB11B708202922AF5F52E4AFEB7B205668  L_3 = ___value2;
		*(DFNode_t8FC489FB11B708202922AF5F52E4AFEB7B205668 *)((void*)il2cpp_codegen_add((intptr_t)L_0, (intptr_t)(((intptr_t)((int64_t)il2cpp_codegen_multiply((int64_t)(((int64_t)((int64_t)L_1))), (int64_t)(((int64_t)((int64_t)(((int32_t)((int32_t)L_2)))))))))))) = L_3;
		return;
	}
}
// System.Void Unity.Collections.LowLevel.Unsafe.UnsafeUtility::WriteArrayElement<Obi.DistanceFieldHeader>(System.Void*,System.Int32,T)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UnsafeUtility_WriteArrayElement_TisDistanceFieldHeader_tE66F34D3B1EDB411B0D6E1346D68383E03E33AD4_m5C4E1B0DF5923982E09CF0DCFA2E7E96D8DBE5B9_gshared (void* ___destination0, int32_t ___index1, DistanceFieldHeader_tE66F34D3B1EDB411B0D6E1346D68383E03E33AD4  ___value2, const RuntimeMethod* method)
{
	{
		void* L_0 = ___destination0;
		int32_t L_1 = ___index1;
		uint32_t L_2 = sizeof(DistanceFieldHeader_tE66F34D3B1EDB411B0D6E1346D68383E03E33AD4 );
		DistanceFieldHeader_tE66F34D3B1EDB411B0D6E1346D68383E03E33AD4  L_3 = ___value2;
		*(DistanceFieldHeader_tE66F34D3B1EDB411B0D6E1346D68383E03E33AD4 *)((void*)il2cpp_codegen_add((intptr_t)L_0, (intptr_t)(((intptr_t)((int64_t)il2cpp_codegen_multiply((int64_t)(((int64_t)((int64_t)L_1))), (int64_t)(((int64_t)((int64_t)(((int32_t)((int32_t)L_2)))))))))))) = L_3;
		return;
	}
}
// System.Void Unity.Collections.LowLevel.Unsafe.UnsafeUtility::WriteArrayElement<Obi.Edge>(System.Void*,System.Int32,T)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UnsafeUtility_WriteArrayElement_TisEdge_tB72B5CE4AD9C11C74696DF37DCED8FB9C2FFDD3C_m275D99AA7F44E8C5A7A43D02AFFF26ED5A82B954_gshared (void* ___destination0, int32_t ___index1, Edge_tB72B5CE4AD9C11C74696DF37DCED8FB9C2FFDD3C  ___value2, const RuntimeMethod* method)
{
	{
		void* L_0 = ___destination0;
		int32_t L_1 = ___index1;
		uint32_t L_2 = sizeof(Edge_tB72B5CE4AD9C11C74696DF37DCED8FB9C2FFDD3C );
		Edge_tB72B5CE4AD9C11C74696DF37DCED8FB9C2FFDD3C  L_3 = ___value2;
		*(Edge_tB72B5CE4AD9C11C74696DF37DCED8FB9C2FFDD3C *)((void*)il2cpp_codegen_add((intptr_t)L_0, (intptr_t)(((intptr_t)((int64_t)il2cpp_codegen_multiply((int64_t)(((int64_t)((int64_t)L_1))), (int64_t)(((int64_t)((int64_t)(((int32_t)((int32_t)L_2)))))))))))) = L_3;
		return;
	}
}
// System.Void Unity.Collections.LowLevel.Unsafe.UnsafeUtility::WriteArrayElement<Obi.EdgeMeshHeader>(System.Void*,System.Int32,T)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UnsafeUtility_WriteArrayElement_TisEdgeMeshHeader_t235B26AE87E4F8BC38EC627430A9E0E344A03878_m1E423F65E3816E88307046AE36A70A421E37CC14_gshared (void* ___destination0, int32_t ___index1, EdgeMeshHeader_t235B26AE87E4F8BC38EC627430A9E0E344A03878  ___value2, const RuntimeMethod* method)
{
	{
		void* L_0 = ___destination0;
		int32_t L_1 = ___index1;
		uint32_t L_2 = sizeof(EdgeMeshHeader_t235B26AE87E4F8BC38EC627430A9E0E344A03878 );
		EdgeMeshHeader_t235B26AE87E4F8BC38EC627430A9E0E344A03878  L_3 = ___value2;
		*(EdgeMeshHeader_t235B26AE87E4F8BC38EC627430A9E0E344A03878 *)((void*)il2cpp_codegen_add((intptr_t)L_0, (intptr_t)(((intptr_t)((int64_t)il2cpp_codegen_multiply((int64_t)(((int64_t)((int64_t)L_1))), (int64_t)(((int64_t)((int64_t)(((int32_t)((int32_t)L_2)))))))))))) = L_3;
		return;
	}
}
// System.Void Unity.Collections.LowLevel.Unsafe.UnsafeUtility::WriteArrayElement<Obi.HeightFieldHeader>(System.Void*,System.Int32,T)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UnsafeUtility_WriteArrayElement_TisHeightFieldHeader_t10BB39F6CE13CD0FBDC200A25AFD4000062D7E4E_m2EECDD5759B748435B781D1EA8AB43CF17F7D2D2_gshared (void* ___destination0, int32_t ___index1, HeightFieldHeader_t10BB39F6CE13CD0FBDC200A25AFD4000062D7E4E  ___value2, const RuntimeMethod* method)
{
	{
		void* L_0 = ___destination0;
		int32_t L_1 = ___index1;
		uint32_t L_2 = sizeof(HeightFieldHeader_t10BB39F6CE13CD0FBDC200A25AFD4000062D7E4E );
		HeightFieldHeader_t10BB39F6CE13CD0FBDC200A25AFD4000062D7E4E  L_3 = ___value2;
		*(HeightFieldHeader_t10BB39F6CE13CD0FBDC200A25AFD4000062D7E4E *)((void*)il2cpp_codegen_add((intptr_t)L_0, (intptr_t)(((intptr_t)((int64_t)il2cpp_codegen_multiply((int64_t)(((int64_t)((int64_t)L_1))), (int64_t)(((int64_t)((int64_t)(((int32_t)((int32_t)L_2)))))))))))) = L_3;
		return;
	}
}
// System.Void Unity.Collections.LowLevel.Unsafe.UnsafeUtility::WriteArrayElement<System.Int32>(System.Void*,System.Int32,T)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UnsafeUtility_WriteArrayElement_TisInt32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046_m7710B9BDDE46DE6C314C3D8D60C4F93C0BD57392_gshared (void* ___destination0, int32_t ___index1, int32_t ___value2, const RuntimeMethod* method)
{
	{
		void* L_0 = ___destination0;
		int32_t L_1 = ___index1;
		uint32_t L_2 = sizeof(int32_t);
		int32_t L_3 = ___value2;
		*(int32_t*)((void*)il2cpp_codegen_add((intptr_t)L_0, (intptr_t)(((intptr_t)((int64_t)il2cpp_codegen_multiply((int64_t)(((int64_t)((int64_t)L_1))), (int64_t)(((int64_t)((int64_t)(((int32_t)((int32_t)L_2)))))))))))) = L_3;
		return;
	}
}
// System.Void Unity.Collections.LowLevel.Unsafe.UnsafeUtility::WriteArrayElement<System.IntPtr>(System.Void*,System.Int32,T)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UnsafeUtility_WriteArrayElement_TisIntPtr_t_m3F997539355BE915FDF43AA69A9E7E27294C7EC9_gshared (void* ___destination0, int32_t ___index1, intptr_t ___value2, const RuntimeMethod* method)
{
	{
		void* L_0 = ___destination0;
		int32_t L_1 = ___index1;
		uint32_t L_2 = sizeof(intptr_t);
		intptr_t L_3 = ___value2;
		*(intptr_t*)((void*)il2cpp_codegen_add((intptr_t)L_0, (intptr_t)(((intptr_t)((int64_t)il2cpp_codegen_multiply((int64_t)(((int64_t)((int64_t)L_1))), (int64_t)(((int64_t)((int64_t)(((int32_t)((int32_t)L_2)))))))))))) = L_3;
		return;
	}
}
// System.Void Unity.Collections.LowLevel.Unsafe.UnsafeUtility::WriteArrayElement<UnityEngine.Experimental.GlobalIllumination.LightDataGI>(System.Void*,System.Int32,T)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UnsafeUtility_WriteArrayElement_TisLightDataGI_t0C34AB69E4E96717FD276B35116C798A641D44F2_mB7E008D6D4C2008C47D4D71528F6AE7AFCF0B7CB_gshared (void* ___destination0, int32_t ___index1, LightDataGI_t0C34AB69E4E96717FD276B35116C798A641D44F2  ___value2, const RuntimeMethod* method)
{
	{
		void* L_0 = ___destination0;
		int32_t L_1 = ___index1;
		uint32_t L_2 = sizeof(LightDataGI_t0C34AB69E4E96717FD276B35116C798A641D44F2 );
		LightDataGI_t0C34AB69E4E96717FD276B35116C798A641D44F2  L_3 = ___value2;
		*(LightDataGI_t0C34AB69E4E96717FD276B35116C798A641D44F2 *)((void*)il2cpp_codegen_add((intptr_t)L_0, (intptr_t)(((intptr_t)((int64_t)il2cpp_codegen_multiply((int64_t)(((int64_t)((int64_t)L_1))), (int64_t)(((int64_t)((int64_t)(((int32_t)((int32_t)L_2)))))))))))) = L_3;
		return;
	}
}
// System.Void Unity.Collections.LowLevel.Unsafe.UnsafeUtility::WriteArrayElement<UnityEngine.Matrix4x4>(System.Void*,System.Int32,T)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UnsafeUtility_WriteArrayElement_TisMatrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461_m343AC83798ED7FC9DE305074D32AED7226937333_gshared (void* ___destination0, int32_t ___index1, Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461  ___value2, const RuntimeMethod* method)
{
	{
		void* L_0 = ___destination0;
		int32_t L_1 = ___index1;
		uint32_t L_2 = sizeof(Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461 );
		Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461  L_3 = ___value2;
		*(Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461 *)((void*)il2cpp_codegen_add((intptr_t)L_0, (intptr_t)(((intptr_t)((int64_t)il2cpp_codegen_multiply((int64_t)(((int64_t)((int64_t)L_1))), (int64_t)(((int64_t)((int64_t)(((int32_t)((int32_t)L_2)))))))))))) = L_3;
		return;
	}
}
// System.Void Unity.Collections.LowLevel.Unsafe.UnsafeUtility::WriteArrayElement<UnityEngine.Plane>(System.Void*,System.Int32,T)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UnsafeUtility_WriteArrayElement_TisPlane_t80844BF2332EAFC1DDEDD616A950242031A115C7_mB75E0A57B03F72BC1744169D7F4F8CF73AD98915_gshared (void* ___destination0, int32_t ___index1, Plane_t80844BF2332EAFC1DDEDD616A950242031A115C7  ___value2, const RuntimeMethod* method)
{
	{
		void* L_0 = ___destination0;
		int32_t L_1 = ___index1;
		uint32_t L_2 = sizeof(Plane_t80844BF2332EAFC1DDEDD616A950242031A115C7 );
		Plane_t80844BF2332EAFC1DDEDD616A950242031A115C7  L_3 = ___value2;
		*(Plane_t80844BF2332EAFC1DDEDD616A950242031A115C7 *)((void*)il2cpp_codegen_add((intptr_t)L_0, (intptr_t)(((intptr_t)((int64_t)il2cpp_codegen_multiply((int64_t)(((int64_t)((int64_t)L_1))), (int64_t)(((int64_t)((int64_t)(((int32_t)((int32_t)L_2)))))))))))) = L_3;
		return;
	}
}
// System.Void Unity.Collections.LowLevel.Unsafe.UnsafeUtility::WriteArrayElement<UnityEngine.Quaternion>(System.Void*,System.Int32,T)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UnsafeUtility_WriteArrayElement_TisQuaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4_mFEA1EE338AAC863BEA2AF2134D3D22ED93E013BB_gshared (void* ___destination0, int32_t ___index1, Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  ___value2, const RuntimeMethod* method)
{
	{
		void* L_0 = ___destination0;
		int32_t L_1 = ___index1;
		uint32_t L_2 = sizeof(Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4 );
		Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  L_3 = ___value2;
		*(Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4 *)((void*)il2cpp_codegen_add((intptr_t)L_0, (intptr_t)(((intptr_t)((int64_t)il2cpp_codegen_multiply((int64_t)(((int64_t)((int64_t)L_1))), (int64_t)(((int64_t)((int64_t)(((int32_t)((int32_t)L_2)))))))))))) = L_3;
		return;
	}
}
// System.Void Unity.Collections.LowLevel.Unsafe.UnsafeUtility::WriteArrayElement<Obi.QueryResult>(System.Void*,System.Int32,T)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UnsafeUtility_WriteArrayElement_TisQueryResult_t4DCEE89279D61183A30969C97F725AA603AD118C_mFF6E4D281F42634EB168B5B074B3F270AB378E2E_gshared (void* ___destination0, int32_t ___index1, QueryResult_t4DCEE89279D61183A30969C97F725AA603AD118C  ___value2, const RuntimeMethod* method)
{
	{
		void* L_0 = ___destination0;
		int32_t L_1 = ___index1;
		uint32_t L_2 = sizeof(QueryResult_t4DCEE89279D61183A30969C97F725AA603AD118C );
		QueryResult_t4DCEE89279D61183A30969C97F725AA603AD118C  L_3 = ___value2;
		*(QueryResult_t4DCEE89279D61183A30969C97F725AA603AD118C *)((void*)il2cpp_codegen_add((intptr_t)L_0, (intptr_t)(((intptr_t)((int64_t)il2cpp_codegen_multiply((int64_t)(((int64_t)((int64_t)L_1))), (int64_t)(((int64_t)((int64_t)(((int32_t)((int32_t)L_2)))))))))))) = L_3;
		return;
	}
}
// System.Void Unity.Collections.LowLevel.Unsafe.UnsafeUtility::WriteArrayElement<Obi.QueryShape>(System.Void*,System.Int32,T)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UnsafeUtility_WriteArrayElement_TisQueryShape_t0A83A6359C90B5213CDEC851B226F8DAAD6D07AF_m921AF38879388653BE45D68E74169C9D477DCB8A_gshared (void* ___destination0, int32_t ___index1, QueryShape_t0A83A6359C90B5213CDEC851B226F8DAAD6D07AF  ___value2, const RuntimeMethod* method)
{
	{
		void* L_0 = ___destination0;
		int32_t L_1 = ___index1;
		uint32_t L_2 = sizeof(QueryShape_t0A83A6359C90B5213CDEC851B226F8DAAD6D07AF );
		QueryShape_t0A83A6359C90B5213CDEC851B226F8DAAD6D07AF  L_3 = ___value2;
		*(QueryShape_t0A83A6359C90B5213CDEC851B226F8DAAD6D07AF *)((void*)il2cpp_codegen_add((intptr_t)L_0, (intptr_t)(((intptr_t)((int64_t)il2cpp_codegen_multiply((int64_t)(((int64_t)((int64_t)L_1))), (int64_t)(((int64_t)((int64_t)(((int32_t)((int32_t)L_2)))))))))))) = L_3;
		return;
	}
}
// System.Void Unity.Collections.LowLevel.Unsafe.UnsafeUtility::WriteArrayElement<System.Single>(System.Void*,System.Int32,T)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UnsafeUtility_WriteArrayElement_TisSingle_tE07797BA3C98D4CA9B5A19413C19A76688AB899E_m969E836808A695805FE4C069301FD551A8F1B01E_gshared (void* ___destination0, int32_t ___index1, float ___value2, const RuntimeMethod* method)
{
	{
		void* L_0 = ___destination0;
		int32_t L_1 = ___index1;
		uint32_t L_2 = sizeof(float);
		float L_3 = ___value2;
		*(float*)((void*)il2cpp_codegen_add((intptr_t)L_0, (intptr_t)(((intptr_t)((int64_t)il2cpp_codegen_multiply((int64_t)(((int64_t)((int64_t)L_1))), (int64_t)(((int64_t)((int64_t)(((int32_t)((int32_t)L_2)))))))))))) = L_3;
		return;
	}
}
// System.Void Unity.Collections.LowLevel.Unsafe.UnsafeUtility::WriteArrayElement<Obi.Triangle>(System.Void*,System.Int32,T)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UnsafeUtility_WriteArrayElement_TisTriangle_t6EBD8067A5E92E374FBCEE8434826BC760D9FDC8_mA9EB2FC0E5606B00955896ECD34CA15BADC106A1_gshared (void* ___destination0, int32_t ___index1, Triangle_t6EBD8067A5E92E374FBCEE8434826BC760D9FDC8  ___value2, const RuntimeMethod* method)
{
	{
		void* L_0 = ___destination0;
		int32_t L_1 = ___index1;
		uint32_t L_2 = sizeof(Triangle_t6EBD8067A5E92E374FBCEE8434826BC760D9FDC8 );
		Triangle_t6EBD8067A5E92E374FBCEE8434826BC760D9FDC8  L_3 = ___value2;
		*(Triangle_t6EBD8067A5E92E374FBCEE8434826BC760D9FDC8 *)((void*)il2cpp_codegen_add((intptr_t)L_0, (intptr_t)(((intptr_t)((int64_t)il2cpp_codegen_multiply((int64_t)(((int64_t)((int64_t)L_1))), (int64_t)(((int64_t)((int64_t)(((int32_t)((int32_t)L_2)))))))))))) = L_3;
		return;
	}
}
// System.Void Unity.Collections.LowLevel.Unsafe.UnsafeUtility::WriteArrayElement<Obi.TriangleMeshHeader>(System.Void*,System.Int32,T)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UnsafeUtility_WriteArrayElement_TisTriangleMeshHeader_tD4F431BF8B26AB40AEE8CC5AF9C045A565607267_mCCE6D3D1A7327DC19F7CE29CEBF30C28120A9618_gshared (void* ___destination0, int32_t ___index1, TriangleMeshHeader_tD4F431BF8B26AB40AEE8CC5AF9C045A565607267  ___value2, const RuntimeMethod* method)
{
	{
		void* L_0 = ___destination0;
		int32_t L_1 = ___index1;
		uint32_t L_2 = sizeof(TriangleMeshHeader_tD4F431BF8B26AB40AEE8CC5AF9C045A565607267 );
		TriangleMeshHeader_tD4F431BF8B26AB40AEE8CC5AF9C045A565607267  L_3 = ___value2;
		*(TriangleMeshHeader_tD4F431BF8B26AB40AEE8CC5AF9C045A565607267 *)((void*)il2cpp_codegen_add((intptr_t)L_0, (intptr_t)(((intptr_t)((int64_t)il2cpp_codegen_multiply((int64_t)(((int64_t)((int64_t)L_1))), (int64_t)(((int64_t)((int64_t)(((int32_t)((int32_t)L_2)))))))))))) = L_3;
		return;
	}
}
// System.Void Unity.Collections.LowLevel.Unsafe.UnsafeUtility::WriteArrayElement<Obi.VInt4>(System.Void*,System.Int32,T)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UnsafeUtility_WriteArrayElement_TisVInt4_t7102225B83232A71C9D784846D42B69BF440AD5D_m070DADF6F0F40A9DE1AEBE3A03AF64CD0D13254D_gshared (void* ___destination0, int32_t ___index1, VInt4_t7102225B83232A71C9D784846D42B69BF440AD5D  ___value2, const RuntimeMethod* method)
{
	{
		void* L_0 = ___destination0;
		int32_t L_1 = ___index1;
		uint32_t L_2 = sizeof(VInt4_t7102225B83232A71C9D784846D42B69BF440AD5D );
		VInt4_t7102225B83232A71C9D784846D42B69BF440AD5D  L_3 = ___value2;
		*(VInt4_t7102225B83232A71C9D784846D42B69BF440AD5D *)((void*)il2cpp_codegen_add((intptr_t)L_0, (intptr_t)(((intptr_t)((int64_t)il2cpp_codegen_multiply((int64_t)(((int64_t)((int64_t)L_1))), (int64_t)(((int64_t)((int64_t)(((int32_t)((int32_t)L_2)))))))))))) = L_3;
		return;
	}
}
// System.Void Unity.Collections.LowLevel.Unsafe.UnsafeUtility::WriteArrayElement<UnityEngine.Vector2>(System.Void*,System.Int32,T)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UnsafeUtility_WriteArrayElement_TisVector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_mBFF73AAEB90319D383F131CB86FCA21CF7D1B7F2_gshared (void* ___destination0, int32_t ___index1, Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___value2, const RuntimeMethod* method)
{
	{
		void* L_0 = ___destination0;
		int32_t L_1 = ___index1;
		uint32_t L_2 = sizeof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 );
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_3 = ___value2;
		*(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 *)((void*)il2cpp_codegen_add((intptr_t)L_0, (intptr_t)(((intptr_t)((int64_t)il2cpp_codegen_multiply((int64_t)(((int64_t)((int64_t)L_1))), (int64_t)(((int64_t)((int64_t)(((int32_t)((int32_t)L_2)))))))))))) = L_3;
		return;
	}
}
// System.Void Unity.Collections.LowLevel.Unsafe.UnsafeUtility::WriteArrayElement<UnityEngine.Vector3>(System.Void*,System.Int32,T)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UnsafeUtility_WriteArrayElement_TisVector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_m0236C8B08D58538865ACC8B7EE0340EBD16B4C28_gshared (void* ___destination0, int32_t ___index1, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___value2, const RuntimeMethod* method)
{
	{
		void* L_0 = ___destination0;
		int32_t L_1 = ___index1;
		uint32_t L_2 = sizeof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E );
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_3 = ___value2;
		*(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E *)((void*)il2cpp_codegen_add((intptr_t)L_0, (intptr_t)(((intptr_t)((int64_t)il2cpp_codegen_multiply((int64_t)(((int64_t)((int64_t)L_1))), (int64_t)(((int64_t)((int64_t)(((int32_t)((int32_t)L_2)))))))))))) = L_3;
		return;
	}
}
// System.Void Unity.Collections.LowLevel.Unsafe.UnsafeUtility::WriteArrayElement<UnityEngine.Vector4>(System.Void*,System.Int32,T)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UnsafeUtility_WriteArrayElement_TisVector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7_mA54583EE7B17CB1CE1F1E15C0E4BF37FD832DAED_gshared (void* ___destination0, int32_t ___index1, Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  ___value2, const RuntimeMethod* method)
{
	{
		void* L_0 = ___destination0;
		int32_t L_1 = ___index1;
		uint32_t L_2 = sizeof(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7 );
		Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  L_3 = ___value2;
		*(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7 *)((void*)il2cpp_codegen_add((intptr_t)L_0, (intptr_t)(((intptr_t)((int64_t)il2cpp_codegen_multiply((int64_t)(((int64_t)((int64_t)L_1))), (int64_t)(((int64_t)((int64_t)(((int32_t)((int32_t)L_2)))))))))))) = L_3;
		return;
	}
}
// System.Void Unity.Collections.LowLevel.Unsafe.UnsafeUtility::WriteArrayElement<Oni_Contact>(System.Void*,System.Int32,T)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UnsafeUtility_WriteArrayElement_TisContact_tA38E76CD5E9A53DE837B07BBB25CABE68B2AF756_mC0EEE0D0E2EC1C07A2E8D4BE510ED1167E01608C_gshared (void* ___destination0, int32_t ___index1, Contact_tA38E76CD5E9A53DE837B07BBB25CABE68B2AF756  ___value2, const RuntimeMethod* method)
{
	{
		void* L_0 = ___destination0;
		int32_t L_1 = ___index1;
		uint32_t L_2 = sizeof(Contact_tA38E76CD5E9A53DE837B07BBB25CABE68B2AF756 );
		Contact_tA38E76CD5E9A53DE837B07BBB25CABE68B2AF756  L_3 = ___value2;
		*(Contact_tA38E76CD5E9A53DE837B07BBB25CABE68B2AF756 *)((void*)il2cpp_codegen_add((intptr_t)L_0, (intptr_t)(((intptr_t)((int64_t)il2cpp_codegen_multiply((int64_t)(((int64_t)((int64_t)L_1))), (int64_t)(((int64_t)((int64_t)(((int32_t)((int32_t)L_2)))))))))))) = L_3;
		return;
	}
}
// System.Void Unity.Collections.LowLevel.Unsafe.UnsafeUtility::WriteArrayElement<System.Object>(System.Void*,System.Int32,T)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UnsafeUtility_WriteArrayElement_TisRuntimeObject_mA6478546A0D6B20061E1DAF0862A895CF5D8C044_gshared (void* ___destination0, int32_t ___index1, RuntimeObject * ___value2, const RuntimeMethod* method)
{
	{
		void* L_0 = ___destination0;
		int32_t L_1 = ___index1;
		uint32_t L_2 = sizeof(RuntimeObject *);
		RuntimeObject * L_3 = ___value2;
		*(RuntimeObject **)((void*)il2cpp_codegen_add((intptr_t)L_0, (intptr_t)(((intptr_t)((int64_t)il2cpp_codegen_multiply((int64_t)(((int64_t)((int64_t)L_1))), (int64_t)(((int64_t)((int64_t)(((int32_t)((int32_t)L_2)))))))))))) = L_3;
		Il2CppCodeGenWriteBarrier((void**)(RuntimeObject **)((void*)il2cpp_codegen_add((intptr_t)L_0, (intptr_t)(((intptr_t)((int64_t)il2cpp_codegen_multiply((int64_t)(((int64_t)((int64_t)L_1))), (int64_t)(((int64_t)((int64_t)(((int32_t)((int32_t)L_2)))))))))))), (void*)L_3);
		return;
	}
}
// System.Void Unity.Collections.LowLevel.Unsafe.UnsafeUtility::WriteArrayElementWithStride<Obi.Aabb>(System.Void*,System.Int32,System.Int32,T)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UnsafeUtility_WriteArrayElementWithStride_TisAabb_t4DF77A399A9E4CB5D52707E4B8B53D3483CD84CF_mF8391700E21C319C584AA384E3F5CB2CC5A8764E_gshared (void* ___destination0, int32_t ___index1, int32_t ___stride2, Aabb_t4DF77A399A9E4CB5D52707E4B8B53D3483CD84CF  ___value3, const RuntimeMethod* method)
{
	{
		void* L_0 = ___destination0;
		int32_t L_1 = ___index1;
		int32_t L_2 = ___stride2;
		Aabb_t4DF77A399A9E4CB5D52707E4B8B53D3483CD84CF  L_3 = ___value3;
		*(Aabb_t4DF77A399A9E4CB5D52707E4B8B53D3483CD84CF *)((void*)il2cpp_codegen_add((intptr_t)L_0, (intptr_t)(((intptr_t)((int64_t)il2cpp_codegen_multiply((int64_t)(((int64_t)((int64_t)L_1))), (int64_t)(((int64_t)((int64_t)L_2))))))))) = L_3;
		return;
	}
}
// System.Void Unity.Collections.LowLevel.Unsafe.UnsafeUtility::WriteArrayElementWithStride<Obi.AffineTransform>(System.Void*,System.Int32,System.Int32,T)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UnsafeUtility_WriteArrayElementWithStride_TisAffineTransform_tC1AED177DBAAED2A3AD2CCA40394AAA8C3DF0193_m1B1397B3973EAD98663B603F4C1763265DE2C4E3_gshared (void* ___destination0, int32_t ___index1, int32_t ___stride2, AffineTransform_tC1AED177DBAAED2A3AD2CCA40394AAA8C3DF0193  ___value3, const RuntimeMethod* method)
{
	{
		void* L_0 = ___destination0;
		int32_t L_1 = ___index1;
		int32_t L_2 = ___stride2;
		AffineTransform_tC1AED177DBAAED2A3AD2CCA40394AAA8C3DF0193  L_3 = ___value3;
		*(AffineTransform_tC1AED177DBAAED2A3AD2CCA40394AAA8C3DF0193 *)((void*)il2cpp_codegen_add((intptr_t)L_0, (intptr_t)(((intptr_t)((int64_t)il2cpp_codegen_multiply((int64_t)(((int64_t)((int64_t)L_1))), (int64_t)(((int64_t)((int64_t)L_2))))))))) = L_3;
		return;
	}
}
// System.Void Unity.Collections.LowLevel.Unsafe.UnsafeUtility::WriteArrayElementWithStride<Obi.BIHNode>(System.Void*,System.Int32,System.Int32,T)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UnsafeUtility_WriteArrayElementWithStride_TisBIHNode_tA8B64B5175479DEC9FBF8312EF5F2163C1FE0D05_m0D5B525EA5209E598AB6FB7F3E15663F59CAA20A_gshared (void* ___destination0, int32_t ___index1, int32_t ___stride2, BIHNode_tA8B64B5175479DEC9FBF8312EF5F2163C1FE0D05  ___value3, const RuntimeMethod* method)
{
	{
		void* L_0 = ___destination0;
		int32_t L_1 = ___index1;
		int32_t L_2 = ___stride2;
		BIHNode_tA8B64B5175479DEC9FBF8312EF5F2163C1FE0D05  L_3 = ___value3;
		*(BIHNode_tA8B64B5175479DEC9FBF8312EF5F2163C1FE0D05 *)((void*)il2cpp_codegen_add((intptr_t)L_0, (intptr_t)(((intptr_t)((int64_t)il2cpp_codegen_multiply((int64_t)(((int64_t)((int64_t)L_1))), (int64_t)(((int64_t)((int64_t)L_2))))))))) = L_3;
		return;
	}
}
// System.Void Unity.Collections.LowLevel.Unsafe.UnsafeUtility::WriteArrayElementWithStride<Obi.CellSpan>(System.Void*,System.Int32,System.Int32,T)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UnsafeUtility_WriteArrayElementWithStride_TisCellSpan_tE5A1247201F556E0880AD919A58F92DEA8402291_m09B80C0FF66699E5330C5FCFB93AFBAA2018A14D_gshared (void* ___destination0, int32_t ___index1, int32_t ___stride2, CellSpan_tE5A1247201F556E0880AD919A58F92DEA8402291  ___value3, const RuntimeMethod* method)
{
	{
		void* L_0 = ___destination0;
		int32_t L_1 = ___index1;
		int32_t L_2 = ___stride2;
		CellSpan_tE5A1247201F556E0880AD919A58F92DEA8402291  L_3 = ___value3;
		*(CellSpan_tE5A1247201F556E0880AD919A58F92DEA8402291 *)((void*)il2cpp_codegen_add((intptr_t)L_0, (intptr_t)(((intptr_t)((int64_t)il2cpp_codegen_multiply((int64_t)(((int64_t)((int64_t)L_1))), (int64_t)(((int64_t)((int64_t)L_2))))))))) = L_3;
		return;
	}
}
// System.Void Unity.Collections.LowLevel.Unsafe.UnsafeUtility::WriteArrayElementWithStride<Obi.ColliderRigidbody>(System.Void*,System.Int32,System.Int32,T)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UnsafeUtility_WriteArrayElementWithStride_TisColliderRigidbody_tC1FF76AA900641BCDF2B31E90931360DA75EAA0D_mD07A561B8322536000DBF2CCB88E52FBF656D685_gshared (void* ___destination0, int32_t ___index1, int32_t ___stride2, ColliderRigidbody_tC1FF76AA900641BCDF2B31E90931360DA75EAA0D  ___value3, const RuntimeMethod* method)
{
	{
		void* L_0 = ___destination0;
		int32_t L_1 = ___index1;
		int32_t L_2 = ___stride2;
		ColliderRigidbody_tC1FF76AA900641BCDF2B31E90931360DA75EAA0D  L_3 = ___value3;
		*(ColliderRigidbody_tC1FF76AA900641BCDF2B31E90931360DA75EAA0D *)((void*)il2cpp_codegen_add((intptr_t)L_0, (intptr_t)(((intptr_t)((int64_t)il2cpp_codegen_multiply((int64_t)(((int64_t)((int64_t)L_1))), (int64_t)(((int64_t)((int64_t)L_2))))))))) = L_3;
		return;
	}
}
// System.Void Unity.Collections.LowLevel.Unsafe.UnsafeUtility::WriteArrayElementWithStride<Obi.ColliderShape>(System.Void*,System.Int32,System.Int32,T)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UnsafeUtility_WriteArrayElementWithStride_TisColliderShape_t7AF5542901AEB2CFA54B6BFD8C852EC91554F586_m56A4BD45929D1C0576013095B7163FBB05B4DB31_gshared (void* ___destination0, int32_t ___index1, int32_t ___stride2, ColliderShape_t7AF5542901AEB2CFA54B6BFD8C852EC91554F586  ___value3, const RuntimeMethod* method)
{
	{
		void* L_0 = ___destination0;
		int32_t L_1 = ___index1;
		int32_t L_2 = ___stride2;
		ColliderShape_t7AF5542901AEB2CFA54B6BFD8C852EC91554F586  L_3 = ___value3;
		*(ColliderShape_t7AF5542901AEB2CFA54B6BFD8C852EC91554F586 *)((void*)il2cpp_codegen_add((intptr_t)L_0, (intptr_t)(((intptr_t)((int64_t)il2cpp_codegen_multiply((int64_t)(((int64_t)((int64_t)L_1))), (int64_t)(((int64_t)((int64_t)L_2))))))))) = L_3;
		return;
	}
}
// System.Void Unity.Collections.LowLevel.Unsafe.UnsafeUtility::WriteArrayElementWithStride<Obi.CollisionMaterial>(System.Void*,System.Int32,System.Int32,T)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UnsafeUtility_WriteArrayElementWithStride_TisCollisionMaterial_t7582B504E28F3D7E580DBB66ECD74F967C92E758_mE7A96CE6540317FC1BA1B997AB5B604769921430_gshared (void* ___destination0, int32_t ___index1, int32_t ___stride2, CollisionMaterial_t7582B504E28F3D7E580DBB66ECD74F967C92E758  ___value3, const RuntimeMethod* method)
{
	{
		void* L_0 = ___destination0;
		int32_t L_1 = ___index1;
		int32_t L_2 = ___stride2;
		CollisionMaterial_t7582B504E28F3D7E580DBB66ECD74F967C92E758  L_3 = ___value3;
		*(CollisionMaterial_t7582B504E28F3D7E580DBB66ECD74F967C92E758 *)((void*)il2cpp_codegen_add((intptr_t)L_0, (intptr_t)(((intptr_t)((int64_t)il2cpp_codegen_multiply((int64_t)(((int64_t)((int64_t)L_1))), (int64_t)(((int64_t)((int64_t)L_2))))))))) = L_3;
		return;
	}
}
// System.Void Unity.Collections.LowLevel.Unsafe.UnsafeUtility::WriteArrayElementWithStride<Obi.DFNode>(System.Void*,System.Int32,System.Int32,T)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UnsafeUtility_WriteArrayElementWithStride_TisDFNode_t8FC489FB11B708202922AF5F52E4AFEB7B205668_m662D876E44C2206B9E1B4C244190F3A400705CA0_gshared (void* ___destination0, int32_t ___index1, int32_t ___stride2, DFNode_t8FC489FB11B708202922AF5F52E4AFEB7B205668  ___value3, const RuntimeMethod* method)
{
	{
		void* L_0 = ___destination0;
		int32_t L_1 = ___index1;
		int32_t L_2 = ___stride2;
		DFNode_t8FC489FB11B708202922AF5F52E4AFEB7B205668  L_3 = ___value3;
		*(DFNode_t8FC489FB11B708202922AF5F52E4AFEB7B205668 *)((void*)il2cpp_codegen_add((intptr_t)L_0, (intptr_t)(((intptr_t)((int64_t)il2cpp_codegen_multiply((int64_t)(((int64_t)((int64_t)L_1))), (int64_t)(((int64_t)((int64_t)L_2))))))))) = L_3;
		return;
	}
}
// System.Void Unity.Collections.LowLevel.Unsafe.UnsafeUtility::WriteArrayElementWithStride<Obi.DistanceFieldHeader>(System.Void*,System.Int32,System.Int32,T)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UnsafeUtility_WriteArrayElementWithStride_TisDistanceFieldHeader_tE66F34D3B1EDB411B0D6E1346D68383E03E33AD4_m33217D75039EA2A3DDF4CE12CF8DA84C45622785_gshared (void* ___destination0, int32_t ___index1, int32_t ___stride2, DistanceFieldHeader_tE66F34D3B1EDB411B0D6E1346D68383E03E33AD4  ___value3, const RuntimeMethod* method)
{
	{
		void* L_0 = ___destination0;
		int32_t L_1 = ___index1;
		int32_t L_2 = ___stride2;
		DistanceFieldHeader_tE66F34D3B1EDB411B0D6E1346D68383E03E33AD4  L_3 = ___value3;
		*(DistanceFieldHeader_tE66F34D3B1EDB411B0D6E1346D68383E03E33AD4 *)((void*)il2cpp_codegen_add((intptr_t)L_0, (intptr_t)(((intptr_t)((int64_t)il2cpp_codegen_multiply((int64_t)(((int64_t)((int64_t)L_1))), (int64_t)(((int64_t)((int64_t)L_2))))))))) = L_3;
		return;
	}
}
// System.Void Unity.Collections.LowLevel.Unsafe.UnsafeUtility::WriteArrayElementWithStride<Obi.Edge>(System.Void*,System.Int32,System.Int32,T)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UnsafeUtility_WriteArrayElementWithStride_TisEdge_tB72B5CE4AD9C11C74696DF37DCED8FB9C2FFDD3C_m196517F0BEA4ADEAD8625A7F78388B8983DBF21F_gshared (void* ___destination0, int32_t ___index1, int32_t ___stride2, Edge_tB72B5CE4AD9C11C74696DF37DCED8FB9C2FFDD3C  ___value3, const RuntimeMethod* method)
{
	{
		void* L_0 = ___destination0;
		int32_t L_1 = ___index1;
		int32_t L_2 = ___stride2;
		Edge_tB72B5CE4AD9C11C74696DF37DCED8FB9C2FFDD3C  L_3 = ___value3;
		*(Edge_tB72B5CE4AD9C11C74696DF37DCED8FB9C2FFDD3C *)((void*)il2cpp_codegen_add((intptr_t)L_0, (intptr_t)(((intptr_t)((int64_t)il2cpp_codegen_multiply((int64_t)(((int64_t)((int64_t)L_1))), (int64_t)(((int64_t)((int64_t)L_2))))))))) = L_3;
		return;
	}
}
// System.Void Unity.Collections.LowLevel.Unsafe.UnsafeUtility::WriteArrayElementWithStride<Obi.EdgeMeshHeader>(System.Void*,System.Int32,System.Int32,T)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UnsafeUtility_WriteArrayElementWithStride_TisEdgeMeshHeader_t235B26AE87E4F8BC38EC627430A9E0E344A03878_mDEADA4B89006D650F124116DE862E77B374B36DB_gshared (void* ___destination0, int32_t ___index1, int32_t ___stride2, EdgeMeshHeader_t235B26AE87E4F8BC38EC627430A9E0E344A03878  ___value3, const RuntimeMethod* method)
{
	{
		void* L_0 = ___destination0;
		int32_t L_1 = ___index1;
		int32_t L_2 = ___stride2;
		EdgeMeshHeader_t235B26AE87E4F8BC38EC627430A9E0E344A03878  L_3 = ___value3;
		*(EdgeMeshHeader_t235B26AE87E4F8BC38EC627430A9E0E344A03878 *)((void*)il2cpp_codegen_add((intptr_t)L_0, (intptr_t)(((intptr_t)((int64_t)il2cpp_codegen_multiply((int64_t)(((int64_t)((int64_t)L_1))), (int64_t)(((int64_t)((int64_t)L_2))))))))) = L_3;
		return;
	}
}
// System.Void Unity.Collections.LowLevel.Unsafe.UnsafeUtility::WriteArrayElementWithStride<Obi.HeightFieldHeader>(System.Void*,System.Int32,System.Int32,T)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UnsafeUtility_WriteArrayElementWithStride_TisHeightFieldHeader_t10BB39F6CE13CD0FBDC200A25AFD4000062D7E4E_m36CADDBFF2D3A38BB40ABB0BBFF841992A522E1B_gshared (void* ___destination0, int32_t ___index1, int32_t ___stride2, HeightFieldHeader_t10BB39F6CE13CD0FBDC200A25AFD4000062D7E4E  ___value3, const RuntimeMethod* method)
{
	{
		void* L_0 = ___destination0;
		int32_t L_1 = ___index1;
		int32_t L_2 = ___stride2;
		HeightFieldHeader_t10BB39F6CE13CD0FBDC200A25AFD4000062D7E4E  L_3 = ___value3;
		*(HeightFieldHeader_t10BB39F6CE13CD0FBDC200A25AFD4000062D7E4E *)((void*)il2cpp_codegen_add((intptr_t)L_0, (intptr_t)(((intptr_t)((int64_t)il2cpp_codegen_multiply((int64_t)(((int64_t)((int64_t)L_1))), (int64_t)(((int64_t)((int64_t)L_2))))))))) = L_3;
		return;
	}
}
// System.Void Unity.Collections.LowLevel.Unsafe.UnsafeUtility::WriteArrayElementWithStride<System.Int32>(System.Void*,System.Int32,System.Int32,T)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UnsafeUtility_WriteArrayElementWithStride_TisInt32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046_mC4B6761723D8C8AE21EFAFF88AEBF01EA83DFFAB_gshared (void* ___destination0, int32_t ___index1, int32_t ___stride2, int32_t ___value3, const RuntimeMethod* method)
{
	{
		void* L_0 = ___destination0;
		int32_t L_1 = ___index1;
		int32_t L_2 = ___stride2;
		int32_t L_3 = ___value3;
		*(int32_t*)((void*)il2cpp_codegen_add((intptr_t)L_0, (intptr_t)(((intptr_t)((int64_t)il2cpp_codegen_multiply((int64_t)(((int64_t)((int64_t)L_1))), (int64_t)(((int64_t)((int64_t)L_2))))))))) = L_3;
		return;
	}
}
// System.Void Unity.Collections.LowLevel.Unsafe.UnsafeUtility::WriteArrayElementWithStride<System.IntPtr>(System.Void*,System.Int32,System.Int32,T)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UnsafeUtility_WriteArrayElementWithStride_TisIntPtr_t_m3467D6D769185FFB5F37A05CEF17F917FED78096_gshared (void* ___destination0, int32_t ___index1, int32_t ___stride2, intptr_t ___value3, const RuntimeMethod* method)
{
	{
		void* L_0 = ___destination0;
		int32_t L_1 = ___index1;
		int32_t L_2 = ___stride2;
		intptr_t L_3 = ___value3;
		*(intptr_t*)((void*)il2cpp_codegen_add((intptr_t)L_0, (intptr_t)(((intptr_t)((int64_t)il2cpp_codegen_multiply((int64_t)(((int64_t)((int64_t)L_1))), (int64_t)(((int64_t)((int64_t)L_2))))))))) = L_3;
		return;
	}
}
// System.Void Unity.Collections.LowLevel.Unsafe.UnsafeUtility::WriteArrayElementWithStride<UnityEngine.Matrix4x4>(System.Void*,System.Int32,System.Int32,T)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UnsafeUtility_WriteArrayElementWithStride_TisMatrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461_m4B5D2E7781665C1D688877AE9811E38A066D7576_gshared (void* ___destination0, int32_t ___index1, int32_t ___stride2, Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461  ___value3, const RuntimeMethod* method)
{
	{
		void* L_0 = ___destination0;
		int32_t L_1 = ___index1;
		int32_t L_2 = ___stride2;
		Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461  L_3 = ___value3;
		*(Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461 *)((void*)il2cpp_codegen_add((intptr_t)L_0, (intptr_t)(((intptr_t)((int64_t)il2cpp_codegen_multiply((int64_t)(((int64_t)((int64_t)L_1))), (int64_t)(((int64_t)((int64_t)L_2))))))))) = L_3;
		return;
	}
}
// System.Void Unity.Collections.LowLevel.Unsafe.UnsafeUtility::WriteArrayElementWithStride<UnityEngine.Quaternion>(System.Void*,System.Int32,System.Int32,T)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UnsafeUtility_WriteArrayElementWithStride_TisQuaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4_m05A878B1DEE66767519DA718C0F8E7C139A1182E_gshared (void* ___destination0, int32_t ___index1, int32_t ___stride2, Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  ___value3, const RuntimeMethod* method)
{
	{
		void* L_0 = ___destination0;
		int32_t L_1 = ___index1;
		int32_t L_2 = ___stride2;
		Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  L_3 = ___value3;
		*(Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4 *)((void*)il2cpp_codegen_add((intptr_t)L_0, (intptr_t)(((intptr_t)((int64_t)il2cpp_codegen_multiply((int64_t)(((int64_t)((int64_t)L_1))), (int64_t)(((int64_t)((int64_t)L_2))))))))) = L_3;
		return;
	}
}
// System.Void Unity.Collections.LowLevel.Unsafe.UnsafeUtility::WriteArrayElementWithStride<Obi.QueryResult>(System.Void*,System.Int32,System.Int32,T)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UnsafeUtility_WriteArrayElementWithStride_TisQueryResult_t4DCEE89279D61183A30969C97F725AA603AD118C_mB5AE7B385C4C9CB1EB533E6EAE8756153AD34831_gshared (void* ___destination0, int32_t ___index1, int32_t ___stride2, QueryResult_t4DCEE89279D61183A30969C97F725AA603AD118C  ___value3, const RuntimeMethod* method)
{
	{
		void* L_0 = ___destination0;
		int32_t L_1 = ___index1;
		int32_t L_2 = ___stride2;
		QueryResult_t4DCEE89279D61183A30969C97F725AA603AD118C  L_3 = ___value3;
		*(QueryResult_t4DCEE89279D61183A30969C97F725AA603AD118C *)((void*)il2cpp_codegen_add((intptr_t)L_0, (intptr_t)(((intptr_t)((int64_t)il2cpp_codegen_multiply((int64_t)(((int64_t)((int64_t)L_1))), (int64_t)(((int64_t)((int64_t)L_2))))))))) = L_3;
		return;
	}
}
// System.Void Unity.Collections.LowLevel.Unsafe.UnsafeUtility::WriteArrayElementWithStride<Obi.QueryShape>(System.Void*,System.Int32,System.Int32,T)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UnsafeUtility_WriteArrayElementWithStride_TisQueryShape_t0A83A6359C90B5213CDEC851B226F8DAAD6D07AF_m9286CB567D626472A4501CD410B7B3980E6CE2B9_gshared (void* ___destination0, int32_t ___index1, int32_t ___stride2, QueryShape_t0A83A6359C90B5213CDEC851B226F8DAAD6D07AF  ___value3, const RuntimeMethod* method)
{
	{
		void* L_0 = ___destination0;
		int32_t L_1 = ___index1;
		int32_t L_2 = ___stride2;
		QueryShape_t0A83A6359C90B5213CDEC851B226F8DAAD6D07AF  L_3 = ___value3;
		*(QueryShape_t0A83A6359C90B5213CDEC851B226F8DAAD6D07AF *)((void*)il2cpp_codegen_add((intptr_t)L_0, (intptr_t)(((intptr_t)((int64_t)il2cpp_codegen_multiply((int64_t)(((int64_t)((int64_t)L_1))), (int64_t)(((int64_t)((int64_t)L_2))))))))) = L_3;
		return;
	}
}
// System.Void Unity.Collections.LowLevel.Unsafe.UnsafeUtility::WriteArrayElementWithStride<System.Single>(System.Void*,System.Int32,System.Int32,T)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UnsafeUtility_WriteArrayElementWithStride_TisSingle_tE07797BA3C98D4CA9B5A19413C19A76688AB899E_m5B678933FD30D386CB82611738020A0CB9CDEAB5_gshared (void* ___destination0, int32_t ___index1, int32_t ___stride2, float ___value3, const RuntimeMethod* method)
{
	{
		void* L_0 = ___destination0;
		int32_t L_1 = ___index1;
		int32_t L_2 = ___stride2;
		float L_3 = ___value3;
		*(float*)((void*)il2cpp_codegen_add((intptr_t)L_0, (intptr_t)(((intptr_t)((int64_t)il2cpp_codegen_multiply((int64_t)(((int64_t)((int64_t)L_1))), (int64_t)(((int64_t)((int64_t)L_2))))))))) = L_3;
		return;
	}
}
// System.Void Unity.Collections.LowLevel.Unsafe.UnsafeUtility::WriteArrayElementWithStride<Obi.Triangle>(System.Void*,System.Int32,System.Int32,T)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UnsafeUtility_WriteArrayElementWithStride_TisTriangle_t6EBD8067A5E92E374FBCEE8434826BC760D9FDC8_mA8C4D4DA5AA46856157EB6D3EFA22BF04DFC485F_gshared (void* ___destination0, int32_t ___index1, int32_t ___stride2, Triangle_t6EBD8067A5E92E374FBCEE8434826BC760D9FDC8  ___value3, const RuntimeMethod* method)
{
	{
		void* L_0 = ___destination0;
		int32_t L_1 = ___index1;
		int32_t L_2 = ___stride2;
		Triangle_t6EBD8067A5E92E374FBCEE8434826BC760D9FDC8  L_3 = ___value3;
		*(Triangle_t6EBD8067A5E92E374FBCEE8434826BC760D9FDC8 *)((void*)il2cpp_codegen_add((intptr_t)L_0, (intptr_t)(((intptr_t)((int64_t)il2cpp_codegen_multiply((int64_t)(((int64_t)((int64_t)L_1))), (int64_t)(((int64_t)((int64_t)L_2))))))))) = L_3;
		return;
	}
}
// System.Void Unity.Collections.LowLevel.Unsafe.UnsafeUtility::WriteArrayElementWithStride<Obi.TriangleMeshHeader>(System.Void*,System.Int32,System.Int32,T)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UnsafeUtility_WriteArrayElementWithStride_TisTriangleMeshHeader_tD4F431BF8B26AB40AEE8CC5AF9C045A565607267_mE168475C97625F3E05A978CCF502F9A318D018D4_gshared (void* ___destination0, int32_t ___index1, int32_t ___stride2, TriangleMeshHeader_tD4F431BF8B26AB40AEE8CC5AF9C045A565607267  ___value3, const RuntimeMethod* method)
{
	{
		void* L_0 = ___destination0;
		int32_t L_1 = ___index1;
		int32_t L_2 = ___stride2;
		TriangleMeshHeader_tD4F431BF8B26AB40AEE8CC5AF9C045A565607267  L_3 = ___value3;
		*(TriangleMeshHeader_tD4F431BF8B26AB40AEE8CC5AF9C045A565607267 *)((void*)il2cpp_codegen_add((intptr_t)L_0, (intptr_t)(((intptr_t)((int64_t)il2cpp_codegen_multiply((int64_t)(((int64_t)((int64_t)L_1))), (int64_t)(((int64_t)((int64_t)L_2))))))))) = L_3;
		return;
	}
}
// System.Void Unity.Collections.LowLevel.Unsafe.UnsafeUtility::WriteArrayElementWithStride<Obi.VInt4>(System.Void*,System.Int32,System.Int32,T)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UnsafeUtility_WriteArrayElementWithStride_TisVInt4_t7102225B83232A71C9D784846D42B69BF440AD5D_m6A339C3ED006C83260AE208092916940E9947CA8_gshared (void* ___destination0, int32_t ___index1, int32_t ___stride2, VInt4_t7102225B83232A71C9D784846D42B69BF440AD5D  ___value3, const RuntimeMethod* method)
{
	{
		void* L_0 = ___destination0;
		int32_t L_1 = ___index1;
		int32_t L_2 = ___stride2;
		VInt4_t7102225B83232A71C9D784846D42B69BF440AD5D  L_3 = ___value3;
		*(VInt4_t7102225B83232A71C9D784846D42B69BF440AD5D *)((void*)il2cpp_codegen_add((intptr_t)L_0, (intptr_t)(((intptr_t)((int64_t)il2cpp_codegen_multiply((int64_t)(((int64_t)((int64_t)L_1))), (int64_t)(((int64_t)((int64_t)L_2))))))))) = L_3;
		return;
	}
}
// System.Void Unity.Collections.LowLevel.Unsafe.UnsafeUtility::WriteArrayElementWithStride<UnityEngine.Vector2>(System.Void*,System.Int32,System.Int32,T)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UnsafeUtility_WriteArrayElementWithStride_TisVector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_m3D7A7F72D127EBD036DF402368F763721F44FE39_gshared (void* ___destination0, int32_t ___index1, int32_t ___stride2, Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___value3, const RuntimeMethod* method)
{
	{
		void* L_0 = ___destination0;
		int32_t L_1 = ___index1;
		int32_t L_2 = ___stride2;
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_3 = ___value3;
		*(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 *)((void*)il2cpp_codegen_add((intptr_t)L_0, (intptr_t)(((intptr_t)((int64_t)il2cpp_codegen_multiply((int64_t)(((int64_t)((int64_t)L_1))), (int64_t)(((int64_t)((int64_t)L_2))))))))) = L_3;
		return;
	}
}
// System.Void Unity.Collections.LowLevel.Unsafe.UnsafeUtility::WriteArrayElementWithStride<UnityEngine.Vector3>(System.Void*,System.Int32,System.Int32,T)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UnsafeUtility_WriteArrayElementWithStride_TisVector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_m4CD1706941D9FEC400F23BE4BA6A7F489F7F1AC6_gshared (void* ___destination0, int32_t ___index1, int32_t ___stride2, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___value3, const RuntimeMethod* method)
{
	{
		void* L_0 = ___destination0;
		int32_t L_1 = ___index1;
		int32_t L_2 = ___stride2;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_3 = ___value3;
		*(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E *)((void*)il2cpp_codegen_add((intptr_t)L_0, (intptr_t)(((intptr_t)((int64_t)il2cpp_codegen_multiply((int64_t)(((int64_t)((int64_t)L_1))), (int64_t)(((int64_t)((int64_t)L_2))))))))) = L_3;
		return;
	}
}
// System.Void Unity.Collections.LowLevel.Unsafe.UnsafeUtility::WriteArrayElementWithStride<UnityEngine.Vector4>(System.Void*,System.Int32,System.Int32,T)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UnsafeUtility_WriteArrayElementWithStride_TisVector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7_mBA57CF971CD9852588DE2FC9B1DD69983961D395_gshared (void* ___destination0, int32_t ___index1, int32_t ___stride2, Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  ___value3, const RuntimeMethod* method)
{
	{
		void* L_0 = ___destination0;
		int32_t L_1 = ___index1;
		int32_t L_2 = ___stride2;
		Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  L_3 = ___value3;
		*(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7 *)((void*)il2cpp_codegen_add((intptr_t)L_0, (intptr_t)(((intptr_t)((int64_t)il2cpp_codegen_multiply((int64_t)(((int64_t)((int64_t)L_1))), (int64_t)(((int64_t)((int64_t)L_2))))))))) = L_3;
		return;
	}
}
// System.Void Unity.Collections.LowLevel.Unsafe.UnsafeUtility::WriteArrayElementWithStride<Oni_Contact>(System.Void*,System.Int32,System.Int32,T)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UnsafeUtility_WriteArrayElementWithStride_TisContact_tA38E76CD5E9A53DE837B07BBB25CABE68B2AF756_m0BD8821D20178095FF46FD6FB791D5E3525260D6_gshared (void* ___destination0, int32_t ___index1, int32_t ___stride2, Contact_tA38E76CD5E9A53DE837B07BBB25CABE68B2AF756  ___value3, const RuntimeMethod* method)
{
	{
		void* L_0 = ___destination0;
		int32_t L_1 = ___index1;
		int32_t L_2 = ___stride2;
		Contact_tA38E76CD5E9A53DE837B07BBB25CABE68B2AF756  L_3 = ___value3;
		*(Contact_tA38E76CD5E9A53DE837B07BBB25CABE68B2AF756 *)((void*)il2cpp_codegen_add((intptr_t)L_0, (intptr_t)(((intptr_t)((int64_t)il2cpp_codegen_multiply((int64_t)(((int64_t)((int64_t)L_1))), (int64_t)(((int64_t)((int64_t)L_2))))))))) = L_3;
		return;
	}
}
// System.Void Unity.Collections.LowLevel.Unsafe.UnsafeUtility::WriteArrayElementWithStride<System.Object>(System.Void*,System.Int32,System.Int32,T)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UnsafeUtility_WriteArrayElementWithStride_TisRuntimeObject_m182506AFA387937C398582D7CBE0188587DB7958_gshared (void* ___destination0, int32_t ___index1, int32_t ___stride2, RuntimeObject * ___value3, const RuntimeMethod* method)
{
	{
		void* L_0 = ___destination0;
		int32_t L_1 = ___index1;
		int32_t L_2 = ___stride2;
		RuntimeObject * L_3 = ___value3;
		*(RuntimeObject **)((void*)il2cpp_codegen_add((intptr_t)L_0, (intptr_t)(((intptr_t)((int64_t)il2cpp_codegen_multiply((int64_t)(((int64_t)((int64_t)L_1))), (int64_t)(((int64_t)((int64_t)L_2))))))))) = L_3;
		Il2CppCodeGenWriteBarrier((void**)(RuntimeObject **)((void*)il2cpp_codegen_add((intptr_t)L_0, (intptr_t)(((intptr_t)((int64_t)il2cpp_codegen_multiply((int64_t)(((int64_t)((int64_t)L_1))), (int64_t)(((int64_t)((int64_t)L_2))))))))), (void*)L_3);
		return;
	}
}
// T System.Threading.Volatile::Read<System.Object>(T&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * Volatile_Read_TisRuntimeObject_m34EC4B420E3B86C8B06957C02A3FDD87D674E50E_gshared (RuntimeObject ** ___location0, const RuntimeMethod* method)
{
	return VolatileRead(___location0);
}
// System.Void System.Threading.Volatile::Write<System.Object>(T&,T)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Volatile_Write_TisRuntimeObject_m3392CE290ADB37963BB566664A63096AF6DABAE4_gshared (RuntimeObject ** ___location0, RuntimeObject * ___value1, const RuntimeMethod* method)
{
	VolatileWrite(___location0, ___value1);
}
// System.Void System.Runtime.CompilerServices.AsyncTaskMethodBuilder`1<System.Boolean>::AwaitUnsafeOnCompleted<System.Runtime.CompilerServices.ConfiguredTaskAwaitable`1_ConfiguredTaskAwaiter<System.Boolean>,System.Threading.SemaphoreSlim_<WaitUntilCountOrTimeoutAsync>d__31>(TAwaiter&,TStateMachine&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AsyncTaskMethodBuilder_1_AwaitUnsafeOnCompleted_TisConfiguredTaskAwaiter_t286C97C0AF102C4C0BE55CE2025CC7BD1FB5C20C_TisU3CWaitUntilCountOrTimeoutAsyncU3Ed__31_t227D1F5F19C948CA8C23C80B5F19147D4AAED14F_m06010DFBCE2CDD2AE1BB5ADD25230521E9B527DC_gshared (AsyncTaskMethodBuilder_1_tABE1DEF12F121D6FC8ABF04869ED964FF83EA9B5 * __this, ConfiguredTaskAwaiter_t286C97C0AF102C4C0BE55CE2025CC7BD1FB5C20C * ___awaiter0, U3CWaitUntilCountOrTimeoutAsyncU3Ed__31_t227D1F5F19C948CA8C23C80B5F19147D4AAED14F * ___stateMachine1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AsyncTaskMethodBuilder_1_AwaitUnsafeOnCompleted_TisConfiguredTaskAwaiter_t286C97C0AF102C4C0BE55CE2025CC7BD1FB5C20C_TisU3CWaitUntilCountOrTimeoutAsyncU3Ed__31_t227D1F5F19C948CA8C23C80B5F19147D4AAED14F_m06010DFBCE2CDD2AE1BB5ADD25230521E9B527DC_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	MoveNextRunner_tFAEA0BEDD353E2E34E8E287C67B1F5572FD30C2D * V_0 = NULL;
	Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 * V_1 = NULL;
	Task_1_t9C1FE9F18F52F3409B9E970FA38801A443AE7849 * V_2 = NULL;
	Exception_t * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	void* __leave_targets_storage = alloca(sizeof(int32_t) * 2);
	il2cpp::utils::LeaveTargetStack __leave_targets(__leave_targets_storage);
	NO_UNUSED_WARNING (__leave_targets);
	AsyncMethodBuilderCore_t2C85055E04767C52B9F66144476FCBF500DBFA34 * G_B2_0 = NULL;
	AsyncMethodBuilderCore_t2C85055E04767C52B9F66144476FCBF500DBFA34 * G_B1_0 = NULL;
	Task_1_t9C1FE9F18F52F3409B9E970FA38801A443AE7849 * G_B3_0 = NULL;
	AsyncMethodBuilderCore_t2C85055E04767C52B9F66144476FCBF500DBFA34 * G_B3_1 = NULL;

IL_0000:
	try
	{ // begin try (depth: 1)
		{
			V_0 = (MoveNextRunner_tFAEA0BEDD353E2E34E8E287C67B1F5572FD30C2D *)NULL;
			AsyncMethodBuilderCore_t2C85055E04767C52B9F66144476FCBF500DBFA34 * L_0 = (AsyncMethodBuilderCore_t2C85055E04767C52B9F66144476FCBF500DBFA34 *)__this->get_address_of_m_coreState_1();
			bool L_1 = AsyncCausalityTracer_get_LoggingOn_mE0A03E121425371B1D1B65640172137C3B8EEA15(/*hidden argument*/NULL);
			G_B1_0 = L_0;
			if (L_1)
			{
				G_B2_0 = L_0;
				goto IL_0012;
			}
		}

IL_000f:
		{
			G_B3_0 = ((Task_1_t9C1FE9F18F52F3409B9E970FA38801A443AE7849 *)(NULL));
			G_B3_1 = G_B1_0;
			goto IL_0018;
		}

IL_0012:
		{
			Task_1_t9C1FE9F18F52F3409B9E970FA38801A443AE7849 * L_2 = AsyncTaskMethodBuilder_1_get_Task_m6F8035FA0DEBE32D20BAD45867B41491F5D55F78((AsyncTaskMethodBuilder_1_tABE1DEF12F121D6FC8ABF04869ED964FF83EA9B5 *)(AsyncTaskMethodBuilder_1_tABE1DEF12F121D6FC8ABF04869ED964FF83EA9B5 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 0));
			G_B3_0 = L_2;
			G_B3_1 = G_B2_0;
		}

IL_0018:
		{
			Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 * L_3 = AsyncMethodBuilderCore_GetCompletionAction_m7FE7F57CC452F0EDE870AB08EEB648E2027D4F5C((AsyncMethodBuilderCore_t2C85055E04767C52B9F66144476FCBF500DBFA34 *)(AsyncMethodBuilderCore_t2C85055E04767C52B9F66144476FCBF500DBFA34 *)G_B3_1, (Task_t804B25CFE3FC13AAEE16C8FA3BF52513F2A8DB60 *)G_B3_0, (MoveNextRunner_tFAEA0BEDD353E2E34E8E287C67B1F5572FD30C2D **)(MoveNextRunner_tFAEA0BEDD353E2E34E8E287C67B1F5572FD30C2D **)(&V_0), /*hidden argument*/NULL);
			V_1 = (Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 *)L_3;
			AsyncMethodBuilderCore_t2C85055E04767C52B9F66144476FCBF500DBFA34 * L_4 = (AsyncMethodBuilderCore_t2C85055E04767C52B9F66144476FCBF500DBFA34 *)__this->get_address_of_m_coreState_1();
			RuntimeObject* L_5 = (RuntimeObject*)L_4->get_m_stateMachine_0();
			if (L_5)
			{
				goto IL_004c;
			}
		}

IL_002d:
		{
			Task_1_t9C1FE9F18F52F3409B9E970FA38801A443AE7849 * L_6 = AsyncTaskMethodBuilder_1_get_Task_m6F8035FA0DEBE32D20BAD45867B41491F5D55F78((AsyncTaskMethodBuilder_1_tABE1DEF12F121D6FC8ABF04869ED964FF83EA9B5 *)(AsyncTaskMethodBuilder_1_tABE1DEF12F121D6FC8ABF04869ED964FF83EA9B5 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 0));
			V_2 = (Task_1_t9C1FE9F18F52F3409B9E970FA38801A443AE7849 *)L_6;
			AsyncMethodBuilderCore_t2C85055E04767C52B9F66144476FCBF500DBFA34 * L_7 = (AsyncMethodBuilderCore_t2C85055E04767C52B9F66144476FCBF500DBFA34 *)__this->get_address_of_m_coreState_1();
			U3CWaitUntilCountOrTimeoutAsyncU3Ed__31_t227D1F5F19C948CA8C23C80B5F19147D4AAED14F * L_8 = ___stateMachine1;
			U3CWaitUntilCountOrTimeoutAsyncU3Ed__31_t227D1F5F19C948CA8C23C80B5F19147D4AAED14F  L_9 = (*(U3CWaitUntilCountOrTimeoutAsyncU3Ed__31_t227D1F5F19C948CA8C23C80B5F19147D4AAED14F *)L_8);
			U3CWaitUntilCountOrTimeoutAsyncU3Ed__31_t227D1F5F19C948CA8C23C80B5F19147D4AAED14F  L_10 = L_9;
			RuntimeObject * L_11 = Box(IL2CPP_RGCTX_DATA(method->rgctx_data, 0), &L_10);
			MoveNextRunner_tFAEA0BEDD353E2E34E8E287C67B1F5572FD30C2D * L_12 = V_0;
			Task_1_t9C1FE9F18F52F3409B9E970FA38801A443AE7849 * L_13 = V_2;
			AsyncMethodBuilderCore_PostBoxInitialization_m22C1D9A2745255C6FC1426D4CB0C4355FBDA07E3((AsyncMethodBuilderCore_t2C85055E04767C52B9F66144476FCBF500DBFA34 *)(AsyncMethodBuilderCore_t2C85055E04767C52B9F66144476FCBF500DBFA34 *)L_7, (RuntimeObject*)L_11, (MoveNextRunner_tFAEA0BEDD353E2E34E8E287C67B1F5572FD30C2D *)L_12, (Task_t804B25CFE3FC13AAEE16C8FA3BF52513F2A8DB60 *)L_13, /*hidden argument*/NULL);
		}

IL_004c:
		{
			ConfiguredTaskAwaiter_t286C97C0AF102C4C0BE55CE2025CC7BD1FB5C20C * L_14 = ___awaiter0;
			Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 * L_15 = V_1;
			Il2CppFakeBox<ConfiguredTaskAwaiter_t286C97C0AF102C4C0BE55CE2025CC7BD1FB5C20C > L_16(IL2CPP_RGCTX_DATA(method->rgctx_data, 1), L_14);
			const VirtualInvokeData& il2cpp_virtual_invoke_data__84 = il2cpp_codegen_get_interface_invoke_data(0, (&L_16), ICriticalNotifyCompletion_t3F4FCEA29CDCACD07747F3A1D6D1406521556838_il2cpp_TypeInfo_var);
			((  void (*) (RuntimeObject*, Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 *, const RuntimeMethod*))il2cpp_virtual_invoke_data__84.methodPtr)((RuntimeObject*)(&L_16), (Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 *)L_15, /*hidden argument*/il2cpp_virtual_invoke_data__84.method);
			*L_14 = L_16.m_Value;
			goto IL_0063;
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Exception_t_il2cpp_TypeInfo_var, il2cpp_codegen_object_class(e.ex)))
			goto CATCH_005b;
		throw e;
	}

CATCH_005b:
	{ // begin catch(System.Exception)
		AsyncMethodBuilderCore_ThrowAsync_m69800E9752B30F7A1D824C2F1EBDEBA44BA75610((Exception_t *)((Exception_t *)__exception_local), (SynchronizationContext_t17D9365B5E0D30A0910A16FA4351C525232EF069 *)NULL, /*hidden argument*/NULL);
		goto IL_0063;
	} // end catch (depth: 1)

IL_0063:
	{
		return;
	}
}
IL2CPP_EXTERN_C  void AsyncTaskMethodBuilder_1_AwaitUnsafeOnCompleted_TisConfiguredTaskAwaiter_t286C97C0AF102C4C0BE55CE2025CC7BD1FB5C20C_TisU3CWaitUntilCountOrTimeoutAsyncU3Ed__31_t227D1F5F19C948CA8C23C80B5F19147D4AAED14F_m06010DFBCE2CDD2AE1BB5ADD25230521E9B527DC_AdjustorThunk (RuntimeObject * __this, ConfiguredTaskAwaiter_t286C97C0AF102C4C0BE55CE2025CC7BD1FB5C20C * ___awaiter0, U3CWaitUntilCountOrTimeoutAsyncU3Ed__31_t227D1F5F19C948CA8C23C80B5F19147D4AAED14F * ___stateMachine1, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	AsyncTaskMethodBuilder_1_tABE1DEF12F121D6FC8ABF04869ED964FF83EA9B5 * _thisAdjusted = reinterpret_cast<AsyncTaskMethodBuilder_1_tABE1DEF12F121D6FC8ABF04869ED964FF83EA9B5 *>(__this + _offset);
	AsyncTaskMethodBuilder_1_AwaitUnsafeOnCompleted_TisConfiguredTaskAwaiter_t286C97C0AF102C4C0BE55CE2025CC7BD1FB5C20C_TisU3CWaitUntilCountOrTimeoutAsyncU3Ed__31_t227D1F5F19C948CA8C23C80B5F19147D4AAED14F_m06010DFBCE2CDD2AE1BB5ADD25230521E9B527DC(_thisAdjusted, ___awaiter0, ___stateMachine1, method);
}
// System.Void System.Runtime.CompilerServices.AsyncTaskMethodBuilder`1<System.Boolean>::AwaitUnsafeOnCompleted<System.Runtime.CompilerServices.ConfiguredTaskAwaitable`1_ConfiguredTaskAwaiter<System.Object>,System.Threading.SemaphoreSlim_<WaitUntilCountOrTimeoutAsync>d__31>(TAwaiter&,TStateMachine&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AsyncTaskMethodBuilder_1_AwaitUnsafeOnCompleted_TisConfiguredTaskAwaiter_t2CE498F9A6CE5405242AE2D77F03E58985B7C3ED_TisU3CWaitUntilCountOrTimeoutAsyncU3Ed__31_t227D1F5F19C948CA8C23C80B5F19147D4AAED14F_m4FDC6642836D25291A0E343845527873A88013C9_gshared (AsyncTaskMethodBuilder_1_tABE1DEF12F121D6FC8ABF04869ED964FF83EA9B5 * __this, ConfiguredTaskAwaiter_t2CE498F9A6CE5405242AE2D77F03E58985B7C3ED * ___awaiter0, U3CWaitUntilCountOrTimeoutAsyncU3Ed__31_t227D1F5F19C948CA8C23C80B5F19147D4AAED14F * ___stateMachine1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AsyncTaskMethodBuilder_1_AwaitUnsafeOnCompleted_TisConfiguredTaskAwaiter_t2CE498F9A6CE5405242AE2D77F03E58985B7C3ED_TisU3CWaitUntilCountOrTimeoutAsyncU3Ed__31_t227D1F5F19C948CA8C23C80B5F19147D4AAED14F_m4FDC6642836D25291A0E343845527873A88013C9_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	MoveNextRunner_tFAEA0BEDD353E2E34E8E287C67B1F5572FD30C2D * V_0 = NULL;
	Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 * V_1 = NULL;
	Task_1_t9C1FE9F18F52F3409B9E970FA38801A443AE7849 * V_2 = NULL;
	Exception_t * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	void* __leave_targets_storage = alloca(sizeof(int32_t) * 2);
	il2cpp::utils::LeaveTargetStack __leave_targets(__leave_targets_storage);
	NO_UNUSED_WARNING (__leave_targets);
	AsyncMethodBuilderCore_t2C85055E04767C52B9F66144476FCBF500DBFA34 * G_B2_0 = NULL;
	AsyncMethodBuilderCore_t2C85055E04767C52B9F66144476FCBF500DBFA34 * G_B1_0 = NULL;
	Task_1_t9C1FE9F18F52F3409B9E970FA38801A443AE7849 * G_B3_0 = NULL;
	AsyncMethodBuilderCore_t2C85055E04767C52B9F66144476FCBF500DBFA34 * G_B3_1 = NULL;

IL_0000:
	try
	{ // begin try (depth: 1)
		{
			V_0 = (MoveNextRunner_tFAEA0BEDD353E2E34E8E287C67B1F5572FD30C2D *)NULL;
			AsyncMethodBuilderCore_t2C85055E04767C52B9F66144476FCBF500DBFA34 * L_0 = (AsyncMethodBuilderCore_t2C85055E04767C52B9F66144476FCBF500DBFA34 *)__this->get_address_of_m_coreState_1();
			bool L_1 = AsyncCausalityTracer_get_LoggingOn_mE0A03E121425371B1D1B65640172137C3B8EEA15(/*hidden argument*/NULL);
			G_B1_0 = L_0;
			if (L_1)
			{
				G_B2_0 = L_0;
				goto IL_0012;
			}
		}

IL_000f:
		{
			G_B3_0 = ((Task_1_t9C1FE9F18F52F3409B9E970FA38801A443AE7849 *)(NULL));
			G_B3_1 = G_B1_0;
			goto IL_0018;
		}

IL_0012:
		{
			Task_1_t9C1FE9F18F52F3409B9E970FA38801A443AE7849 * L_2 = AsyncTaskMethodBuilder_1_get_Task_m6F8035FA0DEBE32D20BAD45867B41491F5D55F78((AsyncTaskMethodBuilder_1_tABE1DEF12F121D6FC8ABF04869ED964FF83EA9B5 *)(AsyncTaskMethodBuilder_1_tABE1DEF12F121D6FC8ABF04869ED964FF83EA9B5 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 0));
			G_B3_0 = L_2;
			G_B3_1 = G_B2_0;
		}

IL_0018:
		{
			Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 * L_3 = AsyncMethodBuilderCore_GetCompletionAction_m7FE7F57CC452F0EDE870AB08EEB648E2027D4F5C((AsyncMethodBuilderCore_t2C85055E04767C52B9F66144476FCBF500DBFA34 *)(AsyncMethodBuilderCore_t2C85055E04767C52B9F66144476FCBF500DBFA34 *)G_B3_1, (Task_t804B25CFE3FC13AAEE16C8FA3BF52513F2A8DB60 *)G_B3_0, (MoveNextRunner_tFAEA0BEDD353E2E34E8E287C67B1F5572FD30C2D **)(MoveNextRunner_tFAEA0BEDD353E2E34E8E287C67B1F5572FD30C2D **)(&V_0), /*hidden argument*/NULL);
			V_1 = (Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 *)L_3;
			AsyncMethodBuilderCore_t2C85055E04767C52B9F66144476FCBF500DBFA34 * L_4 = (AsyncMethodBuilderCore_t2C85055E04767C52B9F66144476FCBF500DBFA34 *)__this->get_address_of_m_coreState_1();
			RuntimeObject* L_5 = (RuntimeObject*)L_4->get_m_stateMachine_0();
			if (L_5)
			{
				goto IL_004c;
			}
		}

IL_002d:
		{
			Task_1_t9C1FE9F18F52F3409B9E970FA38801A443AE7849 * L_6 = AsyncTaskMethodBuilder_1_get_Task_m6F8035FA0DEBE32D20BAD45867B41491F5D55F78((AsyncTaskMethodBuilder_1_tABE1DEF12F121D6FC8ABF04869ED964FF83EA9B5 *)(AsyncTaskMethodBuilder_1_tABE1DEF12F121D6FC8ABF04869ED964FF83EA9B5 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 0));
			V_2 = (Task_1_t9C1FE9F18F52F3409B9E970FA38801A443AE7849 *)L_6;
			AsyncMethodBuilderCore_t2C85055E04767C52B9F66144476FCBF500DBFA34 * L_7 = (AsyncMethodBuilderCore_t2C85055E04767C52B9F66144476FCBF500DBFA34 *)__this->get_address_of_m_coreState_1();
			U3CWaitUntilCountOrTimeoutAsyncU3Ed__31_t227D1F5F19C948CA8C23C80B5F19147D4AAED14F * L_8 = ___stateMachine1;
			U3CWaitUntilCountOrTimeoutAsyncU3Ed__31_t227D1F5F19C948CA8C23C80B5F19147D4AAED14F  L_9 = (*(U3CWaitUntilCountOrTimeoutAsyncU3Ed__31_t227D1F5F19C948CA8C23C80B5F19147D4AAED14F *)L_8);
			U3CWaitUntilCountOrTimeoutAsyncU3Ed__31_t227D1F5F19C948CA8C23C80B5F19147D4AAED14F  L_10 = L_9;
			RuntimeObject * L_11 = Box(IL2CPP_RGCTX_DATA(method->rgctx_data, 0), &L_10);
			MoveNextRunner_tFAEA0BEDD353E2E34E8E287C67B1F5572FD30C2D * L_12 = V_0;
			Task_1_t9C1FE9F18F52F3409B9E970FA38801A443AE7849 * L_13 = V_2;
			AsyncMethodBuilderCore_PostBoxInitialization_m22C1D9A2745255C6FC1426D4CB0C4355FBDA07E3((AsyncMethodBuilderCore_t2C85055E04767C52B9F66144476FCBF500DBFA34 *)(AsyncMethodBuilderCore_t2C85055E04767C52B9F66144476FCBF500DBFA34 *)L_7, (RuntimeObject*)L_11, (MoveNextRunner_tFAEA0BEDD353E2E34E8E287C67B1F5572FD30C2D *)L_12, (Task_t804B25CFE3FC13AAEE16C8FA3BF52513F2A8DB60 *)L_13, /*hidden argument*/NULL);
		}

IL_004c:
		{
			ConfiguredTaskAwaiter_t2CE498F9A6CE5405242AE2D77F03E58985B7C3ED * L_14 = ___awaiter0;
			Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 * L_15 = V_1;
			Il2CppFakeBox<ConfiguredTaskAwaiter_t2CE498F9A6CE5405242AE2D77F03E58985B7C3ED > L_16(IL2CPP_RGCTX_DATA(method->rgctx_data, 1), L_14);
			const VirtualInvokeData& il2cpp_virtual_invoke_data__84 = il2cpp_codegen_get_interface_invoke_data(0, (&L_16), ICriticalNotifyCompletion_t3F4FCEA29CDCACD07747F3A1D6D1406521556838_il2cpp_TypeInfo_var);
			((  void (*) (RuntimeObject*, Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 *, const RuntimeMethod*))il2cpp_virtual_invoke_data__84.methodPtr)((RuntimeObject*)(&L_16), (Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 *)L_15, /*hidden argument*/il2cpp_virtual_invoke_data__84.method);
			*L_14 = L_16.m_Value;
			goto IL_0063;
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Exception_t_il2cpp_TypeInfo_var, il2cpp_codegen_object_class(e.ex)))
			goto CATCH_005b;
		throw e;
	}

CATCH_005b:
	{ // begin catch(System.Exception)
		AsyncMethodBuilderCore_ThrowAsync_m69800E9752B30F7A1D824C2F1EBDEBA44BA75610((Exception_t *)((Exception_t *)__exception_local), (SynchronizationContext_t17D9365B5E0D30A0910A16FA4351C525232EF069 *)NULL, /*hidden argument*/NULL);
		goto IL_0063;
	} // end catch (depth: 1)

IL_0063:
	{
		return;
	}
}
IL2CPP_EXTERN_C  void AsyncTaskMethodBuilder_1_AwaitUnsafeOnCompleted_TisConfiguredTaskAwaiter_t2CE498F9A6CE5405242AE2D77F03E58985B7C3ED_TisU3CWaitUntilCountOrTimeoutAsyncU3Ed__31_t227D1F5F19C948CA8C23C80B5F19147D4AAED14F_m4FDC6642836D25291A0E343845527873A88013C9_AdjustorThunk (RuntimeObject * __this, ConfiguredTaskAwaiter_t2CE498F9A6CE5405242AE2D77F03E58985B7C3ED * ___awaiter0, U3CWaitUntilCountOrTimeoutAsyncU3Ed__31_t227D1F5F19C948CA8C23C80B5F19147D4AAED14F * ___stateMachine1, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	AsyncTaskMethodBuilder_1_tABE1DEF12F121D6FC8ABF04869ED964FF83EA9B5 * _thisAdjusted = reinterpret_cast<AsyncTaskMethodBuilder_1_tABE1DEF12F121D6FC8ABF04869ED964FF83EA9B5 *>(__this + _offset);
	AsyncTaskMethodBuilder_1_AwaitUnsafeOnCompleted_TisConfiguredTaskAwaiter_t2CE498F9A6CE5405242AE2D77F03E58985B7C3ED_TisU3CWaitUntilCountOrTimeoutAsyncU3Ed__31_t227D1F5F19C948CA8C23C80B5F19147D4AAED14F_m4FDC6642836D25291A0E343845527873A88013C9(_thisAdjusted, ___awaiter0, ___stateMachine1, method);
}
// System.Void System.Runtime.CompilerServices.AsyncTaskMethodBuilder`1<System.Boolean>::Start<System.Threading.SemaphoreSlim_<WaitUntilCountOrTimeoutAsync>d__31>(TStateMachine&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AsyncTaskMethodBuilder_1_Start_TisU3CWaitUntilCountOrTimeoutAsyncU3Ed__31_t227D1F5F19C948CA8C23C80B5F19147D4AAED14F_m67A42FEF4C09E843DDA99A81F84E675F9AB87CF9_gshared (AsyncTaskMethodBuilder_1_tABE1DEF12F121D6FC8ABF04869ED964FF83EA9B5 * __this, U3CWaitUntilCountOrTimeoutAsyncU3Ed__31_t227D1F5F19C948CA8C23C80B5F19147D4AAED14F * ___stateMachine0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AsyncTaskMethodBuilder_1_Start_TisU3CWaitUntilCountOrTimeoutAsyncU3Ed__31_t227D1F5F19C948CA8C23C80B5F19147D4AAED14F_m67A42FEF4C09E843DDA99A81F84E675F9AB87CF9_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ExecutionContextSwitcher_t11B7DEE83408478EE3D5E29C988E5385AA9D7277  V_0;
	memset((&V_0), 0, sizeof(V_0));
	Exception_t * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	void* __leave_targets_storage = alloca(sizeof(int32_t) * 1);
	il2cpp::utils::LeaveTargetStack __leave_targets(__leave_targets_storage);
	NO_UNUSED_WARNING (__leave_targets);
	{
		goto IL_0018;
	}
	{
		ArgumentNullException_tFB5C4621957BC53A7D1B4FDD5C38B4D6E15DB8FB * L_1 = (ArgumentNullException_tFB5C4621957BC53A7D1B4FDD5C38B4D6E15DB8FB *)il2cpp_codegen_object_new(ArgumentNullException_tFB5C4621957BC53A7D1B4FDD5C38B4D6E15DB8FB_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m81AB157B93BFE2FBFDB08B88F84B444293042F97(L_1, (String_t*)_stringLiteralC70965A7D491520CA8D04D4EA01613EFED3309E0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1, AsyncTaskMethodBuilder_1_Start_TisU3CWaitUntilCountOrTimeoutAsyncU3Ed__31_t227D1F5F19C948CA8C23C80B5F19147D4AAED14F_m67A42FEF4C09E843DDA99A81F84E675F9AB87CF9_RuntimeMethod_var);
	}

IL_0018:
	{
		il2cpp_codegen_initobj((&V_0), sizeof(ExecutionContextSwitcher_t11B7DEE83408478EE3D5E29C988E5385AA9D7277 ));
		RuntimeHelpers_PrepareConstrainedRegions_m4A4D3987FEE068EE30D1ABC4005CDD29D5C52560(/*hidden argument*/NULL);
	}

IL_0025:
	try
	{ // begin try (depth: 1)
		IL2CPP_RUNTIME_CLASS_INIT(ExecutionContext_t16AC73BB21FEEEAD34A017877AC18DD8BB836414_il2cpp_TypeInfo_var);
		ExecutionContext_EstablishCopyOnWriteScope_m526741703B27626D3AD07B14ADA3FECBF5C4E8DD((ExecutionContextSwitcher_t11B7DEE83408478EE3D5E29C988E5385AA9D7277 *)(ExecutionContextSwitcher_t11B7DEE83408478EE3D5E29C988E5385AA9D7277 *)(&V_0), /*hidden argument*/NULL);
		U3CWaitUntilCountOrTimeoutAsyncU3Ed__31_t227D1F5F19C948CA8C23C80B5F19147D4AAED14F * L_2 = ___stateMachine0;
		U3CWaitUntilCountOrTimeoutAsyncU3Ed__31_MoveNext_mB5E635B57C016D9A7039342703EB200F7BE8218E((U3CWaitUntilCountOrTimeoutAsyncU3Ed__31_t227D1F5F19C948CA8C23C80B5F19147D4AAED14F *)(U3CWaitUntilCountOrTimeoutAsyncU3Ed__31_t227D1F5F19C948CA8C23C80B5F19147D4AAED14F *)L_2, /*hidden argument*/NULL);
		IL2CPP_LEAVE(0x42, FINALLY_003a);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_003a;
	}

FINALLY_003a:
	{ // begin finally (depth: 1)
		ExecutionContextSwitcher_Undo_mEC7752EB8502405D0F45F0E337C1B1FF34B74BF8((ExecutionContextSwitcher_t11B7DEE83408478EE3D5E29C988E5385AA9D7277 *)(ExecutionContextSwitcher_t11B7DEE83408478EE3D5E29C988E5385AA9D7277 *)(&V_0), /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(58)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(58)
	{
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
		IL2CPP_JUMP_TBL(0x42, IL_0042)
	}

IL_0042:
	{
		return;
	}
}
IL2CPP_EXTERN_C  void AsyncTaskMethodBuilder_1_Start_TisU3CWaitUntilCountOrTimeoutAsyncU3Ed__31_t227D1F5F19C948CA8C23C80B5F19147D4AAED14F_m67A42FEF4C09E843DDA99A81F84E675F9AB87CF9_AdjustorThunk (RuntimeObject * __this, U3CWaitUntilCountOrTimeoutAsyncU3Ed__31_t227D1F5F19C948CA8C23C80B5F19147D4AAED14F * ___stateMachine0, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	AsyncTaskMethodBuilder_1_tABE1DEF12F121D6FC8ABF04869ED964FF83EA9B5 * _thisAdjusted = reinterpret_cast<AsyncTaskMethodBuilder_1_tABE1DEF12F121D6FC8ABF04869ED964FF83EA9B5 *>(__this + _offset);
	AsyncTaskMethodBuilder_1_Start_TisU3CWaitUntilCountOrTimeoutAsyncU3Ed__31_t227D1F5F19C948CA8C23C80B5F19147D4AAED14F_m67A42FEF4C09E843DDA99A81F84E675F9AB87CF9(_thisAdjusted, ___stateMachine0, method);
}
// System.Void System.Runtime.CompilerServices.AsyncTaskMethodBuilder`1<System.Object>::AwaitUnsafeOnCompleted<System.Object,System.Object>(TAwaiter&,TStateMachine&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AsyncTaskMethodBuilder_1_AwaitUnsafeOnCompleted_TisRuntimeObject_TisRuntimeObject_mD7F0BC5F052B851265FF930DD5AB23C8E96269DE_gshared (AsyncTaskMethodBuilder_1_tDD2A3BA099C327938EA03C35FDB1A7502BA73020 * __this, RuntimeObject ** ___awaiter0, RuntimeObject ** ___stateMachine1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AsyncTaskMethodBuilder_1_AwaitUnsafeOnCompleted_TisRuntimeObject_TisRuntimeObject_mD7F0BC5F052B851265FF930DD5AB23C8E96269DE_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	MoveNextRunner_tFAEA0BEDD353E2E34E8E287C67B1F5572FD30C2D * V_0 = NULL;
	Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 * V_1 = NULL;
	Task_1_tC1805497876E88B78A2B0CB81C6409E0B381AC17 * V_2 = NULL;
	Exception_t * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	void* __leave_targets_storage = alloca(sizeof(int32_t) * 2);
	il2cpp::utils::LeaveTargetStack __leave_targets(__leave_targets_storage);
	NO_UNUSED_WARNING (__leave_targets);
	AsyncMethodBuilderCore_t2C85055E04767C52B9F66144476FCBF500DBFA34 * G_B2_0 = NULL;
	AsyncMethodBuilderCore_t2C85055E04767C52B9F66144476FCBF500DBFA34 * G_B1_0 = NULL;
	Task_1_tC1805497876E88B78A2B0CB81C6409E0B381AC17 * G_B3_0 = NULL;
	AsyncMethodBuilderCore_t2C85055E04767C52B9F66144476FCBF500DBFA34 * G_B3_1 = NULL;

IL_0000:
	try
	{ // begin try (depth: 1)
		{
			V_0 = (MoveNextRunner_tFAEA0BEDD353E2E34E8E287C67B1F5572FD30C2D *)NULL;
			AsyncMethodBuilderCore_t2C85055E04767C52B9F66144476FCBF500DBFA34 * L_0 = (AsyncMethodBuilderCore_t2C85055E04767C52B9F66144476FCBF500DBFA34 *)__this->get_address_of_m_coreState_1();
			bool L_1 = AsyncCausalityTracer_get_LoggingOn_mE0A03E121425371B1D1B65640172137C3B8EEA15(/*hidden argument*/NULL);
			G_B1_0 = L_0;
			if (L_1)
			{
				G_B2_0 = L_0;
				goto IL_0012;
			}
		}

IL_000f:
		{
			G_B3_0 = ((Task_1_tC1805497876E88B78A2B0CB81C6409E0B381AC17 *)(NULL));
			G_B3_1 = G_B1_0;
			goto IL_0018;
		}

IL_0012:
		{
			Task_1_tC1805497876E88B78A2B0CB81C6409E0B381AC17 * L_2 = AsyncTaskMethodBuilder_1_get_Task_m61DEC300353320E428E17DA0D59D61974F4415BB((AsyncTaskMethodBuilder_1_tDD2A3BA099C327938EA03C35FDB1A7502BA73020 *)(AsyncTaskMethodBuilder_1_tDD2A3BA099C327938EA03C35FDB1A7502BA73020 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 0));
			G_B3_0 = L_2;
			G_B3_1 = G_B2_0;
		}

IL_0018:
		{
			Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 * L_3 = AsyncMethodBuilderCore_GetCompletionAction_m7FE7F57CC452F0EDE870AB08EEB648E2027D4F5C((AsyncMethodBuilderCore_t2C85055E04767C52B9F66144476FCBF500DBFA34 *)(AsyncMethodBuilderCore_t2C85055E04767C52B9F66144476FCBF500DBFA34 *)G_B3_1, (Task_t804B25CFE3FC13AAEE16C8FA3BF52513F2A8DB60 *)G_B3_0, (MoveNextRunner_tFAEA0BEDD353E2E34E8E287C67B1F5572FD30C2D **)(MoveNextRunner_tFAEA0BEDD353E2E34E8E287C67B1F5572FD30C2D **)(&V_0), /*hidden argument*/NULL);
			V_1 = (Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 *)L_3;
			AsyncMethodBuilderCore_t2C85055E04767C52B9F66144476FCBF500DBFA34 * L_4 = (AsyncMethodBuilderCore_t2C85055E04767C52B9F66144476FCBF500DBFA34 *)__this->get_address_of_m_coreState_1();
			RuntimeObject* L_5 = (RuntimeObject*)L_4->get_m_stateMachine_0();
			if (L_5)
			{
				goto IL_004c;
			}
		}

IL_002d:
		{
			Task_1_tC1805497876E88B78A2B0CB81C6409E0B381AC17 * L_6 = AsyncTaskMethodBuilder_1_get_Task_m61DEC300353320E428E17DA0D59D61974F4415BB((AsyncTaskMethodBuilder_1_tDD2A3BA099C327938EA03C35FDB1A7502BA73020 *)(AsyncTaskMethodBuilder_1_tDD2A3BA099C327938EA03C35FDB1A7502BA73020 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 0));
			V_2 = (Task_1_tC1805497876E88B78A2B0CB81C6409E0B381AC17 *)L_6;
			AsyncMethodBuilderCore_t2C85055E04767C52B9F66144476FCBF500DBFA34 * L_7 = (AsyncMethodBuilderCore_t2C85055E04767C52B9F66144476FCBF500DBFA34 *)__this->get_address_of_m_coreState_1();
			RuntimeObject ** L_8 = ___stateMachine1;
			RuntimeObject * L_9 = (*(RuntimeObject **)L_8);
			MoveNextRunner_tFAEA0BEDD353E2E34E8E287C67B1F5572FD30C2D * L_10 = V_0;
			Task_1_tC1805497876E88B78A2B0CB81C6409E0B381AC17 * L_11 = V_2;
			AsyncMethodBuilderCore_PostBoxInitialization_m22C1D9A2745255C6FC1426D4CB0C4355FBDA07E3((AsyncMethodBuilderCore_t2C85055E04767C52B9F66144476FCBF500DBFA34 *)(AsyncMethodBuilderCore_t2C85055E04767C52B9F66144476FCBF500DBFA34 *)L_7, (RuntimeObject*)L_9, (MoveNextRunner_tFAEA0BEDD353E2E34E8E287C67B1F5572FD30C2D *)L_10, (Task_t804B25CFE3FC13AAEE16C8FA3BF52513F2A8DB60 *)L_11, /*hidden argument*/NULL);
		}

IL_004c:
		{
			RuntimeObject ** L_12 = ___awaiter0;
			Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 * L_13 = V_1;
			NullCheck((RuntimeObject*)(*L_12));
			InterfaceActionInvoker1< Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 * >::Invoke(0 /* System.Void System.Runtime.CompilerServices.ICriticalNotifyCompletion::UnsafeOnCompleted(System.Action) */, ICriticalNotifyCompletion_t3F4FCEA29CDCACD07747F3A1D6D1406521556838_il2cpp_TypeInfo_var, (RuntimeObject*)(*L_12), (Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 *)L_13);
			goto IL_0063;
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Exception_t_il2cpp_TypeInfo_var, il2cpp_codegen_object_class(e.ex)))
			goto CATCH_005b;
		throw e;
	}

CATCH_005b:
	{ // begin catch(System.Exception)
		AsyncMethodBuilderCore_ThrowAsync_m69800E9752B30F7A1D824C2F1EBDEBA44BA75610((Exception_t *)((Exception_t *)__exception_local), (SynchronizationContext_t17D9365B5E0D30A0910A16FA4351C525232EF069 *)NULL, /*hidden argument*/NULL);
		goto IL_0063;
	} // end catch (depth: 1)

IL_0063:
	{
		return;
	}
}
IL2CPP_EXTERN_C  void AsyncTaskMethodBuilder_1_AwaitUnsafeOnCompleted_TisRuntimeObject_TisRuntimeObject_mD7F0BC5F052B851265FF930DD5AB23C8E96269DE_AdjustorThunk (RuntimeObject * __this, RuntimeObject ** ___awaiter0, RuntimeObject ** ___stateMachine1, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	AsyncTaskMethodBuilder_1_tDD2A3BA099C327938EA03C35FDB1A7502BA73020 * _thisAdjusted = reinterpret_cast<AsyncTaskMethodBuilder_1_tDD2A3BA099C327938EA03C35FDB1A7502BA73020 *>(__this + _offset);
	AsyncTaskMethodBuilder_1_AwaitUnsafeOnCompleted_TisRuntimeObject_TisRuntimeObject_mD7F0BC5F052B851265FF930DD5AB23C8E96269DE(_thisAdjusted, ___awaiter0, ___stateMachine1, method);
}
// System.Void System.Runtime.CompilerServices.AsyncTaskMethodBuilder`1<System.Object>::Start<System.Object>(TStateMachine&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AsyncTaskMethodBuilder_1_Start_TisRuntimeObject_m25C57EA59EF179635A81530036C12082EE50878B_gshared (AsyncTaskMethodBuilder_1_tDD2A3BA099C327938EA03C35FDB1A7502BA73020 * __this, RuntimeObject ** ___stateMachine0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AsyncTaskMethodBuilder_1_Start_TisRuntimeObject_m25C57EA59EF179635A81530036C12082EE50878B_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ExecutionContextSwitcher_t11B7DEE83408478EE3D5E29C988E5385AA9D7277  V_0;
	memset((&V_0), 0, sizeof(V_0));
	Exception_t * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	void* __leave_targets_storage = alloca(sizeof(int32_t) * 1);
	il2cpp::utils::LeaveTargetStack __leave_targets(__leave_targets_storage);
	NO_UNUSED_WARNING (__leave_targets);
	{
		RuntimeObject ** L_0 = ___stateMachine0;
		RuntimeObject * L_1 = (*(RuntimeObject **)L_0);
		if (L_1)
		{
			goto IL_0018;
		}
	}
	{
		ArgumentNullException_tFB5C4621957BC53A7D1B4FDD5C38B4D6E15DB8FB * L_2 = (ArgumentNullException_tFB5C4621957BC53A7D1B4FDD5C38B4D6E15DB8FB *)il2cpp_codegen_object_new(ArgumentNullException_tFB5C4621957BC53A7D1B4FDD5C38B4D6E15DB8FB_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m81AB157B93BFE2FBFDB08B88F84B444293042F97(L_2, (String_t*)_stringLiteralC70965A7D491520CA8D04D4EA01613EFED3309E0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2, AsyncTaskMethodBuilder_1_Start_TisRuntimeObject_m25C57EA59EF179635A81530036C12082EE50878B_RuntimeMethod_var);
	}

IL_0018:
	{
		il2cpp_codegen_initobj((&V_0), sizeof(ExecutionContextSwitcher_t11B7DEE83408478EE3D5E29C988E5385AA9D7277 ));
		RuntimeHelpers_PrepareConstrainedRegions_m4A4D3987FEE068EE30D1ABC4005CDD29D5C52560(/*hidden argument*/NULL);
	}

IL_0025:
	try
	{ // begin try (depth: 1)
		IL2CPP_RUNTIME_CLASS_INIT(ExecutionContext_t16AC73BB21FEEEAD34A017877AC18DD8BB836414_il2cpp_TypeInfo_var);
		ExecutionContext_EstablishCopyOnWriteScope_m526741703B27626D3AD07B14ADA3FECBF5C4E8DD((ExecutionContextSwitcher_t11B7DEE83408478EE3D5E29C988E5385AA9D7277 *)(ExecutionContextSwitcher_t11B7DEE83408478EE3D5E29C988E5385AA9D7277 *)(&V_0), /*hidden argument*/NULL);
		RuntimeObject ** L_3 = ___stateMachine0;
		NullCheck((RuntimeObject*)(*L_3));
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.Runtime.CompilerServices.IAsyncStateMachine::MoveNext() */, IAsyncStateMachine_tAE063F84A60E1058FCA4E3EA9F555D3462641F7D_il2cpp_TypeInfo_var, (RuntimeObject*)(*L_3));
		IL2CPP_LEAVE(0x42, FINALLY_003a);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_003a;
	}

FINALLY_003a:
	{ // begin finally (depth: 1)
		ExecutionContextSwitcher_Undo_mEC7752EB8502405D0F45F0E337C1B1FF34B74BF8((ExecutionContextSwitcher_t11B7DEE83408478EE3D5E29C988E5385AA9D7277 *)(ExecutionContextSwitcher_t11B7DEE83408478EE3D5E29C988E5385AA9D7277 *)(&V_0), /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(58)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(58)
	{
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
		IL2CPP_JUMP_TBL(0x42, IL_0042)
	}

IL_0042:
	{
		return;
	}
}
IL2CPP_EXTERN_C  void AsyncTaskMethodBuilder_1_Start_TisRuntimeObject_m25C57EA59EF179635A81530036C12082EE50878B_AdjustorThunk (RuntimeObject * __this, RuntimeObject ** ___stateMachine0, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	AsyncTaskMethodBuilder_1_tDD2A3BA099C327938EA03C35FDB1A7502BA73020 * _thisAdjusted = reinterpret_cast<AsyncTaskMethodBuilder_1_tDD2A3BA099C327938EA03C35FDB1A7502BA73020 *>(__this + _offset);
	AsyncTaskMethodBuilder_1_Start_TisRuntimeObject_m25C57EA59EF179635A81530036C12082EE50878B(_thisAdjusted, ___stateMachine0, method);
}
// Unity.Collections.NativeArray`1<U> Obi.ObiNativeList`1<Obi.Aabb>::AsNativeArray<Obi.Aabb>()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR NativeArray_1_t43E7EA4C279410A42CF9A0F46F22F5A084752842  ObiNativeList_1_AsNativeArray_TisAabb_t4DF77A399A9E4CB5D52707E4B8B53D3483CD84CF_m6DFBD6D5AC9B3A67B4BF8BFCB44AC26092696A62_gshared (ObiNativeList_1_t70F6D07F08AA8D90F04FAF88B56FBA49BF80957D * __this, const RuntimeMethod* method)
{
	{
		// return AsNativeArray<U>(m_Count);
		int32_t L_0 = (int32_t)__this->get_m_Count_4();
		NullCheck((ObiNativeList_1_t70F6D07F08AA8D90F04FAF88B56FBA49BF80957D *)__this);
		NativeArray_1_t43E7EA4C279410A42CF9A0F46F22F5A084752842  L_1 = ((  NativeArray_1_t43E7EA4C279410A42CF9A0F46F22F5A084752842  (*) (ObiNativeList_1_t70F6D07F08AA8D90F04FAF88B56FBA49BF80957D *, int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->methodPointer)((ObiNativeList_1_t70F6D07F08AA8D90F04FAF88B56FBA49BF80957D *)__this, (int32_t)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		return (NativeArray_1_t43E7EA4C279410A42CF9A0F46F22F5A084752842 )L_1;
	}
}
// Unity.Collections.NativeArray`1<U> Obi.ObiNativeList`1<Obi.Aabb>::AsNativeArray<Obi.Aabb>(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR NativeArray_1_t43E7EA4C279410A42CF9A0F46F22F5A084752842  ObiNativeList_1_AsNativeArray_TisAabb_t4DF77A399A9E4CB5D52707E4B8B53D3483CD84CF_m98F0E2C967448F43B8BE5C67B429850AF66B556B_gshared (ObiNativeList_1_t70F6D07F08AA8D90F04FAF88B56FBA49BF80957D * __this, int32_t ___arrayLength0, const RuntimeMethod* method)
{
	{
		// NativeArray<U> array = NativeArrayUnsafeUtility.ConvertExistingDataToNativeArray<U>(m_AlignedPtr, arrayLength, Allocator.None);
		void* L_0 = (void*)__this->get_m_AlignedPtr_1();
		int32_t L_1 = ___arrayLength0;
		NativeArray_1_t43E7EA4C279410A42CF9A0F46F22F5A084752842  L_2 = ((  NativeArray_1_t43E7EA4C279410A42CF9A0F46F22F5A084752842  (*) (void*, int32_t, int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->methodPointer)((void*)(void*)L_0, (int32_t)L_1, (int32_t)1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		// return array;
		return (NativeArray_1_t43E7EA4C279410A42CF9A0F46F22F5A084752842 )L_2;
	}
}
// Unity.Collections.NativeArray`1<U> Obi.ObiNativeList`1<Obi.AffineTransform>::AsNativeArray<Obi.AffineTransform>()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR NativeArray_1_tA435936F3C77B97EDF0C46997DD48EF589966961  ObiNativeList_1_AsNativeArray_TisAffineTransform_tC1AED177DBAAED2A3AD2CCA40394AAA8C3DF0193_m9B1CC5A9103B14F6FCEAD2C8FA14B95DF9690D1B_gshared (ObiNativeList_1_t03BFDE41D0D9A18FFB5DAA42B75CB84B78E2C769 * __this, const RuntimeMethod* method)
{
	{
		// return AsNativeArray<U>(m_Count);
		int32_t L_0 = (int32_t)__this->get_m_Count_4();
		NullCheck((ObiNativeList_1_t03BFDE41D0D9A18FFB5DAA42B75CB84B78E2C769 *)__this);
		NativeArray_1_tA435936F3C77B97EDF0C46997DD48EF589966961  L_1 = ((  NativeArray_1_tA435936F3C77B97EDF0C46997DD48EF589966961  (*) (ObiNativeList_1_t03BFDE41D0D9A18FFB5DAA42B75CB84B78E2C769 *, int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->methodPointer)((ObiNativeList_1_t03BFDE41D0D9A18FFB5DAA42B75CB84B78E2C769 *)__this, (int32_t)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		return (NativeArray_1_tA435936F3C77B97EDF0C46997DD48EF589966961 )L_1;
	}
}
// Unity.Collections.NativeArray`1<U> Obi.ObiNativeList`1<Obi.AffineTransform>::AsNativeArray<Obi.AffineTransform>(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR NativeArray_1_tA435936F3C77B97EDF0C46997DD48EF589966961  ObiNativeList_1_AsNativeArray_TisAffineTransform_tC1AED177DBAAED2A3AD2CCA40394AAA8C3DF0193_m1556996FD43EDC4E44B6A91B133A1E6EDB025732_gshared (ObiNativeList_1_t03BFDE41D0D9A18FFB5DAA42B75CB84B78E2C769 * __this, int32_t ___arrayLength0, const RuntimeMethod* method)
{
	{
		// NativeArray<U> array = NativeArrayUnsafeUtility.ConvertExistingDataToNativeArray<U>(m_AlignedPtr, arrayLength, Allocator.None);
		void* L_0 = (void*)__this->get_m_AlignedPtr_1();
		int32_t L_1 = ___arrayLength0;
		NativeArray_1_tA435936F3C77B97EDF0C46997DD48EF589966961  L_2 = ((  NativeArray_1_tA435936F3C77B97EDF0C46997DD48EF589966961  (*) (void*, int32_t, int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->methodPointer)((void*)(void*)L_0, (int32_t)L_1, (int32_t)1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		// return array;
		return (NativeArray_1_tA435936F3C77B97EDF0C46997DD48EF589966961 )L_2;
	}
}
// Unity.Collections.NativeArray`1<U> Obi.ObiNativeList`1<Obi.BIHNode>::AsNativeArray<Obi.BIHNode>()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR NativeArray_1_t76B0EA4D6D2405DF9C6DE308D69483AF146C1D3C  ObiNativeList_1_AsNativeArray_TisBIHNode_tA8B64B5175479DEC9FBF8312EF5F2163C1FE0D05_m78E54BF3473B7C47FB5479DC3F0185D21D8360B8_gshared (ObiNativeList_1_tAB1F96F0D20C4C225040A67D9C56B627503C46A7 * __this, const RuntimeMethod* method)
{
	{
		// return AsNativeArray<U>(m_Count);
		int32_t L_0 = (int32_t)__this->get_m_Count_4();
		NullCheck((ObiNativeList_1_tAB1F96F0D20C4C225040A67D9C56B627503C46A7 *)__this);
		NativeArray_1_t76B0EA4D6D2405DF9C6DE308D69483AF146C1D3C  L_1 = ((  NativeArray_1_t76B0EA4D6D2405DF9C6DE308D69483AF146C1D3C  (*) (ObiNativeList_1_tAB1F96F0D20C4C225040A67D9C56B627503C46A7 *, int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->methodPointer)((ObiNativeList_1_tAB1F96F0D20C4C225040A67D9C56B627503C46A7 *)__this, (int32_t)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		return (NativeArray_1_t76B0EA4D6D2405DF9C6DE308D69483AF146C1D3C )L_1;
	}
}
// Unity.Collections.NativeArray`1<U> Obi.ObiNativeList`1<Obi.BIHNode>::AsNativeArray<Obi.BIHNode>(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR NativeArray_1_t76B0EA4D6D2405DF9C6DE308D69483AF146C1D3C  ObiNativeList_1_AsNativeArray_TisBIHNode_tA8B64B5175479DEC9FBF8312EF5F2163C1FE0D05_m7DDDE0054E9C5019E5F468FCBA30F11327C025AF_gshared (ObiNativeList_1_tAB1F96F0D20C4C225040A67D9C56B627503C46A7 * __this, int32_t ___arrayLength0, const RuntimeMethod* method)
{
	{
		// NativeArray<U> array = NativeArrayUnsafeUtility.ConvertExistingDataToNativeArray<U>(m_AlignedPtr, arrayLength, Allocator.None);
		void* L_0 = (void*)__this->get_m_AlignedPtr_1();
		int32_t L_1 = ___arrayLength0;
		NativeArray_1_t76B0EA4D6D2405DF9C6DE308D69483AF146C1D3C  L_2 = ((  NativeArray_1_t76B0EA4D6D2405DF9C6DE308D69483AF146C1D3C  (*) (void*, int32_t, int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->methodPointer)((void*)(void*)L_0, (int32_t)L_1, (int32_t)1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		// return array;
		return (NativeArray_1_t76B0EA4D6D2405DF9C6DE308D69483AF146C1D3C )L_2;
	}
}
// Unity.Collections.NativeArray`1<U> Obi.ObiNativeList`1<Obi.CellSpan>::AsNativeArray<Obi.CellSpan>()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR NativeArray_1_t1414BC68B8A64DA9A80F71AE9C749A11FEBEDEC4  ObiNativeList_1_AsNativeArray_TisCellSpan_tE5A1247201F556E0880AD919A58F92DEA8402291_mBDF5DC1E3EABBB668CB8F54C01B420F108B19986_gshared (ObiNativeList_1_t32C81553D1B41E21CC334835948700A53FAFF834 * __this, const RuntimeMethod* method)
{
	{
		// return AsNativeArray<U>(m_Count);
		int32_t L_0 = (int32_t)__this->get_m_Count_4();
		NullCheck((ObiNativeList_1_t32C81553D1B41E21CC334835948700A53FAFF834 *)__this);
		NativeArray_1_t1414BC68B8A64DA9A80F71AE9C749A11FEBEDEC4  L_1 = ((  NativeArray_1_t1414BC68B8A64DA9A80F71AE9C749A11FEBEDEC4  (*) (ObiNativeList_1_t32C81553D1B41E21CC334835948700A53FAFF834 *, int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->methodPointer)((ObiNativeList_1_t32C81553D1B41E21CC334835948700A53FAFF834 *)__this, (int32_t)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		return (NativeArray_1_t1414BC68B8A64DA9A80F71AE9C749A11FEBEDEC4 )L_1;
	}
}
// Unity.Collections.NativeArray`1<U> Obi.ObiNativeList`1<Obi.CellSpan>::AsNativeArray<Obi.CellSpan>(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR NativeArray_1_t1414BC68B8A64DA9A80F71AE9C749A11FEBEDEC4  ObiNativeList_1_AsNativeArray_TisCellSpan_tE5A1247201F556E0880AD919A58F92DEA8402291_m696D3FEBFECB7172041325146EC33811A71B7E6D_gshared (ObiNativeList_1_t32C81553D1B41E21CC334835948700A53FAFF834 * __this, int32_t ___arrayLength0, const RuntimeMethod* method)
{
	{
		// NativeArray<U> array = NativeArrayUnsafeUtility.ConvertExistingDataToNativeArray<U>(m_AlignedPtr, arrayLength, Allocator.None);
		void* L_0 = (void*)__this->get_m_AlignedPtr_1();
		int32_t L_1 = ___arrayLength0;
		NativeArray_1_t1414BC68B8A64DA9A80F71AE9C749A11FEBEDEC4  L_2 = ((  NativeArray_1_t1414BC68B8A64DA9A80F71AE9C749A11FEBEDEC4  (*) (void*, int32_t, int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->methodPointer)((void*)(void*)L_0, (int32_t)L_1, (int32_t)1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		// return array;
		return (NativeArray_1_t1414BC68B8A64DA9A80F71AE9C749A11FEBEDEC4 )L_2;
	}
}
// Unity.Collections.NativeArray`1<U> Obi.ObiNativeList`1<Obi.ColliderRigidbody>::AsNativeArray<Obi.ColliderRigidbody>()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR NativeArray_1_t572B8FA0970987F9DED50CD9493AA60298DD0644  ObiNativeList_1_AsNativeArray_TisColliderRigidbody_tC1FF76AA900641BCDF2B31E90931360DA75EAA0D_m69D9F9DBE399A9C1DCBC6BA8407B96084B3333C8_gshared (ObiNativeList_1_t05A6C6A8DA09130C90EB95364D1840CBCDA790F9 * __this, const RuntimeMethod* method)
{
	{
		// return AsNativeArray<U>(m_Count);
		int32_t L_0 = (int32_t)__this->get_m_Count_4();
		NullCheck((ObiNativeList_1_t05A6C6A8DA09130C90EB95364D1840CBCDA790F9 *)__this);
		NativeArray_1_t572B8FA0970987F9DED50CD9493AA60298DD0644  L_1 = ((  NativeArray_1_t572B8FA0970987F9DED50CD9493AA60298DD0644  (*) (ObiNativeList_1_t05A6C6A8DA09130C90EB95364D1840CBCDA790F9 *, int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->methodPointer)((ObiNativeList_1_t05A6C6A8DA09130C90EB95364D1840CBCDA790F9 *)__this, (int32_t)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		return (NativeArray_1_t572B8FA0970987F9DED50CD9493AA60298DD0644 )L_1;
	}
}
// Unity.Collections.NativeArray`1<U> Obi.ObiNativeList`1<Obi.ColliderRigidbody>::AsNativeArray<Obi.ColliderRigidbody>(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR NativeArray_1_t572B8FA0970987F9DED50CD9493AA60298DD0644  ObiNativeList_1_AsNativeArray_TisColliderRigidbody_tC1FF76AA900641BCDF2B31E90931360DA75EAA0D_mCBC4D0FC0F11014C5EA968AFC60560B4C23AA167_gshared (ObiNativeList_1_t05A6C6A8DA09130C90EB95364D1840CBCDA790F9 * __this, int32_t ___arrayLength0, const RuntimeMethod* method)
{
	{
		// NativeArray<U> array = NativeArrayUnsafeUtility.ConvertExistingDataToNativeArray<U>(m_AlignedPtr, arrayLength, Allocator.None);
		void* L_0 = (void*)__this->get_m_AlignedPtr_1();
		int32_t L_1 = ___arrayLength0;
		NativeArray_1_t572B8FA0970987F9DED50CD9493AA60298DD0644  L_2 = ((  NativeArray_1_t572B8FA0970987F9DED50CD9493AA60298DD0644  (*) (void*, int32_t, int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->methodPointer)((void*)(void*)L_0, (int32_t)L_1, (int32_t)1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		// return array;
		return (NativeArray_1_t572B8FA0970987F9DED50CD9493AA60298DD0644 )L_2;
	}
}
// Unity.Collections.NativeArray`1<U> Obi.ObiNativeList`1<Obi.ColliderShape>::AsNativeArray<Obi.ColliderShape>()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR NativeArray_1_t10D0F10FEFFE209A84987F5B88F042A28DAEFA03  ObiNativeList_1_AsNativeArray_TisColliderShape_t7AF5542901AEB2CFA54B6BFD8C852EC91554F586_mB37809CF734B9F90063BF026F3B6BDDDA5AB23E3_gshared (ObiNativeList_1_t30AA57B53B1359362269E706BCAAE5664D2065C0 * __this, const RuntimeMethod* method)
{
	{
		// return AsNativeArray<U>(m_Count);
		int32_t L_0 = (int32_t)__this->get_m_Count_4();
		NullCheck((ObiNativeList_1_t30AA57B53B1359362269E706BCAAE5664D2065C0 *)__this);
		NativeArray_1_t10D0F10FEFFE209A84987F5B88F042A28DAEFA03  L_1 = ((  NativeArray_1_t10D0F10FEFFE209A84987F5B88F042A28DAEFA03  (*) (ObiNativeList_1_t30AA57B53B1359362269E706BCAAE5664D2065C0 *, int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->methodPointer)((ObiNativeList_1_t30AA57B53B1359362269E706BCAAE5664D2065C0 *)__this, (int32_t)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		return (NativeArray_1_t10D0F10FEFFE209A84987F5B88F042A28DAEFA03 )L_1;
	}
}
// Unity.Collections.NativeArray`1<U> Obi.ObiNativeList`1<Obi.ColliderShape>::AsNativeArray<Obi.ColliderShape>(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR NativeArray_1_t10D0F10FEFFE209A84987F5B88F042A28DAEFA03  ObiNativeList_1_AsNativeArray_TisColliderShape_t7AF5542901AEB2CFA54B6BFD8C852EC91554F586_mC7CD7756C478D3877310AA3ACE62B3C043FE37AD_gshared (ObiNativeList_1_t30AA57B53B1359362269E706BCAAE5664D2065C0 * __this, int32_t ___arrayLength0, const RuntimeMethod* method)
{
	{
		// NativeArray<U> array = NativeArrayUnsafeUtility.ConvertExistingDataToNativeArray<U>(m_AlignedPtr, arrayLength, Allocator.None);
		void* L_0 = (void*)__this->get_m_AlignedPtr_1();
		int32_t L_1 = ___arrayLength0;
		NativeArray_1_t10D0F10FEFFE209A84987F5B88F042A28DAEFA03  L_2 = ((  NativeArray_1_t10D0F10FEFFE209A84987F5B88F042A28DAEFA03  (*) (void*, int32_t, int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->methodPointer)((void*)(void*)L_0, (int32_t)L_1, (int32_t)1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		// return array;
		return (NativeArray_1_t10D0F10FEFFE209A84987F5B88F042A28DAEFA03 )L_2;
	}
}
// Unity.Collections.NativeArray`1<U> Obi.ObiNativeList`1<Obi.CollisionMaterial>::AsNativeArray<Obi.CollisionMaterial>()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR NativeArray_1_t9C2DA1A93847BC1D3620655C78E87ACDA7CEC0BB  ObiNativeList_1_AsNativeArray_TisCollisionMaterial_t7582B504E28F3D7E580DBB66ECD74F967C92E758_m2A4D901CA851EED1198439032D61050E8CEDC8C9_gshared (ObiNativeList_1_t0B4D5B3FAA024A9AC166F149B983A78AF3080145 * __this, const RuntimeMethod* method)
{
	{
		// return AsNativeArray<U>(m_Count);
		int32_t L_0 = (int32_t)__this->get_m_Count_4();
		NullCheck((ObiNativeList_1_t0B4D5B3FAA024A9AC166F149B983A78AF3080145 *)__this);
		NativeArray_1_t9C2DA1A93847BC1D3620655C78E87ACDA7CEC0BB  L_1 = ((  NativeArray_1_t9C2DA1A93847BC1D3620655C78E87ACDA7CEC0BB  (*) (ObiNativeList_1_t0B4D5B3FAA024A9AC166F149B983A78AF3080145 *, int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->methodPointer)((ObiNativeList_1_t0B4D5B3FAA024A9AC166F149B983A78AF3080145 *)__this, (int32_t)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		return (NativeArray_1_t9C2DA1A93847BC1D3620655C78E87ACDA7CEC0BB )L_1;
	}
}
// Unity.Collections.NativeArray`1<U> Obi.ObiNativeList`1<Obi.CollisionMaterial>::AsNativeArray<Obi.CollisionMaterial>(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR NativeArray_1_t9C2DA1A93847BC1D3620655C78E87ACDA7CEC0BB  ObiNativeList_1_AsNativeArray_TisCollisionMaterial_t7582B504E28F3D7E580DBB66ECD74F967C92E758_m41AA3552A74503EA2538A661F05C717D1D46D75A_gshared (ObiNativeList_1_t0B4D5B3FAA024A9AC166F149B983A78AF3080145 * __this, int32_t ___arrayLength0, const RuntimeMethod* method)
{
	{
		// NativeArray<U> array = NativeArrayUnsafeUtility.ConvertExistingDataToNativeArray<U>(m_AlignedPtr, arrayLength, Allocator.None);
		void* L_0 = (void*)__this->get_m_AlignedPtr_1();
		int32_t L_1 = ___arrayLength0;
		NativeArray_1_t9C2DA1A93847BC1D3620655C78E87ACDA7CEC0BB  L_2 = ((  NativeArray_1_t9C2DA1A93847BC1D3620655C78E87ACDA7CEC0BB  (*) (void*, int32_t, int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->methodPointer)((void*)(void*)L_0, (int32_t)L_1, (int32_t)1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		// return array;
		return (NativeArray_1_t9C2DA1A93847BC1D3620655C78E87ACDA7CEC0BB )L_2;
	}
}
// Unity.Collections.NativeArray`1<U> Obi.ObiNativeList`1<Obi.DFNode>::AsNativeArray<Obi.DFNode>()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR NativeArray_1_tFA6A4A2D858B86A96EEA4C98F7A00D1C3C1B7045  ObiNativeList_1_AsNativeArray_TisDFNode_t8FC489FB11B708202922AF5F52E4AFEB7B205668_m5AA49A7D59642F5DDBDFD036E94D63C020CDFE53_gshared (ObiNativeList_1_t6ADADB0DAAC6C2F9D071750D398057641903EC70 * __this, const RuntimeMethod* method)
{
	{
		// return AsNativeArray<U>(m_Count);
		int32_t L_0 = (int32_t)__this->get_m_Count_4();
		NullCheck((ObiNativeList_1_t6ADADB0DAAC6C2F9D071750D398057641903EC70 *)__this);
		NativeArray_1_tFA6A4A2D858B86A96EEA4C98F7A00D1C3C1B7045  L_1 = ((  NativeArray_1_tFA6A4A2D858B86A96EEA4C98F7A00D1C3C1B7045  (*) (ObiNativeList_1_t6ADADB0DAAC6C2F9D071750D398057641903EC70 *, int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->methodPointer)((ObiNativeList_1_t6ADADB0DAAC6C2F9D071750D398057641903EC70 *)__this, (int32_t)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		return (NativeArray_1_tFA6A4A2D858B86A96EEA4C98F7A00D1C3C1B7045 )L_1;
	}
}
// Unity.Collections.NativeArray`1<U> Obi.ObiNativeList`1<Obi.DFNode>::AsNativeArray<Obi.DFNode>(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR NativeArray_1_tFA6A4A2D858B86A96EEA4C98F7A00D1C3C1B7045  ObiNativeList_1_AsNativeArray_TisDFNode_t8FC489FB11B708202922AF5F52E4AFEB7B205668_mF887BE4B3210694CEFECAAADA99760AA2FE95108_gshared (ObiNativeList_1_t6ADADB0DAAC6C2F9D071750D398057641903EC70 * __this, int32_t ___arrayLength0, const RuntimeMethod* method)
{
	{
		// NativeArray<U> array = NativeArrayUnsafeUtility.ConvertExistingDataToNativeArray<U>(m_AlignedPtr, arrayLength, Allocator.None);
		void* L_0 = (void*)__this->get_m_AlignedPtr_1();
		int32_t L_1 = ___arrayLength0;
		NativeArray_1_tFA6A4A2D858B86A96EEA4C98F7A00D1C3C1B7045  L_2 = ((  NativeArray_1_tFA6A4A2D858B86A96EEA4C98F7A00D1C3C1B7045  (*) (void*, int32_t, int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->methodPointer)((void*)(void*)L_0, (int32_t)L_1, (int32_t)1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		// return array;
		return (NativeArray_1_tFA6A4A2D858B86A96EEA4C98F7A00D1C3C1B7045 )L_2;
	}
}
// Unity.Collections.NativeArray`1<U> Obi.ObiNativeList`1<Obi.DistanceFieldHeader>::AsNativeArray<Obi.DistanceFieldHeader>()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR NativeArray_1_t1F82401C884356A2CEC88A9DC6CA01BF4295C31A  ObiNativeList_1_AsNativeArray_TisDistanceFieldHeader_tE66F34D3B1EDB411B0D6E1346D68383E03E33AD4_mA5B4CE4BE7308D6DEEE329523901D6B99A87FD05_gshared (ObiNativeList_1_t018D553481251B27F7FE696F676040DCB6C3F0EA * __this, const RuntimeMethod* method)
{
	{
		// return AsNativeArray<U>(m_Count);
		int32_t L_0 = (int32_t)__this->get_m_Count_4();
		NullCheck((ObiNativeList_1_t018D553481251B27F7FE696F676040DCB6C3F0EA *)__this);
		NativeArray_1_t1F82401C884356A2CEC88A9DC6CA01BF4295C31A  L_1 = ((  NativeArray_1_t1F82401C884356A2CEC88A9DC6CA01BF4295C31A  (*) (ObiNativeList_1_t018D553481251B27F7FE696F676040DCB6C3F0EA *, int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->methodPointer)((ObiNativeList_1_t018D553481251B27F7FE696F676040DCB6C3F0EA *)__this, (int32_t)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		return (NativeArray_1_t1F82401C884356A2CEC88A9DC6CA01BF4295C31A )L_1;
	}
}
// Unity.Collections.NativeArray`1<U> Obi.ObiNativeList`1<Obi.DistanceFieldHeader>::AsNativeArray<Obi.DistanceFieldHeader>(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR NativeArray_1_t1F82401C884356A2CEC88A9DC6CA01BF4295C31A  ObiNativeList_1_AsNativeArray_TisDistanceFieldHeader_tE66F34D3B1EDB411B0D6E1346D68383E03E33AD4_m052C3828CBF4880175AED00C27C3D90AE5130B17_gshared (ObiNativeList_1_t018D553481251B27F7FE696F676040DCB6C3F0EA * __this, int32_t ___arrayLength0, const RuntimeMethod* method)
{
	{
		// NativeArray<U> array = NativeArrayUnsafeUtility.ConvertExistingDataToNativeArray<U>(m_AlignedPtr, arrayLength, Allocator.None);
		void* L_0 = (void*)__this->get_m_AlignedPtr_1();
		int32_t L_1 = ___arrayLength0;
		NativeArray_1_t1F82401C884356A2CEC88A9DC6CA01BF4295C31A  L_2 = ((  NativeArray_1_t1F82401C884356A2CEC88A9DC6CA01BF4295C31A  (*) (void*, int32_t, int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->methodPointer)((void*)(void*)L_0, (int32_t)L_1, (int32_t)1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		// return array;
		return (NativeArray_1_t1F82401C884356A2CEC88A9DC6CA01BF4295C31A )L_2;
	}
}
// Unity.Collections.NativeArray`1<U> Obi.ObiNativeList`1<Obi.Edge>::AsNativeArray<Obi.Edge>()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR NativeArray_1_t7C51D2F91F4BF0F8A2B61735BA98C900AE793065  ObiNativeList_1_AsNativeArray_TisEdge_tB72B5CE4AD9C11C74696DF37DCED8FB9C2FFDD3C_m971B2EB25B366C3F48431948A382B5430993D070_gshared (ObiNativeList_1_t96FABDF83D294D0ABBFEFAE74DEFCD1CF2243AF7 * __this, const RuntimeMethod* method)
{
	{
		// return AsNativeArray<U>(m_Count);
		int32_t L_0 = (int32_t)__this->get_m_Count_4();
		NullCheck((ObiNativeList_1_t96FABDF83D294D0ABBFEFAE74DEFCD1CF2243AF7 *)__this);
		NativeArray_1_t7C51D2F91F4BF0F8A2B61735BA98C900AE793065  L_1 = ((  NativeArray_1_t7C51D2F91F4BF0F8A2B61735BA98C900AE793065  (*) (ObiNativeList_1_t96FABDF83D294D0ABBFEFAE74DEFCD1CF2243AF7 *, int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->methodPointer)((ObiNativeList_1_t96FABDF83D294D0ABBFEFAE74DEFCD1CF2243AF7 *)__this, (int32_t)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		return (NativeArray_1_t7C51D2F91F4BF0F8A2B61735BA98C900AE793065 )L_1;
	}
}
// Unity.Collections.NativeArray`1<U> Obi.ObiNativeList`1<Obi.Edge>::AsNativeArray<Obi.Edge>(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR NativeArray_1_t7C51D2F91F4BF0F8A2B61735BA98C900AE793065  ObiNativeList_1_AsNativeArray_TisEdge_tB72B5CE4AD9C11C74696DF37DCED8FB9C2FFDD3C_mBF692587F103DB667DE39E325F3C61A06F0AA4D2_gshared (ObiNativeList_1_t96FABDF83D294D0ABBFEFAE74DEFCD1CF2243AF7 * __this, int32_t ___arrayLength0, const RuntimeMethod* method)
{
	{
		// NativeArray<U> array = NativeArrayUnsafeUtility.ConvertExistingDataToNativeArray<U>(m_AlignedPtr, arrayLength, Allocator.None);
		void* L_0 = (void*)__this->get_m_AlignedPtr_1();
		int32_t L_1 = ___arrayLength0;
		NativeArray_1_t7C51D2F91F4BF0F8A2B61735BA98C900AE793065  L_2 = ((  NativeArray_1_t7C51D2F91F4BF0F8A2B61735BA98C900AE793065  (*) (void*, int32_t, int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->methodPointer)((void*)(void*)L_0, (int32_t)L_1, (int32_t)1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		// return array;
		return (NativeArray_1_t7C51D2F91F4BF0F8A2B61735BA98C900AE793065 )L_2;
	}
}
// Unity.Collections.NativeArray`1<U> Obi.ObiNativeList`1<Obi.EdgeMeshHeader>::AsNativeArray<Obi.EdgeMeshHeader>()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR NativeArray_1_tAFBEB18DC14E5B6EF8F550019E04E75AC3289B6A  ObiNativeList_1_AsNativeArray_TisEdgeMeshHeader_t235B26AE87E4F8BC38EC627430A9E0E344A03878_m4400E608FE9370B820CF2C43D21BEDA286AAE4D9_gshared (ObiNativeList_1_t82AAD6F0A45EA387F6031B345E681AE0DADF910C * __this, const RuntimeMethod* method)
{
	{
		// return AsNativeArray<U>(m_Count);
		int32_t L_0 = (int32_t)__this->get_m_Count_4();
		NullCheck((ObiNativeList_1_t82AAD6F0A45EA387F6031B345E681AE0DADF910C *)__this);
		NativeArray_1_tAFBEB18DC14E5B6EF8F550019E04E75AC3289B6A  L_1 = ((  NativeArray_1_tAFBEB18DC14E5B6EF8F550019E04E75AC3289B6A  (*) (ObiNativeList_1_t82AAD6F0A45EA387F6031B345E681AE0DADF910C *, int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->methodPointer)((ObiNativeList_1_t82AAD6F0A45EA387F6031B345E681AE0DADF910C *)__this, (int32_t)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		return (NativeArray_1_tAFBEB18DC14E5B6EF8F550019E04E75AC3289B6A )L_1;
	}
}
// Unity.Collections.NativeArray`1<U> Obi.ObiNativeList`1<Obi.EdgeMeshHeader>::AsNativeArray<Obi.EdgeMeshHeader>(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR NativeArray_1_tAFBEB18DC14E5B6EF8F550019E04E75AC3289B6A  ObiNativeList_1_AsNativeArray_TisEdgeMeshHeader_t235B26AE87E4F8BC38EC627430A9E0E344A03878_m42A94DC3C9F743F552D48A444824BB446172AD09_gshared (ObiNativeList_1_t82AAD6F0A45EA387F6031B345E681AE0DADF910C * __this, int32_t ___arrayLength0, const RuntimeMethod* method)
{
	{
		// NativeArray<U> array = NativeArrayUnsafeUtility.ConvertExistingDataToNativeArray<U>(m_AlignedPtr, arrayLength, Allocator.None);
		void* L_0 = (void*)__this->get_m_AlignedPtr_1();
		int32_t L_1 = ___arrayLength0;
		NativeArray_1_tAFBEB18DC14E5B6EF8F550019E04E75AC3289B6A  L_2 = ((  NativeArray_1_tAFBEB18DC14E5B6EF8F550019E04E75AC3289B6A  (*) (void*, int32_t, int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->methodPointer)((void*)(void*)L_0, (int32_t)L_1, (int32_t)1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		// return array;
		return (NativeArray_1_tAFBEB18DC14E5B6EF8F550019E04E75AC3289B6A )L_2;
	}
}
// Unity.Collections.NativeArray`1<U> Obi.ObiNativeList`1<Obi.HeightFieldHeader>::AsNativeArray<Obi.HeightFieldHeader>()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR NativeArray_1_tB28C2BA1925952F16FE6E1D099581CE6CE963139  ObiNativeList_1_AsNativeArray_TisHeightFieldHeader_t10BB39F6CE13CD0FBDC200A25AFD4000062D7E4E_mDB7B7EB440CC0762C9253F6F212926A8DDDB6ABD_gshared (ObiNativeList_1_t2A4871B78EE973A2C9E2473CBF7A597169D426CB * __this, const RuntimeMethod* method)
{
	{
		// return AsNativeArray<U>(m_Count);
		int32_t L_0 = (int32_t)__this->get_m_Count_4();
		NullCheck((ObiNativeList_1_t2A4871B78EE973A2C9E2473CBF7A597169D426CB *)__this);
		NativeArray_1_tB28C2BA1925952F16FE6E1D099581CE6CE963139  L_1 = ((  NativeArray_1_tB28C2BA1925952F16FE6E1D099581CE6CE963139  (*) (ObiNativeList_1_t2A4871B78EE973A2C9E2473CBF7A597169D426CB *, int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->methodPointer)((ObiNativeList_1_t2A4871B78EE973A2C9E2473CBF7A597169D426CB *)__this, (int32_t)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		return (NativeArray_1_tB28C2BA1925952F16FE6E1D099581CE6CE963139 )L_1;
	}
}
// Unity.Collections.NativeArray`1<U> Obi.ObiNativeList`1<Obi.HeightFieldHeader>::AsNativeArray<Obi.HeightFieldHeader>(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR NativeArray_1_tB28C2BA1925952F16FE6E1D099581CE6CE963139  ObiNativeList_1_AsNativeArray_TisHeightFieldHeader_t10BB39F6CE13CD0FBDC200A25AFD4000062D7E4E_m6663FC1617F639B3EDE475E4F7282D8DC67A45EC_gshared (ObiNativeList_1_t2A4871B78EE973A2C9E2473CBF7A597169D426CB * __this, int32_t ___arrayLength0, const RuntimeMethod* method)
{
	{
		// NativeArray<U> array = NativeArrayUnsafeUtility.ConvertExistingDataToNativeArray<U>(m_AlignedPtr, arrayLength, Allocator.None);
		void* L_0 = (void*)__this->get_m_AlignedPtr_1();
		int32_t L_1 = ___arrayLength0;
		NativeArray_1_tB28C2BA1925952F16FE6E1D099581CE6CE963139  L_2 = ((  NativeArray_1_tB28C2BA1925952F16FE6E1D099581CE6CE963139  (*) (void*, int32_t, int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->methodPointer)((void*)(void*)L_0, (int32_t)L_1, (int32_t)1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		// return array;
		return (NativeArray_1_tB28C2BA1925952F16FE6E1D099581CE6CE963139 )L_2;
	}
}
// Unity.Collections.NativeArray`1<U> Obi.ObiNativeList`1<System.Int32>::AsNativeArray<System.Int32>()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR NativeArray_1_tD60079F06B473C85CF6C2BB4F2D203F38191AE99  ObiNativeList_1_AsNativeArray_TisInt32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046_m35BE4209BA4AB6E740C9FE18F6DB5D0E72EB931A_gshared (ObiNativeList_1_t838DB140073974101A3C0BD56D899DA67D9B2EC4 * __this, const RuntimeMethod* method)
{
	{
		// return AsNativeArray<U>(m_Count);
		int32_t L_0 = (int32_t)__this->get_m_Count_4();
		NullCheck((ObiNativeList_1_t838DB140073974101A3C0BD56D899DA67D9B2EC4 *)__this);
		NativeArray_1_tD60079F06B473C85CF6C2BB4F2D203F38191AE99  L_1 = ((  NativeArray_1_tD60079F06B473C85CF6C2BB4F2D203F38191AE99  (*) (ObiNativeList_1_t838DB140073974101A3C0BD56D899DA67D9B2EC4 *, int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->methodPointer)((ObiNativeList_1_t838DB140073974101A3C0BD56D899DA67D9B2EC4 *)__this, (int32_t)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		return (NativeArray_1_tD60079F06B473C85CF6C2BB4F2D203F38191AE99 )L_1;
	}
}
// Unity.Collections.NativeArray`1<U> Obi.ObiNativeList`1<System.Int32>::AsNativeArray<System.Int32>(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR NativeArray_1_tD60079F06B473C85CF6C2BB4F2D203F38191AE99  ObiNativeList_1_AsNativeArray_TisInt32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046_m7925D94F0A9C5D95CB3E3E4CA7ACDF37A4C91AB1_gshared (ObiNativeList_1_t838DB140073974101A3C0BD56D899DA67D9B2EC4 * __this, int32_t ___arrayLength0, const RuntimeMethod* method)
{
	{
		// NativeArray<U> array = NativeArrayUnsafeUtility.ConvertExistingDataToNativeArray<U>(m_AlignedPtr, arrayLength, Allocator.None);
		void* L_0 = (void*)__this->get_m_AlignedPtr_1();
		int32_t L_1 = ___arrayLength0;
		NativeArray_1_tD60079F06B473C85CF6C2BB4F2D203F38191AE99  L_2 = ((  NativeArray_1_tD60079F06B473C85CF6C2BB4F2D203F38191AE99  (*) (void*, int32_t, int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->methodPointer)((void*)(void*)L_0, (int32_t)L_1, (int32_t)1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		// return array;
		return (NativeArray_1_tD60079F06B473C85CF6C2BB4F2D203F38191AE99 )L_2;
	}
}
// Unity.Collections.NativeArray`1<U> Obi.ObiNativeList`1<System.IntPtr>::AsNativeArray<System.IntPtr>()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR NativeArray_1_t5DC9D761D5D8A1E88FF45B12664595E0483C146F  ObiNativeList_1_AsNativeArray_TisIntPtr_t_m9702CDA48DAE4BD76977F2CEE6589F7416655945_gshared (ObiNativeList_1_t2691FD222DD25B2FB43888C0159E83392B57A150 * __this, const RuntimeMethod* method)
{
	{
		// return AsNativeArray<U>(m_Count);
		int32_t L_0 = (int32_t)__this->get_m_Count_4();
		NullCheck((ObiNativeList_1_t2691FD222DD25B2FB43888C0159E83392B57A150 *)__this);
		NativeArray_1_t5DC9D761D5D8A1E88FF45B12664595E0483C146F  L_1 = ((  NativeArray_1_t5DC9D761D5D8A1E88FF45B12664595E0483C146F  (*) (ObiNativeList_1_t2691FD222DD25B2FB43888C0159E83392B57A150 *, int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->methodPointer)((ObiNativeList_1_t2691FD222DD25B2FB43888C0159E83392B57A150 *)__this, (int32_t)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		return (NativeArray_1_t5DC9D761D5D8A1E88FF45B12664595E0483C146F )L_1;
	}
}
// Unity.Collections.NativeArray`1<U> Obi.ObiNativeList`1<System.IntPtr>::AsNativeArray<System.IntPtr>(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR NativeArray_1_t5DC9D761D5D8A1E88FF45B12664595E0483C146F  ObiNativeList_1_AsNativeArray_TisIntPtr_t_m21F72CE995CAAD66C5B9F764347C2CD216CF1633_gshared (ObiNativeList_1_t2691FD222DD25B2FB43888C0159E83392B57A150 * __this, int32_t ___arrayLength0, const RuntimeMethod* method)
{
	{
		// NativeArray<U> array = NativeArrayUnsafeUtility.ConvertExistingDataToNativeArray<U>(m_AlignedPtr, arrayLength, Allocator.None);
		void* L_0 = (void*)__this->get_m_AlignedPtr_1();
		int32_t L_1 = ___arrayLength0;
		NativeArray_1_t5DC9D761D5D8A1E88FF45B12664595E0483C146F  L_2 = ((  NativeArray_1_t5DC9D761D5D8A1E88FF45B12664595E0483C146F  (*) (void*, int32_t, int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->methodPointer)((void*)(void*)L_0, (int32_t)L_1, (int32_t)1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		// return array;
		return (NativeArray_1_t5DC9D761D5D8A1E88FF45B12664595E0483C146F )L_2;
	}
}
// Unity.Collections.NativeArray`1<U> Obi.ObiNativeList`1<UnityEngine.Matrix4x4>::AsNativeArray<UnityEngine.Matrix4x4>()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR NativeArray_1_t61B1FEBC3536B66CB721DC2B6CEDBB648D8F7AC8  ObiNativeList_1_AsNativeArray_TisMatrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461_mE51F80748D9F7D8CF5B954D21A5757BA0E476CA1_gshared (ObiNativeList_1_t93A1DE0BF264327DCAC68372C96B4969F13BC062 * __this, const RuntimeMethod* method)
{
	{
		// return AsNativeArray<U>(m_Count);
		int32_t L_0 = (int32_t)__this->get_m_Count_4();
		NullCheck((ObiNativeList_1_t93A1DE0BF264327DCAC68372C96B4969F13BC062 *)__this);
		NativeArray_1_t61B1FEBC3536B66CB721DC2B6CEDBB648D8F7AC8  L_1 = ((  NativeArray_1_t61B1FEBC3536B66CB721DC2B6CEDBB648D8F7AC8  (*) (ObiNativeList_1_t93A1DE0BF264327DCAC68372C96B4969F13BC062 *, int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->methodPointer)((ObiNativeList_1_t93A1DE0BF264327DCAC68372C96B4969F13BC062 *)__this, (int32_t)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		return (NativeArray_1_t61B1FEBC3536B66CB721DC2B6CEDBB648D8F7AC8 )L_1;
	}
}
// Unity.Collections.NativeArray`1<U> Obi.ObiNativeList`1<UnityEngine.Matrix4x4>::AsNativeArray<UnityEngine.Matrix4x4>(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR NativeArray_1_t61B1FEBC3536B66CB721DC2B6CEDBB648D8F7AC8  ObiNativeList_1_AsNativeArray_TisMatrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461_mC3C5535AE4BC7095D15F707F259E00F7D9FFA8D9_gshared (ObiNativeList_1_t93A1DE0BF264327DCAC68372C96B4969F13BC062 * __this, int32_t ___arrayLength0, const RuntimeMethod* method)
{
	{
		// NativeArray<U> array = NativeArrayUnsafeUtility.ConvertExistingDataToNativeArray<U>(m_AlignedPtr, arrayLength, Allocator.None);
		void* L_0 = (void*)__this->get_m_AlignedPtr_1();
		int32_t L_1 = ___arrayLength0;
		NativeArray_1_t61B1FEBC3536B66CB721DC2B6CEDBB648D8F7AC8  L_2 = ((  NativeArray_1_t61B1FEBC3536B66CB721DC2B6CEDBB648D8F7AC8  (*) (void*, int32_t, int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->methodPointer)((void*)(void*)L_0, (int32_t)L_1, (int32_t)1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		// return array;
		return (NativeArray_1_t61B1FEBC3536B66CB721DC2B6CEDBB648D8F7AC8 )L_2;
	}
}
// Unity.Collections.NativeArray`1<U> Obi.ObiNativeList`1<UnityEngine.Quaternion>::AsNativeArray<UnityEngine.Quaternion>()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR NativeArray_1_t2672EA3A09E3FE76419439DC581905D460584A60  ObiNativeList_1_AsNativeArray_TisQuaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4_mF05C94E2152AF08F027143A55CDB53459CC28491_gshared (ObiNativeList_1_tED35083346C98040EABF6B4FAC5245310D8B9939 * __this, const RuntimeMethod* method)
{
	{
		// return AsNativeArray<U>(m_Count);
		int32_t L_0 = (int32_t)__this->get_m_Count_4();
		NullCheck((ObiNativeList_1_tED35083346C98040EABF6B4FAC5245310D8B9939 *)__this);
		NativeArray_1_t2672EA3A09E3FE76419439DC581905D460584A60  L_1 = ((  NativeArray_1_t2672EA3A09E3FE76419439DC581905D460584A60  (*) (ObiNativeList_1_tED35083346C98040EABF6B4FAC5245310D8B9939 *, int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->methodPointer)((ObiNativeList_1_tED35083346C98040EABF6B4FAC5245310D8B9939 *)__this, (int32_t)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		return (NativeArray_1_t2672EA3A09E3FE76419439DC581905D460584A60 )L_1;
	}
}
// Unity.Collections.NativeArray`1<U> Obi.ObiNativeList`1<UnityEngine.Quaternion>::AsNativeArray<UnityEngine.Quaternion>(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR NativeArray_1_t2672EA3A09E3FE76419439DC581905D460584A60  ObiNativeList_1_AsNativeArray_TisQuaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4_m8434DCF53939815B5BBBADA52E8C4BC654C1DE2B_gshared (ObiNativeList_1_tED35083346C98040EABF6B4FAC5245310D8B9939 * __this, int32_t ___arrayLength0, const RuntimeMethod* method)
{
	{
		// NativeArray<U> array = NativeArrayUnsafeUtility.ConvertExistingDataToNativeArray<U>(m_AlignedPtr, arrayLength, Allocator.None);
		void* L_0 = (void*)__this->get_m_AlignedPtr_1();
		int32_t L_1 = ___arrayLength0;
		NativeArray_1_t2672EA3A09E3FE76419439DC581905D460584A60  L_2 = ((  NativeArray_1_t2672EA3A09E3FE76419439DC581905D460584A60  (*) (void*, int32_t, int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->methodPointer)((void*)(void*)L_0, (int32_t)L_1, (int32_t)1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		// return array;
		return (NativeArray_1_t2672EA3A09E3FE76419439DC581905D460584A60 )L_2;
	}
}
// Unity.Collections.NativeArray`1<U> Obi.ObiNativeList`1<Obi.QueryResult>::AsNativeArray<Obi.QueryResult>()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR NativeArray_1_t95FE546DF579DC0687965D420C82508F2B433411  ObiNativeList_1_AsNativeArray_TisQueryResult_t4DCEE89279D61183A30969C97F725AA603AD118C_m24ED6B9271A1B4431240285D46C385E11524701F_gshared (ObiNativeList_1_tE4E9A225B4175E5D04A1C69103C9CB0962541089 * __this, const RuntimeMethod* method)
{
	{
		// return AsNativeArray<U>(m_Count);
		int32_t L_0 = (int32_t)__this->get_m_Count_4();
		NullCheck((ObiNativeList_1_tE4E9A225B4175E5D04A1C69103C9CB0962541089 *)__this);
		NativeArray_1_t95FE546DF579DC0687965D420C82508F2B433411  L_1 = ((  NativeArray_1_t95FE546DF579DC0687965D420C82508F2B433411  (*) (ObiNativeList_1_tE4E9A225B4175E5D04A1C69103C9CB0962541089 *, int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->methodPointer)((ObiNativeList_1_tE4E9A225B4175E5D04A1C69103C9CB0962541089 *)__this, (int32_t)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		return (NativeArray_1_t95FE546DF579DC0687965D420C82508F2B433411 )L_1;
	}
}
// Unity.Collections.NativeArray`1<U> Obi.ObiNativeList`1<Obi.QueryResult>::AsNativeArray<Obi.QueryResult>(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR NativeArray_1_t95FE546DF579DC0687965D420C82508F2B433411  ObiNativeList_1_AsNativeArray_TisQueryResult_t4DCEE89279D61183A30969C97F725AA603AD118C_m8574FF056B310A7142920EC44CAC7D06B2ACA558_gshared (ObiNativeList_1_tE4E9A225B4175E5D04A1C69103C9CB0962541089 * __this, int32_t ___arrayLength0, const RuntimeMethod* method)
{
	{
		// NativeArray<U> array = NativeArrayUnsafeUtility.ConvertExistingDataToNativeArray<U>(m_AlignedPtr, arrayLength, Allocator.None);
		void* L_0 = (void*)__this->get_m_AlignedPtr_1();
		int32_t L_1 = ___arrayLength0;
		NativeArray_1_t95FE546DF579DC0687965D420C82508F2B433411  L_2 = ((  NativeArray_1_t95FE546DF579DC0687965D420C82508F2B433411  (*) (void*, int32_t, int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->methodPointer)((void*)(void*)L_0, (int32_t)L_1, (int32_t)1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		// return array;
		return (NativeArray_1_t95FE546DF579DC0687965D420C82508F2B433411 )L_2;
	}
}
// Unity.Collections.NativeArray`1<U> Obi.ObiNativeList`1<Obi.QueryShape>::AsNativeArray<Obi.QueryShape>()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR NativeArray_1_t17470C60D1D175C02D1B8DE9E928D177760EB8F3  ObiNativeList_1_AsNativeArray_TisQueryShape_t0A83A6359C90B5213CDEC851B226F8DAAD6D07AF_mB3F3FD506BA08C0A446389DEEFFF81FC2037DF10_gshared (ObiNativeList_1_tD298AB56D582AAA797727DF401F4FAC39E7DCAEF * __this, const RuntimeMethod* method)
{
	{
		// return AsNativeArray<U>(m_Count);
		int32_t L_0 = (int32_t)__this->get_m_Count_4();
		NullCheck((ObiNativeList_1_tD298AB56D582AAA797727DF401F4FAC39E7DCAEF *)__this);
		NativeArray_1_t17470C60D1D175C02D1B8DE9E928D177760EB8F3  L_1 = ((  NativeArray_1_t17470C60D1D175C02D1B8DE9E928D177760EB8F3  (*) (ObiNativeList_1_tD298AB56D582AAA797727DF401F4FAC39E7DCAEF *, int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->methodPointer)((ObiNativeList_1_tD298AB56D582AAA797727DF401F4FAC39E7DCAEF *)__this, (int32_t)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		return (NativeArray_1_t17470C60D1D175C02D1B8DE9E928D177760EB8F3 )L_1;
	}
}
// Unity.Collections.NativeArray`1<U> Obi.ObiNativeList`1<Obi.QueryShape>::AsNativeArray<Obi.QueryShape>(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR NativeArray_1_t17470C60D1D175C02D1B8DE9E928D177760EB8F3  ObiNativeList_1_AsNativeArray_TisQueryShape_t0A83A6359C90B5213CDEC851B226F8DAAD6D07AF_m0B159BC01C7BD92A8FECE6546FDEC8750B1FAB48_gshared (ObiNativeList_1_tD298AB56D582AAA797727DF401F4FAC39E7DCAEF * __this, int32_t ___arrayLength0, const RuntimeMethod* method)
{
	{
		// NativeArray<U> array = NativeArrayUnsafeUtility.ConvertExistingDataToNativeArray<U>(m_AlignedPtr, arrayLength, Allocator.None);
		void* L_0 = (void*)__this->get_m_AlignedPtr_1();
		int32_t L_1 = ___arrayLength0;
		NativeArray_1_t17470C60D1D175C02D1B8DE9E928D177760EB8F3  L_2 = ((  NativeArray_1_t17470C60D1D175C02D1B8DE9E928D177760EB8F3  (*) (void*, int32_t, int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->methodPointer)((void*)(void*)L_0, (int32_t)L_1, (int32_t)1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		// return array;
		return (NativeArray_1_t17470C60D1D175C02D1B8DE9E928D177760EB8F3 )L_2;
	}
}
// Unity.Collections.NativeArray`1<U> Obi.ObiNativeList`1<System.Single>::AsNativeArray<System.Single>()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR NativeArray_1_t5F920DC5A531D604D161A0FAD3479B5BFE0D9232  ObiNativeList_1_AsNativeArray_TisSingle_tE07797BA3C98D4CA9B5A19413C19A76688AB899E_mDC7B4A6E852661FED4D00EFB2BD5A23DE65DC212_gshared (ObiNativeList_1_tE371D1BA733B218B4F6296CB724D923CB25CC53B * __this, const RuntimeMethod* method)
{
	{
		// return AsNativeArray<U>(m_Count);
		int32_t L_0 = (int32_t)__this->get_m_Count_4();
		NullCheck((ObiNativeList_1_tE371D1BA733B218B4F6296CB724D923CB25CC53B *)__this);
		NativeArray_1_t5F920DC5A531D604D161A0FAD3479B5BFE0D9232  L_1 = ((  NativeArray_1_t5F920DC5A531D604D161A0FAD3479B5BFE0D9232  (*) (ObiNativeList_1_tE371D1BA733B218B4F6296CB724D923CB25CC53B *, int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->methodPointer)((ObiNativeList_1_tE371D1BA733B218B4F6296CB724D923CB25CC53B *)__this, (int32_t)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		return (NativeArray_1_t5F920DC5A531D604D161A0FAD3479B5BFE0D9232 )L_1;
	}
}
// Unity.Collections.NativeArray`1<U> Obi.ObiNativeList`1<System.Single>::AsNativeArray<System.Single>(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR NativeArray_1_t5F920DC5A531D604D161A0FAD3479B5BFE0D9232  ObiNativeList_1_AsNativeArray_TisSingle_tE07797BA3C98D4CA9B5A19413C19A76688AB899E_m6D587CCA98BDD0AE1B484EB71522B48CC2DC15F4_gshared (ObiNativeList_1_tE371D1BA733B218B4F6296CB724D923CB25CC53B * __this, int32_t ___arrayLength0, const RuntimeMethod* method)
{
	{
		// NativeArray<U> array = NativeArrayUnsafeUtility.ConvertExistingDataToNativeArray<U>(m_AlignedPtr, arrayLength, Allocator.None);
		void* L_0 = (void*)__this->get_m_AlignedPtr_1();
		int32_t L_1 = ___arrayLength0;
		NativeArray_1_t5F920DC5A531D604D161A0FAD3479B5BFE0D9232  L_2 = ((  NativeArray_1_t5F920DC5A531D604D161A0FAD3479B5BFE0D9232  (*) (void*, int32_t, int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->methodPointer)((void*)(void*)L_0, (int32_t)L_1, (int32_t)1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		// return array;
		return (NativeArray_1_t5F920DC5A531D604D161A0FAD3479B5BFE0D9232 )L_2;
	}
}
// Unity.Collections.NativeArray`1<U> Obi.ObiNativeList`1<Obi.Triangle>::AsNativeArray<Obi.Triangle>()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR NativeArray_1_t6611EE86470DC9133E759336BB08C0018D2AACA1  ObiNativeList_1_AsNativeArray_TisTriangle_t6EBD8067A5E92E374FBCEE8434826BC760D9FDC8_m2A64F910D9792EF8F4FF5623D5CAE7E8CF9B24F3_gshared (ObiNativeList_1_tDE675C68FBF18547F862FC9F08A57ABD59D44A88 * __this, const RuntimeMethod* method)
{
	{
		// return AsNativeArray<U>(m_Count);
		int32_t L_0 = (int32_t)__this->get_m_Count_4();
		NullCheck((ObiNativeList_1_tDE675C68FBF18547F862FC9F08A57ABD59D44A88 *)__this);
		NativeArray_1_t6611EE86470DC9133E759336BB08C0018D2AACA1  L_1 = ((  NativeArray_1_t6611EE86470DC9133E759336BB08C0018D2AACA1  (*) (ObiNativeList_1_tDE675C68FBF18547F862FC9F08A57ABD59D44A88 *, int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->methodPointer)((ObiNativeList_1_tDE675C68FBF18547F862FC9F08A57ABD59D44A88 *)__this, (int32_t)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		return (NativeArray_1_t6611EE86470DC9133E759336BB08C0018D2AACA1 )L_1;
	}
}
// Unity.Collections.NativeArray`1<U> Obi.ObiNativeList`1<Obi.Triangle>::AsNativeArray<Obi.Triangle>(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR NativeArray_1_t6611EE86470DC9133E759336BB08C0018D2AACA1  ObiNativeList_1_AsNativeArray_TisTriangle_t6EBD8067A5E92E374FBCEE8434826BC760D9FDC8_m511735B46413F45A6947D0A4458738B2777165D4_gshared (ObiNativeList_1_tDE675C68FBF18547F862FC9F08A57ABD59D44A88 * __this, int32_t ___arrayLength0, const RuntimeMethod* method)
{
	{
		// NativeArray<U> array = NativeArrayUnsafeUtility.ConvertExistingDataToNativeArray<U>(m_AlignedPtr, arrayLength, Allocator.None);
		void* L_0 = (void*)__this->get_m_AlignedPtr_1();
		int32_t L_1 = ___arrayLength0;
		NativeArray_1_t6611EE86470DC9133E759336BB08C0018D2AACA1  L_2 = ((  NativeArray_1_t6611EE86470DC9133E759336BB08C0018D2AACA1  (*) (void*, int32_t, int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->methodPointer)((void*)(void*)L_0, (int32_t)L_1, (int32_t)1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		// return array;
		return (NativeArray_1_t6611EE86470DC9133E759336BB08C0018D2AACA1 )L_2;
	}
}
// Unity.Collections.NativeArray`1<U> Obi.ObiNativeList`1<Obi.TriangleMeshHeader>::AsNativeArray<Obi.TriangleMeshHeader>()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR NativeArray_1_tFC2C5BFEF570BAC41857EA3575BEB7E1F9C1F564  ObiNativeList_1_AsNativeArray_TisTriangleMeshHeader_tD4F431BF8B26AB40AEE8CC5AF9C045A565607267_m6065E08A64FA9E5E1456CE5C248AE65D81B2ADD5_gshared (ObiNativeList_1_t8C4BAE9CBB1F8A70288B65B1791A14FB832E1B1F * __this, const RuntimeMethod* method)
{
	{
		// return AsNativeArray<U>(m_Count);
		int32_t L_0 = (int32_t)__this->get_m_Count_4();
		NullCheck((ObiNativeList_1_t8C4BAE9CBB1F8A70288B65B1791A14FB832E1B1F *)__this);
		NativeArray_1_tFC2C5BFEF570BAC41857EA3575BEB7E1F9C1F564  L_1 = ((  NativeArray_1_tFC2C5BFEF570BAC41857EA3575BEB7E1F9C1F564  (*) (ObiNativeList_1_t8C4BAE9CBB1F8A70288B65B1791A14FB832E1B1F *, int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->methodPointer)((ObiNativeList_1_t8C4BAE9CBB1F8A70288B65B1791A14FB832E1B1F *)__this, (int32_t)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		return (NativeArray_1_tFC2C5BFEF570BAC41857EA3575BEB7E1F9C1F564 )L_1;
	}
}
// Unity.Collections.NativeArray`1<U> Obi.ObiNativeList`1<Obi.TriangleMeshHeader>::AsNativeArray<Obi.TriangleMeshHeader>(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR NativeArray_1_tFC2C5BFEF570BAC41857EA3575BEB7E1F9C1F564  ObiNativeList_1_AsNativeArray_TisTriangleMeshHeader_tD4F431BF8B26AB40AEE8CC5AF9C045A565607267_mA92B4EE69BE460B923C15145A66BF945774C05D4_gshared (ObiNativeList_1_t8C4BAE9CBB1F8A70288B65B1791A14FB832E1B1F * __this, int32_t ___arrayLength0, const RuntimeMethod* method)
{
	{
		// NativeArray<U> array = NativeArrayUnsafeUtility.ConvertExistingDataToNativeArray<U>(m_AlignedPtr, arrayLength, Allocator.None);
		void* L_0 = (void*)__this->get_m_AlignedPtr_1();
		int32_t L_1 = ___arrayLength0;
		NativeArray_1_tFC2C5BFEF570BAC41857EA3575BEB7E1F9C1F564  L_2 = ((  NativeArray_1_tFC2C5BFEF570BAC41857EA3575BEB7E1F9C1F564  (*) (void*, int32_t, int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->methodPointer)((void*)(void*)L_0, (int32_t)L_1, (int32_t)1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		// return array;
		return (NativeArray_1_tFC2C5BFEF570BAC41857EA3575BEB7E1F9C1F564 )L_2;
	}
}
// Unity.Collections.NativeArray`1<U> Obi.ObiNativeList`1<Obi.VInt4>::AsNativeArray<Obi.VInt4>()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR NativeArray_1_t62E64122D8894FB32635400C7790A369FB6512F4  ObiNativeList_1_AsNativeArray_TisVInt4_t7102225B83232A71C9D784846D42B69BF440AD5D_mD1C5C663B98B02D5880E789B42C146AF298DD17A_gshared (ObiNativeList_1_tBFECFF8B42E1120D079278D2C8F079D3E3BEA07A * __this, const RuntimeMethod* method)
{
	{
		// return AsNativeArray<U>(m_Count);
		int32_t L_0 = (int32_t)__this->get_m_Count_4();
		NullCheck((ObiNativeList_1_tBFECFF8B42E1120D079278D2C8F079D3E3BEA07A *)__this);
		NativeArray_1_t62E64122D8894FB32635400C7790A369FB6512F4  L_1 = ((  NativeArray_1_t62E64122D8894FB32635400C7790A369FB6512F4  (*) (ObiNativeList_1_tBFECFF8B42E1120D079278D2C8F079D3E3BEA07A *, int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->methodPointer)((ObiNativeList_1_tBFECFF8B42E1120D079278D2C8F079D3E3BEA07A *)__this, (int32_t)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		return (NativeArray_1_t62E64122D8894FB32635400C7790A369FB6512F4 )L_1;
	}
}
// Unity.Collections.NativeArray`1<U> Obi.ObiNativeList`1<Obi.VInt4>::AsNativeArray<Obi.VInt4>(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR NativeArray_1_t62E64122D8894FB32635400C7790A369FB6512F4  ObiNativeList_1_AsNativeArray_TisVInt4_t7102225B83232A71C9D784846D42B69BF440AD5D_m2BE57597312FF9765FF4ED049F6B69A5940A9B92_gshared (ObiNativeList_1_tBFECFF8B42E1120D079278D2C8F079D3E3BEA07A * __this, int32_t ___arrayLength0, const RuntimeMethod* method)
{
	{
		// NativeArray<U> array = NativeArrayUnsafeUtility.ConvertExistingDataToNativeArray<U>(m_AlignedPtr, arrayLength, Allocator.None);
		void* L_0 = (void*)__this->get_m_AlignedPtr_1();
		int32_t L_1 = ___arrayLength0;
		NativeArray_1_t62E64122D8894FB32635400C7790A369FB6512F4  L_2 = ((  NativeArray_1_t62E64122D8894FB32635400C7790A369FB6512F4  (*) (void*, int32_t, int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->methodPointer)((void*)(void*)L_0, (int32_t)L_1, (int32_t)1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		// return array;
		return (NativeArray_1_t62E64122D8894FB32635400C7790A369FB6512F4 )L_2;
	}
}
// Unity.Collections.NativeArray`1<U> Obi.ObiNativeList`1<UnityEngine.Vector2>::AsNativeArray<UnityEngine.Vector2>()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR NativeArray_1_t431C85F30275831D1F5D458AB73DFCE050693BC0  ObiNativeList_1_AsNativeArray_TisVector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_m8BA7F29C3CE8D16131B711ED8DC84DAF2371D55B_gshared (ObiNativeList_1_t5C6ED20BB4BD829B45C019D5E3348CC6BAF2F37D * __this, const RuntimeMethod* method)
{
	{
		// return AsNativeArray<U>(m_Count);
		int32_t L_0 = (int32_t)__this->get_m_Count_4();
		NullCheck((ObiNativeList_1_t5C6ED20BB4BD829B45C019D5E3348CC6BAF2F37D *)__this);
		NativeArray_1_t431C85F30275831D1F5D458AB73DFCE050693BC0  L_1 = ((  NativeArray_1_t431C85F30275831D1F5D458AB73DFCE050693BC0  (*) (ObiNativeList_1_t5C6ED20BB4BD829B45C019D5E3348CC6BAF2F37D *, int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->methodPointer)((ObiNativeList_1_t5C6ED20BB4BD829B45C019D5E3348CC6BAF2F37D *)__this, (int32_t)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		return (NativeArray_1_t431C85F30275831D1F5D458AB73DFCE050693BC0 )L_1;
	}
}
// Unity.Collections.NativeArray`1<U> Obi.ObiNativeList`1<UnityEngine.Vector2>::AsNativeArray<UnityEngine.Vector2>(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR NativeArray_1_t431C85F30275831D1F5D458AB73DFCE050693BC0  ObiNativeList_1_AsNativeArray_TisVector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_m7A8D45DC696B9164A958BE1C0C32469AE9964D1D_gshared (ObiNativeList_1_t5C6ED20BB4BD829B45C019D5E3348CC6BAF2F37D * __this, int32_t ___arrayLength0, const RuntimeMethod* method)
{
	{
		// NativeArray<U> array = NativeArrayUnsafeUtility.ConvertExistingDataToNativeArray<U>(m_AlignedPtr, arrayLength, Allocator.None);
		void* L_0 = (void*)__this->get_m_AlignedPtr_1();
		int32_t L_1 = ___arrayLength0;
		NativeArray_1_t431C85F30275831D1F5D458AB73DFCE050693BC0  L_2 = ((  NativeArray_1_t431C85F30275831D1F5D458AB73DFCE050693BC0  (*) (void*, int32_t, int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->methodPointer)((void*)(void*)L_0, (int32_t)L_1, (int32_t)1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		// return array;
		return (NativeArray_1_t431C85F30275831D1F5D458AB73DFCE050693BC0 )L_2;
	}
}
// Unity.Collections.NativeArray`1<U> Obi.ObiNativeList`1<UnityEngine.Vector3>::AsNativeArray<UnityEngine.Vector3>()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR NativeArray_1_t2577BCA09CA248EFB78BD7FDA7F09D5C395DFF52  ObiNativeList_1_AsNativeArray_TisVector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_m55BA18F1721190910FFC99285F8601305F7DC190_gshared (ObiNativeList_1_t0F29E875C6BF5DD7F89BA979309B5604FDC8DB26 * __this, const RuntimeMethod* method)
{
	{
		// return AsNativeArray<U>(m_Count);
		int32_t L_0 = (int32_t)__this->get_m_Count_4();
		NullCheck((ObiNativeList_1_t0F29E875C6BF5DD7F89BA979309B5604FDC8DB26 *)__this);
		NativeArray_1_t2577BCA09CA248EFB78BD7FDA7F09D5C395DFF52  L_1 = ((  NativeArray_1_t2577BCA09CA248EFB78BD7FDA7F09D5C395DFF52  (*) (ObiNativeList_1_t0F29E875C6BF5DD7F89BA979309B5604FDC8DB26 *, int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->methodPointer)((ObiNativeList_1_t0F29E875C6BF5DD7F89BA979309B5604FDC8DB26 *)__this, (int32_t)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		return (NativeArray_1_t2577BCA09CA248EFB78BD7FDA7F09D5C395DFF52 )L_1;
	}
}
// Unity.Collections.NativeArray`1<U> Obi.ObiNativeList`1<UnityEngine.Vector3>::AsNativeArray<UnityEngine.Vector3>(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR NativeArray_1_t2577BCA09CA248EFB78BD7FDA7F09D5C395DFF52  ObiNativeList_1_AsNativeArray_TisVector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_m0658A2253A8A1E1F6930C635C203EB18965CAFA1_gshared (ObiNativeList_1_t0F29E875C6BF5DD7F89BA979309B5604FDC8DB26 * __this, int32_t ___arrayLength0, const RuntimeMethod* method)
{
	{
		// NativeArray<U> array = NativeArrayUnsafeUtility.ConvertExistingDataToNativeArray<U>(m_AlignedPtr, arrayLength, Allocator.None);
		void* L_0 = (void*)__this->get_m_AlignedPtr_1();
		int32_t L_1 = ___arrayLength0;
		NativeArray_1_t2577BCA09CA248EFB78BD7FDA7F09D5C395DFF52  L_2 = ((  NativeArray_1_t2577BCA09CA248EFB78BD7FDA7F09D5C395DFF52  (*) (void*, int32_t, int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->methodPointer)((void*)(void*)L_0, (int32_t)L_1, (int32_t)1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		// return array;
		return (NativeArray_1_t2577BCA09CA248EFB78BD7FDA7F09D5C395DFF52 )L_2;
	}
}
// Unity.Collections.NativeArray`1<U> Obi.ObiNativeList`1<UnityEngine.Vector4>::AsNativeArray<UnityEngine.Vector4>()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR NativeArray_1_t381CA7D92591F9C96DB84965C31AF8713A546156  ObiNativeList_1_AsNativeArray_TisVector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7_mE743569B3F4965D7537241C0E6554E27C5F761CE_gshared (ObiNativeList_1_t667688879E4BD2DFE8EDD92169E59F19619DF4F4 * __this, const RuntimeMethod* method)
{
	{
		// return AsNativeArray<U>(m_Count);
		int32_t L_0 = (int32_t)__this->get_m_Count_4();
		NullCheck((ObiNativeList_1_t667688879E4BD2DFE8EDD92169E59F19619DF4F4 *)__this);
		NativeArray_1_t381CA7D92591F9C96DB84965C31AF8713A546156  L_1 = ((  NativeArray_1_t381CA7D92591F9C96DB84965C31AF8713A546156  (*) (ObiNativeList_1_t667688879E4BD2DFE8EDD92169E59F19619DF4F4 *, int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->methodPointer)((ObiNativeList_1_t667688879E4BD2DFE8EDD92169E59F19619DF4F4 *)__this, (int32_t)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		return (NativeArray_1_t381CA7D92591F9C96DB84965C31AF8713A546156 )L_1;
	}
}
// Unity.Collections.NativeArray`1<U> Obi.ObiNativeList`1<UnityEngine.Vector4>::AsNativeArray<UnityEngine.Vector4>(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR NativeArray_1_t381CA7D92591F9C96DB84965C31AF8713A546156  ObiNativeList_1_AsNativeArray_TisVector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7_m23BC4239DA3094BC98C4C37A72614B5ABE0BCF77_gshared (ObiNativeList_1_t667688879E4BD2DFE8EDD92169E59F19619DF4F4 * __this, int32_t ___arrayLength0, const RuntimeMethod* method)
{
	{
		// NativeArray<U> array = NativeArrayUnsafeUtility.ConvertExistingDataToNativeArray<U>(m_AlignedPtr, arrayLength, Allocator.None);
		void* L_0 = (void*)__this->get_m_AlignedPtr_1();
		int32_t L_1 = ___arrayLength0;
		NativeArray_1_t381CA7D92591F9C96DB84965C31AF8713A546156  L_2 = ((  NativeArray_1_t381CA7D92591F9C96DB84965C31AF8713A546156  (*) (void*, int32_t, int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->methodPointer)((void*)(void*)L_0, (int32_t)L_1, (int32_t)1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		// return array;
		return (NativeArray_1_t381CA7D92591F9C96DB84965C31AF8713A546156 )L_2;
	}
}
// Unity.Collections.NativeArray`1<U> Obi.ObiNativeList`1<Oni_Contact>::AsNativeArray<Oni_Contact>()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR NativeArray_1_tD60C94148A4EF939C0FEC50D112D82FBFA965023  ObiNativeList_1_AsNativeArray_TisContact_tA38E76CD5E9A53DE837B07BBB25CABE68B2AF756_m548F03942BBFD91F8A6DFC5426A3162EFB9986FE_gshared (ObiNativeList_1_tCEAE7C719A3DBFC45782E33AFCE91E13E9CA9AF4 * __this, const RuntimeMethod* method)
{
	{
		// return AsNativeArray<U>(m_Count);
		int32_t L_0 = (int32_t)__this->get_m_Count_4();
		NullCheck((ObiNativeList_1_tCEAE7C719A3DBFC45782E33AFCE91E13E9CA9AF4 *)__this);
		NativeArray_1_tD60C94148A4EF939C0FEC50D112D82FBFA965023  L_1 = ((  NativeArray_1_tD60C94148A4EF939C0FEC50D112D82FBFA965023  (*) (ObiNativeList_1_tCEAE7C719A3DBFC45782E33AFCE91E13E9CA9AF4 *, int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->methodPointer)((ObiNativeList_1_tCEAE7C719A3DBFC45782E33AFCE91E13E9CA9AF4 *)__this, (int32_t)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		return (NativeArray_1_tD60C94148A4EF939C0FEC50D112D82FBFA965023 )L_1;
	}
}
// Unity.Collections.NativeArray`1<U> Obi.ObiNativeList`1<Oni_Contact>::AsNativeArray<Oni_Contact>(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR NativeArray_1_tD60C94148A4EF939C0FEC50D112D82FBFA965023  ObiNativeList_1_AsNativeArray_TisContact_tA38E76CD5E9A53DE837B07BBB25CABE68B2AF756_mBDBA6F80FB8DE4F7A49591BC82B43AF593A477EA_gshared (ObiNativeList_1_tCEAE7C719A3DBFC45782E33AFCE91E13E9CA9AF4 * __this, int32_t ___arrayLength0, const RuntimeMethod* method)
{
	{
		// NativeArray<U> array = NativeArrayUnsafeUtility.ConvertExistingDataToNativeArray<U>(m_AlignedPtr, arrayLength, Allocator.None);
		void* L_0 = (void*)__this->get_m_AlignedPtr_1();
		int32_t L_1 = ___arrayLength0;
		NativeArray_1_tD60C94148A4EF939C0FEC50D112D82FBFA965023  L_2 = ((  NativeArray_1_tD60C94148A4EF939C0FEC50D112D82FBFA965023  (*) (void*, int32_t, int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->methodPointer)((void*)(void*)L_0, (int32_t)L_1, (int32_t)1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		// return array;
		return (NativeArray_1_tD60C94148A4EF939C0FEC50D112D82FBFA965023 )L_2;
	}
}

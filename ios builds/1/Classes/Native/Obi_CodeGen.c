﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif



#include "codegen/il2cpp-codegen-metadata.h"





IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END




// 0x00000001 System.Void Microsoft.CodeAnalysis.EmbeddedAttribute::.ctor()
extern void EmbeddedAttribute__ctor_mF092B41AF606A0079DB72F589B9F3A4B09A2879C (void);
// 0x00000002 System.Void System.Runtime.CompilerServices.IsReadOnlyAttribute::.ctor()
extern void IsReadOnlyAttribute__ctor_mA5B530204715922B8E11EA362EE266FA7E65613D (void);
// 0x00000003 System.Boolean ObiContactGrabber::get_grabbed()
extern void ObiContactGrabber_get_grabbed_mB390126778E0A789B441B372D5F525DB318BC301 (void);
// 0x00000004 System.Void ObiContactGrabber::Awake()
extern void ObiContactGrabber_Awake_m81FAB1A7D0894DFF410B881A4BD34C377CCDF33D (void);
// 0x00000005 System.Void ObiContactGrabber::OnEnable()
extern void ObiContactGrabber_OnEnable_m0CDF84CEFA0997E0C363D99406A72B7F6617015D (void);
// 0x00000006 System.Void ObiContactGrabber::OnDisable()
extern void ObiContactGrabber_OnDisable_m70E63109356461451716F2656B1F703ABDF30B67 (void);
// 0x00000007 System.Void ObiContactGrabber::Solver_OnCollision(System.Object,Obi.ObiSolver_ObiCollisionEventArgs)
extern void ObiContactGrabber_Solver_OnCollision_m02CB781C94F8F518ADC1B61E01487BBDC62BA026 (void);
// 0x00000008 System.Void ObiContactGrabber::UpdateParticleProperties()
extern void ObiContactGrabber_UpdateParticleProperties_m874B590742CF4C376FDD13678A387918D098B615 (void);
// 0x00000009 System.Boolean ObiContactGrabber::GrabParticle(Obi.ObiSolver,System.Int32)
extern void ObiContactGrabber_GrabParticle_mA112EAB2920DB3B64AA31FCF9455C6F4702858B5 (void);
// 0x0000000A System.Void ObiContactGrabber::Grab()
extern void ObiContactGrabber_Grab_mF88B7A50127D874AC0295C6D7D98FFEBE6D8F618 (void);
// 0x0000000B System.Void ObiContactGrabber::Release()
extern void ObiContactGrabber_Release_m82B5D7B992BC9423026E8CE0BD6DE0884D72BA68 (void);
// 0x0000000C System.Void ObiContactGrabber::FixedUpdate()
extern void ObiContactGrabber_FixedUpdate_mCAD4BA24875E897F95967F4540B06AC26800BF01 (void);
// 0x0000000D System.Void ObiContactGrabber::.ctor()
extern void ObiContactGrabber__ctor_m9C1D048C7D8633509DB48156260CB765A7E6E364 (void);
// 0x0000000E System.Void ObiContactGrabber_GrabbedParticle::.ctor(Obi.ObiSolver,System.Int32,System.Single)
extern void GrabbedParticle__ctor_m29866A846BC99D62A7E53A7F55FD6234B7DB7567 (void);
// 0x0000000F System.Boolean ObiContactGrabber_GrabbedParticle::Equals(ObiContactGrabber_GrabbedParticle,ObiContactGrabber_GrabbedParticle)
extern void GrabbedParticle_Equals_mFF2582C3BFDDE99366B35AC5DF73C20ECE8E430D (void);
// 0x00000010 System.Int32 ObiContactGrabber_GrabbedParticle::GetHashCode(ObiContactGrabber_GrabbedParticle)
extern void GrabbedParticle_GetHashCode_m37D33E44CE7086BE8CD4AF64B4270BE0FADDACC9 (void);
// 0x00000011 System.Runtime.InteropServices.GCHandle Oni::PinMemory(System.Object)
extern void Oni_PinMemory_m03E1EA6D2AD2B2501D052E52FDE072606EDEB6EF (void);
// 0x00000012 System.Void Oni::UnpinMemory(System.Runtime.InteropServices.GCHandle)
extern void Oni_UnpinMemory_m029F0BA710E2DBE187A2300ED9F5CFA9DE37B62F (void);
// 0x00000013 System.Void Oni::UpdateColliderGrid(System.Single)
extern void Oni_UpdateColliderGrid_m75034617DEA373FD3AAC6D3F57A9530AA426895D (void);
// 0x00000014 System.Void Oni::SetColliders(System.IntPtr,System.IntPtr,System.IntPtr,System.Int32)
extern void Oni_SetColliders_m901960CF23BF96A6DCE6751F09B4425431091A65 (void);
// 0x00000015 System.Void Oni::SetRigidbodies(System.IntPtr)
extern void Oni_SetRigidbodies_mF84FEC7A0C4A850D6CD1F9CF39A29C2D6AFEAECF (void);
// 0x00000016 System.Void Oni::SetCollisionMaterials(System.IntPtr)
extern void Oni_SetCollisionMaterials_mAAE3428F89A91AF30BB753E728556AD1F592F3B1 (void);
// 0x00000017 System.Void Oni::SetTriangleMeshData(System.IntPtr,System.IntPtr,System.IntPtr,System.IntPtr)
extern void Oni_SetTriangleMeshData_mFFF4366C8A8B13EDA5301EDE1CED9A2CD92AE43D (void);
// 0x00000018 System.Void Oni::SetEdgeMeshData(System.IntPtr,System.IntPtr,System.IntPtr,System.IntPtr)
extern void Oni_SetEdgeMeshData_m7D6EBD72BA2169E3C697F341B75BC716161524D0 (void);
// 0x00000019 System.Void Oni::SetDistanceFieldData(System.IntPtr,System.IntPtr)
extern void Oni_SetDistanceFieldData_mED7CC8724B49A2AF36F817A57152CD95AA6E0200 (void);
// 0x0000001A System.Void Oni::SetHeightFieldData(System.IntPtr,System.IntPtr)
extern void Oni_SetHeightFieldData_m3890F2905A2990080378C1C2F28A9AD2DA077BEE (void);
// 0x0000001B System.IntPtr Oni::CreateSolver(System.Int32)
extern void Oni_CreateSolver_m95484CA4C8E4048EB871BB224918540A317C885A (void);
// 0x0000001C System.Void Oni::DestroySolver(System.IntPtr)
extern void Oni_DestroySolver_m0ADEE02261B34DC1C348911FCDC53E4471B37D0B (void);
// 0x0000001D System.Void Oni::SetCapacity(System.IntPtr,System.Int32)
extern void Oni_SetCapacity_m4804EBFCAF115506C8DD5332214C6A2E0CB4810B (void);
// 0x0000001E System.Void Oni::InitializeFrame(System.IntPtr,UnityEngine.Vector4&,UnityEngine.Vector4&,UnityEngine.Quaternion&)
extern void Oni_InitializeFrame_mB306333E4A867678C088DACC72B039EF58E80D31 (void);
// 0x0000001F System.Void Oni::UpdateFrame(System.IntPtr,UnityEngine.Vector4&,UnityEngine.Vector4&,UnityEngine.Quaternion&,System.Single)
extern void Oni_UpdateFrame_m680BE80814872FABDBFDC7ABBE5FF1E740C5773D (void);
// 0x00000020 System.Void Oni::ApplyFrame(System.IntPtr,System.Single,System.Single,System.Single,System.Single,System.Single)
extern void Oni_ApplyFrame_mD136F376FE68D93651459FB1B946CE18BFAA8D10 (void);
// 0x00000021 System.Void Oni::RecalculateInertiaTensors(System.IntPtr)
extern void Oni_RecalculateInertiaTensors_m5D778943EF3E20DCF00A85175E39DD691A8AFA16 (void);
// 0x00000022 System.Void Oni::ResetForces(System.IntPtr)
extern void Oni_ResetForces_m38A179189C49CCAC6142D9F38F234BA8D03C5F64 (void);
// 0x00000023 System.Void Oni::SetRigidbodyLinearDeltas(System.IntPtr,System.IntPtr)
extern void Oni_SetRigidbodyLinearDeltas_m3D5B23B5969BC13392B4E7E019D03730E61E3639 (void);
// 0x00000024 System.Void Oni::SetRigidbodyAngularDeltas(System.IntPtr,System.IntPtr)
extern void Oni_SetRigidbodyAngularDeltas_m4A436C37D1F6F9FE39F1184279883463F586CBF0 (void);
// 0x00000025 System.Void Oni::GetBounds(System.IntPtr,UnityEngine.Vector3&,UnityEngine.Vector3&)
extern void Oni_GetBounds_m363A96EE8BAB3FB760C636B8C5F361B8EE1E53B5 (void);
// 0x00000026 System.Int32 Oni::GetParticleGridSize(System.IntPtr)
extern void Oni_GetParticleGridSize_m2BD9A7939A0F7733323CA9C9EDE32C53FC7B6ABD (void);
// 0x00000027 System.Void Oni::GetParticleGrid(System.IntPtr,Oni_GridCell[])
extern void Oni_GetParticleGrid_m21D4AA3548CF77E25C5E456096E16162C1BAD4C0 (void);
// 0x00000028 System.Int32 Oni::SpatialQuery(System.IntPtr,System.IntPtr,System.IntPtr,System.Int32)
extern void Oni_SpatialQuery_m526F90093505F1A4FB3E31A2AC6EC6B8C1F850A5 (void);
// 0x00000029 System.Void Oni::GetQueryResults(System.IntPtr,System.IntPtr,System.Int32)
extern void Oni_GetQueryResults_mB69A3B8645A8218700E75C49956E15AC27B125B7 (void);
// 0x0000002A System.Void Oni::SetSolverParameters(System.IntPtr,Oni_SolverParameters&)
extern void Oni_SetSolverParameters_mA78FF4C834E23D50A56D989521B278AA6B11BD48 (void);
// 0x0000002B System.Void Oni::GetSolverParameters(System.IntPtr,Oni_SolverParameters&)
extern void Oni_GetSolverParameters_mD299B385DF9770D77C9E0D4F4781EFADE20C30E4 (void);
// 0x0000002C System.Int32 Oni::SetActiveParticles(System.IntPtr,System.Int32[],System.Int32)
extern void Oni_SetActiveParticles_mA8361CFE331459188A7701673465FCEC0770F84B (void);
// 0x0000002D System.IntPtr Oni::CollisionDetection(System.IntPtr,System.Single)
extern void Oni_CollisionDetection_mD890F2AED814E8B7F3766DDD603916EDA4B1B861 (void);
// 0x0000002E System.IntPtr Oni::Step(System.IntPtr,System.Single,System.Single,System.Int32)
extern void Oni_Step_m73AE7B946E5053FC14C8061FCD6508D4824213B7 (void);
// 0x0000002F System.Void Oni::ApplyPositionInterpolation(System.IntPtr,System.IntPtr,System.IntPtr,System.Single,System.Single)
extern void Oni_ApplyPositionInterpolation_mA7A631314B43D3A62FAF81D209F1CCB00A3A901D (void);
// 0x00000030 System.Void Oni::UpdateSkeletalAnimation(System.IntPtr)
extern void Oni_UpdateSkeletalAnimation_m0F0F49630F0ABF1B6C742D13FEDB8A69B3C71A31 (void);
// 0x00000031 System.Int32 Oni::GetConstraintCount(System.IntPtr,System.Int32)
extern void Oni_GetConstraintCount_m046D7B8D04B0EDDC24A336FA1AD6F93B181B143A (void);
// 0x00000032 System.Void Oni::SetRenderableParticlePositions(System.IntPtr,System.IntPtr)
extern void Oni_SetRenderableParticlePositions_mF10457D7F9724C687D3AB6E83DEEBB4C9937F138 (void);
// 0x00000033 System.Void Oni::SetParticlePhases(System.IntPtr,System.IntPtr)
extern void Oni_SetParticlePhases_m330FC442852B31A851FB05486D9D5D4A9E85080C (void);
// 0x00000034 System.Void Oni::SetParticleFilters(System.IntPtr,System.IntPtr)
extern void Oni_SetParticleFilters_m4689F2DCACACC85971902111BF779817074E976C (void);
// 0x00000035 System.Void Oni::SetParticleCollisionMaterials(System.IntPtr,System.IntPtr)
extern void Oni_SetParticleCollisionMaterials_m64562F4539FF5805E44B181ED119B61FB636DB91 (void);
// 0x00000036 System.Void Oni::SetParticlePositions(System.IntPtr,System.IntPtr)
extern void Oni_SetParticlePositions_mBF4B08453E99AACC5D3C934F3B7F1EDB2AA3934A (void);
// 0x00000037 System.Void Oni::SetParticlePreviousPositions(System.IntPtr,System.IntPtr)
extern void Oni_SetParticlePreviousPositions_mF7643A2DD9F3D10409DF62601E0D8EE2D0DBD7E4 (void);
// 0x00000038 System.Void Oni::SetParticleOrientations(System.IntPtr,System.IntPtr)
extern void Oni_SetParticleOrientations_m92BF1084EB29939C992A0666D1E75C6AAB14801D (void);
// 0x00000039 System.Void Oni::SetParticlePreviousOrientations(System.IntPtr,System.IntPtr)
extern void Oni_SetParticlePreviousOrientations_m08E1313630723606D179B6FB44025E49B838D2BB (void);
// 0x0000003A System.Void Oni::SetRenderableParticleOrientations(System.IntPtr,System.IntPtr)
extern void Oni_SetRenderableParticleOrientations_mE90A2228D4CDC6641661C730F1164F65609788B8 (void);
// 0x0000003B System.Void Oni::SetParticleInverseMasses(System.IntPtr,System.IntPtr)
extern void Oni_SetParticleInverseMasses_mA5F3998095FBD44B558C75B634E18628520D2385 (void);
// 0x0000003C System.Void Oni::SetParticleInverseRotationalMasses(System.IntPtr,System.IntPtr)
extern void Oni_SetParticleInverseRotationalMasses_mBB6281669729C39BEF623DDDEDCC12EC93C31FBB (void);
// 0x0000003D System.Void Oni::SetParticlePrincipalRadii(System.IntPtr,System.IntPtr)
extern void Oni_SetParticlePrincipalRadii_mCE141D972ABA98C2CCC7B66419BC0009E227F8CE (void);
// 0x0000003E System.Void Oni::SetParticleVelocities(System.IntPtr,System.IntPtr)
extern void Oni_SetParticleVelocities_m806F66F28AB0D4E632921B7E0CF3464C777B00C7 (void);
// 0x0000003F System.Void Oni::SetParticleAngularVelocities(System.IntPtr,System.IntPtr)
extern void Oni_SetParticleAngularVelocities_mD376C89032B153B1C2EB36FAF673C90600871052 (void);
// 0x00000040 System.Void Oni::SetParticleExternalForces(System.IntPtr,System.IntPtr)
extern void Oni_SetParticleExternalForces_mABE204855B05F30D980984785EFC178E8178657B (void);
// 0x00000041 System.Void Oni::SetParticleExternalTorques(System.IntPtr,System.IntPtr)
extern void Oni_SetParticleExternalTorques_m52F453DFC994BB2CB946C0ECFCA90755E8CE81BF (void);
// 0x00000042 System.Void Oni::SetParticleWinds(System.IntPtr,System.IntPtr)
extern void Oni_SetParticleWinds_m4D8B34B1F691B0D06B66B5AC4A9CE7A47C438D7A (void);
// 0x00000043 System.Void Oni::SetParticlePositionDeltas(System.IntPtr,System.IntPtr)
extern void Oni_SetParticlePositionDeltas_m44290488FA81D82A7B5E34C6C419183E4AC4C7F3 (void);
// 0x00000044 System.Void Oni::SetParticleOrientationDeltas(System.IntPtr,System.IntPtr)
extern void Oni_SetParticleOrientationDeltas_m80BA163C8E315CDE4D535070E044140582F4FB75 (void);
// 0x00000045 System.Void Oni::SetParticlePositionConstraintCounts(System.IntPtr,System.IntPtr)
extern void Oni_SetParticlePositionConstraintCounts_mC6C3DF0767ABFE55B7D56664BD2B85CACC2F2AF3 (void);
// 0x00000046 System.Void Oni::SetParticleOrientationConstraintCounts(System.IntPtr,System.IntPtr)
extern void Oni_SetParticleOrientationConstraintCounts_mE8DE30A217492D6C2961351F94471600E84DDF33 (void);
// 0x00000047 System.Void Oni::SetParticleNormals(System.IntPtr,System.IntPtr)
extern void Oni_SetParticleNormals_mC7DB3812E3D8DA4944D5783D612327C14DAA1407 (void);
// 0x00000048 System.Void Oni::SetParticleInverseInertiaTensors(System.IntPtr,System.IntPtr)
extern void Oni_SetParticleInverseInertiaTensors_m107AC92B3D36DA0EC84A2BC81EAAFDE1A87645F3 (void);
// 0x00000049 System.Void Oni::SetParticleSmoothingRadii(System.IntPtr,System.IntPtr)
extern void Oni_SetParticleSmoothingRadii_m23C24AAADB64BB87F5AB6C0F29146E045BD15F32 (void);
// 0x0000004A System.Void Oni::SetParticleBuoyancy(System.IntPtr,System.IntPtr)
extern void Oni_SetParticleBuoyancy_m1D5C0C42C6E1F7F9F0009907C3C47777493BFFFD (void);
// 0x0000004B System.Void Oni::SetParticleRestDensities(System.IntPtr,System.IntPtr)
extern void Oni_SetParticleRestDensities_mC2C5BF07F3F31A9A9BD30F1E0FD65FA90DAB8668 (void);
// 0x0000004C System.Void Oni::SetParticleViscosities(System.IntPtr,System.IntPtr)
extern void Oni_SetParticleViscosities_m40D16C05777FCCBA236F8812E90FCE61732F8F65 (void);
// 0x0000004D System.Void Oni::SetParticleSurfaceTension(System.IntPtr,System.IntPtr)
extern void Oni_SetParticleSurfaceTension_mEB650D756ACFD8AFCE0B0CA1FA516B8A04166803 (void);
// 0x0000004E System.Void Oni::SetParticleVorticityConfinement(System.IntPtr,System.IntPtr)
extern void Oni_SetParticleVorticityConfinement_m490207BCEB92C2090FA6F812903E9DC720C542CD (void);
// 0x0000004F System.Void Oni::SetParticleAtmosphericDragPressure(System.IntPtr,System.IntPtr,System.IntPtr)
extern void Oni_SetParticleAtmosphericDragPressure_m0DE4F75155DE8255D542463CED62BBA10DE95991 (void);
// 0x00000050 System.Void Oni::SetParticleDiffusion(System.IntPtr,System.IntPtr)
extern void Oni_SetParticleDiffusion_m2E7A56B1429ABF2C68BCBEAF9A8B903BCFBBEB97 (void);
// 0x00000051 System.Void Oni::SetParticleVorticities(System.IntPtr,System.IntPtr)
extern void Oni_SetParticleVorticities_mBFAFD13B80D055251ECE3BB09331FEE93EC096CE (void);
// 0x00000052 System.Void Oni::SetParticleFluidData(System.IntPtr,System.IntPtr)
extern void Oni_SetParticleFluidData_m5475F465341FBD9D3646CF21F640D31DBBDA52BC (void);
// 0x00000053 System.Void Oni::SetParticleUserData(System.IntPtr,System.IntPtr)
extern void Oni_SetParticleUserData_m1C95E2860A342CC2408369BE71453D688A4940E2 (void);
// 0x00000054 System.Void Oni::SetParticleAnisotropies(System.IntPtr,System.IntPtr)
extern void Oni_SetParticleAnisotropies_m4F40683E247F34D06C6FAC46E7BB2D100699D943 (void);
// 0x00000055 System.Void Oni::SetSimplices(System.IntPtr,System.Int32[],System.Int32,System.Int32,System.Int32)
extern void Oni_SetSimplices_mFA1EA60EF9F96B8AD384993D7AC15B73A2B08D95 (void);
// 0x00000056 System.Int32 Oni::GetDeformableTriangleCount(System.IntPtr)
extern void Oni_GetDeformableTriangleCount_m3D720CBB04BADD1E4AAE5D0F630E7E8A1AB8D398 (void);
// 0x00000057 System.Void Oni::SetDeformableTriangles(System.IntPtr,System.Int32[],System.Int32,System.Int32)
extern void Oni_SetDeformableTriangles_m93D02F1E05CEF46D51296532191387392C8885C6 (void);
// 0x00000058 System.Int32 Oni::RemoveDeformableTriangles(System.IntPtr,System.Int32,System.Int32)
extern void Oni_RemoveDeformableTriangles_mA0A4A28EA77A35743C37CF80A333559E7C8CC8AF (void);
// 0x00000059 System.Void Oni::SetConstraintGroupParameters(System.IntPtr,System.Int32,Oni_ConstraintParameters&)
extern void Oni_SetConstraintGroupParameters_m7402C64CA0198FD7D07E4F2BBFC81F8C9DB926B3 (void);
// 0x0000005A System.Void Oni::GetConstraintGroupParameters(System.IntPtr,System.Int32,Oni_ConstraintParameters&)
extern void Oni_GetConstraintGroupParameters_m841DD84B6D78A0B694506D815E4756313934B29F (void);
// 0x0000005B System.Void Oni::SetRestPositions(System.IntPtr,System.IntPtr)
extern void Oni_SetRestPositions_m819342E902C330762AEB5BE9D4C68F31DA6CA8A1 (void);
// 0x0000005C System.Void Oni::SetRestOrientations(System.IntPtr,System.IntPtr)
extern void Oni_SetRestOrientations_m87B3C65B2FA89627A6526A5C22B674280DC3C577 (void);
// 0x0000005D System.IntPtr Oni::CreateBatch(System.Int32)
extern void Oni_CreateBatch_m289C846274F339F1F3E4A026FE857DAAAC56ABE8 (void);
// 0x0000005E System.Void Oni::DestroyBatch(System.IntPtr)
extern void Oni_DestroyBatch_m0C065F052A4E0B16A426180BF8EC84E74845007E (void);
// 0x0000005F System.IntPtr Oni::AddBatch(System.IntPtr,System.IntPtr)
extern void Oni_AddBatch_m22AFEB35DB5CD69377763213EEED12183545A8DE (void);
// 0x00000060 System.Void Oni::RemoveBatch(System.IntPtr,System.IntPtr)
extern void Oni_RemoveBatch_m62A5EE513FF64B56579A035F1463C0308DD01A48 (void);
// 0x00000061 System.Boolean Oni::EnableBatch(System.IntPtr,System.Boolean)
extern void Oni_EnableBatch_mE5AAEE57C0386681A3016600946C205D90CCBD0B (void);
// 0x00000062 System.Int32 Oni::GetBatchConstraintForces(System.IntPtr,System.Single[],System.Int32,System.Int32)
extern void Oni_GetBatchConstraintForces_m2F4FB4C6732CFC54C859D7DB5C4A3AC75E4400FE (void);
// 0x00000063 System.Void Oni::SetBatchConstraintCount(System.IntPtr,System.Int32)
extern void Oni_SetBatchConstraintCount_m31FD93985340AF90A5D98742034D010888144B41 (void);
// 0x00000064 System.Int32 Oni::GetBatchConstraintCount(System.IntPtr)
extern void Oni_GetBatchConstraintCount_mCDA7F7C7B72B04711AAD3435FFE3AB6FDF657966 (void);
// 0x00000065 System.Void Oni::SetDistanceConstraints(System.IntPtr,System.IntPtr,System.IntPtr,System.IntPtr,System.IntPtr,System.Int32)
extern void Oni_SetDistanceConstraints_m6BAF3B1FE95B48E8E681121385077A369140BA88 (void);
// 0x00000066 System.Void Oni::SetBendingConstraints(System.IntPtr,System.IntPtr,System.IntPtr,System.IntPtr,System.IntPtr,System.IntPtr,System.Int32)
extern void Oni_SetBendingConstraints_m4DD1607554632126E50CA62B31DE223000DEE579 (void);
// 0x00000067 System.Void Oni::SetSkinConstraints(System.IntPtr,System.IntPtr,System.IntPtr,System.IntPtr,System.IntPtr,System.IntPtr,System.IntPtr,System.Int32)
extern void Oni_SetSkinConstraints_m980F260DC4107F86B16555CB40B9B63A196F558C (void);
// 0x00000068 System.Void Oni::SetAerodynamicConstraints(System.IntPtr,System.IntPtr,System.IntPtr,System.Int32)
extern void Oni_SetAerodynamicConstraints_m97BED4EFAF9B73A677757503EF1AF7EA59CB712C (void);
// 0x00000069 System.Void Oni::SetVolumeConstraints(System.IntPtr,System.IntPtr,System.IntPtr,System.IntPtr,System.IntPtr,System.IntPtr,System.IntPtr,System.Int32)
extern void Oni_SetVolumeConstraints_m7914A0DFEEF418BE4C6C81DE6A27EEC21701F511 (void);
// 0x0000006A System.Void Oni::SetShapeMatchingConstraints(System.IntPtr,System.IntPtr,System.IntPtr,System.IntPtr,System.IntPtr,System.IntPtr,System.IntPtr,System.IntPtr,System.IntPtr,System.IntPtr,System.IntPtr,System.Int32)
extern void Oni_SetShapeMatchingConstraints_mE2D86779FF689D2FBEB8F39FF1683CEAA9D7547E (void);
// 0x0000006B System.Void Oni::CalculateRestShapeMatching(System.IntPtr,System.IntPtr)
extern void Oni_CalculateRestShapeMatching_mD268677C88F2E8ABB2C0E8149F0683FFF9AF3BDD (void);
// 0x0000006C System.Void Oni::SetStretchShearConstraints(System.IntPtr,System.IntPtr,System.IntPtr,System.IntPtr,System.IntPtr,System.IntPtr,System.IntPtr,System.Int32)
extern void Oni_SetStretchShearConstraints_m49F7345551857151AE2572A3F59A90FCA236CDA8 (void);
// 0x0000006D System.Void Oni::SetBendTwistConstraints(System.IntPtr,System.IntPtr,System.IntPtr,System.IntPtr,System.IntPtr,System.IntPtr,System.Int32)
extern void Oni_SetBendTwistConstraints_mE4976251409BFB7882CC39AD1F21D4A97B7F4D03 (void);
// 0x0000006E System.Void Oni::SetTetherConstraints(System.IntPtr,System.IntPtr,System.IntPtr,System.IntPtr,System.IntPtr,System.Int32)
extern void Oni_SetTetherConstraints_m10810C34086C7751FB8A5600EFBBC9C70887449E (void);
// 0x0000006F System.Void Oni::SetPinConstraints(System.IntPtr,System.IntPtr,System.IntPtr,System.IntPtr,System.IntPtr,System.IntPtr,System.IntPtr,System.Int32)
extern void Oni_SetPinConstraints_mC5929BC4A3B8861A32C47072223BD7C88CAC8843 (void);
// 0x00000070 System.Void Oni::SetStitchConstraints(System.IntPtr,System.IntPtr,System.IntPtr,System.IntPtr,System.Int32)
extern void Oni_SetStitchConstraints_m8576B5213542C38E501BD55E1C0936DF69506253 (void);
// 0x00000071 System.Void Oni::SetChainConstraints(System.IntPtr,System.IntPtr,System.IntPtr,System.IntPtr,System.IntPtr,System.Int32)
extern void Oni_SetChainConstraints_m8F08E1B952B69C7A6D3F1957CA4511BE3D95D25D (void);
// 0x00000072 System.Void Oni::GetCollisionContacts(System.IntPtr,Oni_Contact[],System.Int32)
extern void Oni_GetCollisionContacts_mD80B55291B76FB42F97EE2C75EB2E5089540AE63 (void);
// 0x00000073 System.Void Oni::GetParticleCollisionContacts(System.IntPtr,Oni_Contact[],System.Int32)
extern void Oni_GetParticleCollisionContacts_mCDAC4DB2F8B1A798CEC15191044C4FE761AF69D1 (void);
// 0x00000074 System.Int32 Oni::InterpolateDiffuseParticles(System.IntPtr,System.IntPtr,System.IntPtr,System.IntPtr,System.IntPtr,System.Int32)
extern void Oni_InterpolateDiffuseParticles_m4A3F61BFA0B073204F17E45D12E58B292B3C5D44 (void);
// 0x00000075 System.Int32 Oni::MakePhase(System.Int32,Obi.ObiUtils_ParticleFlags)
extern void Oni_MakePhase_m9FFA8D86C974D669797A0DF8FF1E6A6270554D71 (void);
// 0x00000076 System.Int32 Oni::GetGroupFromPhase(System.Int32)
extern void Oni_GetGroupFromPhase_m1E9E33EEBF211AD24F224C4DDF044F55DEC4E6CF (void);
// 0x00000077 System.Int32 Oni::GetFlagsFromPhase(System.Int32)
extern void Oni_GetFlagsFromPhase_m4EC4AEB3B15B30111080A14FB54D7939A4924F0B (void);
// 0x00000078 System.Single Oni::BendingConstraintRest(System.Single[])
extern void Oni_BendingConstraintRest_mBF9E6E4CB4E998E291B956B4304FFB31A1ED6058 (void);
// 0x00000079 System.Void Oni::CompleteAll()
extern void Oni_CompleteAll_m24DA6FE9992164ABDA19A2E0E5BB58280FD02582 (void);
// 0x0000007A System.Void Oni::Complete(System.IntPtr)
extern void Oni_Complete_m4909573C5EF85056474F9EEB3A8C91F6F62050B5 (void);
// 0x0000007B System.IntPtr Oni::CreateEmpty()
extern void Oni_CreateEmpty_mEEB6A72C1C211426318B7A3164BD1697F1C0859C (void);
// 0x0000007C System.Void Oni::Schedule(System.IntPtr)
extern void Oni_Schedule_mF1BBBF5D226699D18E8EB55A8F31A08BE1DE528B (void);
// 0x0000007D System.Void Oni::AddChild(System.IntPtr,System.IntPtr)
extern void Oni_AddChild_m2AA3FE95D394A0AB211C7FAF46DA6DDA34645B49 (void);
// 0x0000007E System.Int32 Oni::GetMaxSystemConcurrency()
extern void Oni_GetMaxSystemConcurrency_mA467F4FF67759D65BBACC64B265BBD53DBEE3177 (void);
// 0x0000007F System.Void Oni::ClearProfiler()
extern void Oni_ClearProfiler_m3CE1E415966CFAA8CBF71A57B412B980CE3B5E11 (void);
// 0x00000080 System.Void Oni::EnableProfiler(System.Boolean)
extern void Oni_EnableProfiler_m50B45E5B063E18108D99A6851C2C441A8FEDCFD4 (void);
// 0x00000081 System.Void Oni::BeginSample(System.String,System.Byte)
extern void Oni_BeginSample_m584963073106DEA213EA5679DA42F89AB29B0931 (void);
// 0x00000082 System.Void Oni::EndSample()
extern void Oni_EndSample_mE3D8141E3F15F6DEEB30C16BDA9BC9E82368F8EE (void);
// 0x00000083 System.Int32 Oni::GetProfilingInfoCount()
extern void Oni_GetProfilingInfoCount_m89525656BF980E088FBF35150F29DC2BCAF974EB (void);
// 0x00000084 System.Void Oni::GetProfilingInfo(Oni_ProfileInfo[],System.Int32)
extern void Oni_GetProfilingInfo_m5450B203396A0BD0042264E198B358D3FA41BA23 (void);
// 0x00000085 System.Void Oni_SolverParameters::.ctor(Oni_SolverParameters_Interpolation,UnityEngine.Vector4)
extern void SolverParameters__ctor_mBBC8FFC199730BCFDE16BEE7BA9BCC31B01476C3_AdjustorThunk (void);
// 0x00000086 System.Void Oni_ConstraintParameters::.ctor(System.Boolean,Oni_ConstraintParameters_EvaluationOrder,System.Int32)
extern void ConstraintParameters__ctor_m3554D9568B4E65EF55CF0E0477821F3FC84C6566_AdjustorThunk (void);
// 0x00000087 System.Int32 Obi.IObiParticleCollection::get_particleCount()
// 0x00000088 System.Int32 Obi.IObiParticleCollection::get_activeParticleCount()
// 0x00000089 System.Boolean Obi.IObiParticleCollection::get_usesOrientedParticles()
// 0x0000008A System.Int32 Obi.IObiParticleCollection::GetParticleRuntimeIndex(System.Int32)
// 0x0000008B UnityEngine.Vector3 Obi.IObiParticleCollection::GetParticlePosition(System.Int32)
// 0x0000008C UnityEngine.Quaternion Obi.IObiParticleCollection::GetParticleOrientation(System.Int32)
// 0x0000008D System.Void Obi.IObiParticleCollection::GetParticleAnisotropy(System.Int32,UnityEngine.Vector4&,UnityEngine.Vector4&,UnityEngine.Vector4&)
// 0x0000008E System.Single Obi.IObiParticleCollection::GetParticleMaxRadius(System.Int32)
// 0x0000008F UnityEngine.Color Obi.IObiParticleCollection::GetParticleColor(System.Int32)
// 0x00000090 System.Void Obi.ObiActor::add_OnBlueprintLoaded(Obi.ObiActor_ActorBlueprintCallback)
extern void ObiActor_add_OnBlueprintLoaded_m04B2952178ABFA72F0A7E7219EB78A4ECEAE156E (void);
// 0x00000091 System.Void Obi.ObiActor::remove_OnBlueprintLoaded(Obi.ObiActor_ActorBlueprintCallback)
extern void ObiActor_remove_OnBlueprintLoaded_m998C17DB7FED249637AC6D40775D9D50ED1CA3F8 (void);
// 0x00000092 System.Void Obi.ObiActor::add_OnBlueprintUnloaded(Obi.ObiActor_ActorBlueprintCallback)
extern void ObiActor_add_OnBlueprintUnloaded_mB1182B14609EA8CCD5A9BD7C2F3CEDDAFF78C601 (void);
// 0x00000093 System.Void Obi.ObiActor::remove_OnBlueprintUnloaded(Obi.ObiActor_ActorBlueprintCallback)
extern void ObiActor_remove_OnBlueprintUnloaded_mB1AB8C2B322EB4633B0CD45EF5AAF7448BAD68BB (void);
// 0x00000094 System.Void Obi.ObiActor::add_OnPrepareStep(Obi.ObiActor_ActorStepCallback)
extern void ObiActor_add_OnPrepareStep_mC586B960142DDDF81A5B1C87D393C7CA379E743E (void);
// 0x00000095 System.Void Obi.ObiActor::remove_OnPrepareStep(Obi.ObiActor_ActorStepCallback)
extern void ObiActor_remove_OnPrepareStep_mCB8B15D01E8DA343CF2DD8E655932C886DD863CC (void);
// 0x00000096 System.Void Obi.ObiActor::add_OnBeginStep(Obi.ObiActor_ActorStepCallback)
extern void ObiActor_add_OnBeginStep_m7A401277ED988AF4C6388B1F768B4E613BA02E10 (void);
// 0x00000097 System.Void Obi.ObiActor::remove_OnBeginStep(Obi.ObiActor_ActorStepCallback)
extern void ObiActor_remove_OnBeginStep_m80E4C1C9DE59FB6075BFB27E9B8D885ADC413C13 (void);
// 0x00000098 System.Void Obi.ObiActor::add_OnSubstep(Obi.ObiActor_ActorStepCallback)
extern void ObiActor_add_OnSubstep_m4E244BDD4F0DA8AFBEF75036632A65CF4EC1286F (void);
// 0x00000099 System.Void Obi.ObiActor::remove_OnSubstep(Obi.ObiActor_ActorStepCallback)
extern void ObiActor_remove_OnSubstep_mABFE386CEC0194553AEE608F465F311B21360592 (void);
// 0x0000009A System.Void Obi.ObiActor::add_OnEndStep(Obi.ObiActor_ActorStepCallback)
extern void ObiActor_add_OnEndStep_mE226631AC3F2AA3BE52CCB4AB9972A0620E76208 (void);
// 0x0000009B System.Void Obi.ObiActor::remove_OnEndStep(Obi.ObiActor_ActorStepCallback)
extern void ObiActor_remove_OnEndStep_m18354446434FA0FE622CFA9F09B7D0D98B7365F9 (void);
// 0x0000009C System.Void Obi.ObiActor::add_OnInterpolate(Obi.ObiActor_ActorCallback)
extern void ObiActor_add_OnInterpolate_mA39BB43FF56C2FF95981971698C30856DC25ED65 (void);
// 0x0000009D System.Void Obi.ObiActor::remove_OnInterpolate(Obi.ObiActor_ActorCallback)
extern void ObiActor_remove_OnInterpolate_mE2B57508C1595198B4E27E10DA174A0B3792AD8D (void);
// 0x0000009E Obi.ObiSolver Obi.ObiActor::get_solver()
extern void ObiActor_get_solver_mDE668FD9EA16553E2252B51FC4AA0EE5F2476940 (void);
// 0x0000009F System.Boolean Obi.ObiActor::get_isLoaded()
extern void ObiActor_get_isLoaded_mE37E960D0EE5EF278241FE730E4E6D56FBBE217E (void);
// 0x000000A0 Obi.ObiCollisionMaterial Obi.ObiActor::get_collisionMaterial()
extern void ObiActor_get_collisionMaterial_mFEBA9610A92672D379939CCF56E5A07800EF7AB7 (void);
// 0x000000A1 System.Void Obi.ObiActor::set_collisionMaterial(Obi.ObiCollisionMaterial)
extern void ObiActor_set_collisionMaterial_m04C98A6B84C31070613652C4D12C3EFC0D98A0C4 (void);
// 0x000000A2 System.Boolean Obi.ObiActor::get_surfaceCollisions()
extern void ObiActor_get_surfaceCollisions_m280DE5273E14E28B1037C4CEA893F3BFFBEB2CB8 (void);
// 0x000000A3 System.Void Obi.ObiActor::set_surfaceCollisions(System.Boolean)
extern void ObiActor_set_surfaceCollisions_m40EE79424632CAE1EBA8FB1C3087D54FD9FC5046 (void);
// 0x000000A4 System.Int32 Obi.ObiActor::get_particleCount()
extern void ObiActor_get_particleCount_m8A511B9B8EE21FE09B45EA484ADE54CE1A469C24 (void);
// 0x000000A5 System.Int32 Obi.ObiActor::get_activeParticleCount()
extern void ObiActor_get_activeParticleCount_m330540C2CFF296103F759A43A156960A14EF40E3 (void);
// 0x000000A6 System.Boolean Obi.ObiActor::get_usesOrientedParticles()
extern void ObiActor_get_usesOrientedParticles_m1F5AC23A705419FFA18109ACE2CD45861FFCADDE (void);
// 0x000000A7 System.Boolean Obi.ObiActor::get_usesAnisotropicParticles()
extern void ObiActor_get_usesAnisotropicParticles_m54157FE27C8DBE561125BC87EA21BA034F246B69 (void);
// 0x000000A8 System.Boolean Obi.ObiActor::get_usesCustomExternalForces()
extern void ObiActor_get_usesCustomExternalForces_m63A0FBED9893C8176AA7DB6B5567978FB1D71DCA (void);
// 0x000000A9 UnityEngine.Matrix4x4 Obi.ObiActor::get_actorLocalToSolverMatrix()
extern void ObiActor_get_actorLocalToSolverMatrix_mCF3080C74BC381BF19F21BB6204A19DBDDFB82DE (void);
// 0x000000AA UnityEngine.Matrix4x4 Obi.ObiActor::get_actorSolverToLocalMatrix()
extern void ObiActor_get_actorSolverToLocalMatrix_m467C1F4C003B591FBE2C93BA9BDB33C0371962CE (void);
// 0x000000AB Obi.ObiActorBlueprint Obi.ObiActor::get_sourceBlueprint()
// 0x000000AC Obi.ObiActorBlueprint Obi.ObiActor::get_sharedBlueprint()
extern void ObiActor_get_sharedBlueprint_mE4DB059D49A26A1EF8D5AFD8CECEDB63CDACD863 (void);
// 0x000000AD Obi.ObiActorBlueprint Obi.ObiActor::get_blueprint()
extern void ObiActor_get_blueprint_m47A228582F43BE1A2AF98A4CFBF052C0DCF852F5 (void);
// 0x000000AE System.Void Obi.ObiActor::Awake()
extern void ObiActor_Awake_m241F3014CB9150A6234C8BBFE5F7F1506CC4A4A8 (void);
// 0x000000AF System.Void Obi.ObiActor::OnDestroy()
extern void ObiActor_OnDestroy_m2F3D4A01290E0E5EB2F3753AB9913AC96249A655 (void);
// 0x000000B0 System.Void Obi.ObiActor::OnEnable()
extern void ObiActor_OnEnable_m971216E2924B472ED6E1EF5CA46C20711919C967 (void);
// 0x000000B1 System.Void Obi.ObiActor::OnDisable()
extern void ObiActor_OnDisable_mCD8A0D978C2AD361EA37D62139DD083606463FD7 (void);
// 0x000000B2 System.Void Obi.ObiActor::OnValidate()
extern void ObiActor_OnValidate_m52B606D1428A6597252E13B0A7664A6A2A9AC196 (void);
// 0x000000B3 System.Void Obi.ObiActor::OnTransformParentChanged()
extern void ObiActor_OnTransformParentChanged_mE56EB6C5787A9B68CC2DCBF59A69FC5DBAC340B7 (void);
// 0x000000B4 System.Void Obi.ObiActor::AddToSolver()
extern void ObiActor_AddToSolver_m4612A91281D3B2EEA44B2654DF515E757FA4D3FF (void);
// 0x000000B5 System.Void Obi.ObiActor::RemoveFromSolver()
extern void ObiActor_RemoveFromSolver_m6E84D3DEBFE722DD4EC517EB2622FC80350D4FEA (void);
// 0x000000B6 System.Void Obi.ObiActor::SetSolver(Obi.ObiSolver)
extern void ObiActor_SetSolver_m6163A3076A049EAC233DCD2BB28B05C282DA55B6 (void);
// 0x000000B7 System.Void Obi.ObiActor::OnBlueprintRegenerate(Obi.ObiActorBlueprint)
extern void ObiActor_OnBlueprintRegenerate_mC7B6851CA42C438CCA449E6F6B675C88F4515C71 (void);
// 0x000000B8 System.Void Obi.ObiActor::UpdateCollisionMaterials()
extern void ObiActor_UpdateCollisionMaterials_m5E96C2AE2B9F7811085F85D0D25A5621C46D6817 (void);
// 0x000000B9 System.Boolean Obi.ObiActor::CopyParticle(System.Int32,System.Int32)
extern void ObiActor_CopyParticle_m8AA520BF621655A1D7FD60BE63769B097E1996D6 (void);
// 0x000000BA System.Void Obi.ObiActor::TeleportParticle(System.Int32,UnityEngine.Vector3)
extern void ObiActor_TeleportParticle_mD6150ED98C32F82A0ADB734DDE58A37BA0B78E39 (void);
// 0x000000BB System.Void Obi.ObiActor::Teleport(UnityEngine.Vector3,UnityEngine.Quaternion)
extern void ObiActor_Teleport_mE63A190E1F4170CF5E1BE19522E3B6061AE96A04 (void);
// 0x000000BC System.Void Obi.ObiActor::SwapWithFirstInactiveParticle(System.Int32)
extern void ObiActor_SwapWithFirstInactiveParticle_mC6593AA19F1C4AF00040F46D4EC723008B97B52B (void);
// 0x000000BD System.Boolean Obi.ObiActor::ActivateParticle(System.Int32)
extern void ObiActor_ActivateParticle_m45FF9EA8B2788D3C9EAE0EDE77DA4FA2133BF9BA (void);
// 0x000000BE System.Boolean Obi.ObiActor::DeactivateParticle(System.Int32)
extern void ObiActor_DeactivateParticle_m04DDF95A73A8AE09950848D4A8DBB2A57EC42C59 (void);
// 0x000000BF System.Boolean Obi.ObiActor::IsParticleActive(System.Int32)
extern void ObiActor_IsParticleActive_mFD14C2BCE7E86131404B2EEDA413555B7288C0E3 (void);
// 0x000000C0 System.Void Obi.ObiActor::SetSelfCollisions(System.Boolean)
extern void ObiActor_SetSelfCollisions_m45F0B0F5D1A4306D161C7EEC3C1435D9D52E3686 (void);
// 0x000000C1 System.Void Obi.ObiActor::SetOneSided(System.Boolean)
extern void ObiActor_SetOneSided_m99DA6DD46C17B2357C1815E76EA6FB70B6525D33 (void);
// 0x000000C2 System.Void Obi.ObiActor::SetSimplicesDirty()
extern void ObiActor_SetSimplicesDirty_m1FA63CB84FD2F2219BB54CB69CC5E8473FE2167A (void);
// 0x000000C3 System.Void Obi.ObiActor::SetConstraintsDirty(Oni_ConstraintType)
extern void ObiActor_SetConstraintsDirty_mF1D5DEE2405AE6F94AE264FC9CD0A63C31189D0F (void);
// 0x000000C4 Obi.IObiConstraints Obi.ObiActor::GetConstraintsByType(Oni_ConstraintType)
extern void ObiActor_GetConstraintsByType_mA485A63E65B2E5A863562BA515B15F6DE11138E7 (void);
// 0x000000C5 System.Void Obi.ObiActor::UpdateParticleProperties()
extern void ObiActor_UpdateParticleProperties_m9B2C616C0275DB62285F9E0CB0F1FF615D9FFAFF (void);
// 0x000000C6 System.Int32 Obi.ObiActor::GetParticleRuntimeIndex(System.Int32)
extern void ObiActor_GetParticleRuntimeIndex_m4AECF54626524359EE8CA930022DDFA864946B0D (void);
// 0x000000C7 UnityEngine.Vector3 Obi.ObiActor::GetParticlePosition(System.Int32)
extern void ObiActor_GetParticlePosition_m02493A97C489BA4E448F1B3BE3AAB59DB1E9039A (void);
// 0x000000C8 UnityEngine.Quaternion Obi.ObiActor::GetParticleOrientation(System.Int32)
extern void ObiActor_GetParticleOrientation_mC35CD07B08A81AC2159E02DF583118B7ACF8E9C3 (void);
// 0x000000C9 System.Void Obi.ObiActor::GetParticleAnisotropy(System.Int32,UnityEngine.Vector4&,UnityEngine.Vector4&,UnityEngine.Vector4&)
extern void ObiActor_GetParticleAnisotropy_mC8BAD8C2ACCEB1DF3284AEF13CC5E12ABEFF727A (void);
// 0x000000CA System.Single Obi.ObiActor::GetParticleMaxRadius(System.Int32)
extern void ObiActor_GetParticleMaxRadius_m834AF87A1D3204613AC0A6658EDBAF54871C13BD (void);
// 0x000000CB UnityEngine.Color Obi.ObiActor::GetParticleColor(System.Int32)
extern void ObiActor_GetParticleColor_m08A638A989F579EFADEA293B32C74635EECD25EA (void);
// 0x000000CC System.Void Obi.ObiActor::SetFilterCategory(System.Int32)
extern void ObiActor_SetFilterCategory_mAE60934D51EBD397225139EE05F2E80F08118D7D (void);
// 0x000000CD System.Void Obi.ObiActor::SetFilterMask(System.Int32)
extern void ObiActor_SetFilterMask_m7FED73FD99F34632090C9174E5EB278DC5D76F08 (void);
// 0x000000CE System.Void Obi.ObiActor::SetMass(System.Single)
extern void ObiActor_SetMass_m8D79A26F4FBEF1A895C6F482808A23A02B8BFAC4 (void);
// 0x000000CF System.Single Obi.ObiActor::GetMass(UnityEngine.Vector3&)
extern void ObiActor_GetMass_m2E9973F1EE3CBB300948F36489B7A7637B768F98 (void);
// 0x000000D0 System.Void Obi.ObiActor::AddForce(UnityEngine.Vector3,UnityEngine.ForceMode)
extern void ObiActor_AddForce_mE87CF6C0A21C03ED45209758E7BFE0E1AB2B1A00 (void);
// 0x000000D1 System.Void Obi.ObiActor::AddTorque(UnityEngine.Vector3,UnityEngine.ForceMode)
extern void ObiActor_AddTorque_m43A494B40EA87E0EC7A6274A0931F9CF27B5D77D (void);
// 0x000000D2 System.Void Obi.ObiActor::LoadBlueprintParticles(Obi.ObiActorBlueprint,System.Int32)
extern void ObiActor_LoadBlueprintParticles_m11D7669681BB9F35863E2BE6B9B50F9C7F8CFE88 (void);
// 0x000000D3 System.Void Obi.ObiActor::UnloadBlueprintParticles()
extern void ObiActor_UnloadBlueprintParticles_mAFE44CA41C0A763A6142420F2145AAEDCFD296A3 (void);
// 0x000000D4 System.Void Obi.ObiActor::ResetParticles()
extern void ObiActor_ResetParticles_m081DAAF528B4EF4F9E79AA17ECBEFC2C0BCB03D2 (void);
// 0x000000D5 System.Void Obi.ObiActor::SaveStateToBlueprint(Obi.ObiActorBlueprint)
extern void ObiActor_SaveStateToBlueprint_m86BB211764276B407A42BF4C603D75AEBD260F84 (void);
// 0x000000D6 System.Void Obi.ObiActor::StoreState()
extern void ObiActor_StoreState_mDF522A37AED6A366CD98C42E6D12EB524E5A6B67 (void);
// 0x000000D7 System.Void Obi.ObiActor::ClearState()
extern void ObiActor_ClearState_m2708E40E0CC1259F0A4A23A134D7C08A34D64D9E (void);
// 0x000000D8 System.Void Obi.ObiActor::LoadBlueprint(Obi.ObiSolver)
extern void ObiActor_LoadBlueprint_mD8204B2EFE860EADB85ABAAEA380D0EB7DE52966 (void);
// 0x000000D9 System.Void Obi.ObiActor::UnloadBlueprint(Obi.ObiSolver)
extern void ObiActor_UnloadBlueprint_m0380FCD5E41E56876B933D34F94EE112FE49D14E (void);
// 0x000000DA System.Void Obi.ObiActor::PrepareStep(System.Single)
extern void ObiActor_PrepareStep_mB06CED03B998A63A4AC43A5318D99D1AE8ABA400 (void);
// 0x000000DB System.Void Obi.ObiActor::BeginStep(System.Single)
extern void ObiActor_BeginStep_m2CB8338B9754937B0F151D9F06AF97263B2D313F (void);
// 0x000000DC System.Void Obi.ObiActor::Substep(System.Single)
extern void ObiActor_Substep_m784F85C0BD4464D6E4B1BCE24E44047956C78486 (void);
// 0x000000DD System.Void Obi.ObiActor::EndStep(System.Single)
extern void ObiActor_EndStep_m5076BADB8970EC68040EA131738946988F140513 (void);
// 0x000000DE System.Void Obi.ObiActor::Interpolate()
extern void ObiActor_Interpolate_m589CB41AB063846C5C621944F1B14EACB90C3683 (void);
// 0x000000DF System.Void Obi.ObiActor::OnSolverVisibilityChanged(System.Boolean)
extern void ObiActor_OnSolverVisibilityChanged_mA9B03F298B37CD3BB66DD5527F9E3CDB43A728EB (void);
// 0x000000E0 System.Void Obi.ObiActor::.ctor()
extern void ObiActor__ctor_mE992775E0773F687C886A5C462E2EAB7FA5D740E (void);
// 0x000000E1 Obi.ObiSolver Obi.ObiActor_ObiActorSolverArgs::get_solver()
extern void ObiActorSolverArgs_get_solver_m2C65EFA043F915BB25BF5EE5F467EA1119C55BDF (void);
// 0x000000E2 System.Void Obi.ObiActor_ObiActorSolverArgs::.ctor(Obi.ObiSolver)
extern void ObiActorSolverArgs__ctor_m645D2D61FF1A0780D4B70C2494C8C90607C7FA1B (void);
// 0x000000E3 System.Void Obi.ObiActor_ActorCallback::.ctor(System.Object,System.IntPtr)
extern void ActorCallback__ctor_mA6CF95A70DAB4FFF21823741E58A598FD0A3CC61 (void);
// 0x000000E4 System.Void Obi.ObiActor_ActorCallback::Invoke(Obi.ObiActor)
extern void ActorCallback_Invoke_m44385DCB93F7AC483E18B7477BDEB1C44993D508 (void);
// 0x000000E5 System.IAsyncResult Obi.ObiActor_ActorCallback::BeginInvoke(Obi.ObiActor,System.AsyncCallback,System.Object)
extern void ActorCallback_BeginInvoke_m511CEA8353354A1869B3548D98C3EA166C757E30 (void);
// 0x000000E6 System.Void Obi.ObiActor_ActorCallback::EndInvoke(System.IAsyncResult)
extern void ActorCallback_EndInvoke_m44778EF05F7C946EE7ACD0315B4F14F101980024 (void);
// 0x000000E7 System.Void Obi.ObiActor_ActorStepCallback::.ctor(System.Object,System.IntPtr)
extern void ActorStepCallback__ctor_m48119AF13D841B3D5476543A1C79108F94D53E00 (void);
// 0x000000E8 System.Void Obi.ObiActor_ActorStepCallback::Invoke(Obi.ObiActor,System.Single)
extern void ActorStepCallback_Invoke_m25120AB25E0465B7AF5976D51946EED8935E2A38 (void);
// 0x000000E9 System.IAsyncResult Obi.ObiActor_ActorStepCallback::BeginInvoke(Obi.ObiActor,System.Single,System.AsyncCallback,System.Object)
extern void ActorStepCallback_BeginInvoke_mE4197C4E139BFCC95434E2C3E20BE882EB5F2989 (void);
// 0x000000EA System.Void Obi.ObiActor_ActorStepCallback::EndInvoke(System.IAsyncResult)
extern void ActorStepCallback_EndInvoke_m8AA8AAD3C0E68BB1AF5F7E6A61EA2269F3A57AFB (void);
// 0x000000EB System.Void Obi.ObiActor_ActorBlueprintCallback::.ctor(System.Object,System.IntPtr)
extern void ActorBlueprintCallback__ctor_mADC829707A4F631EADEE895A25D2AAEA59A4D64D (void);
// 0x000000EC System.Void Obi.ObiActor_ActorBlueprintCallback::Invoke(Obi.ObiActor,Obi.ObiActorBlueprint)
extern void ActorBlueprintCallback_Invoke_m7BFF8526B4E3B31969016C044E63EA9AF3FFE7AD (void);
// 0x000000ED System.IAsyncResult Obi.ObiActor_ActorBlueprintCallback::BeginInvoke(Obi.ObiActor,Obi.ObiActorBlueprint,System.AsyncCallback,System.Object)
extern void ActorBlueprintCallback_BeginInvoke_m32D286D241BEC1F0C4FD9CF6C04C5E7BF1823485 (void);
// 0x000000EE System.Void Obi.ObiActor_ActorBlueprintCallback::EndInvoke(System.IAsyncResult)
extern void ActorBlueprintCallback_EndInvoke_m7B651D23DA455A93DCC87B95CCE21A5789433013 (void);
// 0x000000EF System.Int32 Obi.IColliderWorldImpl::get_referenceCount()
// 0x000000F0 System.Void Obi.IColliderWorldImpl::UpdateWorld(System.Single)
// 0x000000F1 System.Void Obi.IColliderWorldImpl::SetColliders(Obi.ObiNativeColliderShapeList,Obi.ObiNativeAabbList,Obi.ObiNativeAffineTransformList,System.Int32)
// 0x000000F2 System.Void Obi.IColliderWorldImpl::SetRigidbodies(Obi.ObiNativeRigidbodyList)
// 0x000000F3 System.Void Obi.IColliderWorldImpl::SetCollisionMaterials(Obi.ObiNativeCollisionMaterialList)
// 0x000000F4 System.Void Obi.IColliderWorldImpl::SetTriangleMeshData(Obi.ObiNativeTriangleMeshHeaderList,Obi.ObiNativeBIHNodeList,Obi.ObiNativeTriangleList,Obi.ObiNativeVector3List)
// 0x000000F5 System.Void Obi.IColliderWorldImpl::SetEdgeMeshData(Obi.ObiNativeEdgeMeshHeaderList,Obi.ObiNativeBIHNodeList,Obi.ObiNativeEdgeList,Obi.ObiNativeVector2List)
// 0x000000F6 System.Void Obi.IColliderWorldImpl::SetDistanceFieldData(Obi.ObiNativeDistanceFieldHeaderList,Obi.ObiNativeDFNodeList)
// 0x000000F7 System.Void Obi.IColliderWorldImpl::SetHeightFieldData(Obi.ObiNativeHeightFieldHeaderList,Obi.ObiNativeFloatList)
// 0x000000F8 System.Void Obi.IAerodynamicConstraintsBatchImpl::SetAerodynamicConstraints(Obi.ObiNativeIntList,Obi.ObiNativeFloatList,System.Int32)
// 0x000000F9 System.Void Obi.IBendConstraintsBatchImpl::SetBendConstraints(Obi.ObiNativeIntList,Obi.ObiNativeFloatList,Obi.ObiNativeVector2List,Obi.ObiNativeVector2List,Obi.ObiNativeFloatList,System.Int32)
// 0x000000FA System.Void Obi.IBendTwistConstraintsBatchImpl::SetBendTwistConstraints(Obi.ObiNativeIntList,Obi.ObiNativeQuaternionList,Obi.ObiNativeVector3List,Obi.ObiNativeVector2List,Obi.ObiNativeFloatList,System.Int32)
// 0x000000FB System.Void Obi.IChainConstraintsBatchImpl::SetChainConstraints(Obi.ObiNativeIntList,Obi.ObiNativeVector2List,Obi.ObiNativeIntList,Obi.ObiNativeIntList,System.Int32)
// 0x000000FC Oni_ConstraintType Obi.IConstraintsBatchImpl::get_constraintType()
// 0x000000FD Obi.IConstraints Obi.IConstraintsBatchImpl::get_constraints()
// 0x000000FE System.Void Obi.IConstraintsBatchImpl::set_enabled(System.Boolean)
// 0x000000FF System.Boolean Obi.IConstraintsBatchImpl::get_enabled()
// 0x00000100 System.Void Obi.IConstraintsBatchImpl::Destroy()
// 0x00000101 System.Void Obi.IConstraintsBatchImpl::SetConstraintCount(System.Int32)
// 0x00000102 System.Int32 Obi.IConstraintsBatchImpl::GetConstraintCount()
// 0x00000103 Oni_ConstraintType Obi.IConstraints::get_constraintType()
// 0x00000104 Obi.ISolverImpl Obi.IConstraints::get_solver()
// 0x00000105 System.Int32 Obi.IConstraints::GetConstraintCount()
// 0x00000106 System.Void Obi.IDistanceConstraintsBatchImpl::SetDistanceConstraints(Obi.ObiNativeIntList,Obi.ObiNativeFloatList,Obi.ObiNativeVector2List,Obi.ObiNativeFloatList,System.Int32)
// 0x00000107 System.Void Obi.IPinConstraintsBatchImpl::SetPinConstraints(Obi.ObiNativeIntList,Obi.ObiNativeIntList,Obi.ObiNativeVector4List,Obi.ObiNativeQuaternionList,Obi.ObiNativeFloatList,Obi.ObiNativeFloatList,System.Int32)
// 0x00000108 System.Void Obi.IShapeMatchingConstraintsBatchImpl::SetShapeMatchingConstraints(Obi.ObiNativeIntList,Obi.ObiNativeIntList,Obi.ObiNativeIntList,Obi.ObiNativeIntList,Obi.ObiNativeFloatList,Obi.ObiNativeVector4List,Obi.ObiNativeVector4List,Obi.ObiNativeQuaternionList,Obi.ObiNativeMatrix4x4List,Obi.ObiNativeMatrix4x4List,Obi.ObiNativeFloatList,System.Int32)
// 0x00000109 System.Void Obi.IShapeMatchingConstraintsBatchImpl::CalculateRestShapeMatching()
// 0x0000010A System.Void Obi.ISkinConstraintsBatchImpl::SetSkinConstraints(Obi.ObiNativeIntList,Obi.ObiNativeVector4List,Obi.ObiNativeVector4List,Obi.ObiNativeFloatList,Obi.ObiNativeFloatList,Obi.ObiNativeFloatList,System.Int32)
// 0x0000010B System.Void Obi.IStitchConstraintsBatchImpl::SetStitchConstraints(Obi.ObiNativeIntList,Obi.ObiNativeFloatList,Obi.ObiNativeFloatList,System.Int32)
// 0x0000010C System.Void Obi.IStretchShearConstraintsBatchImpl::SetStretchShearConstraints(Obi.ObiNativeIntList,Obi.ObiNativeIntList,Obi.ObiNativeFloatList,Obi.ObiNativeQuaternionList,Obi.ObiNativeVector3List,Obi.ObiNativeFloatList,System.Int32)
// 0x0000010D System.Void Obi.ITetherConstraintsBatchImpl::SetTetherConstraints(Obi.ObiNativeIntList,Obi.ObiNativeVector2List,Obi.ObiNativeFloatList,Obi.ObiNativeFloatList,System.Int32)
// 0x0000010E System.Void Obi.IVolumeConstraintsBatchImpl::SetVolumeConstraints(Obi.ObiNativeIntList,Obi.ObiNativeIntList,Obi.ObiNativeIntList,Obi.ObiNativeFloatList,Obi.ObiNativeVector2List,Obi.ObiNativeFloatList,System.Int32)
// 0x0000010F Obi.ISolverImpl Obi.IObiBackend::CreateSolver(Obi.ObiSolver,System.Int32)
// 0x00000110 System.Void Obi.IObiBackend::DestroySolver(Obi.ISolverImpl)
// 0x00000111 System.Void Obi.IObiJobHandle::Complete()
// 0x00000112 System.Void Obi.ISolverImpl::Destroy()
// 0x00000113 System.Void Obi.ISolverImpl::InitializeFrame(UnityEngine.Vector4,UnityEngine.Vector4,UnityEngine.Quaternion)
// 0x00000114 System.Void Obi.ISolverImpl::UpdateFrame(UnityEngine.Vector4,UnityEngine.Vector4,UnityEngine.Quaternion,System.Single)
// 0x00000115 System.Void Obi.ISolverImpl::ApplyFrame(System.Single,System.Single,System.Single)
// 0x00000116 System.Void Obi.ISolverImpl::ParticleCountChanged(Obi.ObiSolver)
// 0x00000117 System.Void Obi.ISolverImpl::SetActiveParticles(System.Int32[],System.Int32)
// 0x00000118 System.Void Obi.ISolverImpl::InterpolateDiffuseProperties(Obi.ObiNativeVector4List,Obi.ObiNativeVector4List,Obi.ObiNativeVector4List,Obi.ObiNativeIntList,System.Int32)
// 0x00000119 System.Void Obi.ISolverImpl::SetRigidbodyArrays(Obi.ObiSolver)
// 0x0000011A Obi.IConstraintsBatchImpl Obi.ISolverImpl::CreateConstraintsBatch(Oni_ConstraintType)
// 0x0000011B System.Void Obi.ISolverImpl::DestroyConstraintsBatch(Obi.IConstraintsBatchImpl)
// 0x0000011C System.Int32 Obi.ISolverImpl::GetConstraintCount(Oni_ConstraintType)
// 0x0000011D System.Void Obi.ISolverImpl::GetCollisionContacts(Oni_Contact[],System.Int32)
// 0x0000011E System.Void Obi.ISolverImpl::GetParticleCollisionContacts(Oni_Contact[],System.Int32)
// 0x0000011F System.Void Obi.ISolverImpl::SetConstraintGroupParameters(Oni_ConstraintType,Oni_ConstraintParameters&)
// 0x00000120 Obi.IObiJobHandle Obi.ISolverImpl::CollisionDetection(System.Single)
// 0x00000121 Obi.IObiJobHandle Obi.ISolverImpl::Substep(System.Single,System.Single,System.Int32)
// 0x00000122 System.Void Obi.ISolverImpl::ApplyInterpolation(Obi.ObiNativeVector4List,Obi.ObiNativeQuaternionList,System.Single,System.Single)
// 0x00000123 System.Int32 Obi.ISolverImpl::GetDeformableTriangleCount()
// 0x00000124 System.Void Obi.ISolverImpl::SetDeformableTriangles(System.Int32[],System.Int32,System.Int32)
// 0x00000125 System.Int32 Obi.ISolverImpl::RemoveDeformableTriangles(System.Int32,System.Int32)
// 0x00000126 System.Void Obi.ISolverImpl::SetSimplices(System.Int32[],Obi.SimplexCounts)
// 0x00000127 System.Void Obi.ISolverImpl::SetParameters(Oni_SolverParameters)
// 0x00000128 System.Void Obi.ISolverImpl::GetBounds(UnityEngine.Vector3&,UnityEngine.Vector3&)
// 0x00000129 System.Void Obi.ISolverImpl::ResetForces()
// 0x0000012A System.Int32 Obi.ISolverImpl::GetParticleGridSize()
// 0x0000012B System.Void Obi.ISolverImpl::GetParticleGrid(Obi.ObiNativeAabbList)
// 0x0000012C System.Void Obi.ISolverImpl::SpatialQuery(Obi.ObiNativeQueryShapeList,Obi.ObiNativeAffineTransformList,Obi.ObiNativeQueryResultList)
// 0x0000012D Obi.ISolverImpl Obi.NullBackend::CreateSolver(Obi.ObiSolver,System.Int32)
extern void NullBackend_CreateSolver_m149A74012C4CF9DEC9EC68F1C16EE99F3D6E88AF (void);
// 0x0000012E System.Void Obi.NullBackend::DestroySolver(Obi.ISolverImpl)
extern void NullBackend_DestroySolver_m5E51947F4BEC3D57534FD796732535ED2B83D3F6 (void);
// 0x0000012F System.Void Obi.NullBackend::.ctor()
extern void NullBackend__ctor_m7C4DE499807A5119E6914F1A7B6666E7BBFE44A9 (void);
// 0x00000130 System.Void Obi.NullSolverImpl::Destroy()
extern void NullSolverImpl_Destroy_mCEF74F887CB2BF0E13C8176A052B1539F51BD6AB (void);
// 0x00000131 System.Void Obi.NullSolverImpl::InitializeFrame(UnityEngine.Vector4,UnityEngine.Vector4,UnityEngine.Quaternion)
extern void NullSolverImpl_InitializeFrame_m1A9943BBB1200D17394A300365060CC1C180C333 (void);
// 0x00000132 System.Void Obi.NullSolverImpl::UpdateFrame(UnityEngine.Vector4,UnityEngine.Vector4,UnityEngine.Quaternion,System.Single)
extern void NullSolverImpl_UpdateFrame_mE9842C5421EB8BCEF09BEF66B7ECE4C6BBF68453 (void);
// 0x00000133 System.Void Obi.NullSolverImpl::ApplyFrame(System.Single,System.Single,System.Single)
extern void NullSolverImpl_ApplyFrame_m6E1688FD679EF4B17A6C885CD116D47885BA9A6D (void);
// 0x00000134 System.Int32 Obi.NullSolverImpl::GetDeformableTriangleCount()
extern void NullSolverImpl_GetDeformableTriangleCount_m5B5658F9B5D43BC6CF7131229FE97D16E931E5AB (void);
// 0x00000135 System.Void Obi.NullSolverImpl::SetDeformableTriangles(System.Int32[],System.Int32,System.Int32)
extern void NullSolverImpl_SetDeformableTriangles_m3555C196B17530814FC47EB0D65B0B19428AE73E (void);
// 0x00000136 System.Int32 Obi.NullSolverImpl::RemoveDeformableTriangles(System.Int32,System.Int32)
extern void NullSolverImpl_RemoveDeformableTriangles_m327A23C82D523DB2799DC060C88746468C9CB012 (void);
// 0x00000137 System.Void Obi.NullSolverImpl::SetSimplices(System.Int32[],Obi.SimplexCounts)
extern void NullSolverImpl_SetSimplices_m31A164287A45DD5E59CD0C787A24C85DAB4C3D40 (void);
// 0x00000138 System.Void Obi.NullSolverImpl::ParticleCountChanged(Obi.ObiSolver)
extern void NullSolverImpl_ParticleCountChanged_m8557EBD352BB202123699487E00B843F3CA0DC0B (void);
// 0x00000139 System.Void Obi.NullSolverImpl::SetRigidbodyArrays(Obi.ObiSolver)
extern void NullSolverImpl_SetRigidbodyArrays_mEC64908F48D6E3EE3F03975F21201DAD33E36848 (void);
// 0x0000013A System.Void Obi.NullSolverImpl::SetActiveParticles(System.Int32[],System.Int32)
extern void NullSolverImpl_SetActiveParticles_m270CC166EEFCBB7494E52DFD1F7385B091AFB05B (void);
// 0x0000013B System.Void Obi.NullSolverImpl::ResetForces()
extern void NullSolverImpl_ResetForces_m02F4803EE6AB50D5563B60A00E5535B12F8E3B11 (void);
// 0x0000013C System.Void Obi.NullSolverImpl::GetBounds(UnityEngine.Vector3&,UnityEngine.Vector3&)
extern void NullSolverImpl_GetBounds_m4D24F01134C67A078F93680525FB8DC5804A3DBB (void);
// 0x0000013D System.Void Obi.NullSolverImpl::SetParameters(Oni_SolverParameters)
extern void NullSolverImpl_SetParameters_mDCC55383F8800DB49278298ABF8A176E6F5B3CAA (void);
// 0x0000013E System.Int32 Obi.NullSolverImpl::GetConstraintCount(Oni_ConstraintType)
extern void NullSolverImpl_GetConstraintCount_mA5540FE96215F61ADEFE829931F4D927E70019E0 (void);
// 0x0000013F System.Void Obi.NullSolverImpl::GetCollisionContacts(Oni_Contact[],System.Int32)
extern void NullSolverImpl_GetCollisionContacts_mD2C3B675F93A95F33D2472E11AD4F48C503B2B54 (void);
// 0x00000140 System.Void Obi.NullSolverImpl::GetParticleCollisionContacts(Oni_Contact[],System.Int32)
extern void NullSolverImpl_GetParticleCollisionContacts_m55885C928BC24A887BD63F783CD979F085E07381 (void);
// 0x00000141 System.Void Obi.NullSolverImpl::SetConstraintGroupParameters(Oni_ConstraintType,Oni_ConstraintParameters&)
extern void NullSolverImpl_SetConstraintGroupParameters_m3A8213C3F85B7A2187B6030CC6EDC4DE5CB505BE (void);
// 0x00000142 Obi.IConstraintsBatchImpl Obi.NullSolverImpl::CreateConstraintsBatch(Oni_ConstraintType)
extern void NullSolverImpl_CreateConstraintsBatch_mB98D773275819E868529278DC289D6E084CC913D (void);
// 0x00000143 System.Void Obi.NullSolverImpl::DestroyConstraintsBatch(Obi.IConstraintsBatchImpl)
extern void NullSolverImpl_DestroyConstraintsBatch_m42D46BA79047F3A0DBD2B305549E97FA654EF2B7 (void);
// 0x00000144 Obi.IObiJobHandle Obi.NullSolverImpl::CollisionDetection(System.Single)
extern void NullSolverImpl_CollisionDetection_m3BE61763707C061EB9912C7E9DCD44C3F135C0FB (void);
// 0x00000145 Obi.IObiJobHandle Obi.NullSolverImpl::Substep(System.Single,System.Single,System.Int32)
extern void NullSolverImpl_Substep_m46BCCC5D64A69DD461D4EF42A3D227A2F53A6A08 (void);
// 0x00000146 System.Void Obi.NullSolverImpl::ApplyInterpolation(Obi.ObiNativeVector4List,Obi.ObiNativeQuaternionList,System.Single,System.Single)
extern void NullSolverImpl_ApplyInterpolation_mB63837B2AE0542F306D50ABF7A05868F2A9A75BC (void);
// 0x00000147 System.Void Obi.NullSolverImpl::InterpolateDiffuseProperties(Obi.ObiNativeVector4List,Obi.ObiNativeVector4List,Obi.ObiNativeVector4List,Obi.ObiNativeIntList,System.Int32)
extern void NullSolverImpl_InterpolateDiffuseProperties_m5D4052E47F4C0CE896A55E4932A7AB35F0001133 (void);
// 0x00000148 System.Int32 Obi.NullSolverImpl::GetParticleGridSize()
extern void NullSolverImpl_GetParticleGridSize_mD74EA2D59BC1B4472754D76EFF3B4FBC969B2A66 (void);
// 0x00000149 System.Void Obi.NullSolverImpl::GetParticleGrid(Obi.ObiNativeAabbList)
extern void NullSolverImpl_GetParticleGrid_mCBCC36302C00CB01775017E21F63754021E04BEC (void);
// 0x0000014A System.Void Obi.NullSolverImpl::SpatialQuery(Obi.ObiNativeQueryShapeList,Obi.ObiNativeAffineTransformList,Obi.ObiNativeQueryResultList)
extern void NullSolverImpl_SpatialQuery_m252FFC078850F330BB8276D6996CD2303F90D6F1 (void);
// 0x0000014B System.Void Obi.NullSolverImpl::.ctor()
extern void NullSolverImpl__ctor_m835D73EC52194B7861F10BCEC2C8F3D30E98D6EF (void);
// 0x0000014C System.Void Obi.OniAerodynamicConstraintsBatchImpl::.ctor(Obi.OniAerodynamicConstraintsImpl)
extern void OniAerodynamicConstraintsBatchImpl__ctor_m08CF6D0353900E2F5161D01B30D6D4FD181EAF67 (void);
// 0x0000014D System.Void Obi.OniAerodynamicConstraintsBatchImpl::SetAerodynamicConstraints(Obi.ObiNativeIntList,Obi.ObiNativeFloatList,System.Int32)
extern void OniAerodynamicConstraintsBatchImpl_SetAerodynamicConstraints_m78BC6505E02E608B6E7E64C31C785C0653DCCDD5 (void);
// 0x0000014E System.Void Obi.OniAerodynamicConstraintsImpl::.ctor(Obi.OniSolverImpl)
extern void OniAerodynamicConstraintsImpl__ctor_m525B8236B34A2BEB8A129BC2DFCAD6433E10673D (void);
// 0x0000014F Obi.IConstraintsBatchImpl Obi.OniAerodynamicConstraintsImpl::CreateConstraintsBatch()
extern void OniAerodynamicConstraintsImpl_CreateConstraintsBatch_m3C15118FE42A98BE00C420EFF5CC108133E8C9D1 (void);
// 0x00000150 System.Void Obi.OniAerodynamicConstraintsImpl::RemoveBatch(Obi.IConstraintsBatchImpl)
extern void OniAerodynamicConstraintsImpl_RemoveBatch_m7312BFD8301F75F44EF449DAB1CA208CDF256820 (void);
// 0x00000151 System.Void Obi.OniBendConstraintsBatchImpl::.ctor(Obi.OniBendConstraintsImpl)
extern void OniBendConstraintsBatchImpl__ctor_mD888A9E984F69E1F75358F7C87F224E5BBFEF9C1 (void);
// 0x00000152 System.Void Obi.OniBendConstraintsBatchImpl::SetBendConstraints(Obi.ObiNativeIntList,Obi.ObiNativeFloatList,Obi.ObiNativeVector2List,Obi.ObiNativeVector2List,Obi.ObiNativeFloatList,System.Int32)
extern void OniBendConstraintsBatchImpl_SetBendConstraints_mACD9168310968682EC7F9FAF4FAF01D439561251 (void);
// 0x00000153 System.Void Obi.OniBendConstraintsImpl::.ctor(Obi.OniSolverImpl)
extern void OniBendConstraintsImpl__ctor_mDF7E828956758CC442DFCE530D6ACB3DA28C7520 (void);
// 0x00000154 Obi.IConstraintsBatchImpl Obi.OniBendConstraintsImpl::CreateConstraintsBatch()
extern void OniBendConstraintsImpl_CreateConstraintsBatch_mE943BA310CF0508FA4B457AE9C4CEDE436C0256C (void);
// 0x00000155 System.Void Obi.OniBendConstraintsImpl::RemoveBatch(Obi.IConstraintsBatchImpl)
extern void OniBendConstraintsImpl_RemoveBatch_mE65A138E34D866312EA8D6811377B0D2A5A8F759 (void);
// 0x00000156 System.Void Obi.OniBendTwistConstraintsBatchImpl::.ctor(Obi.OniBendTwistConstraintsImpl)
extern void OniBendTwistConstraintsBatchImpl__ctor_m8F131CD09F6F7F09BBDA55AF3E953E5EA2D1F52F (void);
// 0x00000157 System.Void Obi.OniBendTwistConstraintsBatchImpl::SetBendTwistConstraints(Obi.ObiNativeIntList,Obi.ObiNativeQuaternionList,Obi.ObiNativeVector3List,Obi.ObiNativeVector2List,Obi.ObiNativeFloatList,System.Int32)
extern void OniBendTwistConstraintsBatchImpl_SetBendTwistConstraints_m93208190B4C9F21B66804826F3F388365464C7E6 (void);
// 0x00000158 System.Void Obi.OniBendTwistConstraintsImpl::.ctor(Obi.OniSolverImpl)
extern void OniBendTwistConstraintsImpl__ctor_m2D810DA4120C0AB84298B4A542CA5B932E774DCB (void);
// 0x00000159 Obi.IConstraintsBatchImpl Obi.OniBendTwistConstraintsImpl::CreateConstraintsBatch()
extern void OniBendTwistConstraintsImpl_CreateConstraintsBatch_m5F1FC4850DB914A408732085F9AAEB1FE7D9DEF9 (void);
// 0x0000015A System.Void Obi.OniBendTwistConstraintsImpl::RemoveBatch(Obi.IConstraintsBatchImpl)
extern void OniBendTwistConstraintsImpl_RemoveBatch_m571856C1E93A0B2507FD9309A2984CAD3143BB55 (void);
// 0x0000015B System.Void Obi.OniChainConstraintsBatchImpl::.ctor(Obi.OniChainConstraintsImpl)
extern void OniChainConstraintsBatchImpl__ctor_m11A76775D8DA7D09B7DACBF1D1168D4FEF4F1694 (void);
// 0x0000015C System.Void Obi.OniChainConstraintsBatchImpl::SetChainConstraints(Obi.ObiNativeIntList,Obi.ObiNativeVector2List,Obi.ObiNativeIntList,Obi.ObiNativeIntList,System.Int32)
extern void OniChainConstraintsBatchImpl_SetChainConstraints_mA704113F26DE40220400DE5B85B1475C2B3D0A82 (void);
// 0x0000015D System.Void Obi.OniChainConstraintsImpl::.ctor(Obi.OniSolverImpl)
extern void OniChainConstraintsImpl__ctor_mAE3D234A2597D468A4621529AECFC4CD2D1BB328 (void);
// 0x0000015E Obi.IConstraintsBatchImpl Obi.OniChainConstraintsImpl::CreateConstraintsBatch()
extern void OniChainConstraintsImpl_CreateConstraintsBatch_m84FCCD1FC3FF8E6DE2FF68D3B259A0AF6E595B9D (void);
// 0x0000015F System.Void Obi.OniChainConstraintsImpl::RemoveBatch(Obi.IConstraintsBatchImpl)
extern void OniChainConstraintsImpl_RemoveBatch_mD1FF35D0BD359EB12FC8A74DE8A560E487A9057B (void);
// 0x00000160 System.Void Obi.OniDistanceConstraintsBatchImpl::.ctor(Obi.OniDistanceConstraintsImpl)
extern void OniDistanceConstraintsBatchImpl__ctor_m9005CD0907A9E6BACBBDA3777B6717B8611AFA18 (void);
// 0x00000161 System.Void Obi.OniDistanceConstraintsBatchImpl::SetDistanceConstraints(Obi.ObiNativeIntList,Obi.ObiNativeFloatList,Obi.ObiNativeVector2List,Obi.ObiNativeFloatList,System.Int32)
extern void OniDistanceConstraintsBatchImpl_SetDistanceConstraints_m2305BC0CB077A2F81D031356D983305E8344B34B (void);
// 0x00000162 System.Void Obi.OniDistanceConstraintsImpl::.ctor(Obi.OniSolverImpl)
extern void OniDistanceConstraintsImpl__ctor_m46EF17FABD8A1BFB1EDE24EDE0F6EB61473907F8 (void);
// 0x00000163 Obi.IConstraintsBatchImpl Obi.OniDistanceConstraintsImpl::CreateConstraintsBatch()
extern void OniDistanceConstraintsImpl_CreateConstraintsBatch_m68ACFFA63F7B3ACF12200ACCDD9796BC1E00453D (void);
// 0x00000164 System.Void Obi.OniDistanceConstraintsImpl::RemoveBatch(Obi.IConstraintsBatchImpl)
extern void OniDistanceConstraintsImpl_RemoveBatch_m4F114348DEFF33909A2AFCF15D10D98187F7912C (void);
// 0x00000165 System.IntPtr Obi.OniConstraintsBatchImpl::get_oniBatch()
extern void OniConstraintsBatchImpl_get_oniBatch_mD3068ADFBFA72D2C5AEC1E4493AF44A61BAAB3D6 (void);
// 0x00000166 Oni_ConstraintType Obi.OniConstraintsBatchImpl::get_constraintType()
extern void OniConstraintsBatchImpl_get_constraintType_mA4D89C531D6E50262AAB808ED481F01FCBC4554F (void);
// 0x00000167 Obi.IConstraints Obi.OniConstraintsBatchImpl::get_constraints()
extern void OniConstraintsBatchImpl_get_constraints_mC86F66AEBA1CCFE2EE1484C2CA0844257A7A0CCD (void);
// 0x00000168 System.Void Obi.OniConstraintsBatchImpl::set_enabled(System.Boolean)
extern void OniConstraintsBatchImpl_set_enabled_mE3FE79E17C8786437000CF9DF7ED7CC24C58DD1F (void);
// 0x00000169 System.Boolean Obi.OniConstraintsBatchImpl::get_enabled()
extern void OniConstraintsBatchImpl_get_enabled_m74C3931C9C466399F511D1258F6DF49FAF007D8F (void);
// 0x0000016A System.Void Obi.OniConstraintsBatchImpl::.ctor(Obi.IConstraints,Oni_ConstraintType)
extern void OniConstraintsBatchImpl__ctor_m7E856D436221DA306E7EFF8409F799DC8F926C38 (void);
// 0x0000016B System.Void Obi.OniConstraintsBatchImpl::Destroy()
extern void OniConstraintsBatchImpl_Destroy_mB934F5194DA3A35DAC45581D183BE8C1BB056A1A (void);
// 0x0000016C System.Void Obi.OniConstraintsBatchImpl::SetConstraintCount(System.Int32)
extern void OniConstraintsBatchImpl_SetConstraintCount_m9A9C7B081DBCAFE727F2682F742B32A0A5CD1FC7 (void);
// 0x0000016D System.Int32 Obi.OniConstraintsBatchImpl::GetConstraintCount()
extern void OniConstraintsBatchImpl_GetConstraintCount_mBC3CD543643EBE713BEDBCEC5266DE52F0E6E53E (void);
// 0x0000016E Obi.IConstraintsBatchImpl Obi.IOniConstraintsImpl::CreateConstraintsBatch()
// 0x0000016F System.Void Obi.IOniConstraintsImpl::RemoveBatch(Obi.IConstraintsBatchImpl)
// 0x00000170 Obi.ISolverImpl Obi.OniConstraintsImpl::get_solver()
extern void OniConstraintsImpl_get_solver_m44585FB563C805AF280120F7FDE3B5737F4C0411 (void);
// 0x00000171 Oni_ConstraintType Obi.OniConstraintsImpl::get_constraintType()
extern void OniConstraintsImpl_get_constraintType_mF0F59F28976DF29B32C94C4156BD7752105087CC (void);
// 0x00000172 System.Void Obi.OniConstraintsImpl::.ctor(Obi.OniSolverImpl,Oni_ConstraintType)
extern void OniConstraintsImpl__ctor_m9A0E8C6A195C96B8CB7EF27D99B7A0988672BF61 (void);
// 0x00000173 Obi.IConstraintsBatchImpl Obi.OniConstraintsImpl::CreateConstraintsBatch()
// 0x00000174 System.Void Obi.OniConstraintsImpl::RemoveBatch(Obi.IConstraintsBatchImpl)
// 0x00000175 System.Int32 Obi.OniConstraintsImpl::GetConstraintCount()
extern void OniConstraintsImpl_GetConstraintCount_mDE65EE94DCFAB8905A9AEF5543E7119928F2900A (void);
// 0x00000176 System.Void Obi.OniPinConstraintsBatchImpl::.ctor(Obi.OniPinConstraintsImpl)
extern void OniPinConstraintsBatchImpl__ctor_mA95003D5E91633FA72AA1F912FFCC947CBFC25CF (void);
// 0x00000177 System.Void Obi.OniPinConstraintsBatchImpl::SetPinConstraints(Obi.ObiNativeIntList,Obi.ObiNativeIntList,Obi.ObiNativeVector4List,Obi.ObiNativeQuaternionList,Obi.ObiNativeFloatList,Obi.ObiNativeFloatList,System.Int32)
extern void OniPinConstraintsBatchImpl_SetPinConstraints_mB28FD52B804E30D4B06AADE5A8764C9F3F4DC223 (void);
// 0x00000178 System.Void Obi.OniPinConstraintsImpl::.ctor(Obi.OniSolverImpl)
extern void OniPinConstraintsImpl__ctor_m2F0E0AF0C30CE5AA9BBA5B0F2C170582A8442ABE (void);
// 0x00000179 Obi.IConstraintsBatchImpl Obi.OniPinConstraintsImpl::CreateConstraintsBatch()
extern void OniPinConstraintsImpl_CreateConstraintsBatch_mF0305992D09B680091BD1EEF5519A801F19F69D0 (void);
// 0x0000017A System.Void Obi.OniPinConstraintsImpl::RemoveBatch(Obi.IConstraintsBatchImpl)
extern void OniPinConstraintsImpl_RemoveBatch_m1EA7BE417BEF16951FAD0CA1106DB5BC257F38E1 (void);
// 0x0000017B System.Void Obi.OniShapeMatchingConstraintsBatchImpl::.ctor(Obi.OniShapeMatchingConstraintsImpl)
extern void OniShapeMatchingConstraintsBatchImpl__ctor_mA35A5ADBE7591DFAFCD6775D89CD7C7BBEB7E25D (void);
// 0x0000017C System.Void Obi.OniShapeMatchingConstraintsBatchImpl::SetShapeMatchingConstraints(Obi.ObiNativeIntList,Obi.ObiNativeIntList,Obi.ObiNativeIntList,Obi.ObiNativeIntList,Obi.ObiNativeFloatList,Obi.ObiNativeVector4List,Obi.ObiNativeVector4List,Obi.ObiNativeQuaternionList,Obi.ObiNativeMatrix4x4List,Obi.ObiNativeMatrix4x4List,Obi.ObiNativeFloatList,System.Int32)
extern void OniShapeMatchingConstraintsBatchImpl_SetShapeMatchingConstraints_mDDBCB5CFAB68DDF0E19581524119A7981D214588 (void);
// 0x0000017D System.Void Obi.OniShapeMatchingConstraintsBatchImpl::CalculateRestShapeMatching()
extern void OniShapeMatchingConstraintsBatchImpl_CalculateRestShapeMatching_m6E5073835BB3D1CED559A45D987CC3FCD9D4C21D (void);
// 0x0000017E System.Void Obi.OniShapeMatchingConstraintsImpl::.ctor(Obi.OniSolverImpl)
extern void OniShapeMatchingConstraintsImpl__ctor_m945AA881838FE9644266E98CA531247FE004C1D7 (void);
// 0x0000017F Obi.IConstraintsBatchImpl Obi.OniShapeMatchingConstraintsImpl::CreateConstraintsBatch()
extern void OniShapeMatchingConstraintsImpl_CreateConstraintsBatch_mB055C2DDEB21A68DC4E3A56E2A989231D1109BBD (void);
// 0x00000180 System.Void Obi.OniShapeMatchingConstraintsImpl::RemoveBatch(Obi.IConstraintsBatchImpl)
extern void OniShapeMatchingConstraintsImpl_RemoveBatch_m0C9FC7FFB2680541A78037980C49C907F3598756 (void);
// 0x00000181 System.Void Obi.OniSkinConstraintsBatchImpl::.ctor(Obi.OniSkinConstraintsImpl)
extern void OniSkinConstraintsBatchImpl__ctor_m5DDA5D3F347C6EC9FF501C52A4374AD88A10AABD (void);
// 0x00000182 System.Void Obi.OniSkinConstraintsBatchImpl::SetSkinConstraints(Obi.ObiNativeIntList,Obi.ObiNativeVector4List,Obi.ObiNativeVector4List,Obi.ObiNativeFloatList,Obi.ObiNativeFloatList,Obi.ObiNativeFloatList,System.Int32)
extern void OniSkinConstraintsBatchImpl_SetSkinConstraints_m8065C2605A9D036F95C2F1B234883144F9C84CD3 (void);
// 0x00000183 System.Void Obi.OniSkinConstraintsImpl::.ctor(Obi.OniSolverImpl)
extern void OniSkinConstraintsImpl__ctor_m9A29E8FD8F9F99978DFF2EB7149829624A7939A5 (void);
// 0x00000184 Obi.IConstraintsBatchImpl Obi.OniSkinConstraintsImpl::CreateConstraintsBatch()
extern void OniSkinConstraintsImpl_CreateConstraintsBatch_m9FB2EAD640C26270C35A54351F354F8AA1D7E043 (void);
// 0x00000185 System.Void Obi.OniSkinConstraintsImpl::RemoveBatch(Obi.IConstraintsBatchImpl)
extern void OniSkinConstraintsImpl_RemoveBatch_mC0102FC9F40941F1DDEBCDA28632CBDD43660523 (void);
// 0x00000186 System.Void Obi.OniStitchConstraintsBatchImpl::.ctor(Obi.OniStitchConstraintsImpl)
extern void OniStitchConstraintsBatchImpl__ctor_mC565125A40106134B8A526F21E536F819AACDB34 (void);
// 0x00000187 System.Void Obi.OniStitchConstraintsBatchImpl::SetStitchConstraints(Obi.ObiNativeIntList,Obi.ObiNativeFloatList,Obi.ObiNativeFloatList,System.Int32)
extern void OniStitchConstraintsBatchImpl_SetStitchConstraints_m850BB5F12FBB10D113073FFD360406906FE84F55 (void);
// 0x00000188 System.Void Obi.OniStitchConstraintsImpl::.ctor(Obi.OniSolverImpl)
extern void OniStitchConstraintsImpl__ctor_m5A0F9B6CD43ECC7D7AE65AD107B559178559308C (void);
// 0x00000189 Obi.IConstraintsBatchImpl Obi.OniStitchConstraintsImpl::CreateConstraintsBatch()
extern void OniStitchConstraintsImpl_CreateConstraintsBatch_m94119E21DE281CD292B556C5077176B4D9C9284F (void);
// 0x0000018A System.Void Obi.OniStitchConstraintsImpl::RemoveBatch(Obi.IConstraintsBatchImpl)
extern void OniStitchConstraintsImpl_RemoveBatch_m7B1ED49DA5FB278734D0C9F8597E820B3E284FAC (void);
// 0x0000018B System.Void Obi.OniStretchShearConstraintsBatchImpl::.ctor(Obi.OniStretchShearConstraintsImpl)
extern void OniStretchShearConstraintsBatchImpl__ctor_m06A2F02D467608368981ECD3800852E4B1229908 (void);
// 0x0000018C System.Void Obi.OniStretchShearConstraintsBatchImpl::SetStretchShearConstraints(Obi.ObiNativeIntList,Obi.ObiNativeIntList,Obi.ObiNativeFloatList,Obi.ObiNativeQuaternionList,Obi.ObiNativeVector3List,Obi.ObiNativeFloatList,System.Int32)
extern void OniStretchShearConstraintsBatchImpl_SetStretchShearConstraints_mACC9CB76D4FDD53A4FDD96FA2031EBD5F893B6AA (void);
// 0x0000018D System.Void Obi.OniStretchShearConstraintsImpl::.ctor(Obi.OniSolverImpl)
extern void OniStretchShearConstraintsImpl__ctor_m743767DB7C25C334E9584FACAF87392343A39ACC (void);
// 0x0000018E Obi.IConstraintsBatchImpl Obi.OniStretchShearConstraintsImpl::CreateConstraintsBatch()
extern void OniStretchShearConstraintsImpl_CreateConstraintsBatch_m78706F1FEE7FEAA5EF471FAB390E0E0D71861127 (void);
// 0x0000018F System.Void Obi.OniStretchShearConstraintsImpl::RemoveBatch(Obi.IConstraintsBatchImpl)
extern void OniStretchShearConstraintsImpl_RemoveBatch_mF8F67B57D02D49DEC0DC0197146B243A44090457 (void);
// 0x00000190 System.Void Obi.OniTetherConstraintsBatchImpl::.ctor(Obi.OniTetherConstraintsImpl)
extern void OniTetherConstraintsBatchImpl__ctor_mA18D6CEBB82288A3B578C8C4C52F4E0119DAB0B4 (void);
// 0x00000191 System.Void Obi.OniTetherConstraintsBatchImpl::SetTetherConstraints(Obi.ObiNativeIntList,Obi.ObiNativeVector2List,Obi.ObiNativeFloatList,Obi.ObiNativeFloatList,System.Int32)
extern void OniTetherConstraintsBatchImpl_SetTetherConstraints_mCA9A516027A94559796B9C29CD716F10CB4BEFD7 (void);
// 0x00000192 System.Void Obi.OniTetherConstraintsImpl::.ctor(Obi.OniSolverImpl)
extern void OniTetherConstraintsImpl__ctor_m79D07270D5F4E7E62395AA8BE2B0F5B2C5B9AD25 (void);
// 0x00000193 Obi.IConstraintsBatchImpl Obi.OniTetherConstraintsImpl::CreateConstraintsBatch()
extern void OniTetherConstraintsImpl_CreateConstraintsBatch_mD49AD0DA234A2CBAF481401273DC88CF16D87AD7 (void);
// 0x00000194 System.Void Obi.OniTetherConstraintsImpl::RemoveBatch(Obi.IConstraintsBatchImpl)
extern void OniTetherConstraintsImpl_RemoveBatch_m4B4909C5425342565704DFD35EDD47C5A1E2FB20 (void);
// 0x00000195 System.Void Obi.OniVolumeConstraintsBatchImpl::.ctor(Obi.OniVolumeConstraintsImpl)
extern void OniVolumeConstraintsBatchImpl__ctor_m17E79E7DCFE92922A8861BE569C3FF460D175CAE (void);
// 0x00000196 System.Void Obi.OniVolumeConstraintsBatchImpl::SetVolumeConstraints(Obi.ObiNativeIntList,Obi.ObiNativeIntList,Obi.ObiNativeIntList,Obi.ObiNativeFloatList,Obi.ObiNativeVector2List,Obi.ObiNativeFloatList,System.Int32)
extern void OniVolumeConstraintsBatchImpl_SetVolumeConstraints_m9C580C7D330A95E2032129920D22CC13D70E8AFC (void);
// 0x00000197 System.Void Obi.OniVolumeConstraintsImpl::.ctor(Obi.OniSolverImpl)
extern void OniVolumeConstraintsImpl__ctor_m5147F9E64F13F4D205515CE6F1906887F787BA74 (void);
// 0x00000198 Obi.IConstraintsBatchImpl Obi.OniVolumeConstraintsImpl::CreateConstraintsBatch()
extern void OniVolumeConstraintsImpl_CreateConstraintsBatch_mF7765774D05626A213C729351AE71CF664A0A8B2 (void);
// 0x00000199 System.Void Obi.OniVolumeConstraintsImpl::RemoveBatch(Obi.IConstraintsBatchImpl)
extern void OniVolumeConstraintsImpl_RemoveBatch_m726F19C3F55138E1BB32C8D3C97804E44013547C (void);
// 0x0000019A Obi.ISolverImpl Obi.OniBackend::CreateSolver(Obi.ObiSolver,System.Int32)
extern void OniBackend_CreateSolver_m5E3C44A4EA9BCDD470E7E10E684F0AF897C8AF5F (void);
// 0x0000019B System.Void Obi.OniBackend::DestroySolver(Obi.ISolverImpl)
extern void OniBackend_DestroySolver_mC6AFB023EC9F9532FF2B840DDF955E89C71A0D01 (void);
// 0x0000019C System.Void Obi.OniBackend::GetOrCreateColliderWorld()
extern void OniBackend_GetOrCreateColliderWorld_m92C5F279126E62C80B84C7ACAC6FEB209751EF1D (void);
// 0x0000019D System.Void Obi.OniBackend::.ctor()
extern void OniBackend__ctor_m45B8DE94A53B0740C6704A51A4BC4FDEB7C7D4F7 (void);
// 0x0000019E System.Int32 Obi.OniColliderWorld::get_referenceCount()
extern void OniColliderWorld_get_referenceCount_m2E1D878672ADF99B997AA9EF3050BD41B27DC9FD (void);
// 0x0000019F System.Void Obi.OniColliderWorld::Awake()
extern void OniColliderWorld_Awake_mBB4F8B9A43CB88C337CD9DB344E0B7169D47E923 (void);
// 0x000001A0 System.Void Obi.OniColliderWorld::OnDestroy()
extern void OniColliderWorld_OnDestroy_mAF83AA5D931A38B0E55E2226B94527B592FD9D83 (void);
// 0x000001A1 System.Void Obi.OniColliderWorld::IncreaseReferenceCount()
extern void OniColliderWorld_IncreaseReferenceCount_m78FCC3BFF315F84942D3994F5092A54A8D7F678E (void);
// 0x000001A2 System.Void Obi.OniColliderWorld::DecreaseReferenceCount()
extern void OniColliderWorld_DecreaseReferenceCount_m35AE9297895C720AC03E0D0F320DDAD1366BF64C (void);
// 0x000001A3 System.Void Obi.OniColliderWorld::UpdateWorld(System.Single)
extern void OniColliderWorld_UpdateWorld_m07BB5250B08FE6F9B66FBDC04C2B54A98C4B4251 (void);
// 0x000001A4 System.Void Obi.OniColliderWorld::SetColliders(Obi.ObiNativeColliderShapeList,Obi.ObiNativeAabbList,Obi.ObiNativeAffineTransformList,System.Int32)
extern void OniColliderWorld_SetColliders_mBC49AA3ED5E83E70CE733DCEC0683FF672872263 (void);
// 0x000001A5 System.Void Obi.OniColliderWorld::SetRigidbodies(Obi.ObiNativeRigidbodyList)
extern void OniColliderWorld_SetRigidbodies_m28F9E1B58302DB5A756C243EF52E788E708DA85F (void);
// 0x000001A6 System.Void Obi.OniColliderWorld::SetCollisionMaterials(Obi.ObiNativeCollisionMaterialList)
extern void OniColliderWorld_SetCollisionMaterials_mEB26C8AC30F1B5CE27D8F19A77ABB7B252115E0D (void);
// 0x000001A7 System.Void Obi.OniColliderWorld::SetTriangleMeshData(Obi.ObiNativeTriangleMeshHeaderList,Obi.ObiNativeBIHNodeList,Obi.ObiNativeTriangleList,Obi.ObiNativeVector3List)
extern void OniColliderWorld_SetTriangleMeshData_m4965F3D38925BFCACB96B6B3479DD4F232ED7104 (void);
// 0x000001A8 System.Void Obi.OniColliderWorld::SetEdgeMeshData(Obi.ObiNativeEdgeMeshHeaderList,Obi.ObiNativeBIHNodeList,Obi.ObiNativeEdgeList,Obi.ObiNativeVector2List)
extern void OniColliderWorld_SetEdgeMeshData_mD9F54F0603E44D518BC9243FF1979EC24E25FD44 (void);
// 0x000001A9 System.Void Obi.OniColliderWorld::SetDistanceFieldData(Obi.ObiNativeDistanceFieldHeaderList,Obi.ObiNativeDFNodeList)
extern void OniColliderWorld_SetDistanceFieldData_m2C1ADF6E94752BC41CE632A804CBAE1AC19A9EDF (void);
// 0x000001AA System.Void Obi.OniColliderWorld::SetHeightFieldData(Obi.ObiNativeHeightFieldHeaderList,Obi.ObiNativeFloatList)
extern void OniColliderWorld_SetHeightFieldData_m4C593F78531A7AE191FA9F8D4A8BBFC23E84F68A (void);
// 0x000001AB System.Void Obi.OniColliderWorld::.ctor()
extern void OniColliderWorld__ctor_m4DE663B94B5C376427CEB42288BA88428E549A52 (void);
// 0x000001AC System.Void Obi.OniJobHandle::.ctor(System.IntPtr)
extern void OniJobHandle__ctor_m3C938A79AA35165ECEC272FDB9E229430994231B (void);
// 0x000001AD System.Void Obi.OniJobHandle::Complete()
extern void OniJobHandle_Complete_mB9E3D94D50CF207A0F961C9F4ADEB30B2FA8A6DE (void);
// 0x000001AE System.IntPtr Obi.OniSolverImpl::get_oniSolver()
extern void OniSolverImpl_get_oniSolver_mA021288BE5461FA7F14399158965ECDD9ECA1706 (void);
// 0x000001AF System.Void Obi.OniSolverImpl::.ctor(System.IntPtr)
extern void OniSolverImpl__ctor_m2FE0413D6AF7939DBE480B789D43360A39DDFAD4 (void);
// 0x000001B0 System.Void Obi.OniSolverImpl::Destroy()
extern void OniSolverImpl_Destroy_m317E88AD3A919043461CE59F399DF3670A904961 (void);
// 0x000001B1 System.Void Obi.OniSolverImpl::InitializeFrame(UnityEngine.Vector4,UnityEngine.Vector4,UnityEngine.Quaternion)
extern void OniSolverImpl_InitializeFrame_mC8E921CBD6A70A2F9F46310EB5556586B6749DC7 (void);
// 0x000001B2 System.Void Obi.OniSolverImpl::UpdateFrame(UnityEngine.Vector4,UnityEngine.Vector4,UnityEngine.Quaternion,System.Single)
extern void OniSolverImpl_UpdateFrame_m52DA894215FDD134640AD29208E780EEBD65180A (void);
// 0x000001B3 System.Void Obi.OniSolverImpl::ApplyFrame(System.Single,System.Single,System.Single)
extern void OniSolverImpl_ApplyFrame_m67E28FA4B4FBE5C072E359F3574F562C59820DB4 (void);
// 0x000001B4 System.Int32 Obi.OniSolverImpl::GetDeformableTriangleCount()
extern void OniSolverImpl_GetDeformableTriangleCount_m230CCC63F4275668FF4EF1B34F56859D5C70E68F (void);
// 0x000001B5 System.Void Obi.OniSolverImpl::SetDeformableTriangles(System.Int32[],System.Int32,System.Int32)
extern void OniSolverImpl_SetDeformableTriangles_mE67CB6225F084DE826F48AA4D27E43FA43BE9827 (void);
// 0x000001B6 System.Int32 Obi.OniSolverImpl::RemoveDeformableTriangles(System.Int32,System.Int32)
extern void OniSolverImpl_RemoveDeformableTriangles_m3B80FB7C6FA7DB0890B5BC37670151643F54C650 (void);
// 0x000001B7 System.Void Obi.OniSolverImpl::SetSimplices(System.Int32[],Obi.SimplexCounts)
extern void OniSolverImpl_SetSimplices_mD571A7AFB950F771AE96A6A30743D2E7CE5BAE5B (void);
// 0x000001B8 System.Void Obi.OniSolverImpl::ParticleCountChanged(Obi.ObiSolver)
extern void OniSolverImpl_ParticleCountChanged_mC72028DD674A3E54C4E08E758A3CE3D26E3E7577 (void);
// 0x000001B9 System.Void Obi.OniSolverImpl::SetRigidbodyArrays(Obi.ObiSolver)
extern void OniSolverImpl_SetRigidbodyArrays_mFA7655D4B61E7E1C65C4E42C8B77EA55DFC70AEE (void);
// 0x000001BA System.Void Obi.OniSolverImpl::SetActiveParticles(System.Int32[],System.Int32)
extern void OniSolverImpl_SetActiveParticles_m850250C4A4DE24AB05AB59AADCD43D6F232077E8 (void);
// 0x000001BB System.Void Obi.OniSolverImpl::ResetForces()
extern void OniSolverImpl_ResetForces_m5E8F36B6FCA63BD1E720DBFD5B8764863E3CE626 (void);
// 0x000001BC System.Void Obi.OniSolverImpl::GetBounds(UnityEngine.Vector3&,UnityEngine.Vector3&)
extern void OniSolverImpl_GetBounds_m478CF8A2FAB372FE347E9899D262869163ACCB57 (void);
// 0x000001BD System.Void Obi.OniSolverImpl::SetParameters(Oni_SolverParameters)
extern void OniSolverImpl_SetParameters_mF506A029DB22FF674610C76D892D387BAD7178A5 (void);
// 0x000001BE System.Int32 Obi.OniSolverImpl::GetConstraintCount(Oni_ConstraintType)
extern void OniSolverImpl_GetConstraintCount_m7C95C3E195679C7076DEBAB096213874408B03EF (void);
// 0x000001BF System.Void Obi.OniSolverImpl::GetCollisionContacts(Oni_Contact[],System.Int32)
extern void OniSolverImpl_GetCollisionContacts_m3179A5E075C13134C753E47D58F150C004D15906 (void);
// 0x000001C0 System.Void Obi.OniSolverImpl::GetParticleCollisionContacts(Oni_Contact[],System.Int32)
extern void OniSolverImpl_GetParticleCollisionContacts_m4B99957B57A3E6BAF05C9E3D64DF28B5E0748259 (void);
// 0x000001C1 System.Void Obi.OniSolverImpl::SetConstraintGroupParameters(Oni_ConstraintType,Oni_ConstraintParameters&)
extern void OniSolverImpl_SetConstraintGroupParameters_mB58C6320D75A44B682B4AEA82214F8ACA62E53DA (void);
// 0x000001C2 Obi.IConstraintsBatchImpl Obi.OniSolverImpl::CreateConstraintsBatch(Oni_ConstraintType)
extern void OniSolverImpl_CreateConstraintsBatch_m388EEDF658EF8F63CD78DC8DC33F0DC642688911 (void);
// 0x000001C3 System.Void Obi.OniSolverImpl::DestroyConstraintsBatch(Obi.IConstraintsBatchImpl)
extern void OniSolverImpl_DestroyConstraintsBatch_m7769244161551FB17B31600D453AC2F68F156860 (void);
// 0x000001C4 Obi.IObiJobHandle Obi.OniSolverImpl::CollisionDetection(System.Single)
extern void OniSolverImpl_CollisionDetection_m56F7F7DC6C224E1DEE5CF8A1437ED6FE02BAB5E2 (void);
// 0x000001C5 Obi.IObiJobHandle Obi.OniSolverImpl::Substep(System.Single,System.Single,System.Int32)
extern void OniSolverImpl_Substep_mCA3F4A59045D0A506692A48C78ACE6BB4E884D93 (void);
// 0x000001C6 System.Void Obi.OniSolverImpl::ApplyInterpolation(Obi.ObiNativeVector4List,Obi.ObiNativeQuaternionList,System.Single,System.Single)
extern void OniSolverImpl_ApplyInterpolation_mAD4820F8A797FF35107C1C920D5B50D4415C67AD (void);
// 0x000001C7 System.Void Obi.OniSolverImpl::InterpolateDiffuseProperties(Obi.ObiNativeVector4List,Obi.ObiNativeVector4List,Obi.ObiNativeVector4List,Obi.ObiNativeIntList,System.Int32)
extern void OniSolverImpl_InterpolateDiffuseProperties_m0678F52DF465E6E9B34DECE91C6C3D48F96E8821 (void);
// 0x000001C8 System.Int32 Obi.OniSolverImpl::GetParticleGridSize()
extern void OniSolverImpl_GetParticleGridSize_mC858C1BFF95D86B1B0841E40378F882A014E8E78 (void);
// 0x000001C9 System.Void Obi.OniSolverImpl::GetParticleGrid(Obi.ObiNativeAabbList)
extern void OniSolverImpl_GetParticleGrid_m3A454F1EA5EDCF6A590F99A29EEAC84F0D65B790 (void);
// 0x000001CA System.Void Obi.OniSolverImpl::SpatialQuery(Obi.ObiNativeQueryShapeList,Obi.ObiNativeAffineTransformList,Obi.ObiNativeQueryResultList)
extern void OniSolverImpl_SpatialQuery_m6BD4BCC1AACE17D8D66365F9FE7F16012A1E8F7F (void);
// 0x000001CB System.Single Obi.IStructuralConstraintBatch::GetRestLength(System.Int32)
// 0x000001CC System.Void Obi.IStructuralConstraintBatch::SetRestLength(System.Int32,System.Single)
// 0x000001CD Obi.ParticlePair Obi.IStructuralConstraintBatch::GetParticleIndices(System.Int32)
// 0x000001CE Oni_ConstraintType Obi.ObiAerodynamicConstraintsBatch::get_constraintType()
extern void ObiAerodynamicConstraintsBatch_get_constraintType_mBC6B3C472BCF899A37C3C9F15B61F0B22DB160F4 (void);
// 0x000001CF Obi.IConstraintsBatchImpl Obi.ObiAerodynamicConstraintsBatch::get_implementation()
extern void ObiAerodynamicConstraintsBatch_get_implementation_mD6185053ABF60610B561CE15B51770F6E2556E08 (void);
// 0x000001D0 System.Void Obi.ObiAerodynamicConstraintsBatch::.ctor(Obi.ObiAerodynamicConstraintsData)
extern void ObiAerodynamicConstraintsBatch__ctor_m9DE858EA294B273AEED18201A289AEC50F28F02E (void);
// 0x000001D1 System.Void Obi.ObiAerodynamicConstraintsBatch::AddConstraint(System.Int32,System.Single,System.Single,System.Single)
extern void ObiAerodynamicConstraintsBatch_AddConstraint_mB9A927126FB700DC1E84DDA91D9132310EFEE124 (void);
// 0x000001D2 System.Void Obi.ObiAerodynamicConstraintsBatch::GetParticlesInvolved(System.Int32,System.Collections.Generic.List`1<System.Int32>)
extern void ObiAerodynamicConstraintsBatch_GetParticlesInvolved_m430311A6B436224B845F2F196525AD41A67EB605 (void);
// 0x000001D3 System.Void Obi.ObiAerodynamicConstraintsBatch::Clear()
extern void ObiAerodynamicConstraintsBatch_Clear_m5ACDB0CA34591C9F454153521B67AC2478467EA2 (void);
// 0x000001D4 System.Void Obi.ObiAerodynamicConstraintsBatch::SwapConstraints(System.Int32,System.Int32)
extern void ObiAerodynamicConstraintsBatch_SwapConstraints_m3F24E8A02E9F7E06130C7E27A8438623CBC36B99 (void);
// 0x000001D5 System.Void Obi.ObiAerodynamicConstraintsBatch::Merge(Obi.ObiActor,Obi.IObiConstraintsBatch)
extern void ObiAerodynamicConstraintsBatch_Merge_m4CEBEA6CE4426002B0326CFCAE2C25E3F4954CD0 (void);
// 0x000001D6 System.Void Obi.ObiAerodynamicConstraintsBatch::AddToSolver(Obi.ObiSolver)
extern void ObiAerodynamicConstraintsBatch_AddToSolver_mF2BA5925F8D160264B0EC1995C4EF9856E4E48FB (void);
// 0x000001D7 System.Void Obi.ObiAerodynamicConstraintsBatch::RemoveFromSolver(Obi.ObiSolver)
extern void ObiAerodynamicConstraintsBatch_RemoveFromSolver_m3288141E2907B24EF4337F9C513C6C69D68D8684 (void);
// 0x000001D8 Oni_ConstraintType Obi.ObiBendConstraintsBatch::get_constraintType()
extern void ObiBendConstraintsBatch_get_constraintType_mA302627388787213E045E481476B7C9EB7F43D92 (void);
// 0x000001D9 Obi.IConstraintsBatchImpl Obi.ObiBendConstraintsBatch::get_implementation()
extern void ObiBendConstraintsBatch_get_implementation_mE6CC10221A772592842A23E0EE91871FAA8ED5FA (void);
// 0x000001DA System.Void Obi.ObiBendConstraintsBatch::.ctor(Obi.ObiBendConstraintsData)
extern void ObiBendConstraintsBatch__ctor_mA90DDA31688111131E370BE9154E15ED99CF09E9 (void);
// 0x000001DB System.Void Obi.ObiBendConstraintsBatch::Merge(Obi.ObiActor,Obi.IObiConstraintsBatch)
extern void ObiBendConstraintsBatch_Merge_mC7EBF91A7220B4C03792E048ABB718337C6F03AA (void);
// 0x000001DC System.Void Obi.ObiBendConstraintsBatch::AddConstraint(UnityEngine.Vector3Int,System.Single)
extern void ObiBendConstraintsBatch_AddConstraint_m59872A1ADDD39CF3FB2C303EA08FD4E6E9AFAA0C (void);
// 0x000001DD System.Void Obi.ObiBendConstraintsBatch::Clear()
extern void ObiBendConstraintsBatch_Clear_mA0DC32AE2AE98E09AF547DE49FB25D8CDFF0DB97 (void);
// 0x000001DE System.Void Obi.ObiBendConstraintsBatch::GetParticlesInvolved(System.Int32,System.Collections.Generic.List`1<System.Int32>)
extern void ObiBendConstraintsBatch_GetParticlesInvolved_mA9C31748143F96BC5E341BE711B68E9F78204C4A (void);
// 0x000001DF System.Void Obi.ObiBendConstraintsBatch::SwapConstraints(System.Int32,System.Int32)
extern void ObiBendConstraintsBatch_SwapConstraints_mD42CD848B36CEB0540B9F211A1C7592348949487 (void);
// 0x000001E0 System.Void Obi.ObiBendConstraintsBatch::AddToSolver(Obi.ObiSolver)
extern void ObiBendConstraintsBatch_AddToSolver_m65C74A506765E2DFFD6575EE0E9D8AF1F723EBC1 (void);
// 0x000001E1 System.Void Obi.ObiBendConstraintsBatch::RemoveFromSolver(Obi.ObiSolver)
extern void ObiBendConstraintsBatch_RemoveFromSolver_m771E799A2D261FFC3AB2686EB70676ABEBCE21F7 (void);
// 0x000001E2 Oni_ConstraintType Obi.ObiBendTwistConstraintsBatch::get_constraintType()
extern void ObiBendTwistConstraintsBatch_get_constraintType_mAC26F20B9CE45F7ACE966652B0ACAB8337A9703D (void);
// 0x000001E3 Obi.IConstraintsBatchImpl Obi.ObiBendTwistConstraintsBatch::get_implementation()
extern void ObiBendTwistConstraintsBatch_get_implementation_m293D659D51624728D74EB2EC1BEF2EBA93A301D2 (void);
// 0x000001E4 System.Void Obi.ObiBendTwistConstraintsBatch::.ctor(Obi.ObiBendTwistConstraintsData)
extern void ObiBendTwistConstraintsBatch__ctor_mA84FE1A64BC385B93B82ACDB515147D1BFD9D8E9 (void);
// 0x000001E5 System.Void Obi.ObiBendTwistConstraintsBatch::AddConstraint(UnityEngine.Vector2Int,UnityEngine.Quaternion)
extern void ObiBendTwistConstraintsBatch_AddConstraint_m04DEFCAA48B91E8849ECC4FB1F151C17740B3E78 (void);
// 0x000001E6 System.Void Obi.ObiBendTwistConstraintsBatch::Clear()
extern void ObiBendTwistConstraintsBatch_Clear_mEA7D8BB6BCCA8C72D2CB42CF1D9C197F3C3FB905 (void);
// 0x000001E7 System.Void Obi.ObiBendTwistConstraintsBatch::GetParticlesInvolved(System.Int32,System.Collections.Generic.List`1<System.Int32>)
extern void ObiBendTwistConstraintsBatch_GetParticlesInvolved_m7F039D1FBFA40A6E0EFFB3F66FAA2A5D4D621393 (void);
// 0x000001E8 System.Void Obi.ObiBendTwistConstraintsBatch::SwapConstraints(System.Int32,System.Int32)
extern void ObiBendTwistConstraintsBatch_SwapConstraints_m19754F194196E8509DE60D4C6A4E01CDE61951CC (void);
// 0x000001E9 System.Void Obi.ObiBendTwistConstraintsBatch::Merge(Obi.ObiActor,Obi.IObiConstraintsBatch)
extern void ObiBendTwistConstraintsBatch_Merge_m65D2EB86911176AE0DBCFC755950330B0FF4D9C4 (void);
// 0x000001EA System.Void Obi.ObiBendTwistConstraintsBatch::AddToSolver(Obi.ObiSolver)
extern void ObiBendTwistConstraintsBatch_AddToSolver_m00517D42056B5BD7E11AF70F1C617FD33E313B35 (void);
// 0x000001EB System.Void Obi.ObiBendTwistConstraintsBatch::RemoveFromSolver(Obi.ObiSolver)
extern void ObiBendTwistConstraintsBatch_RemoveFromSolver_m89781A48762FBA02AFF8BE9423981220F26A147B (void);
// 0x000001EC Oni_ConstraintType Obi.ObiChainConstraintsBatch::get_constraintType()
extern void ObiChainConstraintsBatch_get_constraintType_mBAD7C6C6A0862F3A03015BCA2BA2B969706672C7 (void);
// 0x000001ED Obi.IConstraintsBatchImpl Obi.ObiChainConstraintsBatch::get_implementation()
extern void ObiChainConstraintsBatch_get_implementation_m2883B97D7E6175B2C1FE458CD3E667789C1FEC92 (void);
// 0x000001EE System.Void Obi.ObiChainConstraintsBatch::.ctor(Obi.ObiChainConstraintsData)
extern void ObiChainConstraintsBatch__ctor_m8E8E6310A3DDA3527AFF7F0DF6A851F75B52AEDC (void);
// 0x000001EF System.Void Obi.ObiChainConstraintsBatch::AddConstraint(System.Int32[],System.Single,System.Single,System.Single)
extern void ObiChainConstraintsBatch_AddConstraint_m8615D25C5C8628CEE00BD28F9D2D465A635813CB (void);
// 0x000001F0 System.Void Obi.ObiChainConstraintsBatch::Clear()
extern void ObiChainConstraintsBatch_Clear_mC5BA1FD8A165468C43063493EF8BFB93A3D2CE21 (void);
// 0x000001F1 System.Void Obi.ObiChainConstraintsBatch::GetParticlesInvolved(System.Int32,System.Collections.Generic.List`1<System.Int32>)
extern void ObiChainConstraintsBatch_GetParticlesInvolved_m1B0852EE90B8C0089C7793F4915B9D5C5E739187 (void);
// 0x000001F2 System.Void Obi.ObiChainConstraintsBatch::SwapConstraints(System.Int32,System.Int32)
extern void ObiChainConstraintsBatch_SwapConstraints_m56826B5809EDF1D888AC022EC80813AB5E8A8029 (void);
// 0x000001F3 System.Void Obi.ObiChainConstraintsBatch::Merge(Obi.ObiActor,Obi.IObiConstraintsBatch)
extern void ObiChainConstraintsBatch_Merge_m7C8E59CC49A14B6A3F20B923CB9983A18709369E (void);
// 0x000001F4 System.Void Obi.ObiChainConstraintsBatch::AddToSolver(Obi.ObiSolver)
extern void ObiChainConstraintsBatch_AddToSolver_m8E12FD5F4ACAA95E5FFA4E57A15C6D01796354AA (void);
// 0x000001F5 System.Void Obi.ObiChainConstraintsBatch::RemoveFromSolver(Obi.ObiSolver)
extern void ObiChainConstraintsBatch_RemoveFromSolver_m97FD3AAF49F7296DC655C1BE2B0C866D40330AAB (void);
// 0x000001F6 System.Int32 Obi.IObiConstraintsBatch::get_constraintCount()
// 0x000001F7 System.Int32 Obi.IObiConstraintsBatch::get_activeConstraintCount()
// 0x000001F8 System.Void Obi.IObiConstraintsBatch::set_activeConstraintCount(System.Int32)
// 0x000001F9 System.Int32 Obi.IObiConstraintsBatch::get_initialActiveConstraintCount()
// 0x000001FA System.Void Obi.IObiConstraintsBatch::set_initialActiveConstraintCount(System.Int32)
// 0x000001FB Oni_ConstraintType Obi.IObiConstraintsBatch::get_constraintType()
// 0x000001FC Obi.IConstraintsBatchImpl Obi.IObiConstraintsBatch::get_implementation()
// 0x000001FD System.Void Obi.IObiConstraintsBatch::AddToSolver(Obi.ObiSolver)
// 0x000001FE System.Void Obi.IObiConstraintsBatch::RemoveFromSolver(Obi.ObiSolver)
// 0x000001FF System.Void Obi.IObiConstraintsBatch::Merge(Obi.ObiActor,Obi.IObiConstraintsBatch)
// 0x00000200 System.Boolean Obi.IObiConstraintsBatch::DeactivateConstraint(System.Int32)
// 0x00000201 System.Boolean Obi.IObiConstraintsBatch::ActivateConstraint(System.Int32)
// 0x00000202 System.Void Obi.IObiConstraintsBatch::DeactivateAllConstraints()
// 0x00000203 System.Void Obi.IObiConstraintsBatch::Clear()
// 0x00000204 System.Void Obi.IObiConstraintsBatch::GetParticlesInvolved(System.Int32,System.Collections.Generic.List`1<System.Int32>)
// 0x00000205 System.Void Obi.IObiConstraintsBatch::ParticlesSwapped(System.Int32,System.Int32)
// 0x00000206 System.Int32 Obi.ObiConstraintsBatch::get_constraintCount()
extern void ObiConstraintsBatch_get_constraintCount_mA8FC55D7B67C6DE94D5B7383C39D4330CEC3C4A8 (void);
// 0x00000207 System.Int32 Obi.ObiConstraintsBatch::get_activeConstraintCount()
extern void ObiConstraintsBatch_get_activeConstraintCount_mC2764F79C4929BA7245103D0DB1CFB1D6203FC02 (void);
// 0x00000208 System.Void Obi.ObiConstraintsBatch::set_activeConstraintCount(System.Int32)
extern void ObiConstraintsBatch_set_activeConstraintCount_m5C26A4A50F8C1542DD4EA73BDACF3814B758155A (void);
// 0x00000209 System.Int32 Obi.ObiConstraintsBatch::get_initialActiveConstraintCount()
extern void ObiConstraintsBatch_get_initialActiveConstraintCount_m5F0841BDCCB934AB148E2A870F3EAA2F8552189B (void);
// 0x0000020A System.Void Obi.ObiConstraintsBatch::set_initialActiveConstraintCount(System.Int32)
extern void ObiConstraintsBatch_set_initialActiveConstraintCount_mF8081C7DCFDC4CF1B2A306517632F6E9C026912C (void);
// 0x0000020B Oni_ConstraintType Obi.ObiConstraintsBatch::get_constraintType()
// 0x0000020C Obi.IConstraintsBatchImpl Obi.ObiConstraintsBatch::get_implementation()
// 0x0000020D System.Void Obi.ObiConstraintsBatch::Merge(Obi.ObiActor,Obi.IObiConstraintsBatch)
extern void ObiConstraintsBatch_Merge_m0157340350A52C6574BBB2D68D612AA6852ADEC3 (void);
// 0x0000020E System.Void Obi.ObiConstraintsBatch::SwapConstraints(System.Int32,System.Int32)
// 0x0000020F System.Void Obi.ObiConstraintsBatch::GetParticlesInvolved(System.Int32,System.Collections.Generic.List`1<System.Int32>)
// 0x00000210 System.Void Obi.ObiConstraintsBatch::AddToSolver(Obi.ObiSolver)
// 0x00000211 System.Void Obi.ObiConstraintsBatch::RemoveFromSolver(Obi.ObiSolver)
// 0x00000212 System.Void Obi.ObiConstraintsBatch::CopyConstraint(Obi.ObiConstraintsBatch,System.Int32)
extern void ObiConstraintsBatch_CopyConstraint_m204FC07814B69BA73E78A0E743D059F565B85874 (void);
// 0x00000213 System.Void Obi.ObiConstraintsBatch::InnerSwapConstraints(System.Int32,System.Int32)
extern void ObiConstraintsBatch_InnerSwapConstraints_m72121F6E08F53AA93A94891FB98F3B6FF2D23A91 (void);
// 0x00000214 System.Void Obi.ObiConstraintsBatch::RegisterConstraint()
extern void ObiConstraintsBatch_RegisterConstraint_m8EABAFCE01074053D942CE05D9CF5CEC9381F5FB (void);
// 0x00000215 System.Void Obi.ObiConstraintsBatch::Clear()
extern void ObiConstraintsBatch_Clear_mC782D142E91B8F81F511A011339846EC1310C2D5 (void);
// 0x00000216 System.Int32 Obi.ObiConstraintsBatch::GetConstraintIndex(System.Int32)
extern void ObiConstraintsBatch_GetConstraintIndex_mA7193601AF174D3A9323DCBC41B86CF2F9EDF24B (void);
// 0x00000217 System.Boolean Obi.ObiConstraintsBatch::IsConstraintActive(System.Int32)
extern void ObiConstraintsBatch_IsConstraintActive_m86CFCB36A5A1AEBBE1FA877DFEE386BB244A7C65 (void);
// 0x00000218 System.Boolean Obi.ObiConstraintsBatch::ActivateConstraint(System.Int32)
extern void ObiConstraintsBatch_ActivateConstraint_m715C1DA819EA321555CC3E50E102E4130B0DF12E (void);
// 0x00000219 System.Boolean Obi.ObiConstraintsBatch::DeactivateConstraint(System.Int32)
extern void ObiConstraintsBatch_DeactivateConstraint_m75EE8E2E70006545791D2BA14E1E4513F9193025 (void);
// 0x0000021A System.Void Obi.ObiConstraintsBatch::DeactivateAllConstraints()
extern void ObiConstraintsBatch_DeactivateAllConstraints_mC7259C1E725CF3BFB8EC6ECFD548E6C70C2F63C5 (void);
// 0x0000021B System.Void Obi.ObiConstraintsBatch::RemoveConstraint(System.Int32)
extern void ObiConstraintsBatch_RemoveConstraint_m5AAEA90963297F57C4BFF0C6858AA1E13AE6EC46 (void);
// 0x0000021C System.Void Obi.ObiConstraintsBatch::ParticlesSwapped(System.Int32,System.Int32)
extern void ObiConstraintsBatch_ParticlesSwapped_mFA21282517AF5D3D422315E8748273AC39BC7ED1 (void);
// 0x0000021D System.Void Obi.ObiConstraintsBatch::.ctor()
extern void ObiConstraintsBatch__ctor_m2B0D34B60AB411AA8D929BA04F3BF98231678A81 (void);
// 0x0000021E Oni_ConstraintType Obi.ObiDistanceConstraintsBatch::get_constraintType()
extern void ObiDistanceConstraintsBatch_get_constraintType_m738D4B915ECC57433D1FBE3736930F510797A6D5 (void);
// 0x0000021F Obi.IConstraintsBatchImpl Obi.ObiDistanceConstraintsBatch::get_implementation()
extern void ObiDistanceConstraintsBatch_get_implementation_mB2318A9886BF7C3D0513E65A18526B4F28740515 (void);
// 0x00000220 System.Void Obi.ObiDistanceConstraintsBatch::.ctor(System.Int32)
extern void ObiDistanceConstraintsBatch__ctor_mE47DA7819812295CB5E274C6A916E54E705A0EC3 (void);
// 0x00000221 System.Void Obi.ObiDistanceConstraintsBatch::AddConstraint(UnityEngine.Vector2Int,System.Single)
extern void ObiDistanceConstraintsBatch_AddConstraint_m118C1575A23FFAC933204048E9DD894F44CDD021 (void);
// 0x00000222 System.Void Obi.ObiDistanceConstraintsBatch::Clear()
extern void ObiDistanceConstraintsBatch_Clear_mEE9F3DDD52ECC58C3A9B4391667F88D3FF4B48C4 (void);
// 0x00000223 System.Single Obi.ObiDistanceConstraintsBatch::GetRestLength(System.Int32)
extern void ObiDistanceConstraintsBatch_GetRestLength_m5E1C408F549700872BADA871F7301C1A368E0F12 (void);
// 0x00000224 System.Void Obi.ObiDistanceConstraintsBatch::SetRestLength(System.Int32,System.Single)
extern void ObiDistanceConstraintsBatch_SetRestLength_m1CA836FD76DCC1AC15BECCF86DD92E487F71397E (void);
// 0x00000225 Obi.ParticlePair Obi.ObiDistanceConstraintsBatch::GetParticleIndices(System.Int32)
extern void ObiDistanceConstraintsBatch_GetParticleIndices_mB6664FFE2333714B74F7BFEE6D915DC49C3812C6 (void);
// 0x00000226 System.Void Obi.ObiDistanceConstraintsBatch::GetParticlesInvolved(System.Int32,System.Collections.Generic.List`1<System.Int32>)
extern void ObiDistanceConstraintsBatch_GetParticlesInvolved_mC73EBE0B6DB236EC4BE938CB43ACE50F2AE65EFE (void);
// 0x00000227 System.Void Obi.ObiDistanceConstraintsBatch::CopyConstraint(Obi.ObiConstraintsBatch,System.Int32)
extern void ObiDistanceConstraintsBatch_CopyConstraint_m0EBF810160015C8A19A39367C329D343DBD1BF44 (void);
// 0x00000228 System.Void Obi.ObiDistanceConstraintsBatch::SwapConstraints(System.Int32,System.Int32)
extern void ObiDistanceConstraintsBatch_SwapConstraints_mFC45320687BA195BF90408368D14ED2B804DC370 (void);
// 0x00000229 System.Void Obi.ObiDistanceConstraintsBatch::Merge(Obi.ObiActor,Obi.IObiConstraintsBatch)
extern void ObiDistanceConstraintsBatch_Merge_m4D30F883821CC7CF022584BEC90B548D14C6DC15 (void);
// 0x0000022A System.Void Obi.ObiDistanceConstraintsBatch::AddToSolver(Obi.ObiSolver)
extern void ObiDistanceConstraintsBatch_AddToSolver_m3308FD903CF4BC955B8D2F2E0B387D2C8A79B7B8 (void);
// 0x0000022B System.Void Obi.ObiDistanceConstraintsBatch::RemoveFromSolver(Obi.ObiSolver)
extern void ObiDistanceConstraintsBatch_RemoveFromSolver_m6BC9C6E426F431C44B10DCA8CBFFEF4F67E0936E (void);
// 0x0000022C Oni_ConstraintType Obi.ObiPinConstraintsBatch::get_constraintType()
extern void ObiPinConstraintsBatch_get_constraintType_m41C00D52B2D27084965D72E1F35D4E075444E482 (void);
// 0x0000022D Obi.IConstraintsBatchImpl Obi.ObiPinConstraintsBatch::get_implementation()
extern void ObiPinConstraintsBatch_get_implementation_m4578AAC572F0C5466E210AC1BC23930F8C09C0E6 (void);
// 0x0000022E System.Void Obi.ObiPinConstraintsBatch::.ctor(Obi.ObiPinConstraintsData)
extern void ObiPinConstraintsBatch__ctor_mEE99790C9B386AE46D24F129CDDCF2C1C5B5F5FC (void);
// 0x0000022F System.Void Obi.ObiPinConstraintsBatch::AddConstraint(System.Int32,Obi.ObiColliderBase,UnityEngine.Vector3,UnityEngine.Quaternion,System.Single,System.Single,System.Single)
extern void ObiPinConstraintsBatch_AddConstraint_mF22DD07678C73491928469A7741BD8B076071DCC (void);
// 0x00000230 System.Void Obi.ObiPinConstraintsBatch::Clear()
extern void ObiPinConstraintsBatch_Clear_mB0A07E7DF21B6C3CF9C5E054F58510FFDAA522AE (void);
// 0x00000231 System.Void Obi.ObiPinConstraintsBatch::GetParticlesInvolved(System.Int32,System.Collections.Generic.List`1<System.Int32>)
extern void ObiPinConstraintsBatch_GetParticlesInvolved_m91C044A47CCA58E91BD3E72C7DE0ECE74BC37DA8 (void);
// 0x00000232 System.Void Obi.ObiPinConstraintsBatch::SwapConstraints(System.Int32,System.Int32)
extern void ObiPinConstraintsBatch_SwapConstraints_mAE0389E323855EC02CB78CE15E73076B72CB1D85 (void);
// 0x00000233 System.Void Obi.ObiPinConstraintsBatch::Merge(Obi.ObiActor,Obi.IObiConstraintsBatch)
extern void ObiPinConstraintsBatch_Merge_m2E0E93A262BDA66314A1CF7125B02B0E3D3AA3D7 (void);
// 0x00000234 System.Void Obi.ObiPinConstraintsBatch::AddToSolver(Obi.ObiSolver)
extern void ObiPinConstraintsBatch_AddToSolver_mDAB05B2150F51AA80C2911CEF1BCE5E104D37E86 (void);
// 0x00000235 System.Void Obi.ObiPinConstraintsBatch::RemoveFromSolver(Obi.ObiSolver)
extern void ObiPinConstraintsBatch_RemoveFromSolver_mC766AF925EE815328507994A26FC0DA6399A473C (void);
// 0x00000236 Oni_ConstraintType Obi.ObiShapeMatchingConstraintsBatch::get_constraintType()
extern void ObiShapeMatchingConstraintsBatch_get_constraintType_m1D1B013631E72B441D2F440FABC9A47260BC1EFC (void);
// 0x00000237 Obi.IConstraintsBatchImpl Obi.ObiShapeMatchingConstraintsBatch::get_implementation()
extern void ObiShapeMatchingConstraintsBatch_get_implementation_mDE72A361D520AEF2417D5FA3D99265B24AE622BE (void);
// 0x00000238 System.Void Obi.ObiShapeMatchingConstraintsBatch::.ctor(Obi.ObiShapeMatchingConstraintsData)
extern void ObiShapeMatchingConstraintsBatch__ctor_m07C5E1D77AD00968700D64371442119BD0EFD860 (void);
// 0x00000239 System.Void Obi.ObiShapeMatchingConstraintsBatch::AddConstraint(System.Int32[],System.Boolean)
extern void ObiShapeMatchingConstraintsBatch_AddConstraint_m36DB2368774B955435601E74A559F9194797B73E (void);
// 0x0000023A System.Void Obi.ObiShapeMatchingConstraintsBatch::Clear()
extern void ObiShapeMatchingConstraintsBatch_Clear_m7613DFE3A4A449971A4C9C0E7F78D8ED7D99B7CF (void);
// 0x0000023B System.Void Obi.ObiShapeMatchingConstraintsBatch::GetParticlesInvolved(System.Int32,System.Collections.Generic.List`1<System.Int32>)
extern void ObiShapeMatchingConstraintsBatch_GetParticlesInvolved_m460EACD5CA047B22B48866E7980990188EF04B88 (void);
// 0x0000023C System.Void Obi.ObiShapeMatchingConstraintsBatch::SwapConstraints(System.Int32,System.Int32)
extern void ObiShapeMatchingConstraintsBatch_SwapConstraints_m203CA70E418F8DF2BF506ED333695247E286BA2B (void);
// 0x0000023D System.Void Obi.ObiShapeMatchingConstraintsBatch::Merge(Obi.ObiActor,Obi.IObiConstraintsBatch)
extern void ObiShapeMatchingConstraintsBatch_Merge_m5A9DEA535310B3863D91C0FBE5A52C051E0357A7 (void);
// 0x0000023E System.Void Obi.ObiShapeMatchingConstraintsBatch::AddToSolver(Obi.ObiSolver)
extern void ObiShapeMatchingConstraintsBatch_AddToSolver_mA64BACE29C81AC0D914642A2326C5A768BCAD690 (void);
// 0x0000023F System.Void Obi.ObiShapeMatchingConstraintsBatch::RemoveFromSolver(Obi.ObiSolver)
extern void ObiShapeMatchingConstraintsBatch_RemoveFromSolver_m64F2CB468C81ED844CEE845FC280A5497A18AD33 (void);
// 0x00000240 System.Void Obi.ObiShapeMatchingConstraintsBatch::RecalculateRestShapeMatching()
extern void ObiShapeMatchingConstraintsBatch_RecalculateRestShapeMatching_m49C8363C8157E30089548C4BDF8BDA633D26DDB7 (void);
// 0x00000241 Oni_ConstraintType Obi.ObiSkinConstraintsBatch::get_constraintType()
extern void ObiSkinConstraintsBatch_get_constraintType_mB8336F19B6DBE7777042FA4814EA12115D899223 (void);
// 0x00000242 Obi.IConstraintsBatchImpl Obi.ObiSkinConstraintsBatch::get_implementation()
extern void ObiSkinConstraintsBatch_get_implementation_m8EFE8C0407125BE90F4A544FD0BBD6801F606924 (void);
// 0x00000243 System.Void Obi.ObiSkinConstraintsBatch::.ctor(Obi.ObiSkinConstraintsData)
extern void ObiSkinConstraintsBatch__ctor_m1C9F633F90BE267DDA66B580DED05A7F19BD61C5 (void);
// 0x00000244 System.Void Obi.ObiSkinConstraintsBatch::AddConstraint(System.Int32,UnityEngine.Vector4,UnityEngine.Vector4,System.Single,System.Single,System.Single,System.Single)
extern void ObiSkinConstraintsBatch_AddConstraint_m5D3D3C2490F09B0C7B72F763AA56E3AE9D485134 (void);
// 0x00000245 System.Void Obi.ObiSkinConstraintsBatch::Clear()
extern void ObiSkinConstraintsBatch_Clear_m9760EF8B82970CC9CDE9A840BD9EAF78BD982393 (void);
// 0x00000246 System.Void Obi.ObiSkinConstraintsBatch::GetParticlesInvolved(System.Int32,System.Collections.Generic.List`1<System.Int32>)
extern void ObiSkinConstraintsBatch_GetParticlesInvolved_m77E2085BC3B66F68FE4B9453F0D76A9566718597 (void);
// 0x00000247 System.Void Obi.ObiSkinConstraintsBatch::SwapConstraints(System.Int32,System.Int32)
extern void ObiSkinConstraintsBatch_SwapConstraints_m90010A1D7C64381F5575F0F393B7030C2201DF8F (void);
// 0x00000248 System.Void Obi.ObiSkinConstraintsBatch::Merge(Obi.ObiActor,Obi.IObiConstraintsBatch)
extern void ObiSkinConstraintsBatch_Merge_m5AC677F66016C8CC6DCC768E4A60ECEE633075A6 (void);
// 0x00000249 System.Void Obi.ObiSkinConstraintsBatch::AddToSolver(Obi.ObiSolver)
extern void ObiSkinConstraintsBatch_AddToSolver_mF9FF0C39D2BA6C21C642F2F0880004B3D719DC2C (void);
// 0x0000024A System.Void Obi.ObiSkinConstraintsBatch::RemoveFromSolver(Obi.ObiSolver)
extern void ObiSkinConstraintsBatch_RemoveFromSolver_m146DF16BA35D6BBBAE5781504D744C59D61D80FE (void);
// 0x0000024B Oni_ConstraintType Obi.ObiStretchShearConstraintsBatch::get_constraintType()
extern void ObiStretchShearConstraintsBatch_get_constraintType_mC6DEA77B39D02D597E2CA898182A9BD560B545C6 (void);
// 0x0000024C Obi.IConstraintsBatchImpl Obi.ObiStretchShearConstraintsBatch::get_implementation()
extern void ObiStretchShearConstraintsBatch_get_implementation_m0DAC376106BC35C967BE578A40049E3C35E27024 (void);
// 0x0000024D System.Void Obi.ObiStretchShearConstraintsBatch::.ctor(Obi.ObiStretchShearConstraintsData)
extern void ObiStretchShearConstraintsBatch__ctor_m8927550DA897869840F27FAE27254EF2961AEFB7 (void);
// 0x0000024E System.Void Obi.ObiStretchShearConstraintsBatch::AddConstraint(UnityEngine.Vector2Int,System.Int32,System.Single,UnityEngine.Quaternion)
extern void ObiStretchShearConstraintsBatch_AddConstraint_m3EDB080395E5167A460038CE0B00CEEC306C19D5 (void);
// 0x0000024F System.Void Obi.ObiStretchShearConstraintsBatch::Clear()
extern void ObiStretchShearConstraintsBatch_Clear_m827C6A5985D286459089DC4635044609BC6B098C (void);
// 0x00000250 System.Single Obi.ObiStretchShearConstraintsBatch::GetRestLength(System.Int32)
extern void ObiStretchShearConstraintsBatch_GetRestLength_mC0368700D80E14C1F758085E1051265DB7E6F140 (void);
// 0x00000251 System.Void Obi.ObiStretchShearConstraintsBatch::SetRestLength(System.Int32,System.Single)
extern void ObiStretchShearConstraintsBatch_SetRestLength_m4401CCB79E8593DE77C2F66191AE967C6DC7E5DC (void);
// 0x00000252 Obi.ParticlePair Obi.ObiStretchShearConstraintsBatch::GetParticleIndices(System.Int32)
extern void ObiStretchShearConstraintsBatch_GetParticleIndices_mC265643A55371F01BD13A0C3FC34A11D9FFE838C (void);
// 0x00000253 System.Void Obi.ObiStretchShearConstraintsBatch::GetParticlesInvolved(System.Int32,System.Collections.Generic.List`1<System.Int32>)
extern void ObiStretchShearConstraintsBatch_GetParticlesInvolved_m304EEFB3092A0112EAE84B4CECF3F6A948D9833E (void);
// 0x00000254 System.Void Obi.ObiStretchShearConstraintsBatch::SwapConstraints(System.Int32,System.Int32)
extern void ObiStretchShearConstraintsBatch_SwapConstraints_m2C8677D96612ADAE5BFCC4825A9BA4E42685609E (void);
// 0x00000255 System.Void Obi.ObiStretchShearConstraintsBatch::Merge(Obi.ObiActor,Obi.IObiConstraintsBatch)
extern void ObiStretchShearConstraintsBatch_Merge_mC487E3A37D51D78F57A1880A27B8168E45F05BB8 (void);
// 0x00000256 System.Void Obi.ObiStretchShearConstraintsBatch::AddToSolver(Obi.ObiSolver)
extern void ObiStretchShearConstraintsBatch_AddToSolver_mA25B51FDA1EBF1E1BE6AD594D45C153216AA8C94 (void);
// 0x00000257 System.Void Obi.ObiStretchShearConstraintsBatch::RemoveFromSolver(Obi.ObiSolver)
extern void ObiStretchShearConstraintsBatch_RemoveFromSolver_m45797BE9F2A7CC2B0BD983E810A313C126621382 (void);
// 0x00000258 Oni_ConstraintType Obi.ObiTetherConstraintsBatch::get_constraintType()
extern void ObiTetherConstraintsBatch_get_constraintType_m8483A4DC0E6B622F67EC74C1BB113C00981EFA6C (void);
// 0x00000259 Obi.IConstraintsBatchImpl Obi.ObiTetherConstraintsBatch::get_implementation()
extern void ObiTetherConstraintsBatch_get_implementation_mC6C6A9923482CF3A380B6D250E1A63C0EC69E02F (void);
// 0x0000025A System.Void Obi.ObiTetherConstraintsBatch::.ctor(Obi.ObiTetherConstraintsData)
extern void ObiTetherConstraintsBatch__ctor_m8866FC2DA610911877A83CE2904D78CACB19FC51 (void);
// 0x0000025B System.Void Obi.ObiTetherConstraintsBatch::AddConstraint(UnityEngine.Vector2Int,System.Single,System.Single)
extern void ObiTetherConstraintsBatch_AddConstraint_m1BF702B37FD24AD678D201379C614BFA7C24824A (void);
// 0x0000025C System.Void Obi.ObiTetherConstraintsBatch::Clear()
extern void ObiTetherConstraintsBatch_Clear_m2F60F212808B2D2566D9486751BA43D287D7A616 (void);
// 0x0000025D System.Void Obi.ObiTetherConstraintsBatch::GetParticlesInvolved(System.Int32,System.Collections.Generic.List`1<System.Int32>)
extern void ObiTetherConstraintsBatch_GetParticlesInvolved_m77FB1AD6E9F76D24F63C6CF2FCA60F8627D497BA (void);
// 0x0000025E System.Void Obi.ObiTetherConstraintsBatch::SwapConstraints(System.Int32,System.Int32)
extern void ObiTetherConstraintsBatch_SwapConstraints_mE3AD347DF0A9C8FA3D0467F87FFEC0B13FD61E72 (void);
// 0x0000025F System.Void Obi.ObiTetherConstraintsBatch::Merge(Obi.ObiActor,Obi.IObiConstraintsBatch)
extern void ObiTetherConstraintsBatch_Merge_m1AD265CC0EE520D80F27DEFBE161FF9E44B80A76 (void);
// 0x00000260 System.Void Obi.ObiTetherConstraintsBatch::AddToSolver(Obi.ObiSolver)
extern void ObiTetherConstraintsBatch_AddToSolver_mD57866191E80F88A3D272C0E0AC76364326A80E3 (void);
// 0x00000261 System.Void Obi.ObiTetherConstraintsBatch::RemoveFromSolver(Obi.ObiSolver)
extern void ObiTetherConstraintsBatch_RemoveFromSolver_mEE3D5BD7C2CF365225947E773A815095121EF2C6 (void);
// 0x00000262 System.Void Obi.ObiTetherConstraintsBatch::SetParameters(System.Single,System.Single)
extern void ObiTetherConstraintsBatch_SetParameters_m1577626B4E1C0111CF067DABC451BC0503615466 (void);
// 0x00000263 Oni_ConstraintType Obi.ObiVolumeConstraintsBatch::get_constraintType()
extern void ObiVolumeConstraintsBatch_get_constraintType_m381ED3FD4921DB235D1AE2E0D93D354C840CE86A (void);
// 0x00000264 Obi.IConstraintsBatchImpl Obi.ObiVolumeConstraintsBatch::get_implementation()
extern void ObiVolumeConstraintsBatch_get_implementation_mA945BB5C6B7A7EE83AC6C01C6B5DD21207FB0325 (void);
// 0x00000265 System.Void Obi.ObiVolumeConstraintsBatch::.ctor(Obi.ObiVolumeConstraintsData)
extern void ObiVolumeConstraintsBatch__ctor_mA44A358BC97BD1830050A02DFFDF5108A6FD9715 (void);
// 0x00000266 System.Void Obi.ObiVolumeConstraintsBatch::AddConstraint(System.Int32[],System.Single)
extern void ObiVolumeConstraintsBatch_AddConstraint_m411C62C1E1117BF0B3B6954C3641F5B418583424 (void);
// 0x00000267 System.Void Obi.ObiVolumeConstraintsBatch::Clear()
extern void ObiVolumeConstraintsBatch_Clear_m53B029F54892E0D5B9D4D2BCB647A4176FE793A0 (void);
// 0x00000268 System.Void Obi.ObiVolumeConstraintsBatch::GetParticlesInvolved(System.Int32,System.Collections.Generic.List`1<System.Int32>)
extern void ObiVolumeConstraintsBatch_GetParticlesInvolved_mC38E81ADD236D1D8841141D16BA6989DA3A489F2 (void);
// 0x00000269 System.Void Obi.ObiVolumeConstraintsBatch::SwapConstraints(System.Int32,System.Int32)
extern void ObiVolumeConstraintsBatch_SwapConstraints_m42B669FC7CBDFA494210C6486682BD7A3CFFDDB4 (void);
// 0x0000026A System.Void Obi.ObiVolumeConstraintsBatch::Merge(Obi.ObiActor,Obi.IObiConstraintsBatch)
extern void ObiVolumeConstraintsBatch_Merge_m1DDAB49D1408ADCD12D27C72737072480252DA7B (void);
// 0x0000026B System.Void Obi.ObiVolumeConstraintsBatch::AddToSolver(Obi.ObiSolver)
extern void ObiVolumeConstraintsBatch_AddToSolver_m1070A8D58A5B37CC00B5290D1448490F3DD9CE29 (void);
// 0x0000026C System.Void Obi.ObiVolumeConstraintsBatch::RemoveFromSolver(Obi.ObiSolver)
extern void ObiVolumeConstraintsBatch_RemoveFromSolver_m01AB99DBC04273CEB49EFAAFE595F93D5F05B726 (void);
// 0x0000026D System.Void Obi.ObiVolumeConstraintsBatch::SetParameters(System.Single,System.Single)
extern void ObiVolumeConstraintsBatch_SetParameters_m8B574D29F6430355A0AF20B229AA493E0119AEF5 (void);
// 0x0000026E System.Boolean Obi.IAerodynamicConstraintsUser::get_aerodynamicsEnabled()
// 0x0000026F System.Void Obi.IAerodynamicConstraintsUser::set_aerodynamicsEnabled(System.Boolean)
// 0x00000270 System.Single Obi.IAerodynamicConstraintsUser::get_drag()
// 0x00000271 System.Void Obi.IAerodynamicConstraintsUser::set_drag(System.Single)
// 0x00000272 System.Single Obi.IAerodynamicConstraintsUser::get_lift()
// 0x00000273 System.Void Obi.IAerodynamicConstraintsUser::set_lift(System.Single)
// 0x00000274 Obi.ObiAerodynamicConstraintsBatch Obi.ObiAerodynamicConstraintsData::CreateBatch(Obi.ObiAerodynamicConstraintsBatch)
extern void ObiAerodynamicConstraintsData_CreateBatch_mD91F60B676E69FE28DD53AA57D5E64E621F7E489 (void);
// 0x00000275 System.Void Obi.ObiAerodynamicConstraintsData::.ctor()
extern void ObiAerodynamicConstraintsData__ctor_mD5BD0D829EC39F1CACD35CF16E040852B4B4784B (void);
// 0x00000276 System.Boolean Obi.IBendConstraintsUser::get_bendConstraintsEnabled()
// 0x00000277 System.Void Obi.IBendConstraintsUser::set_bendConstraintsEnabled(System.Boolean)
// 0x00000278 System.Single Obi.IBendConstraintsUser::get_bendCompliance()
// 0x00000279 System.Void Obi.IBendConstraintsUser::set_bendCompliance(System.Single)
// 0x0000027A System.Single Obi.IBendConstraintsUser::get_maxBending()
// 0x0000027B System.Void Obi.IBendConstraintsUser::set_maxBending(System.Single)
// 0x0000027C System.Single Obi.IBendConstraintsUser::get_plasticYield()
// 0x0000027D System.Void Obi.IBendConstraintsUser::set_plasticYield(System.Single)
// 0x0000027E System.Single Obi.IBendConstraintsUser::get_plasticCreep()
// 0x0000027F System.Void Obi.IBendConstraintsUser::set_plasticCreep(System.Single)
// 0x00000280 Obi.ObiBendConstraintsBatch Obi.ObiBendConstraintsData::CreateBatch(Obi.ObiBendConstraintsBatch)
extern void ObiBendConstraintsData_CreateBatch_mD0A372B63F6C4F88AC48D4E4E9304944D3FEA0F2 (void);
// 0x00000281 System.Void Obi.ObiBendConstraintsData::.ctor()
extern void ObiBendConstraintsData__ctor_mFE69ED43DBE4731FAAAF8C32658B04596342980A (void);
// 0x00000282 System.Boolean Obi.IBendTwistConstraintsUser::get_bendTwistConstraintsEnabled()
// 0x00000283 System.Void Obi.IBendTwistConstraintsUser::set_bendTwistConstraintsEnabled(System.Boolean)
// 0x00000284 System.Single Obi.IBendTwistConstraintsUser::get_torsionCompliance()
// 0x00000285 System.Void Obi.IBendTwistConstraintsUser::set_torsionCompliance(System.Single)
// 0x00000286 System.Single Obi.IBendTwistConstraintsUser::get_bend1Compliance()
// 0x00000287 System.Void Obi.IBendTwistConstraintsUser::set_bend1Compliance(System.Single)
// 0x00000288 System.Single Obi.IBendTwistConstraintsUser::get_bend2Compliance()
// 0x00000289 System.Void Obi.IBendTwistConstraintsUser::set_bend2Compliance(System.Single)
// 0x0000028A System.Single Obi.IBendTwistConstraintsUser::get_plasticYield()
// 0x0000028B System.Void Obi.IBendTwistConstraintsUser::set_plasticYield(System.Single)
// 0x0000028C System.Single Obi.IBendTwistConstraintsUser::get_plasticCreep()
// 0x0000028D System.Void Obi.IBendTwistConstraintsUser::set_plasticCreep(System.Single)
// 0x0000028E Obi.ObiBendTwistConstraintsBatch Obi.ObiBendTwistConstraintsData::CreateBatch(Obi.ObiBendTwistConstraintsBatch)
extern void ObiBendTwistConstraintsData_CreateBatch_m9A87A7382C1A63A339F0C8B17CF14D1CB03B0D46 (void);
// 0x0000028F System.Void Obi.ObiBendTwistConstraintsData::.ctor()
extern void ObiBendTwistConstraintsData__ctor_mC6E27E26B877C88510C9E27FB9ED17BF94E98B9A (void);
// 0x00000290 System.Boolean Obi.IChainConstraintsUser::get_chainConstraintsEnabled()
// 0x00000291 System.Void Obi.IChainConstraintsUser::set_chainConstraintsEnabled(System.Boolean)
// 0x00000292 System.Single Obi.IChainConstraintsUser::get_tightness()
// 0x00000293 System.Void Obi.IChainConstraintsUser::set_tightness(System.Single)
// 0x00000294 Obi.ObiChainConstraintsBatch Obi.ObiChainConstraintsData::CreateBatch(Obi.ObiChainConstraintsBatch)
extern void ObiChainConstraintsData_CreateBatch_mC2FB00A47639537C6959AE16661DAAF1AD4C3F7B (void);
// 0x00000295 System.Void Obi.ObiChainConstraintsData::.ctor()
extern void ObiChainConstraintsData__ctor_m9D1C51412A1878F85D8C173777A60EC621A9FFDD (void);
// 0x00000296 System.Boolean Obi.IDistanceConstraintsUser::get_distanceConstraintsEnabled()
// 0x00000297 System.Void Obi.IDistanceConstraintsUser::set_distanceConstraintsEnabled(System.Boolean)
// 0x00000298 System.Single Obi.IDistanceConstraintsUser::get_stretchingScale()
// 0x00000299 System.Void Obi.IDistanceConstraintsUser::set_stretchingScale(System.Single)
// 0x0000029A System.Single Obi.IDistanceConstraintsUser::get_stretchCompliance()
// 0x0000029B System.Void Obi.IDistanceConstraintsUser::set_stretchCompliance(System.Single)
// 0x0000029C System.Single Obi.IDistanceConstraintsUser::get_maxCompression()
// 0x0000029D System.Void Obi.IDistanceConstraintsUser::set_maxCompression(System.Single)
// 0x0000029E Obi.ObiDistanceConstraintsBatch Obi.ObiDistanceConstraintsData::CreateBatch(Obi.ObiDistanceConstraintsBatch)
extern void ObiDistanceConstraintsData_CreateBatch_mA63574AD8629D5C6907EB15B3FFA9F0D1616D026 (void);
// 0x0000029F System.Void Obi.ObiDistanceConstraintsData::.ctor()
extern void ObiDistanceConstraintsData__ctor_m10E8E535222BCC55670623BBB7DA36CAF7A2A95E (void);
// 0x000002A0 Obi.ObiPinConstraintsBatch Obi.ObiPinConstraintsData::CreateBatch(Obi.ObiPinConstraintsBatch)
extern void ObiPinConstraintsData_CreateBatch_mC8E21D95D7C5E27097C6B75B20DC74CBDD2D345B (void);
// 0x000002A1 System.Void Obi.ObiPinConstraintsData::.ctor()
extern void ObiPinConstraintsData__ctor_mA86A434A33F8CF37FA8E7E2410BF8223116F03A0 (void);
// 0x000002A2 System.Boolean Obi.IShapeMatchingConstraintsUser::get_shapeMatchingConstraintsEnabled()
// 0x000002A3 System.Void Obi.IShapeMatchingConstraintsUser::set_shapeMatchingConstraintsEnabled(System.Boolean)
// 0x000002A4 System.Single Obi.IShapeMatchingConstraintsUser::get_deformationResistance()
// 0x000002A5 System.Void Obi.IShapeMatchingConstraintsUser::set_deformationResistance(System.Single)
// 0x000002A6 System.Single Obi.IShapeMatchingConstraintsUser::get_maxDeformation()
// 0x000002A7 System.Void Obi.IShapeMatchingConstraintsUser::set_maxDeformation(System.Single)
// 0x000002A8 System.Single Obi.IShapeMatchingConstraintsUser::get_plasticYield()
// 0x000002A9 System.Void Obi.IShapeMatchingConstraintsUser::set_plasticYield(System.Single)
// 0x000002AA System.Single Obi.IShapeMatchingConstraintsUser::get_plasticCreep()
// 0x000002AB System.Void Obi.IShapeMatchingConstraintsUser::set_plasticCreep(System.Single)
// 0x000002AC System.Single Obi.IShapeMatchingConstraintsUser::get_plasticRecovery()
// 0x000002AD System.Void Obi.IShapeMatchingConstraintsUser::set_plasticRecovery(System.Single)
// 0x000002AE Obi.ObiShapeMatchingConstraintsBatch Obi.ObiShapeMatchingConstraintsData::CreateBatch(Obi.ObiShapeMatchingConstraintsBatch)
extern void ObiShapeMatchingConstraintsData_CreateBatch_mDD0753126D9E04AB8FD0D7F4CB4D6C9ACA8BF359 (void);
// 0x000002AF System.Void Obi.ObiShapeMatchingConstraintsData::.ctor()
extern void ObiShapeMatchingConstraintsData__ctor_m39A87174E47FB428D6589800F2F18FE1F79D340A (void);
// 0x000002B0 Obi.ObiSkinConstraintsBatch Obi.ObiSkinConstraintsData::CreateBatch(Obi.ObiSkinConstraintsBatch)
extern void ObiSkinConstraintsData_CreateBatch_m0420AD7727A873F46A9E43F8BF1F94895AB14369 (void);
// 0x000002B1 System.Void Obi.ObiSkinConstraintsData::.ctor()
extern void ObiSkinConstraintsData__ctor_mCB9A63E7B637DEAEB1063E52913985321ABBCAA3 (void);
// 0x000002B2 System.Boolean Obi.IStretchShearConstraintsUser::get_stretchShearConstraintsEnabled()
// 0x000002B3 System.Void Obi.IStretchShearConstraintsUser::set_stretchShearConstraintsEnabled(System.Boolean)
// 0x000002B4 System.Single Obi.IStretchShearConstraintsUser::get_stretchCompliance()
// 0x000002B5 System.Void Obi.IStretchShearConstraintsUser::set_stretchCompliance(System.Single)
// 0x000002B6 System.Single Obi.IStretchShearConstraintsUser::get_shear1Compliance()
// 0x000002B7 System.Void Obi.IStretchShearConstraintsUser::set_shear1Compliance(System.Single)
// 0x000002B8 System.Single Obi.IStretchShearConstraintsUser::get_shear2Compliance()
// 0x000002B9 System.Void Obi.IStretchShearConstraintsUser::set_shear2Compliance(System.Single)
// 0x000002BA Obi.ObiStretchShearConstraintsBatch Obi.ObiStretchShearConstraintsData::CreateBatch(Obi.ObiStretchShearConstraintsBatch)
extern void ObiStretchShearConstraintsData_CreateBatch_mFBE3B0B12D6BD8E226076AB93E16906F92B60C68 (void);
// 0x000002BB System.Void Obi.ObiStretchShearConstraintsData::.ctor()
extern void ObiStretchShearConstraintsData__ctor_m81576073ED6B14D835150C4666A3588AEDBD29FC (void);
// 0x000002BC System.Boolean Obi.ITetherConstraintsUser::get_tetherConstraintsEnabled()
// 0x000002BD System.Void Obi.ITetherConstraintsUser::set_tetherConstraintsEnabled(System.Boolean)
// 0x000002BE System.Single Obi.ITetherConstraintsUser::get_tetherCompliance()
// 0x000002BF System.Void Obi.ITetherConstraintsUser::set_tetherCompliance(System.Single)
// 0x000002C0 System.Single Obi.ITetherConstraintsUser::get_tetherScale()
// 0x000002C1 System.Void Obi.ITetherConstraintsUser::set_tetherScale(System.Single)
// 0x000002C2 Obi.ObiTetherConstraintsBatch Obi.ObiTetherConstraintsData::CreateBatch(Obi.ObiTetherConstraintsBatch)
extern void ObiTetherConstraintsData_CreateBatch_m28C477BD204B78B77C867473CB959C5340B918D7 (void);
// 0x000002C3 System.Void Obi.ObiTetherConstraintsData::.ctor()
extern void ObiTetherConstraintsData__ctor_m7488642C0CCCDA9C252605F19F887C961D3BB5C4 (void);
// 0x000002C4 System.Boolean Obi.IVolumeConstraintsUser::get_volumeConstraintsEnabled()
// 0x000002C5 System.Void Obi.IVolumeConstraintsUser::set_volumeConstraintsEnabled(System.Boolean)
// 0x000002C6 System.Single Obi.IVolumeConstraintsUser::get_compressionCompliance()
// 0x000002C7 System.Void Obi.IVolumeConstraintsUser::set_compressionCompliance(System.Single)
// 0x000002C8 System.Single Obi.IVolumeConstraintsUser::get_pressure()
// 0x000002C9 System.Void Obi.IVolumeConstraintsUser::set_pressure(System.Single)
// 0x000002CA Obi.ObiVolumeConstraintsBatch Obi.ObiVolumeConstraintsData::CreateBatch(Obi.ObiVolumeConstraintsBatch)
extern void ObiVolumeConstraintsData_CreateBatch_m1DDE0FAC895B8FC54D095EB773D7AEBDE107A061 (void);
// 0x000002CB System.Void Obi.ObiVolumeConstraintsData::.ctor()
extern void ObiVolumeConstraintsData__ctor_m09F646B51D450E9B87B194F9E29B8B5FE9C2404B (void);
// 0x000002CC System.Nullable`1<Oni_ConstraintType> Obi.IObiConstraints::GetConstraintType()
// 0x000002CD Obi.IObiConstraintsBatch Obi.IObiConstraints::GetBatch(System.Int32)
// 0x000002CE System.Int32 Obi.IObiConstraints::GetBatchCount()
// 0x000002CF System.Void Obi.IObiConstraints::Clear()
// 0x000002D0 System.Boolean Obi.IObiConstraints::AddToSolver(Obi.ObiSolver)
// 0x000002D1 System.Boolean Obi.IObiConstraints::RemoveFromSolver()
// 0x000002D2 System.Int32 Obi.IObiConstraints::GetConstraintCount()
// 0x000002D3 System.Int32 Obi.IObiConstraints::GetActiveConstraintCount()
// 0x000002D4 System.Void Obi.IObiConstraints::DeactivateAllConstraints()
// 0x000002D5 System.Void Obi.IObiConstraints::Merge(Obi.ObiActor,Obi.IObiConstraints)
// 0x000002D6 System.Void Obi.ObiConstraints`1::Merge(Obi.ObiActor,Obi.IObiConstraints)
// 0x000002D7 Obi.IObiConstraintsBatch Obi.ObiConstraints`1::GetBatch(System.Int32)
// 0x000002D8 System.Int32 Obi.ObiConstraints`1::GetBatchCount()
// 0x000002D9 System.Int32 Obi.ObiConstraints`1::GetConstraintCount()
// 0x000002DA System.Int32 Obi.ObiConstraints`1::GetActiveConstraintCount()
// 0x000002DB System.Void Obi.ObiConstraints`1::DeactivateAllConstraints()
// 0x000002DC T Obi.ObiConstraints`1::GetFirstBatch()
// 0x000002DD System.Nullable`1<Oni_ConstraintType> Obi.ObiConstraints`1::GetConstraintType()
// 0x000002DE System.Void Obi.ObiConstraints`1::Clear()
// 0x000002DF T Obi.ObiConstraints`1::CreateBatch(T)
// 0x000002E0 System.Void Obi.ObiConstraints`1::AddBatch(T)
// 0x000002E1 System.Boolean Obi.ObiConstraints`1::RemoveBatch(T)
// 0x000002E2 System.Boolean Obi.ObiConstraints`1::AddToSolver(Obi.ObiSolver)
// 0x000002E3 System.Boolean Obi.ObiConstraints`1::RemoveFromSolver()
// 0x000002E4 System.Void Obi.ObiConstraints`1::.ctor()
// 0x000002E5 System.Single Obi.StructuralConstraint::get_restLength()
extern void StructuralConstraint_get_restLength_mA86CC2512F19A0314F76C459C7027A50DEB66F57_AdjustorThunk (void);
// 0x000002E6 System.Void Obi.StructuralConstraint::set_restLength(System.Single)
extern void StructuralConstraint_set_restLength_mFDDB0536829F1845FAB3F39B80ED0520431D2BDF_AdjustorThunk (void);
// 0x000002E7 System.Void Obi.StructuralConstraint::.ctor(Obi.IStructuralConstraintBatch,System.Int32,System.Single)
extern void StructuralConstraint__ctor_m9F7FF404C8685160AC46543FCE7681E91FF6B322_AdjustorThunk (void);
// 0x000002E8 System.Int32[] Obi.GraphColoring::Colorize(System.Int32[],System.Int32[])
extern void GraphColoring_Colorize_mC1E72C7E56EE6F1F72682546D5CB94934287958C (void);
// 0x000002E9 System.Void Obi.ObiActorBlueprint::add_OnBlueprintGenerate(Obi.ObiActorBlueprint_BlueprintCallback)
extern void ObiActorBlueprint_add_OnBlueprintGenerate_mA116B88726BA07CDAC7EA40506772CC39F8FF86E (void);
// 0x000002EA System.Void Obi.ObiActorBlueprint::remove_OnBlueprintGenerate(Obi.ObiActorBlueprint_BlueprintCallback)
extern void ObiActorBlueprint_remove_OnBlueprintGenerate_m780AFE9B203E214D86769A37EADEE63D5FC9967E (void);
// 0x000002EB System.Int32 Obi.ObiActorBlueprint::get_particleCount()
extern void ObiActorBlueprint_get_particleCount_mCC6F47C83D7F2BE2BBF1667511B5C96BAA5A94B2 (void);
// 0x000002EC System.Int32 Obi.ObiActorBlueprint::get_activeParticleCount()
extern void ObiActorBlueprint_get_activeParticleCount_m22AD50CE827DB1E01A7937D968032E6AF08B228A (void);
// 0x000002ED System.Boolean Obi.ObiActorBlueprint::get_usesOrientedParticles()
extern void ObiActorBlueprint_get_usesOrientedParticles_m8E951A7BDC8EEA54633C2AA8BE8E41F01A418233 (void);
// 0x000002EE System.Boolean Obi.ObiActorBlueprint::get_usesTethers()
extern void ObiActorBlueprint_get_usesTethers_m7A16422C66E95F3984D2A3A8F950305E9D5C2026 (void);
// 0x000002EF System.Boolean Obi.ObiActorBlueprint::IsParticleActive(System.Int32)
extern void ObiActorBlueprint_IsParticleActive_mD079A02C9E95DB8F8900FA0C1FE9A057C1F1D05A (void);
// 0x000002F0 System.Void Obi.ObiActorBlueprint::SwapWithFirstInactiveParticle(System.Int32)
extern void ObiActorBlueprint_SwapWithFirstInactiveParticle_mCBD4A74BAC682B6DB6228F3B53FCC5D90B069126 (void);
// 0x000002F1 System.Boolean Obi.ObiActorBlueprint::ActivateParticle(System.Int32)
extern void ObiActorBlueprint_ActivateParticle_mA772B1D5DA1364AA8A889F069D9F903E565A3F02 (void);
// 0x000002F2 System.Boolean Obi.ObiActorBlueprint::DeactivateParticle(System.Int32)
extern void ObiActorBlueprint_DeactivateParticle_m2EFB2F10CE118C9041593C482C6FC80F24A9C8A7 (void);
// 0x000002F3 System.Boolean Obi.ObiActorBlueprint::get_empty()
extern void ObiActorBlueprint_get_empty_m5407EABADC0A7CFE5EBBA320717C3B4609F2FDD1 (void);
// 0x000002F4 System.Void Obi.ObiActorBlueprint::RecalculateBounds()
extern void ObiActorBlueprint_RecalculateBounds_mC32E8C37E376B8342D46E8894A36161EDF37E48A (void);
// 0x000002F5 UnityEngine.Bounds Obi.ObiActorBlueprint::get_bounds()
extern void ObiActorBlueprint_get_bounds_m8F16C4CEAFB92614DEC83B9F41D565B4FE542C15 (void);
// 0x000002F6 System.Collections.Generic.IEnumerable`1<Obi.IObiConstraints> Obi.ObiActorBlueprint::GetConstraints()
extern void ObiActorBlueprint_GetConstraints_m1C48E3155094EC159203D4F23B25015FCA6F0597 (void);
// 0x000002F7 Obi.IObiConstraints Obi.ObiActorBlueprint::GetConstraintsByType(Oni_ConstraintType)
extern void ObiActorBlueprint_GetConstraintsByType_mD32BF0C8BE5453BF0F534BCC92F2BD30477E7761 (void);
// 0x000002F8 System.Int32 Obi.ObiActorBlueprint::GetParticleRuntimeIndex(System.Int32)
extern void ObiActorBlueprint_GetParticleRuntimeIndex_m6495778E4FF39D59DA86401684ABED0361E6803B (void);
// 0x000002F9 UnityEngine.Vector3 Obi.ObiActorBlueprint::GetParticlePosition(System.Int32)
extern void ObiActorBlueprint_GetParticlePosition_m4B9F2D62EEF90A610C7DC8E1B07D4525ED90A081 (void);
// 0x000002FA UnityEngine.Quaternion Obi.ObiActorBlueprint::GetParticleOrientation(System.Int32)
extern void ObiActorBlueprint_GetParticleOrientation_mE80B9B4333A1EE1670B7DAD73FD55F8C8F6F178D (void);
// 0x000002FB System.Void Obi.ObiActorBlueprint::GetParticleAnisotropy(System.Int32,UnityEngine.Vector4&,UnityEngine.Vector4&,UnityEngine.Vector4&)
extern void ObiActorBlueprint_GetParticleAnisotropy_mBFB7DD70BF52D6DEC5E08DEFC5A8F07BBFCBE6B9 (void);
// 0x000002FC System.Single Obi.ObiActorBlueprint::GetParticleMaxRadius(System.Int32)
extern void ObiActorBlueprint_GetParticleMaxRadius_m1106A97F11068907833E40301081A52749BC79F4 (void);
// 0x000002FD UnityEngine.Color Obi.ObiActorBlueprint::GetParticleColor(System.Int32)
extern void ObiActorBlueprint_GetParticleColor_mA1CA0E89A089B4CEBAFFD4EB83CBFC5EB5A6EB0D (void);
// 0x000002FE System.Void Obi.ObiActorBlueprint::GenerateImmediate()
extern void ObiActorBlueprint_GenerateImmediate_mD3FF977E24F4965AB776BF1E1DF0272F3D6CCA33 (void);
// 0x000002FF System.Collections.IEnumerator Obi.ObiActorBlueprint::Generate()
extern void ObiActorBlueprint_Generate_m14C8F36BC89354F15F0F82AE9546B5024F47FF7A (void);
// 0x00000300 Obi.ObiParticleGroup Obi.ObiActorBlueprint::InsertNewParticleGroup(System.String,System.Int32)
extern void ObiActorBlueprint_InsertNewParticleGroup_m13FF8726A0D8F315CE56ED27091B83E9FD733F01 (void);
// 0x00000301 Obi.ObiParticleGroup Obi.ObiActorBlueprint::AppendNewParticleGroup(System.String)
extern void ObiActorBlueprint_AppendNewParticleGroup_m1FA41729CF6A7830711798726512D51B97355689 (void);
// 0x00000302 System.Boolean Obi.ObiActorBlueprint::RemoveParticleGroupAt(System.Int32)
extern void ObiActorBlueprint_RemoveParticleGroupAt_mE4755BA1B826AA1AA58A63022077FA4B0BB67772 (void);
// 0x00000303 System.Boolean Obi.ObiActorBlueprint::SetParticleGroupName(System.Int32,System.String)
extern void ObiActorBlueprint_SetParticleGroupName_m7E56781B15B7C8A3357E58EF2B7C78FBFFF2B78C (void);
// 0x00000304 System.Void Obi.ObiActorBlueprint::ClearParticleGroups()
extern void ObiActorBlueprint_ClearParticleGroups_mBBF558B57D8AC4475F781081F63A6D2FB906D9A5 (void);
// 0x00000305 System.Boolean Obi.ObiActorBlueprint::IsParticleSharedInConstraint(System.Int32,System.Collections.Generic.List`1<System.Int32>,System.Boolean[])
extern void ObiActorBlueprint_IsParticleSharedInConstraint_m9F71E2E0E9B16D9CCB47B7D270391F65232D64A1 (void);
// 0x00000306 System.Boolean Obi.ObiActorBlueprint::DoesParticleShareConstraints(Obi.IObiConstraints,System.Int32,System.Collections.Generic.List`1<System.Int32>,System.Boolean[])
extern void ObiActorBlueprint_DoesParticleShareConstraints_mCCD929D6E8D050B9B2A377A3965B6F8E64538883 (void);
// 0x00000307 System.Void Obi.ObiActorBlueprint::DeactivateConstraintsWithInactiveParticles(Obi.IObiConstraints,System.Collections.Generic.List`1<System.Int32>)
extern void ObiActorBlueprint_DeactivateConstraintsWithInactiveParticles_m2F8B411DA9FE9FCDC8116158247E0E83EEA513E1 (void);
// 0x00000308 System.Void Obi.ObiActorBlueprint::ParticlesSwappedInGroups(System.Int32,System.Int32)
extern void ObiActorBlueprint_ParticlesSwappedInGroups_m9104E8E53B4D76CFFD62EF8277E5313A73867EE5 (void);
// 0x00000309 System.Void Obi.ObiActorBlueprint::RemoveSelectedParticles(System.Boolean[]&,System.Boolean)
extern void ObiActorBlueprint_RemoveSelectedParticles_m1FD86679B5F6C5DEDED32F79658292F13566C63F (void);
// 0x0000030A System.Void Obi.ObiActorBlueprint::RestoreRemovedParticles()
extern void ObiActorBlueprint_RestoreRemovedParticles_m6968118C8E330062E76B6F79155E07B86C091400 (void);
// 0x0000030B System.Void Obi.ObiActorBlueprint::GenerateTethers(System.Boolean[])
extern void ObiActorBlueprint_GenerateTethers_mE0CECBEF8A33A7E44D7EFD24A87697C45209B668 (void);
// 0x0000030C System.Void Obi.ObiActorBlueprint::ClearTethers()
extern void ObiActorBlueprint_ClearTethers_mA11CB91D97A72D5311C67FCC335ACA3900020EEB (void);
// 0x0000030D System.Collections.IEnumerator Obi.ObiActorBlueprint::Initialize()
// 0x0000030E System.Void Obi.ObiActorBlueprint::.ctor()
extern void ObiActorBlueprint__ctor_m9D95F18391AC084CE03CF008E1D47864DF75A636 (void);
// 0x0000030F System.Void Obi.ObiActorBlueprint_BlueprintCallback::.ctor(System.Object,System.IntPtr)
extern void BlueprintCallback__ctor_m75F65AE60505CE5651A0F020168360371AD0D0EE (void);
// 0x00000310 System.Void Obi.ObiActorBlueprint_BlueprintCallback::Invoke(Obi.ObiActorBlueprint)
extern void BlueprintCallback_Invoke_m0F0A317733BA923A00DCF82C10E2AF745667377A (void);
// 0x00000311 System.IAsyncResult Obi.ObiActorBlueprint_BlueprintCallback::BeginInvoke(Obi.ObiActorBlueprint,System.AsyncCallback,System.Object)
extern void BlueprintCallback_BeginInvoke_mD8A830F18B9F7B76E81DB436AF1462092202B3F3 (void);
// 0x00000312 System.Void Obi.ObiActorBlueprint_BlueprintCallback::EndInvoke(System.IAsyncResult)
extern void BlueprintCallback_EndInvoke_m35D1EBBFF55BAC420142369AEB4B24D44D031B3F (void);
// 0x00000313 System.Void Obi.ObiActorBlueprint_<GetConstraints>d__50::.ctor(System.Int32)
extern void U3CGetConstraintsU3Ed__50__ctor_m463D73E183DDE1A5C71E6E3CC821D0E7F30E347C (void);
// 0x00000314 System.Void Obi.ObiActorBlueprint_<GetConstraints>d__50::System.IDisposable.Dispose()
extern void U3CGetConstraintsU3Ed__50_System_IDisposable_Dispose_m1C1B68AF4C49065E226E78E8D46FD68A406FC4DB (void);
// 0x00000315 System.Boolean Obi.ObiActorBlueprint_<GetConstraints>d__50::MoveNext()
extern void U3CGetConstraintsU3Ed__50_MoveNext_m440BD75AC003FACCE06308279346A5432EDC9BFC (void);
// 0x00000316 Obi.IObiConstraints Obi.ObiActorBlueprint_<GetConstraints>d__50::System.Collections.Generic.IEnumerator<Obi.IObiConstraints>.get_Current()
extern void U3CGetConstraintsU3Ed__50_System_Collections_Generic_IEnumeratorU3CObi_IObiConstraintsU3E_get_Current_m603F544C865B5A382E4D45071F9EA103E35296E2 (void);
// 0x00000317 System.Void Obi.ObiActorBlueprint_<GetConstraints>d__50::System.Collections.IEnumerator.Reset()
extern void U3CGetConstraintsU3Ed__50_System_Collections_IEnumerator_Reset_m8E40706068668910D9F7D8A1EACB85F8E6FFF77A (void);
// 0x00000318 System.Object Obi.ObiActorBlueprint_<GetConstraints>d__50::System.Collections.IEnumerator.get_Current()
extern void U3CGetConstraintsU3Ed__50_System_Collections_IEnumerator_get_Current_mD7F6DAA1BCB1AB519151F38ACED86E62BB422730 (void);
// 0x00000319 System.Collections.Generic.IEnumerator`1<Obi.IObiConstraints> Obi.ObiActorBlueprint_<GetConstraints>d__50::System.Collections.Generic.IEnumerable<Obi.IObiConstraints>.GetEnumerator()
extern void U3CGetConstraintsU3Ed__50_System_Collections_Generic_IEnumerableU3CObi_IObiConstraintsU3E_GetEnumerator_mFFFD55BE544D6CB567D145A4DCB2564DD48B4CDF (void);
// 0x0000031A System.Collections.IEnumerator Obi.ObiActorBlueprint_<GetConstraints>d__50::System.Collections.IEnumerable.GetEnumerator()
extern void U3CGetConstraintsU3Ed__50_System_Collections_IEnumerable_GetEnumerator_m3284B5FE565ECE74AF7F1FBE65BCD30C41CADA8D (void);
// 0x0000031B System.Void Obi.ObiActorBlueprint_<Generate>d__59::.ctor(System.Int32)
extern void U3CGenerateU3Ed__59__ctor_mD0933DCC8582C89719A5869AA40F1D96BC46A2A1 (void);
// 0x0000031C System.Void Obi.ObiActorBlueprint_<Generate>d__59::System.IDisposable.Dispose()
extern void U3CGenerateU3Ed__59_System_IDisposable_Dispose_m4C2610F3D57B4EB14EE56E2E116536B5A23C8B22 (void);
// 0x0000031D System.Boolean Obi.ObiActorBlueprint_<Generate>d__59::MoveNext()
extern void U3CGenerateU3Ed__59_MoveNext_m1B248A89761AD00C954EAEB25335090B6378CB9E (void);
// 0x0000031E System.Object Obi.ObiActorBlueprint_<Generate>d__59::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CGenerateU3Ed__59_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mB7A53073B04207358D27C5F279097C3AECBDC48E (void);
// 0x0000031F System.Void Obi.ObiActorBlueprint_<Generate>d__59::System.Collections.IEnumerator.Reset()
extern void U3CGenerateU3Ed__59_System_Collections_IEnumerator_Reset_m326C85F4CC091D9CEBD26372E5491D58808FECB6 (void);
// 0x00000320 System.Object Obi.ObiActorBlueprint_<Generate>d__59::System.Collections.IEnumerator.get_Current()
extern void U3CGenerateU3Ed__59_System_Collections_IEnumerator_get_Current_mB78D5553C5C65C889A8A4E2F6FCCADA45472D734 (void);
// 0x00000321 System.Void Obi.ObiMeshBasedActorBlueprint::.ctor()
extern void ObiMeshBasedActorBlueprint__ctor_m68ACFAD2ACE6D36D748D434E2BD130B742B17BE8 (void);
// 0x00000322 Obi.ObiActorBlueprint Obi.ObiParticleGroup::get_blueprint()
extern void ObiParticleGroup_get_blueprint_m7B969093BC47FD5B58C6D6839D8F9E3BDA3119FF (void);
// 0x00000323 System.Void Obi.ObiParticleGroup::SetSourceBlueprint(Obi.ObiActorBlueprint)
extern void ObiParticleGroup_SetSourceBlueprint_m83016ECF67DCB8BDF873C154BB4A0EE7A8091011 (void);
// 0x00000324 System.Int32 Obi.ObiParticleGroup::get_Count()
extern void ObiParticleGroup_get_Count_m98490363FF19DE1FFEBC361D7302AEF05B6F21B4 (void);
// 0x00000325 System.Boolean Obi.ObiParticleGroup::ContainsParticle(System.Int32)
extern void ObiParticleGroup_ContainsParticle_mFF1BABB2B3D4D3CD4AE1E5B44C3DC83765EB11AC (void);
// 0x00000326 System.Void Obi.ObiParticleGroup::.ctor()
extern void ObiParticleGroup__ctor_mD6FDCA405D8CBEB22044CFD3635E3202AF1A45A2 (void);
// 0x00000327 System.Void Obi.ObiBoxShapeTracker2D::.ctor(Obi.ObiCollider2D,UnityEngine.BoxCollider2D)
extern void ObiBoxShapeTracker2D__ctor_m628217C8133AD0BA22DE8815770C03CD31236C8F (void);
// 0x00000328 System.Boolean Obi.ObiBoxShapeTracker2D::UpdateIfNeeded()
extern void ObiBoxShapeTracker2D_UpdateIfNeeded_m7CF0C0561350D4A21F8FC768C7158E4BAE328434 (void);
// 0x00000329 System.Void Obi.ObiCapsuleShapeTracker2D::.ctor(Obi.ObiCollider2D,UnityEngine.CapsuleCollider2D)
extern void ObiCapsuleShapeTracker2D__ctor_m9F0647AFDA77ACDB5B00DDA8F614F2CD3F61F464 (void);
// 0x0000032A System.Boolean Obi.ObiCapsuleShapeTracker2D::UpdateIfNeeded()
extern void ObiCapsuleShapeTracker2D_UpdateIfNeeded_m65EF7C477B78DF113F5C43446E266DA9790B333A (void);
// 0x0000032B System.Void Obi.ObiCircleShapeTracker2D::.ctor(Obi.ObiCollider2D,UnityEngine.CircleCollider2D)
extern void ObiCircleShapeTracker2D__ctor_m91428FB089519F84073171B1EF2AF27FA7029F13 (void);
// 0x0000032C System.Boolean Obi.ObiCircleShapeTracker2D::UpdateIfNeeded()
extern void ObiCircleShapeTracker2D_UpdateIfNeeded_m855CD60D865E47BD776A38CE2F44554BD5B69E49 (void);
// 0x0000032D System.Void Obi.ObiEdgeShapeTracker2D::.ctor(Obi.ObiCollider2D,UnityEngine.EdgeCollider2D)
extern void ObiEdgeShapeTracker2D__ctor_m9CF5E0F349BC73573202F01E32482578DE1C1A10 (void);
// 0x0000032E System.Void Obi.ObiEdgeShapeTracker2D::UpdateEdgeData()
extern void ObiEdgeShapeTracker2D_UpdateEdgeData_m7208F5B1FF03775D5A425836047FB54E9BB747C1 (void);
// 0x0000032F System.Boolean Obi.ObiEdgeShapeTracker2D::UpdateIfNeeded()
extern void ObiEdgeShapeTracker2D_UpdateIfNeeded_m380635280878C5D86F90898E11CAD1664E3E4BDC (void);
// 0x00000330 System.Void Obi.ObiEdgeShapeTracker2D::Destroy()
extern void ObiEdgeShapeTracker2D_Destroy_m01DDDC54A11D898BAABA7391098DD4D2458CA946 (void);
// 0x00000331 System.Void Obi.ObiBoxShapeTracker::.ctor(Obi.ObiCollider,UnityEngine.BoxCollider)
extern void ObiBoxShapeTracker__ctor_mE75CAF1F86BB53D6AEF98085C09D17E66DEEF582 (void);
// 0x00000332 System.Boolean Obi.ObiBoxShapeTracker::UpdateIfNeeded()
extern void ObiBoxShapeTracker_UpdateIfNeeded_mF819B8E4C30C6AFAF29A4117798FDF1316EA834A (void);
// 0x00000333 System.Void Obi.ObiCapsuleShapeTracker::.ctor(Obi.ObiCollider,UnityEngine.CapsuleCollider)
extern void ObiCapsuleShapeTracker__ctor_mBE6839D0297E242539ABACA7E8D10CC7D1FF90BD (void);
// 0x00000334 System.Boolean Obi.ObiCapsuleShapeTracker::UpdateIfNeeded()
extern void ObiCapsuleShapeTracker_UpdateIfNeeded_mE44AC47537FB4437697D85609FB76AC306E29C1A (void);
// 0x00000335 System.Void Obi.ObiCharacterControllerShapeTracker::.ctor(Obi.ObiCollider,UnityEngine.CharacterController)
extern void ObiCharacterControllerShapeTracker__ctor_m58178DB03B830C4137C4E297650BAB50EFD3625E (void);
// 0x00000336 System.Boolean Obi.ObiCharacterControllerShapeTracker::UpdateIfNeeded()
extern void ObiCharacterControllerShapeTracker_UpdateIfNeeded_m37B806A39DB2EEF9B54A5B5F191B7F9C7602E517 (void);
// 0x00000337 System.Void Obi.ObiDistanceFieldShapeTracker::.ctor(Obi.ObiCollider,UnityEngine.Component,Obi.ObiDistanceField)
extern void ObiDistanceFieldShapeTracker__ctor_mA5B4583A9547EB0FE0C666EBB88E671DEC7F9035 (void);
// 0x00000338 System.Void Obi.ObiDistanceFieldShapeTracker::UpdateDistanceFieldData()
extern void ObiDistanceFieldShapeTracker_UpdateDistanceFieldData_mA701C6AE3562FD84B9C2C56A60A80A88D8B961E7 (void);
// 0x00000339 System.Boolean Obi.ObiDistanceFieldShapeTracker::UpdateIfNeeded()
extern void ObiDistanceFieldShapeTracker_UpdateIfNeeded_mA4E6477716FF64D462625E6D8F4898A7E47BD384 (void);
// 0x0000033A System.Void Obi.ObiDistanceFieldShapeTracker::Destroy()
extern void ObiDistanceFieldShapeTracker_Destroy_mF2D78CB9BA87BFF2D01E82CA3796B3E975B387C3 (void);
// 0x0000033B System.Void Obi.ObiMeshShapeTracker::.ctor(Obi.ObiCollider,UnityEngine.MeshCollider)
extern void ObiMeshShapeTracker__ctor_m08CD689CF8203745944B75624F6F10B9B5991B41 (void);
// 0x0000033C System.Void Obi.ObiMeshShapeTracker::UpdateMeshData()
extern void ObiMeshShapeTracker_UpdateMeshData_m3FE402BFC2EA1B1AEF97C10345A781D1F1453374 (void);
// 0x0000033D System.Boolean Obi.ObiMeshShapeTracker::UpdateIfNeeded()
extern void ObiMeshShapeTracker_UpdateIfNeeded_m12F1895305D96BA383B3C6D04D0960CDE65BB9DD (void);
// 0x0000033E System.Void Obi.ObiMeshShapeTracker::Destroy()
extern void ObiMeshShapeTracker_Destroy_mF175565DFFD64344104928752D528653AEEF4C1D (void);
// 0x0000033F System.Void Obi.ObiShapeTracker::Destroy()
extern void ObiShapeTracker_Destroy_m0FF375956D9FB5363A7595568F62FC49288D102A (void);
// 0x00000340 System.Boolean Obi.ObiShapeTracker::UpdateIfNeeded()
// 0x00000341 System.Void Obi.ObiShapeTracker::.ctor()
extern void ObiShapeTracker__ctor_mD3C85F1D395CDC2FA8F0FADBF02F5035493C1A7C (void);
// 0x00000342 System.Void Obi.ObiSphereShapeTracker::.ctor(Obi.ObiCollider,UnityEngine.SphereCollider)
extern void ObiSphereShapeTracker__ctor_m1F91597098DFDE9D3D331BB46F9885EF84816FCC (void);
// 0x00000343 System.Boolean Obi.ObiSphereShapeTracker::UpdateIfNeeded()
extern void ObiSphereShapeTracker_UpdateIfNeeded_m956A2557E0849CFD2C667CED772595BB3DB2517F (void);
// 0x00000344 System.Void Obi.ObiTerrainShapeTracker::.ctor(Obi.ObiCollider,UnityEngine.TerrainCollider)
extern void ObiTerrainShapeTracker__ctor_m7BEE7A899503A6A8D1B542BB48B2902966AB22B6 (void);
// 0x00000345 System.Void Obi.ObiTerrainShapeTracker::UpdateHeightData()
extern void ObiTerrainShapeTracker_UpdateHeightData_mF979C90B8BF4BAA1D77BB55829060BE1D02DB1FB (void);
// 0x00000346 System.Boolean Obi.ObiTerrainShapeTracker::UpdateIfNeeded()
extern void ObiTerrainShapeTracker_UpdateIfNeeded_mF4287DBBCE367005E4F1EF1342AB50882CDBE460 (void);
// 0x00000347 System.Void Obi.ObiTerrainShapeTracker::Destroy()
extern void ObiTerrainShapeTracker_Destroy_m778990BC8F8E80F2EAD34786597F7803DA9E55EE (void);
// 0x00000348 System.Void Obi.ObiCollider::set_sourceCollider(UnityEngine.Collider)
extern void ObiCollider_set_sourceCollider_mE74FCFF39705097FEAD7580CD74ADA13071B3CB1 (void);
// 0x00000349 UnityEngine.Collider Obi.ObiCollider::get_sourceCollider()
extern void ObiCollider_get_sourceCollider_m9128E7F1E524ACFDD350E7347570A6253DFDB19D (void);
// 0x0000034A System.Void Obi.ObiCollider::set_distanceField(Obi.ObiDistanceField)
extern void ObiCollider_set_distanceField_m1E7758FFA96690595BB90B1BEB48B7AD958EB53F (void);
// 0x0000034B Obi.ObiDistanceField Obi.ObiCollider::get_distanceField()
extern void ObiCollider_get_distanceField_mC9798980E3932FBA4649A14AB780CA819D06B050 (void);
// 0x0000034C System.Void Obi.ObiCollider::CreateTracker()
extern void ObiCollider_CreateTracker_mD3862A5A96ACE781C43578C71C7F3B34597B72B5 (void);
// 0x0000034D UnityEngine.Component Obi.ObiCollider::GetUnityCollider(System.Boolean&)
extern void ObiCollider_GetUnityCollider_m8F9125F3CE4C77B54A293FCB58A506F9003F1645 (void);
// 0x0000034E System.Void Obi.ObiCollider::FindSourceCollider()
extern void ObiCollider_FindSourceCollider_m637BA0D9C7416963F07A6C91C46898E63A2AA7E6 (void);
// 0x0000034F System.Void Obi.ObiCollider::.ctor()
extern void ObiCollider__ctor_mC40B217031806CE6CC171DB8519F2BDC13237937 (void);
// 0x00000350 System.Void Obi.ObiCollider2D::set_SourceCollider(UnityEngine.Collider2D)
extern void ObiCollider2D_set_SourceCollider_m1018D5A5D84BEE9BFF223E7C96DFEF6391A61025 (void);
// 0x00000351 UnityEngine.Collider2D Obi.ObiCollider2D::get_SourceCollider()
extern void ObiCollider2D_get_SourceCollider_m0F0FA6FE50C824AA1D508B0624CFC78BC5CB1324 (void);
// 0x00000352 System.Void Obi.ObiCollider2D::CreateTracker()
extern void ObiCollider2D_CreateTracker_mF85B5C235CC7FD0AA628697696F24E75B0677D95 (void);
// 0x00000353 UnityEngine.Component Obi.ObiCollider2D::GetUnityCollider(System.Boolean&)
extern void ObiCollider2D_GetUnityCollider_mC7022D7CE3DD5DAFC483FE44489C1826C6F719A9 (void);
// 0x00000354 System.Void Obi.ObiCollider2D::FindSourceCollider()
extern void ObiCollider2D_FindSourceCollider_m1C3542EAB959FFB8DD376825B89F1FCF6EF5B71A (void);
// 0x00000355 System.Void Obi.ObiCollider2D::.ctor()
extern void ObiCollider2D__ctor_m1D8E050EE358DD5E91FBCBC06CEEA010D1096877 (void);
// 0x00000356 System.Void Obi.ObiColliderBase::set_CollisionMaterial(Obi.ObiCollisionMaterial)
extern void ObiColliderBase_set_CollisionMaterial_m79AC8EC4C2B24D56D2EEB8E7E3EC8758D71DBCEF (void);
// 0x00000357 Obi.ObiCollisionMaterial Obi.ObiColliderBase::get_CollisionMaterial()
extern void ObiColliderBase_get_CollisionMaterial_m0DF69ECB8E1D3B265A93C3411558938452320DB8 (void);
// 0x00000358 System.Void Obi.ObiColliderBase::set_Filter(System.Int32)
extern void ObiColliderBase_set_Filter_m7EFACC9D0DDC53860DD8EC7711CA2B537861B143 (void);
// 0x00000359 System.Int32 Obi.ObiColliderBase::get_Filter()
extern void ObiColliderBase_get_Filter_mC113E8E2C6A721BDC36A1380CDA7B1274877FA9F (void);
// 0x0000035A System.Void Obi.ObiColliderBase::set_Thickness(System.Single)
extern void ObiColliderBase_set_Thickness_m8B643679A1B8F3502E0983283BFAFC788F59FED7 (void);
// 0x0000035B System.Single Obi.ObiColliderBase::get_Thickness()
extern void ObiColliderBase_get_Thickness_m39EFFE2661BFC77996AFE413C11C9842813B0CCD (void);
// 0x0000035C Obi.ObiShapeTracker Obi.ObiColliderBase::get_Tracker()
extern void ObiColliderBase_get_Tracker_mC248A17400AE5158EBE13D85059023F2777EF96E (void);
// 0x0000035D Obi.ObiColliderHandle Obi.ObiColliderBase::get_Handle()
extern void ObiColliderBase_get_Handle_mC54EFB3D8F0D65205AECE01C1523F9684610FDF9 (void);
// 0x0000035E System.IntPtr Obi.ObiColliderBase::get_OniCollider()
extern void ObiColliderBase_get_OniCollider_m1A453A14EEE1A71AD174EA6FD490CBF11FB13516 (void);
// 0x0000035F Obi.ObiRigidbodyBase Obi.ObiColliderBase::get_Rigidbody()
extern void ObiColliderBase_get_Rigidbody_mFE29C0B5E72DE47B2F8CECC10D691E3CB1113260 (void);
// 0x00000360 System.Void Obi.ObiColliderBase::CreateTracker()
// 0x00000361 UnityEngine.Component Obi.ObiColliderBase::GetUnityCollider(System.Boolean&)
// 0x00000362 System.Void Obi.ObiColliderBase::FindSourceCollider()
// 0x00000363 System.Void Obi.ObiColliderBase::CreateRigidbody()
extern void ObiColliderBase_CreateRigidbody_m28447722765BD28A1F74FD197E44CD73585CFA55 (void);
// 0x00000364 System.Void Obi.ObiColliderBase::OnTransformParentChanged()
extern void ObiColliderBase_OnTransformParentChanged_mB24B89C5A1D09A5CDE05559DDDE3AE584CB45385 (void);
// 0x00000365 System.Void Obi.ObiColliderBase::AddCollider()
extern void ObiColliderBase_AddCollider_mAE172D1DDA35D52A9D36D68E68CB739157F34870 (void);
// 0x00000366 System.Void Obi.ObiColliderBase::RemoveCollider()
extern void ObiColliderBase_RemoveCollider_m95F3C0AD19C848FFF82DF5DB8584D5E949F808DC (void);
// 0x00000367 System.Void Obi.ObiColliderBase::UpdateIfNeeded()
extern void ObiColliderBase_UpdateIfNeeded_mF9F82931F7D1F7703B77125BB3612E1781EF4622 (void);
// 0x00000368 System.Void Obi.ObiColliderBase::OnEnable()
extern void ObiColliderBase_OnEnable_m9D06C6503D3AA87D2ED910B831C6927655449D5C (void);
// 0x00000369 System.Void Obi.ObiColliderBase::OnDisable()
extern void ObiColliderBase_OnDisable_mDCA28DAF071BB2F8AB35E5A290B1935BB47F320F (void);
// 0x0000036A System.Void Obi.ObiColliderBase::.ctor()
extern void ObiColliderBase__ctor_m0425A5B3CFADDE3908C4CDE7564045D2E49156C6 (void);
// 0x0000036B System.Boolean Obi.ObiResourceHandle`1::get_isValid()
// 0x0000036C System.Void Obi.ObiResourceHandle`1::Invalidate()
// 0x0000036D System.Void Obi.ObiResourceHandle`1::Reference()
// 0x0000036E System.Boolean Obi.ObiResourceHandle`1::Dereference()
// 0x0000036F System.Void Obi.ObiResourceHandle`1::.ctor(System.Int32)
// 0x00000370 System.Void Obi.ObiColliderHandle::.ctor(System.Int32)
extern void ObiColliderHandle__ctor_m928271CA0B5407C025BBCC1A4C1B41A772842B98 (void);
// 0x00000371 System.Void Obi.ObiCollisionMaterialHandle::.ctor(System.Int32)
extern void ObiCollisionMaterialHandle__ctor_mF9EF7E13E41C1CFA35A66984FEAE0EA7A6E01622 (void);
// 0x00000372 System.Void Obi.ObiRigidbodyHandle::.ctor(System.Int32)
extern void ObiRigidbodyHandle__ctor_m7A187DF1938668726DB0353C6DF5A404B9661FDB (void);
// 0x00000373 Obi.ObiColliderWorld Obi.ObiColliderWorld::GetInstance()
extern void ObiColliderWorld_GetInstance_mDCD3CE9448DD243AF9314FC3AE6EFF25A0A9D804 (void);
// 0x00000374 System.Void Obi.ObiColliderWorld::Initialize()
extern void ObiColliderWorld_Initialize_mA686CF0330B087A58794A38B02A5C474C0ACE80D (void);
// 0x00000375 System.Void Obi.ObiColliderWorld::Destroy()
extern void ObiColliderWorld_Destroy_mB2941E38388F04B017C98D209866BF6234D85C66 (void);
// 0x00000376 System.Void Obi.ObiColliderWorld::DestroyIfUnused()
extern void ObiColliderWorld_DestroyIfUnused_mF7FB902FE93CE094938D57DC0048249F810AFAB0 (void);
// 0x00000377 System.Void Obi.ObiColliderWorld::RegisterImplementation(Obi.IColliderWorldImpl)
extern void ObiColliderWorld_RegisterImplementation_m04E6D07B4FE9DB89CB602DDE1177100646AB5F80 (void);
// 0x00000378 System.Void Obi.ObiColliderWorld::UnregisterImplementation(Obi.IColliderWorldImpl)
extern void ObiColliderWorld_UnregisterImplementation_m1FB0CC2F87786339836A3FE23662CE7E87AFD313 (void);
// 0x00000379 Obi.ObiColliderHandle Obi.ObiColliderWorld::CreateCollider()
extern void ObiColliderWorld_CreateCollider_m209298226F546EF09F80E9DC3C9CEDB9EA6A3ECF (void);
// 0x0000037A Obi.ObiRigidbodyHandle Obi.ObiColliderWorld::CreateRigidbody()
extern void ObiColliderWorld_CreateRigidbody_m1A6A020A8B5E9F2333767813122FAD64F13E58C7 (void);
// 0x0000037B Obi.ObiCollisionMaterialHandle Obi.ObiColliderWorld::CreateCollisionMaterial()
extern void ObiColliderWorld_CreateCollisionMaterial_m88809726D6F5777CA5CA979375202E079FBA523F (void);
// 0x0000037C Obi.ObiTriangleMeshHandle Obi.ObiColliderWorld::GetOrCreateTriangleMesh(UnityEngine.Mesh)
extern void ObiColliderWorld_GetOrCreateTriangleMesh_m68BA906C938414D9FA39EF9434D16EDD5BC6B008 (void);
// 0x0000037D System.Void Obi.ObiColliderWorld::DestroyTriangleMesh(Obi.ObiTriangleMeshHandle)
extern void ObiColliderWorld_DestroyTriangleMesh_m0BAE4F6D11B77B8B13C2D4C1DE5A6140E76E582C (void);
// 0x0000037E Obi.ObiEdgeMeshHandle Obi.ObiColliderWorld::GetOrCreateEdgeMesh(UnityEngine.EdgeCollider2D)
extern void ObiColliderWorld_GetOrCreateEdgeMesh_mD3188273479E4DC678356643F456C0AA97BFBD1B (void);
// 0x0000037F System.Void Obi.ObiColliderWorld::DestroyEdgeMesh(Obi.ObiEdgeMeshHandle)
extern void ObiColliderWorld_DestroyEdgeMesh_mC35DD757DCFE047FCE86C3190EEFA0B19A891306 (void);
// 0x00000380 Obi.ObiDistanceFieldHandle Obi.ObiColliderWorld::GetOrCreateDistanceField(Obi.ObiDistanceField)
extern void ObiColliderWorld_GetOrCreateDistanceField_mD90630455B72C5BAD3E94DAE4F3FE4E979E2C431 (void);
// 0x00000381 System.Void Obi.ObiColliderWorld::DestroyDistanceField(Obi.ObiDistanceFieldHandle)
extern void ObiColliderWorld_DestroyDistanceField_mB645511AF520AEF6AB93B8EE03F0A2A7D62ECEE8 (void);
// 0x00000382 Obi.ObiHeightFieldHandle Obi.ObiColliderWorld::GetOrCreateHeightField(UnityEngine.TerrainData)
extern void ObiColliderWorld_GetOrCreateHeightField_m67768E9620255D9407B6D664A21B5F46A06DA9DD (void);
// 0x00000383 System.Void Obi.ObiColliderWorld::DestroyHeightField(Obi.ObiHeightFieldHandle)
extern void ObiColliderWorld_DestroyHeightField_mEDED727FD4CD60E71555E1F0B7223CF0C2F060D7 (void);
// 0x00000384 System.Void Obi.ObiColliderWorld::DestroyCollider(Obi.ObiColliderHandle)
extern void ObiColliderWorld_DestroyCollider_m834E6E9C211C47C4746C0658D072D11488444C7D (void);
// 0x00000385 System.Void Obi.ObiColliderWorld::DestroyRigidbody(Obi.ObiRigidbodyHandle)
extern void ObiColliderWorld_DestroyRigidbody_m06DB0A63D4471A043E38A1BFEC429E693E36E82B (void);
// 0x00000386 System.Void Obi.ObiColliderWorld::DestroyCollisionMaterial(Obi.ObiCollisionMaterialHandle)
extern void ObiColliderWorld_DestroyCollisionMaterial_m95080D33292635B836E68FDF7DBD877FF51A3302 (void);
// 0x00000387 System.Void Obi.ObiColliderWorld::UpdateColliders()
extern void ObiColliderWorld_UpdateColliders_mFE5C2781107A2BF876523BC7D1A371930FA231DA (void);
// 0x00000388 System.Void Obi.ObiColliderWorld::UpdateRigidbodies(System.Collections.Generic.List`1<Obi.ObiSolver>,System.Single)
extern void ObiColliderWorld_UpdateRigidbodies_m8561FC0652FF0142400D2886A02C9EFAEB685819 (void);
// 0x00000389 System.Void Obi.ObiColliderWorld::UpdateWorld(System.Single)
extern void ObiColliderWorld_UpdateWorld_m864B8D4C74E8309F9FA29064D39B1A37FCC9538D (void);
// 0x0000038A System.Void Obi.ObiColliderWorld::UpdateRigidbodyVelocities(System.Collections.Generic.List`1<Obi.ObiSolver>)
extern void ObiColliderWorld_UpdateRigidbodyVelocities_m98240C979197C4339F335E642B3DF480F7A281E3 (void);
// 0x0000038B System.Void Obi.ObiColliderWorld::.ctor()
extern void ObiColliderWorld__ctor_mB72724E3C4F8ABB4A7845EA540DF7FE796E1E971 (void);
// 0x0000038C Obi.ObiCollisionMaterialHandle Obi.ObiCollisionMaterial::get_handle()
extern void ObiCollisionMaterial_get_handle_m594CB055313A49F8543490E78B1F48E7C21C7462 (void);
// 0x0000038D System.Void Obi.ObiCollisionMaterial::OnEnable()
extern void ObiCollisionMaterial_OnEnable_m8FA0F5FEF8BDA6E093C8A903CD09B45F107B87F3 (void);
// 0x0000038E System.Void Obi.ObiCollisionMaterial::OnDisable()
extern void ObiCollisionMaterial_OnDisable_m835688D4697C04CFE7B9F09ECBA5E0DD0201EE4E (void);
// 0x0000038F System.Void Obi.ObiCollisionMaterial::OnValidate()
extern void ObiCollisionMaterial_OnValidate_m39E140FEFC698F6790902D5698BCCDAA4C037250 (void);
// 0x00000390 System.Void Obi.ObiCollisionMaterial::UpdateMaterial()
extern void ObiCollisionMaterial_UpdateMaterial_m01C9A16AD952A5AC4E018F035CD98AD7C3F7D6F2 (void);
// 0x00000391 System.Void Obi.ObiCollisionMaterial::CreateMaterialIfNeeded()
extern void ObiCollisionMaterial_CreateMaterialIfNeeded_m7B6E786F93918CFCBCAFE79D73C6D39DB7D2E467 (void);
// 0x00000392 System.Void Obi.ObiCollisionMaterial::.ctor()
extern void ObiCollisionMaterial__ctor_m72400798714AEFF19085712777080ABFBB13BAC6 (void);
// 0x00000393 System.Boolean Obi.ObiDistanceField::get_Initialized()
extern void ObiDistanceField_get_Initialized_mAABFFF3B58AB387E5CB5FB8C1AB746B0247F983C (void);
// 0x00000394 UnityEngine.Bounds Obi.ObiDistanceField::get_FieldBounds()
extern void ObiDistanceField_get_FieldBounds_m9863DC2138A3DBE77E04380C54236345A1133CBD (void);
// 0x00000395 System.Single Obi.ObiDistanceField::get_EffectiveSampleSize()
extern void ObiDistanceField_get_EffectiveSampleSize_mB9F3DB29086DB4AD2249B28C1C8C888484833AAD (void);
// 0x00000396 System.Void Obi.ObiDistanceField::set_InputMesh(UnityEngine.Mesh)
extern void ObiDistanceField_set_InputMesh_mD2BFC4B7BAE4B8EBEFF52AC8E89CF319F03D3780 (void);
// 0x00000397 UnityEngine.Mesh Obi.ObiDistanceField::get_InputMesh()
extern void ObiDistanceField_get_InputMesh_m9B9ABB7CAE379468D3FACDBD886581F843FA22B5 (void);
// 0x00000398 System.Void Obi.ObiDistanceField::Reset()
extern void ObiDistanceField_Reset_m00EC5397C61DF8D42A0E2201EBE65C30AB391DF8 (void);
// 0x00000399 System.Collections.IEnumerator Obi.ObiDistanceField::Generate()
extern void ObiDistanceField_Generate_m2FF083D72E253BE27D9DFD07C8816B449F778266 (void);
// 0x0000039A UnityEngine.Texture3D Obi.ObiDistanceField::GetVolumeTexture(System.Int32)
extern void ObiDistanceField_GetVolumeTexture_m3C63485A3F2BB3E0D84E496DFD190836DD7C9A64 (void);
// 0x0000039B System.Void Obi.ObiDistanceField::.ctor()
extern void ObiDistanceField__ctor_m79451FE12A75F2ED6B511F1DFC382E6493E09DB2 (void);
// 0x0000039C System.Void Obi.ObiDistanceField_<Generate>d__16::.ctor(System.Int32)
extern void U3CGenerateU3Ed__16__ctor_m926CEA2198C2B31432AD7B0C629E0026FD9135A8 (void);
// 0x0000039D System.Void Obi.ObiDistanceField_<Generate>d__16::System.IDisposable.Dispose()
extern void U3CGenerateU3Ed__16_System_IDisposable_Dispose_mE8B29A18143A848E1C4C46A4D41CE2EE66999F73 (void);
// 0x0000039E System.Boolean Obi.ObiDistanceField_<Generate>d__16::MoveNext()
extern void U3CGenerateU3Ed__16_MoveNext_mE09C7584F62ED7F884B6289D784C5474BAB6648D (void);
// 0x0000039F System.Object Obi.ObiDistanceField_<Generate>d__16::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CGenerateU3Ed__16_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m39320DBD9BADA847C3DC1B06385734720B3ED5D1 (void);
// 0x000003A0 System.Void Obi.ObiDistanceField_<Generate>d__16::System.Collections.IEnumerator.Reset()
extern void U3CGenerateU3Ed__16_System_Collections_IEnumerator_Reset_m94171808CA1CAE76B9F0511FE6570131CD857EBB (void);
// 0x000003A1 System.Object Obi.ObiDistanceField_<Generate>d__16::System.Collections.IEnumerator.get_Current()
extern void U3CGenerateU3Ed__16_System_Collections_IEnumerator_get_Current_mE403FD60E16DACEE30BE06E73A882BBF823618FC (void);
// 0x000003A2 System.Void Obi.ObiDistanceFieldHandle::.ctor(Obi.ObiDistanceField,System.Int32)
extern void ObiDistanceFieldHandle__ctor_m6E0E72314F9B3CD649F6E52743A19A05BDD53486 (void);
// 0x000003A3 System.Void Obi.DistanceFieldHeader::.ctor(System.Int32,System.Int32)
extern void DistanceFieldHeader__ctor_mD9023657AE562DC6E93CAEBAEA87D8BBFBE3CAC0_AdjustorThunk (void);
// 0x000003A4 System.Void Obi.ObiDistanceFieldContainer::.ctor()
extern void ObiDistanceFieldContainer__ctor_m3770E97A1E94FE7F79D6C4476A4B5538615321FC (void);
// 0x000003A5 Obi.ObiDistanceFieldHandle Obi.ObiDistanceFieldContainer::GetOrCreateDistanceField(Obi.ObiDistanceField)
extern void ObiDistanceFieldContainer_GetOrCreateDistanceField_mACCA0F4E84E3D53991F18FEC18CCC8230451C913 (void);
// 0x000003A6 System.Void Obi.ObiDistanceFieldContainer::DestroyDistanceField(Obi.ObiDistanceFieldHandle)
extern void ObiDistanceFieldContainer_DestroyDistanceField_m85914D4DE23293E844CC8082A6B7B4F427C8505B (void);
// 0x000003A7 System.Void Obi.ObiDistanceFieldContainer::Dispose()
extern void ObiDistanceFieldContainer_Dispose_m7A92A1441B9CB196E1D25BF4587B01FDFF2CA2A8 (void);
// 0x000003A8 System.Void Obi.Edge::.ctor(System.Int32,System.Int32,UnityEngine.Vector2,UnityEngine.Vector2)
extern void Edge__ctor_m58FEEBE96D412AFED6635E824686137251119088_AdjustorThunk (void);
// 0x000003A9 Obi.Aabb Obi.Edge::GetBounds()
extern void Edge_GetBounds_m3627A8DC0128EA13268432FD42FDFFA5E9E8FC16_AdjustorThunk (void);
// 0x000003AA System.Void Obi.ObiEdgeMeshHandle::.ctor(UnityEngine.EdgeCollider2D,System.Int32)
extern void ObiEdgeMeshHandle__ctor_m055ABDB8025F78423ED6926D1E316EB48FABC4AE (void);
// 0x000003AB System.Void Obi.EdgeMeshHeader::.ctor(System.Int32,System.Int32,System.Int32,System.Int32,System.Int32,System.Int32)
extern void EdgeMeshHeader__ctor_mFBC1ECFE2844D2E52FDDEF03C23174C84E12C124_AdjustorThunk (void);
// 0x000003AC System.Void Obi.ObiEdgeMeshContainer::.ctor()
extern void ObiEdgeMeshContainer__ctor_m7E7182023FCAB6FD0840A93D3F33CD76868DEC2D (void);
// 0x000003AD Obi.ObiEdgeMeshHandle Obi.ObiEdgeMeshContainer::GetOrCreateEdgeMesh(UnityEngine.EdgeCollider2D)
extern void ObiEdgeMeshContainer_GetOrCreateEdgeMesh_m08E80D7BB9E4C1AD3040ECCB3E3F80BB0EBC5127 (void);
// 0x000003AE System.Void Obi.ObiEdgeMeshContainer::DestroyEdgeMesh(Obi.ObiEdgeMeshHandle)
extern void ObiEdgeMeshContainer_DestroyEdgeMesh_m6A49B7BD3DACF76A1B0E7F591A523795F55758EC (void);
// 0x000003AF System.Void Obi.ObiEdgeMeshContainer::Dispose()
extern void ObiEdgeMeshContainer_Dispose_m1005586E5D7BE477D43008737857278C3088223A (void);
// 0x000003B0 System.Void Obi.ObiEdgeMeshContainer_<>c::.cctor()
extern void U3CU3Ec__cctor_m319B04707AB4F191DD80BB15D8CE099EE0A1A7B4 (void);
// 0x000003B1 System.Void Obi.ObiEdgeMeshContainer_<>c::.ctor()
extern void U3CU3Ec__ctor_m0A7F956BDB67E4A8C2BCD9D83C276D02DC561279 (void);
// 0x000003B2 Obi.Edge Obi.ObiEdgeMeshContainer_<>c::<GetOrCreateEdgeMesh>b__6_0(Obi.IBounded)
extern void U3CU3Ec_U3CGetOrCreateEdgeMeshU3Eb__6_0_mDEAC05CDAF4D92AFBB301FED8F1C6099004401DD (void);
// 0x000003B3 System.Void Obi.ObiHeightFieldHandle::.ctor(UnityEngine.TerrainData,System.Int32)
extern void ObiHeightFieldHandle__ctor_m7487BDAEEC65A9BBFE0A38658CA884E309BC6FEA (void);
// 0x000003B4 System.Void Obi.HeightFieldHeader::.ctor(System.Int32,System.Int32)
extern void HeightFieldHeader__ctor_m68533ABFD4D9CB9C9AE92C04D96F69D03704B421_AdjustorThunk (void);
// 0x000003B5 System.Void Obi.ObiHeightFieldContainer::.ctor()
extern void ObiHeightFieldContainer__ctor_m5666C4D77395B12D8510DE3C1C91F7DF7080917E (void);
// 0x000003B6 Obi.ObiHeightFieldHandle Obi.ObiHeightFieldContainer::GetOrCreateHeightField(UnityEngine.TerrainData)
extern void ObiHeightFieldContainer_GetOrCreateHeightField_m24A1A0266B0B0558347D317ECAF37AE00CF5F73D (void);
// 0x000003B7 System.Void Obi.ObiHeightFieldContainer::DestroyHeightField(Obi.ObiHeightFieldHandle)
extern void ObiHeightFieldContainer_DestroyHeightField_m0C864AEC3E3DF063366196C10432FC0E9582C124 (void);
// 0x000003B8 System.Void Obi.ObiHeightFieldContainer::Dispose()
extern void ObiHeightFieldContainer_Dispose_m381A36CB411FE4B112A455EB52BEF459C07B542E (void);
// 0x000003B9 System.Void Obi.ObiRigidbody::Awake()
extern void ObiRigidbody_Awake_mF76BC3EB63FCF9C9203C0490FB685630EA22B0E2 (void);
// 0x000003BA System.Void Obi.ObiRigidbody::UpdateKinematicVelocities(System.Single)
extern void ObiRigidbody_UpdateKinematicVelocities_mCB621E407E54750602D501EF243278687D054900 (void);
// 0x000003BB System.Void Obi.ObiRigidbody::UpdateIfNeeded(System.Single)
extern void ObiRigidbody_UpdateIfNeeded_mE8E08B343D9013FC5168AC526FDD2FD60713C63F (void);
// 0x000003BC System.Void Obi.ObiRigidbody::UpdateVelocities(UnityEngine.Vector3,UnityEngine.Vector3)
extern void ObiRigidbody_UpdateVelocities_m8383481CAC453BA89BF272E3DA3AC19A575ED21F (void);
// 0x000003BD System.Void Obi.ObiRigidbody::.ctor()
extern void ObiRigidbody__ctor_mD7D0C954158E4444C3068E83B3327DF0C19F22B4 (void);
// 0x000003BE System.Void Obi.ObiRigidbody2D::Awake()
extern void ObiRigidbody2D_Awake_mFBE100A1729F05F2F8C0D6F81A5D55A200C18D8F (void);
// 0x000003BF System.Void Obi.ObiRigidbody2D::UpdateKinematicVelocities(System.Single)
extern void ObiRigidbody2D_UpdateKinematicVelocities_m9FA8BC7541C8841BA05CEFB2E5153BEF8D0F0998 (void);
// 0x000003C0 System.Void Obi.ObiRigidbody2D::UpdateIfNeeded(System.Single)
extern void ObiRigidbody2D_UpdateIfNeeded_m2B7DDB040171C3260191C90FCA6E67EA1658C21F (void);
// 0x000003C1 System.Void Obi.ObiRigidbody2D::UpdateVelocities(UnityEngine.Vector3,UnityEngine.Vector3)
extern void ObiRigidbody2D_UpdateVelocities_m9E81C9B71FCBB4C7F67940267609771648B6FC02 (void);
// 0x000003C2 System.Void Obi.ObiRigidbody2D::.ctor()
extern void ObiRigidbody2D__ctor_m4B1B8A6B4DC629D021CE278C6D8926801ABF2B5A (void);
// 0x000003C3 System.Void Obi.ObiRigidbodyBase::Awake()
extern void ObiRigidbodyBase_Awake_m76001FC03150D7505A5D630EA20933DF245BA3C7 (void);
// 0x000003C4 System.Void Obi.ObiRigidbodyBase::OnDestroy()
extern void ObiRigidbodyBase_OnDestroy_mE8BDF21E79746CC2EF9F1CD492C425FE5FD46159 (void);
// 0x000003C5 System.Void Obi.ObiRigidbodyBase::UpdateIfNeeded(System.Single)
// 0x000003C6 System.Void Obi.ObiRigidbodyBase::UpdateVelocities(UnityEngine.Vector3,UnityEngine.Vector3)
// 0x000003C7 System.Void Obi.ObiRigidbodyBase::.ctor()
extern void ObiRigidbodyBase__ctor_m97C098719F7244A8923BA99EB88178D5BC81E375 (void);
// 0x000003C8 System.Void Obi.Triangle::.ctor(System.Int32,System.Int32,System.Int32,UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3)
extern void Triangle__ctor_mF3FB7D0163513605C1284E55D476938CFA8D8B3E_AdjustorThunk (void);
// 0x000003C9 Obi.Aabb Obi.Triangle::GetBounds()
extern void Triangle_GetBounds_m96DD7752DB622FBB5D130CC0203D16EF9A16183D_AdjustorThunk (void);
// 0x000003CA System.Void Obi.ObiTriangleMeshHandle::.ctor(UnityEngine.Mesh,System.Int32)
extern void ObiTriangleMeshHandle__ctor_m4C8FBE34410F6DE0216D655610C66D865609FD82 (void);
// 0x000003CB System.Void Obi.TriangleMeshHeader::.ctor(System.Int32,System.Int32,System.Int32,System.Int32,System.Int32,System.Int32)
extern void TriangleMeshHeader__ctor_m05352EA77E9896540763C1FC4DA9A86E8FD110E4_AdjustorThunk (void);
// 0x000003CC System.Void Obi.ObiTriangleMeshContainer::.ctor()
extern void ObiTriangleMeshContainer__ctor_m7744CBB6264FB551DA0E62E0AE2B14750075FAFC (void);
// 0x000003CD Obi.ObiTriangleMeshHandle Obi.ObiTriangleMeshContainer::GetOrCreateTriangleMesh(UnityEngine.Mesh)
extern void ObiTriangleMeshContainer_GetOrCreateTriangleMesh_m7966A3DEF57A37FDCD4EAB1254350FCF3C443C54 (void);
// 0x000003CE System.Void Obi.ObiTriangleMeshContainer::DestroyTriangleMesh(Obi.ObiTriangleMeshHandle)
extern void ObiTriangleMeshContainer_DestroyTriangleMesh_mC9C784E4C7DD7154C4F28D56E3D49B0CCFDFA4A8 (void);
// 0x000003CF System.Void Obi.ObiTriangleMeshContainer::Dispose()
extern void ObiTriangleMeshContainer_Dispose_m5B8C0A01AFA58E44642169B3B0230566C3901B80 (void);
// 0x000003D0 System.Void Obi.ObiTriangleMeshContainer_<>c::.cctor()
extern void U3CU3Ec__cctor_m78E18BF63E0FC93B04931361A400A550DF8E42FE (void);
// 0x000003D1 System.Void Obi.ObiTriangleMeshContainer_<>c::.ctor()
extern void U3CU3Ec__ctor_m353F343C4C6BE6C321A97142213FA89DC2CBACA8 (void);
// 0x000003D2 Obi.Triangle Obi.ObiTriangleMeshContainer_<>c::<GetOrCreateTriangleMesh>b__6_0(Obi.IBounded)
extern void U3CU3Ec_U3CGetOrCreateTriangleMeshU3Eb__6_0_m55F60D5CC64D104ED8E0B37F968D4479C3124E42 (void);
// 0x000003D3 System.Collections.IEnumerator Obi.ASDF::Build(System.Single,System.Int32,UnityEngine.Vector3[],System.Int32[],System.Collections.Generic.List`1<Obi.DFNode>,System.Int32)
extern void ASDF_Build_m0B3E28D5C89253E2A2FAA9545A0B748B5C69E1D4 (void);
// 0x000003D4 System.Single Obi.ASDF::Sample(System.Collections.Generic.List`1<Obi.DFNode>,UnityEngine.Vector3)
extern void ASDF_Sample_mA68406A166BFA7E95D0C62E769CB30FC8F2C78C0 (void);
// 0x000003D5 System.Void Obi.ASDF::.ctor()
extern void ASDF__ctor_mC6D0B839501A5338ADE298E18D27946D91973849 (void);
// 0x000003D6 System.Void Obi.ASDF::.cctor()
extern void ASDF__cctor_m3BDC04F77E29E81E3A541CFA21851E2153E7EC1A (void);
// 0x000003D7 System.Void Obi.ASDF_<>c::.cctor()
extern void U3CU3Ec__cctor_m02BD313FFAEEDA3D68EE3E46312C75EB44777102 (void);
// 0x000003D8 System.Void Obi.ASDF_<>c::.ctor()
extern void U3CU3Ec__ctor_m4CEC5676823706F8C33924A47217BB2E3C584BB8 (void);
// 0x000003D9 Obi.Triangle Obi.ASDF_<>c::<Build>b__3_0(Obi.IBounded)
extern void U3CU3Ec_U3CBuildU3Eb__3_0_m2F53BFE53C74A23E70A52AA23A3625EA199232CA (void);
// 0x000003DA System.Void Obi.ASDF_<Build>d__3::.ctor(System.Int32)
extern void U3CBuildU3Ed__3__ctor_m2AF6E46C3AF1DCE8EB0AC87345B0082720891BF3 (void);
// 0x000003DB System.Void Obi.ASDF_<Build>d__3::System.IDisposable.Dispose()
extern void U3CBuildU3Ed__3_System_IDisposable_Dispose_m407D5BEFA6CC050E62B83A69FC1C81FA11062FE4 (void);
// 0x000003DC System.Boolean Obi.ASDF_<Build>d__3::MoveNext()
extern void U3CBuildU3Ed__3_MoveNext_m2878CF634F624CD9D2E068EB30A27BFF838B1AE9 (void);
// 0x000003DD System.Object Obi.ASDF_<Build>d__3::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CBuildU3Ed__3_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m3FD71D70EB820213EDF7BC3853561DF86E43D30D (void);
// 0x000003DE System.Void Obi.ASDF_<Build>d__3::System.Collections.IEnumerator.Reset()
extern void U3CBuildU3Ed__3_System_Collections_IEnumerator_Reset_m026B7C1E35013639385A979664E9479DCFB7269A (void);
// 0x000003DF System.Object Obi.ASDF_<Build>d__3::System.Collections.IEnumerator.get_Current()
extern void U3CBuildU3Ed__3_System_Collections_IEnumerator_get_Current_m8730288CA28692A77A479C27FB99AFB9421E825B (void);
// 0x000003E0 System.Void Obi.DFNode::.ctor(UnityEngine.Vector4)
extern void DFNode__ctor_m5AA5A22FD71D7439C9136C2FBC6F151E25C8A609_AdjustorThunk (void);
// 0x000003E1 System.Single Obi.DFNode::Sample(UnityEngine.Vector3)
extern void DFNode_Sample_mD228ACC5980D03D69BA1510383BA88AD4253D1D3_AdjustorThunk (void);
// 0x000003E2 UnityEngine.Vector3 Obi.DFNode::GetNormalizedPos(UnityEngine.Vector3)
extern void DFNode_GetNormalizedPos_m48205296253B504FB1F9D3177EADD8E62A70F31B_AdjustorThunk (void);
// 0x000003E3 System.Int32 Obi.DFNode::GetOctant(UnityEngine.Vector3)
extern void DFNode_GetOctant_m8E57E952AB3E07E8596E051854C4B9EF62575192_AdjustorThunk (void);
// 0x000003E4 UnityEngine.Vector4 Obi.Aabb::get_center()
extern void Aabb_get_center_m6F29EA934C6E7C6A2557550214746BE327737113_AdjustorThunk (void);
// 0x000003E5 UnityEngine.Vector4 Obi.Aabb::get_size()
extern void Aabb_get_size_m316B3CACF5BB496D6CF8532AF06AF826C78486D7_AdjustorThunk (void);
// 0x000003E6 System.Void Obi.Aabb::.ctor(UnityEngine.Vector4,UnityEngine.Vector4)
extern void Aabb__ctor_m8B1C8639B79DD7C951EF63FD820BB63B8A6B5DB4_AdjustorThunk (void);
// 0x000003E7 System.Void Obi.Aabb::.ctor(UnityEngine.Vector4)
extern void Aabb__ctor_m293B0520F59E98F3B14FEAFE282685CA663D2E0B_AdjustorThunk (void);
// 0x000003E8 System.Void Obi.Aabb::Encapsulate(UnityEngine.Vector4)
extern void Aabb_Encapsulate_mDCE164B716BFA4A7F8147BC997DDDAFF5A0A9F08_AdjustorThunk (void);
// 0x000003E9 System.Void Obi.Aabb::Encapsulate(Obi.Aabb)
extern void Aabb_Encapsulate_m4F8536D0A22C6A596F1393D02F97BA7C45B9F9A8_AdjustorThunk (void);
// 0x000003EA System.Void Obi.Aabb::FromBounds(UnityEngine.Bounds,System.Single,System.Boolean)
extern void Aabb_FromBounds_mB313907D1B69DA3917270DE5C2B39AD5ECCCF664_AdjustorThunk (void);
// 0x000003EB System.Void Obi.AffineTransform::.ctor(UnityEngine.Vector4,UnityEngine.Quaternion,UnityEngine.Vector4)
extern void AffineTransform__ctor_m1113EA596451380FE97BF986F98D180B67B50914_AdjustorThunk (void);
// 0x000003EC System.Void Obi.AffineTransform::FromTransform(UnityEngine.Transform,System.Boolean)
extern void AffineTransform_FromTransform_mF590DA4EFBCE17F0245A9C33FE65B7405154A7EB_AdjustorThunk (void);
// 0x000003ED Obi.BIHNode[] Obi.BIH::Build(Obi.IBounded[]&,System.Int32,System.Single)
extern void BIH_Build_m543AD7EA6BD7D11FCF4DD88B06747F4B797784D9 (void);
// 0x000003EE System.Int32 Obi.BIH::HoarePartition(Obi.IBounded[],System.Int32,System.Int32,System.Single,Obi.BIHNode&,System.Int32)
extern void BIH_HoarePartition_m26E2AC749940517A897AF2963BE5322BB38A3CF9 (void);
// 0x000003EF System.Single Obi.BIH::DistanceToSurface(Obi.Triangle[],UnityEngine.Vector3[],UnityEngine.Vector3[],Obi.BIHNode&,UnityEngine.Vector3&)
extern void BIH_DistanceToSurface_m23810E4AED9E58F613C341BC0C9B6D7FECF9D905 (void);
// 0x000003F0 System.Single Obi.BIH::DistanceToSurface(Obi.BIHNode[],Obi.Triangle[],UnityEngine.Vector3[],UnityEngine.Vector3[],UnityEngine.Vector3&)
extern void BIH_DistanceToSurface_mAE13B1EFAA791602268DFF2A5ED429691EA76178 (void);
// 0x000003F1 System.Single Obi.BIH::DistanceToSurface(Obi.BIHNode[],Obi.Triangle[],UnityEngine.Vector3[],UnityEngine.Vector3[],Obi.BIHNode&,UnityEngine.Vector3&)
extern void BIH_DistanceToSurface_mCDD4F03C3B60D8D084F6EDB334C195DE89C58B05 (void);
// 0x000003F2 System.Void Obi.BIH::.ctor()
extern void BIH__ctor_m6D1B7878A01D15CA2622A366286C9308BFB07727 (void);
// 0x000003F3 System.Single Obi.BIH::<DistanceToSurface>g__MinSignedDistanceU7C4_0(System.Single,System.Single)
extern void BIH_U3CDistanceToSurfaceU3Eg__MinSignedDistanceU7C4_0_mED2F171342075378B5B140C14A73A72EF8ACC45F (void);
// 0x000003F4 System.Void Obi.BIHNode::.ctor(System.Int32,System.Int32)
extern void BIHNode__ctor_mBE805A0C558FDCA2FEDE25366ED721EA3776834A_AdjustorThunk (void);
// 0x000003F5 Obi.Aabb Obi.IBounded::GetBounds()
// 0x000003F6 System.Void Obi.CellSpan::.ctor(Obi.VInt4,Obi.VInt4)
extern void CellSpan__ctor_m93CAC71E3E1DC790B927C8C76F72A3784072746B_AdjustorThunk (void);
// 0x000003F7 System.Void Obi.ColliderRigidbody::FromRigidbody(UnityEngine.Rigidbody,System.Boolean)
extern void ColliderRigidbody_FromRigidbody_m4921C838DA8C3C147A91168C9232CB97780260BB_AdjustorThunk (void);
// 0x000003F8 System.Void Obi.ColliderRigidbody::FromRigidbody(UnityEngine.Rigidbody2D,System.Boolean)
extern void ColliderRigidbody_FromRigidbody_m2311F1A25138EA07373D405B7EF2F1EB3859138E_AdjustorThunk (void);
// 0x000003F9 System.Void Obi.CollisionMaterial::FromObiCollisionMaterial(Obi.ObiCollisionMaterial)
extern void CollisionMaterial_FromObiCollisionMaterial_mBF4E29EA56479F76E396B2B5EA9BDEFED6663530_AdjustorThunk (void);
// 0x000003FA System.Void Obi.ObiNativeAabbList::.ctor(System.Int32,System.Int32)
extern void ObiNativeAabbList__ctor_m59E302FAD9AEFD8A3BE4E2C51B322FB0420E1E2F (void);
// 0x000003FB System.Void Obi.ObiNativeAffineTransformList::.ctor(System.Int32,System.Int32)
extern void ObiNativeAffineTransformList__ctor_mCB63BE2A5D0F613E12C647669D05EB6BE52FC83E (void);
// 0x000003FC System.Void Obi.ObiNativeBIHNodeList::.ctor(System.Int32,System.Int32)
extern void ObiNativeBIHNodeList__ctor_m6CB7A19C221489E05D62A7996A07E287439F3D73 (void);
// 0x000003FD System.Void Obi.ObiNativeCellSpanList::.ctor(System.Int32,System.Int32)
extern void ObiNativeCellSpanList__ctor_m9810884859E402D959A6369EEBD22320DC40D2D6 (void);
// 0x000003FE System.Void Obi.ObiNativeColliderShapeList::.ctor(System.Int32,System.Int32)
extern void ObiNativeColliderShapeList__ctor_m79BC575A7059E83119DCE5DFC6D7CE8E308245C3 (void);
// 0x000003FF System.Void Obi.ObiNativeCollisionMaterialList::.ctor(System.Int32,System.Int32)
extern void ObiNativeCollisionMaterialList__ctor_m90089B05CB810F2C23D47F0077910F26FCC61888 (void);
// 0x00000400 System.Void Obi.ObiNativeContactShapeList::.ctor(System.Int32,System.Int32)
extern void ObiNativeContactShapeList__ctor_mDB12EB79069ADCBD7B0F4377C8C5A1545F646DE8 (void);
// 0x00000401 System.Void Obi.ObiNativeDFNodeList::.ctor(System.Int32,System.Int32)
extern void ObiNativeDFNodeList__ctor_mCDC4549E10B99A08FD83058EFD46F45299C71FEC (void);
// 0x00000402 System.Void Obi.ObiNativeDistanceFieldHeaderList::.ctor(System.Int32,System.Int32)
extern void ObiNativeDistanceFieldHeaderList__ctor_mF1E073829DBEAD5BC9ECC60BB9AF6463019282EC (void);
// 0x00000403 System.Void Obi.ObiNativeEdgeList::.ctor(System.Int32,System.Int32)
extern void ObiNativeEdgeList__ctor_m1DFD5ED07389DD12179C9D3494C4E2BF3D62D563 (void);
// 0x00000404 System.Void Obi.ObiNativeEdgeMeshHeaderList::.ctor(System.Int32,System.Int32)
extern void ObiNativeEdgeMeshHeaderList__ctor_m372AE7E0664D6BE77453AB0C52023FC77C720E8C (void);
// 0x00000405 System.Void Obi.ObiNativeFloatList::.ctor(System.Int32,System.Int32)
extern void ObiNativeFloatList__ctor_m109DA72958D0EBB1AEF825F9A43C34F2BD1A42E5 (void);
// 0x00000406 System.Void Obi.ObiNativeHeightFieldHeaderList::.ctor(System.Int32,System.Int32)
extern void ObiNativeHeightFieldHeaderList__ctor_m5EC50E82D2C0FE9DBB46618885DE4D9A4FB490A1 (void);
// 0x00000407 System.Void Obi.ObiNativeInt4List::.ctor(System.Int32,System.Int32)
extern void ObiNativeInt4List__ctor_mF3060DFCCBC09AF993C3C96608B51FA83537EDC8 (void);
// 0x00000408 System.Void Obi.ObiNativeInt4List::.ctor(System.Int32,System.Int32,Obi.VInt4)
extern void ObiNativeInt4List__ctor_m24F99A45DDF792A152AF6A140C910F32C460E40A (void);
// 0x00000409 System.Void Obi.ObiNativeIntList::.ctor(System.Int32,System.Int32)
extern void ObiNativeIntList__ctor_m25ADBFB84E370EE592FFF05D343541D84462FED6 (void);
// 0x0000040A System.Void Obi.ObiNativeIntPtrList::.ctor(System.Int32,System.Int32)
extern void ObiNativeIntPtrList__ctor_mE54F7AA88D3E5B4AD478C37640EDFAD145057FB8 (void);
// 0x0000040B System.Void Obi.ObiNativeList`1::set_count(System.Int32)
// 0x0000040C System.Int32 Obi.ObiNativeList`1::get_count()
// 0x0000040D System.Void Obi.ObiNativeList`1::set_capacity(System.Int32)
// 0x0000040E System.Int32 Obi.ObiNativeList`1::get_capacity()
// 0x0000040F System.Boolean Obi.ObiNativeList`1::get_isCreated()
// 0x00000410 T Obi.ObiNativeList`1::get_Item(System.Int32)
// 0x00000411 System.Void Obi.ObiNativeList`1::set_Item(System.Int32,T)
// 0x00000412 System.Void Obi.ObiNativeList`1::.ctor(System.Int32,System.Int32)
// 0x00000413 System.Void Obi.ObiNativeList`1::Finalize()
// 0x00000414 System.Void Obi.ObiNativeList`1::Dispose(System.Boolean)
// 0x00000415 System.Void Obi.ObiNativeList`1::Dispose()
// 0x00000416 System.Void Obi.ObiNativeList`1::OnBeforeSerialize()
// 0x00000417 System.Void Obi.ObiNativeList`1::OnAfterDeserialize()
// 0x00000418 Unity.Collections.NativeArray`1<U> Obi.ObiNativeList`1::AsNativeArray()
// 0x00000419 Unity.Collections.NativeArray`1<U> Obi.ObiNativeList`1::AsNativeArray(System.Int32)
// 0x0000041A UnityEngine.ComputeBuffer Obi.ObiNativeList`1::AsComputeBuffer()
// 0x0000041B System.Void Obi.ObiNativeList`1::ChangeCapacity(System.Int32,System.Int32)
// 0x0000041C System.Boolean Obi.ObiNativeList`1::Compare(Obi.ObiNativeList`1<T>)
// 0x0000041D System.Void Obi.ObiNativeList`1::CopyFrom(Obi.ObiNativeList`1<T>)
// 0x0000041E System.Void Obi.ObiNativeList`1::CopyFrom(Obi.ObiNativeList`1<T>,System.Int32,System.Int32,System.Int32)
// 0x0000041F System.Void Obi.ObiNativeList`1::CopyReplicate(T,System.Int32,System.Int32)
// 0x00000420 System.Void Obi.ObiNativeList`1::CopyTo(T[],System.Int32,System.Int32)
// 0x00000421 System.Void Obi.ObiNativeList`1::Clear()
// 0x00000422 System.Void Obi.ObiNativeList`1::Add(T)
// 0x00000423 System.Void Obi.ObiNativeList`1::AddRange(System.Collections.Generic.IEnumerable`1<T>)
// 0x00000424 System.Void Obi.ObiNativeList`1::RemoveRange(System.Int32,System.Int32)
// 0x00000425 System.Void Obi.ObiNativeList`1::RemoveAt(System.Int32)
// 0x00000426 System.Boolean Obi.ObiNativeList`1::ResizeUninitialized(System.Int32)
// 0x00000427 System.Boolean Obi.ObiNativeList`1::ResizeInitialized(System.Int32,T)
// 0x00000428 System.Boolean Obi.ObiNativeList`1::EnsureCapacity(System.Int32)
// 0x00000429 System.Void Obi.ObiNativeList`1::WipeToZero()
// 0x0000042A System.String Obi.ObiNativeList`1::ToString()
// 0x0000042B System.Void* Obi.ObiNativeList`1::AddressOfElement(System.Int32)
// 0x0000042C System.IntPtr Obi.ObiNativeList`1::GetIntPtr()
// 0x0000042D System.Void Obi.ObiNativeList`1::Swap(System.Int32,System.Int32)
// 0x0000042E System.Collections.Generic.IEnumerator`1<T> Obi.ObiNativeList`1::GetEnumerator()
// 0x0000042F System.Collections.IEnumerator Obi.ObiNativeList`1::System.Collections.IEnumerable.GetEnumerator()
// 0x00000430 System.Void Obi.ObiNativeList`1_<GetEnumerator>d__46::.ctor(System.Int32)
// 0x00000431 System.Void Obi.ObiNativeList`1_<GetEnumerator>d__46::System.IDisposable.Dispose()
// 0x00000432 System.Boolean Obi.ObiNativeList`1_<GetEnumerator>d__46::MoveNext()
// 0x00000433 T Obi.ObiNativeList`1_<GetEnumerator>d__46::System.Collections.Generic.IEnumerator<T>.get_Current()
// 0x00000434 System.Void Obi.ObiNativeList`1_<GetEnumerator>d__46::System.Collections.IEnumerator.Reset()
// 0x00000435 System.Object Obi.ObiNativeList`1_<GetEnumerator>d__46::System.Collections.IEnumerator.get_Current()
// 0x00000436 System.Void Obi.ObiNativeMatrix4x4List::.ctor(System.Int32,System.Int32)
extern void ObiNativeMatrix4x4List__ctor_m8315DA2799111EE4624C6AD421E912B9E2C0D775 (void);
// 0x00000437 System.Void Obi.ObiNativeQuaternionList::.ctor(System.Int32,System.Int32)
extern void ObiNativeQuaternionList__ctor_mF8798CFC21A168F77FDE7E4E433E4ED07BB42B0B (void);
// 0x00000438 System.Void Obi.ObiNativeQuaternionList::.ctor(System.Int32,System.Int32,UnityEngine.Quaternion)
extern void ObiNativeQuaternionList__ctor_m2733D11CD8396D239DBC16995D1E9404EA29AADA (void);
// 0x00000439 System.Void Obi.ObiNativeQueryResultList::.ctor(System.Int32,System.Int32)
extern void ObiNativeQueryResultList__ctor_m5B404B4FBEB82A5A119AC92DFC671C161598266D (void);
// 0x0000043A System.Void Obi.ObiNativeQueryShapeList::.ctor(System.Int32,System.Int32)
extern void ObiNativeQueryShapeList__ctor_m1FC70B84E766982F194F6DC2FE87D15DA79493E6 (void);
// 0x0000043B System.Void Obi.ObiNativeRigidbodyList::.ctor(System.Int32,System.Int32)
extern void ObiNativeRigidbodyList__ctor_m21C628B6ADABDBDFA0D64D291A00D1BBC923B686 (void);
// 0x0000043C System.Void Obi.ObiNativeTriangleList::.ctor(System.Int32,System.Int32)
extern void ObiNativeTriangleList__ctor_m9FD16BE51A73FCC0ECD34FE1E3E60DE410644782 (void);
// 0x0000043D System.Void Obi.ObiNativeTriangleMeshHeaderList::.ctor(System.Int32,System.Int32)
extern void ObiNativeTriangleMeshHeaderList__ctor_m4F5742A78ACC6F92F44E484750F2710E45D32A8F (void);
// 0x0000043E System.Void Obi.ObiNativeVector2List::.ctor(System.Int32,System.Int32)
extern void ObiNativeVector2List__ctor_m3326D8FF9523937A0D58CCEB0AA53A7F715AB740 (void);
// 0x0000043F System.Void Obi.ObiNativeVector3List::.ctor(System.Int32,System.Int32)
extern void ObiNativeVector3List__ctor_m454CD1792A828386A51E51AC80EF30B4EC59DB25 (void);
// 0x00000440 System.Void Obi.ObiNativeVector4List::.ctor(System.Int32,System.Int32)
extern void ObiNativeVector4List__ctor_m4E5196643444C3F0983F4C1D35EC054085549834 (void);
// 0x00000441 UnityEngine.Vector3 Obi.ObiNativeVector4List::GetVector3(System.Int32)
extern void ObiNativeVector4List_GetVector3_m8EAE7F64879DD9A415D25E05C258ADEBC921A55D (void);
// 0x00000442 System.Void Obi.ObiNativeVector4List::SetVector3(System.Int32,UnityEngine.Vector3)
extern void ObiNativeVector4List_SetVector3_mEA4A6008125CDE1854000FBF64CEA0887E8A7D94 (void);
// 0x00000443 System.Collections.Generic.IEnumerator`1<T> Obi.ObiList`1::GetEnumerator()
// 0x00000444 System.Collections.IEnumerator Obi.ObiList`1::System.Collections.IEnumerable.GetEnumerator()
// 0x00000445 System.Void Obi.ObiList`1::Add(T)
// 0x00000446 System.Void Obi.ObiList`1::Clear()
// 0x00000447 System.Boolean Obi.ObiList`1::Contains(T)
// 0x00000448 System.Void Obi.ObiList`1::CopyTo(T[],System.Int32)
// 0x00000449 System.Boolean Obi.ObiList`1::Remove(T)
// 0x0000044A System.Int32 Obi.ObiList`1::get_Count()
// 0x0000044B System.Boolean Obi.ObiList`1::get_IsReadOnly()
// 0x0000044C System.Int32 Obi.ObiList`1::IndexOf(T)
// 0x0000044D System.Void Obi.ObiList`1::Insert(System.Int32,T)
// 0x0000044E System.Void Obi.ObiList`1::RemoveAt(System.Int32)
// 0x0000044F T Obi.ObiList`1::get_Item(System.Int32)
// 0x00000450 System.Void Obi.ObiList`1::set_Item(System.Int32,T)
// 0x00000451 T[] Obi.ObiList`1::get_Data()
// 0x00000452 System.Void Obi.ObiList`1::SetCount(System.Int32)
// 0x00000453 System.Void Obi.ObiList`1::EnsureCapacity(System.Int32)
// 0x00000454 System.Void Obi.ObiList`1::.ctor()
// 0x00000455 System.Void Obi.ObiList`1_<GetEnumerator>d__2::.ctor(System.Int32)
// 0x00000456 System.Void Obi.ObiList`1_<GetEnumerator>d__2::System.IDisposable.Dispose()
// 0x00000457 System.Boolean Obi.ObiList`1_<GetEnumerator>d__2::MoveNext()
// 0x00000458 T Obi.ObiList`1_<GetEnumerator>d__2::System.Collections.Generic.IEnumerator<T>.get_Current()
// 0x00000459 System.Void Obi.ObiList`1_<GetEnumerator>d__2::System.Collections.IEnumerator.Reset()
// 0x0000045A System.Object Obi.ObiList`1_<GetEnumerator>d__2::System.Collections.IEnumerator.get_Current()
// 0x0000045B System.Void Obi.ParticlePair::.ctor(System.Int32,System.Int32)
extern void ParticlePair__ctor_mDDAD78C599D557D2A63715207CBC71A5A93581CA_AdjustorThunk (void);
// 0x0000045C System.Int32 Obi.ParticlePair::get_Item(System.Int32)
extern void ParticlePair_get_Item_m96F244D843B43EE4A0E48128BDAE3837B0574AF9_AdjustorThunk (void);
// 0x0000045D System.Void Obi.ParticlePair::set_Item(System.Int32,System.Int32)
extern void ParticlePair_set_Item_m39EFBD38D2AB94AA009AAF1C18B7EA40077EEDB1_AdjustorThunk (void);
// 0x0000045E System.Void Obi.QueryShape::.ctor(Obi.QueryShape_QueryType,UnityEngine.Vector3,UnityEngine.Vector3,System.Single,System.Single,System.Int32)
extern void QueryShape__ctor_m10B944E68ED9FE30CB3D7EE090068A33F9EDB453_AdjustorThunk (void);
// 0x0000045F System.Int32 Obi.SimplexCounts::get_simplexCount()
extern void SimplexCounts_get_simplexCount_m99631648B641C2ECF96D0C8E5DB265D40F522A86_AdjustorThunk (void);
// 0x00000460 System.Void Obi.SimplexCounts::.ctor(System.Int32,System.Int32,System.Int32)
extern void SimplexCounts__ctor_m68F3A4CF7A694C3300E7BA4227020A89A724F066_AdjustorThunk (void);
// 0x00000461 System.Int32 Obi.SimplexCounts::GetSimplexStartAndSize(System.Int32,System.Int32&)
extern void SimplexCounts_GetSimplexStartAndSize_m27E5A762121E379170DD5C8A757EB3124C259B95_AdjustorThunk (void);
// 0x00000462 System.Void Obi.VInt4::.ctor(System.Int32,System.Int32,System.Int32,System.Int32)
extern void VInt4__ctor_m6FDFB28D4337C36A4A49681BA2E0B85D0B36AFCB_AdjustorThunk (void);
// 0x00000463 System.Void Obi.VInt4::.ctor(System.Int32)
extern void VInt4__ctor_mDF85C8595BC6DCEAAF3A5A4A04704DEFDB238C6C_AdjustorThunk (void);
// 0x00000464 UnityEngine.Vector3Int Obi.MeshVoxelizer::get_Origin()
extern void MeshVoxelizer_get_Origin_m98D23A3FAE59358DCED36A586C0AB6B4F3E77B4D (void);
// 0x00000465 System.Void Obi.MeshVoxelizer::.ctor(UnityEngine.Mesh,System.Single)
extern void MeshVoxelizer__ctor_m2E9885D259222CEE4CB38497584FE8F3EBB0E2B4 (void);
// 0x00000466 UnityEngine.Bounds Obi.MeshVoxelizer::GetTriangleBounds(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3)
extern void MeshVoxelizer_GetTriangleBounds_mABBFE555503EEE8CC183A0BAE00F0BDBDECD1CAF (void);
// 0x00000467 UnityEngine.Vector3Int Obi.MeshVoxelizer::GetPointVoxel(UnityEngine.Vector3)
extern void MeshVoxelizer_GetPointVoxel_m3AAC4B30F0165C5FB8628549722D2D2014FBE20F (void);
// 0x00000468 System.Boolean Obi.MeshVoxelizer::VoxelExists(UnityEngine.Vector3Int)
extern void MeshVoxelizer_VoxelExists_mDFD49B240D2C297535458EACA6E13AA030D19B80 (void);
// 0x00000469 System.Void Obi.MeshVoxelizer::AppendOverlappingVoxels(UnityEngine.Bounds,UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3)
extern void MeshVoxelizer_AppendOverlappingVoxels_m51F870847D3D1B887816F1A7CF7633846F9CF8C9 (void);
// 0x0000046A System.Void Obi.MeshVoxelizer::Voxelize(UnityEngine.Vector3)
extern void MeshVoxelizer_Voxelize_mCD2507C55D53B7CA373FF8EB2E43B8521336704B (void);
// 0x0000046B System.Void Obi.MeshVoxelizer::FloodFill()
extern void MeshVoxelizer_FloodFill_m494C68D66BBCD2F6B85A05DCE56948EE185F7B0B (void);
// 0x0000046C System.Boolean Obi.MeshVoxelizer::IsIntersecting(UnityEngine.Bounds,UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3)
extern void MeshVoxelizer_IsIntersecting_m1EF0FBCCADA0DB46F268E88673A9400D0A6DD7E5 (void);
// 0x0000046D System.Void Obi.MeshVoxelizer::Project(System.Collections.Generic.IEnumerable`1<UnityEngine.Vector3>,UnityEngine.Vector3,System.Double&,System.Double&)
extern void MeshVoxelizer_Project_m9D37115C6F24C964F334813BD4D1C978F4EE066B (void);
// 0x0000046E System.Void Obi.VoxelDistanceField::.ctor(Obi.MeshVoxelizer)
extern void VoxelDistanceField__ctor_mF001EDBFAD1E407A198F85AE578DC94C131DC940 (void);
// 0x0000046F System.Boolean Obi.VoxelDistanceField::VoxelExists(UnityEngine.Vector3Int)
extern void VoxelDistanceField_VoxelExists_m7C7078B4A4027B2D5E090BA4A41367F328F2770D (void);
// 0x00000470 System.Void Obi.VoxelDistanceField::JumpFlood()
extern void VoxelDistanceField_JumpFlood_m1809D2D4CCD13129EE9B88593EC5A9572DD58F3E (void);
// 0x00000471 System.Void Obi.VoxelDistanceField::JumpFloodPass(System.Int32,UnityEngine.Vector3Int[0...,0...,0...],UnityEngine.Vector3Int[0...,0...,0...])
extern void VoxelDistanceField_JumpFloodPass_m0CAE942EA7B7FB237160C57ABAC04D2F77198AC3 (void);
// 0x00000472 System.Void Obi.ObiDistanceFieldRenderer::Awake()
extern void ObiDistanceFieldRenderer_Awake_m02202AAABFC3B738E4965E9B2B897A228E6AC210 (void);
// 0x00000473 System.Void Obi.ObiDistanceFieldRenderer::OnEnable()
extern void ObiDistanceFieldRenderer_OnEnable_m5A75F9F5EE526E06FF0D410741740FCB6B6478E1 (void);
// 0x00000474 System.Void Obi.ObiDistanceFieldRenderer::OnDisable()
extern void ObiDistanceFieldRenderer_OnDisable_m7C13494A2B29D5E6827B2757573C7370FC80A394 (void);
// 0x00000475 System.Void Obi.ObiDistanceFieldRenderer::Cleanup()
extern void ObiDistanceFieldRenderer_Cleanup_mFE94B31A3E0EED155DC9650B112587501A88D532 (void);
// 0x00000476 System.Void Obi.ObiDistanceFieldRenderer::ResizeTexture()
extern void ObiDistanceFieldRenderer_ResizeTexture_mE50107B1ECBAB2706311F61EB8EDFAA5A47FB02C (void);
// 0x00000477 System.Void Obi.ObiDistanceFieldRenderer::CreatePlaneMesh(Obi.ObiDistanceField)
extern void ObiDistanceFieldRenderer_CreatePlaneMesh_m7DABB1814E6572C53BCBCC12B382369FA534AAA4 (void);
// 0x00000478 System.Void Obi.ObiDistanceFieldRenderer::RefreshCutawayTexture(Obi.ObiDistanceField)
extern void ObiDistanceFieldRenderer_RefreshCutawayTexture_mFE87BC7C9D4D36CAE8BA3A2BB5CCFE9195422F93 (void);
// 0x00000479 System.Void Obi.ObiDistanceFieldRenderer::DrawCutawayPlane(Obi.ObiDistanceField,UnityEngine.Matrix4x4)
extern void ObiDistanceFieldRenderer_DrawCutawayPlane_m881EB8355EA4D1D21C65297319180645AD0BBB3A (void);
// 0x0000047A System.Void Obi.ObiDistanceFieldRenderer::OnDrawGizmos()
extern void ObiDistanceFieldRenderer_OnDrawGizmos_m216C9ED791D6583CFB5AC31CCF96FC999FEB065B (void);
// 0x0000047B System.Void Obi.ObiDistanceFieldRenderer::.ctor()
extern void ObiDistanceFieldRenderer__ctor_m5BDF67924CA8661E32DCAE76825AECE42C4E08AA (void);
// 0x0000047C System.Void Obi.ObiInstancedParticleRenderer::OnEnable()
extern void ObiInstancedParticleRenderer_OnEnable_mE355745DF976D2E5FB662C641FFF1207CFF849A8 (void);
// 0x0000047D System.Void Obi.ObiInstancedParticleRenderer::OnDisable()
extern void ObiInstancedParticleRenderer_OnDisable_mB6727244E6FCBAA7A72470799C34118F47F54C26 (void);
// 0x0000047E System.Void Obi.ObiInstancedParticleRenderer::DrawParticles(Obi.ObiActor)
extern void ObiInstancedParticleRenderer_DrawParticles_m006744E372DC552B4BBE90BDA10C2936E58A7CEB (void);
// 0x0000047F System.Void Obi.ObiInstancedParticleRenderer::.ctor()
extern void ObiInstancedParticleRenderer__ctor_m36160262281CB03A2EF5F4F65B1599650E20D198 (void);
// 0x00000480 System.Void Obi.ObiInstancedParticleRenderer::.cctor()
extern void ObiInstancedParticleRenderer__cctor_m8F094F8AD40C59C077031DB8223AF95B8123B2B7 (void);
// 0x00000481 System.Collections.Generic.IEnumerable`1<UnityEngine.Mesh> Obi.ObiParticleRenderer::get_ParticleMeshes()
extern void ObiParticleRenderer_get_ParticleMeshes_m19B21A66F62AF9565B5610A7E406A9F4D9AB2624 (void);
// 0x00000482 UnityEngine.Material Obi.ObiParticleRenderer::get_ParticleMaterial()
extern void ObiParticleRenderer_get_ParticleMaterial_mACE9EAEA565D0D3F7A7BF6C53B8C04111AAFB530 (void);
// 0x00000483 System.Void Obi.ObiParticleRenderer::OnEnable()
extern void ObiParticleRenderer_OnEnable_m5936C65AFFDE90197548673B64DCDB559B634084 (void);
// 0x00000484 System.Void Obi.ObiParticleRenderer::OnDisable()
extern void ObiParticleRenderer_OnDisable_mAA99859EE95AC59E3CCEBDAEE3C9F3D1EBE9F2A3 (void);
// 0x00000485 System.Void Obi.ObiParticleRenderer::CreateMaterialIfNeeded()
extern void ObiParticleRenderer_CreateMaterialIfNeeded_m3F402725EA811E7191F495C02BC1D42C9D3508E2 (void);
// 0x00000486 System.Void Obi.ObiParticleRenderer::DrawParticles(Obi.ObiActor)
extern void ObiParticleRenderer_DrawParticles_m968118939B2050BAB32AF92EFCB004552F31FCAE (void);
// 0x00000487 System.Void Obi.ObiParticleRenderer::DrawParticles()
extern void ObiParticleRenderer_DrawParticles_m10909ABED462F26F971463C7A569BC19C8115FD8 (void);
// 0x00000488 System.Void Obi.ObiParticleRenderer::.ctor()
extern void ObiParticleRenderer__ctor_mFC28AF85E8C3264440883850C411477777E11A29 (void);
// 0x00000489 System.Void Obi.ObiParticleRenderer::.cctor()
extern void ObiParticleRenderer__cctor_m25FE6E62F8C43957CDFA579FE81399B4270C8DDC (void);
// 0x0000048A UnityEngine.ParticleSystem Obi.ParticleAdvector::get_Particles()
extern void ParticleAdvector_get_Particles_m779A3049F43CB0E74A8A78144C4BA3C3B21664F7 (void);
// 0x0000048B System.Void Obi.ParticleAdvector::OnEnable()
extern void ParticleAdvector_OnEnable_m51E5BC2C9C3DA646BABCDCF5659CF771D3C52FA3 (void);
// 0x0000048C System.Void Obi.ParticleAdvector::OnDisable()
extern void ParticleAdvector_OnDisable_m1477C9FCCA953B2731C85BCA4C8D85FF0A752816 (void);
// 0x0000048D System.Void Obi.ParticleAdvector::ReallocateParticles()
extern void ParticleAdvector_ReallocateParticles_mCC3C40E6C9410A048F88E31E179EE1EFDA18B099 (void);
// 0x0000048E System.Void Obi.ParticleAdvector::Solver_OnStepEnd(Obi.ObiSolver)
extern void ParticleAdvector_Solver_OnStepEnd_mEB2985B8866986B1A9D814C779A04BB66B05EF63 (void);
// 0x0000048F System.Void Obi.ParticleAdvector::.ctor()
extern void ParticleAdvector__ctor_m55BA4D19DA2791FC95B08F60C50CC3D2E74D0866 (void);
// 0x00000490 System.Collections.Generic.IEnumerable`1<UnityEngine.Mesh> Obi.ParticleImpostorRendering::get_Meshes()
extern void ParticleImpostorRendering_get_Meshes_m755745FEE84971387953757DE9D15D9D32204C2F (void);
// 0x00000491 System.Void Obi.ParticleImpostorRendering::Apply(UnityEngine.Mesh)
extern void ParticleImpostorRendering_Apply_mC4B8B70A312A115D0D3264BA0F4D6CB8920AD9D5 (void);
// 0x00000492 System.Void Obi.ParticleImpostorRendering::ClearMeshes()
extern void ParticleImpostorRendering_ClearMeshes_m425FD6121111FFDD2834713DDE69988571B30BD8 (void);
// 0x00000493 System.Void Obi.ParticleImpostorRendering::UpdateMeshes(Obi.IObiParticleCollection)
extern void ParticleImpostorRendering_UpdateMeshes_m08FF131D2263DFFD11AFC5C78A699C031A1AC02D (void);
// 0x00000494 System.Void Obi.ParticleImpostorRendering::.ctor()
extern void ParticleImpostorRendering__ctor_m83C3D5BED9E2463E4E916753E01D78C90E7C9BBE (void);
// 0x00000495 System.Void Obi.ParticleImpostorRendering::.cctor()
extern void ParticleImpostorRendering__cctor_m096389C9C5C4FB998DFF6B43A943B8F29D300B72 (void);
// 0x00000496 System.Void Obi.ShadowmapExposer::Awake()
extern void ShadowmapExposer_Awake_mA4E68783C4BCFD7B7FCCF2FB2047921F739DA519 (void);
// 0x00000497 System.Void Obi.ShadowmapExposer::OnEnable()
extern void ShadowmapExposer_OnEnable_mA68155C92E4F83954076D9000038D58A265100F5 (void);
// 0x00000498 System.Void Obi.ShadowmapExposer::OnDisable()
extern void ShadowmapExposer_OnDisable_m7CC85B1D1729AA03CDAC1816F9A84E3350254554 (void);
// 0x00000499 System.Void Obi.ShadowmapExposer::Cleanup()
extern void ShadowmapExposer_Cleanup_m32D90F4E8CAC5C2EB13BFAD141A056F8D3B143E5 (void);
// 0x0000049A System.Void Obi.ShadowmapExposer::SetupFluidShadowsCommandBuffer()
extern void ShadowmapExposer_SetupFluidShadowsCommandBuffer_mE799C5800875D6B7CA60B3775768C9F07B2288B4 (void);
// 0x0000049B System.Void Obi.ShadowmapExposer::Update()
extern void ShadowmapExposer_Update_mBFA72090D0E7D36367BCA31B001E2965D1B264E2 (void);
// 0x0000049C System.Void Obi.ShadowmapExposer::.ctor()
extern void ShadowmapExposer__ctor_m10E8C4FAA46FCB5CC04085336279E049BC51FAFA (void);
// 0x0000049D System.Void Obi.ObiSolver::add_OnCollision(Obi.ObiSolver_CollisionCallback)
extern void ObiSolver_add_OnCollision_mEB324EABAE3F36C43F055950F52827839851DAF5 (void);
// 0x0000049E System.Void Obi.ObiSolver::remove_OnCollision(Obi.ObiSolver_CollisionCallback)
extern void ObiSolver_remove_OnCollision_mA07AA8AA273F50BC4DDAFD0523A7CA23C628D674 (void);
// 0x0000049F System.Void Obi.ObiSolver::add_OnParticleCollision(Obi.ObiSolver_CollisionCallback)
extern void ObiSolver_add_OnParticleCollision_m73A739A0544F098FE805CEF2C6ED3E9528060A52 (void);
// 0x000004A0 System.Void Obi.ObiSolver::remove_OnParticleCollision(Obi.ObiSolver_CollisionCallback)
extern void ObiSolver_remove_OnParticleCollision_m153C65501BB104D19DFB0BC50CE655A20BB725B7 (void);
// 0x000004A1 System.Void Obi.ObiSolver::add_OnUpdateParameters(Obi.ObiSolver_SolverCallback)
extern void ObiSolver_add_OnUpdateParameters_mDCE3C3436FCB6E239FC3C229799E1CA65048798B (void);
// 0x000004A2 System.Void Obi.ObiSolver::remove_OnUpdateParameters(Obi.ObiSolver_SolverCallback)
extern void ObiSolver_remove_OnUpdateParameters_m1D7467B121B4968CCB10AE69A8FCE3F601A622E1 (void);
// 0x000004A3 System.Void Obi.ObiSolver::add_OnPrepareStep(Obi.ObiSolver_SolverStepCallback)
extern void ObiSolver_add_OnPrepareStep_m22DB6F7A692B42DCB0AF33EDDFAF38062A4AB42E (void);
// 0x000004A4 System.Void Obi.ObiSolver::remove_OnPrepareStep(Obi.ObiSolver_SolverStepCallback)
extern void ObiSolver_remove_OnPrepareStep_m5D2D4E8CACADB7938868BE6F29546BA497A05250 (void);
// 0x000004A5 System.Void Obi.ObiSolver::add_OnBeginStep(Obi.ObiSolver_SolverStepCallback)
extern void ObiSolver_add_OnBeginStep_mE11B9F3927E6908AC1C8CA09888FAC4145B60F72 (void);
// 0x000004A6 System.Void Obi.ObiSolver::remove_OnBeginStep(Obi.ObiSolver_SolverStepCallback)
extern void ObiSolver_remove_OnBeginStep_m828F85A9556BCF73D5434B9A689918DEF5F5D39D (void);
// 0x000004A7 System.Void Obi.ObiSolver::add_OnSubstep(Obi.ObiSolver_SolverStepCallback)
extern void ObiSolver_add_OnSubstep_mF4D4FE1ED9E867E6F111B8B082E377D950607C6E (void);
// 0x000004A8 System.Void Obi.ObiSolver::remove_OnSubstep(Obi.ObiSolver_SolverStepCallback)
extern void ObiSolver_remove_OnSubstep_m60FD7FE897C29FEF9EE0EDC9303C62CFF70D2B33 (void);
// 0x000004A9 System.Void Obi.ObiSolver::add_OnEndStep(Obi.ObiSolver_SolverCallback)
extern void ObiSolver_add_OnEndStep_m85BFAD019C29970E500BB5079DA70527A4EB324D (void);
// 0x000004AA System.Void Obi.ObiSolver::remove_OnEndStep(Obi.ObiSolver_SolverCallback)
extern void ObiSolver_remove_OnEndStep_m53846A7D3E4D2113D49EAA4EE56FAE9752CB6CA3 (void);
// 0x000004AB System.Void Obi.ObiSolver::add_OnInterpolate(Obi.ObiSolver_SolverCallback)
extern void ObiSolver_add_OnInterpolate_mBADEB5D974C090F97884D9FF885C017D0C36CF7D (void);
// 0x000004AC System.Void Obi.ObiSolver::remove_OnInterpolate(Obi.ObiSolver_SolverCallback)
extern void ObiSolver_remove_OnInterpolate_m5F9AAE96B45E17989172907EECA1966282A87626 (void);
// 0x000004AD Obi.ISolverImpl Obi.ObiSolver::get_implementation()
extern void ObiSolver_get_implementation_m9F3A6A2A2D6921B6A59A82F935039FC29F24E422 (void);
// 0x000004AE System.Boolean Obi.ObiSolver::get_initialized()
extern void ObiSolver_get_initialized_m245E2320CEE2189A248BB0DE795CD9FE80839C78 (void);
// 0x000004AF Obi.IObiBackend Obi.ObiSolver::get_simulationBackend()
extern void ObiSolver_get_simulationBackend_m67AEE52B9C07C78C1231FE5074F7125E47A8109A (void);
// 0x000004B0 System.Void Obi.ObiSolver::set_backendType(Obi.ObiSolver_BackendType)
extern void ObiSolver_set_backendType_mE714E4CD3D72BE093E2ECBB2B418D7095A3B11B4 (void);
// 0x000004B1 Obi.ObiSolver_BackendType Obi.ObiSolver::get_backendType()
extern void ObiSolver_get_backendType_m49EDD493C5D45A0D0728C26F95F1E955FEFB4104 (void);
// 0x000004B2 Obi.SimplexCounts Obi.ObiSolver::get_simplexCounts()
extern void ObiSolver_get_simplexCounts_m78AF46D65457FB56118D4ECD884FEAD71D3E28C4 (void);
// 0x000004B3 UnityEngine.Bounds Obi.ObiSolver::get_Bounds()
extern void ObiSolver_get_Bounds_m4C8914413BBBC0968BA6ADCF3E7149AA5F7FC7D3 (void);
// 0x000004B4 System.Boolean Obi.ObiSolver::get_IsVisible()
extern void ObiSolver_get_IsVisible_mA12ECA2DFE503F721181D2A8A5F5F9065BABAF0B (void);
// 0x000004B5 System.Single Obi.ObiSolver::get_maxScale()
extern void ObiSolver_get_maxScale_m2118E5D74E45C017D6D91E33790768FC10FE9B18 (void);
// 0x000004B6 System.Int32 Obi.ObiSolver::get_allocParticleCount()
extern void ObiSolver_get_allocParticleCount_m98C18E7A785C31F32283F6A52AB1372313E3E16F (void);
// 0x000004B7 System.Int32 Obi.ObiSolver::get_pointCount()
extern void ObiSolver_get_pointCount_m3D08620D1A6E4D85E71BC101C1D3AE6537462732 (void);
// 0x000004B8 System.Int32 Obi.ObiSolver::get_edgeCount()
extern void ObiSolver_get_edgeCount_m73CFBC056AAC931EFB5589CAFB53274B14851EAB (void);
// 0x000004B9 System.Int32 Obi.ObiSolver::get_triCount()
extern void ObiSolver_get_triCount_m28DC623922A897A5E53CBB6D3CFBB43A4AB02145 (void);
// 0x000004BA System.Int32 Obi.ObiSolver::get_contactCount()
extern void ObiSolver_get_contactCount_mAE6F8DD8070340148DCC11C0F729F1B38C4AF654 (void);
// 0x000004BB System.Int32 Obi.ObiSolver::get_particleContactCount()
extern void ObiSolver_get_particleContactCount_mFDF3ADAB6B54D7FECF6F1943E4AE93C0AE10F18F (void);
// 0x000004BC Obi.ObiSolver_ParticleInActor[] Obi.ObiSolver::get_particleToActor()
extern void ObiSolver_get_particleToActor_mFCEA216986C39AD5264352D73B10017FE5E7F3D3 (void);
// 0x000004BD Obi.ObiNativeVector4List Obi.ObiSolver::get_rigidbodyLinearDeltas()
extern void ObiSolver_get_rigidbodyLinearDeltas_mEB6D6F68DEAD8616EA48FA36AEA9B37520CA9367 (void);
// 0x000004BE Obi.ObiNativeVector4List Obi.ObiSolver::get_rigidbodyAngularDeltas()
extern void ObiSolver_get_rigidbodyAngularDeltas_m05353966D848B59BC9586B0C19CAE476834502C9 (void);
// 0x000004BF UnityEngine.Color[] Obi.ObiSolver::get_colors()
extern void ObiSolver_get_colors_m3263CBAEB79EE1F583593969A2EAD32549CB875D (void);
// 0x000004C0 Obi.ObiNativeInt4List Obi.ObiSolver::get_cellCoords()
extern void ObiSolver_get_cellCoords_m6BB3F63111C716E45AAC8FC0BE4AB0CD17331B28 (void);
// 0x000004C1 Obi.ObiNativeVector4List Obi.ObiSolver::get_positions()
extern void ObiSolver_get_positions_mB7EA5209FDB8289380DBA07A2F28E640D485F557 (void);
// 0x000004C2 Obi.ObiNativeVector4List Obi.ObiSolver::get_restPositions()
extern void ObiSolver_get_restPositions_m1BCA73DCC44BA1B9181838370F0E546B4D66CFC4 (void);
// 0x000004C3 Obi.ObiNativeVector4List Obi.ObiSolver::get_prevPositions()
extern void ObiSolver_get_prevPositions_mA99888F076C4FB374B14DEE1178C625A3F8F9B00 (void);
// 0x000004C4 Obi.ObiNativeVector4List Obi.ObiSolver::get_startPositions()
extern void ObiSolver_get_startPositions_m3B9144D015BDB7F0F676F065AAC9A22F0DA9CC27 (void);
// 0x000004C5 Obi.ObiNativeVector4List Obi.ObiSolver::get_renderablePositions()
extern void ObiSolver_get_renderablePositions_m56EE654A3E7ECEF99EE3F1481E759B01DAB697BE (void);
// 0x000004C6 Obi.ObiNativeQuaternionList Obi.ObiSolver::get_orientations()
extern void ObiSolver_get_orientations_mAF1A79C344CC44789900361936635BBD508D2973 (void);
// 0x000004C7 Obi.ObiNativeQuaternionList Obi.ObiSolver::get_restOrientations()
extern void ObiSolver_get_restOrientations_m5A079EC8140D809FB02223ECCA6986917B333AE4 (void);
// 0x000004C8 Obi.ObiNativeQuaternionList Obi.ObiSolver::get_prevOrientations()
extern void ObiSolver_get_prevOrientations_mBB52AEB5D484B0A4BAE23213D65585BE498B5F68 (void);
// 0x000004C9 Obi.ObiNativeQuaternionList Obi.ObiSolver::get_startOrientations()
extern void ObiSolver_get_startOrientations_mED3EA83E62A458AE8F95A89CEC15ECA81FDF28D1 (void);
// 0x000004CA Obi.ObiNativeQuaternionList Obi.ObiSolver::get_renderableOrientations()
extern void ObiSolver_get_renderableOrientations_mB71AEBAF05CD2CA3FED8936554686F5A0414C06A (void);
// 0x000004CB Obi.ObiNativeVector4List Obi.ObiSolver::get_velocities()
extern void ObiSolver_get_velocities_m73051C86B492795133F138E42FF08698222FCCD6 (void);
// 0x000004CC Obi.ObiNativeVector4List Obi.ObiSolver::get_angularVelocities()
extern void ObiSolver_get_angularVelocities_mE565A9561A09435E294755AA2F7E31993BD932DB (void);
// 0x000004CD Obi.ObiNativeFloatList Obi.ObiSolver::get_invMasses()
extern void ObiSolver_get_invMasses_m403082935BEB22CE4C8A086C31B748FC46B8C319 (void);
// 0x000004CE Obi.ObiNativeFloatList Obi.ObiSolver::get_invRotationalMasses()
extern void ObiSolver_get_invRotationalMasses_m35049317216303EC2D97F143D98DBB60C9EADF58 (void);
// 0x000004CF Obi.ObiNativeVector4List Obi.ObiSolver::get_invInertiaTensors()
extern void ObiSolver_get_invInertiaTensors_m66E7A977E525B6628FA0D14015A52DE6CC26AADA (void);
// 0x000004D0 Obi.ObiNativeVector4List Obi.ObiSolver::get_externalForces()
extern void ObiSolver_get_externalForces_m6DE0779EAA775DB7E7A853B844E07464240BED59 (void);
// 0x000004D1 Obi.ObiNativeVector4List Obi.ObiSolver::get_externalTorques()
extern void ObiSolver_get_externalTorques_m7A057FD836A21C5CA1DAEA2258E05F81D15F8991 (void);
// 0x000004D2 Obi.ObiNativeVector4List Obi.ObiSolver::get_wind()
extern void ObiSolver_get_wind_m72179C4670DBCE8A1838066EA134F02A16DC371C (void);
// 0x000004D3 Obi.ObiNativeVector4List Obi.ObiSolver::get_positionDeltas()
extern void ObiSolver_get_positionDeltas_mA020DD1DBE7DDC042F8C730EBE9A71D6AE213AF9 (void);
// 0x000004D4 Obi.ObiNativeQuaternionList Obi.ObiSolver::get_orientationDeltas()
extern void ObiSolver_get_orientationDeltas_m747EE331713678E42AE2A16F9201AF96E8CFAF21 (void);
// 0x000004D5 Obi.ObiNativeIntList Obi.ObiSolver::get_positionConstraintCounts()
extern void ObiSolver_get_positionConstraintCounts_mFBC0F55E2F573CC7A38F71703D8617DF5C9BF895 (void);
// 0x000004D6 Obi.ObiNativeIntList Obi.ObiSolver::get_orientationConstraintCounts()
extern void ObiSolver_get_orientationConstraintCounts_m6752E0D0CAA0272BD7FF4242E5A426F4310FAECA (void);
// 0x000004D7 Obi.ObiNativeIntList Obi.ObiSolver::get_collisionMaterials()
extern void ObiSolver_get_collisionMaterials_mE98DA539A9F5EC9BE8B35D9AA8C6F4C4FDA211A1 (void);
// 0x000004D8 Obi.ObiNativeIntList Obi.ObiSolver::get_phases()
extern void ObiSolver_get_phases_m1DFABF0F6107CA7A301BBC35A0896C69FA1D7422 (void);
// 0x000004D9 Obi.ObiNativeIntList Obi.ObiSolver::get_filters()
extern void ObiSolver_get_filters_m35DFCECD0FEF501CB8903CF986F67CD3487DFEBB (void);
// 0x000004DA Obi.ObiNativeVector4List Obi.ObiSolver::get_anisotropies()
extern void ObiSolver_get_anisotropies_mDD48FB3AC63921409AAF55FB2FED3833FC458009 (void);
// 0x000004DB Obi.ObiNativeVector4List Obi.ObiSolver::get_principalRadii()
extern void ObiSolver_get_principalRadii_m6C3307DA982056B2AF918D77B539E22580E5F179 (void);
// 0x000004DC Obi.ObiNativeVector4List Obi.ObiSolver::get_normals()
extern void ObiSolver_get_normals_m241E9FFCE3A531BFF2679A3420EBCE7B313FF52C (void);
// 0x000004DD Obi.ObiNativeVector4List Obi.ObiSolver::get_vorticities()
extern void ObiSolver_get_vorticities_mDC8CC9D454FBD7AABF51AEDD83B63D62D74D2E62 (void);
// 0x000004DE Obi.ObiNativeVector4List Obi.ObiSolver::get_fluidData()
extern void ObiSolver_get_fluidData_m70D3FEC3C44F6EA5D68AB6B39BD72A08B41EF349 (void);
// 0x000004DF Obi.ObiNativeVector4List Obi.ObiSolver::get_userData()
extern void ObiSolver_get_userData_m63E5DF119EE501A9380C6A176079C9FD1361B561 (void);
// 0x000004E0 Obi.ObiNativeFloatList Obi.ObiSolver::get_smoothingRadii()
extern void ObiSolver_get_smoothingRadii_mEE720BA5CB3140CCB508FC97466DDC06F18A9380 (void);
// 0x000004E1 Obi.ObiNativeFloatList Obi.ObiSolver::get_buoyancies()
extern void ObiSolver_get_buoyancies_m14142F7AA18DAE0CD2BA25EA5F3F7E5796938365 (void);
// 0x000004E2 Obi.ObiNativeFloatList Obi.ObiSolver::get_restDensities()
extern void ObiSolver_get_restDensities_mA85A3F3E9C265A9E3B01B957691808F072123C96 (void);
// 0x000004E3 Obi.ObiNativeFloatList Obi.ObiSolver::get_viscosities()
extern void ObiSolver_get_viscosities_mBCDE34198266ABADF85472134F8A69F897BB37D6 (void);
// 0x000004E4 Obi.ObiNativeFloatList Obi.ObiSolver::get_surfaceTension()
extern void ObiSolver_get_surfaceTension_m06BBC45CF2C65E927490672F37C89AD69ACE039A (void);
// 0x000004E5 Obi.ObiNativeFloatList Obi.ObiSolver::get_vortConfinement()
extern void ObiSolver_get_vortConfinement_mBCA2B8E737E63A3DADDC34F1E7E7C4D26F7A71D0 (void);
// 0x000004E6 Obi.ObiNativeFloatList Obi.ObiSolver::get_atmosphericDrag()
extern void ObiSolver_get_atmosphericDrag_m81584993AAF86345E489C4D833D1BAF3E704CB16 (void);
// 0x000004E7 Obi.ObiNativeFloatList Obi.ObiSolver::get_atmosphericPressure()
extern void ObiSolver_get_atmosphericPressure_m363045FB67ED30D2413C84317FBF2033FF9BE1B6 (void);
// 0x000004E8 Obi.ObiNativeFloatList Obi.ObiSolver::get_diffusion()
extern void ObiSolver_get_diffusion_m614EC192C40E1DB26E20E2B7AEEB1E3853E1CBAA (void);
// 0x000004E9 System.Void Obi.ObiSolver::Update()
extern void ObiSolver_Update_m3432EE148E4943BFEA7F248CC1ADF410EB009532 (void);
// 0x000004EA System.Void Obi.ObiSolver::OnDestroy()
extern void ObiSolver_OnDestroy_m10E800119F5492F06C454965798031E2959B4D91 (void);
// 0x000004EB System.Void Obi.ObiSolver::CreateBackend()
extern void ObiSolver_CreateBackend_m91B701B5D03356B869ECCC5E5B4E0F9E82CA7377 (void);
// 0x000004EC System.Void Obi.ObiSolver::Initialize()
extern void ObiSolver_Initialize_m69393E4E890B2AF174E313FAB09AF20C8858713B (void);
// 0x000004ED System.Void Obi.ObiSolver::Teardown()
extern void ObiSolver_Teardown_m7AFA06EFC9E0BA723690DF91837C3E3B8D6C30C1 (void);
// 0x000004EE System.Void Obi.ObiSolver::UpdateBackend()
extern void ObiSolver_UpdateBackend_mED822610A9CB49AC6197C9EB12A70EBF7F5959A5 (void);
// 0x000004EF System.Void Obi.ObiSolver::FreeRigidbodyArrays()
extern void ObiSolver_FreeRigidbodyArrays_m75E7122190DD0CFFE6594F39BED2EE9685488FA2 (void);
// 0x000004F0 System.Void Obi.ObiSolver::EnsureRigidbodyArraysCapacity(System.Int32)
extern void ObiSolver_EnsureRigidbodyArraysCapacity_mC0A5656D8985DE06BC6084750EC6AE266B7E059A (void);
// 0x000004F1 System.Void Obi.ObiSolver::FreeParticleArrays()
extern void ObiSolver_FreeParticleArrays_mC803A0F5E3291D91B643E13F40F56A1DD906DB19 (void);
// 0x000004F2 System.Void Obi.ObiSolver::EnsureParticleArraysCapacity(System.Int32)
extern void ObiSolver_EnsureParticleArraysCapacity_mA3FF2925ACDE72EC10DF9853E51FF3440F27EB67 (void);
// 0x000004F3 System.Void Obi.ObiSolver::AllocateParticles(System.Int32[])
extern void ObiSolver_AllocateParticles_m2437F6049C3AB8F791378C25BB220F3D447E9209 (void);
// 0x000004F4 System.Void Obi.ObiSolver::FreeParticles(System.Int32[])
extern void ObiSolver_FreeParticles_m4CD1BFC3A67A853618E2BC48B666735D57D69CBB (void);
// 0x000004F5 System.Boolean Obi.ObiSolver::AddActor(Obi.ObiActor)
extern void ObiSolver_AddActor_m33CCB3774385993AD4756D44613032EDF7602076 (void);
// 0x000004F6 System.Boolean Obi.ObiSolver::RemoveActor(Obi.ObiActor)
extern void ObiSolver_RemoveActor_m9816A116E0848361565F850D70B3B8A876ACC5A2 (void);
// 0x000004F7 System.Void Obi.ObiSolver::PushSolverParameters()
extern void ObiSolver_PushSolverParameters_mC566D7A04AABE0FD982060FDDCDF94BF0330A898 (void);
// 0x000004F8 Oni_ConstraintParameters Obi.ObiSolver::GetConstraintParameters(Oni_ConstraintType)
extern void ObiSolver_GetConstraintParameters_m612C27EA24521064F789D15FABB06E8320EC50DD (void);
// 0x000004F9 Obi.IObiConstraints Obi.ObiSolver::GetConstraintsByType(Oni_ConstraintType)
extern void ObiSolver_GetConstraintsByType_mE11A373E6629FB18A577363D79AE2626806FB095 (void);
// 0x000004FA System.Void Obi.ObiSolver::PushActiveParticles()
extern void ObiSolver_PushActiveParticles_m699BFA15ECA1837EB8E6FB0C4C06564BC00A4356 (void);
// 0x000004FB System.Void Obi.ObiSolver::PushSimplices()
extern void ObiSolver_PushSimplices_mCF761C06FD3C0023D8AC8CAF847F22856CA29F2B (void);
// 0x000004FC System.Void Obi.ObiSolver::PushConstraints()
extern void ObiSolver_PushConstraints_m1FE372176ADB5BDDB3C11F042850CA126D62EA7D (void);
// 0x000004FD System.Void Obi.ObiSolver::UpdateVisibility()
extern void ObiSolver_UpdateVisibility_m78B2DB3B14730D673C823699F0AF2CEB06524552 (void);
// 0x000004FE System.Void Obi.ObiSolver::InitializeTransformFrame()
extern void ObiSolver_InitializeTransformFrame_m37F3F63EE2DD9DDE17329B3FDF750A4C9AFF317C (void);
// 0x000004FF System.Void Obi.ObiSolver::UpdateTransformFrame(System.Single)
extern void ObiSolver_UpdateTransformFrame_m95D29D02A287E86629B2BF566891B519A5D3B2F4 (void);
// 0x00000500 Obi.IObiJobHandle Obi.ObiSolver::BeginStep(System.Single)
extern void ObiSolver_BeginStep_m08735BDB104DA6C7B894DDCBCE685C8F08EAE8C5 (void);
// 0x00000501 Obi.IObiJobHandle Obi.ObiSolver::Substep(System.Single,System.Single,System.Int32)
extern void ObiSolver_Substep_m8EF42B8FCB4A92B66A18967A5E30C0FAF9D91226 (void);
// 0x00000502 System.Void Obi.ObiSolver::EndStep(System.Single)
extern void ObiSolver_EndStep_m12D95AB19D02AB5C1E2ED1D8F3B1E586831DBA64 (void);
// 0x00000503 System.Void Obi.ObiSolver::Interpolate(System.Single,System.Single)
extern void ObiSolver_Interpolate_mAE7A83C120C8BCD50676E781D540026B6BAEEDE2 (void);
// 0x00000504 System.Void Obi.ObiSolver::SpatialQuery(Obi.ObiNativeQueryShapeList,Obi.ObiNativeAffineTransformList,Obi.ObiNativeQueryResultList)
extern void ObiSolver_SpatialQuery_m6D0D03F289F49B89217E8A449DAABC0CCB90D83B (void);
// 0x00000505 Obi.QueryResult[] Obi.ObiSolver::SpatialQuery(Obi.QueryShape,Obi.AffineTransform)
extern void ObiSolver_SpatialQuery_m5C0D958E05B3BE7A66EFA6E320C0CB6E3A196607 (void);
// 0x00000506 System.Boolean Obi.ObiSolver::Raycast(UnityEngine.Ray,Obi.QueryResult&,System.Int32,System.Single,System.Single)
extern void ObiSolver_Raycast_m8F28F2F638DC71BC4716816A590D3D86E0274409 (void);
// 0x00000507 Obi.QueryResult[] Obi.ObiSolver::Raycast(System.Collections.Generic.List`1<UnityEngine.Ray>,System.Int32,System.Single,System.Single)
extern void ObiSolver_Raycast_mBABB28101E4819C4AC8C9D3189DB8636FD255007 (void);
// 0x00000508 System.Void Obi.ObiSolver::.ctor()
extern void ObiSolver__ctor_mA963ED6A2F8A02E33B2E3BACB178FC2F0436F753 (void);
// 0x00000509 System.Void Obi.ObiSolver::.cctor()
extern void ObiSolver__cctor_m4023459B57CD95EEF9656B8BDA9B18BDD558C46B (void);
// 0x0000050A System.Void Obi.ObiSolver_ObiCollisionEventArgs::.ctor()
extern void ObiCollisionEventArgs__ctor_m346881EB85AB69845A91659208E75756E4EF6D17 (void);
// 0x0000050B System.Void Obi.ObiSolver_ParticleInActor::.ctor()
extern void ParticleInActor__ctor_mAA089A9B7EBB63E8671D8AAB97FD0836220D7EEA (void);
// 0x0000050C System.Void Obi.ObiSolver_ParticleInActor::.ctor(Obi.ObiActor,System.Int32)
extern void ParticleInActor__ctor_m3829A4666F38432208F2811BDE98CD7A8C6B25F3 (void);
// 0x0000050D System.Void Obi.ObiSolver_SolverCallback::.ctor(System.Object,System.IntPtr)
extern void SolverCallback__ctor_mE4083F1B124E6A1FFB21E69EE2AEE7318B28AAD7 (void);
// 0x0000050E System.Void Obi.ObiSolver_SolverCallback::Invoke(Obi.ObiSolver)
extern void SolverCallback_Invoke_m45F6B3AC4BD61E22AAA076A7DBA5544E5C72F057 (void);
// 0x0000050F System.IAsyncResult Obi.ObiSolver_SolverCallback::BeginInvoke(Obi.ObiSolver,System.AsyncCallback,System.Object)
extern void SolverCallback_BeginInvoke_mD89642A3E1F37C4DC118EA6CACD1B6D68CD83A08 (void);
// 0x00000510 System.Void Obi.ObiSolver_SolverCallback::EndInvoke(System.IAsyncResult)
extern void SolverCallback_EndInvoke_m395BC54EFE0998B85005E9E4F66858F146C931F2 (void);
// 0x00000511 System.Void Obi.ObiSolver_SolverStepCallback::.ctor(System.Object,System.IntPtr)
extern void SolverStepCallback__ctor_m2079BE6990F358A8A5837AE21480E58DD4D0EEA3 (void);
// 0x00000512 System.Void Obi.ObiSolver_SolverStepCallback::Invoke(Obi.ObiSolver,System.Single)
extern void SolverStepCallback_Invoke_m68DD32CB29FBD1EC423CC5A72702D2CB05715754 (void);
// 0x00000513 System.IAsyncResult Obi.ObiSolver_SolverStepCallback::BeginInvoke(Obi.ObiSolver,System.Single,System.AsyncCallback,System.Object)
extern void SolverStepCallback_BeginInvoke_m434058E191E84C9A47950451E2B46A1F8F957F40 (void);
// 0x00000514 System.Void Obi.ObiSolver_SolverStepCallback::EndInvoke(System.IAsyncResult)
extern void SolverStepCallback_EndInvoke_mF3E0E0DE67ABA86472824000ACA507AB3352A533 (void);
// 0x00000515 System.Void Obi.ObiSolver_CollisionCallback::.ctor(System.Object,System.IntPtr)
extern void CollisionCallback__ctor_mA5A4EE9E910214B425B42DD40A2671EB949D661E (void);
// 0x00000516 System.Void Obi.ObiSolver_CollisionCallback::Invoke(Obi.ObiSolver,Obi.ObiSolver_ObiCollisionEventArgs)
extern void CollisionCallback_Invoke_m941AD708CC00FD8EA47569A5B061BB862B96F98C (void);
// 0x00000517 System.IAsyncResult Obi.ObiSolver_CollisionCallback::BeginInvoke(Obi.ObiSolver,Obi.ObiSolver_ObiCollisionEventArgs,System.AsyncCallback,System.Object)
extern void CollisionCallback_BeginInvoke_m0A88695225D3C02615D754DC211FD3EE3468BFAB (void);
// 0x00000518 System.Void Obi.ObiSolver_CollisionCallback::EndInvoke(System.IAsyncResult)
extern void CollisionCallback_EndInvoke_m46F561EE559B214E9D38C29ABCADAB3336B18A8A (void);
// 0x00000519 System.Void Obi.ObiSolver_<>c::.cctor()
extern void U3CU3Ec__cctor_m89EAB0A60F1D2D1667AB8DC5FFD689526BA4D3DE (void);
// 0x0000051A System.Void Obi.ObiSolver_<>c::.ctor()
extern void U3CU3Ec__ctor_m2D8F2915236AB77A7C36A87C7761B5503DCA10F2 (void);
// 0x0000051B System.Boolean Obi.ObiSolver_<>c::<get_allocParticleCount>b__143_0(Obi.ObiSolver_ParticleInActor)
extern void U3CU3Ec_U3Cget_allocParticleCountU3Eb__143_0_m44CB897A1835DA47864F1E0B7AE082D2592C9FE4 (void);
// 0x0000051C System.Void Obi.ObiFixedUpdater::OnValidate()
extern void ObiFixedUpdater_OnValidate_m5F6FE3B9B8E80E3B125B39F142D2DA57A4DD8BFB (void);
// 0x0000051D System.Void Obi.ObiFixedUpdater::Awake()
extern void ObiFixedUpdater_Awake_m90D52F196706902484B59979C0A7537552FAB320 (void);
// 0x0000051E System.Void Obi.ObiFixedUpdater::OnDisable()
extern void ObiFixedUpdater_OnDisable_mEAE5E8BA5B2A650527D99517C37FBF98173259E9 (void);
// 0x0000051F System.Void Obi.ObiFixedUpdater::FixedUpdate()
extern void ObiFixedUpdater_FixedUpdate_m9CB26DEA62FD0EA6E7FBF86A2CFB12C375B81CE0 (void);
// 0x00000520 System.Void Obi.ObiFixedUpdater::Update()
extern void ObiFixedUpdater_Update_m485E215A24D5B8A2C3B191F2F65615D4C2A27A90 (void);
// 0x00000521 System.Void Obi.ObiFixedUpdater::.ctor()
extern void ObiFixedUpdater__ctor_m129BDE7A45025352A7238A317F1A3E7B31A00C9F (void);
// 0x00000522 System.Void Obi.ObiLateFixedUpdater::OnValidate()
extern void ObiLateFixedUpdater_OnValidate_m4B24969542BF8BAA45ABF2DD0FC90CD910FA27AC (void);
// 0x00000523 System.Void Obi.ObiLateFixedUpdater::Awake()
extern void ObiLateFixedUpdater_Awake_mA03AD9976B8FA43BE9FC155469524FBA224C096B (void);
// 0x00000524 System.Void Obi.ObiLateFixedUpdater::OnEnable()
extern void ObiLateFixedUpdater_OnEnable_m7AA55EFB2018C0FE7A7450DE8B124E9947735430 (void);
// 0x00000525 System.Void Obi.ObiLateFixedUpdater::OnDisable()
extern void ObiLateFixedUpdater_OnDisable_m508D776D30650C73FB72E47AEB5F2F7EFB64DFB9 (void);
// 0x00000526 System.Collections.IEnumerator Obi.ObiLateFixedUpdater::RunLateFixedUpdate()
extern void ObiLateFixedUpdater_RunLateFixedUpdate_mA207589FB295229010BC3ED39CA0A33B465AE4AB (void);
// 0x00000527 System.Void Obi.ObiLateFixedUpdater::LateFixedUpdate()
extern void ObiLateFixedUpdater_LateFixedUpdate_mC459F999C033D7BC22CD0A0BDA9569E93C2C6F2A (void);
// 0x00000528 System.Void Obi.ObiLateFixedUpdater::Update()
extern void ObiLateFixedUpdater_Update_mE16274CC3FCABACAA0430D79903C2298050907B9 (void);
// 0x00000529 System.Void Obi.ObiLateFixedUpdater::.ctor()
extern void ObiLateFixedUpdater__ctor_mA0140EBDF2452F0A415601A43DAE9822CCA249C6 (void);
// 0x0000052A System.Void Obi.ObiLateFixedUpdater_<RunLateFixedUpdate>d__6::.ctor(System.Int32)
extern void U3CRunLateFixedUpdateU3Ed__6__ctor_m3269F746C194951D4FFCB5CFE81A956F7C6913F4 (void);
// 0x0000052B System.Void Obi.ObiLateFixedUpdater_<RunLateFixedUpdate>d__6::System.IDisposable.Dispose()
extern void U3CRunLateFixedUpdateU3Ed__6_System_IDisposable_Dispose_mFEF635C7933D3DD06A06668B01022B79D7348076 (void);
// 0x0000052C System.Boolean Obi.ObiLateFixedUpdater_<RunLateFixedUpdate>d__6::MoveNext()
extern void U3CRunLateFixedUpdateU3Ed__6_MoveNext_m5E828028CAFAD2A27BAD42F54BD73085E6C7F802 (void);
// 0x0000052D System.Object Obi.ObiLateFixedUpdater_<RunLateFixedUpdate>d__6::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CRunLateFixedUpdateU3Ed__6_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m1BC60167DF9644F2049A2591D4130F2190A2A4C9 (void);
// 0x0000052E System.Void Obi.ObiLateFixedUpdater_<RunLateFixedUpdate>d__6::System.Collections.IEnumerator.Reset()
extern void U3CRunLateFixedUpdateU3Ed__6_System_Collections_IEnumerator_Reset_m55403139467BF8AF39A859E7823363583E55162B (void);
// 0x0000052F System.Object Obi.ObiLateFixedUpdater_<RunLateFixedUpdate>d__6::System.Collections.IEnumerator.get_Current()
extern void U3CRunLateFixedUpdateU3Ed__6_System_Collections_IEnumerator_get_Current_m8E98B873A2E9B9BE3DEE0CA669677BA2ECDFE0FF (void);
// 0x00000530 System.Void Obi.ObiLateUpdater::OnValidate()
extern void ObiLateUpdater_OnValidate_m6360BD327E35B0C56A590D51CB066B811DAD1774 (void);
// 0x00000531 System.Void Obi.ObiLateUpdater::LateUpdate()
extern void ObiLateUpdater_LateUpdate_m04DE9233394AE2CC13CF1DA4F136364E81FE4C06 (void);
// 0x00000532 System.Void Obi.ObiLateUpdater::.ctor()
extern void ObiLateUpdater__ctor_mD001E04F8FFF222B20A8ECDBE7A8C2507C2F0894 (void);
// 0x00000533 System.Void Obi.ObiUpdater::BeginStep(System.Single)
extern void ObiUpdater_BeginStep_m871A82CAEA561457F1FB19B27ADC346661839AA5 (void);
// 0x00000534 System.Void Obi.ObiUpdater::Substep(System.Single,System.Single,System.Int32)
extern void ObiUpdater_Substep_m44F3765E77796D6D71287FF2883FF2E496E0767F (void);
// 0x00000535 System.Void Obi.ObiUpdater::EndStep(System.Single)
extern void ObiUpdater_EndStep_mD6CEC404A973E002CD4D81FC87442C77F537E55E (void);
// 0x00000536 System.Void Obi.ObiUpdater::Interpolate(System.Single,System.Single)
extern void ObiUpdater_Interpolate_m38784F191B281CBF7A3DE79B71EA71ED2FE60681 (void);
// 0x00000537 System.Void Obi.ObiUpdater::.ctor()
extern void ObiUpdater__ctor_m3A352531659D7BCCB0CF2AFF0732D3B28AB26092 (void);
// 0x00000538 System.Void Obi.ObiUpdater::.cctor()
extern void ObiUpdater__cctor_m272383BDBA28D7F89B9965940A9188BDE81EA1A9 (void);
// 0x00000539 System.Void Obi.ChildrenOnly::.ctor()
extern void ChildrenOnly__ctor_m4D3FAAA742E61B7FA2323CE9F448A1877F87CAE0 (void);
// 0x0000053A System.Void Obi.Indent::.ctor()
extern void Indent__ctor_m5A092B8D80C878888D885C6ADC0B3A9D3A90E3C7 (void);
// 0x0000053B System.Single Obi.InspectorButtonAttribute::get_ButtonWidth()
extern void InspectorButtonAttribute_get_ButtonWidth_mE4C1BF731F1203ABCDC7FF911FBC9AA619ECC4FB (void);
// 0x0000053C System.Void Obi.InspectorButtonAttribute::set_ButtonWidth(System.Single)
extern void InspectorButtonAttribute_set_ButtonWidth_mD7A12A99D234B3911E6D035C0224BC6AB4CC773A (void);
// 0x0000053D System.Void Obi.InspectorButtonAttribute::.ctor(System.String)
extern void InspectorButtonAttribute__ctor_m975A49E0C15C48813192407A712BDB21DCC86987 (void);
// 0x0000053E System.Void Obi.InspectorButtonAttribute::.cctor()
extern void InspectorButtonAttribute__cctor_mD9D19B7D28E89349026C4FBCAF129D59060E0349 (void);
// 0x0000053F System.Void Obi.MinMaxAttribute::.ctor(System.Single,System.Single)
extern void MinMaxAttribute__ctor_mAEC8FD68CEFC58D324E8BBF941E6940654EF2F78 (void);
// 0x00000540 System.Void Obi.MultiDelayed::.ctor()
extern void MultiDelayed__ctor_mC38D0EAB98C18CE8CCEBCC3767678783367BC0F5 (void);
// 0x00000541 System.Void Obi.MultiPropertyAttribute::.ctor()
extern void MultiPropertyAttribute__ctor_m0CEA425DBF3D690C2FBF18251EA3D45B8A2EEEFD (void);
// 0x00000542 System.Void Obi.MultiRange::.ctor(System.Single,System.Single)
extern void MultiRange__ctor_mB97A29FE677F7549CF13B4C38DEAA868640876C1 (void);
// 0x00000543 System.String Obi.SerializeProperty::get_PropertyName()
extern void SerializeProperty_get_PropertyName_mDC8A03791EF5C0FC8DB9BEE2645D05EC456A9B21 (void);
// 0x00000544 System.Void Obi.SerializeProperty::set_PropertyName(System.String)
extern void SerializeProperty_set_PropertyName_m0D097B39C9F982C6DA9CFE395012ED31AE075A55 (void);
// 0x00000545 System.Void Obi.SerializeProperty::.ctor(System.String)
extern void SerializeProperty__ctor_mF607BA112BF60A359F1C32ECA1F8F473737FE5E3 (void);
// 0x00000546 System.String Obi.VisibleIf::get_MethodName()
extern void VisibleIf_get_MethodName_mA9D547283CA5003031A6052EC2F01BDD20626C79 (void);
// 0x00000547 System.Void Obi.VisibleIf::set_MethodName(System.String)
extern void VisibleIf_set_MethodName_mEEE7DE756E758BB291ECC2AF95FB9331577A89BD (void);
// 0x00000548 System.Boolean Obi.VisibleIf::get_Negate()
extern void VisibleIf_get_Negate_m70429C0B2F6E15B7329A21127ED7F095B5B9F79C (void);
// 0x00000549 System.Void Obi.VisibleIf::set_Negate(System.Boolean)
extern void VisibleIf_set_Negate_m7AE9FD0E091984A71A593C3FE0A0A4CB89CDDF7E (void);
// 0x0000054A System.Void Obi.VisibleIf::.ctor(System.String,System.Boolean)
extern void VisibleIf__ctor_m74CCE1E688720090402D0E285F11AA7EE004E6C6 (void);
// 0x0000054B System.Object Obi.CoroutineJob::get_Result()
extern void CoroutineJob_get_Result_mD748A43832803B6F5A24988519355E0B5E960BD3 (void);
// 0x0000054C System.Boolean Obi.CoroutineJob::get_IsDone()
extern void CoroutineJob_get_IsDone_m4278531E6DBB4F3ED38BEB44A7D3365F6008420B (void);
// 0x0000054D System.Boolean Obi.CoroutineJob::get_RaisedException()
extern void CoroutineJob_get_RaisedException_mB7118E9ABE0C6CC7C00D741FC8BC5E859CC81F9A (void);
// 0x0000054E System.Void Obi.CoroutineJob::Init()
extern void CoroutineJob_Init_m08092162CA93D4B06D6F23B48138F018CA204416 (void);
// 0x0000054F System.Object Obi.CoroutineJob::RunSynchronously(System.Collections.IEnumerator)
extern void CoroutineJob_RunSynchronously_mC0C37C33DF39624CE6811D4D2229BF1DBB004DAB (void);
// 0x00000550 System.Collections.IEnumerator Obi.CoroutineJob::Start(System.Collections.IEnumerator)
extern void CoroutineJob_Start_m80C8E17CD749B13B237150583A6280C213F8C39F (void);
// 0x00000551 System.Void Obi.CoroutineJob::Stop()
extern void CoroutineJob_Stop_m503EBD4DCD2DB059CA0681D562A66E37442C7E4B (void);
// 0x00000552 System.Void Obi.CoroutineJob::.ctor()
extern void CoroutineJob__ctor_m3B1FC813C2F585D8B745A74C9F40059C41C72437 (void);
// 0x00000553 System.Void Obi.CoroutineJob_ProgressInfo::.ctor(System.String,System.Single)
extern void ProgressInfo__ctor_m61203218594B2063E536F24871C934C1CA813DA1 (void);
// 0x00000554 System.Void Obi.CoroutineJob_<Start>d__15::.ctor(System.Int32)
extern void U3CStartU3Ed__15__ctor_mD7A6880AAEB86FECE339EAA24C276AA5D91FE5AD (void);
// 0x00000555 System.Void Obi.CoroutineJob_<Start>d__15::System.IDisposable.Dispose()
extern void U3CStartU3Ed__15_System_IDisposable_Dispose_m4542E430FBE633F30A5F1B001DA8C7D01BE636F1 (void);
// 0x00000556 System.Boolean Obi.CoroutineJob_<Start>d__15::MoveNext()
extern void U3CStartU3Ed__15_MoveNext_mF349A26AF1025AF7487E1BB1D725340CD887C18F (void);
// 0x00000557 System.Object Obi.CoroutineJob_<Start>d__15::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CStartU3Ed__15_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m857DB02265865988D8A060D28C87EAB01F6DEC55 (void);
// 0x00000558 System.Void Obi.CoroutineJob_<Start>d__15::System.Collections.IEnumerator.Reset()
extern void U3CStartU3Ed__15_System_Collections_IEnumerator_Reset_mC889240009E6BB1F36B3BC74F618A35A604608B3 (void);
// 0x00000559 System.Object Obi.CoroutineJob_<Start>d__15::System.Collections.IEnumerator.get_Current()
extern void U3CStartU3Ed__15_System_Collections_IEnumerator_get_Current_mE937748101E5BB647A3762B97A9DE2FED38D8283 (void);
// 0x0000055A System.Void Obi.EditorCoroutine::ShowCoroutineProgressBar(System.String,System.Collections.IEnumerator&)
extern void EditorCoroutine_ShowCoroutineProgressBar_mBD8BA0BAB4A8F6DBA32D5EB38A2AC2598992E522 (void);
// 0x0000055B System.Void Obi.EditorCoroutine::.ctor()
extern void EditorCoroutine__ctor_mDA14C1513A94EABA131D822C5BC96C78411A872E (void);
// 0x0000055C System.Void Obi.ObiAmbientForceZone::ApplyForcesToActor(Obi.ObiActor)
extern void ObiAmbientForceZone_ApplyForcesToActor_mAE4A4FC41002FA3D906FEA74194C377D2C60A8D8 (void);
// 0x0000055D System.Void Obi.ObiAmbientForceZone::OnDrawGizmosSelected()
extern void ObiAmbientForceZone_OnDrawGizmosSelected_m9F4A990AC63D31EFB4E459DDDE79DA6D99ED98DE (void);
// 0x0000055E System.Void Obi.ObiAmbientForceZone::.ctor()
extern void ObiAmbientForceZone__ctor_mD887D3FBC82BCD8DF7720A9B25BF72572118351E (void);
// 0x0000055F System.Void Obi.ObiExternalForce::OnEnable()
extern void ObiExternalForce_OnEnable_m0FB8D535D2ED4F394E48424E48C073B8E3FBD10F (void);
// 0x00000560 System.Void Obi.ObiExternalForce::OnDisable()
extern void ObiExternalForce_OnDisable_mD1867136686D828057C7DF70B6A77B5DE81E459C (void);
// 0x00000561 System.Void Obi.ObiExternalForce::Solver_OnStepBegin(Obi.ObiSolver,System.Single)
extern void ObiExternalForce_Solver_OnStepBegin_mC46587312AE49CD7183DAD9D16BBB66C9BF64E0E (void);
// 0x00000562 System.Single Obi.ObiExternalForce::GetTurbulence(System.Single)
extern void ObiExternalForce_GetTurbulence_m36B324BC86245A69B5FFB5D924E08E35BCEC135D (void);
// 0x00000563 System.Void Obi.ObiExternalForce::ApplyForcesToActor(Obi.ObiActor)
// 0x00000564 System.Void Obi.ObiExternalForce::.ctor()
extern void ObiExternalForce__ctor_mD434F0876366D23C0D68DAB6A8949E018A42ACBD (void);
// 0x00000565 System.Void Obi.ObiSphericalForceZone::ApplyForcesToActor(Obi.ObiActor)
extern void ObiSphericalForceZone_ApplyForcesToActor_m506A142D9EF2B82CD7310E150EF302F8A6B2FA2B (void);
// 0x00000566 System.Void Obi.ObiSphericalForceZone::OnDrawGizmosSelected()
extern void ObiSphericalForceZone_OnDrawGizmosSelected_m428130DB0C2E2D23A31E3D0FFE00EDA0FB6744B2 (void);
// 0x00000567 System.Void Obi.ObiSphericalForceZone::.ctor()
extern void ObiSphericalForceZone__ctor_mCB4320E2FDC045FAC6D25069AA629243434775F0 (void);
// 0x00000568 System.Int32 Obi.ObiContactEventDispatcher::CompareByRef(Oni_Contact&,Oni_Contact&,Obi.ObiSolver)
extern void ObiContactEventDispatcher_CompareByRef_mCC27E2711928972C0B16573985711C3432D1719E (void);
// 0x00000569 System.Void Obi.ObiContactEventDispatcher::Awake()
extern void ObiContactEventDispatcher_Awake_m4C9DC422D39BD21B3BE61C3BE4EB262BD0FECAD9 (void);
// 0x0000056A System.Void Obi.ObiContactEventDispatcher::OnEnable()
extern void ObiContactEventDispatcher_OnEnable_m9E49028DB875725C353943480CB5C447179953DC (void);
// 0x0000056B System.Void Obi.ObiContactEventDispatcher::OnDisable()
extern void ObiContactEventDispatcher_OnDisable_m8F75848B0553FD8776A22ABD1253D891B592CFA0 (void);
// 0x0000056C System.Int32 Obi.ObiContactEventDispatcher::FilterOutDistantContacts(Oni_Contact[],System.Int32)
extern void ObiContactEventDispatcher_FilterOutDistantContacts_m2088093F9BFC093F73ECB24BDC9E6DA7A748962B (void);
// 0x0000056D System.Int32 Obi.ObiContactEventDispatcher::RemoveDuplicates(Oni_Contact[],System.Int32)
extern void ObiContactEventDispatcher_RemoveDuplicates_m394D908408DBDED555F844001682FD217E94C5F9 (void);
// 0x0000056E System.Void Obi.ObiContactEventDispatcher::InvokeCallbacks(Oni_Contact[],System.Int32)
extern void ObiContactEventDispatcher_InvokeCallbacks_mB588492B596248DBC043C8235D2AD8D3AC3AC423 (void);
// 0x0000056F System.Void Obi.ObiContactEventDispatcher::Solver_OnCollision(System.Object,Obi.ObiSolver_ObiCollisionEventArgs)
extern void ObiContactEventDispatcher_Solver_OnCollision_m12E8F9442701D7911DE4F0D18CB0F4EC82F263FC (void);
// 0x00000570 System.Void Obi.ObiContactEventDispatcher::.ctor()
extern void ObiContactEventDispatcher__ctor_m357D739552531DA027D8F320CCFAC444814BD791 (void);
// 0x00000571 System.Void Obi.ObiContactEventDispatcher_ContactComparer::.ctor(Obi.ObiSolver)
extern void ContactComparer__ctor_m5DC2082E03C9DA3062C2D13D3F1702B3763C4379 (void);
// 0x00000572 System.Int32 Obi.ObiContactEventDispatcher_ContactComparer::Compare(Oni_Contact,Oni_Contact)
extern void ContactComparer_Compare_mB97171F79E54851FEF29ACC2ED3F06C5A1452362 (void);
// 0x00000573 System.Void Obi.ObiContactEventDispatcher_ContactCallback::.ctor()
extern void ContactCallback__ctor_m77F3DD3B53374589F094279143B1638E57919AD3 (void);
// 0x00000574 System.Void Obi.ObiFluidSplatter::Awake()
extern void ObiFluidSplatter_Awake_m180B1F61A1609936F14809C864A905D4A4D40F2C (void);
// 0x00000575 System.Void Obi.ObiFluidSplatter::OnEnable()
extern void ObiFluidSplatter_OnEnable_mBDCEEDC5BED2E9C0F5111461825B0D4B56749FDB (void);
// 0x00000576 System.Void Obi.ObiFluidSplatter::OnDisable()
extern void ObiFluidSplatter_OnDisable_mDC1FF267D05B6458DB05FA28C8F8B562740F21F7 (void);
// 0x00000577 System.Void Obi.ObiFluidSplatter::Solver_OnCollision(System.Object,Obi.ObiSolver_ObiCollisionEventArgs)
extern void ObiFluidSplatter_Solver_OnCollision_mE276A022CC5F710B72A8ABFD17ED05806105B5A3 (void);
// 0x00000578 System.Void Obi.ObiFluidSplatter::Splat(UnityEngine.Ray)
extern void ObiFluidSplatter_Splat_mA9CB94A6D812366143B19EF2E4598738883D2202 (void);
// 0x00000579 System.Void Obi.ObiFluidSplatter::.ctor()
extern void ObiFluidSplatter__ctor_mD7383422A06AD16985364BA58C7D725B563842CF (void);
// 0x0000057A System.Void Obi.ObiFoamGenerator::Awake()
extern void ObiFoamGenerator_Awake_mDB72B0B94E32EC83533994FADE8240683A89032E (void);
// 0x0000057B System.Void Obi.ObiFoamGenerator::Subscribe()
extern void ObiFoamGenerator_Subscribe_mC830ADA3F8CE03DFD5DF0AC5B3CCC7F93E67E96D (void);
// 0x0000057C System.Void Obi.ObiFoamGenerator::Unsubscribe()
extern void ObiFoamGenerator_Unsubscribe_m6AC698705FF8B6D1FC8C5ECE8AA1844F9ABA2941 (void);
// 0x0000057D System.Void Obi.ObiFoamGenerator::OnEnable()
extern void ObiFoamGenerator_OnEnable_mED12FEDE21B0EF34DD54C441B017A87CB17E39A5 (void);
// 0x0000057E System.Void Obi.ObiFoamGenerator::OnDisable()
extern void ObiFoamGenerator_OnDisable_mA5011CA8707BD429697E92CA70FB74F9D6E3C2F6 (void);
// 0x0000057F System.Void Obi.ObiFoamGenerator::Emitter_Solver_OnFrameEnd(Obi.ObiSolver)
extern void ObiFoamGenerator_Emitter_Solver_OnFrameEnd_m544658BBC292FFE97627EB7A4CE20FA9845FBF1D (void);
// 0x00000580 System.Void Obi.ObiFoamGenerator::.ctor()
extern void ObiFoamGenerator__ctor_m255CB16E7C25BBAEC9D02B40DE5366CA40063634 (void);
// 0x00000581 System.Void Obi.ObiFoamGenerator::<Awake>b__9_0(Obi.ObiActor,Obi.ObiActorBlueprint)
extern void ObiFoamGenerator_U3CAwakeU3Eb__9_0_mB3999207C0001BBF12FD700855CD29BC88008E6C (void);
// 0x00000582 System.Void Obi.ObiFoamGenerator::<Awake>b__9_1(Obi.ObiActor,Obi.ObiActorBlueprint)
extern void ObiFoamGenerator_U3CAwakeU3Eb__9_1_m877DE629D12722C15AC8C9490921EDC6D9C479E4 (void);
// 0x00000583 Obi.ObiActor Obi.ObiParticleAttachment::get_actor()
extern void ObiParticleAttachment_get_actor_m8A6C711886370813BD7BC301BFB6C7812B6B193A (void);
// 0x00000584 UnityEngine.Transform Obi.ObiParticleAttachment::get_target()
extern void ObiParticleAttachment_get_target_m87F09D825C3B834F3F32869BA35539B4525FA08E (void);
// 0x00000585 System.Void Obi.ObiParticleAttachment::set_target(UnityEngine.Transform)
extern void ObiParticleAttachment_set_target_m05F5DB1086FA979873CCECE322EDE77C26550DF7 (void);
// 0x00000586 Obi.ObiParticleGroup Obi.ObiParticleAttachment::get_particleGroup()
extern void ObiParticleAttachment_get_particleGroup_mE66342EC326E748311096EBF6508598B2D0E9CC1 (void);
// 0x00000587 System.Void Obi.ObiParticleAttachment::set_particleGroup(Obi.ObiParticleGroup)
extern void ObiParticleAttachment_set_particleGroup_mD2ACDCEDFFDECA0B1737C92F4E2941CD93BEFDE7 (void);
// 0x00000588 System.Boolean Obi.ObiParticleAttachment::get_isBound()
extern void ObiParticleAttachment_get_isBound_mD1067521C804A334B7A28CA3A9BD65237C0FD29D (void);
// 0x00000589 Obi.ObiParticleAttachment_AttachmentType Obi.ObiParticleAttachment::get_attachmentType()
extern void ObiParticleAttachment_get_attachmentType_m3AE549A02BCF14D72CE6EDCC93541FD03EA3B271 (void);
// 0x0000058A System.Void Obi.ObiParticleAttachment::set_attachmentType(Obi.ObiParticleAttachment_AttachmentType)
extern void ObiParticleAttachment_set_attachmentType_m5631ABD4AF1A1AFB91F5851BF869AD3974C2586F (void);
// 0x0000058B System.Boolean Obi.ObiParticleAttachment::get_constrainOrientation()
extern void ObiParticleAttachment_get_constrainOrientation_mD07EF64C3AE8F1F9CE99F747CF0D12C2DE7D6E1D (void);
// 0x0000058C System.Void Obi.ObiParticleAttachment::set_constrainOrientation(System.Boolean)
extern void ObiParticleAttachment_set_constrainOrientation_m17A07949BE7D8BBF51FCE020DFB2B07DA73A722F (void);
// 0x0000058D System.Single Obi.ObiParticleAttachment::get_compliance()
extern void ObiParticleAttachment_get_compliance_mF04CA0E1C82403AC5039D8F3816CB750E16E22D9 (void);
// 0x0000058E System.Void Obi.ObiParticleAttachment::set_compliance(System.Single)
extern void ObiParticleAttachment_set_compliance_m559FB99156912E50F36AE6239AEEF8DCE995A132 (void);
// 0x0000058F System.Single Obi.ObiParticleAttachment::get_breakThreshold()
extern void ObiParticleAttachment_get_breakThreshold_mA89F93A18D03F1DADE699A97634716D15617B02F (void);
// 0x00000590 System.Void Obi.ObiParticleAttachment::set_breakThreshold(System.Single)
extern void ObiParticleAttachment_set_breakThreshold_mF3F55D89A7E9E6EEF600D372A34DB537A752EB06 (void);
// 0x00000591 System.Void Obi.ObiParticleAttachment::Awake()
extern void ObiParticleAttachment_Awake_m3F7F1CA4DFB4B2C06A8F98ACAA99335C589D62A8 (void);
// 0x00000592 System.Void Obi.ObiParticleAttachment::OnDestroy()
extern void ObiParticleAttachment_OnDestroy_m5F5AB114D3BB369BF21F771A3F56EE68D5CF47B7 (void);
// 0x00000593 System.Void Obi.ObiParticleAttachment::OnEnable()
extern void ObiParticleAttachment_OnEnable_mE67B8CA340675DC78907D05D206A0B32B47C8F6E (void);
// 0x00000594 System.Void Obi.ObiParticleAttachment::OnDisable()
extern void ObiParticleAttachment_OnDisable_m970C2F665946F9526288866F57E00845E57F6231 (void);
// 0x00000595 System.Void Obi.ObiParticleAttachment::OnValidate()
extern void ObiParticleAttachment_OnValidate_m8F4069C273A48281199B02A40AECFF1D83EA0C32 (void);
// 0x00000596 System.Void Obi.ObiParticleAttachment::Actor_OnBlueprintLoaded(Obi.ObiActor,Obi.ObiActorBlueprint)
extern void ObiParticleAttachment_Actor_OnBlueprintLoaded_m5736115ED7F6C5E1935D2217E7E5DBFDB5B20487 (void);
// 0x00000597 System.Void Obi.ObiParticleAttachment::Actor_OnPrepareStep(Obi.ObiActor,System.Single)
extern void ObiParticleAttachment_Actor_OnPrepareStep_m9EA64FC23D3365CA10F37123637E56D333D6A2A8 (void);
// 0x00000598 System.Void Obi.ObiParticleAttachment::Actor_OnEndStep(Obi.ObiActor,System.Single)
extern void ObiParticleAttachment_Actor_OnEndStep_m09B95B61E9325F33E9DF9B85945979C11962E0C1 (void);
// 0x00000599 System.Void Obi.ObiParticleAttachment::Bind()
extern void ObiParticleAttachment_Bind_m41671D060988813057E5D6CB710621B387C3B4C7 (void);
// 0x0000059A System.Void Obi.ObiParticleAttachment::EnableAttachment(Obi.ObiParticleAttachment_AttachmentType)
extern void ObiParticleAttachment_EnableAttachment_mB6BAA968F14899C9E41D3827506F5A05E45247CF (void);
// 0x0000059B System.Void Obi.ObiParticleAttachment::DisableAttachment(Obi.ObiParticleAttachment_AttachmentType)
extern void ObiParticleAttachment_DisableAttachment_m91236D7F5CFA0439C6EB8D250816CB587AD6392A (void);
// 0x0000059C System.Void Obi.ObiParticleAttachment::UpdateAttachment()
extern void ObiParticleAttachment_UpdateAttachment_m5537ED01556A4DC62E3F46AB5D43A9E2C875856A (void);
// 0x0000059D System.Void Obi.ObiParticleAttachment::BreakDynamicAttachment(System.Single)
extern void ObiParticleAttachment_BreakDynamicAttachment_m3AA55F0956AA144B3AC657224DF303EBACBB9022 (void);
// 0x0000059E System.Void Obi.ObiParticleAttachment::.ctor()
extern void ObiParticleAttachment__ctor_mAC181D9F03BE52C0DB29C07F539A74C0DEA1744B (void);
// 0x0000059F System.Void Obi.ObiParticleDragger::OnEnable()
extern void ObiParticleDragger_OnEnable_m72B8C23953592A731785106A8459284845816A55 (void);
// 0x000005A0 System.Void Obi.ObiParticleDragger::OnDisable()
extern void ObiParticleDragger_OnDisable_mB11CAEAA8554518A0B474976561C8FEF0D6E8E49 (void);
// 0x000005A1 System.Void Obi.ObiParticleDragger::FixedUpdate()
extern void ObiParticleDragger_FixedUpdate_m9851F3C063BF90687B18E10701B7B1F5193A8E6E (void);
// 0x000005A2 System.Void Obi.ObiParticleDragger::Picker_OnParticleDragged(Obi.ObiParticlePicker_ParticlePickEventArgs)
extern void ObiParticleDragger_Picker_OnParticleDragged_mECCECAF2424C1D2656A6601ED602BFEA343EBB7C (void);
// 0x000005A3 System.Void Obi.ObiParticleDragger::Picker_OnParticleReleased(Obi.ObiParticlePicker_ParticlePickEventArgs)
extern void ObiParticleDragger_Picker_OnParticleReleased_mD5AFBB9569AFDD6F33D4E148A5A63B542331C87C (void);
// 0x000005A4 System.Void Obi.ObiParticleDragger::.ctor()
extern void ObiParticleDragger__ctor_mC7F5FD7D2B50D344D2D85CDA6CB55EA76DFD7A73 (void);
// 0x000005A5 System.Void Obi.ObiParticleGridDebugger::OnEnable()
extern void ObiParticleGridDebugger_OnEnable_mDCE0CF67BDC98D81F9FB3B4AA3DA73485F24A8E3 (void);
// 0x000005A6 System.Void Obi.ObiParticleGridDebugger::OnDisable()
extern void ObiParticleGridDebugger_OnDisable_m6EEAA88C141739F08763B496873E4FB8583ABA2D (void);
// 0x000005A7 System.Void Obi.ObiParticleGridDebugger::LateUpdate()
extern void ObiParticleGridDebugger_LateUpdate_m7A114715F2660DE5C8004F414580ABAB4B6BD796 (void);
// 0x000005A8 System.Void Obi.ObiParticleGridDebugger::OnDrawGizmos()
extern void ObiParticleGridDebugger_OnDrawGizmos_mFF5DD489989676AD3C6BACA195FE47F4E18C22AC (void);
// 0x000005A9 System.Void Obi.ObiParticleGridDebugger::.ctor()
extern void ObiParticleGridDebugger__ctor_mC3547171B3F6C6C08F8E759186D7007E186A29B8 (void);
// 0x000005AA System.Void Obi.ObiParticlePicker::Awake()
extern void ObiParticlePicker_Awake_m74E07553D81756450A0D1B8351E1CBF8142F8F11 (void);
// 0x000005AB System.Void Obi.ObiParticlePicker::LateUpdate()
extern void ObiParticlePicker_LateUpdate_m9E75050EBAC6B52C81851303DEE0B2EA70353CF1 (void);
// 0x000005AC System.Void Obi.ObiParticlePicker::.ctor()
extern void ObiParticlePicker__ctor_m5818ABF9055AD22D294ED1EB13021BC9FE8AB3A9 (void);
// 0x000005AD System.Void Obi.ObiParticlePicker_ParticlePickEventArgs::.ctor(System.Int32,UnityEngine.Vector3)
extern void ParticlePickEventArgs__ctor_m64293DD03DECF493CE8EBD7844C21D2746C09EF0 (void);
// 0x000005AE System.Void Obi.ObiParticlePicker_ParticlePickUnityEvent::.ctor()
extern void ParticlePickUnityEvent__ctor_m8E79C656B1CDE3FB655B899990D215C91879BFA0 (void);
// 0x000005AF System.Void Obi.ObiProfiler::Awake()
extern void ObiProfiler_Awake_mAB7F0E70026EC600D00A9130AEA40B9F2DD4D0D7 (void);
// 0x000005B0 System.Void Obi.ObiProfiler::OnDestroy()
extern void ObiProfiler_OnDestroy_m952E74CFF016FB1DB495D76228352ABED2E56166 (void);
// 0x000005B1 System.Void Obi.ObiProfiler::OnEnable()
extern void ObiProfiler_OnEnable_m2FA73A238837F2034B8E2907F2E87C347A07EFDC (void);
// 0x000005B2 System.Void Obi.ObiProfiler::OnDisable()
extern void ObiProfiler_OnDisable_m677BD3207054B10CDE9BAFA6EDC6D7C52DAD7B09 (void);
// 0x000005B3 System.Void Obi.ObiProfiler::EnableProfiler()
extern void ObiProfiler_EnableProfiler_mE07D4D3052ADC88FA8D0D882DF933E094D9F25FD (void);
// 0x000005B4 System.Void Obi.ObiProfiler::DisableProfiler()
extern void ObiProfiler_DisableProfiler_m6E225ACE957C1D38336D2C4FFEEBAC52540241E9 (void);
// 0x000005B5 System.Void Obi.ObiProfiler::BeginSample(System.String,System.Byte)
extern void ObiProfiler_BeginSample_mCA17FA232FD590BF488C6304FC5C4B4B77105DEC (void);
// 0x000005B6 System.Void Obi.ObiProfiler::EndSample()
extern void ObiProfiler_EndSample_mA76E83B9E7BB9DCEA7626780607D999D34868425 (void);
// 0x000005B7 System.Void Obi.ObiProfiler::UpdateProfilerInfo()
extern void ObiProfiler_UpdateProfilerInfo_m89B2E0A641CDEA86AB5195C8CB8CDE7ED666B9DF (void);
// 0x000005B8 System.Void Obi.ObiProfiler::OnGUI()
extern void ObiProfiler_OnGUI_m975F6EE95578505D3E6F1792AD1AFBBAD1375139 (void);
// 0x000005B9 System.Void Obi.ObiProfiler::.ctor()
extern void ObiProfiler__ctor_m3EEB8D1C03DFE08C0EC0E23E334C190F41E40424 (void);
// 0x000005BA System.Void Obi.ObiStitcher::set_Actor1(Obi.ObiActor)
extern void ObiStitcher_set_Actor1_mABB05BE8A26AE51871FE07C7EAA9D53359016679 (void);
// 0x000005BB Obi.ObiActor Obi.ObiStitcher::get_Actor1()
extern void ObiStitcher_get_Actor1_mF7B8097B74CDE0D5491AA8CA0BFB9EA6F6521DE7 (void);
// 0x000005BC System.Void Obi.ObiStitcher::set_Actor2(Obi.ObiActor)
extern void ObiStitcher_set_Actor2_m8D4021102EDF55C787F2AFB5CBD83AEA4443C95E (void);
// 0x000005BD Obi.ObiActor Obi.ObiStitcher::get_Actor2()
extern void ObiStitcher_get_Actor2_mF6FCD44A6E57935EF04782FE6E80FF30A500356E (void);
// 0x000005BE System.Int32 Obi.ObiStitcher::get_StitchCount()
extern void ObiStitcher_get_StitchCount_m0A3C3380AA1B0536814453807186D85002F57744 (void);
// 0x000005BF System.Collections.Generic.IEnumerable`1<Obi.ObiStitcher_Stitch> Obi.ObiStitcher::get_Stitches()
extern void ObiStitcher_get_Stitches_mD04C880005B8B5F6261CFF2E5ABB4C713C27CDFA (void);
// 0x000005C0 System.Void Obi.ObiStitcher::RegisterActor(Obi.ObiActor)
extern void ObiStitcher_RegisterActor_m6CF833604BDC8434EA68BD461456A891CCE1A6B6 (void);
// 0x000005C1 System.Void Obi.ObiStitcher::UnregisterActor(Obi.ObiActor)
extern void ObiStitcher_UnregisterActor_m1C06E9E8003BEAEF75F59ED25B8B56561E5D4A4C (void);
// 0x000005C2 System.Void Obi.ObiStitcher::OnEnable()
extern void ObiStitcher_OnEnable_mDEE87492FEFF02F28EC6D8CB231D8DA3893A8BBC (void);
// 0x000005C3 System.Void Obi.ObiStitcher::OnDisable()
extern void ObiStitcher_OnDisable_m44319848A4EA3CB9AF55AE503389703F57250C00 (void);
// 0x000005C4 System.Int32 Obi.ObiStitcher::AddStitch(System.Int32,System.Int32)
extern void ObiStitcher_AddStitch_m9CDD6C005F4A71C8E942AFCF0EE588AC47CBA1D7 (void);
// 0x000005C5 System.Void Obi.ObiStitcher::RemoveStitch(System.Int32)
extern void ObiStitcher_RemoveStitch_m050637591BDE1882809E3902CD2E7C0FF62EB0CC (void);
// 0x000005C6 System.Void Obi.ObiStitcher::Clear()
extern void ObiStitcher_Clear_mC2BBA753DCB0899C7C486C20565D05F6ED1F1B2F (void);
// 0x000005C7 System.Void Obi.ObiStitcher::Actor_OnBlueprintUnloaded(Obi.ObiActor,Obi.ObiActorBlueprint)
extern void ObiStitcher_Actor_OnBlueprintUnloaded_m6CE4420D85CBD27371B1BCD548FC50F983C7DDC6 (void);
// 0x000005C8 System.Void Obi.ObiStitcher::Actor_OnBlueprintLoaded(Obi.ObiActor,Obi.ObiActorBlueprint)
extern void ObiStitcher_Actor_OnBlueprintLoaded_mF598296F75D17D0F818E4D7F7A1E98F7F3B8200A (void);
// 0x000005C9 System.Void Obi.ObiStitcher::AddToSolver(Obi.ObiSolver)
extern void ObiStitcher_AddToSolver_mACC6329F09C6AC34703CC2C3CDCD6531D7ADCA63 (void);
// 0x000005CA System.Void Obi.ObiStitcher::RemoveFromSolver(Obi.ObiSolver)
extern void ObiStitcher_RemoveFromSolver_m5B3C5E8739E3393D3654A34D0D84972D7582C457 (void);
// 0x000005CB System.Void Obi.ObiStitcher::PushDataToSolver()
extern void ObiStitcher_PushDataToSolver_mA3B052D96FECDB53E4616B27E72A44F77A88EB31 (void);
// 0x000005CC System.Void Obi.ObiStitcher::.ctor()
extern void ObiStitcher__ctor_m8703E582D33DF3E6FB4265F91A5061420D984F97 (void);
// 0x000005CD System.Void Obi.ObiStitcher_Stitch::.ctor(System.Int32,System.Int32)
extern void Stitch__ctor_mA9EBE5BCBF80CAFB90725EF8F4B9E8EA693A9800 (void);
// 0x000005CE System.Void Obi.ObiUtils::DrawArrowGizmo(System.Single,System.Single,System.Single,System.Single)
extern void ObiUtils_DrawArrowGizmo_mB0F16371F7CA701F1934AB1D68E2C543BBEDC6D7 (void);
// 0x000005CF System.Void Obi.ObiUtils::DebugDrawCross(UnityEngine.Vector3,System.Single,UnityEngine.Color)
extern void ObiUtils_DebugDrawCross_m56B5756A7B53EF07A54DA1521D785F4B447A0794 (void);
// 0x000005D0 System.Void Obi.ObiUtils::Swap(T&,T&)
// 0x000005D1 System.Void Obi.ObiUtils::Swap(T[],System.Int32,System.Int32)
// 0x000005D2 System.Void Obi.ObiUtils::Swap(System.Collections.Generic.IList`1<T>,System.Int32,System.Int32)
// 0x000005D3 System.Void Obi.ObiUtils::ShiftLeft(T[],System.Int32,System.Int32,System.Int32)
// 0x000005D4 System.Void Obi.ObiUtils::ShiftRight(T[],System.Int32,System.Int32,System.Int32)
// 0x000005D5 System.Boolean Obi.ObiUtils::AreValid(UnityEngine.Bounds)
extern void ObiUtils_AreValid_m36409B4F74E4DA83A742954433F6CA4EE168BAE6 (void);
// 0x000005D6 UnityEngine.Bounds Obi.ObiUtils::Transform(UnityEngine.Bounds,UnityEngine.Matrix4x4)
extern void ObiUtils_Transform_m733A8AC89C2BFBC7A0E0089512FFC4709454DE88 (void);
// 0x000005D7 System.Int32 Obi.ObiUtils::CountTrailingZeroes(System.Int32)
extern void ObiUtils_CountTrailingZeroes_mD04646F64AE816B0BBE5FE3044C2DED6E90A0333 (void);
// 0x000005D8 System.Void Obi.ObiUtils::Add(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3&)
extern void ObiUtils_Add_m33CB21C40D66A37C46501B8D0F17D03411F7A6B8 (void);
// 0x000005D9 System.Single Obi.ObiUtils::Remap(System.Single,System.Single,System.Single,System.Single,System.Single)
extern void ObiUtils_Remap_m8F506FF64E3B9168DE2B0235E274CC1033B7D86F (void);
// 0x000005DA System.Single Obi.ObiUtils::Mod(System.Single,System.Single)
extern void ObiUtils_Mod_mD2EF1D694D52B0B1882C801A2FD576D8FC49D90E (void);
// 0x000005DB UnityEngine.Matrix4x4 Obi.ObiUtils::Add(UnityEngine.Matrix4x4,UnityEngine.Matrix4x4)
extern void ObiUtils_Add_mC0C822C7DBBF1BA01EE882208292D519B2C9C2D4 (void);
// 0x000005DC System.Single Obi.ObiUtils::FrobeniusNorm(UnityEngine.Matrix4x4)
extern void ObiUtils_FrobeniusNorm_m864D4DA65931429E717193CB634FFAAF31C5DCF4 (void);
// 0x000005DD UnityEngine.Matrix4x4 Obi.ObiUtils::ScalarMultiply(UnityEngine.Matrix4x4,System.Single)
extern void ObiUtils_ScalarMultiply_mE25A02716B8B63129015ACA756D1DB1AD469DE51 (void);
// 0x000005DE UnityEngine.Vector3 Obi.ObiUtils::ProjectPointLine(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3,System.Single&,System.Boolean)
extern void ObiUtils_ProjectPointLine_m39534F2B61F19A53C004F9F72C2D5561C3BFCA15 (void);
// 0x000005DF System.Boolean Obi.ObiUtils::LinePlaneIntersection(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3&)
extern void ObiUtils_LinePlaneIntersection_mA331CE0E861BFF21738B92A85B6009EA4641AD91 (void);
// 0x000005E0 System.Single Obi.ObiUtils::RaySphereIntersection(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3,System.Single)
extern void ObiUtils_RaySphereIntersection_m878269850BD66F1202937DEFF22C3A31ED3B5996 (void);
// 0x000005E1 System.Single Obi.ObiUtils::InvMassToMass(System.Single)
extern void ObiUtils_InvMassToMass_m1ECAAAEDD4E27ADA1FDE1594DACCD249742ED136 (void);
// 0x000005E2 System.Single Obi.ObiUtils::MassToInvMass(System.Single)
extern void ObiUtils_MassToInvMass_m7E1782BA36AD544C22BE9B80A3D222F6D4000B68 (void);
// 0x000005E3 System.Int32 Obi.ObiUtils::PureSign(System.Single)
extern void ObiUtils_PureSign_m92E94D198EF88DC390CCBA619718D6A6C27BA5C5 (void);
// 0x000005E4 System.Void Obi.ObiUtils::NearestPointOnTri(UnityEngine.Vector3&,UnityEngine.Vector3&,UnityEngine.Vector3&,UnityEngine.Vector3&,UnityEngine.Vector3&)
extern void ObiUtils_NearestPointOnTri_m321A69E9A25CCB719C7215829C81D8D8CBF382ED (void);
// 0x000005E5 System.Single Obi.ObiUtils::TriangleArea(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3)
extern void ObiUtils_TriangleArea_m560C3409898B4D67699FCFD0338C3ECDB244BCEB (void);
// 0x000005E6 System.Single Obi.ObiUtils::EllipsoidVolume(UnityEngine.Vector3)
extern void ObiUtils_EllipsoidVolume_m406A1FFC6FADF93B25B2B072064B345E5DA5D2C5 (void);
// 0x000005E7 UnityEngine.Quaternion Obi.ObiUtils::RestDarboux(UnityEngine.Quaternion,UnityEngine.Quaternion)
extern void ObiUtils_RestDarboux_mA5FBE38C5DA10FBD3F920E2F0E96683D3EF91507 (void);
// 0x000005E8 System.Single Obi.ObiUtils::RestBendingConstraint(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3)
extern void ObiUtils_RestBendingConstraint_mA2A2E588C9B5B6907DC2670CF3176A0441D95C78 (void);
// 0x000005E9 System.Collections.IEnumerable Obi.ObiUtils::BilateralInterleaved(System.Int32)
extern void ObiUtils_BilateralInterleaved_m17A79FDAFD53A3C8E7CB5403AC539CD1BC4D5453 (void);
// 0x000005EA System.Void Obi.ObiUtils::BarycentricCoordinates(UnityEngine.Vector3&,UnityEngine.Vector3&,UnityEngine.Vector3&,UnityEngine.Vector3&,UnityEngine.Vector3&)
extern void ObiUtils_BarycentricCoordinates_m3A02D87AC39EA5DB7EC627E892E9F1C3B8801197 (void);
// 0x000005EB System.Void Obi.ObiUtils::BarycentricInterpolation(UnityEngine.Vector3&,UnityEngine.Vector3&,UnityEngine.Vector3&,UnityEngine.Vector3&,UnityEngine.Vector3&)
extern void ObiUtils_BarycentricInterpolation_m3D92C8376763EA18C44BA152EB2C0E19EB7CD4A1 (void);
// 0x000005EC System.Single Obi.ObiUtils::BarycentricInterpolation(System.Single,System.Single,System.Single,UnityEngine.Vector3)
extern void ObiUtils_BarycentricInterpolation_mAC20CA1234A3FB1D1D9AB2AA29D7987C0DF12361 (void);
// 0x000005ED System.Single Obi.ObiUtils::BarycentricExtrapolationScale(UnityEngine.Vector3)
extern void ObiUtils_BarycentricExtrapolationScale_m84AD9B6B2173A5538FDB174D72AA5CE127045D33 (void);
// 0x000005EE UnityEngine.Vector3[] Obi.ObiUtils::CalculateAngleWeightedNormals(UnityEngine.Vector3[],System.Int32[])
extern void ObiUtils_CalculateAngleWeightedNormals_m97CD00C800AA1BD2705A48113DD44B57E450DD34 (void);
// 0x000005EF System.Int32 Obi.ObiUtils::MakePhase(System.Int32,Obi.ObiUtils_ParticleFlags)
extern void ObiUtils_MakePhase_mED5844CB11913FC70EC9041F512C75E0B2F4E167 (void);
// 0x000005F0 System.Int32 Obi.ObiUtils::GetGroupFromPhase(System.Int32)
extern void ObiUtils_GetGroupFromPhase_m95E80301165EB08A322A5043278D698903867054 (void);
// 0x000005F1 Obi.ObiUtils_ParticleFlags Obi.ObiUtils::GetFlagsFromPhase(System.Int32)
extern void ObiUtils_GetFlagsFromPhase_m6712C3AFEE4371CD66C9A5548C96FEF64A990A57 (void);
// 0x000005F2 System.Int32 Obi.ObiUtils::MakeFilter(System.Int32,System.Int32)
extern void ObiUtils_MakeFilter_m8370C56CCFA7758D6E03671DCC2E8F0546BACEF3 (void);
// 0x000005F3 System.Int32 Obi.ObiUtils::GetCategoryFromFilter(System.Int32)
extern void ObiUtils_GetCategoryFromFilter_mE5495C835432A19EE7A32B4E72D104A5179AD9ED (void);
// 0x000005F4 System.Int32 Obi.ObiUtils::GetMaskFromFilter(System.Int32)
extern void ObiUtils_GetMaskFromFilter_mD6E7EF064AF96C8238D05A7EB5504E72CA1ECFCD (void);
// 0x000005F5 System.Void Obi.ObiUtils::EigenSolve(UnityEngine.Matrix4x4,UnityEngine.Vector3&,UnityEngine.Matrix4x4&)
extern void ObiUtils_EigenSolve_m52D85DC48A8905EEFA96A9E0618B6569757370F7 (void);
// 0x000005F6 UnityEngine.Vector3 Obi.ObiUtils::unitOrthogonal(UnityEngine.Vector3)
extern void ObiUtils_unitOrthogonal_m08E2004A970D966D5227E325780C23E6FAB28529 (void);
// 0x000005F7 UnityEngine.Vector3 Obi.ObiUtils::EigenVector(UnityEngine.Matrix4x4,System.Single)
extern void ObiUtils_EigenVector_m650F505BE39681115F5A78F3AB4C76D6B766E360 (void);
// 0x000005F8 UnityEngine.Vector3 Obi.ObiUtils::EigenValues(UnityEngine.Matrix4x4)
extern void ObiUtils_EigenValues_m4B78523278F5C36F028CC1E7AB8FA13BE10FB476 (void);
// 0x000005F9 UnityEngine.Vector3 Obi.ObiUtils::GetPointCloudCentroid(System.Collections.Generic.List`1<UnityEngine.Vector3>)
extern void ObiUtils_GetPointCloudCentroid_m8C1020D867B29A5A516F0DF741330765D4B47BB5 (void);
// 0x000005FA System.Void Obi.ObiUtils::GetPointCloudAnisotropy(System.Collections.Generic.List`1<UnityEngine.Vector3>,System.Single,System.Single,UnityEngine.Vector3&,UnityEngine.Vector3&,UnityEngine.Quaternion&,UnityEngine.Vector3&)
extern void ObiUtils_GetPointCloudAnisotropy_m3D48980B4381A7B54F37C063FE47EF7C737CA9A2 (void);
// 0x000005FB System.Void Obi.ObiUtils::.cctor()
extern void ObiUtils__cctor_mE9D4CD7AA4BC90F980D0AC4F4DA0B543DFD75D01 (void);
// 0x000005FC System.Void Obi.ObiUtils_<BilateralInterleaved>d__38::.ctor(System.Int32)
extern void U3CBilateralInterleavedU3Ed__38__ctor_m19BC8267EC16344E9F475D2D8F4155172B85C4D2 (void);
// 0x000005FD System.Void Obi.ObiUtils_<BilateralInterleaved>d__38::System.IDisposable.Dispose()
extern void U3CBilateralInterleavedU3Ed__38_System_IDisposable_Dispose_m0882D85F88A623231272D83F0D0DA178DCEF01BA (void);
// 0x000005FE System.Boolean Obi.ObiUtils_<BilateralInterleaved>d__38::MoveNext()
extern void U3CBilateralInterleavedU3Ed__38_MoveNext_m6AAB9119339072DCC1E88707B3CE2C74EC73CA9D (void);
// 0x000005FF System.Object Obi.ObiUtils_<BilateralInterleaved>d__38::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CBilateralInterleavedU3Ed__38_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mE8BF0E22888DF4C74A1A58C2C0CFC10DE467BA08 (void);
// 0x00000600 System.Void Obi.ObiUtils_<BilateralInterleaved>d__38::System.Collections.IEnumerator.Reset()
extern void U3CBilateralInterleavedU3Ed__38_System_Collections_IEnumerator_Reset_mC549AFDEE04DEDBB58A64225A8EECED92476FDFF (void);
// 0x00000601 System.Object Obi.ObiUtils_<BilateralInterleaved>d__38::System.Collections.IEnumerator.get_Current()
extern void U3CBilateralInterleavedU3Ed__38_System_Collections_IEnumerator_get_Current_mBD38911DD74EDEF3DDD7E26C4B713A64A2F46DF3 (void);
// 0x00000602 System.Collections.Generic.IEnumerator`1<System.Object> Obi.ObiUtils_<BilateralInterleaved>d__38::System.Collections.Generic.IEnumerable<System.Object>.GetEnumerator()
extern void U3CBilateralInterleavedU3Ed__38_System_Collections_Generic_IEnumerableU3CSystem_ObjectU3E_GetEnumerator_mBB9EE9A4785E405D7DE91A4DD014CE7B467A9A56 (void);
// 0x00000603 System.Collections.IEnumerator Obi.ObiUtils_<BilateralInterleaved>d__38::System.Collections.IEnumerable.GetEnumerator()
extern void U3CBilateralInterleavedU3Ed__38_System_Collections_IEnumerable_GetEnumerator_mC092BD1AE35CAE1A4FB3644AE120E31AA501A148 (void);
// 0x00000604 System.Void Obi.ObiVectorMath::Cross(UnityEngine.Vector3,UnityEngine.Vector3,System.Single&,System.Single&,System.Single&)
extern void ObiVectorMath_Cross_m23CD1AEE1CC6B74D7930C98AA1D280513238A553 (void);
// 0x00000605 System.Void Obi.ObiVectorMath::Cross(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3&)
extern void ObiVectorMath_Cross_mA47A8583DC381295371BD66F8397DD209C2B7731 (void);
// 0x00000606 System.Void Obi.ObiVectorMath::Cross(System.Single,System.Single,System.Single,System.Single,System.Single,System.Single,System.Single&,System.Single&,System.Single&)
extern void ObiVectorMath_Cross_m4C1FC1059C8BF006437BAA9D3CBBCD7323016E19 (void);
// 0x00000607 System.Void Obi.ObiVectorMath::Subtract(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3&)
extern void ObiVectorMath_Subtract_mD703C48EC9EAAAF9901BCB1F1D0BF46E23A8925A (void);
// 0x00000608 System.Void Obi.ObiVectorMath::BarycentricInterpolation(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3,System.Single,UnityEngine.Vector3&)
extern void ObiVectorMath_BarycentricInterpolation_m7CD9E0E0E11533DEB84C7003A1FBC5E415B6CAD4 (void);
// 0x00000609 System.Void Obi.SetCategory::Awake()
extern void SetCategory_Awake_mC91A0F14152A3306C3D58FFDB3268FC42CA75A1B (void);
// 0x0000060A System.Void Obi.SetCategory::OnDestroy()
extern void SetCategory_OnDestroy_m7E095279FCAA7AC75B26DBC31335E8C1B6156A5F (void);
// 0x0000060B System.Void Obi.SetCategory::OnValidate()
extern void SetCategory_OnValidate_m96641FEA78031AF96A7904C7B89F5526989E129A (void);
// 0x0000060C System.Void Obi.SetCategory::OnLoad(Obi.ObiActor,Obi.ObiActorBlueprint)
extern void SetCategory_OnLoad_m105F18C77751C59F67759FE5006D36A29B4480AF (void);
// 0x0000060D System.Void Obi.SetCategory::.ctor()
extern void SetCategory__ctor_m5F2C7ADC7EAEF4C3E7B89A74858C43C67F3F9AD5 (void);
// 0x0000060E System.Void Obi.ObiEmitter::add_OnEmitParticle(Obi.ObiEmitter_EmitterParticleCallback)
extern void ObiEmitter_add_OnEmitParticle_m8491547E27FD28676D80322B77B456F0C63CAD15 (void);
// 0x0000060F System.Void Obi.ObiEmitter::remove_OnEmitParticle(Obi.ObiEmitter_EmitterParticleCallback)
extern void ObiEmitter_remove_OnEmitParticle_m37801BDD62149B5462BEE2401FE604F9958FD475 (void);
// 0x00000610 System.Void Obi.ObiEmitter::add_OnKillParticle(Obi.ObiEmitter_EmitterParticleCallback)
extern void ObiEmitter_add_OnKillParticle_m072CE84C1A40E144AAAE8799AFA1375DD3EE51A6 (void);
// 0x00000611 System.Void Obi.ObiEmitter::remove_OnKillParticle(Obi.ObiEmitter_EmitterParticleCallback)
extern void ObiEmitter_remove_OnKillParticle_m25E9F562BEC40314A39FB0539A099C60BEE0E03E (void);
// 0x00000612 Obi.ObiActorBlueprint Obi.ObiEmitter::get_sourceBlueprint()
extern void ObiEmitter_get_sourceBlueprint_m26D4695B2546A33A98C940075D884E7BCABB9C21 (void);
// 0x00000613 System.Void Obi.ObiEmitter::set_Filter(System.Int32)
extern void ObiEmitter_set_Filter_m9D0B5EB8094CAB07DA4A4E54DD47B619B1C64DB2 (void);
// 0x00000614 System.Int32 Obi.ObiEmitter::get_Filter()
extern void ObiEmitter_get_Filter_m2B0C29BC4AAE23B8F8BCC6F5E0AB42FF6CB18688 (void);
// 0x00000615 System.Boolean Obi.ObiEmitter::get_isEmitting()
extern void ObiEmitter_get_isEmitting_m2E167BA71486E50EA1FA553928089078FF2CCDC2 (void);
// 0x00000616 System.Boolean Obi.ObiEmitter::get_surfaceCollisions()
extern void ObiEmitter_get_surfaceCollisions_m7948BECE7563A00F333CDD1E17A340DFDB508548 (void);
// 0x00000617 System.Void Obi.ObiEmitter::set_surfaceCollisions(System.Boolean)
extern void ObiEmitter_set_surfaceCollisions_m4214AB7FF3E3B1573122F95846D3EBD206C9F70E (void);
// 0x00000618 System.Boolean Obi.ObiEmitter::get_usesCustomExternalForces()
extern void ObiEmitter_get_usesCustomExternalForces_m7DD026E3D65624A9202152C5798EFB87976DB787 (void);
// 0x00000619 System.Boolean Obi.ObiEmitter::get_usesAnisotropicParticles()
extern void ObiEmitter_get_usesAnisotropicParticles_mC0661B5AFC22D80BE3C61E7D5AFE806F5D9B02F2 (void);
// 0x0000061A System.Void Obi.ObiEmitter::LoadBlueprint(Obi.ObiSolver)
extern void ObiEmitter_LoadBlueprint_mED89098C507E400D06A15AA61ACBC53ED585A18D (void);
// 0x0000061B System.Void Obi.ObiEmitter::OnValidate()
extern void ObiEmitter_OnValidate_m57F92C20FFB8E298BD6BE40714D94BE3B46070FC (void);
// 0x0000061C System.Void Obi.ObiEmitter::AddShape(Obi.ObiEmitterShape)
extern void ObiEmitter_AddShape_m9BF07C5236F3F8117EFBD7384949CB95038A68BC (void);
// 0x0000061D System.Void Obi.ObiEmitter::RemoveShape(Obi.ObiEmitterShape)
extern void ObiEmitter_RemoveShape_mD5BF4FBDB20CBF6CEDFEF6BA1AE6D10B2CE11286 (void);
// 0x0000061E System.Void Obi.ObiEmitter::UpdateEmitterDistribution()
extern void ObiEmitter_UpdateEmitterDistribution_mFB97BE307BCDC399CB8D5F846E83E7AF0565D9AF (void);
// 0x0000061F System.Collections.Generic.IEnumerator`1<Obi.ObiEmitterShape_DistributionPoint> Obi.ObiEmitter::GetDistributionEnumerator()
extern void ObiEmitter_GetDistributionEnumerator_m7A7448549FB95DA20C722E386EC54A6C6457CAE0 (void);
// 0x00000620 System.Void Obi.ObiEmitter::UpdateParticleMaterial()
extern void ObiEmitter_UpdateParticleMaterial_m517E76333581737EEF86813987E4153F385DF6EC (void);
// 0x00000621 System.Void Obi.ObiEmitter::SetSelfCollisions(System.Boolean)
extern void ObiEmitter_SetSelfCollisions_mADD790DDF745EC730B238E53811B1BA769BD8ACE (void);
// 0x00000622 System.Void Obi.ObiEmitter::UpdateFilter()
extern void ObiEmitter_UpdateFilter_m72F9D1ADB52414CB1EDF57DC683C23A550F087B0 (void);
// 0x00000623 System.Void Obi.ObiEmitter::UpdateParticleResolution(System.Int32)
extern void ObiEmitter_UpdateParticleResolution_m6CD9FD98DEE0C8E93C71890A1DBB703AC094D644 (void);
// 0x00000624 System.Void Obi.ObiEmitter::UpdateParticleMaterial(System.Int32)
extern void ObiEmitter_UpdateParticleMaterial_m4B7AA6AA73521B582CDDAE4F515609EB0390B930 (void);
// 0x00000625 System.Void Obi.ObiEmitter::SwapWithFirstInactiveParticle(System.Int32)
extern void ObiEmitter_SwapWithFirstInactiveParticle_m1F1A1EB2283E25A96C6BDA0C1D0161AE80185E43 (void);
// 0x00000626 System.Void Obi.ObiEmitter::ResetParticle(System.Int32,System.Single,System.Single)
extern void ObiEmitter_ResetParticle_mEB6C06CEAEDCF61DCDA75BEFC8EF573CBF5586CD (void);
// 0x00000627 System.Boolean Obi.ObiEmitter::EmitParticle(System.Single,System.Single)
extern void ObiEmitter_EmitParticle_mFE11259A7521888CF3A0E65D9125A8F0C5327D92 (void);
// 0x00000628 System.Boolean Obi.ObiEmitter::KillParticle(System.Int32)
extern void ObiEmitter_KillParticle_m6734F9974FDEA10F0BA301736753003913ACA3F0 (void);
// 0x00000629 System.Void Obi.ObiEmitter::KillAll()
extern void ObiEmitter_KillAll_m8BF9564C90A434B223579225A3E6B3AD6E662FC4 (void);
// 0x0000062A System.Int32 Obi.ObiEmitter::GetDistributionPointsCount()
extern void ObiEmitter_GetDistributionPointsCount_m5E8C1423287A0704A40FA47A7AB41C37E0950674 (void);
// 0x0000062B System.Void Obi.ObiEmitter::BeginStep(System.Single)
extern void ObiEmitter_BeginStep_m665B3103CE41C6C937F85F73D528F8FDC0116F67 (void);
// 0x0000062C System.Void Obi.ObiEmitter::.ctor()
extern void ObiEmitter__ctor_m1E29FB9EC76CA6E43CDE2741DEDA938D7BD20185 (void);
// 0x0000062D System.Void Obi.ObiEmitter_EmitterParticleCallback::.ctor(System.Object,System.IntPtr)
extern void EmitterParticleCallback__ctor_m9C4DBA29CFCF9F31E0D9EBB78450F63F21DAC8EB (void);
// 0x0000062E System.Void Obi.ObiEmitter_EmitterParticleCallback::Invoke(Obi.ObiEmitter,System.Int32)
extern void EmitterParticleCallback_Invoke_mA9C7D02905E6C6D1B3BDFE6337130668330BB2D9 (void);
// 0x0000062F System.IAsyncResult Obi.ObiEmitter_EmitterParticleCallback::BeginInvoke(Obi.ObiEmitter,System.Int32,System.AsyncCallback,System.Object)
extern void EmitterParticleCallback_BeginInvoke_mF10FA008B7EE75DE9287B041AF6EF5468246456B (void);
// 0x00000630 System.Void Obi.ObiEmitter_EmitterParticleCallback::EndInvoke(System.IAsyncResult)
extern void EmitterParticleCallback_EndInvoke_m2241BFB0C7805D824FB2FC8BBAE6CAFF2E44C7C8 (void);
// 0x00000631 System.Void Obi.ObiEmitter_<GetDistributionEnumerator>d__40::.ctor(System.Int32)
extern void U3CGetDistributionEnumeratorU3Ed__40__ctor_m449587DF3040BED65DA885F0CB975BAF5648B862 (void);
// 0x00000632 System.Void Obi.ObiEmitter_<GetDistributionEnumerator>d__40::System.IDisposable.Dispose()
extern void U3CGetDistributionEnumeratorU3Ed__40_System_IDisposable_Dispose_mB66E839E2203FA3D371BE99CF5E206ED314BB16F (void);
// 0x00000633 System.Boolean Obi.ObiEmitter_<GetDistributionEnumerator>d__40::MoveNext()
extern void U3CGetDistributionEnumeratorU3Ed__40_MoveNext_mAA03394FE695F1B8CFC92E643C69BDF17C3CEDFA (void);
// 0x00000634 Obi.ObiEmitterShape_DistributionPoint Obi.ObiEmitter_<GetDistributionEnumerator>d__40::System.Collections.Generic.IEnumerator<Obi.ObiEmitterShape.DistributionPoint>.get_Current()
extern void U3CGetDistributionEnumeratorU3Ed__40_System_Collections_Generic_IEnumeratorU3CObi_ObiEmitterShape_DistributionPointU3E_get_Current_m099460B8F655B695ECCDDA49652656A00D9E4DAA (void);
// 0x00000635 System.Void Obi.ObiEmitter_<GetDistributionEnumerator>d__40::System.Collections.IEnumerator.Reset()
extern void U3CGetDistributionEnumeratorU3Ed__40_System_Collections_IEnumerator_Reset_m11ED40F759FA3014EB42EC36A4812A9AA6784C12 (void);
// 0x00000636 System.Object Obi.ObiEmitter_<GetDistributionEnumerator>d__40::System.Collections.IEnumerator.get_Current()
extern void U3CGetDistributionEnumeratorU3Ed__40_System_Collections_IEnumerator_get_Current_mA5947F59DA4154A7983A004718D35B54846233BC (void);
// 0x00000637 System.Single Obi.ObiEmitterBlueprintBase::GetParticleSize(Oni_SolverParameters_Mode)
extern void ObiEmitterBlueprintBase_GetParticleSize_m171C2A62E702E4266BB8068AA7B9C8C0C7E6006C (void);
// 0x00000638 System.Single Obi.ObiEmitterBlueprintBase::GetParticleMass(Oni_SolverParameters_Mode)
extern void ObiEmitterBlueprintBase_GetParticleMass_m5315467E58807831A1582F8C68CFCB2BF9E9BDF4 (void);
// 0x00000639 System.Collections.IEnumerator Obi.ObiEmitterBlueprintBase::Initialize()
extern void ObiEmitterBlueprintBase_Initialize_mC055F9C9FAA49BD2835667B062902B987C9FF68D (void);
// 0x0000063A System.Void Obi.ObiEmitterBlueprintBase::.ctor()
extern void ObiEmitterBlueprintBase__ctor_mFFA6CC80C85F17DBBA55896FAF790F27E4DCB4F8 (void);
// 0x0000063B System.Void Obi.ObiEmitterBlueprintBase_<Initialize>d__5::.ctor(System.Int32)
extern void U3CInitializeU3Ed__5__ctor_m438B4BA7AD1B7ABF3B194B7211DACD1B6CAF513A (void);
// 0x0000063C System.Void Obi.ObiEmitterBlueprintBase_<Initialize>d__5::System.IDisposable.Dispose()
extern void U3CInitializeU3Ed__5_System_IDisposable_Dispose_mADA4B1631A55EEC9DE2EF0096136DFC72E91BD37 (void);
// 0x0000063D System.Boolean Obi.ObiEmitterBlueprintBase_<Initialize>d__5::MoveNext()
extern void U3CInitializeU3Ed__5_MoveNext_mDDDC37540F8037B1D2B1D7E8EE83A51BD8F599F5 (void);
// 0x0000063E System.Object Obi.ObiEmitterBlueprintBase_<Initialize>d__5::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CInitializeU3Ed__5_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mFD5E2A626D669E05BD156AB6C47B08812732EB0F (void);
// 0x0000063F System.Void Obi.ObiEmitterBlueprintBase_<Initialize>d__5::System.Collections.IEnumerator.Reset()
extern void U3CInitializeU3Ed__5_System_Collections_IEnumerator_Reset_m924E660EAF2B642353D8C885A7DEE6A273E90FF9 (void);
// 0x00000640 System.Object Obi.ObiEmitterBlueprintBase_<Initialize>d__5::System.Collections.IEnumerator.get_Current()
extern void U3CInitializeU3Ed__5_System_Collections_IEnumerator_get_Current_m49C637AFA064F7F5A9498FFD167D9D4643B275B9 (void);
// 0x00000641 System.Void Obi.ObiFluidEmitterBlueprint::OnValidate()
extern void ObiFluidEmitterBlueprint_OnValidate_m492FA6F5DFC1E38BE273FCDA58079568D42DB10F (void);
// 0x00000642 System.Single Obi.ObiFluidEmitterBlueprint::GetSmoothingRadius(Oni_SolverParameters_Mode)
extern void ObiFluidEmitterBlueprint_GetSmoothingRadius_m60BE1C679492A1E85F360D049C654A999C3A2B90 (void);
// 0x00000643 System.Void Obi.ObiFluidEmitterBlueprint::.ctor()
extern void ObiFluidEmitterBlueprint__ctor_mAB848A5DCED957321A0F778171622D710B441D82 (void);
// 0x00000644 System.Void Obi.ObiGranularEmitterBlueprint::OnValidate()
extern void ObiGranularEmitterBlueprint_OnValidate_mDCD55411AFDD5188709D2335212D8A97667D9AB8 (void);
// 0x00000645 System.Void Obi.ObiGranularEmitterBlueprint::.ctor()
extern void ObiGranularEmitterBlueprint__ctor_mD506D7406ECAEFE7FCB31261B6A9F66FF6B05F61 (void);
// 0x00000646 System.Void Obi.ObiEmitterShape::set_Emitter(Obi.ObiEmitter)
extern void ObiEmitterShape_set_Emitter_m738B65FF67C0C5F293362EC39E2FF44AF9B4E06C (void);
// 0x00000647 Obi.ObiEmitter Obi.ObiEmitterShape::get_Emitter()
extern void ObiEmitterShape_get_Emitter_mFAE7391AF31C09A3F8D91BD51C0CAEBC3AF32E0F (void);
// 0x00000648 UnityEngine.Matrix4x4 Obi.ObiEmitterShape::get_ShapeLocalToSolverMatrix()
extern void ObiEmitterShape_get_ShapeLocalToSolverMatrix_mC212FFE89C25BE3398276B4200A6F3315286207F (void);
// 0x00000649 System.Void Obi.ObiEmitterShape::OnEnable()
extern void ObiEmitterShape_OnEnable_m19B742103C8F1AFEE0306BE076869064457B4CE0 (void);
// 0x0000064A System.Void Obi.ObiEmitterShape::OnDisable()
extern void ObiEmitterShape_OnDisable_m0DB579AD465ECCCB9498A5DF9564F71822A9A5C7 (void);
// 0x0000064B System.Void Obi.ObiEmitterShape::UpdateLocalToSolverMatrix()
extern void ObiEmitterShape_UpdateLocalToSolverMatrix_m2BC8BCA9E1B90BAB87298EFB9DE2528CAA010116 (void);
// 0x0000064C System.Void Obi.ObiEmitterShape::GenerateDistribution()
// 0x0000064D System.Void Obi.ObiEmitterShape::.ctor()
extern void ObiEmitterShape__ctor_mCF574C14DDE13454E3366335DF34EB8837B2FC72 (void);
// 0x0000064E System.Void Obi.ObiEmitterShape_DistributionPoint::.ctor(UnityEngine.Vector3,UnityEngine.Vector3)
extern void DistributionPoint__ctor_m558DC84A6B0F7A8C37E8894D08E1A759B3B09E13_AdjustorThunk (void);
// 0x0000064F System.Void Obi.ObiEmitterShape_DistributionPoint::.ctor(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Color)
extern void DistributionPoint__ctor_m7CD39DE09F96C6280D631352FFB3E38DC8C7EBCC_AdjustorThunk (void);
// 0x00000650 Obi.ObiEmitterShape_DistributionPoint Obi.ObiEmitterShape_DistributionPoint::GetTransformed(UnityEngine.Matrix4x4,UnityEngine.Color)
extern void DistributionPoint_GetTransformed_m4231D7EA07AB8B71E35E91C62C3F37F17B930F27_AdjustorThunk (void);
// 0x00000651 System.Void Obi.ObiEmitterShapeCube::OnValidate()
extern void ObiEmitterShapeCube_OnValidate_m1458ACED6D98E6EB8F6E2E0EFAF084B9E865E4B6 (void);
// 0x00000652 System.Void Obi.ObiEmitterShapeCube::GenerateDistribution()
extern void ObiEmitterShapeCube_GenerateDistribution_m4C3072C14BCCB8A1C96C57BCAE69E459BB0C3BD9 (void);
// 0x00000653 System.Void Obi.ObiEmitterShapeCube::.ctor()
extern void ObiEmitterShapeCube__ctor_m4307FA9A97458348EEEF8C9EFFA9BDEEEA0D0890 (void);
// 0x00000654 System.Void Obi.ObiEmitterShapeDisk::OnValidate()
extern void ObiEmitterShapeDisk_OnValidate_m92DDC81192E354D15DCE6317298D02FCE0A5BA37 (void);
// 0x00000655 System.Void Obi.ObiEmitterShapeDisk::GenerateDistribution()
extern void ObiEmitterShapeDisk_GenerateDistribution_m9BFB838DD7FE448E91A409C3E998436F47D175FA (void);
// 0x00000656 System.Void Obi.ObiEmitterShapeDisk::.ctor()
extern void ObiEmitterShapeDisk__ctor_mD88D3F0C289E9B7BD2333BB4269D012B2A01F40A (void);
// 0x00000657 System.Void Obi.ObiEmitterShapeEdge::OnValidate()
extern void ObiEmitterShapeEdge_OnValidate_m8687BA352FB579F0928C80D690C262F5C7BAAD73 (void);
// 0x00000658 System.Void Obi.ObiEmitterShapeEdge::GenerateDistribution()
extern void ObiEmitterShapeEdge_GenerateDistribution_m5C9BF94C7AA8CCE15A49F2B14385BB1829E66A53 (void);
// 0x00000659 System.Void Obi.ObiEmitterShapeEdge::.ctor()
extern void ObiEmitterShapeEdge__ctor_mFDD80D7B2CCAE5C7DC190F58171805071E63185F (void);
// 0x0000065A System.Void Obi.ObiEmitterShapeImage::GenerateDistribution()
extern void ObiEmitterShapeImage_GenerateDistribution_mC3F74D5440F38B83BC1D13415A0A876F9A22183F (void);
// 0x0000065B System.Void Obi.ObiEmitterShapeImage::GetWorldSpaceEmitterSize(System.Single&,System.Single&)
extern void ObiEmitterShapeImage_GetWorldSpaceEmitterSize_mF397EF0461A96C8A6DE60C38D7C8AAB107655548 (void);
// 0x0000065C System.Void Obi.ObiEmitterShapeImage::.ctor()
extern void ObiEmitterShapeImage__ctor_m67C9A11F0A682BA20064F26D53DDB8BDF8F44D92 (void);
// 0x0000065D System.Void Obi.ObiEmitterShapeMesh::GenerateDistribution()
extern void ObiEmitterShapeMesh_GenerateDistribution_m1F4D55DF41C92CDBA0256F6D25822F38ABBAB911 (void);
// 0x0000065E System.Void Obi.ObiEmitterShapeMesh::.ctor()
extern void ObiEmitterShapeMesh__ctor_mE1E90478BFC0388C8A751CA3B31BCD6A0723BD0D (void);
// 0x0000065F System.Void Obi.ObiEmitterShapeSphere::OnValidate()
extern void ObiEmitterShapeSphere_OnValidate_mB9AE74B99D2643B70612081FE225FEB7C508CA53 (void);
// 0x00000660 System.Void Obi.ObiEmitterShapeSphere::GenerateDistribution()
extern void ObiEmitterShapeSphere_GenerateDistribution_m3AF0847B95050CA48104F579A075CEC2B9DBA09A (void);
// 0x00000661 System.Void Obi.ObiEmitterShapeSphere::.ctor()
extern void ObiEmitterShapeSphere__ctor_m072D019F93BE50415E75B13371E45A5E8C0F2C82 (void);
// 0x00000662 System.Void Obi.ObiEmitterShapeSquare::GenerateDistribution()
extern void ObiEmitterShapeSquare_GenerateDistribution_m25B2D91883D1A366DCAE4B2E169E7E079EC08A72 (void);
// 0x00000663 System.Void Obi.ObiEmitterShapeSquare::.ctor()
extern void ObiEmitterShapeSquare__ctor_m6A2CB35F2453CCE9C2C62C54810120FF99AA8EF2 (void);
// 0x00000664 UnityEngine.Material Obi.FluidRenderingUtils::CreateMaterial(UnityEngine.Shader)
extern void FluidRenderingUtils_CreateMaterial_mE2F2958ECC414D242F52450D32B5FDBA6413EC7B (void);
// 0x00000665 System.Single Obi.FluidRenderingUtils::SetupFluidCamera(UnityEngine.Camera)
extern void FluidRenderingUtils_SetupFluidCamera_m4C8B96EE9F29DD22ED24AE70D6919704AB882ACE (void);
// 0x00000666 System.Void Obi.FluidRenderingUtils::SurfaceReconstruction(UnityEngine.Rendering.CommandBuffer,Obi.FluidRenderingUtils_FluidRenderTargets,UnityEngine.Material,UnityEngine.Material,Obi.ObiParticleRenderer[])
extern void FluidRenderingUtils_SurfaceReconstruction_m12923003A21DF48D20B7D785CA33448CC83FF4CE (void);
// 0x00000667 System.Void Obi.FluidRenderingUtils::VolumeReconstruction(UnityEngine.Rendering.CommandBuffer,Obi.FluidRenderingUtils_FluidRenderTargets,UnityEngine.Material,UnityEngine.Material,Obi.ObiParticleRenderer[])
extern void FluidRenderingUtils_VolumeReconstruction_m6F3E2956C90F0A927EBEEED56F050D2EF8B655B7 (void);
// 0x00000668 System.Void Obi.FluidRenderingUtils::Foam(UnityEngine.Rendering.CommandBuffer,Obi.FluidRenderingUtils_FluidRenderTargets,Obi.ObiParticleRenderer[])
extern void FluidRenderingUtils_Foam_mA2AA55FA9A3A2B571B27438565088A32A27EA0EB (void);
// 0x00000669 System.Void Obi.FluidRenderingUtils::Refraction(UnityEngine.Rendering.CommandBuffer,UnityEngine.Rendering.RenderTargetIdentifier,Obi.FluidRenderingUtils_FluidRenderTargets)
extern void FluidRenderingUtils_Refraction_m0C0ED2E17F2BE32ED84C45894BB9042D41317C0D (void);
// 0x0000066A System.Void Obi.FluidRenderingUtils::.cctor()
extern void FluidRenderingUtils__cctor_m92C53BB8B58AE98522664EBB9DCA3C49C1269DDD (void);
// 0x0000066B System.Void Obi.FluidRenderingUtils_FluidRendererSettings::.ctor()
extern void FluidRendererSettings__ctor_m9FD316F71F74710EFBE27C0EA32FCCC7066ACD77 (void);
// 0x0000066C System.Void Obi.ObiBaseFluidRenderer::Awake()
extern void ObiBaseFluidRenderer_Awake_m858EF05E711487D19C53F023D2D4B103C799B190 (void);
// 0x0000066D System.Void Obi.ObiBaseFluidRenderer::OnEnable()
extern void ObiBaseFluidRenderer_OnEnable_m046808A62B1B6D06CDC3A696BDFA2A736941407A (void);
// 0x0000066E System.Void Obi.ObiBaseFluidRenderer::OnDisable()
extern void ObiBaseFluidRenderer_OnDisable_mB900AAE985EF73F6B9717F9F6E5671B1D0CA9B4E (void);
// 0x0000066F UnityEngine.Material Obi.ObiBaseFluidRenderer::CreateMaterial(UnityEngine.Shader)
extern void ObiBaseFluidRenderer_CreateMaterial_mD1FBCC71591C2EEE677330E7632884EDFE70100A (void);
// 0x00000670 System.Void Obi.ObiBaseFluidRenderer::Setup()
extern void ObiBaseFluidRenderer_Setup_mC196BA1C57308A9E0921ACB33194B16DBCE4D389 (void);
// 0x00000671 System.Void Obi.ObiBaseFluidRenderer::Cleanup()
extern void ObiBaseFluidRenderer_Cleanup_mCDB11B875A93046A352E255B834E84287C0BF570 (void);
// 0x00000672 System.Void Obi.ObiBaseFluidRenderer::UpdateFluidRenderingCommandBuffer()
// 0x00000673 System.Void Obi.ObiBaseFluidRenderer::DestroyCommandBuffer()
extern void ObiBaseFluidRenderer_DestroyCommandBuffer_mDA1CD19796D16750C6D274EC32F3C9B8D58ADD63 (void);
// 0x00000674 System.Void Obi.ObiBaseFluidRenderer::OnPreRender()
extern void ObiBaseFluidRenderer_OnPreRender_m262AE226F352EE9A5338670F4CFC3F77FE6E36C7 (void);
// 0x00000675 System.Void Obi.ObiBaseFluidRenderer::.ctor()
extern void ObiBaseFluidRenderer__ctor_mDD76E1D3E3C376F0D2DEC27CDD5121EE7AB9B845 (void);
// 0x00000676 System.Void Obi.ObiFluidRenderer::Setup()
extern void ObiFluidRenderer_Setup_mC65951B017D727A5AA07DB94EE3A2CE5D09BC389 (void);
// 0x00000677 System.Void Obi.ObiFluidRenderer::Cleanup()
extern void ObiFluidRenderer_Cleanup_mE554CB09BB193BCE63198D44A1BC2C9E0133FDC1 (void);
// 0x00000678 System.Void Obi.ObiFluidRenderer::UpdateFluidRenderingCommandBuffer()
extern void ObiFluidRenderer_UpdateFluidRenderingCommandBuffer_mCB5FDDEAAEB29CD6F9A856565B78E2286AD21C6D (void);
// 0x00000679 System.Void Obi.ObiFluidRenderer::.ctor()
extern void ObiFluidRenderer__ctor_mE66384FDCC40AEF4BBCD474C35B16463CF08FE5D (void);
static Il2CppMethodPointer s_methodPointers[1657] = 
{
	EmbeddedAttribute__ctor_mF092B41AF606A0079DB72F589B9F3A4B09A2879C,
	IsReadOnlyAttribute__ctor_mA5B530204715922B8E11EA362EE266FA7E65613D,
	ObiContactGrabber_get_grabbed_mB390126778E0A789B441B372D5F525DB318BC301,
	ObiContactGrabber_Awake_m81FAB1A7D0894DFF410B881A4BD34C377CCDF33D,
	ObiContactGrabber_OnEnable_m0CDF84CEFA0997E0C363D99406A72B7F6617015D,
	ObiContactGrabber_OnDisable_m70E63109356461451716F2656B1F703ABDF30B67,
	ObiContactGrabber_Solver_OnCollision_m02CB781C94F8F518ADC1B61E01487BBDC62BA026,
	ObiContactGrabber_UpdateParticleProperties_m874B590742CF4C376FDD13678A387918D098B615,
	ObiContactGrabber_GrabParticle_mA112EAB2920DB3B64AA31FCF9455C6F4702858B5,
	ObiContactGrabber_Grab_mF88B7A50127D874AC0295C6D7D98FFEBE6D8F618,
	ObiContactGrabber_Release_m82B5D7B992BC9423026E8CE0BD6DE0884D72BA68,
	ObiContactGrabber_FixedUpdate_mCAD4BA24875E897F95967F4540B06AC26800BF01,
	ObiContactGrabber__ctor_m9C1D048C7D8633509DB48156260CB765A7E6E364,
	GrabbedParticle__ctor_m29866A846BC99D62A7E53A7F55FD6234B7DB7567,
	GrabbedParticle_Equals_mFF2582C3BFDDE99366B35AC5DF73C20ECE8E430D,
	GrabbedParticle_GetHashCode_m37D33E44CE7086BE8CD4AF64B4270BE0FADDACC9,
	Oni_PinMemory_m03E1EA6D2AD2B2501D052E52FDE072606EDEB6EF,
	Oni_UnpinMemory_m029F0BA710E2DBE187A2300ED9F5CFA9DE37B62F,
	Oni_UpdateColliderGrid_m75034617DEA373FD3AAC6D3F57A9530AA426895D,
	Oni_SetColliders_m901960CF23BF96A6DCE6751F09B4425431091A65,
	Oni_SetRigidbodies_mF84FEC7A0C4A850D6CD1F9CF39A29C2D6AFEAECF,
	Oni_SetCollisionMaterials_mAAE3428F89A91AF30BB753E728556AD1F592F3B1,
	Oni_SetTriangleMeshData_mFFF4366C8A8B13EDA5301EDE1CED9A2CD92AE43D,
	Oni_SetEdgeMeshData_m7D6EBD72BA2169E3C697F341B75BC716161524D0,
	Oni_SetDistanceFieldData_mED7CC8724B49A2AF36F817A57152CD95AA6E0200,
	Oni_SetHeightFieldData_m3890F2905A2990080378C1C2F28A9AD2DA077BEE,
	Oni_CreateSolver_m95484CA4C8E4048EB871BB224918540A317C885A,
	Oni_DestroySolver_m0ADEE02261B34DC1C348911FCDC53E4471B37D0B,
	Oni_SetCapacity_m4804EBFCAF115506C8DD5332214C6A2E0CB4810B,
	Oni_InitializeFrame_mB306333E4A867678C088DACC72B039EF58E80D31,
	Oni_UpdateFrame_m680BE80814872FABDBFDC7ABBE5FF1E740C5773D,
	Oni_ApplyFrame_mD136F376FE68D93651459FB1B946CE18BFAA8D10,
	Oni_RecalculateInertiaTensors_m5D778943EF3E20DCF00A85175E39DD691A8AFA16,
	Oni_ResetForces_m38A179189C49CCAC6142D9F38F234BA8D03C5F64,
	Oni_SetRigidbodyLinearDeltas_m3D5B23B5969BC13392B4E7E019D03730E61E3639,
	Oni_SetRigidbodyAngularDeltas_m4A436C37D1F6F9FE39F1184279883463F586CBF0,
	Oni_GetBounds_m363A96EE8BAB3FB760C636B8C5F361B8EE1E53B5,
	Oni_GetParticleGridSize_m2BD9A7939A0F7733323CA9C9EDE32C53FC7B6ABD,
	Oni_GetParticleGrid_m21D4AA3548CF77E25C5E456096E16162C1BAD4C0,
	Oni_SpatialQuery_m526F90093505F1A4FB3E31A2AC6EC6B8C1F850A5,
	Oni_GetQueryResults_mB69A3B8645A8218700E75C49956E15AC27B125B7,
	Oni_SetSolverParameters_mA78FF4C834E23D50A56D989521B278AA6B11BD48,
	Oni_GetSolverParameters_mD299B385DF9770D77C9E0D4F4781EFADE20C30E4,
	Oni_SetActiveParticles_mA8361CFE331459188A7701673465FCEC0770F84B,
	Oni_CollisionDetection_mD890F2AED814E8B7F3766DDD603916EDA4B1B861,
	Oni_Step_m73AE7B946E5053FC14C8061FCD6508D4824213B7,
	Oni_ApplyPositionInterpolation_mA7A631314B43D3A62FAF81D209F1CCB00A3A901D,
	Oni_UpdateSkeletalAnimation_m0F0F49630F0ABF1B6C742D13FEDB8A69B3C71A31,
	Oni_GetConstraintCount_m046D7B8D04B0EDDC24A336FA1AD6F93B181B143A,
	Oni_SetRenderableParticlePositions_mF10457D7F9724C687D3AB6E83DEEBB4C9937F138,
	Oni_SetParticlePhases_m330FC442852B31A851FB05486D9D5D4A9E85080C,
	Oni_SetParticleFilters_m4689F2DCACACC85971902111BF779817074E976C,
	Oni_SetParticleCollisionMaterials_m64562F4539FF5805E44B181ED119B61FB636DB91,
	Oni_SetParticlePositions_mBF4B08453E99AACC5D3C934F3B7F1EDB2AA3934A,
	Oni_SetParticlePreviousPositions_mF7643A2DD9F3D10409DF62601E0D8EE2D0DBD7E4,
	Oni_SetParticleOrientations_m92BF1084EB29939C992A0666D1E75C6AAB14801D,
	Oni_SetParticlePreviousOrientations_m08E1313630723606D179B6FB44025E49B838D2BB,
	Oni_SetRenderableParticleOrientations_mE90A2228D4CDC6641661C730F1164F65609788B8,
	Oni_SetParticleInverseMasses_mA5F3998095FBD44B558C75B634E18628520D2385,
	Oni_SetParticleInverseRotationalMasses_mBB6281669729C39BEF623DDDEDCC12EC93C31FBB,
	Oni_SetParticlePrincipalRadii_mCE141D972ABA98C2CCC7B66419BC0009E227F8CE,
	Oni_SetParticleVelocities_m806F66F28AB0D4E632921B7E0CF3464C777B00C7,
	Oni_SetParticleAngularVelocities_mD376C89032B153B1C2EB36FAF673C90600871052,
	Oni_SetParticleExternalForces_mABE204855B05F30D980984785EFC178E8178657B,
	Oni_SetParticleExternalTorques_m52F453DFC994BB2CB946C0ECFCA90755E8CE81BF,
	Oni_SetParticleWinds_m4D8B34B1F691B0D06B66B5AC4A9CE7A47C438D7A,
	Oni_SetParticlePositionDeltas_m44290488FA81D82A7B5E34C6C419183E4AC4C7F3,
	Oni_SetParticleOrientationDeltas_m80BA163C8E315CDE4D535070E044140582F4FB75,
	Oni_SetParticlePositionConstraintCounts_mC6C3DF0767ABFE55B7D56664BD2B85CACC2F2AF3,
	Oni_SetParticleOrientationConstraintCounts_mE8DE30A217492D6C2961351F94471600E84DDF33,
	Oni_SetParticleNormals_mC7DB3812E3D8DA4944D5783D612327C14DAA1407,
	Oni_SetParticleInverseInertiaTensors_m107AC92B3D36DA0EC84A2BC81EAAFDE1A87645F3,
	Oni_SetParticleSmoothingRadii_m23C24AAADB64BB87F5AB6C0F29146E045BD15F32,
	Oni_SetParticleBuoyancy_m1D5C0C42C6E1F7F9F0009907C3C47777493BFFFD,
	Oni_SetParticleRestDensities_mC2C5BF07F3F31A9A9BD30F1E0FD65FA90DAB8668,
	Oni_SetParticleViscosities_m40D16C05777FCCBA236F8812E90FCE61732F8F65,
	Oni_SetParticleSurfaceTension_mEB650D756ACFD8AFCE0B0CA1FA516B8A04166803,
	Oni_SetParticleVorticityConfinement_m490207BCEB92C2090FA6F812903E9DC720C542CD,
	Oni_SetParticleAtmosphericDragPressure_m0DE4F75155DE8255D542463CED62BBA10DE95991,
	Oni_SetParticleDiffusion_m2E7A56B1429ABF2C68BCBEAF9A8B903BCFBBEB97,
	Oni_SetParticleVorticities_mBFAFD13B80D055251ECE3BB09331FEE93EC096CE,
	Oni_SetParticleFluidData_m5475F465341FBD9D3646CF21F640D31DBBDA52BC,
	Oni_SetParticleUserData_m1C95E2860A342CC2408369BE71453D688A4940E2,
	Oni_SetParticleAnisotropies_m4F40683E247F34D06C6FAC46E7BB2D100699D943,
	Oni_SetSimplices_mFA1EA60EF9F96B8AD384993D7AC15B73A2B08D95,
	Oni_GetDeformableTriangleCount_m3D720CBB04BADD1E4AAE5D0F630E7E8A1AB8D398,
	Oni_SetDeformableTriangles_m93D02F1E05CEF46D51296532191387392C8885C6,
	Oni_RemoveDeformableTriangles_mA0A4A28EA77A35743C37CF80A333559E7C8CC8AF,
	Oni_SetConstraintGroupParameters_m7402C64CA0198FD7D07E4F2BBFC81F8C9DB926B3,
	Oni_GetConstraintGroupParameters_m841DD84B6D78A0B694506D815E4756313934B29F,
	Oni_SetRestPositions_m819342E902C330762AEB5BE9D4C68F31DA6CA8A1,
	Oni_SetRestOrientations_m87B3C65B2FA89627A6526A5C22B674280DC3C577,
	Oni_CreateBatch_m289C846274F339F1F3E4A026FE857DAAAC56ABE8,
	Oni_DestroyBatch_m0C065F052A4E0B16A426180BF8EC84E74845007E,
	Oni_AddBatch_m22AFEB35DB5CD69377763213EEED12183545A8DE,
	Oni_RemoveBatch_m62A5EE513FF64B56579A035F1463C0308DD01A48,
	Oni_EnableBatch_mE5AAEE57C0386681A3016600946C205D90CCBD0B,
	Oni_GetBatchConstraintForces_m2F4FB4C6732CFC54C859D7DB5C4A3AC75E4400FE,
	Oni_SetBatchConstraintCount_m31FD93985340AF90A5D98742034D010888144B41,
	Oni_GetBatchConstraintCount_mCDA7F7C7B72B04711AAD3435FFE3AB6FDF657966,
	Oni_SetDistanceConstraints_m6BAF3B1FE95B48E8E681121385077A369140BA88,
	Oni_SetBendingConstraints_m4DD1607554632126E50CA62B31DE223000DEE579,
	Oni_SetSkinConstraints_m980F260DC4107F86B16555CB40B9B63A196F558C,
	Oni_SetAerodynamicConstraints_m97BED4EFAF9B73A677757503EF1AF7EA59CB712C,
	Oni_SetVolumeConstraints_m7914A0DFEEF418BE4C6C81DE6A27EEC21701F511,
	Oni_SetShapeMatchingConstraints_mE2D86779FF689D2FBEB8F39FF1683CEAA9D7547E,
	Oni_CalculateRestShapeMatching_mD268677C88F2E8ABB2C0E8149F0683FFF9AF3BDD,
	Oni_SetStretchShearConstraints_m49F7345551857151AE2572A3F59A90FCA236CDA8,
	Oni_SetBendTwistConstraints_mE4976251409BFB7882CC39AD1F21D4A97B7F4D03,
	Oni_SetTetherConstraints_m10810C34086C7751FB8A5600EFBBC9C70887449E,
	Oni_SetPinConstraints_mC5929BC4A3B8861A32C47072223BD7C88CAC8843,
	Oni_SetStitchConstraints_m8576B5213542C38E501BD55E1C0936DF69506253,
	Oni_SetChainConstraints_m8F08E1B952B69C7A6D3F1957CA4511BE3D95D25D,
	Oni_GetCollisionContacts_mD80B55291B76FB42F97EE2C75EB2E5089540AE63,
	Oni_GetParticleCollisionContacts_mCDAC4DB2F8B1A798CEC15191044C4FE761AF69D1,
	Oni_InterpolateDiffuseParticles_m4A3F61BFA0B073204F17E45D12E58B292B3C5D44,
	Oni_MakePhase_m9FFA8D86C974D669797A0DF8FF1E6A6270554D71,
	Oni_GetGroupFromPhase_m1E9E33EEBF211AD24F224C4DDF044F55DEC4E6CF,
	Oni_GetFlagsFromPhase_m4EC4AEB3B15B30111080A14FB54D7939A4924F0B,
	Oni_BendingConstraintRest_mBF9E6E4CB4E998E291B956B4304FFB31A1ED6058,
	Oni_CompleteAll_m24DA6FE9992164ABDA19A2E0E5BB58280FD02582,
	Oni_Complete_m4909573C5EF85056474F9EEB3A8C91F6F62050B5,
	Oni_CreateEmpty_mEEB6A72C1C211426318B7A3164BD1697F1C0859C,
	Oni_Schedule_mF1BBBF5D226699D18E8EB55A8F31A08BE1DE528B,
	Oni_AddChild_m2AA3FE95D394A0AB211C7FAF46DA6DDA34645B49,
	Oni_GetMaxSystemConcurrency_mA467F4FF67759D65BBACC64B265BBD53DBEE3177,
	Oni_ClearProfiler_m3CE1E415966CFAA8CBF71A57B412B980CE3B5E11,
	Oni_EnableProfiler_m50B45E5B063E18108D99A6851C2C441A8FEDCFD4,
	Oni_BeginSample_m584963073106DEA213EA5679DA42F89AB29B0931,
	Oni_EndSample_mE3D8141E3F15F6DEEB30C16BDA9BC9E82368F8EE,
	Oni_GetProfilingInfoCount_m89525656BF980E088FBF35150F29DC2BCAF974EB,
	Oni_GetProfilingInfo_m5450B203396A0BD0042264E198B358D3FA41BA23,
	SolverParameters__ctor_mBBC8FFC199730BCFDE16BEE7BA9BCC31B01476C3_AdjustorThunk,
	ConstraintParameters__ctor_m3554D9568B4E65EF55CF0E0477821F3FC84C6566_AdjustorThunk,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	ObiActor_add_OnBlueprintLoaded_m04B2952178ABFA72F0A7E7219EB78A4ECEAE156E,
	ObiActor_remove_OnBlueprintLoaded_m998C17DB7FED249637AC6D40775D9D50ED1CA3F8,
	ObiActor_add_OnBlueprintUnloaded_mB1182B14609EA8CCD5A9BD7C2F3CEDDAFF78C601,
	ObiActor_remove_OnBlueprintUnloaded_mB1AB8C2B322EB4633B0CD45EF5AAF7448BAD68BB,
	ObiActor_add_OnPrepareStep_mC586B960142DDDF81A5B1C87D393C7CA379E743E,
	ObiActor_remove_OnPrepareStep_mCB8B15D01E8DA343CF2DD8E655932C886DD863CC,
	ObiActor_add_OnBeginStep_m7A401277ED988AF4C6388B1F768B4E613BA02E10,
	ObiActor_remove_OnBeginStep_m80E4C1C9DE59FB6075BFB27E9B8D885ADC413C13,
	ObiActor_add_OnSubstep_m4E244BDD4F0DA8AFBEF75036632A65CF4EC1286F,
	ObiActor_remove_OnSubstep_mABFE386CEC0194553AEE608F465F311B21360592,
	ObiActor_add_OnEndStep_mE226631AC3F2AA3BE52CCB4AB9972A0620E76208,
	ObiActor_remove_OnEndStep_m18354446434FA0FE622CFA9F09B7D0D98B7365F9,
	ObiActor_add_OnInterpolate_mA39BB43FF56C2FF95981971698C30856DC25ED65,
	ObiActor_remove_OnInterpolate_mE2B57508C1595198B4E27E10DA174A0B3792AD8D,
	ObiActor_get_solver_mDE668FD9EA16553E2252B51FC4AA0EE5F2476940,
	ObiActor_get_isLoaded_mE37E960D0EE5EF278241FE730E4E6D56FBBE217E,
	ObiActor_get_collisionMaterial_mFEBA9610A92672D379939CCF56E5A07800EF7AB7,
	ObiActor_set_collisionMaterial_m04C98A6B84C31070613652C4D12C3EFC0D98A0C4,
	ObiActor_get_surfaceCollisions_m280DE5273E14E28B1037C4CEA893F3BFFBEB2CB8,
	ObiActor_set_surfaceCollisions_m40EE79424632CAE1EBA8FB1C3087D54FD9FC5046,
	ObiActor_get_particleCount_m8A511B9B8EE21FE09B45EA484ADE54CE1A469C24,
	ObiActor_get_activeParticleCount_m330540C2CFF296103F759A43A156960A14EF40E3,
	ObiActor_get_usesOrientedParticles_m1F5AC23A705419FFA18109ACE2CD45861FFCADDE,
	ObiActor_get_usesAnisotropicParticles_m54157FE27C8DBE561125BC87EA21BA034F246B69,
	ObiActor_get_usesCustomExternalForces_m63A0FBED9893C8176AA7DB6B5567978FB1D71DCA,
	ObiActor_get_actorLocalToSolverMatrix_mCF3080C74BC381BF19F21BB6204A19DBDDFB82DE,
	ObiActor_get_actorSolverToLocalMatrix_m467C1F4C003B591FBE2C93BA9BDB33C0371962CE,
	NULL,
	ObiActor_get_sharedBlueprint_mE4DB059D49A26A1EF8D5AFD8CECEDB63CDACD863,
	ObiActor_get_blueprint_m47A228582F43BE1A2AF98A4CFBF052C0DCF852F5,
	ObiActor_Awake_m241F3014CB9150A6234C8BBFE5F7F1506CC4A4A8,
	ObiActor_OnDestroy_m2F3D4A01290E0E5EB2F3753AB9913AC96249A655,
	ObiActor_OnEnable_m971216E2924B472ED6E1EF5CA46C20711919C967,
	ObiActor_OnDisable_mCD8A0D978C2AD361EA37D62139DD083606463FD7,
	ObiActor_OnValidate_m52B606D1428A6597252E13B0A7664A6A2A9AC196,
	ObiActor_OnTransformParentChanged_mE56EB6C5787A9B68CC2DCBF59A69FC5DBAC340B7,
	ObiActor_AddToSolver_m4612A91281D3B2EEA44B2654DF515E757FA4D3FF,
	ObiActor_RemoveFromSolver_m6E84D3DEBFE722DD4EC517EB2622FC80350D4FEA,
	ObiActor_SetSolver_m6163A3076A049EAC233DCD2BB28B05C282DA55B6,
	ObiActor_OnBlueprintRegenerate_mC7B6851CA42C438CCA449E6F6B675C88F4515C71,
	ObiActor_UpdateCollisionMaterials_m5E96C2AE2B9F7811085F85D0D25A5621C46D6817,
	ObiActor_CopyParticle_m8AA520BF621655A1D7FD60BE63769B097E1996D6,
	ObiActor_TeleportParticle_mD6150ED98C32F82A0ADB734DDE58A37BA0B78E39,
	ObiActor_Teleport_mE63A190E1F4170CF5E1BE19522E3B6061AE96A04,
	ObiActor_SwapWithFirstInactiveParticle_mC6593AA19F1C4AF00040F46D4EC723008B97B52B,
	ObiActor_ActivateParticle_m45FF9EA8B2788D3C9EAE0EDE77DA4FA2133BF9BA,
	ObiActor_DeactivateParticle_m04DDF95A73A8AE09950848D4A8DBB2A57EC42C59,
	ObiActor_IsParticleActive_mFD14C2BCE7E86131404B2EEDA413555B7288C0E3,
	ObiActor_SetSelfCollisions_m45F0B0F5D1A4306D161C7EEC3C1435D9D52E3686,
	ObiActor_SetOneSided_m99DA6DD46C17B2357C1815E76EA6FB70B6525D33,
	ObiActor_SetSimplicesDirty_m1FA63CB84FD2F2219BB54CB69CC5E8473FE2167A,
	ObiActor_SetConstraintsDirty_mF1D5DEE2405AE6F94AE264FC9CD0A63C31189D0F,
	ObiActor_GetConstraintsByType_mA485A63E65B2E5A863562BA515B15F6DE11138E7,
	ObiActor_UpdateParticleProperties_m9B2C616C0275DB62285F9E0CB0F1FF615D9FFAFF,
	ObiActor_GetParticleRuntimeIndex_m4AECF54626524359EE8CA930022DDFA864946B0D,
	ObiActor_GetParticlePosition_m02493A97C489BA4E448F1B3BE3AAB59DB1E9039A,
	ObiActor_GetParticleOrientation_mC35CD07B08A81AC2159E02DF583118B7ACF8E9C3,
	ObiActor_GetParticleAnisotropy_mC8BAD8C2ACCEB1DF3284AEF13CC5E12ABEFF727A,
	ObiActor_GetParticleMaxRadius_m834AF87A1D3204613AC0A6658EDBAF54871C13BD,
	ObiActor_GetParticleColor_m08A638A989F579EFADEA293B32C74635EECD25EA,
	ObiActor_SetFilterCategory_mAE60934D51EBD397225139EE05F2E80F08118D7D,
	ObiActor_SetFilterMask_m7FED73FD99F34632090C9174E5EB278DC5D76F08,
	ObiActor_SetMass_m8D79A26F4FBEF1A895C6F482808A23A02B8BFAC4,
	ObiActor_GetMass_m2E9973F1EE3CBB300948F36489B7A7637B768F98,
	ObiActor_AddForce_mE87CF6C0A21C03ED45209758E7BFE0E1AB2B1A00,
	ObiActor_AddTorque_m43A494B40EA87E0EC7A6274A0931F9CF27B5D77D,
	ObiActor_LoadBlueprintParticles_m11D7669681BB9F35863E2BE6B9B50F9C7F8CFE88,
	ObiActor_UnloadBlueprintParticles_mAFE44CA41C0A763A6142420F2145AAEDCFD296A3,
	ObiActor_ResetParticles_m081DAAF528B4EF4F9E79AA17ECBEFC2C0BCB03D2,
	ObiActor_SaveStateToBlueprint_m86BB211764276B407A42BF4C603D75AEBD260F84,
	ObiActor_StoreState_mDF522A37AED6A366CD98C42E6D12EB524E5A6B67,
	ObiActor_ClearState_m2708E40E0CC1259F0A4A23A134D7C08A34D64D9E,
	ObiActor_LoadBlueprint_mD8204B2EFE860EADB85ABAAEA380D0EB7DE52966,
	ObiActor_UnloadBlueprint_m0380FCD5E41E56876B933D34F94EE112FE49D14E,
	ObiActor_PrepareStep_mB06CED03B998A63A4AC43A5318D99D1AE8ABA400,
	ObiActor_BeginStep_m2CB8338B9754937B0F151D9F06AF97263B2D313F,
	ObiActor_Substep_m784F85C0BD4464D6E4B1BCE24E44047956C78486,
	ObiActor_EndStep_m5076BADB8970EC68040EA131738946988F140513,
	ObiActor_Interpolate_m589CB41AB063846C5C621944F1B14EACB90C3683,
	ObiActor_OnSolverVisibilityChanged_mA9B03F298B37CD3BB66DD5527F9E3CDB43A728EB,
	ObiActor__ctor_mE992775E0773F687C886A5C462E2EAB7FA5D740E,
	ObiActorSolverArgs_get_solver_m2C65EFA043F915BB25BF5EE5F467EA1119C55BDF,
	ObiActorSolverArgs__ctor_m645D2D61FF1A0780D4B70C2494C8C90607C7FA1B,
	ActorCallback__ctor_mA6CF95A70DAB4FFF21823741E58A598FD0A3CC61,
	ActorCallback_Invoke_m44385DCB93F7AC483E18B7477BDEB1C44993D508,
	ActorCallback_BeginInvoke_m511CEA8353354A1869B3548D98C3EA166C757E30,
	ActorCallback_EndInvoke_m44778EF05F7C946EE7ACD0315B4F14F101980024,
	ActorStepCallback__ctor_m48119AF13D841B3D5476543A1C79108F94D53E00,
	ActorStepCallback_Invoke_m25120AB25E0465B7AF5976D51946EED8935E2A38,
	ActorStepCallback_BeginInvoke_mE4197C4E139BFCC95434E2C3E20BE882EB5F2989,
	ActorStepCallback_EndInvoke_m8AA8AAD3C0E68BB1AF5F7E6A61EA2269F3A57AFB,
	ActorBlueprintCallback__ctor_mADC829707A4F631EADEE895A25D2AAEA59A4D64D,
	ActorBlueprintCallback_Invoke_m7BFF8526B4E3B31969016C044E63EA9AF3FFE7AD,
	ActorBlueprintCallback_BeginInvoke_m32D286D241BEC1F0C4FD9CF6C04C5E7BF1823485,
	ActorBlueprintCallback_EndInvoke_m7B651D23DA455A93DCC87B95CCE21A5789433013,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NullBackend_CreateSolver_m149A74012C4CF9DEC9EC68F1C16EE99F3D6E88AF,
	NullBackend_DestroySolver_m5E51947F4BEC3D57534FD796732535ED2B83D3F6,
	NullBackend__ctor_m7C4DE499807A5119E6914F1A7B6666E7BBFE44A9,
	NullSolverImpl_Destroy_mCEF74F887CB2BF0E13C8176A052B1539F51BD6AB,
	NullSolverImpl_InitializeFrame_m1A9943BBB1200D17394A300365060CC1C180C333,
	NullSolverImpl_UpdateFrame_mE9842C5421EB8BCEF09BEF66B7ECE4C6BBF68453,
	NullSolverImpl_ApplyFrame_m6E1688FD679EF4B17A6C885CD116D47885BA9A6D,
	NullSolverImpl_GetDeformableTriangleCount_m5B5658F9B5D43BC6CF7131229FE97D16E931E5AB,
	NullSolverImpl_SetDeformableTriangles_m3555C196B17530814FC47EB0D65B0B19428AE73E,
	NullSolverImpl_RemoveDeformableTriangles_m327A23C82D523DB2799DC060C88746468C9CB012,
	NullSolverImpl_SetSimplices_m31A164287A45DD5E59CD0C787A24C85DAB4C3D40,
	NullSolverImpl_ParticleCountChanged_m8557EBD352BB202123699487E00B843F3CA0DC0B,
	NullSolverImpl_SetRigidbodyArrays_mEC64908F48D6E3EE3F03975F21201DAD33E36848,
	NullSolverImpl_SetActiveParticles_m270CC166EEFCBB7494E52DFD1F7385B091AFB05B,
	NullSolverImpl_ResetForces_m02F4803EE6AB50D5563B60A00E5535B12F8E3B11,
	NullSolverImpl_GetBounds_m4D24F01134C67A078F93680525FB8DC5804A3DBB,
	NullSolverImpl_SetParameters_mDCC55383F8800DB49278298ABF8A176E6F5B3CAA,
	NullSolverImpl_GetConstraintCount_mA5540FE96215F61ADEFE829931F4D927E70019E0,
	NullSolverImpl_GetCollisionContacts_mD2C3B675F93A95F33D2472E11AD4F48C503B2B54,
	NullSolverImpl_GetParticleCollisionContacts_m55885C928BC24A887BD63F783CD979F085E07381,
	NullSolverImpl_SetConstraintGroupParameters_m3A8213C3F85B7A2187B6030CC6EDC4DE5CB505BE,
	NullSolverImpl_CreateConstraintsBatch_mB98D773275819E868529278DC289D6E084CC913D,
	NullSolverImpl_DestroyConstraintsBatch_m42D46BA79047F3A0DBD2B305549E97FA654EF2B7,
	NullSolverImpl_CollisionDetection_m3BE61763707C061EB9912C7E9DCD44C3F135C0FB,
	NullSolverImpl_Substep_m46BCCC5D64A69DD461D4EF42A3D227A2F53A6A08,
	NullSolverImpl_ApplyInterpolation_mB63837B2AE0542F306D50ABF7A05868F2A9A75BC,
	NullSolverImpl_InterpolateDiffuseProperties_m5D4052E47F4C0CE896A55E4932A7AB35F0001133,
	NullSolverImpl_GetParticleGridSize_mD74EA2D59BC1B4472754D76EFF3B4FBC969B2A66,
	NullSolverImpl_GetParticleGrid_mCBCC36302C00CB01775017E21F63754021E04BEC,
	NullSolverImpl_SpatialQuery_m252FFC078850F330BB8276D6996CD2303F90D6F1,
	NullSolverImpl__ctor_m835D73EC52194B7861F10BCEC2C8F3D30E98D6EF,
	OniAerodynamicConstraintsBatchImpl__ctor_m08CF6D0353900E2F5161D01B30D6D4FD181EAF67,
	OniAerodynamicConstraintsBatchImpl_SetAerodynamicConstraints_m78BC6505E02E608B6E7E64C31C785C0653DCCDD5,
	OniAerodynamicConstraintsImpl__ctor_m525B8236B34A2BEB8A129BC2DFCAD6433E10673D,
	OniAerodynamicConstraintsImpl_CreateConstraintsBatch_m3C15118FE42A98BE00C420EFF5CC108133E8C9D1,
	OniAerodynamicConstraintsImpl_RemoveBatch_m7312BFD8301F75F44EF449DAB1CA208CDF256820,
	OniBendConstraintsBatchImpl__ctor_mD888A9E984F69E1F75358F7C87F224E5BBFEF9C1,
	OniBendConstraintsBatchImpl_SetBendConstraints_mACD9168310968682EC7F9FAF4FAF01D439561251,
	OniBendConstraintsImpl__ctor_mDF7E828956758CC442DFCE530D6ACB3DA28C7520,
	OniBendConstraintsImpl_CreateConstraintsBatch_mE943BA310CF0508FA4B457AE9C4CEDE436C0256C,
	OniBendConstraintsImpl_RemoveBatch_mE65A138E34D866312EA8D6811377B0D2A5A8F759,
	OniBendTwistConstraintsBatchImpl__ctor_m8F131CD09F6F7F09BBDA55AF3E953E5EA2D1F52F,
	OniBendTwistConstraintsBatchImpl_SetBendTwistConstraints_m93208190B4C9F21B66804826F3F388365464C7E6,
	OniBendTwistConstraintsImpl__ctor_m2D810DA4120C0AB84298B4A542CA5B932E774DCB,
	OniBendTwistConstraintsImpl_CreateConstraintsBatch_m5F1FC4850DB914A408732085F9AAEB1FE7D9DEF9,
	OniBendTwistConstraintsImpl_RemoveBatch_m571856C1E93A0B2507FD9309A2984CAD3143BB55,
	OniChainConstraintsBatchImpl__ctor_m11A76775D8DA7D09B7DACBF1D1168D4FEF4F1694,
	OniChainConstraintsBatchImpl_SetChainConstraints_mA704113F26DE40220400DE5B85B1475C2B3D0A82,
	OniChainConstraintsImpl__ctor_mAE3D234A2597D468A4621529AECFC4CD2D1BB328,
	OniChainConstraintsImpl_CreateConstraintsBatch_m84FCCD1FC3FF8E6DE2FF68D3B259A0AF6E595B9D,
	OniChainConstraintsImpl_RemoveBatch_mD1FF35D0BD359EB12FC8A74DE8A560E487A9057B,
	OniDistanceConstraintsBatchImpl__ctor_m9005CD0907A9E6BACBBDA3777B6717B8611AFA18,
	OniDistanceConstraintsBatchImpl_SetDistanceConstraints_m2305BC0CB077A2F81D031356D983305E8344B34B,
	OniDistanceConstraintsImpl__ctor_m46EF17FABD8A1BFB1EDE24EDE0F6EB61473907F8,
	OniDistanceConstraintsImpl_CreateConstraintsBatch_m68ACFFA63F7B3ACF12200ACCDD9796BC1E00453D,
	OniDistanceConstraintsImpl_RemoveBatch_m4F114348DEFF33909A2AFCF15D10D98187F7912C,
	OniConstraintsBatchImpl_get_oniBatch_mD3068ADFBFA72D2C5AEC1E4493AF44A61BAAB3D6,
	OniConstraintsBatchImpl_get_constraintType_mA4D89C531D6E50262AAB808ED481F01FCBC4554F,
	OniConstraintsBatchImpl_get_constraints_mC86F66AEBA1CCFE2EE1484C2CA0844257A7A0CCD,
	OniConstraintsBatchImpl_set_enabled_mE3FE79E17C8786437000CF9DF7ED7CC24C58DD1F,
	OniConstraintsBatchImpl_get_enabled_m74C3931C9C466399F511D1258F6DF49FAF007D8F,
	OniConstraintsBatchImpl__ctor_m7E856D436221DA306E7EFF8409F799DC8F926C38,
	OniConstraintsBatchImpl_Destroy_mB934F5194DA3A35DAC45581D183BE8C1BB056A1A,
	OniConstraintsBatchImpl_SetConstraintCount_m9A9C7B081DBCAFE727F2682F742B32A0A5CD1FC7,
	OniConstraintsBatchImpl_GetConstraintCount_mBC3CD543643EBE713BEDBCEC5266DE52F0E6E53E,
	NULL,
	NULL,
	OniConstraintsImpl_get_solver_m44585FB563C805AF280120F7FDE3B5737F4C0411,
	OniConstraintsImpl_get_constraintType_mF0F59F28976DF29B32C94C4156BD7752105087CC,
	OniConstraintsImpl__ctor_m9A0E8C6A195C96B8CB7EF27D99B7A0988672BF61,
	NULL,
	NULL,
	OniConstraintsImpl_GetConstraintCount_mDE65EE94DCFAB8905A9AEF5543E7119928F2900A,
	OniPinConstraintsBatchImpl__ctor_mA95003D5E91633FA72AA1F912FFCC947CBFC25CF,
	OniPinConstraintsBatchImpl_SetPinConstraints_mB28FD52B804E30D4B06AADE5A8764C9F3F4DC223,
	OniPinConstraintsImpl__ctor_m2F0E0AF0C30CE5AA9BBA5B0F2C170582A8442ABE,
	OniPinConstraintsImpl_CreateConstraintsBatch_mF0305992D09B680091BD1EEF5519A801F19F69D0,
	OniPinConstraintsImpl_RemoveBatch_m1EA7BE417BEF16951FAD0CA1106DB5BC257F38E1,
	OniShapeMatchingConstraintsBatchImpl__ctor_mA35A5ADBE7591DFAFCD6775D89CD7C7BBEB7E25D,
	OniShapeMatchingConstraintsBatchImpl_SetShapeMatchingConstraints_mDDBCB5CFAB68DDF0E19581524119A7981D214588,
	OniShapeMatchingConstraintsBatchImpl_CalculateRestShapeMatching_m6E5073835BB3D1CED559A45D987CC3FCD9D4C21D,
	OniShapeMatchingConstraintsImpl__ctor_m945AA881838FE9644266E98CA531247FE004C1D7,
	OniShapeMatchingConstraintsImpl_CreateConstraintsBatch_mB055C2DDEB21A68DC4E3A56E2A989231D1109BBD,
	OniShapeMatchingConstraintsImpl_RemoveBatch_m0C9FC7FFB2680541A78037980C49C907F3598756,
	OniSkinConstraintsBatchImpl__ctor_m5DDA5D3F347C6EC9FF501C52A4374AD88A10AABD,
	OniSkinConstraintsBatchImpl_SetSkinConstraints_m8065C2605A9D036F95C2F1B234883144F9C84CD3,
	OniSkinConstraintsImpl__ctor_m9A29E8FD8F9F99978DFF2EB7149829624A7939A5,
	OniSkinConstraintsImpl_CreateConstraintsBatch_m9FB2EAD640C26270C35A54351F354F8AA1D7E043,
	OniSkinConstraintsImpl_RemoveBatch_mC0102FC9F40941F1DDEBCDA28632CBDD43660523,
	OniStitchConstraintsBatchImpl__ctor_mC565125A40106134B8A526F21E536F819AACDB34,
	OniStitchConstraintsBatchImpl_SetStitchConstraints_m850BB5F12FBB10D113073FFD360406906FE84F55,
	OniStitchConstraintsImpl__ctor_m5A0F9B6CD43ECC7D7AE65AD107B559178559308C,
	OniStitchConstraintsImpl_CreateConstraintsBatch_m94119E21DE281CD292B556C5077176B4D9C9284F,
	OniStitchConstraintsImpl_RemoveBatch_m7B1ED49DA5FB278734D0C9F8597E820B3E284FAC,
	OniStretchShearConstraintsBatchImpl__ctor_m06A2F02D467608368981ECD3800852E4B1229908,
	OniStretchShearConstraintsBatchImpl_SetStretchShearConstraints_mACC9CB76D4FDD53A4FDD96FA2031EBD5F893B6AA,
	OniStretchShearConstraintsImpl__ctor_m743767DB7C25C334E9584FACAF87392343A39ACC,
	OniStretchShearConstraintsImpl_CreateConstraintsBatch_m78706F1FEE7FEAA5EF471FAB390E0E0D71861127,
	OniStretchShearConstraintsImpl_RemoveBatch_mF8F67B57D02D49DEC0DC0197146B243A44090457,
	OniTetherConstraintsBatchImpl__ctor_mA18D6CEBB82288A3B578C8C4C52F4E0119DAB0B4,
	OniTetherConstraintsBatchImpl_SetTetherConstraints_mCA9A516027A94559796B9C29CD716F10CB4BEFD7,
	OniTetherConstraintsImpl__ctor_m79D07270D5F4E7E62395AA8BE2B0F5B2C5B9AD25,
	OniTetherConstraintsImpl_CreateConstraintsBatch_mD49AD0DA234A2CBAF481401273DC88CF16D87AD7,
	OniTetherConstraintsImpl_RemoveBatch_m4B4909C5425342565704DFD35EDD47C5A1E2FB20,
	OniVolumeConstraintsBatchImpl__ctor_m17E79E7DCFE92922A8861BE569C3FF460D175CAE,
	OniVolumeConstraintsBatchImpl_SetVolumeConstraints_m9C580C7D330A95E2032129920D22CC13D70E8AFC,
	OniVolumeConstraintsImpl__ctor_m5147F9E64F13F4D205515CE6F1906887F787BA74,
	OniVolumeConstraintsImpl_CreateConstraintsBatch_mF7765774D05626A213C729351AE71CF664A0A8B2,
	OniVolumeConstraintsImpl_RemoveBatch_m726F19C3F55138E1BB32C8D3C97804E44013547C,
	OniBackend_CreateSolver_m5E3C44A4EA9BCDD470E7E10E684F0AF897C8AF5F,
	OniBackend_DestroySolver_mC6AFB023EC9F9532FF2B840DDF955E89C71A0D01,
	OniBackend_GetOrCreateColliderWorld_m92C5F279126E62C80B84C7ACAC6FEB209751EF1D,
	OniBackend__ctor_m45B8DE94A53B0740C6704A51A4BC4FDEB7C7D4F7,
	OniColliderWorld_get_referenceCount_m2E1D878672ADF99B997AA9EF3050BD41B27DC9FD,
	OniColliderWorld_Awake_mBB4F8B9A43CB88C337CD9DB344E0B7169D47E923,
	OniColliderWorld_OnDestroy_mAF83AA5D931A38B0E55E2226B94527B592FD9D83,
	OniColliderWorld_IncreaseReferenceCount_m78FCC3BFF315F84942D3994F5092A54A8D7F678E,
	OniColliderWorld_DecreaseReferenceCount_m35AE9297895C720AC03E0D0F320DDAD1366BF64C,
	OniColliderWorld_UpdateWorld_m07BB5250B08FE6F9B66FBDC04C2B54A98C4B4251,
	OniColliderWorld_SetColliders_mBC49AA3ED5E83E70CE733DCEC0683FF672872263,
	OniColliderWorld_SetRigidbodies_m28F9E1B58302DB5A756C243EF52E788E708DA85F,
	OniColliderWorld_SetCollisionMaterials_mEB26C8AC30F1B5CE27D8F19A77ABB7B252115E0D,
	OniColliderWorld_SetTriangleMeshData_m4965F3D38925BFCACB96B6B3479DD4F232ED7104,
	OniColliderWorld_SetEdgeMeshData_mD9F54F0603E44D518BC9243FF1979EC24E25FD44,
	OniColliderWorld_SetDistanceFieldData_m2C1ADF6E94752BC41CE632A804CBAE1AC19A9EDF,
	OniColliderWorld_SetHeightFieldData_m4C593F78531A7AE191FA9F8D4A8BBFC23E84F68A,
	OniColliderWorld__ctor_m4DE663B94B5C376427CEB42288BA88428E549A52,
	OniJobHandle__ctor_m3C938A79AA35165ECEC272FDB9E229430994231B,
	OniJobHandle_Complete_mB9E3D94D50CF207A0F961C9F4ADEB30B2FA8A6DE,
	OniSolverImpl_get_oniSolver_mA021288BE5461FA7F14399158965ECDD9ECA1706,
	OniSolverImpl__ctor_m2FE0413D6AF7939DBE480B789D43360A39DDFAD4,
	OniSolverImpl_Destroy_m317E88AD3A919043461CE59F399DF3670A904961,
	OniSolverImpl_InitializeFrame_mC8E921CBD6A70A2F9F46310EB5556586B6749DC7,
	OniSolverImpl_UpdateFrame_m52DA894215FDD134640AD29208E780EEBD65180A,
	OniSolverImpl_ApplyFrame_m67E28FA4B4FBE5C072E359F3574F562C59820DB4,
	OniSolverImpl_GetDeformableTriangleCount_m230CCC63F4275668FF4EF1B34F56859D5C70E68F,
	OniSolverImpl_SetDeformableTriangles_mE67CB6225F084DE826F48AA4D27E43FA43BE9827,
	OniSolverImpl_RemoveDeformableTriangles_m3B80FB7C6FA7DB0890B5BC37670151643F54C650,
	OniSolverImpl_SetSimplices_mD571A7AFB950F771AE96A6A30743D2E7CE5BAE5B,
	OniSolverImpl_ParticleCountChanged_mC72028DD674A3E54C4E08E758A3CE3D26E3E7577,
	OniSolverImpl_SetRigidbodyArrays_mFA7655D4B61E7E1C65C4E42C8B77EA55DFC70AEE,
	OniSolverImpl_SetActiveParticles_m850250C4A4DE24AB05AB59AADCD43D6F232077E8,
	OniSolverImpl_ResetForces_m5E8F36B6FCA63BD1E720DBFD5B8764863E3CE626,
	OniSolverImpl_GetBounds_m478CF8A2FAB372FE347E9899D262869163ACCB57,
	OniSolverImpl_SetParameters_mF506A029DB22FF674610C76D892D387BAD7178A5,
	OniSolverImpl_GetConstraintCount_m7C95C3E195679C7076DEBAB096213874408B03EF,
	OniSolverImpl_GetCollisionContacts_m3179A5E075C13134C753E47D58F150C004D15906,
	OniSolverImpl_GetParticleCollisionContacts_m4B99957B57A3E6BAF05C9E3D64DF28B5E0748259,
	OniSolverImpl_SetConstraintGroupParameters_mB58C6320D75A44B682B4AEA82214F8ACA62E53DA,
	OniSolverImpl_CreateConstraintsBatch_m388EEDF658EF8F63CD78DC8DC33F0DC642688911,
	OniSolverImpl_DestroyConstraintsBatch_m7769244161551FB17B31600D453AC2F68F156860,
	OniSolverImpl_CollisionDetection_m56F7F7DC6C224E1DEE5CF8A1437ED6FE02BAB5E2,
	OniSolverImpl_Substep_mCA3F4A59045D0A506692A48C78ACE6BB4E884D93,
	OniSolverImpl_ApplyInterpolation_mAD4820F8A797FF35107C1C920D5B50D4415C67AD,
	OniSolverImpl_InterpolateDiffuseProperties_m0678F52DF465E6E9B34DECE91C6C3D48F96E8821,
	OniSolverImpl_GetParticleGridSize_mC858C1BFF95D86B1B0841E40378F882A014E8E78,
	OniSolverImpl_GetParticleGrid_m3A454F1EA5EDCF6A590F99A29EEAC84F0D65B790,
	OniSolverImpl_SpatialQuery_m6BD4BCC1AACE17D8D66365F9FE7F16012A1E8F7F,
	NULL,
	NULL,
	NULL,
	ObiAerodynamicConstraintsBatch_get_constraintType_mBC6B3C472BCF899A37C3C9F15B61F0B22DB160F4,
	ObiAerodynamicConstraintsBatch_get_implementation_mD6185053ABF60610B561CE15B51770F6E2556E08,
	ObiAerodynamicConstraintsBatch__ctor_m9DE858EA294B273AEED18201A289AEC50F28F02E,
	ObiAerodynamicConstraintsBatch_AddConstraint_mB9A927126FB700DC1E84DDA91D9132310EFEE124,
	ObiAerodynamicConstraintsBatch_GetParticlesInvolved_m430311A6B436224B845F2F196525AD41A67EB605,
	ObiAerodynamicConstraintsBatch_Clear_m5ACDB0CA34591C9F454153521B67AC2478467EA2,
	ObiAerodynamicConstraintsBatch_SwapConstraints_m3F24E8A02E9F7E06130C7E27A8438623CBC36B99,
	ObiAerodynamicConstraintsBatch_Merge_m4CEBEA6CE4426002B0326CFCAE2C25E3F4954CD0,
	ObiAerodynamicConstraintsBatch_AddToSolver_mF2BA5925F8D160264B0EC1995C4EF9856E4E48FB,
	ObiAerodynamicConstraintsBatch_RemoveFromSolver_m3288141E2907B24EF4337F9C513C6C69D68D8684,
	ObiBendConstraintsBatch_get_constraintType_mA302627388787213E045E481476B7C9EB7F43D92,
	ObiBendConstraintsBatch_get_implementation_mE6CC10221A772592842A23E0EE91871FAA8ED5FA,
	ObiBendConstraintsBatch__ctor_mA90DDA31688111131E370BE9154E15ED99CF09E9,
	ObiBendConstraintsBatch_Merge_mC7EBF91A7220B4C03792E048ABB718337C6F03AA,
	ObiBendConstraintsBatch_AddConstraint_m59872A1ADDD39CF3FB2C303EA08FD4E6E9AFAA0C,
	ObiBendConstraintsBatch_Clear_mA0DC32AE2AE98E09AF547DE49FB25D8CDFF0DB97,
	ObiBendConstraintsBatch_GetParticlesInvolved_mA9C31748143F96BC5E341BE711B68E9F78204C4A,
	ObiBendConstraintsBatch_SwapConstraints_mD42CD848B36CEB0540B9F211A1C7592348949487,
	ObiBendConstraintsBatch_AddToSolver_m65C74A506765E2DFFD6575EE0E9D8AF1F723EBC1,
	ObiBendConstraintsBatch_RemoveFromSolver_m771E799A2D261FFC3AB2686EB70676ABEBCE21F7,
	ObiBendTwistConstraintsBatch_get_constraintType_mAC26F20B9CE45F7ACE966652B0ACAB8337A9703D,
	ObiBendTwistConstraintsBatch_get_implementation_m293D659D51624728D74EB2EC1BEF2EBA93A301D2,
	ObiBendTwistConstraintsBatch__ctor_mA84FE1A64BC385B93B82ACDB515147D1BFD9D8E9,
	ObiBendTwistConstraintsBatch_AddConstraint_m04DEFCAA48B91E8849ECC4FB1F151C17740B3E78,
	ObiBendTwistConstraintsBatch_Clear_mEA7D8BB6BCCA8C72D2CB42CF1D9C197F3C3FB905,
	ObiBendTwistConstraintsBatch_GetParticlesInvolved_m7F039D1FBFA40A6E0EFFB3F66FAA2A5D4D621393,
	ObiBendTwistConstraintsBatch_SwapConstraints_m19754F194196E8509DE60D4C6A4E01CDE61951CC,
	ObiBendTwistConstraintsBatch_Merge_m65D2EB86911176AE0DBCFC755950330B0FF4D9C4,
	ObiBendTwistConstraintsBatch_AddToSolver_m00517D42056B5BD7E11AF70F1C617FD33E313B35,
	ObiBendTwistConstraintsBatch_RemoveFromSolver_m89781A48762FBA02AFF8BE9423981220F26A147B,
	ObiChainConstraintsBatch_get_constraintType_mBAD7C6C6A0862F3A03015BCA2BA2B969706672C7,
	ObiChainConstraintsBatch_get_implementation_m2883B97D7E6175B2C1FE458CD3E667789C1FEC92,
	ObiChainConstraintsBatch__ctor_m8E8E6310A3DDA3527AFF7F0DF6A851F75B52AEDC,
	ObiChainConstraintsBatch_AddConstraint_m8615D25C5C8628CEE00BD28F9D2D465A635813CB,
	ObiChainConstraintsBatch_Clear_mC5BA1FD8A165468C43063493EF8BFB93A3D2CE21,
	ObiChainConstraintsBatch_GetParticlesInvolved_m1B0852EE90B8C0089C7793F4915B9D5C5E739187,
	ObiChainConstraintsBatch_SwapConstraints_m56826B5809EDF1D888AC022EC80813AB5E8A8029,
	ObiChainConstraintsBatch_Merge_m7C8E59CC49A14B6A3F20B923CB9983A18709369E,
	ObiChainConstraintsBatch_AddToSolver_m8E12FD5F4ACAA95E5FFA4E57A15C6D01796354AA,
	ObiChainConstraintsBatch_RemoveFromSolver_m97FD3AAF49F7296DC655C1BE2B0C866D40330AAB,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	ObiConstraintsBatch_get_constraintCount_mA8FC55D7B67C6DE94D5B7383C39D4330CEC3C4A8,
	ObiConstraintsBatch_get_activeConstraintCount_mC2764F79C4929BA7245103D0DB1CFB1D6203FC02,
	ObiConstraintsBatch_set_activeConstraintCount_m5C26A4A50F8C1542DD4EA73BDACF3814B758155A,
	ObiConstraintsBatch_get_initialActiveConstraintCount_m5F0841BDCCB934AB148E2A870F3EAA2F8552189B,
	ObiConstraintsBatch_set_initialActiveConstraintCount_mF8081C7DCFDC4CF1B2A306517632F6E9C026912C,
	NULL,
	NULL,
	ObiConstraintsBatch_Merge_m0157340350A52C6574BBB2D68D612AA6852ADEC3,
	NULL,
	NULL,
	NULL,
	NULL,
	ObiConstraintsBatch_CopyConstraint_m204FC07814B69BA73E78A0E743D059F565B85874,
	ObiConstraintsBatch_InnerSwapConstraints_m72121F6E08F53AA93A94891FB98F3B6FF2D23A91,
	ObiConstraintsBatch_RegisterConstraint_m8EABAFCE01074053D942CE05D9CF5CEC9381F5FB,
	ObiConstraintsBatch_Clear_mC782D142E91B8F81F511A011339846EC1310C2D5,
	ObiConstraintsBatch_GetConstraintIndex_mA7193601AF174D3A9323DCBC41B86CF2F9EDF24B,
	ObiConstraintsBatch_IsConstraintActive_m86CFCB36A5A1AEBBE1FA877DFEE386BB244A7C65,
	ObiConstraintsBatch_ActivateConstraint_m715C1DA819EA321555CC3E50E102E4130B0DF12E,
	ObiConstraintsBatch_DeactivateConstraint_m75EE8E2E70006545791D2BA14E1E4513F9193025,
	ObiConstraintsBatch_DeactivateAllConstraints_mC7259C1E725CF3BFB8EC6ECFD548E6C70C2F63C5,
	ObiConstraintsBatch_RemoveConstraint_m5AAEA90963297F57C4BFF0C6858AA1E13AE6EC46,
	ObiConstraintsBatch_ParticlesSwapped_mFA21282517AF5D3D422315E8748273AC39BC7ED1,
	ObiConstraintsBatch__ctor_m2B0D34B60AB411AA8D929BA04F3BF98231678A81,
	ObiDistanceConstraintsBatch_get_constraintType_m738D4B915ECC57433D1FBE3736930F510797A6D5,
	ObiDistanceConstraintsBatch_get_implementation_mB2318A9886BF7C3D0513E65A18526B4F28740515,
	ObiDistanceConstraintsBatch__ctor_mE47DA7819812295CB5E274C6A916E54E705A0EC3,
	ObiDistanceConstraintsBatch_AddConstraint_m118C1575A23FFAC933204048E9DD894F44CDD021,
	ObiDistanceConstraintsBatch_Clear_mEE9F3DDD52ECC58C3A9B4391667F88D3FF4B48C4,
	ObiDistanceConstraintsBatch_GetRestLength_m5E1C408F549700872BADA871F7301C1A368E0F12,
	ObiDistanceConstraintsBatch_SetRestLength_m1CA836FD76DCC1AC15BECCF86DD92E487F71397E,
	ObiDistanceConstraintsBatch_GetParticleIndices_mB6664FFE2333714B74F7BFEE6D915DC49C3812C6,
	ObiDistanceConstraintsBatch_GetParticlesInvolved_mC73EBE0B6DB236EC4BE938CB43ACE50F2AE65EFE,
	ObiDistanceConstraintsBatch_CopyConstraint_m0EBF810160015C8A19A39367C329D343DBD1BF44,
	ObiDistanceConstraintsBatch_SwapConstraints_mFC45320687BA195BF90408368D14ED2B804DC370,
	ObiDistanceConstraintsBatch_Merge_m4D30F883821CC7CF022584BEC90B548D14C6DC15,
	ObiDistanceConstraintsBatch_AddToSolver_m3308FD903CF4BC955B8D2F2E0B387D2C8A79B7B8,
	ObiDistanceConstraintsBatch_RemoveFromSolver_m6BC9C6E426F431C44B10DCA8CBFFEF4F67E0936E,
	ObiPinConstraintsBatch_get_constraintType_m41C00D52B2D27084965D72E1F35D4E075444E482,
	ObiPinConstraintsBatch_get_implementation_m4578AAC572F0C5466E210AC1BC23930F8C09C0E6,
	ObiPinConstraintsBatch__ctor_mEE99790C9B386AE46D24F129CDDCF2C1C5B5F5FC,
	ObiPinConstraintsBatch_AddConstraint_mF22DD07678C73491928469A7741BD8B076071DCC,
	ObiPinConstraintsBatch_Clear_mB0A07E7DF21B6C3CF9C5E054F58510FFDAA522AE,
	ObiPinConstraintsBatch_GetParticlesInvolved_m91C044A47CCA58E91BD3E72C7DE0ECE74BC37DA8,
	ObiPinConstraintsBatch_SwapConstraints_mAE0389E323855EC02CB78CE15E73076B72CB1D85,
	ObiPinConstraintsBatch_Merge_m2E0E93A262BDA66314A1CF7125B02B0E3D3AA3D7,
	ObiPinConstraintsBatch_AddToSolver_mDAB05B2150F51AA80C2911CEF1BCE5E104D37E86,
	ObiPinConstraintsBatch_RemoveFromSolver_mC766AF925EE815328507994A26FC0DA6399A473C,
	ObiShapeMatchingConstraintsBatch_get_constraintType_m1D1B013631E72B441D2F440FABC9A47260BC1EFC,
	ObiShapeMatchingConstraintsBatch_get_implementation_mDE72A361D520AEF2417D5FA3D99265B24AE622BE,
	ObiShapeMatchingConstraintsBatch__ctor_m07C5E1D77AD00968700D64371442119BD0EFD860,
	ObiShapeMatchingConstraintsBatch_AddConstraint_m36DB2368774B955435601E74A559F9194797B73E,
	ObiShapeMatchingConstraintsBatch_Clear_m7613DFE3A4A449971A4C9C0E7F78D8ED7D99B7CF,
	ObiShapeMatchingConstraintsBatch_GetParticlesInvolved_m460EACD5CA047B22B48866E7980990188EF04B88,
	ObiShapeMatchingConstraintsBatch_SwapConstraints_m203CA70E418F8DF2BF506ED333695247E286BA2B,
	ObiShapeMatchingConstraintsBatch_Merge_m5A9DEA535310B3863D91C0FBE5A52C051E0357A7,
	ObiShapeMatchingConstraintsBatch_AddToSolver_mA64BACE29C81AC0D914642A2326C5A768BCAD690,
	ObiShapeMatchingConstraintsBatch_RemoveFromSolver_m64F2CB468C81ED844CEE845FC280A5497A18AD33,
	ObiShapeMatchingConstraintsBatch_RecalculateRestShapeMatching_m49C8363C8157E30089548C4BDF8BDA633D26DDB7,
	ObiSkinConstraintsBatch_get_constraintType_mB8336F19B6DBE7777042FA4814EA12115D899223,
	ObiSkinConstraintsBatch_get_implementation_m8EFE8C0407125BE90F4A544FD0BBD6801F606924,
	ObiSkinConstraintsBatch__ctor_m1C9F633F90BE267DDA66B580DED05A7F19BD61C5,
	ObiSkinConstraintsBatch_AddConstraint_m5D3D3C2490F09B0C7B72F763AA56E3AE9D485134,
	ObiSkinConstraintsBatch_Clear_m9760EF8B82970CC9CDE9A840BD9EAF78BD982393,
	ObiSkinConstraintsBatch_GetParticlesInvolved_m77E2085BC3B66F68FE4B9453F0D76A9566718597,
	ObiSkinConstraintsBatch_SwapConstraints_m90010A1D7C64381F5575F0F393B7030C2201DF8F,
	ObiSkinConstraintsBatch_Merge_m5AC677F66016C8CC6DCC768E4A60ECEE633075A6,
	ObiSkinConstraintsBatch_AddToSolver_mF9FF0C39D2BA6C21C642F2F0880004B3D719DC2C,
	ObiSkinConstraintsBatch_RemoveFromSolver_m146DF16BA35D6BBBAE5781504D744C59D61D80FE,
	ObiStretchShearConstraintsBatch_get_constraintType_mC6DEA77B39D02D597E2CA898182A9BD560B545C6,
	ObiStretchShearConstraintsBatch_get_implementation_m0DAC376106BC35C967BE578A40049E3C35E27024,
	ObiStretchShearConstraintsBatch__ctor_m8927550DA897869840F27FAE27254EF2961AEFB7,
	ObiStretchShearConstraintsBatch_AddConstraint_m3EDB080395E5167A460038CE0B00CEEC306C19D5,
	ObiStretchShearConstraintsBatch_Clear_m827C6A5985D286459089DC4635044609BC6B098C,
	ObiStretchShearConstraintsBatch_GetRestLength_mC0368700D80E14C1F758085E1051265DB7E6F140,
	ObiStretchShearConstraintsBatch_SetRestLength_m4401CCB79E8593DE77C2F66191AE967C6DC7E5DC,
	ObiStretchShearConstraintsBatch_GetParticleIndices_mC265643A55371F01BD13A0C3FC34A11D9FFE838C,
	ObiStretchShearConstraintsBatch_GetParticlesInvolved_m304EEFB3092A0112EAE84B4CECF3F6A948D9833E,
	ObiStretchShearConstraintsBatch_SwapConstraints_m2C8677D96612ADAE5BFCC4825A9BA4E42685609E,
	ObiStretchShearConstraintsBatch_Merge_mC487E3A37D51D78F57A1880A27B8168E45F05BB8,
	ObiStretchShearConstraintsBatch_AddToSolver_mA25B51FDA1EBF1E1BE6AD594D45C153216AA8C94,
	ObiStretchShearConstraintsBatch_RemoveFromSolver_m45797BE9F2A7CC2B0BD983E810A313C126621382,
	ObiTetherConstraintsBatch_get_constraintType_m8483A4DC0E6B622F67EC74C1BB113C00981EFA6C,
	ObiTetherConstraintsBatch_get_implementation_mC6C6A9923482CF3A380B6D250E1A63C0EC69E02F,
	ObiTetherConstraintsBatch__ctor_m8866FC2DA610911877A83CE2904D78CACB19FC51,
	ObiTetherConstraintsBatch_AddConstraint_m1BF702B37FD24AD678D201379C614BFA7C24824A,
	ObiTetherConstraintsBatch_Clear_m2F60F212808B2D2566D9486751BA43D287D7A616,
	ObiTetherConstraintsBatch_GetParticlesInvolved_m77FB1AD6E9F76D24F63C6CF2FCA60F8627D497BA,
	ObiTetherConstraintsBatch_SwapConstraints_mE3AD347DF0A9C8FA3D0467F87FFEC0B13FD61E72,
	ObiTetherConstraintsBatch_Merge_m1AD265CC0EE520D80F27DEFBE161FF9E44B80A76,
	ObiTetherConstraintsBatch_AddToSolver_mD57866191E80F88A3D272C0E0AC76364326A80E3,
	ObiTetherConstraintsBatch_RemoveFromSolver_mEE3D5BD7C2CF365225947E773A815095121EF2C6,
	ObiTetherConstraintsBatch_SetParameters_m1577626B4E1C0111CF067DABC451BC0503615466,
	ObiVolumeConstraintsBatch_get_constraintType_m381ED3FD4921DB235D1AE2E0D93D354C840CE86A,
	ObiVolumeConstraintsBatch_get_implementation_mA945BB5C6B7A7EE83AC6C01C6B5DD21207FB0325,
	ObiVolumeConstraintsBatch__ctor_mA44A358BC97BD1830050A02DFFDF5108A6FD9715,
	ObiVolumeConstraintsBatch_AddConstraint_m411C62C1E1117BF0B3B6954C3641F5B418583424,
	ObiVolumeConstraintsBatch_Clear_m53B029F54892E0D5B9D4D2BCB647A4176FE793A0,
	ObiVolumeConstraintsBatch_GetParticlesInvolved_mC38E81ADD236D1D8841141D16BA6989DA3A489F2,
	ObiVolumeConstraintsBatch_SwapConstraints_m42B669FC7CBDFA494210C6486682BD7A3CFFDDB4,
	ObiVolumeConstraintsBatch_Merge_m1DDAB49D1408ADCD12D27C72737072480252DA7B,
	ObiVolumeConstraintsBatch_AddToSolver_m1070A8D58A5B37CC00B5290D1448490F3DD9CE29,
	ObiVolumeConstraintsBatch_RemoveFromSolver_m01AB99DBC04273CEB49EFAAFE595F93D5F05B726,
	ObiVolumeConstraintsBatch_SetParameters_m8B574D29F6430355A0AF20B229AA493E0119AEF5,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	ObiAerodynamicConstraintsData_CreateBatch_mD91F60B676E69FE28DD53AA57D5E64E621F7E489,
	ObiAerodynamicConstraintsData__ctor_mD5BD0D829EC39F1CACD35CF16E040852B4B4784B,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	ObiBendConstraintsData_CreateBatch_mD0A372B63F6C4F88AC48D4E4E9304944D3FEA0F2,
	ObiBendConstraintsData__ctor_mFE69ED43DBE4731FAAAF8C32658B04596342980A,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	ObiBendTwistConstraintsData_CreateBatch_m9A87A7382C1A63A339F0C8B17CF14D1CB03B0D46,
	ObiBendTwistConstraintsData__ctor_mC6E27E26B877C88510C9E27FB9ED17BF94E98B9A,
	NULL,
	NULL,
	NULL,
	NULL,
	ObiChainConstraintsData_CreateBatch_mC2FB00A47639537C6959AE16661DAAF1AD4C3F7B,
	ObiChainConstraintsData__ctor_m9D1C51412A1878F85D8C173777A60EC621A9FFDD,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	ObiDistanceConstraintsData_CreateBatch_mA63574AD8629D5C6907EB15B3FFA9F0D1616D026,
	ObiDistanceConstraintsData__ctor_m10E8E535222BCC55670623BBB7DA36CAF7A2A95E,
	ObiPinConstraintsData_CreateBatch_mC8E21D95D7C5E27097C6B75B20DC74CBDD2D345B,
	ObiPinConstraintsData__ctor_mA86A434A33F8CF37FA8E7E2410BF8223116F03A0,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	ObiShapeMatchingConstraintsData_CreateBatch_mDD0753126D9E04AB8FD0D7F4CB4D6C9ACA8BF359,
	ObiShapeMatchingConstraintsData__ctor_m39A87174E47FB428D6589800F2F18FE1F79D340A,
	ObiSkinConstraintsData_CreateBatch_m0420AD7727A873F46A9E43F8BF1F94895AB14369,
	ObiSkinConstraintsData__ctor_mCB9A63E7B637DEAEB1063E52913985321ABBCAA3,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	ObiStretchShearConstraintsData_CreateBatch_mFBE3B0B12D6BD8E226076AB93E16906F92B60C68,
	ObiStretchShearConstraintsData__ctor_m81576073ED6B14D835150C4666A3588AEDBD29FC,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	ObiTetherConstraintsData_CreateBatch_m28C477BD204B78B77C867473CB959C5340B918D7,
	ObiTetherConstraintsData__ctor_m7488642C0CCCDA9C252605F19F887C961D3BB5C4,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	ObiVolumeConstraintsData_CreateBatch_m1DDE0FAC895B8FC54D095EB773D7AEBDE107A061,
	ObiVolumeConstraintsData__ctor_m09F646B51D450E9B87B194F9E29B8B5FE9C2404B,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	StructuralConstraint_get_restLength_mA86CC2512F19A0314F76C459C7027A50DEB66F57_AdjustorThunk,
	StructuralConstraint_set_restLength_mFDDB0536829F1845FAB3F39B80ED0520431D2BDF_AdjustorThunk,
	StructuralConstraint__ctor_m9F7FF404C8685160AC46543FCE7681E91FF6B322_AdjustorThunk,
	GraphColoring_Colorize_mC1E72C7E56EE6F1F72682546D5CB94934287958C,
	ObiActorBlueprint_add_OnBlueprintGenerate_mA116B88726BA07CDAC7EA40506772CC39F8FF86E,
	ObiActorBlueprint_remove_OnBlueprintGenerate_m780AFE9B203E214D86769A37EADEE63D5FC9967E,
	ObiActorBlueprint_get_particleCount_mCC6F47C83D7F2BE2BBF1667511B5C96BAA5A94B2,
	ObiActorBlueprint_get_activeParticleCount_m22AD50CE827DB1E01A7937D968032E6AF08B228A,
	ObiActorBlueprint_get_usesOrientedParticles_m8E951A7BDC8EEA54633C2AA8BE8E41F01A418233,
	ObiActorBlueprint_get_usesTethers_m7A16422C66E95F3984D2A3A8F950305E9D5C2026,
	ObiActorBlueprint_IsParticleActive_mD079A02C9E95DB8F8900FA0C1FE9A057C1F1D05A,
	ObiActorBlueprint_SwapWithFirstInactiveParticle_mCBD4A74BAC682B6DB6228F3B53FCC5D90B069126,
	ObiActorBlueprint_ActivateParticle_mA772B1D5DA1364AA8A889F069D9F903E565A3F02,
	ObiActorBlueprint_DeactivateParticle_m2EFB2F10CE118C9041593C482C6FC80F24A9C8A7,
	ObiActorBlueprint_get_empty_m5407EABADC0A7CFE5EBBA320717C3B4609F2FDD1,
	ObiActorBlueprint_RecalculateBounds_mC32E8C37E376B8342D46E8894A36161EDF37E48A,
	ObiActorBlueprint_get_bounds_m8F16C4CEAFB92614DEC83B9F41D565B4FE542C15,
	ObiActorBlueprint_GetConstraints_m1C48E3155094EC159203D4F23B25015FCA6F0597,
	ObiActorBlueprint_GetConstraintsByType_mD32BF0C8BE5453BF0F534BCC92F2BD30477E7761,
	ObiActorBlueprint_GetParticleRuntimeIndex_m6495778E4FF39D59DA86401684ABED0361E6803B,
	ObiActorBlueprint_GetParticlePosition_m4B9F2D62EEF90A610C7DC8E1B07D4525ED90A081,
	ObiActorBlueprint_GetParticleOrientation_mE80B9B4333A1EE1670B7DAD73FD55F8C8F6F178D,
	ObiActorBlueprint_GetParticleAnisotropy_mBFB7DD70BF52D6DEC5E08DEFC5A8F07BBFCBE6B9,
	ObiActorBlueprint_GetParticleMaxRadius_m1106A97F11068907833E40301081A52749BC79F4,
	ObiActorBlueprint_GetParticleColor_mA1CA0E89A089B4CEBAFFD4EB83CBFC5EB5A6EB0D,
	ObiActorBlueprint_GenerateImmediate_mD3FF977E24F4965AB776BF1E1DF0272F3D6CCA33,
	ObiActorBlueprint_Generate_m14C8F36BC89354F15F0F82AE9546B5024F47FF7A,
	ObiActorBlueprint_InsertNewParticleGroup_m13FF8726A0D8F315CE56ED27091B83E9FD733F01,
	ObiActorBlueprint_AppendNewParticleGroup_m1FA41729CF6A7830711798726512D51B97355689,
	ObiActorBlueprint_RemoveParticleGroupAt_mE4755BA1B826AA1AA58A63022077FA4B0BB67772,
	ObiActorBlueprint_SetParticleGroupName_m7E56781B15B7C8A3357E58EF2B7C78FBFFF2B78C,
	ObiActorBlueprint_ClearParticleGroups_mBBF558B57D8AC4475F781081F63A6D2FB906D9A5,
	ObiActorBlueprint_IsParticleSharedInConstraint_m9F71E2E0E9B16D9CCB47B7D270391F65232D64A1,
	ObiActorBlueprint_DoesParticleShareConstraints_mCCD929D6E8D050B9B2A377A3965B6F8E64538883,
	ObiActorBlueprint_DeactivateConstraintsWithInactiveParticles_m2F8B411DA9FE9FCDC8116158247E0E83EEA513E1,
	ObiActorBlueprint_ParticlesSwappedInGroups_m9104E8E53B4D76CFFD62EF8277E5313A73867EE5,
	ObiActorBlueprint_RemoveSelectedParticles_m1FD86679B5F6C5DEDED32F79658292F13566C63F,
	ObiActorBlueprint_RestoreRemovedParticles_m6968118C8E330062E76B6F79155E07B86C091400,
	ObiActorBlueprint_GenerateTethers_mE0CECBEF8A33A7E44D7EFD24A87697C45209B668,
	ObiActorBlueprint_ClearTethers_mA11CB91D97A72D5311C67FCC335ACA3900020EEB,
	NULL,
	ObiActorBlueprint__ctor_m9D95F18391AC084CE03CF008E1D47864DF75A636,
	BlueprintCallback__ctor_m75F65AE60505CE5651A0F020168360371AD0D0EE,
	BlueprintCallback_Invoke_m0F0A317733BA923A00DCF82C10E2AF745667377A,
	BlueprintCallback_BeginInvoke_mD8A830F18B9F7B76E81DB436AF1462092202B3F3,
	BlueprintCallback_EndInvoke_m35D1EBBFF55BAC420142369AEB4B24D44D031B3F,
	U3CGetConstraintsU3Ed__50__ctor_m463D73E183DDE1A5C71E6E3CC821D0E7F30E347C,
	U3CGetConstraintsU3Ed__50_System_IDisposable_Dispose_m1C1B68AF4C49065E226E78E8D46FD68A406FC4DB,
	U3CGetConstraintsU3Ed__50_MoveNext_m440BD75AC003FACCE06308279346A5432EDC9BFC,
	U3CGetConstraintsU3Ed__50_System_Collections_Generic_IEnumeratorU3CObi_IObiConstraintsU3E_get_Current_m603F544C865B5A382E4D45071F9EA103E35296E2,
	U3CGetConstraintsU3Ed__50_System_Collections_IEnumerator_Reset_m8E40706068668910D9F7D8A1EACB85F8E6FFF77A,
	U3CGetConstraintsU3Ed__50_System_Collections_IEnumerator_get_Current_mD7F6DAA1BCB1AB519151F38ACED86E62BB422730,
	U3CGetConstraintsU3Ed__50_System_Collections_Generic_IEnumerableU3CObi_IObiConstraintsU3E_GetEnumerator_mFFFD55BE544D6CB567D145A4DCB2564DD48B4CDF,
	U3CGetConstraintsU3Ed__50_System_Collections_IEnumerable_GetEnumerator_m3284B5FE565ECE74AF7F1FBE65BCD30C41CADA8D,
	U3CGenerateU3Ed__59__ctor_mD0933DCC8582C89719A5869AA40F1D96BC46A2A1,
	U3CGenerateU3Ed__59_System_IDisposable_Dispose_m4C2610F3D57B4EB14EE56E2E116536B5A23C8B22,
	U3CGenerateU3Ed__59_MoveNext_m1B248A89761AD00C954EAEB25335090B6378CB9E,
	U3CGenerateU3Ed__59_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mB7A53073B04207358D27C5F279097C3AECBDC48E,
	U3CGenerateU3Ed__59_System_Collections_IEnumerator_Reset_m326C85F4CC091D9CEBD26372E5491D58808FECB6,
	U3CGenerateU3Ed__59_System_Collections_IEnumerator_get_Current_mB78D5553C5C65C889A8A4E2F6FCCADA45472D734,
	ObiMeshBasedActorBlueprint__ctor_m68ACFAD2ACE6D36D748D434E2BD130B742B17BE8,
	ObiParticleGroup_get_blueprint_m7B969093BC47FD5B58C6D6839D8F9E3BDA3119FF,
	ObiParticleGroup_SetSourceBlueprint_m83016ECF67DCB8BDF873C154BB4A0EE7A8091011,
	ObiParticleGroup_get_Count_m98490363FF19DE1FFEBC361D7302AEF05B6F21B4,
	ObiParticleGroup_ContainsParticle_mFF1BABB2B3D4D3CD4AE1E5B44C3DC83765EB11AC,
	ObiParticleGroup__ctor_mD6FDCA405D8CBEB22044CFD3635E3202AF1A45A2,
	ObiBoxShapeTracker2D__ctor_m628217C8133AD0BA22DE8815770C03CD31236C8F,
	ObiBoxShapeTracker2D_UpdateIfNeeded_m7CF0C0561350D4A21F8FC768C7158E4BAE328434,
	ObiCapsuleShapeTracker2D__ctor_m9F0647AFDA77ACDB5B00DDA8F614F2CD3F61F464,
	ObiCapsuleShapeTracker2D_UpdateIfNeeded_m65EF7C477B78DF113F5C43446E266DA9790B333A,
	ObiCircleShapeTracker2D__ctor_m91428FB089519F84073171B1EF2AF27FA7029F13,
	ObiCircleShapeTracker2D_UpdateIfNeeded_m855CD60D865E47BD776A38CE2F44554BD5B69E49,
	ObiEdgeShapeTracker2D__ctor_m9CF5E0F349BC73573202F01E32482578DE1C1A10,
	ObiEdgeShapeTracker2D_UpdateEdgeData_m7208F5B1FF03775D5A425836047FB54E9BB747C1,
	ObiEdgeShapeTracker2D_UpdateIfNeeded_m380635280878C5D86F90898E11CAD1664E3E4BDC,
	ObiEdgeShapeTracker2D_Destroy_m01DDDC54A11D898BAABA7391098DD4D2458CA946,
	ObiBoxShapeTracker__ctor_mE75CAF1F86BB53D6AEF98085C09D17E66DEEF582,
	ObiBoxShapeTracker_UpdateIfNeeded_mF819B8E4C30C6AFAF29A4117798FDF1316EA834A,
	ObiCapsuleShapeTracker__ctor_mBE6839D0297E242539ABACA7E8D10CC7D1FF90BD,
	ObiCapsuleShapeTracker_UpdateIfNeeded_mE44AC47537FB4437697D85609FB76AC306E29C1A,
	ObiCharacterControllerShapeTracker__ctor_m58178DB03B830C4137C4E297650BAB50EFD3625E,
	ObiCharacterControllerShapeTracker_UpdateIfNeeded_m37B806A39DB2EEF9B54A5B5F191B7F9C7602E517,
	ObiDistanceFieldShapeTracker__ctor_mA5B4583A9547EB0FE0C666EBB88E671DEC7F9035,
	ObiDistanceFieldShapeTracker_UpdateDistanceFieldData_mA701C6AE3562FD84B9C2C56A60A80A88D8B961E7,
	ObiDistanceFieldShapeTracker_UpdateIfNeeded_mA4E6477716FF64D462625E6D8F4898A7E47BD384,
	ObiDistanceFieldShapeTracker_Destroy_mF2D78CB9BA87BFF2D01E82CA3796B3E975B387C3,
	ObiMeshShapeTracker__ctor_m08CD689CF8203745944B75624F6F10B9B5991B41,
	ObiMeshShapeTracker_UpdateMeshData_m3FE402BFC2EA1B1AEF97C10345A781D1F1453374,
	ObiMeshShapeTracker_UpdateIfNeeded_m12F1895305D96BA383B3C6D04D0960CDE65BB9DD,
	ObiMeshShapeTracker_Destroy_mF175565DFFD64344104928752D528653AEEF4C1D,
	ObiShapeTracker_Destroy_m0FF375956D9FB5363A7595568F62FC49288D102A,
	NULL,
	ObiShapeTracker__ctor_mD3C85F1D395CDC2FA8F0FADBF02F5035493C1A7C,
	ObiSphereShapeTracker__ctor_m1F91597098DFDE9D3D331BB46F9885EF84816FCC,
	ObiSphereShapeTracker_UpdateIfNeeded_m956A2557E0849CFD2C667CED772595BB3DB2517F,
	ObiTerrainShapeTracker__ctor_m7BEE7A899503A6A8D1B542BB48B2902966AB22B6,
	ObiTerrainShapeTracker_UpdateHeightData_mF979C90B8BF4BAA1D77BB55829060BE1D02DB1FB,
	ObiTerrainShapeTracker_UpdateIfNeeded_mF4287DBBCE367005E4F1EF1342AB50882CDBE460,
	ObiTerrainShapeTracker_Destroy_m778990BC8F8E80F2EAD34786597F7803DA9E55EE,
	ObiCollider_set_sourceCollider_mE74FCFF39705097FEAD7580CD74ADA13071B3CB1,
	ObiCollider_get_sourceCollider_m9128E7F1E524ACFDD350E7347570A6253DFDB19D,
	ObiCollider_set_distanceField_m1E7758FFA96690595BB90B1BEB48B7AD958EB53F,
	ObiCollider_get_distanceField_mC9798980E3932FBA4649A14AB780CA819D06B050,
	ObiCollider_CreateTracker_mD3862A5A96ACE781C43578C71C7F3B34597B72B5,
	ObiCollider_GetUnityCollider_m8F9125F3CE4C77B54A293FCB58A506F9003F1645,
	ObiCollider_FindSourceCollider_m637BA0D9C7416963F07A6C91C46898E63A2AA7E6,
	ObiCollider__ctor_mC40B217031806CE6CC171DB8519F2BDC13237937,
	ObiCollider2D_set_SourceCollider_m1018D5A5D84BEE9BFF223E7C96DFEF6391A61025,
	ObiCollider2D_get_SourceCollider_m0F0FA6FE50C824AA1D508B0624CFC78BC5CB1324,
	ObiCollider2D_CreateTracker_mF85B5C235CC7FD0AA628697696F24E75B0677D95,
	ObiCollider2D_GetUnityCollider_mC7022D7CE3DD5DAFC483FE44489C1826C6F719A9,
	ObiCollider2D_FindSourceCollider_m1C3542EAB959FFB8DD376825B89F1FCF6EF5B71A,
	ObiCollider2D__ctor_m1D8E050EE358DD5E91FBCBC06CEEA010D1096877,
	ObiColliderBase_set_CollisionMaterial_m79AC8EC4C2B24D56D2EEB8E7E3EC8758D71DBCEF,
	ObiColliderBase_get_CollisionMaterial_m0DF69ECB8E1D3B265A93C3411558938452320DB8,
	ObiColliderBase_set_Filter_m7EFACC9D0DDC53860DD8EC7711CA2B537861B143,
	ObiColliderBase_get_Filter_mC113E8E2C6A721BDC36A1380CDA7B1274877FA9F,
	ObiColliderBase_set_Thickness_m8B643679A1B8F3502E0983283BFAFC788F59FED7,
	ObiColliderBase_get_Thickness_m39EFFE2661BFC77996AFE413C11C9842813B0CCD,
	ObiColliderBase_get_Tracker_mC248A17400AE5158EBE13D85059023F2777EF96E,
	ObiColliderBase_get_Handle_mC54EFB3D8F0D65205AECE01C1523F9684610FDF9,
	ObiColliderBase_get_OniCollider_m1A453A14EEE1A71AD174EA6FD490CBF11FB13516,
	ObiColliderBase_get_Rigidbody_mFE29C0B5E72DE47B2F8CECC10D691E3CB1113260,
	NULL,
	NULL,
	NULL,
	ObiColliderBase_CreateRigidbody_m28447722765BD28A1F74FD197E44CD73585CFA55,
	ObiColliderBase_OnTransformParentChanged_mB24B89C5A1D09A5CDE05559DDDE3AE584CB45385,
	ObiColliderBase_AddCollider_mAE172D1DDA35D52A9D36D68E68CB739157F34870,
	ObiColliderBase_RemoveCollider_m95F3C0AD19C848FFF82DF5DB8584D5E949F808DC,
	ObiColliderBase_UpdateIfNeeded_mF9F82931F7D1F7703B77125BB3612E1781EF4622,
	ObiColliderBase_OnEnable_m9D06C6503D3AA87D2ED910B831C6927655449D5C,
	ObiColliderBase_OnDisable_mDCA28DAF071BB2F8AB35E5A290B1935BB47F320F,
	ObiColliderBase__ctor_m0425A5B3CFADDE3908C4CDE7564045D2E49156C6,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	ObiColliderHandle__ctor_m928271CA0B5407C025BBCC1A4C1B41A772842B98,
	ObiCollisionMaterialHandle__ctor_mF9EF7E13E41C1CFA35A66984FEAE0EA7A6E01622,
	ObiRigidbodyHandle__ctor_m7A187DF1938668726DB0353C6DF5A404B9661FDB,
	ObiColliderWorld_GetInstance_mDCD3CE9448DD243AF9314FC3AE6EFF25A0A9D804,
	ObiColliderWorld_Initialize_mA686CF0330B087A58794A38B02A5C474C0ACE80D,
	ObiColliderWorld_Destroy_mB2941E38388F04B017C98D209866BF6234D85C66,
	ObiColliderWorld_DestroyIfUnused_mF7FB902FE93CE094938D57DC0048249F810AFAB0,
	ObiColliderWorld_RegisterImplementation_m04E6D07B4FE9DB89CB602DDE1177100646AB5F80,
	ObiColliderWorld_UnregisterImplementation_m1FB0CC2F87786339836A3FE23662CE7E87AFD313,
	ObiColliderWorld_CreateCollider_m209298226F546EF09F80E9DC3C9CEDB9EA6A3ECF,
	ObiColliderWorld_CreateRigidbody_m1A6A020A8B5E9F2333767813122FAD64F13E58C7,
	ObiColliderWorld_CreateCollisionMaterial_m88809726D6F5777CA5CA979375202E079FBA523F,
	ObiColliderWorld_GetOrCreateTriangleMesh_m68BA906C938414D9FA39EF9434D16EDD5BC6B008,
	ObiColliderWorld_DestroyTriangleMesh_m0BAE4F6D11B77B8B13C2D4C1DE5A6140E76E582C,
	ObiColliderWorld_GetOrCreateEdgeMesh_mD3188273479E4DC678356643F456C0AA97BFBD1B,
	ObiColliderWorld_DestroyEdgeMesh_mC35DD757DCFE047FCE86C3190EEFA0B19A891306,
	ObiColliderWorld_GetOrCreateDistanceField_mD90630455B72C5BAD3E94DAE4F3FE4E979E2C431,
	ObiColliderWorld_DestroyDistanceField_mB645511AF520AEF6AB93B8EE03F0A2A7D62ECEE8,
	ObiColliderWorld_GetOrCreateHeightField_m67768E9620255D9407B6D664A21B5F46A06DA9DD,
	ObiColliderWorld_DestroyHeightField_mEDED727FD4CD60E71555E1F0B7223CF0C2F060D7,
	ObiColliderWorld_DestroyCollider_m834E6E9C211C47C4746C0658D072D11488444C7D,
	ObiColliderWorld_DestroyRigidbody_m06DB0A63D4471A043E38A1BFEC429E693E36E82B,
	ObiColliderWorld_DestroyCollisionMaterial_m95080D33292635B836E68FDF7DBD877FF51A3302,
	ObiColliderWorld_UpdateColliders_mFE5C2781107A2BF876523BC7D1A371930FA231DA,
	ObiColliderWorld_UpdateRigidbodies_m8561FC0652FF0142400D2886A02C9EFAEB685819,
	ObiColliderWorld_UpdateWorld_m864B8D4C74E8309F9FA29064D39B1A37FCC9538D,
	ObiColliderWorld_UpdateRigidbodyVelocities_m98240C979197C4339F335E642B3DF480F7A281E3,
	ObiColliderWorld__ctor_mB72724E3C4F8ABB4A7845EA540DF7FE796E1E971,
	ObiCollisionMaterial_get_handle_m594CB055313A49F8543490E78B1F48E7C21C7462,
	ObiCollisionMaterial_OnEnable_m8FA0F5FEF8BDA6E093C8A903CD09B45F107B87F3,
	ObiCollisionMaterial_OnDisable_m835688D4697C04CFE7B9F09ECBA5E0DD0201EE4E,
	ObiCollisionMaterial_OnValidate_m39E140FEFC698F6790902D5698BCCDAA4C037250,
	ObiCollisionMaterial_UpdateMaterial_m01C9A16AD952A5AC4E018F035CD98AD7C3F7D6F2,
	ObiCollisionMaterial_CreateMaterialIfNeeded_m7B6E786F93918CFCBCAFE79D73C6D39DB7D2E467,
	ObiCollisionMaterial__ctor_m72400798714AEFF19085712777080ABFBB13BAC6,
	ObiDistanceField_get_Initialized_mAABFFF3B58AB387E5CB5FB8C1AB746B0247F983C,
	ObiDistanceField_get_FieldBounds_m9863DC2138A3DBE77E04380C54236345A1133CBD,
	ObiDistanceField_get_EffectiveSampleSize_mB9F3DB29086DB4AD2249B28C1C8C888484833AAD,
	ObiDistanceField_set_InputMesh_mD2BFC4B7BAE4B8EBEFF52AC8E89CF319F03D3780,
	ObiDistanceField_get_InputMesh_m9B9ABB7CAE379468D3FACDBD886581F843FA22B5,
	ObiDistanceField_Reset_m00EC5397C61DF8D42A0E2201EBE65C30AB391DF8,
	ObiDistanceField_Generate_m2FF083D72E253BE27D9DFD07C8816B449F778266,
	ObiDistanceField_GetVolumeTexture_m3C63485A3F2BB3E0D84E496DFD190836DD7C9A64,
	ObiDistanceField__ctor_m79451FE12A75F2ED6B511F1DFC382E6493E09DB2,
	U3CGenerateU3Ed__16__ctor_m926CEA2198C2B31432AD7B0C629E0026FD9135A8,
	U3CGenerateU3Ed__16_System_IDisposable_Dispose_mE8B29A18143A848E1C4C46A4D41CE2EE66999F73,
	U3CGenerateU3Ed__16_MoveNext_mE09C7584F62ED7F884B6289D784C5474BAB6648D,
	U3CGenerateU3Ed__16_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m39320DBD9BADA847C3DC1B06385734720B3ED5D1,
	U3CGenerateU3Ed__16_System_Collections_IEnumerator_Reset_m94171808CA1CAE76B9F0511FE6570131CD857EBB,
	U3CGenerateU3Ed__16_System_Collections_IEnumerator_get_Current_mE403FD60E16DACEE30BE06E73A882BBF823618FC,
	ObiDistanceFieldHandle__ctor_m6E0E72314F9B3CD649F6E52743A19A05BDD53486,
	DistanceFieldHeader__ctor_mD9023657AE562DC6E93CAEBAEA87D8BBFBE3CAC0_AdjustorThunk,
	ObiDistanceFieldContainer__ctor_m3770E97A1E94FE7F79D6C4476A4B5538615321FC,
	ObiDistanceFieldContainer_GetOrCreateDistanceField_mACCA0F4E84E3D53991F18FEC18CCC8230451C913,
	ObiDistanceFieldContainer_DestroyDistanceField_m85914D4DE23293E844CC8082A6B7B4F427C8505B,
	ObiDistanceFieldContainer_Dispose_m7A92A1441B9CB196E1D25BF4587B01FDFF2CA2A8,
	Edge__ctor_m58FEEBE96D412AFED6635E824686137251119088_AdjustorThunk,
	Edge_GetBounds_m3627A8DC0128EA13268432FD42FDFFA5E9E8FC16_AdjustorThunk,
	ObiEdgeMeshHandle__ctor_m055ABDB8025F78423ED6926D1E316EB48FABC4AE,
	EdgeMeshHeader__ctor_mFBC1ECFE2844D2E52FDDEF03C23174C84E12C124_AdjustorThunk,
	ObiEdgeMeshContainer__ctor_m7E7182023FCAB6FD0840A93D3F33CD76868DEC2D,
	ObiEdgeMeshContainer_GetOrCreateEdgeMesh_m08E80D7BB9E4C1AD3040ECCB3E3F80BB0EBC5127,
	ObiEdgeMeshContainer_DestroyEdgeMesh_m6A49B7BD3DACF76A1B0E7F591A523795F55758EC,
	ObiEdgeMeshContainer_Dispose_m1005586E5D7BE477D43008737857278C3088223A,
	U3CU3Ec__cctor_m319B04707AB4F191DD80BB15D8CE099EE0A1A7B4,
	U3CU3Ec__ctor_m0A7F956BDB67E4A8C2BCD9D83C276D02DC561279,
	U3CU3Ec_U3CGetOrCreateEdgeMeshU3Eb__6_0_mDEAC05CDAF4D92AFBB301FED8F1C6099004401DD,
	ObiHeightFieldHandle__ctor_m7487BDAEEC65A9BBFE0A38658CA884E309BC6FEA,
	HeightFieldHeader__ctor_m68533ABFD4D9CB9C9AE92C04D96F69D03704B421_AdjustorThunk,
	ObiHeightFieldContainer__ctor_m5666C4D77395B12D8510DE3C1C91F7DF7080917E,
	ObiHeightFieldContainer_GetOrCreateHeightField_m24A1A0266B0B0558347D317ECAF37AE00CF5F73D,
	ObiHeightFieldContainer_DestroyHeightField_m0C864AEC3E3DF063366196C10432FC0E9582C124,
	ObiHeightFieldContainer_Dispose_m381A36CB411FE4B112A455EB52BEF459C07B542E,
	ObiRigidbody_Awake_mF76BC3EB63FCF9C9203C0490FB685630EA22B0E2,
	ObiRigidbody_UpdateKinematicVelocities_mCB621E407E54750602D501EF243278687D054900,
	ObiRigidbody_UpdateIfNeeded_mE8E08B343D9013FC5168AC526FDD2FD60713C63F,
	ObiRigidbody_UpdateVelocities_m8383481CAC453BA89BF272E3DA3AC19A575ED21F,
	ObiRigidbody__ctor_mD7D0C954158E4444C3068E83B3327DF0C19F22B4,
	ObiRigidbody2D_Awake_mFBE100A1729F05F2F8C0D6F81A5D55A200C18D8F,
	ObiRigidbody2D_UpdateKinematicVelocities_m9FA8BC7541C8841BA05CEFB2E5153BEF8D0F0998,
	ObiRigidbody2D_UpdateIfNeeded_m2B7DDB040171C3260191C90FCA6E67EA1658C21F,
	ObiRigidbody2D_UpdateVelocities_m9E81C9B71FCBB4C7F67940267609771648B6FC02,
	ObiRigidbody2D__ctor_m4B1B8A6B4DC629D021CE278C6D8926801ABF2B5A,
	ObiRigidbodyBase_Awake_m76001FC03150D7505A5D630EA20933DF245BA3C7,
	ObiRigidbodyBase_OnDestroy_mE8BDF21E79746CC2EF9F1CD492C425FE5FD46159,
	NULL,
	NULL,
	ObiRigidbodyBase__ctor_m97C098719F7244A8923BA99EB88178D5BC81E375,
	Triangle__ctor_mF3FB7D0163513605C1284E55D476938CFA8D8B3E_AdjustorThunk,
	Triangle_GetBounds_m96DD7752DB622FBB5D130CC0203D16EF9A16183D_AdjustorThunk,
	ObiTriangleMeshHandle__ctor_m4C8FBE34410F6DE0216D655610C66D865609FD82,
	TriangleMeshHeader__ctor_m05352EA77E9896540763C1FC4DA9A86E8FD110E4_AdjustorThunk,
	ObiTriangleMeshContainer__ctor_m7744CBB6264FB551DA0E62E0AE2B14750075FAFC,
	ObiTriangleMeshContainer_GetOrCreateTriangleMesh_m7966A3DEF57A37FDCD4EAB1254350FCF3C443C54,
	ObiTriangleMeshContainer_DestroyTriangleMesh_mC9C784E4C7DD7154C4F28D56E3D49B0CCFDFA4A8,
	ObiTriangleMeshContainer_Dispose_m5B8C0A01AFA58E44642169B3B0230566C3901B80,
	U3CU3Ec__cctor_m78E18BF63E0FC93B04931361A400A550DF8E42FE,
	U3CU3Ec__ctor_m353F343C4C6BE6C321A97142213FA89DC2CBACA8,
	U3CU3Ec_U3CGetOrCreateTriangleMeshU3Eb__6_0_m55F60D5CC64D104ED8E0B37F968D4479C3124E42,
	ASDF_Build_m0B3E28D5C89253E2A2FAA9545A0B748B5C69E1D4,
	ASDF_Sample_mA68406A166BFA7E95D0C62E769CB30FC8F2C78C0,
	ASDF__ctor_mC6D0B839501A5338ADE298E18D27946D91973849,
	ASDF__cctor_m3BDC04F77E29E81E3A541CFA21851E2153E7EC1A,
	U3CU3Ec__cctor_m02BD313FFAEEDA3D68EE3E46312C75EB44777102,
	U3CU3Ec__ctor_m4CEC5676823706F8C33924A47217BB2E3C584BB8,
	U3CU3Ec_U3CBuildU3Eb__3_0_m2F53BFE53C74A23E70A52AA23A3625EA199232CA,
	U3CBuildU3Ed__3__ctor_m2AF6E46C3AF1DCE8EB0AC87345B0082720891BF3,
	U3CBuildU3Ed__3_System_IDisposable_Dispose_m407D5BEFA6CC050E62B83A69FC1C81FA11062FE4,
	U3CBuildU3Ed__3_MoveNext_m2878CF634F624CD9D2E068EB30A27BFF838B1AE9,
	U3CBuildU3Ed__3_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m3FD71D70EB820213EDF7BC3853561DF86E43D30D,
	U3CBuildU3Ed__3_System_Collections_IEnumerator_Reset_m026B7C1E35013639385A979664E9479DCFB7269A,
	U3CBuildU3Ed__3_System_Collections_IEnumerator_get_Current_m8730288CA28692A77A479C27FB99AFB9421E825B,
	DFNode__ctor_m5AA5A22FD71D7439C9136C2FBC6F151E25C8A609_AdjustorThunk,
	DFNode_Sample_mD228ACC5980D03D69BA1510383BA88AD4253D1D3_AdjustorThunk,
	DFNode_GetNormalizedPos_m48205296253B504FB1F9D3177EADD8E62A70F31B_AdjustorThunk,
	DFNode_GetOctant_m8E57E952AB3E07E8596E051854C4B9EF62575192_AdjustorThunk,
	Aabb_get_center_m6F29EA934C6E7C6A2557550214746BE327737113_AdjustorThunk,
	Aabb_get_size_m316B3CACF5BB496D6CF8532AF06AF826C78486D7_AdjustorThunk,
	Aabb__ctor_m8B1C8639B79DD7C951EF63FD820BB63B8A6B5DB4_AdjustorThunk,
	Aabb__ctor_m293B0520F59E98F3B14FEAFE282685CA663D2E0B_AdjustorThunk,
	Aabb_Encapsulate_mDCE164B716BFA4A7F8147BC997DDDAFF5A0A9F08_AdjustorThunk,
	Aabb_Encapsulate_m4F8536D0A22C6A596F1393D02F97BA7C45B9F9A8_AdjustorThunk,
	Aabb_FromBounds_mB313907D1B69DA3917270DE5C2B39AD5ECCCF664_AdjustorThunk,
	AffineTransform__ctor_m1113EA596451380FE97BF986F98D180B67B50914_AdjustorThunk,
	AffineTransform_FromTransform_mF590DA4EFBCE17F0245A9C33FE65B7405154A7EB_AdjustorThunk,
	BIH_Build_m543AD7EA6BD7D11FCF4DD88B06747F4B797784D9,
	BIH_HoarePartition_m26E2AC749940517A897AF2963BE5322BB38A3CF9,
	BIH_DistanceToSurface_m23810E4AED9E58F613C341BC0C9B6D7FECF9D905,
	BIH_DistanceToSurface_mAE13B1EFAA791602268DFF2A5ED429691EA76178,
	BIH_DistanceToSurface_mCDD4F03C3B60D8D084F6EDB334C195DE89C58B05,
	BIH__ctor_m6D1B7878A01D15CA2622A366286C9308BFB07727,
	BIH_U3CDistanceToSurfaceU3Eg__MinSignedDistanceU7C4_0_mED2F171342075378B5B140C14A73A72EF8ACC45F,
	BIHNode__ctor_mBE805A0C558FDCA2FEDE25366ED721EA3776834A_AdjustorThunk,
	NULL,
	CellSpan__ctor_m93CAC71E3E1DC790B927C8C76F72A3784072746B_AdjustorThunk,
	ColliderRigidbody_FromRigidbody_m4921C838DA8C3C147A91168C9232CB97780260BB_AdjustorThunk,
	ColliderRigidbody_FromRigidbody_m2311F1A25138EA07373D405B7EF2F1EB3859138E_AdjustorThunk,
	CollisionMaterial_FromObiCollisionMaterial_mBF4E29EA56479F76E396B2B5EA9BDEFED6663530_AdjustorThunk,
	ObiNativeAabbList__ctor_m59E302FAD9AEFD8A3BE4E2C51B322FB0420E1E2F,
	ObiNativeAffineTransformList__ctor_mCB63BE2A5D0F613E12C647669D05EB6BE52FC83E,
	ObiNativeBIHNodeList__ctor_m6CB7A19C221489E05D62A7996A07E287439F3D73,
	ObiNativeCellSpanList__ctor_m9810884859E402D959A6369EEBD22320DC40D2D6,
	ObiNativeColliderShapeList__ctor_m79BC575A7059E83119DCE5DFC6D7CE8E308245C3,
	ObiNativeCollisionMaterialList__ctor_m90089B05CB810F2C23D47F0077910F26FCC61888,
	ObiNativeContactShapeList__ctor_mDB12EB79069ADCBD7B0F4377C8C5A1545F646DE8,
	ObiNativeDFNodeList__ctor_mCDC4549E10B99A08FD83058EFD46F45299C71FEC,
	ObiNativeDistanceFieldHeaderList__ctor_mF1E073829DBEAD5BC9ECC60BB9AF6463019282EC,
	ObiNativeEdgeList__ctor_m1DFD5ED07389DD12179C9D3494C4E2BF3D62D563,
	ObiNativeEdgeMeshHeaderList__ctor_m372AE7E0664D6BE77453AB0C52023FC77C720E8C,
	ObiNativeFloatList__ctor_m109DA72958D0EBB1AEF825F9A43C34F2BD1A42E5,
	ObiNativeHeightFieldHeaderList__ctor_m5EC50E82D2C0FE9DBB46618885DE4D9A4FB490A1,
	ObiNativeInt4List__ctor_mF3060DFCCBC09AF993C3C96608B51FA83537EDC8,
	ObiNativeInt4List__ctor_m24F99A45DDF792A152AF6A140C910F32C460E40A,
	ObiNativeIntList__ctor_m25ADBFB84E370EE592FFF05D343541D84462FED6,
	ObiNativeIntPtrList__ctor_mE54F7AA88D3E5B4AD478C37640EDFAD145057FB8,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	ObiNativeMatrix4x4List__ctor_m8315DA2799111EE4624C6AD421E912B9E2C0D775,
	ObiNativeQuaternionList__ctor_mF8798CFC21A168F77FDE7E4E433E4ED07BB42B0B,
	ObiNativeQuaternionList__ctor_m2733D11CD8396D239DBC16995D1E9404EA29AADA,
	ObiNativeQueryResultList__ctor_m5B404B4FBEB82A5A119AC92DFC671C161598266D,
	ObiNativeQueryShapeList__ctor_m1FC70B84E766982F194F6DC2FE87D15DA79493E6,
	ObiNativeRigidbodyList__ctor_m21C628B6ADABDBDFA0D64D291A00D1BBC923B686,
	ObiNativeTriangleList__ctor_m9FD16BE51A73FCC0ECD34FE1E3E60DE410644782,
	ObiNativeTriangleMeshHeaderList__ctor_m4F5742A78ACC6F92F44E484750F2710E45D32A8F,
	ObiNativeVector2List__ctor_m3326D8FF9523937A0D58CCEB0AA53A7F715AB740,
	ObiNativeVector3List__ctor_m454CD1792A828386A51E51AC80EF30B4EC59DB25,
	ObiNativeVector4List__ctor_m4E5196643444C3F0983F4C1D35EC054085549834,
	ObiNativeVector4List_GetVector3_m8EAE7F64879DD9A415D25E05C258ADEBC921A55D,
	ObiNativeVector4List_SetVector3_mEA4A6008125CDE1854000FBF64CEA0887E8A7D94,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	ParticlePair__ctor_mDDAD78C599D557D2A63715207CBC71A5A93581CA_AdjustorThunk,
	ParticlePair_get_Item_m96F244D843B43EE4A0E48128BDAE3837B0574AF9_AdjustorThunk,
	ParticlePair_set_Item_m39EFBD38D2AB94AA009AAF1C18B7EA40077EEDB1_AdjustorThunk,
	QueryShape__ctor_m10B944E68ED9FE30CB3D7EE090068A33F9EDB453_AdjustorThunk,
	SimplexCounts_get_simplexCount_m99631648B641C2ECF96D0C8E5DB265D40F522A86_AdjustorThunk,
	SimplexCounts__ctor_m68F3A4CF7A694C3300E7BA4227020A89A724F066_AdjustorThunk,
	SimplexCounts_GetSimplexStartAndSize_m27E5A762121E379170DD5C8A757EB3124C259B95_AdjustorThunk,
	VInt4__ctor_m6FDFB28D4337C36A4A49681BA2E0B85D0B36AFCB_AdjustorThunk,
	VInt4__ctor_mDF85C8595BC6DCEAAF3A5A4A04704DEFDB238C6C_AdjustorThunk,
	MeshVoxelizer_get_Origin_m98D23A3FAE59358DCED36A586C0AB6B4F3E77B4D,
	MeshVoxelizer__ctor_m2E9885D259222CEE4CB38497584FE8F3EBB0E2B4,
	MeshVoxelizer_GetTriangleBounds_mABBFE555503EEE8CC183A0BAE00F0BDBDECD1CAF,
	MeshVoxelizer_GetPointVoxel_m3AAC4B30F0165C5FB8628549722D2D2014FBE20F,
	MeshVoxelizer_VoxelExists_mDFD49B240D2C297535458EACA6E13AA030D19B80,
	MeshVoxelizer_AppendOverlappingVoxels_m51F870847D3D1B887816F1A7CF7633846F9CF8C9,
	MeshVoxelizer_Voxelize_mCD2507C55D53B7CA373FF8EB2E43B8521336704B,
	MeshVoxelizer_FloodFill_m494C68D66BBCD2F6B85A05DCE56948EE185F7B0B,
	MeshVoxelizer_IsIntersecting_m1EF0FBCCADA0DB46F268E88673A9400D0A6DD7E5,
	MeshVoxelizer_Project_m9D37115C6F24C964F334813BD4D1C978F4EE066B,
	VoxelDistanceField__ctor_mF001EDBFAD1E407A198F85AE578DC94C131DC940,
	VoxelDistanceField_VoxelExists_m7C7078B4A4027B2D5E090BA4A41367F328F2770D,
	VoxelDistanceField_JumpFlood_m1809D2D4CCD13129EE9B88593EC5A9572DD58F3E,
	VoxelDistanceField_JumpFloodPass_m0CAE942EA7B7FB237160C57ABAC04D2F77198AC3,
	ObiDistanceFieldRenderer_Awake_m02202AAABFC3B738E4965E9B2B897A228E6AC210,
	ObiDistanceFieldRenderer_OnEnable_m5A75F9F5EE526E06FF0D410741740FCB6B6478E1,
	ObiDistanceFieldRenderer_OnDisable_m7C13494A2B29D5E6827B2757573C7370FC80A394,
	ObiDistanceFieldRenderer_Cleanup_mFE94B31A3E0EED155DC9650B112587501A88D532,
	ObiDistanceFieldRenderer_ResizeTexture_mE50107B1ECBAB2706311F61EB8EDFAA5A47FB02C,
	ObiDistanceFieldRenderer_CreatePlaneMesh_m7DABB1814E6572C53BCBCC12B382369FA534AAA4,
	ObiDistanceFieldRenderer_RefreshCutawayTexture_mFE87BC7C9D4D36CAE8BA3A2BB5CCFE9195422F93,
	ObiDistanceFieldRenderer_DrawCutawayPlane_m881EB8355EA4D1D21C65297319180645AD0BBB3A,
	ObiDistanceFieldRenderer_OnDrawGizmos_m216C9ED791D6583CFB5AC31CCF96FC999FEB065B,
	ObiDistanceFieldRenderer__ctor_m5BDF67924CA8661E32DCAE76825AECE42C4E08AA,
	ObiInstancedParticleRenderer_OnEnable_mE355745DF976D2E5FB662C641FFF1207CFF849A8,
	ObiInstancedParticleRenderer_OnDisable_mB6727244E6FCBAA7A72470799C34118F47F54C26,
	ObiInstancedParticleRenderer_DrawParticles_m006744E372DC552B4BBE90BDA10C2936E58A7CEB,
	ObiInstancedParticleRenderer__ctor_m36160262281CB03A2EF5F4F65B1599650E20D198,
	ObiInstancedParticleRenderer__cctor_m8F094F8AD40C59C077031DB8223AF95B8123B2B7,
	ObiParticleRenderer_get_ParticleMeshes_m19B21A66F62AF9565B5610A7E406A9F4D9AB2624,
	ObiParticleRenderer_get_ParticleMaterial_mACE9EAEA565D0D3F7A7BF6C53B8C04111AAFB530,
	ObiParticleRenderer_OnEnable_m5936C65AFFDE90197548673B64DCDB559B634084,
	ObiParticleRenderer_OnDisable_mAA99859EE95AC59E3CCEBDAEE3C9F3D1EBE9F2A3,
	ObiParticleRenderer_CreateMaterialIfNeeded_m3F402725EA811E7191F495C02BC1D42C9D3508E2,
	ObiParticleRenderer_DrawParticles_m968118939B2050BAB32AF92EFCB004552F31FCAE,
	ObiParticleRenderer_DrawParticles_m10909ABED462F26F971463C7A569BC19C8115FD8,
	ObiParticleRenderer__ctor_mFC28AF85E8C3264440883850C411477777E11A29,
	ObiParticleRenderer__cctor_m25FE6E62F8C43957CDFA579FE81399B4270C8DDC,
	ParticleAdvector_get_Particles_m779A3049F43CB0E74A8A78144C4BA3C3B21664F7,
	ParticleAdvector_OnEnable_m51E5BC2C9C3DA646BABCDCF5659CF771D3C52FA3,
	ParticleAdvector_OnDisable_m1477C9FCCA953B2731C85BCA4C8D85FF0A752816,
	ParticleAdvector_ReallocateParticles_mCC3C40E6C9410A048F88E31E179EE1EFDA18B099,
	ParticleAdvector_Solver_OnStepEnd_mEB2985B8866986B1A9D814C779A04BB66B05EF63,
	ParticleAdvector__ctor_m55BA4D19DA2791FC95B08F60C50CC3D2E74D0866,
	ParticleImpostorRendering_get_Meshes_m755745FEE84971387953757DE9D15D9D32204C2F,
	ParticleImpostorRendering_Apply_mC4B8B70A312A115D0D3264BA0F4D6CB8920AD9D5,
	ParticleImpostorRendering_ClearMeshes_m425FD6121111FFDD2834713DDE69988571B30BD8,
	ParticleImpostorRendering_UpdateMeshes_m08FF131D2263DFFD11AFC5C78A699C031A1AC02D,
	ParticleImpostorRendering__ctor_m83C3D5BED9E2463E4E916753E01D78C90E7C9BBE,
	ParticleImpostorRendering__cctor_m096389C9C5C4FB998DFF6B43A943B8F29D300B72,
	ShadowmapExposer_Awake_mA4E68783C4BCFD7B7FCCF2FB2047921F739DA519,
	ShadowmapExposer_OnEnable_mA68155C92E4F83954076D9000038D58A265100F5,
	ShadowmapExposer_OnDisable_m7CC85B1D1729AA03CDAC1816F9A84E3350254554,
	ShadowmapExposer_Cleanup_m32D90F4E8CAC5C2EB13BFAD141A056F8D3B143E5,
	ShadowmapExposer_SetupFluidShadowsCommandBuffer_mE799C5800875D6B7CA60B3775768C9F07B2288B4,
	ShadowmapExposer_Update_mBFA72090D0E7D36367BCA31B001E2965D1B264E2,
	ShadowmapExposer__ctor_m10E8C4FAA46FCB5CC04085336279E049BC51FAFA,
	ObiSolver_add_OnCollision_mEB324EABAE3F36C43F055950F52827839851DAF5,
	ObiSolver_remove_OnCollision_mA07AA8AA273F50BC4DDAFD0523A7CA23C628D674,
	ObiSolver_add_OnParticleCollision_m73A739A0544F098FE805CEF2C6ED3E9528060A52,
	ObiSolver_remove_OnParticleCollision_m153C65501BB104D19DFB0BC50CE655A20BB725B7,
	ObiSolver_add_OnUpdateParameters_mDCE3C3436FCB6E239FC3C229799E1CA65048798B,
	ObiSolver_remove_OnUpdateParameters_m1D7467B121B4968CCB10AE69A8FCE3F601A622E1,
	ObiSolver_add_OnPrepareStep_m22DB6F7A692B42DCB0AF33EDDFAF38062A4AB42E,
	ObiSolver_remove_OnPrepareStep_m5D2D4E8CACADB7938868BE6F29546BA497A05250,
	ObiSolver_add_OnBeginStep_mE11B9F3927E6908AC1C8CA09888FAC4145B60F72,
	ObiSolver_remove_OnBeginStep_m828F85A9556BCF73D5434B9A689918DEF5F5D39D,
	ObiSolver_add_OnSubstep_mF4D4FE1ED9E867E6F111B8B082E377D950607C6E,
	ObiSolver_remove_OnSubstep_m60FD7FE897C29FEF9EE0EDC9303C62CFF70D2B33,
	ObiSolver_add_OnEndStep_m85BFAD019C29970E500BB5079DA70527A4EB324D,
	ObiSolver_remove_OnEndStep_m53846A7D3E4D2113D49EAA4EE56FAE9752CB6CA3,
	ObiSolver_add_OnInterpolate_mBADEB5D974C090F97884D9FF885C017D0C36CF7D,
	ObiSolver_remove_OnInterpolate_m5F9AAE96B45E17989172907EECA1966282A87626,
	ObiSolver_get_implementation_m9F3A6A2A2D6921B6A59A82F935039FC29F24E422,
	ObiSolver_get_initialized_m245E2320CEE2189A248BB0DE795CD9FE80839C78,
	ObiSolver_get_simulationBackend_m67AEE52B9C07C78C1231FE5074F7125E47A8109A,
	ObiSolver_set_backendType_mE714E4CD3D72BE093E2ECBB2B418D7095A3B11B4,
	ObiSolver_get_backendType_m49EDD493C5D45A0D0728C26F95F1E955FEFB4104,
	ObiSolver_get_simplexCounts_m78AF46D65457FB56118D4ECD884FEAD71D3E28C4,
	ObiSolver_get_Bounds_m4C8914413BBBC0968BA6ADCF3E7149AA5F7FC7D3,
	ObiSolver_get_IsVisible_mA12ECA2DFE503F721181D2A8A5F5F9065BABAF0B,
	ObiSolver_get_maxScale_m2118E5D74E45C017D6D91E33790768FC10FE9B18,
	ObiSolver_get_allocParticleCount_m98C18E7A785C31F32283F6A52AB1372313E3E16F,
	ObiSolver_get_pointCount_m3D08620D1A6E4D85E71BC101C1D3AE6537462732,
	ObiSolver_get_edgeCount_m73CFBC056AAC931EFB5589CAFB53274B14851EAB,
	ObiSolver_get_triCount_m28DC623922A897A5E53CBB6D3CFBB43A4AB02145,
	ObiSolver_get_contactCount_mAE6F8DD8070340148DCC11C0F729F1B38C4AF654,
	ObiSolver_get_particleContactCount_mFDF3ADAB6B54D7FECF6F1943E4AE93C0AE10F18F,
	ObiSolver_get_particleToActor_mFCEA216986C39AD5264352D73B10017FE5E7F3D3,
	ObiSolver_get_rigidbodyLinearDeltas_mEB6D6F68DEAD8616EA48FA36AEA9B37520CA9367,
	ObiSolver_get_rigidbodyAngularDeltas_m05353966D848B59BC9586B0C19CAE476834502C9,
	ObiSolver_get_colors_m3263CBAEB79EE1F583593969A2EAD32549CB875D,
	ObiSolver_get_cellCoords_m6BB3F63111C716E45AAC8FC0BE4AB0CD17331B28,
	ObiSolver_get_positions_mB7EA5209FDB8289380DBA07A2F28E640D485F557,
	ObiSolver_get_restPositions_m1BCA73DCC44BA1B9181838370F0E546B4D66CFC4,
	ObiSolver_get_prevPositions_mA99888F076C4FB374B14DEE1178C625A3F8F9B00,
	ObiSolver_get_startPositions_m3B9144D015BDB7F0F676F065AAC9A22F0DA9CC27,
	ObiSolver_get_renderablePositions_m56EE654A3E7ECEF99EE3F1481E759B01DAB697BE,
	ObiSolver_get_orientations_mAF1A79C344CC44789900361936635BBD508D2973,
	ObiSolver_get_restOrientations_m5A079EC8140D809FB02223ECCA6986917B333AE4,
	ObiSolver_get_prevOrientations_mBB52AEB5D484B0A4BAE23213D65585BE498B5F68,
	ObiSolver_get_startOrientations_mED3EA83E62A458AE8F95A89CEC15ECA81FDF28D1,
	ObiSolver_get_renderableOrientations_mB71AEBAF05CD2CA3FED8936554686F5A0414C06A,
	ObiSolver_get_velocities_m73051C86B492795133F138E42FF08698222FCCD6,
	ObiSolver_get_angularVelocities_mE565A9561A09435E294755AA2F7E31993BD932DB,
	ObiSolver_get_invMasses_m403082935BEB22CE4C8A086C31B748FC46B8C319,
	ObiSolver_get_invRotationalMasses_m35049317216303EC2D97F143D98DBB60C9EADF58,
	ObiSolver_get_invInertiaTensors_m66E7A977E525B6628FA0D14015A52DE6CC26AADA,
	ObiSolver_get_externalForces_m6DE0779EAA775DB7E7A853B844E07464240BED59,
	ObiSolver_get_externalTorques_m7A057FD836A21C5CA1DAEA2258E05F81D15F8991,
	ObiSolver_get_wind_m72179C4670DBCE8A1838066EA134F02A16DC371C,
	ObiSolver_get_positionDeltas_mA020DD1DBE7DDC042F8C730EBE9A71D6AE213AF9,
	ObiSolver_get_orientationDeltas_m747EE331713678E42AE2A16F9201AF96E8CFAF21,
	ObiSolver_get_positionConstraintCounts_mFBC0F55E2F573CC7A38F71703D8617DF5C9BF895,
	ObiSolver_get_orientationConstraintCounts_m6752E0D0CAA0272BD7FF4242E5A426F4310FAECA,
	ObiSolver_get_collisionMaterials_mE98DA539A9F5EC9BE8B35D9AA8C6F4C4FDA211A1,
	ObiSolver_get_phases_m1DFABF0F6107CA7A301BBC35A0896C69FA1D7422,
	ObiSolver_get_filters_m35DFCECD0FEF501CB8903CF986F67CD3487DFEBB,
	ObiSolver_get_anisotropies_mDD48FB3AC63921409AAF55FB2FED3833FC458009,
	ObiSolver_get_principalRadii_m6C3307DA982056B2AF918D77B539E22580E5F179,
	ObiSolver_get_normals_m241E9FFCE3A531BFF2679A3420EBCE7B313FF52C,
	ObiSolver_get_vorticities_mDC8CC9D454FBD7AABF51AEDD83B63D62D74D2E62,
	ObiSolver_get_fluidData_m70D3FEC3C44F6EA5D68AB6B39BD72A08B41EF349,
	ObiSolver_get_userData_m63E5DF119EE501A9380C6A176079C9FD1361B561,
	ObiSolver_get_smoothingRadii_mEE720BA5CB3140CCB508FC97466DDC06F18A9380,
	ObiSolver_get_buoyancies_m14142F7AA18DAE0CD2BA25EA5F3F7E5796938365,
	ObiSolver_get_restDensities_mA85A3F3E9C265A9E3B01B957691808F072123C96,
	ObiSolver_get_viscosities_mBCDE34198266ABADF85472134F8A69F897BB37D6,
	ObiSolver_get_surfaceTension_m06BBC45CF2C65E927490672F37C89AD69ACE039A,
	ObiSolver_get_vortConfinement_mBCA2B8E737E63A3DADDC34F1E7E7C4D26F7A71D0,
	ObiSolver_get_atmosphericDrag_m81584993AAF86345E489C4D833D1BAF3E704CB16,
	ObiSolver_get_atmosphericPressure_m363045FB67ED30D2413C84317FBF2033FF9BE1B6,
	ObiSolver_get_diffusion_m614EC192C40E1DB26E20E2B7AEEB1E3853E1CBAA,
	ObiSolver_Update_m3432EE148E4943BFEA7F248CC1ADF410EB009532,
	ObiSolver_OnDestroy_m10E800119F5492F06C454965798031E2959B4D91,
	ObiSolver_CreateBackend_m91B701B5D03356B869ECCC5E5B4E0F9E82CA7377,
	ObiSolver_Initialize_m69393E4E890B2AF174E313FAB09AF20C8858713B,
	ObiSolver_Teardown_m7AFA06EFC9E0BA723690DF91837C3E3B8D6C30C1,
	ObiSolver_UpdateBackend_mED822610A9CB49AC6197C9EB12A70EBF7F5959A5,
	ObiSolver_FreeRigidbodyArrays_m75E7122190DD0CFFE6594F39BED2EE9685488FA2,
	ObiSolver_EnsureRigidbodyArraysCapacity_mC0A5656D8985DE06BC6084750EC6AE266B7E059A,
	ObiSolver_FreeParticleArrays_mC803A0F5E3291D91B643E13F40F56A1DD906DB19,
	ObiSolver_EnsureParticleArraysCapacity_mA3FF2925ACDE72EC10DF9853E51FF3440F27EB67,
	ObiSolver_AllocateParticles_m2437F6049C3AB8F791378C25BB220F3D447E9209,
	ObiSolver_FreeParticles_m4CD1BFC3A67A853618E2BC48B666735D57D69CBB,
	ObiSolver_AddActor_m33CCB3774385993AD4756D44613032EDF7602076,
	ObiSolver_RemoveActor_m9816A116E0848361565F850D70B3B8A876ACC5A2,
	ObiSolver_PushSolverParameters_mC566D7A04AABE0FD982060FDDCDF94BF0330A898,
	ObiSolver_GetConstraintParameters_m612C27EA24521064F789D15FABB06E8320EC50DD,
	ObiSolver_GetConstraintsByType_mE11A373E6629FB18A577363D79AE2626806FB095,
	ObiSolver_PushActiveParticles_m699BFA15ECA1837EB8E6FB0C4C06564BC00A4356,
	ObiSolver_PushSimplices_mCF761C06FD3C0023D8AC8CAF847F22856CA29F2B,
	ObiSolver_PushConstraints_m1FE372176ADB5BDDB3C11F042850CA126D62EA7D,
	ObiSolver_UpdateVisibility_m78B2DB3B14730D673C823699F0AF2CEB06524552,
	ObiSolver_InitializeTransformFrame_m37F3F63EE2DD9DDE17329B3FDF750A4C9AFF317C,
	ObiSolver_UpdateTransformFrame_m95D29D02A287E86629B2BF566891B519A5D3B2F4,
	ObiSolver_BeginStep_m08735BDB104DA6C7B894DDCBCE685C8F08EAE8C5,
	ObiSolver_Substep_m8EF42B8FCB4A92B66A18967A5E30C0FAF9D91226,
	ObiSolver_EndStep_m12D95AB19D02AB5C1E2ED1D8F3B1E586831DBA64,
	ObiSolver_Interpolate_mAE7A83C120C8BCD50676E781D540026B6BAEEDE2,
	ObiSolver_SpatialQuery_m6D0D03F289F49B89217E8A449DAABC0CCB90D83B,
	ObiSolver_SpatialQuery_m5C0D958E05B3BE7A66EFA6E320C0CB6E3A196607,
	ObiSolver_Raycast_m8F28F2F638DC71BC4716816A590D3D86E0274409,
	ObiSolver_Raycast_mBABB28101E4819C4AC8C9D3189DB8636FD255007,
	ObiSolver__ctor_mA963ED6A2F8A02E33B2E3BACB178FC2F0436F753,
	ObiSolver__cctor_m4023459B57CD95EEF9656B8BDA9B18BDD558C46B,
	ObiCollisionEventArgs__ctor_m346881EB85AB69845A91659208E75756E4EF6D17,
	ParticleInActor__ctor_mAA089A9B7EBB63E8671D8AAB97FD0836220D7EEA,
	ParticleInActor__ctor_m3829A4666F38432208F2811BDE98CD7A8C6B25F3,
	SolverCallback__ctor_mE4083F1B124E6A1FFB21E69EE2AEE7318B28AAD7,
	SolverCallback_Invoke_m45F6B3AC4BD61E22AAA076A7DBA5544E5C72F057,
	SolverCallback_BeginInvoke_mD89642A3E1F37C4DC118EA6CACD1B6D68CD83A08,
	SolverCallback_EndInvoke_m395BC54EFE0998B85005E9E4F66858F146C931F2,
	SolverStepCallback__ctor_m2079BE6990F358A8A5837AE21480E58DD4D0EEA3,
	SolverStepCallback_Invoke_m68DD32CB29FBD1EC423CC5A72702D2CB05715754,
	SolverStepCallback_BeginInvoke_m434058E191E84C9A47950451E2B46A1F8F957F40,
	SolverStepCallback_EndInvoke_mF3E0E0DE67ABA86472824000ACA507AB3352A533,
	CollisionCallback__ctor_mA5A4EE9E910214B425B42DD40A2671EB949D661E,
	CollisionCallback_Invoke_m941AD708CC00FD8EA47569A5B061BB862B96F98C,
	CollisionCallback_BeginInvoke_m0A88695225D3C02615D754DC211FD3EE3468BFAB,
	CollisionCallback_EndInvoke_m46F561EE559B214E9D38C29ABCADAB3336B18A8A,
	U3CU3Ec__cctor_m89EAB0A60F1D2D1667AB8DC5FFD689526BA4D3DE,
	U3CU3Ec__ctor_m2D8F2915236AB77A7C36A87C7761B5503DCA10F2,
	U3CU3Ec_U3Cget_allocParticleCountU3Eb__143_0_m44CB897A1835DA47864F1E0B7AE082D2592C9FE4,
	ObiFixedUpdater_OnValidate_m5F6FE3B9B8E80E3B125B39F142D2DA57A4DD8BFB,
	ObiFixedUpdater_Awake_m90D52F196706902484B59979C0A7537552FAB320,
	ObiFixedUpdater_OnDisable_mEAE5E8BA5B2A650527D99517C37FBF98173259E9,
	ObiFixedUpdater_FixedUpdate_m9CB26DEA62FD0EA6E7FBF86A2CFB12C375B81CE0,
	ObiFixedUpdater_Update_m485E215A24D5B8A2C3B191F2F65615D4C2A27A90,
	ObiFixedUpdater__ctor_m129BDE7A45025352A7238A317F1A3E7B31A00C9F,
	ObiLateFixedUpdater_OnValidate_m4B24969542BF8BAA45ABF2DD0FC90CD910FA27AC,
	ObiLateFixedUpdater_Awake_mA03AD9976B8FA43BE9FC155469524FBA224C096B,
	ObiLateFixedUpdater_OnEnable_m7AA55EFB2018C0FE7A7450DE8B124E9947735430,
	ObiLateFixedUpdater_OnDisable_m508D776D30650C73FB72E47AEB5F2F7EFB64DFB9,
	ObiLateFixedUpdater_RunLateFixedUpdate_mA207589FB295229010BC3ED39CA0A33B465AE4AB,
	ObiLateFixedUpdater_LateFixedUpdate_mC459F999C033D7BC22CD0A0BDA9569E93C2C6F2A,
	ObiLateFixedUpdater_Update_mE16274CC3FCABACAA0430D79903C2298050907B9,
	ObiLateFixedUpdater__ctor_mA0140EBDF2452F0A415601A43DAE9822CCA249C6,
	U3CRunLateFixedUpdateU3Ed__6__ctor_m3269F746C194951D4FFCB5CFE81A956F7C6913F4,
	U3CRunLateFixedUpdateU3Ed__6_System_IDisposable_Dispose_mFEF635C7933D3DD06A06668B01022B79D7348076,
	U3CRunLateFixedUpdateU3Ed__6_MoveNext_m5E828028CAFAD2A27BAD42F54BD73085E6C7F802,
	U3CRunLateFixedUpdateU3Ed__6_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m1BC60167DF9644F2049A2591D4130F2190A2A4C9,
	U3CRunLateFixedUpdateU3Ed__6_System_Collections_IEnumerator_Reset_m55403139467BF8AF39A859E7823363583E55162B,
	U3CRunLateFixedUpdateU3Ed__6_System_Collections_IEnumerator_get_Current_m8E98B873A2E9B9BE3DEE0CA669677BA2ECDFE0FF,
	ObiLateUpdater_OnValidate_m6360BD327E35B0C56A590D51CB066B811DAD1774,
	ObiLateUpdater_LateUpdate_m04DE9233394AE2CC13CF1DA4F136364E81FE4C06,
	ObiLateUpdater__ctor_mD001E04F8FFF222B20A8ECDBE7A8C2507C2F0894,
	ObiUpdater_BeginStep_m871A82CAEA561457F1FB19B27ADC346661839AA5,
	ObiUpdater_Substep_m44F3765E77796D6D71287FF2883FF2E496E0767F,
	ObiUpdater_EndStep_mD6CEC404A973E002CD4D81FC87442C77F537E55E,
	ObiUpdater_Interpolate_m38784F191B281CBF7A3DE79B71EA71ED2FE60681,
	ObiUpdater__ctor_m3A352531659D7BCCB0CF2AFF0732D3B28AB26092,
	ObiUpdater__cctor_m272383BDBA28D7F89B9965940A9188BDE81EA1A9,
	ChildrenOnly__ctor_m4D3FAAA742E61B7FA2323CE9F448A1877F87CAE0,
	Indent__ctor_m5A092B8D80C878888D885C6ADC0B3A9D3A90E3C7,
	InspectorButtonAttribute_get_ButtonWidth_mE4C1BF731F1203ABCDC7FF911FBC9AA619ECC4FB,
	InspectorButtonAttribute_set_ButtonWidth_mD7A12A99D234B3911E6D035C0224BC6AB4CC773A,
	InspectorButtonAttribute__ctor_m975A49E0C15C48813192407A712BDB21DCC86987,
	InspectorButtonAttribute__cctor_mD9D19B7D28E89349026C4FBCAF129D59060E0349,
	MinMaxAttribute__ctor_mAEC8FD68CEFC58D324E8BBF941E6940654EF2F78,
	MultiDelayed__ctor_mC38D0EAB98C18CE8CCEBCC3767678783367BC0F5,
	MultiPropertyAttribute__ctor_m0CEA425DBF3D690C2FBF18251EA3D45B8A2EEEFD,
	MultiRange__ctor_mB97A29FE677F7549CF13B4C38DEAA868640876C1,
	SerializeProperty_get_PropertyName_mDC8A03791EF5C0FC8DB9BEE2645D05EC456A9B21,
	SerializeProperty_set_PropertyName_m0D097B39C9F982C6DA9CFE395012ED31AE075A55,
	SerializeProperty__ctor_mF607BA112BF60A359F1C32ECA1F8F473737FE5E3,
	VisibleIf_get_MethodName_mA9D547283CA5003031A6052EC2F01BDD20626C79,
	VisibleIf_set_MethodName_mEEE7DE756E758BB291ECC2AF95FB9331577A89BD,
	VisibleIf_get_Negate_m70429C0B2F6E15B7329A21127ED7F095B5B9F79C,
	VisibleIf_set_Negate_m7AE9FD0E091984A71A593C3FE0A0A4CB89CDDF7E,
	VisibleIf__ctor_m74CCE1E688720090402D0E285F11AA7EE004E6C6,
	CoroutineJob_get_Result_mD748A43832803B6F5A24988519355E0B5E960BD3,
	CoroutineJob_get_IsDone_m4278531E6DBB4F3ED38BEB44A7D3365F6008420B,
	CoroutineJob_get_RaisedException_mB7118E9ABE0C6CC7C00D741FC8BC5E859CC81F9A,
	CoroutineJob_Init_m08092162CA93D4B06D6F23B48138F018CA204416,
	CoroutineJob_RunSynchronously_mC0C37C33DF39624CE6811D4D2229BF1DBB004DAB,
	CoroutineJob_Start_m80C8E17CD749B13B237150583A6280C213F8C39F,
	CoroutineJob_Stop_m503EBD4DCD2DB059CA0681D562A66E37442C7E4B,
	CoroutineJob__ctor_m3B1FC813C2F585D8B745A74C9F40059C41C72437,
	ProgressInfo__ctor_m61203218594B2063E536F24871C934C1CA813DA1,
	U3CStartU3Ed__15__ctor_mD7A6880AAEB86FECE339EAA24C276AA5D91FE5AD,
	U3CStartU3Ed__15_System_IDisposable_Dispose_m4542E430FBE633F30A5F1B001DA8C7D01BE636F1,
	U3CStartU3Ed__15_MoveNext_mF349A26AF1025AF7487E1BB1D725340CD887C18F,
	U3CStartU3Ed__15_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m857DB02265865988D8A060D28C87EAB01F6DEC55,
	U3CStartU3Ed__15_System_Collections_IEnumerator_Reset_mC889240009E6BB1F36B3BC74F618A35A604608B3,
	U3CStartU3Ed__15_System_Collections_IEnumerator_get_Current_mE937748101E5BB647A3762B97A9DE2FED38D8283,
	EditorCoroutine_ShowCoroutineProgressBar_mBD8BA0BAB4A8F6DBA32D5EB38A2AC2598992E522,
	EditorCoroutine__ctor_mDA14C1513A94EABA131D822C5BC96C78411A872E,
	ObiAmbientForceZone_ApplyForcesToActor_mAE4A4FC41002FA3D906FEA74194C377D2C60A8D8,
	ObiAmbientForceZone_OnDrawGizmosSelected_m9F4A990AC63D31EFB4E459DDDE79DA6D99ED98DE,
	ObiAmbientForceZone__ctor_mD887D3FBC82BCD8DF7720A9B25BF72572118351E,
	ObiExternalForce_OnEnable_m0FB8D535D2ED4F394E48424E48C073B8E3FBD10F,
	ObiExternalForce_OnDisable_mD1867136686D828057C7DF70B6A77B5DE81E459C,
	ObiExternalForce_Solver_OnStepBegin_mC46587312AE49CD7183DAD9D16BBB66C9BF64E0E,
	ObiExternalForce_GetTurbulence_m36B324BC86245A69B5FFB5D924E08E35BCEC135D,
	NULL,
	ObiExternalForce__ctor_mD434F0876366D23C0D68DAB6A8949E018A42ACBD,
	ObiSphericalForceZone_ApplyForcesToActor_m506A142D9EF2B82CD7310E150EF302F8A6B2FA2B,
	ObiSphericalForceZone_OnDrawGizmosSelected_m428130DB0C2E2D23A31E3D0FFE00EDA0FB6744B2,
	ObiSphericalForceZone__ctor_mCB4320E2FDC045FAC6D25069AA629243434775F0,
	ObiContactEventDispatcher_CompareByRef_mCC27E2711928972C0B16573985711C3432D1719E,
	ObiContactEventDispatcher_Awake_m4C9DC422D39BD21B3BE61C3BE4EB262BD0FECAD9,
	ObiContactEventDispatcher_OnEnable_m9E49028DB875725C353943480CB5C447179953DC,
	ObiContactEventDispatcher_OnDisable_m8F75848B0553FD8776A22ABD1253D891B592CFA0,
	ObiContactEventDispatcher_FilterOutDistantContacts_m2088093F9BFC093F73ECB24BDC9E6DA7A748962B,
	ObiContactEventDispatcher_RemoveDuplicates_m394D908408DBDED555F844001682FD217E94C5F9,
	ObiContactEventDispatcher_InvokeCallbacks_mB588492B596248DBC043C8235D2AD8D3AC3AC423,
	ObiContactEventDispatcher_Solver_OnCollision_m12E8F9442701D7911DE4F0D18CB0F4EC82F263FC,
	ObiContactEventDispatcher__ctor_m357D739552531DA027D8F320CCFAC444814BD791,
	ContactComparer__ctor_m5DC2082E03C9DA3062C2D13D3F1702B3763C4379,
	ContactComparer_Compare_mB97171F79E54851FEF29ACC2ED3F06C5A1452362,
	ContactCallback__ctor_m77F3DD3B53374589F094279143B1638E57919AD3,
	ObiFluidSplatter_Awake_m180B1F61A1609936F14809C864A905D4A4D40F2C,
	ObiFluidSplatter_OnEnable_mBDCEEDC5BED2E9C0F5111461825B0D4B56749FDB,
	ObiFluidSplatter_OnDisable_mDC1FF267D05B6458DB05FA28C8F8B562740F21F7,
	ObiFluidSplatter_Solver_OnCollision_mE276A022CC5F710B72A8ABFD17ED05806105B5A3,
	ObiFluidSplatter_Splat_mA9CB94A6D812366143B19EF2E4598738883D2202,
	ObiFluidSplatter__ctor_mD7383422A06AD16985364BA58C7D725B563842CF,
	ObiFoamGenerator_Awake_mDB72B0B94E32EC83533994FADE8240683A89032E,
	ObiFoamGenerator_Subscribe_mC830ADA3F8CE03DFD5DF0AC5B3CCC7F93E67E96D,
	ObiFoamGenerator_Unsubscribe_m6AC698705FF8B6D1FC8C5ECE8AA1844F9ABA2941,
	ObiFoamGenerator_OnEnable_mED12FEDE21B0EF34DD54C441B017A87CB17E39A5,
	ObiFoamGenerator_OnDisable_mA5011CA8707BD429697E92CA70FB74F9D6E3C2F6,
	ObiFoamGenerator_Emitter_Solver_OnFrameEnd_m544658BBC292FFE97627EB7A4CE20FA9845FBF1D,
	ObiFoamGenerator__ctor_m255CB16E7C25BBAEC9D02B40DE5366CA40063634,
	ObiFoamGenerator_U3CAwakeU3Eb__9_0_mB3999207C0001BBF12FD700855CD29BC88008E6C,
	ObiFoamGenerator_U3CAwakeU3Eb__9_1_m877DE629D12722C15AC8C9490921EDC6D9C479E4,
	ObiParticleAttachment_get_actor_m8A6C711886370813BD7BC301BFB6C7812B6B193A,
	ObiParticleAttachment_get_target_m87F09D825C3B834F3F32869BA35539B4525FA08E,
	ObiParticleAttachment_set_target_m05F5DB1086FA979873CCECE322EDE77C26550DF7,
	ObiParticleAttachment_get_particleGroup_mE66342EC326E748311096EBF6508598B2D0E9CC1,
	ObiParticleAttachment_set_particleGroup_mD2ACDCEDFFDECA0B1737C92F4E2941CD93BEFDE7,
	ObiParticleAttachment_get_isBound_mD1067521C804A334B7A28CA3A9BD65237C0FD29D,
	ObiParticleAttachment_get_attachmentType_m3AE549A02BCF14D72CE6EDCC93541FD03EA3B271,
	ObiParticleAttachment_set_attachmentType_m5631ABD4AF1A1AFB91F5851BF869AD3974C2586F,
	ObiParticleAttachment_get_constrainOrientation_mD07EF64C3AE8F1F9CE99F747CF0D12C2DE7D6E1D,
	ObiParticleAttachment_set_constrainOrientation_m17A07949BE7D8BBF51FCE020DFB2B07DA73A722F,
	ObiParticleAttachment_get_compliance_mF04CA0E1C82403AC5039D8F3816CB750E16E22D9,
	ObiParticleAttachment_set_compliance_m559FB99156912E50F36AE6239AEEF8DCE995A132,
	ObiParticleAttachment_get_breakThreshold_mA89F93A18D03F1DADE699A97634716D15617B02F,
	ObiParticleAttachment_set_breakThreshold_mF3F55D89A7E9E6EEF600D372A34DB537A752EB06,
	ObiParticleAttachment_Awake_m3F7F1CA4DFB4B2C06A8F98ACAA99335C589D62A8,
	ObiParticleAttachment_OnDestroy_m5F5AB114D3BB369BF21F771A3F56EE68D5CF47B7,
	ObiParticleAttachment_OnEnable_mE67B8CA340675DC78907D05D206A0B32B47C8F6E,
	ObiParticleAttachment_OnDisable_m970C2F665946F9526288866F57E00845E57F6231,
	ObiParticleAttachment_OnValidate_m8F4069C273A48281199B02A40AECFF1D83EA0C32,
	ObiParticleAttachment_Actor_OnBlueprintLoaded_m5736115ED7F6C5E1935D2217E7E5DBFDB5B20487,
	ObiParticleAttachment_Actor_OnPrepareStep_m9EA64FC23D3365CA10F37123637E56D333D6A2A8,
	ObiParticleAttachment_Actor_OnEndStep_m09B95B61E9325F33E9DF9B85945979C11962E0C1,
	ObiParticleAttachment_Bind_m41671D060988813057E5D6CB710621B387C3B4C7,
	ObiParticleAttachment_EnableAttachment_mB6BAA968F14899C9E41D3827506F5A05E45247CF,
	ObiParticleAttachment_DisableAttachment_m91236D7F5CFA0439C6EB8D250816CB587AD6392A,
	ObiParticleAttachment_UpdateAttachment_m5537ED01556A4DC62E3F46AB5D43A9E2C875856A,
	ObiParticleAttachment_BreakDynamicAttachment_m3AA55F0956AA144B3AC657224DF303EBACBB9022,
	ObiParticleAttachment__ctor_mAC181D9F03BE52C0DB29C07F539A74C0DEA1744B,
	ObiParticleDragger_OnEnable_m72B8C23953592A731785106A8459284845816A55,
	ObiParticleDragger_OnDisable_mB11CAEAA8554518A0B474976561C8FEF0D6E8E49,
	ObiParticleDragger_FixedUpdate_m9851F3C063BF90687B18E10701B7B1F5193A8E6E,
	ObiParticleDragger_Picker_OnParticleDragged_mECCECAF2424C1D2656A6601ED602BFEA343EBB7C,
	ObiParticleDragger_Picker_OnParticleReleased_mD5AFBB9569AFDD6F33D4E148A5A63B542331C87C,
	ObiParticleDragger__ctor_mC7F5FD7D2B50D344D2D85CDA6CB55EA76DFD7A73,
	ObiParticleGridDebugger_OnEnable_mDCE0CF67BDC98D81F9FB3B4AA3DA73485F24A8E3,
	ObiParticleGridDebugger_OnDisable_m6EEAA88C141739F08763B496873E4FB8583ABA2D,
	ObiParticleGridDebugger_LateUpdate_m7A114715F2660DE5C8004F414580ABAB4B6BD796,
	ObiParticleGridDebugger_OnDrawGizmos_mFF5DD489989676AD3C6BACA195FE47F4E18C22AC,
	ObiParticleGridDebugger__ctor_mC3547171B3F6C6C08F8E759186D7007E186A29B8,
	ObiParticlePicker_Awake_m74E07553D81756450A0D1B8351E1CBF8142F8F11,
	ObiParticlePicker_LateUpdate_m9E75050EBAC6B52C81851303DEE0B2EA70353CF1,
	ObiParticlePicker__ctor_m5818ABF9055AD22D294ED1EB13021BC9FE8AB3A9,
	ParticlePickEventArgs__ctor_m64293DD03DECF493CE8EBD7844C21D2746C09EF0,
	ParticlePickUnityEvent__ctor_m8E79C656B1CDE3FB655B899990D215C91879BFA0,
	ObiProfiler_Awake_mAB7F0E70026EC600D00A9130AEA40B9F2DD4D0D7,
	ObiProfiler_OnDestroy_m952E74CFF016FB1DB495D76228352ABED2E56166,
	ObiProfiler_OnEnable_m2FA73A238837F2034B8E2907F2E87C347A07EFDC,
	ObiProfiler_OnDisable_m677BD3207054B10CDE9BAFA6EDC6D7C52DAD7B09,
	ObiProfiler_EnableProfiler_mE07D4D3052ADC88FA8D0D882DF933E094D9F25FD,
	ObiProfiler_DisableProfiler_m6E225ACE957C1D38336D2C4FFEEBAC52540241E9,
	ObiProfiler_BeginSample_mCA17FA232FD590BF488C6304FC5C4B4B77105DEC,
	ObiProfiler_EndSample_mA76E83B9E7BB9DCEA7626780607D999D34868425,
	ObiProfiler_UpdateProfilerInfo_m89B2E0A641CDEA86AB5195C8CB8CDE7ED666B9DF,
	ObiProfiler_OnGUI_m975F6EE95578505D3E6F1792AD1AFBBAD1375139,
	ObiProfiler__ctor_m3EEB8D1C03DFE08C0EC0E23E334C190F41E40424,
	ObiStitcher_set_Actor1_mABB05BE8A26AE51871FE07C7EAA9D53359016679,
	ObiStitcher_get_Actor1_mF7B8097B74CDE0D5491AA8CA0BFB9EA6F6521DE7,
	ObiStitcher_set_Actor2_m8D4021102EDF55C787F2AFB5CBD83AEA4443C95E,
	ObiStitcher_get_Actor2_mF6FCD44A6E57935EF04782FE6E80FF30A500356E,
	ObiStitcher_get_StitchCount_m0A3C3380AA1B0536814453807186D85002F57744,
	ObiStitcher_get_Stitches_mD04C880005B8B5F6261CFF2E5ABB4C713C27CDFA,
	ObiStitcher_RegisterActor_m6CF833604BDC8434EA68BD461456A891CCE1A6B6,
	ObiStitcher_UnregisterActor_m1C06E9E8003BEAEF75F59ED25B8B56561E5D4A4C,
	ObiStitcher_OnEnable_mDEE87492FEFF02F28EC6D8CB231D8DA3893A8BBC,
	ObiStitcher_OnDisable_m44319848A4EA3CB9AF55AE503389703F57250C00,
	ObiStitcher_AddStitch_m9CDD6C005F4A71C8E942AFCF0EE588AC47CBA1D7,
	ObiStitcher_RemoveStitch_m050637591BDE1882809E3902CD2E7C0FF62EB0CC,
	ObiStitcher_Clear_mC2BBA753DCB0899C7C486C20565D05F6ED1F1B2F,
	ObiStitcher_Actor_OnBlueprintUnloaded_m6CE4420D85CBD27371B1BCD548FC50F983C7DDC6,
	ObiStitcher_Actor_OnBlueprintLoaded_mF598296F75D17D0F818E4D7F7A1E98F7F3B8200A,
	ObiStitcher_AddToSolver_mACC6329F09C6AC34703CC2C3CDCD6531D7ADCA63,
	ObiStitcher_RemoveFromSolver_m5B3C5E8739E3393D3654A34D0D84972D7582C457,
	ObiStitcher_PushDataToSolver_mA3B052D96FECDB53E4616B27E72A44F77A88EB31,
	ObiStitcher__ctor_m8703E582D33DF3E6FB4265F91A5061420D984F97,
	Stitch__ctor_mA9EBE5BCBF80CAFB90725EF8F4B9E8EA693A9800,
	ObiUtils_DrawArrowGizmo_mB0F16371F7CA701F1934AB1D68E2C543BBEDC6D7,
	ObiUtils_DebugDrawCross_m56B5756A7B53EF07A54DA1521D785F4B447A0794,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	ObiUtils_AreValid_m36409B4F74E4DA83A742954433F6CA4EE168BAE6,
	ObiUtils_Transform_m733A8AC89C2BFBC7A0E0089512FFC4709454DE88,
	ObiUtils_CountTrailingZeroes_mD04646F64AE816B0BBE5FE3044C2DED6E90A0333,
	ObiUtils_Add_m33CB21C40D66A37C46501B8D0F17D03411F7A6B8,
	ObiUtils_Remap_m8F506FF64E3B9168DE2B0235E274CC1033B7D86F,
	ObiUtils_Mod_mD2EF1D694D52B0B1882C801A2FD576D8FC49D90E,
	ObiUtils_Add_mC0C822C7DBBF1BA01EE882208292D519B2C9C2D4,
	ObiUtils_FrobeniusNorm_m864D4DA65931429E717193CB634FFAAF31C5DCF4,
	ObiUtils_ScalarMultiply_mE25A02716B8B63129015ACA756D1DB1AD469DE51,
	ObiUtils_ProjectPointLine_m39534F2B61F19A53C004F9F72C2D5561C3BFCA15,
	ObiUtils_LinePlaneIntersection_mA331CE0E861BFF21738B92A85B6009EA4641AD91,
	ObiUtils_RaySphereIntersection_m878269850BD66F1202937DEFF22C3A31ED3B5996,
	ObiUtils_InvMassToMass_m1ECAAAEDD4E27ADA1FDE1594DACCD249742ED136,
	ObiUtils_MassToInvMass_m7E1782BA36AD544C22BE9B80A3D222F6D4000B68,
	ObiUtils_PureSign_m92E94D198EF88DC390CCBA619718D6A6C27BA5C5,
	ObiUtils_NearestPointOnTri_m321A69E9A25CCB719C7215829C81D8D8CBF382ED,
	ObiUtils_TriangleArea_m560C3409898B4D67699FCFD0338C3ECDB244BCEB,
	ObiUtils_EllipsoidVolume_m406A1FFC6FADF93B25B2B072064B345E5DA5D2C5,
	ObiUtils_RestDarboux_mA5FBE38C5DA10FBD3F920E2F0E96683D3EF91507,
	ObiUtils_RestBendingConstraint_mA2A2E588C9B5B6907DC2670CF3176A0441D95C78,
	ObiUtils_BilateralInterleaved_m17A79FDAFD53A3C8E7CB5403AC539CD1BC4D5453,
	ObiUtils_BarycentricCoordinates_m3A02D87AC39EA5DB7EC627E892E9F1C3B8801197,
	ObiUtils_BarycentricInterpolation_m3D92C8376763EA18C44BA152EB2C0E19EB7CD4A1,
	ObiUtils_BarycentricInterpolation_mAC20CA1234A3FB1D1D9AB2AA29D7987C0DF12361,
	ObiUtils_BarycentricExtrapolationScale_m84AD9B6B2173A5538FDB174D72AA5CE127045D33,
	ObiUtils_CalculateAngleWeightedNormals_m97CD00C800AA1BD2705A48113DD44B57E450DD34,
	ObiUtils_MakePhase_mED5844CB11913FC70EC9041F512C75E0B2F4E167,
	ObiUtils_GetGroupFromPhase_m95E80301165EB08A322A5043278D698903867054,
	ObiUtils_GetFlagsFromPhase_m6712C3AFEE4371CD66C9A5548C96FEF64A990A57,
	ObiUtils_MakeFilter_m8370C56CCFA7758D6E03671DCC2E8F0546BACEF3,
	ObiUtils_GetCategoryFromFilter_mE5495C835432A19EE7A32B4E72D104A5179AD9ED,
	ObiUtils_GetMaskFromFilter_mD6E7EF064AF96C8238D05A7EB5504E72CA1ECFCD,
	ObiUtils_EigenSolve_m52D85DC48A8905EEFA96A9E0618B6569757370F7,
	ObiUtils_unitOrthogonal_m08E2004A970D966D5227E325780C23E6FAB28529,
	ObiUtils_EigenVector_m650F505BE39681115F5A78F3AB4C76D6B766E360,
	ObiUtils_EigenValues_m4B78523278F5C36F028CC1E7AB8FA13BE10FB476,
	ObiUtils_GetPointCloudCentroid_m8C1020D867B29A5A516F0DF741330765D4B47BB5,
	ObiUtils_GetPointCloudAnisotropy_m3D48980B4381A7B54F37C063FE47EF7C737CA9A2,
	ObiUtils__cctor_mE9D4CD7AA4BC90F980D0AC4F4DA0B543DFD75D01,
	U3CBilateralInterleavedU3Ed__38__ctor_m19BC8267EC16344E9F475D2D8F4155172B85C4D2,
	U3CBilateralInterleavedU3Ed__38_System_IDisposable_Dispose_m0882D85F88A623231272D83F0D0DA178DCEF01BA,
	U3CBilateralInterleavedU3Ed__38_MoveNext_m6AAB9119339072DCC1E88707B3CE2C74EC73CA9D,
	U3CBilateralInterleavedU3Ed__38_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mE8BF0E22888DF4C74A1A58C2C0CFC10DE467BA08,
	U3CBilateralInterleavedU3Ed__38_System_Collections_IEnumerator_Reset_mC549AFDEE04DEDBB58A64225A8EECED92476FDFF,
	U3CBilateralInterleavedU3Ed__38_System_Collections_IEnumerator_get_Current_mBD38911DD74EDEF3DDD7E26C4B713A64A2F46DF3,
	U3CBilateralInterleavedU3Ed__38_System_Collections_Generic_IEnumerableU3CSystem_ObjectU3E_GetEnumerator_mBB9EE9A4785E405D7DE91A4DD014CE7B467A9A56,
	U3CBilateralInterleavedU3Ed__38_System_Collections_IEnumerable_GetEnumerator_mC092BD1AE35CAE1A4FB3644AE120E31AA501A148,
	ObiVectorMath_Cross_m23CD1AEE1CC6B74D7930C98AA1D280513238A553,
	ObiVectorMath_Cross_mA47A8583DC381295371BD66F8397DD209C2B7731,
	ObiVectorMath_Cross_m4C1FC1059C8BF006437BAA9D3CBBCD7323016E19,
	ObiVectorMath_Subtract_mD703C48EC9EAAAF9901BCB1F1D0BF46E23A8925A,
	ObiVectorMath_BarycentricInterpolation_m7CD9E0E0E11533DEB84C7003A1FBC5E415B6CAD4,
	SetCategory_Awake_mC91A0F14152A3306C3D58FFDB3268FC42CA75A1B,
	SetCategory_OnDestroy_m7E095279FCAA7AC75B26DBC31335E8C1B6156A5F,
	SetCategory_OnValidate_m96641FEA78031AF96A7904C7B89F5526989E129A,
	SetCategory_OnLoad_m105F18C77751C59F67759FE5006D36A29B4480AF,
	SetCategory__ctor_m5F2C7ADC7EAEF4C3E7B89A74858C43C67F3F9AD5,
	ObiEmitter_add_OnEmitParticle_m8491547E27FD28676D80322B77B456F0C63CAD15,
	ObiEmitter_remove_OnEmitParticle_m37801BDD62149B5462BEE2401FE604F9958FD475,
	ObiEmitter_add_OnKillParticle_m072CE84C1A40E144AAAE8799AFA1375DD3EE51A6,
	ObiEmitter_remove_OnKillParticle_m25E9F562BEC40314A39FB0539A099C60BEE0E03E,
	ObiEmitter_get_sourceBlueprint_m26D4695B2546A33A98C940075D884E7BCABB9C21,
	ObiEmitter_set_Filter_m9D0B5EB8094CAB07DA4A4E54DD47B619B1C64DB2,
	ObiEmitter_get_Filter_m2B0C29BC4AAE23B8F8BCC6F5E0AB42FF6CB18688,
	ObiEmitter_get_isEmitting_m2E167BA71486E50EA1FA553928089078FF2CCDC2,
	ObiEmitter_get_surfaceCollisions_m7948BECE7563A00F333CDD1E17A340DFDB508548,
	ObiEmitter_set_surfaceCollisions_m4214AB7FF3E3B1573122F95846D3EBD206C9F70E,
	ObiEmitter_get_usesCustomExternalForces_m7DD026E3D65624A9202152C5798EFB87976DB787,
	ObiEmitter_get_usesAnisotropicParticles_mC0661B5AFC22D80BE3C61E7D5AFE806F5D9B02F2,
	ObiEmitter_LoadBlueprint_mED89098C507E400D06A15AA61ACBC53ED585A18D,
	ObiEmitter_OnValidate_m57F92C20FFB8E298BD6BE40714D94BE3B46070FC,
	ObiEmitter_AddShape_m9BF07C5236F3F8117EFBD7384949CB95038A68BC,
	ObiEmitter_RemoveShape_mD5BF4FBDB20CBF6CEDFEF6BA1AE6D10B2CE11286,
	ObiEmitter_UpdateEmitterDistribution_mFB97BE307BCDC399CB8D5F846E83E7AF0565D9AF,
	ObiEmitter_GetDistributionEnumerator_m7A7448549FB95DA20C722E386EC54A6C6457CAE0,
	ObiEmitter_UpdateParticleMaterial_m517E76333581737EEF86813987E4153F385DF6EC,
	ObiEmitter_SetSelfCollisions_mADD790DDF745EC730B238E53811B1BA769BD8ACE,
	ObiEmitter_UpdateFilter_m72F9D1ADB52414CB1EDF57DC683C23A550F087B0,
	ObiEmitter_UpdateParticleResolution_m6CD9FD98DEE0C8E93C71890A1DBB703AC094D644,
	ObiEmitter_UpdateParticleMaterial_m4B7AA6AA73521B582CDDAE4F515609EB0390B930,
	ObiEmitter_SwapWithFirstInactiveParticle_m1F1A1EB2283E25A96C6BDA0C1D0161AE80185E43,
	ObiEmitter_ResetParticle_mEB6C06CEAEDCF61DCDA75BEFC8EF573CBF5586CD,
	ObiEmitter_EmitParticle_mFE11259A7521888CF3A0E65D9125A8F0C5327D92,
	ObiEmitter_KillParticle_m6734F9974FDEA10F0BA301736753003913ACA3F0,
	ObiEmitter_KillAll_m8BF9564C90A434B223579225A3E6B3AD6E662FC4,
	ObiEmitter_GetDistributionPointsCount_m5E8C1423287A0704A40FA47A7AB41C37E0950674,
	ObiEmitter_BeginStep_m665B3103CE41C6C937F85F73D528F8FDC0116F67,
	ObiEmitter__ctor_m1E29FB9EC76CA6E43CDE2741DEDA938D7BD20185,
	EmitterParticleCallback__ctor_m9C4DBA29CFCF9F31E0D9EBB78450F63F21DAC8EB,
	EmitterParticleCallback_Invoke_mA9C7D02905E6C6D1B3BDFE6337130668330BB2D9,
	EmitterParticleCallback_BeginInvoke_mF10FA008B7EE75DE9287B041AF6EF5468246456B,
	EmitterParticleCallback_EndInvoke_m2241BFB0C7805D824FB2FC8BBAE6CAFF2E44C7C8,
	U3CGetDistributionEnumeratorU3Ed__40__ctor_m449587DF3040BED65DA885F0CB975BAF5648B862,
	U3CGetDistributionEnumeratorU3Ed__40_System_IDisposable_Dispose_mB66E839E2203FA3D371BE99CF5E206ED314BB16F,
	U3CGetDistributionEnumeratorU3Ed__40_MoveNext_mAA03394FE695F1B8CFC92E643C69BDF17C3CEDFA,
	U3CGetDistributionEnumeratorU3Ed__40_System_Collections_Generic_IEnumeratorU3CObi_ObiEmitterShape_DistributionPointU3E_get_Current_m099460B8F655B695ECCDDA49652656A00D9E4DAA,
	U3CGetDistributionEnumeratorU3Ed__40_System_Collections_IEnumerator_Reset_m11ED40F759FA3014EB42EC36A4812A9AA6784C12,
	U3CGetDistributionEnumeratorU3Ed__40_System_Collections_IEnumerator_get_Current_mA5947F59DA4154A7983A004718D35B54846233BC,
	ObiEmitterBlueprintBase_GetParticleSize_m171C2A62E702E4266BB8068AA7B9C8C0C7E6006C,
	ObiEmitterBlueprintBase_GetParticleMass_m5315467E58807831A1582F8C68CFCB2BF9E9BDF4,
	ObiEmitterBlueprintBase_Initialize_mC055F9C9FAA49BD2835667B062902B987C9FF68D,
	ObiEmitterBlueprintBase__ctor_mFFA6CC80C85F17DBBA55896FAF790F27E4DCB4F8,
	U3CInitializeU3Ed__5__ctor_m438B4BA7AD1B7ABF3B194B7211DACD1B6CAF513A,
	U3CInitializeU3Ed__5_System_IDisposable_Dispose_mADA4B1631A55EEC9DE2EF0096136DFC72E91BD37,
	U3CInitializeU3Ed__5_MoveNext_mDDDC37540F8037B1D2B1D7E8EE83A51BD8F599F5,
	U3CInitializeU3Ed__5_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mFD5E2A626D669E05BD156AB6C47B08812732EB0F,
	U3CInitializeU3Ed__5_System_Collections_IEnumerator_Reset_m924E660EAF2B642353D8C885A7DEE6A273E90FF9,
	U3CInitializeU3Ed__5_System_Collections_IEnumerator_get_Current_m49C637AFA064F7F5A9498FFD167D9D4643B275B9,
	ObiFluidEmitterBlueprint_OnValidate_m492FA6F5DFC1E38BE273FCDA58079568D42DB10F,
	ObiFluidEmitterBlueprint_GetSmoothingRadius_m60BE1C679492A1E85F360D049C654A999C3A2B90,
	ObiFluidEmitterBlueprint__ctor_mAB848A5DCED957321A0F778171622D710B441D82,
	ObiGranularEmitterBlueprint_OnValidate_mDCD55411AFDD5188709D2335212D8A97667D9AB8,
	ObiGranularEmitterBlueprint__ctor_mD506D7406ECAEFE7FCB31261B6A9F66FF6B05F61,
	ObiEmitterShape_set_Emitter_m738B65FF67C0C5F293362EC39E2FF44AF9B4E06C,
	ObiEmitterShape_get_Emitter_mFAE7391AF31C09A3F8D91BD51C0CAEBC3AF32E0F,
	ObiEmitterShape_get_ShapeLocalToSolverMatrix_mC212FFE89C25BE3398276B4200A6F3315286207F,
	ObiEmitterShape_OnEnable_m19B742103C8F1AFEE0306BE076869064457B4CE0,
	ObiEmitterShape_OnDisable_m0DB579AD465ECCCB9498A5DF9564F71822A9A5C7,
	ObiEmitterShape_UpdateLocalToSolverMatrix_m2BC8BCA9E1B90BAB87298EFB9DE2528CAA010116,
	NULL,
	ObiEmitterShape__ctor_mCF574C14DDE13454E3366335DF34EB8837B2FC72,
	DistributionPoint__ctor_m558DC84A6B0F7A8C37E8894D08E1A759B3B09E13_AdjustorThunk,
	DistributionPoint__ctor_m7CD39DE09F96C6280D631352FFB3E38DC8C7EBCC_AdjustorThunk,
	DistributionPoint_GetTransformed_m4231D7EA07AB8B71E35E91C62C3F37F17B930F27_AdjustorThunk,
	ObiEmitterShapeCube_OnValidate_m1458ACED6D98E6EB8F6E2E0EFAF084B9E865E4B6,
	ObiEmitterShapeCube_GenerateDistribution_m4C3072C14BCCB8A1C96C57BCAE69E459BB0C3BD9,
	ObiEmitterShapeCube__ctor_m4307FA9A97458348EEEF8C9EFFA9BDEEEA0D0890,
	ObiEmitterShapeDisk_OnValidate_m92DDC81192E354D15DCE6317298D02FCE0A5BA37,
	ObiEmitterShapeDisk_GenerateDistribution_m9BFB838DD7FE448E91A409C3E998436F47D175FA,
	ObiEmitterShapeDisk__ctor_mD88D3F0C289E9B7BD2333BB4269D012B2A01F40A,
	ObiEmitterShapeEdge_OnValidate_m8687BA352FB579F0928C80D690C262F5C7BAAD73,
	ObiEmitterShapeEdge_GenerateDistribution_m5C9BF94C7AA8CCE15A49F2B14385BB1829E66A53,
	ObiEmitterShapeEdge__ctor_mFDD80D7B2CCAE5C7DC190F58171805071E63185F,
	ObiEmitterShapeImage_GenerateDistribution_mC3F74D5440F38B83BC1D13415A0A876F9A22183F,
	ObiEmitterShapeImage_GetWorldSpaceEmitterSize_mF397EF0461A96C8A6DE60C38D7C8AAB107655548,
	ObiEmitterShapeImage__ctor_m67C9A11F0A682BA20064F26D53DDB8BDF8F44D92,
	ObiEmitterShapeMesh_GenerateDistribution_m1F4D55DF41C92CDBA0256F6D25822F38ABBAB911,
	ObiEmitterShapeMesh__ctor_mE1E90478BFC0388C8A751CA3B31BCD6A0723BD0D,
	ObiEmitterShapeSphere_OnValidate_mB9AE74B99D2643B70612081FE225FEB7C508CA53,
	ObiEmitterShapeSphere_GenerateDistribution_m3AF0847B95050CA48104F579A075CEC2B9DBA09A,
	ObiEmitterShapeSphere__ctor_m072D019F93BE50415E75B13371E45A5E8C0F2C82,
	ObiEmitterShapeSquare_GenerateDistribution_m25B2D91883D1A366DCAE4B2E169E7E079EC08A72,
	ObiEmitterShapeSquare__ctor_m6A2CB35F2453CCE9C2C62C54810120FF99AA8EF2,
	FluidRenderingUtils_CreateMaterial_mE2F2958ECC414D242F52450D32B5FDBA6413EC7B,
	FluidRenderingUtils_SetupFluidCamera_m4C8B96EE9F29DD22ED24AE70D6919704AB882ACE,
	FluidRenderingUtils_SurfaceReconstruction_m12923003A21DF48D20B7D785CA33448CC83FF4CE,
	FluidRenderingUtils_VolumeReconstruction_m6F3E2956C90F0A927EBEEED56F050D2EF8B655B7,
	FluidRenderingUtils_Foam_mA2AA55FA9A3A2B571B27438565088A32A27EA0EB,
	FluidRenderingUtils_Refraction_m0C0ED2E17F2BE32ED84C45894BB9042D41317C0D,
	FluidRenderingUtils__cctor_m92C53BB8B58AE98522664EBB9DCA3C49C1269DDD,
	FluidRendererSettings__ctor_m9FD316F71F74710EFBE27C0EA32FCCC7066ACD77,
	ObiBaseFluidRenderer_Awake_m858EF05E711487D19C53F023D2D4B103C799B190,
	ObiBaseFluidRenderer_OnEnable_m046808A62B1B6D06CDC3A696BDFA2A736941407A,
	ObiBaseFluidRenderer_OnDisable_mB900AAE985EF73F6B9717F9F6E5671B1D0CA9B4E,
	ObiBaseFluidRenderer_CreateMaterial_mD1FBCC71591C2EEE677330E7632884EDFE70100A,
	ObiBaseFluidRenderer_Setup_mC196BA1C57308A9E0921ACB33194B16DBCE4D389,
	ObiBaseFluidRenderer_Cleanup_mCDB11B875A93046A352E255B834E84287C0BF570,
	NULL,
	ObiBaseFluidRenderer_DestroyCommandBuffer_mDA1CD19796D16750C6D274EC32F3C9B8D58ADD63,
	ObiBaseFluidRenderer_OnPreRender_m262AE226F352EE9A5338670F4CFC3F77FE6E36C7,
	ObiBaseFluidRenderer__ctor_mDD76E1D3E3C376F0D2DEC27CDD5121EE7AB9B845,
	ObiFluidRenderer_Setup_mC65951B017D727A5AA07DB94EE3A2CE5D09BC389,
	ObiFluidRenderer_Cleanup_mE554CB09BB193BCE63198D44A1BC2C9E0133FDC1,
	ObiFluidRenderer_UpdateFluidRenderingCommandBuffer_mCB5FDDEAAEB29CD6F9A856565B78E2286AD21C6D,
	ObiFluidRenderer__ctor_mE66384FDCC40AEF4BBCD474C35B16463CF08FE5D,
};
static const int32_t s_InvokerIndices[1657] = 
{
	23,
	23,
	102,
	23,
	23,
	23,
	27,
	23,
	414,
	23,
	23,
	23,
	23,
	1417,
	103,
	104,
	887,
	1418,
	1156,
	1419,
	25,
	25,
	1420,
	1420,
	1421,
	1421,
	527,
	25,
	1422,
	1423,
	1424,
	1425,
	25,
	25,
	1421,
	1421,
	1007,
	112,
	1426,
	1427,
	1428,
	641,
	641,
	1429,
	1430,
	1431,
	1432,
	25,
	1433,
	1421,
	1421,
	1421,
	1421,
	1421,
	1421,
	1421,
	1421,
	1421,
	1421,
	1421,
	1421,
	1421,
	1421,
	1421,
	1421,
	1421,
	1421,
	1421,
	1421,
	1421,
	1421,
	1421,
	1421,
	1421,
	1421,
	1421,
	1421,
	1421,
	1434,
	1421,
	1421,
	1421,
	1421,
	1421,
	1435,
	112,
	893,
	1436,
	1437,
	1437,
	1421,
	1421,
	527,
	25,
	1438,
	1421,
	1439,
	1440,
	1422,
	112,
	1441,
	1442,
	1443,
	1419,
	1443,
	1444,
	1421,
	1443,
	1442,
	1441,
	1443,
	1445,
	1441,
	1446,
	1446,
	1447,
	125,
	21,
	21,
	1115,
	3,
	25,
	681,
	25,
	1421,
	509,
	3,
	761,
	541,
	3,
	509,
	303,
	1086,
	1448,
	10,
	10,
	102,
	37,
	1449,
	1450,
	1451,
	1082,
	1027,
	26,
	26,
	26,
	26,
	26,
	26,
	26,
	26,
	26,
	26,
	26,
	26,
	26,
	26,
	14,
	102,
	14,
	26,
	102,
	31,
	10,
	10,
	102,
	102,
	102,
	963,
	963,
	14,
	14,
	14,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	26,
	26,
	23,
	52,
	1019,
	1452,
	32,
	30,
	30,
	30,
	31,
	31,
	23,
	32,
	34,
	23,
	37,
	1449,
	1450,
	1451,
	1082,
	1027,
	32,
	32,
	273,
	1453,
	1163,
	1163,
	124,
	23,
	23,
	26,
	23,
	23,
	26,
	26,
	273,
	273,
	273,
	273,
	23,
	31,
	23,
	14,
	26,
	163,
	26,
	166,
	26,
	163,
	824,
	1454,
	26,
	163,
	27,
	202,
	26,
	10,
	273,
	503,
	26,
	26,
	366,
	366,
	27,
	27,
	578,
	1455,
	1455,
	1456,
	10,
	14,
	31,
	102,
	23,
	32,
	10,
	10,
	14,
	10,
	1456,
	1457,
	1458,
	23,
	1457,
	503,
	1457,
	1456,
	1457,
	58,
	26,
	23,
	23,
	1459,
	1460,
	1064,
	26,
	124,
	1456,
	26,
	34,
	26,
	37,
	124,
	124,
	737,
	1461,
	1462,
	1463,
	10,
	35,
	56,
	1464,
	1465,
	498,
	23,
	10,
	26,
	156,
	58,
	26,
	23,
	23,
	1459,
	1460,
	1064,
	10,
	35,
	56,
	1464,
	26,
	26,
	124,
	23,
	498,
	1465,
	37,
	124,
	124,
	737,
	34,
	26,
	1461,
	1462,
	1463,
	1456,
	10,
	26,
	156,
	23,
	26,
	578,
	26,
	14,
	26,
	26,
	1455,
	26,
	14,
	26,
	26,
	1455,
	26,
	14,
	26,
	26,
	1456,
	26,
	14,
	26,
	26,
	1456,
	26,
	14,
	26,
	15,
	10,
	14,
	31,
	102,
	124,
	23,
	32,
	10,
	14,
	26,
	14,
	10,
	124,
	14,
	26,
	10,
	26,
	1457,
	26,
	14,
	26,
	26,
	1458,
	23,
	26,
	14,
	26,
	26,
	1457,
	26,
	14,
	26,
	26,
	503,
	26,
	14,
	26,
	26,
	1457,
	26,
	14,
	26,
	26,
	1456,
	26,
	14,
	26,
	26,
	1457,
	26,
	14,
	26,
	58,
	26,
	23,
	23,
	10,
	23,
	23,
	23,
	23,
	273,
	503,
	26,
	26,
	366,
	366,
	27,
	27,
	23,
	7,
	23,
	15,
	7,
	23,
	1459,
	1460,
	1064,
	10,
	35,
	56,
	1464,
	26,
	26,
	124,
	23,
	498,
	1465,
	37,
	124,
	124,
	737,
	34,
	26,
	1461,
	1462,
	1463,
	1456,
	10,
	26,
	156,
	1082,
	895,
	1466,
	10,
	14,
	26,
	1467,
	62,
	23,
	157,
	27,
	26,
	26,
	10,
	14,
	26,
	27,
	1468,
	23,
	62,
	157,
	26,
	26,
	10,
	14,
	26,
	1469,
	23,
	62,
	157,
	27,
	26,
	26,
	10,
	14,
	26,
	1349,
	23,
	62,
	157,
	27,
	26,
	26,
	10,
	10,
	32,
	10,
	32,
	10,
	14,
	26,
	26,
	27,
	30,
	30,
	23,
	23,
	62,
	157,
	10,
	10,
	32,
	10,
	32,
	10,
	14,
	27,
	157,
	62,
	26,
	26,
	124,
	157,
	23,
	23,
	37,
	30,
	30,
	30,
	23,
	32,
	157,
	23,
	10,
	14,
	32,
	1470,
	23,
	1082,
	895,
	1466,
	62,
	124,
	157,
	27,
	26,
	26,
	10,
	14,
	26,
	1471,
	23,
	62,
	157,
	27,
	26,
	26,
	10,
	14,
	26,
	380,
	23,
	62,
	157,
	27,
	26,
	26,
	23,
	10,
	14,
	26,
	1472,
	23,
	62,
	157,
	27,
	26,
	26,
	10,
	14,
	26,
	1473,
	23,
	1082,
	895,
	1466,
	62,
	157,
	27,
	26,
	26,
	10,
	14,
	26,
	1474,
	23,
	62,
	157,
	27,
	26,
	26,
	1092,
	10,
	14,
	26,
	824,
	23,
	62,
	157,
	27,
	26,
	26,
	1092,
	102,
	31,
	650,
	273,
	650,
	273,
	28,
	23,
	102,
	31,
	650,
	273,
	650,
	273,
	650,
	273,
	650,
	273,
	28,
	23,
	102,
	31,
	650,
	273,
	650,
	273,
	650,
	273,
	650,
	273,
	650,
	273,
	28,
	23,
	102,
	31,
	650,
	273,
	28,
	23,
	102,
	31,
	650,
	273,
	650,
	273,
	650,
	273,
	28,
	23,
	28,
	23,
	102,
	31,
	650,
	273,
	650,
	273,
	650,
	273,
	650,
	273,
	650,
	273,
	28,
	23,
	28,
	23,
	102,
	31,
	650,
	273,
	650,
	273,
	650,
	273,
	28,
	23,
	102,
	31,
	650,
	273,
	650,
	273,
	28,
	23,
	102,
	31,
	650,
	273,
	650,
	273,
	28,
	23,
	1475,
	34,
	10,
	23,
	9,
	102,
	10,
	10,
	23,
	27,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	650,
	273,
	1417,
	1,
	26,
	26,
	10,
	10,
	102,
	102,
	30,
	32,
	30,
	30,
	102,
	23,
	1034,
	14,
	34,
	37,
	1449,
	1450,
	1451,
	1082,
	1027,
	23,
	14,
	58,
	28,
	30,
	1476,
	23,
	848,
	1477,
	27,
	157,
	1478,
	23,
	26,
	23,
	14,
	23,
	163,
	26,
	166,
	26,
	32,
	23,
	102,
	14,
	23,
	14,
	14,
	14,
	32,
	23,
	102,
	14,
	23,
	14,
	23,
	14,
	26,
	10,
	30,
	23,
	27,
	102,
	27,
	102,
	27,
	102,
	27,
	23,
	102,
	23,
	27,
	102,
	27,
	102,
	27,
	102,
	156,
	23,
	102,
	23,
	27,
	23,
	102,
	23,
	23,
	102,
	23,
	27,
	102,
	27,
	23,
	102,
	23,
	26,
	14,
	26,
	14,
	23,
	424,
	23,
	23,
	26,
	14,
	23,
	424,
	23,
	23,
	26,
	14,
	32,
	10,
	273,
	650,
	14,
	14,
	15,
	14,
	23,
	424,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	-1,
	-1,
	-1,
	-1,
	-1,
	32,
	32,
	32,
	4,
	23,
	23,
	23,
	26,
	26,
	14,
	14,
	14,
	28,
	26,
	28,
	26,
	28,
	26,
	28,
	26,
	26,
	26,
	26,
	23,
	824,
	273,
	26,
	23,
	14,
	23,
	23,
	23,
	23,
	23,
	23,
	102,
	1034,
	650,
	26,
	14,
	23,
	14,
	34,
	23,
	32,
	23,
	102,
	14,
	23,
	14,
	124,
	157,
	23,
	28,
	26,
	23,
	1479,
	1480,
	124,
	1055,
	23,
	28,
	26,
	23,
	3,
	23,
	1481,
	124,
	157,
	23,
	28,
	26,
	23,
	23,
	273,
	273,
	977,
	23,
	23,
	273,
	273,
	977,
	23,
	23,
	23,
	273,
	977,
	23,
	1482,
	1480,
	124,
	1055,
	23,
	28,
	26,
	23,
	3,
	23,
	1483,
	1484,
	1485,
	23,
	3,
	3,
	23,
	1483,
	32,
	23,
	102,
	14,
	23,
	14,
	1486,
	1487,
	965,
	1488,
	1164,
	1164,
	1489,
	1486,
	1486,
	1490,
	1491,
	1492,
	380,
	1493,
	1494,
	1495,
	1496,
	1497,
	23,
	365,
	157,
	1480,
	1498,
	380,
	380,
	26,
	157,
	157,
	157,
	157,
	157,
	157,
	157,
	157,
	157,
	157,
	157,
	157,
	157,
	157,
	1499,
	157,
	157,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	157,
	157,
	1500,
	157,
	157,
	157,
	157,
	157,
	157,
	157,
	157,
	1449,
	1019,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	157,
	37,
	157,
	1501,
	10,
	38,
	1502,
	277,
	32,
	1503,
	824,
	1504,
	1505,
	1131,
	1506,
	980,
	23,
	1507,
	1508,
	26,
	1131,
	23,
	345,
	23,
	23,
	23,
	23,
	23,
	26,
	26,
	1509,
	23,
	23,
	23,
	23,
	26,
	23,
	3,
	14,
	14,
	23,
	23,
	23,
	26,
	23,
	23,
	3,
	14,
	23,
	23,
	23,
	26,
	23,
	14,
	26,
	23,
	26,
	23,
	3,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	26,
	26,
	26,
	26,
	26,
	26,
	26,
	26,
	26,
	26,
	26,
	26,
	26,
	26,
	26,
	26,
	14,
	102,
	14,
	32,
	10,
	1510,
	1034,
	102,
	650,
	10,
	10,
	10,
	10,
	10,
	10,
	14,
	14,
	14,
	14,
	14,
	14,
	14,
	14,
	14,
	14,
	14,
	14,
	14,
	14,
	14,
	14,
	14,
	14,
	14,
	14,
	14,
	14,
	14,
	14,
	14,
	14,
	14,
	14,
	14,
	14,
	14,
	14,
	14,
	14,
	14,
	14,
	14,
	14,
	14,
	14,
	14,
	14,
	14,
	14,
	14,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	32,
	23,
	32,
	26,
	26,
	9,
	9,
	23,
	1511,
	34,
	23,
	23,
	23,
	23,
	23,
	273,
	1461,
	1462,
	273,
	1092,
	156,
	1512,
	1513,
	1514,
	23,
	3,
	23,
	23,
	124,
	163,
	26,
	166,
	26,
	163,
	824,
	1454,
	26,
	163,
	27,
	202,
	26,
	3,
	23,
	9,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	14,
	23,
	23,
	23,
	32,
	23,
	102,
	14,
	23,
	14,
	23,
	23,
	23,
	273,
	1515,
	273,
	1092,
	23,
	3,
	23,
	23,
	650,
	273,
	26,
	3,
	1092,
	23,
	23,
	1092,
	14,
	26,
	26,
	14,
	26,
	102,
	31,
	380,
	14,
	102,
	102,
	23,
	0,
	28,
	23,
	23,
	824,
	32,
	23,
	102,
	14,
	23,
	14,
	324,
	23,
	26,
	23,
	23,
	23,
	23,
	824,
	1395,
	26,
	23,
	26,
	23,
	23,
	1516,
	23,
	23,
	23,
	431,
	431,
	124,
	27,
	23,
	26,
	1517,
	23,
	23,
	23,
	23,
	27,
	1518,
	23,
	23,
	23,
	23,
	23,
	23,
	26,
	23,
	27,
	27,
	14,
	14,
	26,
	14,
	26,
	102,
	10,
	32,
	102,
	31,
	650,
	273,
	650,
	273,
	23,
	23,
	23,
	23,
	23,
	27,
	824,
	824,
	23,
	32,
	32,
	23,
	273,
	23,
	23,
	23,
	23,
	26,
	26,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	1019,
	23,
	23,
	23,
	23,
	23,
	3,
	3,
	541,
	3,
	23,
	23,
	23,
	26,
	14,
	26,
	14,
	10,
	14,
	26,
	26,
	23,
	23,
	56,
	32,
	23,
	27,
	27,
	26,
	26,
	23,
	23,
	157,
	1519,
	1520,
	-1,
	-1,
	-1,
	-1,
	-1,
	1521,
	1522,
	21,
	1523,
	1524,
	365,
	1084,
	1525,
	1526,
	1527,
	1528,
	1529,
	364,
	364,
	218,
	1530,
	1531,
	1095,
	1106,
	1531,
	43,
	1530,
	1530,
	1532,
	1095,
	1,
	125,
	21,
	21,
	125,
	21,
	21,
	1533,
	1006,
	1534,
	1535,
	1536,
	1537,
	3,
	32,
	23,
	102,
	14,
	23,
	14,
	14,
	14,
	1538,
	1523,
	1539,
	1523,
	1540,
	23,
	23,
	23,
	27,
	23,
	26,
	26,
	26,
	26,
	14,
	32,
	10,
	102,
	102,
	31,
	102,
	102,
	26,
	23,
	26,
	26,
	23,
	14,
	23,
	31,
	23,
	32,
	32,
	32,
	1541,
	1542,
	30,
	23,
	10,
	273,
	23,
	163,
	124,
	1543,
	26,
	32,
	23,
	102,
	1544,
	23,
	14,
	1082,
	1082,
	14,
	23,
	32,
	23,
	102,
	14,
	23,
	14,
	23,
	1082,
	23,
	23,
	23,
	26,
	14,
	963,
	23,
	23,
	23,
	23,
	23,
	977,
	1545,
	1546,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	498,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	0,
	1115,
	1547,
	1547,
	1548,
	1549,
	3,
	23,
	23,
	23,
	23,
	28,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
};
static const Il2CppTokenRangePair s_rgctxIndices[11] = 
{
	{ 0x02000074, { 0, 17 } },
	{ 0x020000C5, { 17, 24 } },
	{ 0x020000C6, { 45, 3 } },
	{ 0x020000D1, { 48, 8 } },
	{ 0x020000D2, { 56, 1 } },
	{ 0x06000418, { 41, 1 } },
	{ 0x06000419, { 42, 1 } },
	{ 0x0600041A, { 43, 2 } },
	{ 0x060005D2, { 57, 2 } },
	{ 0x060005D3, { 59, 1 } },
	{ 0x060005D4, { 60, 1 } },
};
static const Il2CppRGCTXDefinition s_rgctxValues[61] = 
{
	{ (Il2CppRGCTXDataType)2, 9064 },
	{ (Il2CppRGCTXDataType)3, 10378 },
	{ (Il2CppRGCTXDataType)3, 10379 },
	{ (Il2CppRGCTXDataType)3, 10380 },
	{ (Il2CppRGCTXDataType)3, 10381 },
	{ (Il2CppRGCTXDataType)2, 7310 },
	{ (Il2CppRGCTXDataType)3, 10382 },
	{ (Il2CppRGCTXDataType)3, 10383 },
	{ (Il2CppRGCTXDataType)3, 10384 },
	{ (Il2CppRGCTXDataType)3, 10385 },
	{ (Il2CppRGCTXDataType)2, 9065 },
	{ (Il2CppRGCTXDataType)3, 10386 },
	{ (Il2CppRGCTXDataType)3, 10387 },
	{ (Il2CppRGCTXDataType)3, 10388 },
	{ (Il2CppRGCTXDataType)3, 10389 },
	{ (Il2CppRGCTXDataType)2, 9066 },
	{ (Il2CppRGCTXDataType)3, 10390 },
	{ (Il2CppRGCTXDataType)3, 10391 },
	{ (Il2CppRGCTXDataType)3, 10392 },
	{ (Il2CppRGCTXDataType)3, 10393 },
	{ (Il2CppRGCTXDataType)3, 10394 },
	{ (Il2CppRGCTXDataType)3, 10395 },
	{ (Il2CppRGCTXDataType)3, 10396 },
	{ (Il2CppRGCTXDataType)3, 10397 },
	{ (Il2CppRGCTXDataType)3, 10398 },
	{ (Il2CppRGCTXDataType)2, 7565 },
	{ (Il2CppRGCTXDataType)3, 10399 },
	{ (Il2CppRGCTXDataType)3, 10400 },
	{ (Il2CppRGCTXDataType)3, 10401 },
	{ (Il2CppRGCTXDataType)3, 10402 },
	{ (Il2CppRGCTXDataType)3, 10403 },
	{ (Il2CppRGCTXDataType)3, 10404 },
	{ (Il2CppRGCTXDataType)2, 9067 },
	{ (Il2CppRGCTXDataType)2, 7566 },
	{ (Il2CppRGCTXDataType)2, 7568 },
	{ (Il2CppRGCTXDataType)3, 10405 },
	{ (Il2CppRGCTXDataType)3, 10406 },
	{ (Il2CppRGCTXDataType)2, 7559 },
	{ (Il2CppRGCTXDataType)2, 9068 },
	{ (Il2CppRGCTXDataType)3, 10407 },
	{ (Il2CppRGCTXDataType)3, 10408 },
	{ (Il2CppRGCTXDataType)3, 10409 },
	{ (Il2CppRGCTXDataType)3, 10410 },
	{ (Il2CppRGCTXDataType)3, 10411 },
	{ (Il2CppRGCTXDataType)3, 10412 },
	{ (Il2CppRGCTXDataType)3, 10413 },
	{ (Il2CppRGCTXDataType)3, 10414 },
	{ (Il2CppRGCTXDataType)2, 7574 },
	{ (Il2CppRGCTXDataType)2, 9070 },
	{ (Il2CppRGCTXDataType)3, 10415 },
	{ (Il2CppRGCTXDataType)3, 10416 },
	{ (Il2CppRGCTXDataType)3, 10417 },
	{ (Il2CppRGCTXDataType)2, 7602 },
	{ (Il2CppRGCTXDataType)3, 10418 },
	{ (Il2CppRGCTXDataType)3, 10419 },
	{ (Il2CppRGCTXDataType)2, 7603 },
	{ (Il2CppRGCTXDataType)2, 7607 },
	{ (Il2CppRGCTXDataType)2, 9071 },
	{ (Il2CppRGCTXDataType)2, 7829 },
	{ (Il2CppRGCTXDataType)3, 10420 },
	{ (Il2CppRGCTXDataType)3, 10421 },
};
extern const Il2CppCodeGenModule g_ObiCodeGenModule;
const Il2CppCodeGenModule g_ObiCodeGenModule = 
{
	"Obi.dll",
	1657,
	s_methodPointers,
	s_InvokerIndices,
	0,
	NULL,
	11,
	s_rgctxIndices,
	61,
	s_rgctxValues,
	NULL,
};

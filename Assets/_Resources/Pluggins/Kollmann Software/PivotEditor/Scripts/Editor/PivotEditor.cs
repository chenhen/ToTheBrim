﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEditor.EditorTools;
using UnityEngine;

[EditorTool("Pivot Editor", typeof(MeshFilter))]
public class PivotEditor : EditorTool {

    /// <summary>
    /// GUIContent for the Toolbar
    /// </summary>
    GUIContent editorToolIcon;

    /// <summary>
    /// Local Position of the selected objects pivot
    /// </summary>
    Vector3 pivotPosition;

    /// <summary>
    /// Selected transform
    /// </summary>
    Transform selected;

    /// <summary>
    /// Height of the tool GUI window
    /// </summary>
    float toolGUIWindowHeight = EditorGUIUtility.singleLineHeight * 4f;

    /// <summary>
    /// Width of the tool GUI window
    /// </summary>
    float toolGUIWindowWidth = 300f;
    
    /// <summary>
    /// Margin of the tool GUI window
    /// </summary>
    float toolGUIWindowMargin = 10f;

    /// <summary>
    /// Sets the icon for the toolbar and adds a delegate callback for resetting the pivots position when a different object is selected
    /// </summary>
    void OnEnable() {
        editorToolIcon = new GUIContent() {
            image = EditorGUIUtility.IconContent("ToolHandlePivot").image,
            text = "Pivot Editor",
            tooltip = "Pivot Editor"
        };
        Selection.selectionChanged += ResetPivotPosition;
    }

    /// <summary>
    /// Removes the callback for resetting the pivots position when a different object is selected
    /// </summary>
    void OnDisable() {
        Selection.selectionChanged -= ResetPivotPosition;
    }

    /// <summary>
    /// icon for the toolbar
    /// </summary>
    public override GUIContent toolbarIcon {
        get { return editorToolIcon; }
    }

    /// <summary>
    /// Draws the handles and Tool window on the scene GUI
    /// </summary>
    public override void OnToolGUI(EditorWindow window) {
        selected = Selection.activeGameObject.transform;
        pivotPosition = selected.InverseTransformPoint(Handles.DoPositionHandle(selected.TransformPoint(pivotPosition), Tools.pivotRotation == PivotRotation.Global ? Quaternion.identity : selected.rotation));
        var windowRect = new Rect(toolGUIWindowMargin, window.position.height - (toolGUIWindowHeight + 30), toolGUIWindowWidth, toolGUIWindowHeight);
        using (new GUILayout.AreaScope(windowRect)) {
            DrawPivotEditorToolWindow();
        }
    }

    /// <summary>
    /// Draws the Tool window to the bottom left of the scene view
    /// </summary>
    void DrawPivotEditorToolWindow() {
        using (new GUILayout.VerticalScope("Pivot Editor", ((GUIStyle)"Window"))) {
            pivotPosition = EditorGUILayout.Vector3Field("", pivotPosition);
            using (new EditorGUILayout.HorizontalScope()) {
                if (GUILayout.Button("Save Pivot (New Mesh)")) {
                    SaveNewPivotedMesh();
                }
                if (GUILayout.Button("Reset")) {
                    ResetPivotPosition();
                }
            }
        }
    }

    /// <summary>
    /// Saves the mesh of the selected object to a new .asset file and sets it as mesh used on the selected object
    /// also updates the object, so it and its colliders stays in the same position
    /// </summary>
    void SaveNewPivotedMesh() {
        MeshFilter selectedMeshFilter = selected.GetComponent<MeshFilter>();
        selected.transform.localPosition += Vector3.Scale(pivotPosition, selected.transform.localScale);
        Mesh newMesh = CloneMesh(selectedMeshFilter.sharedMesh);
        newMesh.vertices = MoveVertices(newMesh, pivotPosition);

        string filePath = EditorUtility.SaveFilePanelInProject("Save New Pivoted Mesh", "New Mesh", "asset", "");
        if (filePath == "") {
            return;
        }
        AssetDatabase.CreateAsset(newMesh, filePath);
        selectedMeshFilter.sharedMesh = newMesh;

        ReadjustCollider();
        ResetPivotPosition();
    }    

    /// <summary>
    /// Clones the given mesh
    /// </summary>
    /// <param name="meshToClone">Mesh to Clone</param>
    /// <returns>Clone of <paramref name="meshToClone"/></returns>
    Mesh CloneMesh(Mesh meshToClone) {
        Mesh newMesh = new Mesh();
        newMesh.vertices = meshToClone.vertices;
        newMesh.triangles = meshToClone.triangles;
        newMesh.uv = meshToClone.uv;
        newMesh.uv2 = meshToClone.uv2;
        newMesh.normals = meshToClone.normals;
        newMesh.tangents = meshToClone.tangents;
        newMesh.colors = meshToClone.colors;
        return newMesh;
    }

    /// <summary>
    /// Resets the position of the pivot
    /// </summary>
    void ResetPivotPosition() {
        pivotPosition = Vector3.zero;
    }

    /// <summary>
    /// Moves all of <paramref name="mesh"/>'s vertices by <paramref name="movement"/>
    /// </summary>
    /// <param name="mesh">Mesh whichs vertices should be moved</param>
    /// <param name="movement">movement by which the vertices should be moved</param>
    /// <returns>Array of the new Vertice positions</returns>
    Vector3[] MoveVertices(Mesh mesh, Vector3 movement) {
        Vector3[] verts = mesh.vertices;
        for (int i = 0; i < verts.Length; i++) {
            verts[i] -= movement;
        }
        return verts;
    }

    /// <summary>
    /// Readjusts the selected objects collider to fit the new pivot
    /// </summary>
    void ReadjustCollider() {
        var collider = selected.GetComponent<Collider>();
        if (collider == null) {
            return;
        }
        if (collider is BoxCollider) {
            ((BoxCollider)collider).center -= pivotPosition;
        }
        else if (collider is CapsuleCollider) {
            ((CapsuleCollider)collider).center -= pivotPosition;
        }
        else if (collider is SphereCollider) {
            ((SphereCollider)collider).center -= pivotPosition;
        }
        else if (collider is MeshCollider) {
            ((MeshCollider)collider).sharedMesh = null;
            ((MeshCollider)collider).sharedMesh = selected.GetComponent<MeshFilter>().mesh;
        }
    }
}
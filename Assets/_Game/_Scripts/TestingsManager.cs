﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Obi;

public class TestingsManager : MonoBehaviour
{
    [SerializeField] ObiEmitter m_obiEmitter;
    [SerializeField] float emmiterspeed;

    private void Update()
    {
        
        if (Input.GetMouseButton(0))
        {
            m_obiEmitter.speed = emmiterspeed;
        }
        else
        {
            m_obiEmitter.speed = 0;
        }
    }

}

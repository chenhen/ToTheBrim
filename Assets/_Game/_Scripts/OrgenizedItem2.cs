﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OrgenizedItem2 : MonoBehaviour
{
  [SerializeField] private LayerMask _hitLayers;

  private SphereCollider _itemMoveCollider;
  // private BoxCollider _itemTriggeredCollider;
  [SerializeField] Vector3 orgPos;
  [SerializeField] Animator m_Animator;
  [SerializeField] Camera currCamera;
  [SerializeField] BoxCollider FridgeCollider;
  private void Awake()
  {
    orgPos = transform.localPosition;
    _itemMoveCollider = gameObject.GetComponent<SphereCollider>();
  }

  void Update()
  {
    if (Input.GetMouseButton(0))
    {
      Vector3 mouse = Input.mousePosition;
      Ray castPoint = currCamera.ScreenPointToRay(mouse);
      RaycastHit hit;

      if (Physics.Raycast(castPoint, out hit, Mathf.Infinity, _hitLayers))
      {
        gameObject.transform.position = new Vector3(hit.point.x - 0.1f, hit.point.y + 0.2f, gameObject.transform.position.z);

        if (gameObject.transform.localPosition.y >= 1.4f)
        {
          OrgenizingManager2.instance.FinishStageTwoRoutine();
        }
      }
    }
    else
    {
      gameObject.transform.localPosition = orgPos;
    }
  }
}

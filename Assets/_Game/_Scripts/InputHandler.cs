﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using Obi;

public class InputHandler : MonoBehaviour
{

    [SerializeField] Animator m_Animator;
    [SerializeField] ObiEmitter m_obiEmitter;
    public bool didStart;

    private void Awake()
    {
        didStart = false;
    }

    void Update()
    {
        if (Input.GetMouseButton(0) && !IsPointerOverGameObject())
        {
            didStart = true;
            m_Animator.SetBool("Isclicking", true);

        }
        else 
        {

            m_Animator.SetBool("Isclicking", false);
            GameManager.instance.obiEmmiter.speed = 0;

            if (didStart == true)
            {
                GameManager.instance.OnFinish();
                didStart = false;
            }
        }


    }

    public void StartPouring()
    {
        if (didStart == true)
        {
            GameManager.instance.obiEmmiter.speed = GameManager.instance.emitterSpeed;
            AudioManager.instance.PlayPouring();
        }

    }


    public static bool IsPointerOverGameObject()
    {
        // Check mouse
        if (EventSystem.current.IsPointerOverGameObject())
        {
            return true;
        }

        // Check touches
        for (int i = 0; i < Input.touchCount; i++)
        {
            var touch = Input.GetTouch(i);
            if (EventSystem.current.IsPointerOverGameObject(touch.fingerId))
            {
                return true;
            }

        }

        return false;
    }

 }

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OrgenizedItem : MonoBehaviour
{
    [SerializeField] private LayerMask _hitLayers;

    private SphereCollider _itemMoveCollider;
    private BoxCollider _itemTriggeredCollider;
    private int chosenContainer = 0;

    private void Awake()
    {
        _itemMoveCollider = gameObject.GetComponent<SphereCollider>();
        _itemTriggeredCollider = gameObject.GetComponent<BoxCollider>();
        _itemTriggeredCollider.isTrigger = false;
    }

    void Update()
    {
        if (Input.GetMouseButton(0))
        {
            _itemTriggeredCollider.isTrigger = true;

            Vector3 mouse = Input.mousePosition;
            Ray castPoint = Camera.main.ScreenPointToRay(mouse);
            RaycastHit hit;

            if (Physics.Raycast(castPoint, out hit, Mathf.Infinity, _hitLayers))
            {
                gameObject.transform.position = new Vector3(hit.point.x - 0.1f, hit.point.y + 0.2f, gameObject.transform.position.z);
            }
        }
        else
        {
            _itemTriggeredCollider.isTrigger = false;
            if (chosenContainer != 0)
            {
                OrgenizingManager.instance.InsertItem(chosenContainer);
                chosenContainer = 0;
            }
        }
    }

    void OnTriggerEnter(Collider other)
    {
        if (other == OrgenizingManager.instance.container1)
        {
            // Debug.Log("Con1");
            OrgenizingManager.instance.container1.GetComponent<MeshRenderer>().material = OrgenizingManager.instance.higlitedMaterial;
            OrgenizingManager.instance.container2.GetComponent<MeshRenderer>().material = OrgenizingManager.instance.originalMaterial;
            chosenContainer = 1;
        }
        else if (other == OrgenizingManager.instance.container2)
        {
            //Debug.Log("Con2");
            OrgenizingManager.instance.container1.GetComponent<MeshRenderer>().material = OrgenizingManager.instance.originalMaterial;
            OrgenizingManager.instance.container2.GetComponent<MeshRenderer>().material = OrgenizingManager.instance.higlitedMaterial;

            chosenContainer = 2;
        }
    }

    void OnTriggerExit(Collider other)
    {
       // OrgenizingManager.instance.ResetCollidersColor();
        gameObject.transform.localPosition = OrgenizingManager.instance.itemFinalPos;
    }
}

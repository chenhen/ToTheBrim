﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using Obi;
using UnityEngine.SceneManagement;
using Tabtale.TTPlugins;

public class GameManager : MonoBehaviour
{

  public static GameManager instance;

  [SerializeField] ObiFluidRenderer obiFluidRenderer;

  [SerializeField] GameObject WinPanel;
  [SerializeField] GameObject LoosePanel;
  [SerializeField] GameObject obiSolver;
  [SerializeField] GameObject obiSolverClone;

  [SerializeField] int minParticles;
  [SerializeField] int maxParticles;

  [SerializeField] GameObject _milkCarton;
  [SerializeField] GameObject _maxLine;
  [SerializeField] GameObject _cork;

  [SerializeField] Text _PositivefeedBackText;
  [SerializeField] Text _NegitivefeedBackText;
  [SerializeField] Text _LogText;

  public ObiEmitter obiEmmiter;

  public float emitterSpeed;
  public bool isFinished;

  [SerializeField] Vector3 corkOriginalPos;
  [SerializeField] Vector3 corkFinalPos;

  [SerializeField] Vector3 solverPos;
  [SerializeField] Quaternion solverRot;

  private void Awake()
  {
    if (instance != null)
    {
      Destroy(gameObject);
    }
    else
    {
      instance = this;
    }
        TTPCore.Setup();

  }

  private void Start()
  {
    if (_LogText != null)
    {
      _LogText.text = "Graphics Shader Level == " + SystemInfo.graphicsShaderLevel;
    }


    Reset();
    //  StartCoroutine(WaitAndReset());
  }

  private void Update()
  {
    if (Input.GetMouseButtonDown(0) && !EventSystem.current.IsPointerOverGameObject() && isFinished == true)
    {
      StartCoroutine(Cork());
    }
  }

  public void OnFinish()
  {
    Debug.Log(obiEmmiter.activeParticleCount);

    if (obiEmmiter.activeParticleCount >= minParticles && obiEmmiter.activeParticleCount < maxParticles)
    {
      _PositivefeedBackText.text = "Just Right";
      AudioManager.instance.PlayPutDown();
      _milkCarton.SetActive(false);
      isFinished = true;
      _maxLine.SetActive(false);
      _cork.SetActive(true);

    }
    else if (obiEmmiter.activeParticleCount <= minParticles)
    {
      OnLoose();
      _NegitivefeedBackText.text = "Too Low";
      //            Debug.Log("Too Low");
    }
    else if (obiEmmiter.activeParticleCount >= maxParticles)
    {
      OnLoose();
      _NegitivefeedBackText.text = "Too High";
      //           Debug.Log("Too High");
    }
  }

  public IEnumerator Cork()
  {
    isFinished = false;
    float timeSinceStarted = 0f;

    while (true)
    {
      timeSinceStarted += Time.deltaTime;
      _cork.transform.localPosition = Vector3.Lerp(_cork.transform.localPosition, corkFinalPos, timeSinceStarted);

      // If the object has arrived, stop the coroutine
      if (_cork.transform.localPosition == corkFinalPos)
      {
        AudioManager.instance.PlaySuccess();
        // Debug.Log("win !!");
        WinPanel.SetActive(true);

        yield return new WaitForSeconds(2);
        Onwin();

        yield break;
      }

      // Otherwise, continue next frame
      yield return null;
    }

  }



  void Onwin()
  {

    if (SceneManager.GetActiveScene().buildIndex == 0)
    {
      SceneManager.LoadScene(1);
    }
    else if (SceneManager.GetActiveScene().buildIndex == 1)
    {
      if (obiSolverClone != null)
      {
        Destroy(obiSolverClone);
      }
      SceneManager.LoadScene(2);
    }

  }

  void OnLoose()
  {
    if (obiSolverClone != null)
    {
      Destroy(obiSolverClone);
    }

    AudioManager.instance.PlayFail();
    // Debug.Log("loose!!!");
    LoosePanel.SetActive(true);

    StartCoroutine(WaitAndReset());

  }


  public IEnumerator WaitAndReset()
  {
    yield return new WaitForSeconds(1);
    Reset();
  }

  public void Reset()
  {
    StopAllCoroutines();

    obiSolverClone = Instantiate(obiSolver, solverPos, solverRot);
    obiEmmiter = obiSolverClone.GetComponentInChildren<ObiEmitter>();

    if (obiFluidRenderer != null)
    {
      obiFluidRenderer.particleRenderers[0] = obiEmmiter.GetComponent<ObiParticleRenderer>();
    }

    AudioManager.instance.PlayReset();

    // feedback text
    _PositivefeedBackText.text = "";
    _NegitivefeedBackText.text = "";

    // cork 
    _cork.transform.localPosition = corkOriginalPos;
    _cork.SetActive(false);

    WinPanel.SetActive(false);
    LoosePanel.SetActive(false);

    _cork.SetActive(false);
    _maxLine.SetActive(true);

    _milkCarton.SetActive(true);

    // Check for shader model 4.1 or better support
    /*  if (obiEmmiter.gameObject.GetComponent<ObiParticleRenderer>() != null && obiEmmiter.gameObject.GetComponent<ObiInstancedParticleRenderer>() != null)
    {
      if (SystemInfo.graphicsShaderLevel >= 41)
      {
        obiEmmiter.gameObject.GetComponent<ObiParticleRenderer>().enabled = false;
        obiEmmiter.gameObject.GetComponent<ObiInstancedParticleRenderer>().enabled = true;
      }
      else
      {
        obiEmmiter.gameObject.GetComponent<ObiParticleRenderer>().enabled = true;
        obiEmmiter.gameObject.GetComponent<ObiInstancedParticleRenderer>().enabled = false;
      }

    }
    */


  }
}

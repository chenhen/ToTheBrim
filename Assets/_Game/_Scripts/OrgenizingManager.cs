﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;


public class OrgenizingManager : MonoBehaviour
{
    public static OrgenizingManager instance;

    public Collider container1;
    public Collider container2;

    public Material originalMaterial;
    public Material higlitedMaterial;
    public Material errorMaterial;

    public Vector3 itemStartPos;
    public Vector3 itemFinalPos;

    public List<GameObject> instantiatedItem = new List<GameObject>();

    public List<GameObject> container1Items = new List<GameObject>();
    public List<GameObject> container2Items = new List<GameObject>();

    public List<GameObject> useddItems = new List<GameObject>();

    public GameObject currentItem;
    public int currentItemNumber = 0;

    int tempContailner1 = 0;
    int tempContailner2 = 0;


    [SerializeField] List<Transform> milkUIitems = new List<Transform>();
    [SerializeField] List<Transform> chocolateUIitems = new List<Transform>();


    [SerializeField] Text feedbackTextPositive;
    [SerializeField] Text feedbackTextNegative;

    private void Awake()
    {
        if (GameManager.instance)
            UnityEngine.Object.Destroy(gameObject);
        else
            instance = this;
    }

    void Start()
    {
        Reset();
       // showItem(currentItemNumber);
    }

    public void Reset()
    {
        currentItemNumber = 0;

        for (int i = 0; i < container1Items.Count; i++)
        {
            container1Items[i].SetActive(false);
        }
        for (int i = 0; i < container2Items.Count; i++)
        {
            container2Items[i].SetActive(false);
        }
        for (int i = 0; i < useddItems.Count; i++)
        {
            Destroy(useddItems[i]);
        }

        for (int i = 0; i < milkUIitems.Count; i++)
        {
            milkUIitems[i].GetChild(0).gameObject.SetActive(false);
        }

        for (int i = 0; i < chocolateUIitems.Count; i++)
        {
            chocolateUIitems[i].GetChild(0).gameObject.SetActive(false);
        }

        feedbackTextPositive.gameObject.SetActive(false);
        feedbackTextNegative.gameObject.SetActive(false);

        ResetCollidersColor();

        useddItems.Clear();

        tempContailner1 = 0;
        tempContailner2 = 0;

        showItem(0);
    }

    public void ResetCollidersColor()
    {
        container1.GetComponent<MeshRenderer>().material = originalMaterial;
        container2.GetComponent<MeshRenderer>().material = originalMaterial;
    }


    public void showItem(int itemNumber)
    {
        ResetCollidersColor();
        currentItem = Instantiate(instantiatedItem[itemNumber], itemStartPos, transform.rotation);
        useddItems.Add(currentItem);
        StartCoroutine(ItemAnimate());
    }

    public void InsertItem(int ContainerNumber)
    {
       

        // Win
        if (tempContailner1 + tempContailner2 == instantiatedItem.Count -1)
        {
            AudioManager.instance.PlayMountMilk();
            container1Items[tempContailner1].SetActive(true);
            milkUIitems[tempContailner1].GetChild(0).gameObject.SetActive(true);
            currentItem.SetActive(false);
            ResetCollidersColor();
            StartCoroutine(WinRoutine());
            return;
        }

        // Correct
        if (ContainerNumber == 1 && currentItem.tag == "Milk")
        {
            AudioManager.instance.PlayMountMilk();
            container1Items[tempContailner1].SetActive(true);
            milkUIitems[tempContailner1].GetChild(0).gameObject.SetActive(true);
            tempContailner1 += 1;
            currentItem.SetActive(false);
            currentItemNumber++;
            showItem(currentItemNumber);
        }

        else if (ContainerNumber == 2 && currentItem.tag == "Chocolate")
        {
            AudioManager.instance.PlayMountChocolate();
            container2Items[tempContailner2].SetActive(true);
            chocolateUIitems[tempContailner2].GetChild(0).gameObject.SetActive(true);
            tempContailner2 += 1;
            currentItem.SetActive(false);
            currentItemNumber++;
            showItem(currentItemNumber);

        }

        // Loose
        else
        {
            if(ContainerNumber == 1)
            {
                container1.GetComponent<MeshRenderer>().material = errorMaterial;
                StartCoroutine(LooseRoutine());
            }
            if (ContainerNumber == 2)
            {
                container2.GetComponent<MeshRenderer>().material = errorMaterial;
                StartCoroutine(LooseRoutine());
            }

        }


    }

    IEnumerator ItemAnimate()
    {
        
        float timeSinceStarted = 0f;

        while (true)
        {
            timeSinceStarted += Time.deltaTime * 2f;
            currentItem.transform.localPosition = Vector3.Lerp(itemStartPos, itemFinalPos, timeSinceStarted);

            // If the object has arrived, stop the coroutine
            if (currentItem.transform.localPosition == itemFinalPos)
            {
                //AudioManager.instance.PlaySuccess();
                //  Debug.Log("Finished !!");
               // yield return new WaitForSeconds(0.2f);
                AudioManager.instance.PlaySwoosh();
                yield break;
            }

            // Otherwise, continue next frame
            yield return null;

        }

     //   yield return new WaitForSeconds(0.2f);



    }

    IEnumerator WinRoutine()
    {
        AudioManager.instance.PlaySuccess();

        feedbackTextPositive.gameObject.SetActive(true);
        yield return new WaitForSeconds(2);
        SceneManager.LoadScene(0);
    }


    IEnumerator LooseRoutine()
    {
        AudioManager.instance.PlayFail();

        Destroy(currentItem);
        feedbackTextNegative.gameObject.SetActive(true);
        yield return new WaitForSeconds(1);
        Reset();
    }
}

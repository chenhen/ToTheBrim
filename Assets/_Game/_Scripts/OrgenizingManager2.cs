﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class OrgenizingManager2 : MonoBehaviour
{
  public static OrgenizingManager2 instance;

  [SerializeField] Vector3 itemStartPos;
  [SerializeField] Vector3 itemFinalPos;

  [SerializeField] List<GameObject> containerItems = new List<GameObject>();
  [SerializeField] List<GameObject> instantiatedItem = new List<GameObject>();
  [SerializeField] List<GameObject> useddItems = new List<GameObject>();
  [SerializeField] List<Transform> milkUIitems = new List<Transform>();

  [SerializeField] Button clickButton;
  [SerializeField] GameObject container;
  [SerializeField] GameObject mountedContainer;
  [SerializeField] Camera secondCamera;

  [SerializeField] GameObject winScreen;

  GameObject currentItem;
  int currentItemNumber = 0;
  int tempContailner = 0;


  private void Awake()
  {
    if (GameManager.instance)
      UnityEngine.Object.Destroy(gameObject);
    else
      instance = this;
  }

  void Start()
  {
    Reset();
  }

  public void Reset()
  {
    secondCamera.gameObject.SetActive(false);
    container.GetComponent<OrgenizedItem2>().enabled = false;
    currentItemNumber = 0;

    for (int i = 0; i < containerItems.Count; i++)
    {
      containerItems[i].SetActive(false);
    }
    for (int i = 0; i < milkUIitems.Count; i++)
    {
      milkUIitems[i].GetChild(0).gameObject.SetActive(false);
    }

    for (int i = 0; i < useddItems.Count; i++)
    {
      Destroy(useddItems[i]);
    }

    useddItems.Clear();

    tempContailner = 0;
    showItem(0);
  }


  public void showItem(int itemNumber)
  {
    AudioManager.instance.PlaySwoosh();
    currentItem = Instantiate(instantiatedItem[itemNumber], itemStartPos, transform.rotation);
    useddItems.Add(currentItem);
    StartCoroutine(ItemAnimate());
  }

  IEnumerator ItemAnimate()
  {

    float timeSinceStarted = 0f;

    while (true)
    {
      // clickButton.enabled = false;
      timeSinceStarted += Time.deltaTime * 5f;
      currentItem.transform.localPosition = Vector3.Lerp(itemStartPos, itemFinalPos, timeSinceStarted);

      // If the object has arrived, stop the coroutine
      if (currentItem.transform.localPosition == itemFinalPos)
      {
        //  clickButton.enabled = true;
        yield break;
      }

      // Otherwise, continue next frame
      yield return null;
    }
  }


  public void InsertItem()
  {
    currentItem.GetComponent<Animator>().enabled = true;

    containerItems[tempContailner].SetActive(true);
    milkUIitems[tempContailner].GetChild(0).gameObject.SetActive(true);
    tempContailner += 1;
    currentItem.SetActive(false);
    currentItemNumber++;

    if (tempContailner < instantiatedItem.Count)
    {
      showItem(currentItemNumber);
    }
    else
    {

      clickButton.enabled = false;
      StartCoroutine(FinishStageOneRoutine());
    }

  }

  IEnumerator FinishStageOneRoutine()
  {
    yield return new WaitForSeconds(0.6f);
    secondCamera.gameObject.SetActive(true);
    AudioManager.instance.PlayPutDown();
    container.GetComponent<OrgenizedItem2>().enabled = true;
  }

  public void FinishStageTwoRoutine()
  {
    container.gameObject.SetActive(false);
    mountedContainer.gameObject.SetActive(true);
    StartCoroutine(WinRoutine());
  }

  IEnumerator WinRoutine()
  {
    yield return new WaitForSeconds(0.2f);
    AudioManager.instance.PlayMountContainer();
    yield return new WaitForSeconds(0.6f);
    winScreen.SetActive(true);
    AudioManager.instance.PlaySuccess();
    yield return new WaitForSeconds(1f);
    SceneManager.LoadScene(0);
  }
}

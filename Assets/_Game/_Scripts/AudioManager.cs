﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioManager : MonoBehaviour
{
  public static AudioManager instance;

  [SerializeField] AudioSource _maudiosource;

  [SerializeField] AudioClip _success;
  [SerializeField] AudioClip _fail;
  [SerializeField] AudioClip _pouring;
  [SerializeField] AudioClip _putDown;
  [SerializeField] AudioClip _reset;
  [SerializeField] AudioClip _swoosh;
  [SerializeField] AudioClip _mountMilk;
  [SerializeField] AudioClip _mountChocolate;
  [SerializeField] AudioClip _mountContainer;

  private void Awake()
  {
    if (instance != null)
    {
      Destroy(gameObject);
    }
    else
    {
      instance = this;
    }
  }

  public void PlaySuccess()
  {
    _maudiosource.clip = _success;
    _maudiosource.Play();
    _maudiosource.loop = false;
  }

  public void PlayFail()
  {
    _maudiosource.clip = _fail;
    _maudiosource.Play();
    _maudiosource.loop = false;
  }

  public void PlayPouring()
  {
    _maudiosource.clip = _pouring;
    _maudiosource.Play();
    _maudiosource.loop = true;
  }

  public void PlayPutDown()
  {
    _maudiosource.clip = _putDown;
    _maudiosource.Play();
    _maudiosource.loop = false;
  }

  public void PlayReset()
  {
    _maudiosource.clip = _reset;
    _maudiosource.Play();
    _maudiosource.loop = false;
  }

  public void PlaySwoosh()
  {
    _maudiosource.clip = _swoosh;
    _maudiosource.Play();
    _maudiosource.loop = false;
  }

  public void PlayMountMilk()
  {
    _maudiosource.clip = _mountMilk;
    _maudiosource.Play();
    _maudiosource.loop = false;
  }

  public void PlayMountChocolate()
  {
    _maudiosource.clip = _mountChocolate;
    _maudiosource.Play();
    _maudiosource.loop = false;
  }

  public void StopEffects()
  {
    _maudiosource.Stop();
    _maudiosource.loop = false;
  }

  public void PlayMountContainer()
  {
    _maudiosource.clip = _mountContainer;
    _maudiosource.Play();
    _maudiosource.loop = false;
  }

}
